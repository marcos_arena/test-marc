Running Imports and Exports
===========================
This is a library with utilities and scripts for the normal scenarios. It is
available in each provider after deploy under the folder `RunnerScripts`.

Exports
-------
In short create a provider specific script called `Invoke-Export.ps1` in the
provider configuration root. If nothing custom is to take place, you can base
the script on the template from `RunnerScripts\Templates\Invoke-
Export.ps1-template`.

If the normal workflow needs to be changed, base your `Invoke-Export.ps1` on
the script `RunnerScripts\Invoke-SunhExport.ps1`.

Imports
-------
In short create a provider specific script called `Invoke-Import.ps1` in the
provider root. If nothing custom is to take place, you can base the script on
the template from `RunnerScripts\Templates\Invoke-Import.ps1-template`.

If the normal workflow needs to be changed, base your `Invoke-Import.ps1` on
the script `RunnerScripts\Invoke-SunhImport.ps1`.

SunHotelsImportExport Cmdlets
-----------------------------
There are several utility cmdlets that is used by the template scripts and are
available for any custom workflows. To gain access to the just import the
module.

    Import-Module SunHotelsImportExport\SunHotelsImportExport.psm1

See the scripts and cmdlets under `RunnerScripts` for examples.

Deployment
----------
The deploy engine takes care of deploying all `*.ps1` files from the provider
configuration folder and the library into each published provider
configuration's folder.

Logging
-------
When a provider configuration is run it's output is put in the file `Last
invoke log <provider config>.txt`.