function Invoke-Export 
{
    param(
        $ProviderConfigs,
        $CachePath = "Cache"
    )

	Import-Module $PSScriptRoot\SunHotelsImportExport\SunHotelsImportExport.psm1

    Write-Output "Clearing cache"
    Clear-SunhProviderCache $CachePath

    foreach ($providerConfig in $ProviderConfigs) 
    {
        Write-Output "Running export $providerConfig"
        Invoke-SunhProvider $providerConfig
    }

    Write-Output "Compressing cache"
    Compress-SunhProviderCache $CachePath

    Write-Output "Publishing cache to web"
    Publish-SunhExport $CachePath

    Write-Output "Clearing cache"
    Clear-SunhProviderCache $CachePath
}
