param(
    $FriendlyName,
    $ProviderConfigs,
    $CachePath = "Cache",
    $PublishSubPath
)

$startTime = (Get-Date)

Import-Module $PSScriptRoot\SunHotelsImportExport\SunHotelsImportExport.psm1 -Force
Clear-Log
Write-Log "Invoking provider $FriendlyName"
$invokeLogFile = "Last invokation log.txt"

try {
    Write-Log "Clearing cache in $CachePath"
    Clear-SunhProviderCache $CachePath

    foreach ($providerConfig in $ProviderConfigs) 
    {  
        Write-Log "Running export config $providerConfig"
        try {
            Invoke-SunhProvider $providerConfig
        }
        catch {
            $errorMessage = $_.Exception.Message
            Write-Log "Exception thrown:`n$errorMessage"
            Write-Log "Sending error notification"

            $errorMessage = $_.Exception.Message
            
            $configLogFile = "Last invoke log $providerConfig.txt"
            Send-SunhErrorNotification -ErrorSummary "Export $FriendlyName failed with config $providerConfig " -ErrorMessage $errorMessage -Attachments $invokeLogFile,$configLogFile
            return
        }
    }

    Write-Log "Compressing files in cache folder $CachePath"
    Compress-SunhProviderCache $CachePath

    Write-Log "Publishing cache to web"
    Publish-SunhExport $CachePath $PublishSubPath

    Write-Log "Clearing cache in $CachePath"
    Clear-SunhProviderCache $CachePath
}
catch {
    $errorMessage = $_.Exception.Message
    Write-Log "Exception thrown:`n$errorMessage"
    Write-Log "Sending error notification"
    $errorMessage = $_.Exception.Message
    Send-SunhErrorNotification -ErrorSummary "Export $FriendlyName failed" -ErrorMessage $errorMessage -Attachments $invokeLogFile
}
finally {
    $endTime = (Get-Date)
    Write-Log "Export invoke ended after $(($endTime - $startTime).TotalSeconds) s"
}