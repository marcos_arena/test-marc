function Get-SunhProviderEnvironment {
    $node = (Select-Xml "configuration/appSettings/add[@key='ConfigMode']" -Path "SunHotels.Loader2.exe.config").Node
    if ($node -and $node.value) 
    {
        return $node.value
    }

    throw "Could not find configuration for environment."
}