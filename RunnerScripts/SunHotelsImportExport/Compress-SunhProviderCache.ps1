function Compress-SunhProviderCache {
    param(
        $CachePath
    )

    $files = gci $CachePath *.xml
    foreach ($file in $files) {
        $zipFilePath = Join-Path $CachePath "$($file.BaseName).zip"
        & "$env:ProgramFiles\7-Zip\7z.exe" a -tzip ""$zipFilePath"" ""$($file.fullName)"" -mx9 -mmt -aoa > $null
        if ($LastExitCode -ne 0) {
            throw "7-zip did not complete. Exit code $LastExitCode"
        }
    }
}
