function Invoke-SunhProvider {
    param(
        $ProviderConfig
    )

    Write-Log "Invoke log available at ""Last invoke log $ProviderConfig.txt"""
    & .\SunHotels.Loader2.exe $ProviderConfig -destSTO | Out-File "Last invoke log $ProviderConfig.txt"
    if ($LastExitCode -ne 0) {
        throw "Provider config $ProviderConfig failed and exited with code $LastExitCode.`nDetails in attached log."
    }
}
