function Get-SunhConfig {
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$true, Position=1)]
        $Key,
        $Config = "Sunhotels",
        $Environment = "Dev"
    )

    $basePath = (Get-ItemProperty -Path hklm:\software\sunhotels\Config | select -expand GlobalConfigPath)
    $configPath = Join-Path $basePath $environment

    $configFile = $Config
    while ($configFile) { 
        $configFilePath = Join-Path $configPath "$configFile.xml"

        if ((Test-Path $configFilePath)) {
            $xPath = $Key -replace "\.","/"
            try {
                $node = (Select-Xml "Settings/$xPath" -Path $configFilePath).Node 
                if ($node -and $node.value) {
                    return $node.value
                }
            }
            catch {
                # Intentionally left blank, just try the next file.
            }
        }

        $lastPeriodIx = $configFile.LastIndexOf(".")
        if ($lastPeriodIx -eq -1) {
            $configFile = ""
        }
        else {
            $configFile = $configFile.Substring(0, $lastPeriodIx)
        }
    }

    throw "Could not find config key ""$Key"" in environment ""$Environment"". Tried recursing upwards from config ""$Config"" in hierarchy with no luck."
}
