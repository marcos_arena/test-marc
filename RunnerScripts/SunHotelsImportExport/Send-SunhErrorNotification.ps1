function Send-SunhErrorNotification {
	param(
		$ErrorSummary,
        $ErrorMessage,
        $Attachments = $null
	)

    $config = "Sunhotels.Import.ProviderToFileImport"
	$environment = Get-SunhProviderEnvironment
    $recipients = (Get-SunhConfig -Key "Mail.Import.NotifyOnError" -Config $config -Environment $environment) -Split ","
    $from = Get-SunhConfig -Key "Mail.FromAddress" -Config $config -Environment $environment
    $smtp = Get-SunhConfig -Key "Mail.Smtp" -Config $config -Environment $environment
    
	Write-Log "Sending error notification to $recipients"

    Send-MailMessage -From $from -To $recipients -SmtpServer $smtp `
        -Subject $ErrorSummary `
        -Body $ErrorMessage `
        -Attachments $Attachments
}