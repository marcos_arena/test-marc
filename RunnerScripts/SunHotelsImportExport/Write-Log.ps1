function Write-Log {
    param(
        $Message
    )
    
    "$(Get-Date -format s) $Message" | Tee-Object "Last invokation log.txt" -append | Write-Output
}

function Clear-Log {
    $logFile = "Last invokation log.txt"
    if (Test-Path $logFile) {
        Remove-Item $logFile > $null
    }
}