function Publish-SunhExport {
    param(
        $CachePath,
        $PublishSubPath
    )
    
    $environment = Get-SunhProviderEnvironment
    $publishBasePath = Get-SunhConfig -Key "Directories.ExportPublishingPath" -Config "Sunhotels.Import.ProviderToFileImport" -Environment $environment
    $publishPath = Join-Path $publishBasePath $PublishSubPath
    Write-Log "Publishing to $publishPath."
    
    if (-not (Test-Path $publishPath)) {
        New-Item $publishPath -ItemType Directory > $null
    }

    Copy-Item $CachePath\* $publishPath 
}
