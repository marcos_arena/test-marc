function Test-SunhProviders {
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$True,Position=1)]
        $ProviderBasePath = "..\Output\Packages\local",
        [int]$BatchSize = 5
        )

    $providers = @(
        @{ Folder = "InternalExport"; Configs = "InternalExport_dk.xml" }
        @{ Folder = "InternalExport120"; Configs = "InternalExport_dk.xml" }
        @{ Folder = "InternalExport130"; Configs = "InternalExport_dk.xml" }
        @{ Folder = "InternalExport140"; Configs = "InternalExport_dk.xml" }
        @{ Folder = "InternalExport150"; Configs = "InternalExport_dk.xml" }

        @{ Folder = "AvailabilityExportAllinc"; Configs = "AvailabilityExport.xml" }
        @{ Folder = "AvailabilityExportBigTravel"; Configs = "AvailabilityExport.xml" }
        @{ Folder = "AvailabilityExportBucherReisen"; Configs = "AvailabilityExportSplitted.xml" }
        @{ Folder = "AvailabilityExportBucherTestDEV"; Configs = "AvailabilityExportSplitted.xml" }
        @{ Folder = "AvailabilityExportConso"; Configs = "AvailabilityExport121.xml", "AvailabilityExport150.xml" }
        @{ Folder = "AvailabilityExportFTI"; Configs = "AvailabilityExport.xml", "AvailabilityExportSplitted.xml" }
        @{ Folder = "AvailabilityExportGetABed"; Configs = "AvailabilityExportSplitted.xml" }
        @{ Folder = "AvailabilityExportH4U"; Configs = "AvailabilityExportSplitted.xml" }
        @{ Folder = "AvailabilityExportJumbo"; Configs = "AvailabilityExport.xml", "AvailabilityExportSplitted.xml" }
        @{ Folder = "AvailabilityExportMeetingPointTest"; Configs = "AvailabilityExport121.xml", "AvailabilityExport150.xml" }
        @{ Folder = "AvailabilityExportStayNo"; Configs = "AvailabilityExportSplitted.xml" }
        @{ Folder = "AvailabilityExportTestExport"; Configs = "AvailabilityExport.xml", "AvailabilityExportSplitted.xml" }
        @{ Folder = "AvailabilityExportTestExportNew"; Configs = "AvailabilityExport.xml", "AvailabilityExportSimplified.xml" }
        @{ Folder = "AvailabilityExportTravelTainment"; Configs = "AvailabilityExport.xml" }
        @{ Folder = "AvailabilityExportTripleMind"; Configs = "AvailabilityExport.xml" }
        #@{ Folder = "AvailabilityExportVTours"; Configs = @() } # No configurations found
        @{ Folder = "AvailabilityExportVToursFull"; Configs = "AvailabilityExport.xml" }

        #@{ Folder = "EuroTours"; Configs = "Config.xml" }
        #@{ Folder = "HotelBeds"; Configs = "HotelBeds.live.xml", "HotelBeds.Mapped.xml", "HotelBeds.UnMapped.xml", "HotelBeds.xml", "HotelBedsPush.live.xml", "HotelBedsPush.xml" }
        #@{ Folder = "Jacobline"; Configs = "Jacobline.xml", "JacoblineAvailabilityOnly.xml" }
        #@{ Folder = "JacoblineXMLNRF"; Configs = "JacoblineNRFAvailabilityOnly.xml", "JacoblineNRFFull.xml" }
        #@{ Folder = "Jumbotours"; Configs = "JumboTours.xml", "JumboToursAvailabilityOnly.xml" }
        #@{ Folder = "JumboToursXMLSpain"; Configs = "JumboToursSpain.xml", "JumboToursSpain_Avail.xml" }
        #@{ Folder = "PeakworkEDF"; Configs = "PeakworkEDF.xml" }
        #@{ Folder = "Tourico"; Configs = "Config.xml", "ConfigAvailabilityOnly.xml" }
        #@{ Folder = "TouricoXMLLive"; Configs = "TouricoXMLLive.static.live.xml" }
        #@{ Folder = "Travalco"; Configs = "Travalco.xml", "TravalcoAvailabilityOnly.xml" }
    )

    function ChunkBy($items,[int]$size) {
        $list = new-object System.Collections.ArrayList
        $tmpList = new-object System.Collections.ArrayList
        foreach($item in $items) {
            $tmpList.Add($item) | out-null
            if ($tmpList.Count -ge $size) {
                $list.Add($tmpList.ToArray()) | out-null
                $tmpList.Clear()
            }
        }

        if ($tmpList.Count -gt 0) {
            $list.Add($tmpList.ToArray()) | out-null
        }

        return $list.ToArray()
    }

    $configurations = $providers | % { 
        $provider = $_.Folder
        $_.Configs | % {
            $config = $_
            @{ Provider = $provider; Config = $config}
        }}

    if (-not (Test-Path $ProviderBasePath)) {
        throw "ProviderBasePath is not a valid directory."
    }

    try {
        Write-Output "##teamcity[testSuiteStarted name='Provider Tests']"
        
        $fullBasePath = Resolve-Path $ProviderBasePath
        $testLogPath = (Join-Path $fullBasePath "Logs")
        if (-not (Test-Path $testLogPath)) {
            New-Item -ItemType directory -Path $testLogPath
        }

        ChunkBy $configurations $BatchSize | % {
            $batch = $_
            $batch | % {
                $provider = $_.Provider
                $config = $_.Config 
                $providerPath = (Join-Path $fullBasePath $provider)
                
                Start-Job -Name "$provider-$config" -ArgumentList $providerPath,$provider,$config,$testLogPath -ScriptBlock {
                    param (
                        $providerPath,
                        $provider,
                        $config,
                        $testLogPath
                    )
                    Write-Output "##teamcity[testStarted name='$provider $config']"
                    $sw = [Diagnostics.Stopwatch]::StartNew()

                    try {
                        pushd $providerPath
                        & .\SunHotels.Loader2.exe $config -destSTO | Out-File (Join-Path $testLogPath "$provider-$config.txt")
                        if ($LastExitCode -ne 0) {
                            Write-Output "##teamcity[testFailed name='$provider $config'] message='Returned exit code $LastExitCode'"
                        }
                    }
                    catch [Exception]
                    {
                        Write-Host $_.Exception.Message
                        Write-Output "##teamcity[testFailed name='$provider $config'] message='The test script threw an exception.' details='The test was probably not executed at all.'"
                    }
                    finally {
                        $sw.Stop()
                        Write-Output "##teamcity[testFinished name='$provider $config' duration='$($sw.ElapsedMilliseconds)']"
                        popd
                    }
                }
            }   
        
            Get-Job | Wait-Job | Receive-Job
            Get-Job | Remove-Job
        }
    }
    finally {
        Write-Output "##teamcity[testSuiteFinished name='Provider Tests']"
    }
}