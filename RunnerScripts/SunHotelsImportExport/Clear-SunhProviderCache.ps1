function Clear-SunhProviderCache {
	param(
        $cachePath
    )

    if ((Test-Path $cachePath)) {
        Remove-Item $cachePath -Recurse -Force
    }

    New-Item $cachePath -ItemType Directory > $null
}
