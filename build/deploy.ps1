param(
    $environment, 
    $password = "",
    $customConfig = @{}
    )

. .\nuget-restore.ps1

$projectRoot = Resolve-Path ..
$buildscriptsDirName = gci ..\packages -Filter SunHotels.Build* | sort -Descending | Select -First 1 -ExpandProperty Name
$buildscriptsPath = "..\packages\$buildscriptsDirName\scripts\"
Push-Location $buildscriptsPath
. .\deploystep.ps1 -projectRoot $projectRoot -environment $environment -password $password -customConfig $customConfig
Pop-Location