$providers = 
    [pscustomobject]@{
        Library =   "AvailabilityExport"
        Configs =   "AvailabilityExportAllinc",
                    "AvailabilityExportBigTravel",
                    "AvailabilityExportBucherReisen",
                    "AvailabilityExportBucherTestDEV",
                    "AvailabilityExportConso",
                    "AvailabilityExportFTI",
                    "AvailabilityExportGetABed",
                    "AvailabilityExportH4U",
                    "AvailabilityExportJumbo",
                    "AvailabilityExportMeetingPointTest",
                    "AvailabilityExportStayNo",
                    "AvailabilityExportTestExport",
                    "AvailabilityExportTestExportNew",
                    "AvailabilityExportTravelTainment",
                    "AvailabilityExportTripleMind",
                    "AvailabilityExportVToursFull"
        },
    [pscustomobject]@{
        Library = "EuroTours"
        Configs = "EuroTours"
        },
    [pscustomobject]@{
        Library = "HotelBeds"
        Configs = "HotelBeds"
        },
    [pscustomobject]@{
        Library =   "InternalExport"
        Configs =   "InternalExport",
                    "InternalExport120",
                    "InternalExport130",
                    "InternalExport140",
                    "InternalExport150"
    },
    [pscustomobject]@{
        Library = "JacoblineXML"
        Configs = "Jacobline",
				  "JacoblineXMLNRF"
        },
# Not sure how JacoblineXMLNRF is supposed to be managed, GBrian mentioned a merge between libraries. // Martin Östemar 2015-01-23        
#    [pscustomobject]@{
#        Library = "JacoblineXMLNRF"
#        Configs = "JacoblineXMLNRF"
#        },
    [pscustomobject]@{
        Library =   "JumbotoursXML"
        Configs =   "Jumbotours",
                    "JumboToursXMLSpain"
        },
    [pscustomobject]@{
        Library =	"PeakworkEDF"
        Configs =	"PeakworkEDF",
					"PeakworkExportVTours",
					"PeakworkExportBucherReisen",
					"PeakworkExportThomasCook"
        },
    [pscustomobject]@{
        Library = "TouricoXML"
        Configs = "Tourico"
        },
    [pscustomobject]@{
        Library = "TouricoXMLLive"
        Configs = "TouricoXMLLive"
        },
	[pscustomobject]@{
        Library = "TravalcoXML"
        Configs = "Travalco"
    },
	[pscustomobject]@{
        Library = "ExpediaXMLLive"
        Configs = "ExpediaXMLLive"
    },
	[pscustomobject]@{
        Library = "TravelgateXMLLive"
        Configs = "AbreuXMLLive",
				  "Hotusa",
				  "Traveltino"
    },
	[pscustomobject]@{
        Library = "DerbySoftXMLLive"
        Configs = "DerbySoftXMLLive"
    }