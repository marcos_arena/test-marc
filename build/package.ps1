param($branch)

. .\nuget-restore.ps1

$projectRoot = Resolve-Path ..
$buildscriptsDirName = gci ..\packages -Filter SunHotels.Build* | sort -Descending | Select -First 1 -ExpandProperty Name
$buildscriptsPath = "..\packages\$buildscriptsDirName\scripts\"
Push-Location $buildscriptsPath
. .\packagestep.ps1 -projectRoot $projectRoot -branch $branch
Pop-Location