param (
	$Destination
)

. $projectRoot\build\providers.ps1
$providers | select -expand Configs | % { 
    [pscustomobject]@{
            Name = "$_"
            Destination = $Destination
        }
    }
