if (!(Get-Command "nuget.exe" -ErrorAction SilentlyContinue)) {
    throw "You must have NuGet installed and in PATH."
}

$sunhotelsNuGet = "http://nuget.shdev.local/NuGet/default"

Write-Output "Restoring solution packages ...";
# Use one of the following lines depending on whether you have a single solution or multiple solutions in the solution root folder.

# Use this when you have a single solution.
& nuget.exe restore .. -Source $sunhotelsNuGet

# Use this when you have multiple solutions.
#& nuget.exe restore ..\<the solution>.sln -Source $sunhotelsNuGet 
