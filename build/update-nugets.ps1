param(
	$preRelease = $false,
	$customConfig = @{},
	$source = "http://nuget.shdev.local/NuGet/default"
)

. .\nuget-restore.ps1

$projectRoot = Resolve-Path ..
$buildscriptsDirName = gci ..\packages -Filter SunHotels.Build* | sort -Descending | Select -First 1 -ExpandProperty Name
$buildscriptsPath = "..\packages\$buildscriptsDirName\scripts\"
Push-Location $buildscriptsPath
. .\update-nugets-step.ps1 -projectRoot $projectRoot -preRelease $preRelease -customConfig $customConfig -source $source
Pop-Location