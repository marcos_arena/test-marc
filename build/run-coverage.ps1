param(
	$minLineCoverage = $null,
	$minBranchCoverage = $null
)

. .\nuget-restore.ps1

$projectRoot = Resolve-Path ..
$buildscriptsDirName = gci ..\packages -Filter SunHotels.Build* | sort -Descending | Select -First 1 -ExpandProperty Name
$buildscriptsPath = "..\packages\$buildscriptsDirName\scripts\"

Push-Location $buildscriptsPath
. .\coveragestep.ps1 -projectRoot $projectRoot -minLineCoverage $minLineCoverage -minBranchCoverage $minBranchCoverage
Pop-Location