properties {
    . ".\build-paths.ps1"

    $source_dir = "$packages_dir\$environment"
    $destination_dir = "$($deployItem.Destination)\$($buildItem.Name)"
    $providerName = $buildItem.Name
}

task PublishProvider -depends ClearProviderDestination,CopyProviderConfiguration,CopyProviderLoader,CopyProviderLibrary,CopyRunnerScripts -precondition { ($customConfig.Providers -split ",") -contains $buildItem.Name} {
}

task ClearProviderDestination {
    if (Test-Path -Path $destination_dir) {
        Remove-Item -Force -Recurse $destination_dir
    }
}

task CopyProviderConfiguration {
    CopyProviderFiles "$source_dir\$providerName" $destination_dir
}

task CopyProviderLoader {
    CopyProviderFiles "$source_dir\Loader" $destination_dir
}

task CopyProviderLibrary {
    CopyProviderFiles "$source_dir\ProviderLibraries\$($buildItem.ProviderLibraryName)" $destination_dir\Providers
}

task CopyRunnerScripts {
	CopyProviderFiles "$source_dir\RunnerScripts" "$destination_dir\RunnerScripts"
}


function CopyProviderFiles($source, $destination) {
    ROBOCOPY $source $destination /NS /NC /NP /NJH /NJS /E

    if ($LASTEXITCODE -eq 16) {
        write-host "##teamcity[buildStatus status='FAILURE' text='Error: Robocopy did not copy any files.']"    
        throw "Error: Robocopy did not copy any files."
    }
    elseif ($LASTEXITCODE -ge 8) {
        write-host "##teamcity[buildStatus status='FAILURE' text='Error: Some files or directories could not be copied (copy errors occurred and the retry limit was exceeded).']"  
        throw "Error: Some files or directories could not be copied (copy errors occurred and the retry limit was exceeded)."
    }
    elseif ($LASTEXITCODE -ge 4) {
        write-host "##teamcity[buildStatus status='FAILURE' text='Error: Some Mismatched files or directories were detected.']" 
        throw "Error: Some Mismatched files or directories were detected."
    }
}