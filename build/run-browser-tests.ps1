param(
    [Parameter(Mandatory=$True,Position=1)]
    $environment
    )

. .\nuget-restore.ps1
$environmentDir = Resolve-Path "..\Output\Packages\$environment"
$testAssemblies = (Get-ChildItem $environmentDir\* -Recurse -i *.BrowserTests.dll)

$projectRoot = Resolve-Path ..
$buildscriptsDirName = gci ..\packages -Filter SunHotels.Build* | sort -Descending | Select -First 1 -ExpandProperty Name
$buildscriptsPath = "..\packages\$buildscriptsDirName\scripts\"
Push-Location $buildscriptsPath
. .\unitteststep.ps1 -projectRoot $projectRoot -testAssemblies $testAssemblies -unitTestWorkingDirectory $environmentDir
Pop-Location

