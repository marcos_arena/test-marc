task PrepareRunnerScriptsFolder {
    Write-Host "Preparing build output for provider $providerName configuration."
    
    $destinationDir = "$output_dir\RunnerScripts"
    $providerSource = "$projectRoot\RunnerScripts"
    ROBOCOPY $providerSource $destinationDir /NS /NC /NP /NJH /E
    if ($lastexitcode -ge 8) {
        throw "Failed to copy runner script files"
    }
}

task PrepareProviderFolder {
    $providerName = $($buildItem.Name)

    Write-Host "Preparing build output for provider $providerName configuration."
    
    $destinationDir = "$output_dir\$providerName"
    $providerSource = "$projectRoot\Config\$providerName"
    ROBOCOPY $providerSource $destinationDir /NS /NC /NP /NJH /E
    if ($lastexitcode -ge 8) {
        throw "Failed to copy provider files"
    }
}

task PrepareProviderLibraryFolder {
    Write-Host "Preparing folder with all provider libraries."

    $librarySources = "$output_dir\SunHotels.Providers.*"
    $destinationDir = "$output_dir\ProviderLibraries"

    gci $librarySources | ForEach {
        $libraryName = ($_.name -split "SunHotels\.Providers\.")[1]
        ROBOCOPY $_.fullName "$destinationDir\$libraryName" "$libraryName.dll" "$libraryName.pdb" /NS /NC /NP /NJH /E 
        if ($lastexitcode -ge 8) {
            throw "Failed to copy provider library files for $_.name"
        }
    }
}
