$solution_file  = "$projectRoot\ProviderToFileImport.sln"

$branchTargets  = @{ 
    "feature/*" = "local","develop_a","develop_b","develop_c","develop_d","develop_main"
    "develop"   = "develop_main","develop_a","develop_b","develop_c","develop_d"
    "release/*" = "develop_main","develop_a","develop_b","develop_c","develop_d","release_test"
    "hotfix/*"  = "develop_main","develop_a","develop_b","develop_c","develop_d","release_test","live"
    "default"   = "develop_main","develop_a","develop_b","develop_c","develop_d","release_test","live"
}

$buildItems = @()
$buildItems += [pscustomobject]@{
    Name                = "Loader"
    
    PSakeTaskPackage    = "PackageExe"
    Source              = "$output_dir\SunHotels.Loader"
    AppExeName          = "SunHotels.Loader2.exe"
}

$buildItems += [pscustomobject]@{
    Name                = "RunnerScripts"
	PSakeTaskPostBuild  = "PrepareRunnerScriptsFolder"
	
    PSakeTaskPackage    = "PackageFiles"
    Source              = "$output_dir\RunnerScripts"
}

$buildItems += [pscustomobject]@{
    Name                = "ProviderLibraries"
    PSakeTaskPostBuild  = "PrepareProviderLibraryFolder"

    PSakeTaskPackage    = "PackageFiles"
    Source              = "$output_dir\ProviderLibraries"
}

. $projectRoot\build\providers.ps1
foreach ($provider in $providers) {
    foreach ($config in $provider.Configs) {
        $buildItems += [pscustomobject]@{
            Name                = "$config"
            PSakeTaskPostBuild  = "PrepareProviderFolder"
            
            PSakeTaskPackage    = "PackageFiles"
            Source              = "$output_dir\$config"

            PSakeTaskDeploy     = "PublishProvider"
            ProviderLibraryName = "$($provider.Library)"
        }    
    }
}
