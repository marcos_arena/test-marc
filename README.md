Provider to FileImport (+Exports)
=================================

Apart from common libraries, the Provider to FileImport project consists of
four major parts:

1.   **Sunhotels.Loader**  
     This is the application entry point for all imports and exports.

2.   **DataCollection\\***  
     All provider specific runners are implemented as separate libraries.

3.   **Config\\***  
     All configurations needed to import and export for all providers. Note
     that a single provider library can be used in several different
     configurations.  Each of these are what are considered separete deploy
     entities. The configuration should include the provider's name in the name. I.e. `Tourico.xml` for Tourico, `JacoblineAvailabilityOnly.xml` for Jacobline (availability only). 

4.   **build\build-config.ps1**  
     The setup of what configuration uses what provider library.


Configuration
=============

Inside the config files, use relative paths. A setting of `<CacheOutput>Cache\Eurotours\Cache.xml</CacheOutput>` will result in this on disk:

    +- <deploy destination>\
		+- EuroTours\
			| +- Cache\
				| +- Eurotours
					| Cache.xml

For some providers, extra settings are necessary. Those settings should be defined in a settings file inside the `Providers` folder and the file should be set in the `ExportConfigFile` element in the config file.

Some providers have the servers, where to get the data from, in a special file. That special "servers" file has to be formatted according to class `SunHotels.Providers.ProviderCommon.Servers`. The file should be placed in the `Providers` folder and for clarity and to avoid confusion with the other settings file, they should be suffixed with `_Servers.xml`. Then the file name should be specified in the `ServersConfigFile` element in the config file.

**Example**:

	<ExportConfigFile>JumboToursSettings.xml</ExportConfigFile>
    <ServersConfigFile>JumboToursXML_Servers.xml</ServersConfigFile>


Build and Deploy
================

To support the build engine there are two things that need to be present.
First the configurations must be set up in `build\build-config.ps1`.  Have a
look at how the existing looks and follow the same pattern.

The seconds thing is to make sure to use Global Config wherever possible.
There is already config transforms present in the build for making sure the
correct environment is used.

For some legacy providers, namely the AvailabilityExport and InternalExport
libraries there are typed datasets that prohibits the use of global config.
For those there is a named connection string in Loader's `app.config` file.
There are config transforms of those as well.


The Engine
----------

All of the steps below are only if you want to build, package and deploy
locally.  For servers all of this is taken care of by the build servers.

To build the project locally, open a PowerShell and go to the `.\build` folder
and issue:

    PS> .\build.ps1

To package each configuration issue the package command.  The branch must be
specified so the build engine knows what deploy targets to transform app
configurations for.

    PS> .\package.ps1 -branch feature/my-cool-feature

This will result in the following folder structure under
`[root]\Output\Packages`.

    .\
    +- develop_a\
    |  +- Provider A\
    |  |  +- Providers\
    |  |  |  | LibrarySetting.xml
    |  |  | ProviderA Config.xml
    |  +- Provider B\
    |  |  | ProviderB config 1.xml
    |  |  | ProviderB config 2.xml
    |  +- ProviderLibraries\
    |  |  +- ProviderLibA\
    |  |     | ProviderLibA.dll
    |  |  +- ProviderLibB\
    |  |     | ProviderLibB.dll    
    |  + SunHotels.Loader\
    |    | SunHotels.Loader.exe
    |    | SunHotels.Loader.exe.config (transformed)
    +- develop_b
    |  +- Provider A\
    ....

To deploy one or more provider configurations you issue a deploy command.  For
example to deploy the provider configurations A, B and C locally you issue:

    PS> .\deploy.ps1 -environment local --customConfig @{Providers="A,B,C"}

The project uses a custom deploy engine that takes the loader app, the
specified provider configuration and the provider library associated with that
configuration as stated in build-config.ps1 and combines them into the correct
folder structure in the deploy destination. An example would be:

    +- deploy destination\
       +- Provider A\
          |  +- Providers\
          |     | LibrarySetting.xml
          |     | ProviderLibA.dll
          | ProviderA Config.xml
          | SunHotels.Loader.exe
          | SunHotels.Loader.exe.config (transformed)

To run the configuration above you would go to the Provider A folder and
issue:

    PS> .\SunHotels.Loader.exe "ProviderA Config.xml"

