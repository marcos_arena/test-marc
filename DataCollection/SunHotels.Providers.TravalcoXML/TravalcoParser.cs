using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace SunHotels.Providers
{
    public class TravalcoParser
    {
        Dictionary<string, TravalcoCountry> countries = new Dictionary<string, TravalcoCountry>();
        public Dictionary<string, TravalcoCountry> Countries
        {
            get { return countries; }
            set { countries = value; }
        }

        Dictionary<string, TravalcoHotel> hotels = new Dictionary<string, TravalcoHotel>();
        public Dictionary<string, TravalcoHotel> Hotels
        {
            get { return hotels; }
            set { hotels = value; }
        }

        Dictionary<string, TravalcoHotelInfo> hotelsInfo = new Dictionary<string, TravalcoHotelInfo>();
        public Dictionary<string, TravalcoHotelInfo> HotelsInfo
        {
            get { return hotelsInfo; }
            set { hotelsInfo = value; }
        }

        Dictionary<string, Dictionary<string, TravalcoAmenity>> amenities = new Dictionary<string, Dictionary<string, TravalcoAmenity>>();
        public Dictionary<string, Dictionary<string, TravalcoAmenity>> Amenities
        {
            get { return amenities; }
            set { amenities = value; }
        }

        Dictionary<string, TravalcoInvt> invt = new Dictionary<string, TravalcoInvt>();
        public Dictionary<string, TravalcoInvt> Invt
        {
            get { return invt; }
            set { invt = value; }
        }

        Dictionary<string, TravalcoHotelRates> hotelRates = new Dictionary<string, TravalcoHotelRates>();
        public Dictionary<string, TravalcoHotelRates> HotelRates
        {
            get { return hotelRates; }
            set { hotelRates = value; }
        }

        Dictionary<string, TravalcoCancellationFee> cancellationFees = new Dictionary<string, TravalcoCancellationFee>();
        public Dictionary<string, TravalcoCancellationFee> CancellationFees
        {
            get { return cancellationFees; }
            set { cancellationFees = value; }
        }

        Dictionary<string, TravalcoCommission> commissions = new Dictionary<string, TravalcoCommission>();
        public Dictionary<string, TravalcoCommission> Commissions
        {
            get { return commissions; }
            set { commissions = value; }
        }

        /// <summary>
        /// Parses all the hotels into objects and puts them in a dictionary
        /// </summary>
        public void ParseHotels()
        {
            XmlDocument xmlDocHotels = new XmlDocument();
            var xml = System.IO.File.ReadAllText("./Tmp/Travalco/hotel2.xml");
            xmlDocHotels.LoadXml(xml);
            //xmlDocHotels.Load("./Tmp/Travalco/hotel2.xml"); it fails

            foreach (XmlNode hotelNode in xmlDocHotels.DocumentElement.SelectNodes("hotel"))
            {
                TravalcoHotel h = new TravalcoHotel(hotelNode);
                if (!hotels.ContainsKey(h.Code.ToUpperInvariant()))
                {
                    hotels.Add(h.Code.ToUpperInvariant(), h);

                    var placeCode = h.Country ?? h.CityCode;
                    var placeName = h.Country ?? h.CityName;
                    TravalcoCountry c = new TravalcoCountry(placeCode, placeName);
                    if (!countries.ContainsKey(c.Code.ToUpperInvariant()))
                    {
                        countries.Add(c.Code.ToUpperInvariant(), c);
                    }
                }
            }
        }

        /// <summary>
        /// Parses all the countries into objects and puts them into a dictionary
        /// </summary>
        public void ParseCountries()
        {
            if (!System.IO.File.Exists("./Tmp/Travalco/countries.xml"))
                return;

            XmlDocument xmlDocCountries = new XmlDocument();
            xmlDocCountries.Load("./Tmp/Travalco/countries.xml");

            foreach (XmlNode countryNode in xmlDocCountries.DocumentElement.SelectNodes("country"))
            {
                TravalcoCountry c = new TravalcoCountry(countryNode);
                if (!countries.ContainsKey(c.Code.ToUpperInvariant()))
                {
                    countries.Add(c.Code.ToUpperInvariant(), c);
                }
            }
        }

        /// <summary>
        /// Parses all the hotelInfo into objects and puts them into a dictionary
        /// </summary>
        public void ParseHotelInfo()
        {
            XmlDocument xmlDocHotelsInfo = new XmlDocument();
            xmlDocHotelsInfo.Load("./Tmp/Travalco/hotelinfo.xml");

            foreach (XmlNode hotelInfoNode in xmlDocHotelsInfo.DocumentElement.SelectNodes("hotel"))
            {
                string hotelId = hotelInfoNode.Attributes["code"].Value;
                TravalcoHotelInfo hInfo = new TravalcoHotelInfo(hotelInfoNode);
                if (!hotelsInfo.ContainsKey(hInfo.Code.ToUpperInvariant()))
                {
                    hotelsInfo.Add(hInfo.Code.ToUpperInvariant(), hInfo);
                }

                XmlNodeList amenitiesList = hotelInfoNode.SelectNodes("amenities/amenity");

                foreach (XmlNode amenityNode in amenitiesList)
                {
                    if (!amenities.ContainsKey(hotelId))
                    {
                        amenities.Add(hotelId, new Dictionary<string, TravalcoAmenity>());
                    }
                    TravalcoAmenity hAmenity = new TravalcoAmenity(amenityNode);
                    if(!amenities[hotelId].ContainsKey(hAmenity.Code))
                        amenities[hotelId].Add(hAmenity.Code, hAmenity);
                }
            }
        }

        /// <summary>
        /// Parses all the Inventory into objects and puts them into a dictionary
        /// </summary>
        public void ParseInvt()
        {
            XmlDocument xmlDocInvt = new XmlDocument();
            xmlDocInvt.Load("./Tmp/Travalco/Invt.xml");

            foreach (XmlNode invtNode in xmlDocInvt.DocumentElement.SelectNodes("hotel"))
            {
                TravalcoInvt hInvt = new TravalcoInvt(invtNode);
                if (!invt.ContainsKey(hInvt.HotelCode.ToUpperInvariant()))
                {
                    invt.Add(hInvt.HotelCode.ToUpperInvariant(), hInvt);
                }
            }
        }

        /// <summary>
        /// Parses all the hotel prices into objects and puts them into a dictionary
        /// </summary>
        public void ParseHotelRates()
        {
            XmlDocument xmlDocHotelRates = new XmlDocument();
            xmlDocHotelRates.Load("./Tmp/Travalco/HotelRates.xml");

            foreach (XmlNode hotelRateNode in xmlDocHotelRates.DocumentElement.SelectNodes("hotel"))
            {
                TravalcoHotelRates hRate = new TravalcoHotelRates(hotelRateNode);
                if (!hotelRates.ContainsKey(hRate.HotelCode.ToUpperInvariant()))
                {
                    hotelRates.Add(hRate.HotelCode.ToUpperInvariant(), hRate);
                }
            }
        }

        /// <summary>
        /// Parses all the cancellation fees into objects and put them into a dictionary
        /// </summary>
        public void ParseCancellationFees()
        {
            XmlDocument xmlCancellationFees = new XmlDocument();
            xmlCancellationFees.Load("./Tmp/Travalco/cxlfee.xml");

            foreach (XmlNode cancellationFeeNode in xmlCancellationFees.DocumentElement.SelectNodes("hotel"))
            {
                TravalcoCancellationFee cancellationFee = new TravalcoCancellationFee(cancellationFeeNode);

                if (!cancellationFees.ContainsKey(cancellationFee.HotelCode.ToUpperInvariant()))
                {
                    cancellationFees.Add(cancellationFee.HotelCode.ToUpperInvariant(), cancellationFee);
                }
            }
        }
      
        /// <summary>
        /// Parses all discounts that are aplicable to sunhotels into objects and puts them into a dictionary
        /// </summary>
        public void ParseCommissions()
        {
            XmlDocument xmlCommissions = new XmlDocument();
			xmlCommissions.Load("./Tmp/Travalco/Commissions_408062.xml");

            foreach (XmlNode commissionNode in xmlCommissions.DocumentElement.SelectNodes("hotel"))
            {
                TravalcoCommission commission = new TravalcoCommission(commissionNode);

                if (!commissions.ContainsKey(commission.HotelCode.ToUpperInvariant()))
                {
                    commissions.Add(commission.HotelCode.ToUpperInvariant(), commission);
                }
            }
        }

        /// <summary>
        /// Calls all the functions that parse Travalco cachefiles and map the nodes into objects
        /// </summary>
        public void ParseAll()
        {
            ParseCountries();
            ParseHotels();
            ParseHotelInfo();
            ParseInvt();
            ParseHotelRates();
            ParseCancellationFees();
            ParseCommissions();
        }
    }
}
