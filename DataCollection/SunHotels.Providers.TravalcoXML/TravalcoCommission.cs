﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace SunHotels.Providers
{
    public class TravalcoCommission
    {
        string hotelCode;
        public string HotelCode
        {
            get { return hotelCode; }
            set { hotelCode = value; }
        }

        List<CommissionCategory> categories;
        public List<CommissionCategory> Categories
        {
            get { return categories; }
            set { categories = value; }
        }

        public TravalcoCommission(XmlNode commissionNode)
        {
            this.hotelCode = commissionNode.Attributes["code"].Value;
            this.categories = new List<CommissionCategory>();

            foreach (XmlNode categoryNode in commissionNode.SelectNodes("category"))
            {
                this.categories.Add(new CommissionCategory(categoryNode));
            }
        }
    }

    public class CommissionCategory
    {
        string code;
        public string Code
        {
            get { return code; }
            set { code = value; }
        }

        string description;
        public string Description
        {
            get { return description; }
            set { description = value; }
        }

        List<CommissionPeriod> periods;
        public List<CommissionPeriod> Periods
        {
            get { return periods; }
            set { periods = value; }
        }

        public CommissionCategory(XmlNode categoryNode)
        {
            this.code = categoryNode.Attributes["code"].Value;
            this.description = categoryNode.Attributes["description"].Value;

            this.periods = new List<CommissionPeriod>();

            foreach (XmlNode period in categoryNode.SelectNodes("period"))
            {
                this.periods.Add(new CommissionPeriod(period));
            }
        }
    }

    public class CommissionPeriod
    {
        string start;
        public string Start
        {
            get { return start; }
            set { start = value; }
        }

        string end;
        public string End
        {
            get { return end; }
            set { end = value; }
        }

        string percentage;
        public string Percentage
        {
            get { return percentage; }
            set { percentage = value; }
        }

        public CommissionPeriod(XmlNode periodNode)
        {
            this.start = periodNode.Attributes["start"].Value;
            this.end = periodNode.Attributes["end"].Value;
            this.percentage = periodNode.Attributes["percentage"].Value;
        }
    }

}





