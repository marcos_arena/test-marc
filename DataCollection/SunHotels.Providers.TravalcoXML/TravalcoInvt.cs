using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace SunHotels.Providers
{
    /// <summary>
    /// A class that corresponds the Travalco inventory node
    /// </summary>
    public class TravalcoInvt
    {

        string hotelCode;
        public string HotelCode
        {
            get { return hotelCode; }
            set { hotelCode = value; }
        }

        List<TravalcoInvtRow> invtRows;
        public List<TravalcoInvtRow> InvtRows
        {
            get { return invtRows; }
            set { invtRows = value; }
        }

        public TravalcoInvt(XmlNode InvtNode)
        {
            this.hotelCode = InvtNode.Attributes["code"].Value;
            this.invtRows = new List<TravalcoInvtRow>();
            foreach (XmlNode invtRowNode in InvtNode.SelectNodes("row"))
            {
                InvtRows.Add(new TravalcoInvtRow(invtRowNode));
            }
        }
    }

    /// <summary>
    /// A class that corresponds the Travalco Inventory Row
    /// </summary>
    public class TravalcoInvtRow
    {
        string cat;
        public string Cat
        {
            get { return cat; }
            set { cat = value; }
        }
        
        string tp;
        public string Tp
        {
            get { return tp; }
            set { tp = value; }
        }
        
        string date;
        public string Date
        {
            get { return date; }
            set { date = value; }
        }
        
        string amt;
        public string Amt
        {
            get { return amt; }
            set { amt = value; }
        }
        
        string av;
        public string Av
        {
            get { return av; }
            set { av = value; }
        }
        
        string minstay;
        public string Minstay
        {
            get { return minstay; }
            set { minstay = value; }
        }
        
        string maxstay;
        public string Maxstay
        {
            get { return maxstay; }
            set { maxstay = value; }
        }
        
        string release;
        public string Release
        {
            get { return release; }
            set { release = value; }
        }

        public TravalcoInvtRow(XmlNode invtRowNode)
        {
            this.cat = invtRowNode.Attributes["cat"].Value;
            this.tp = invtRowNode.Attributes["tp"].Value;
            this.date = invtRowNode.Attributes["date"].Value;
            this.amt = invtRowNode.Attributes["amt"].Value;
            this.av = invtRowNode.Attributes["av"].Value;
            this.minstay = invtRowNode.Attributes["minstay"].Value;
            this.maxstay = invtRowNode.Attributes["maxstay"].Value;
            this.release = invtRowNode.Attributes["release"].Value;
        }
    }

}
