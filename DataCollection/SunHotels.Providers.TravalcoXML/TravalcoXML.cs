using System;
using System.Collections.Generic;
using System.Text;
using SunHotels.XML;
using SunHotels.XML.Data;
using System.Xml;
using System.IO;
using ICSharpCode.SharpZipLib.Zip;
using ICSharpCode.SharpZipLib.Zip.Compression.Streams;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Threading;
using Sunhotels.Export;
using Sunhotels.Export.Utils;
using System.Threading.Tasks;

namespace SunHotels.Providers
{
    public class TravalcoXML : XMLProvider
    {
        private DirectoryInfo tempDirectory = new DirectoryInfo("./Tmp/Travalco/");
        public static IBasicLogger log = null;
        public DirectoryInfo TempDirectory
        {
            get { return tempDirectory; }
            set { tempDirectory = value; }
        }

        Dictionary<string, string> categoryTranslations = new Dictionary<string, string>();
        public Dictionary<string, string> CategoryTranslations
        {
            get { return categoryTranslations; }
            set { categoryTranslations = value; }
        }

        protected override void UpdateConfiguration(Configuration configuration)
        {
            configuration.AutoValidateOutput = false;
            configuration.WriteRoomsWithoutAvailability = false;
        }

        protected override void getData(Root root)
        {

            try
            {


                string filename = string.Format("Travalco {0}.log", DateTime.Now.ToString("yyyyMMdd HHmmss"));
                log = BasicLogger.Create(configuration.LogOutput + filename);
                if (!tempDirectory.Exists)
                {
                    tempDirectory.Create();
                }
                bool downLoadAndUnzip = true;
#if DEBUG
                downLoadAndUnzip = false;
#endif
                if (downLoadAndUnzip)
                {
                    DownloadFiles();
                }




                Console.ForegroundColor = ConsoleColor.DarkYellow;
                Console.WriteLine("======================================================");
                Console.WriteLine(string.Format("************ Start {0} ***************", DateTime.Now));
                Console.WriteLine("======================================================");
                log.WriteLine("Starting process");



                TravalcoParser tParser = new TravalcoParser();
                //Read all needed xml files and create classes out of them for speedier execution
                tParser.ParseAll();

                log.WriteLine("Files parsed. Getting places.");

                Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US", true);
                Thread.CurrentThread.CurrentUICulture = new CultureInfo("en-US", true);


                Dictionary<string, Place> cities = GetPlaces(tParser);

                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("Getting hotels.");
                
                log.WriteLine(tParser.Hotels.Keys.Count + " hotels to process");

                var hotels = 0;
                foreach (TravalcoHotel th in tParser.Hotels.Values)
                {
                    hotels += 1;
                    try
                    {
                        if (hotels % 87 == 86)
                            log.WriteLine(hotels.ToString() + " hotels processed");

                        // if (!th.Code.Contains("ABA-ABAC")) continue;

                        Console.ForegroundColor = ConsoleColor.White;
                        Console.WriteLine("   " + th.Name);
                        Hotel tHotel = new Hotel();
                        tHotel.Id = th.Code.ToUpperInvariant();

                        switch (th.TypeDesc)
                        {
                            case "Apartment": tHotel.AccomodationType = AccomodationType.Apartment; break;
                            case "Villa": tHotel.AccomodationType = AccomodationType.Villa; break;
                            case "Hotel": tHotel.AccomodationType = AccomodationType.Hotel; break;
                            default: tHotel.AccomodationType = AccomodationType.Hotel; break;
                                //default: throw new Exception("Previously unknown accomodationtype changes neccessary to program to implement" + th.TypeDesc);
                        }

                        tHotel.Name = th.Name;

                        tHotel.Adress_City = th.CityName;
                        tHotel.Adress_Country = th.Country;
                        tHotel.Adress_State = th.State;
                        tHotel.Adress_Street1 = th.Adress1;
                        tHotel.Adress_Street2 = th.Adress2;
                        tHotel.Adress_Zipcode = th.Zip;
                        tHotel.Classification = th.Rating;


                        string hotelDesc = "";
                        if (tParser.HotelsInfo.ContainsKey(tHotel.Id.ToUpperInvariant()))
                        {
                            hotelDesc = tParser.HotelsInfo[tHotel.Id.ToUpperInvariant()].GeneralInformation;
                            if (hotelDesc == null)
                            {
                                hotelDesc = "";
                            }
                        }
                        else
                        {
                            hotelDesc = th.TypeDesc;
                        }


                        //removes the characters
                        string regPattern = "(\r)|(\t)|(\n)(\f)(  )*";
                        hotelDesc = Regex.Replace(hotelDesc + "", regPattern, "");

                        if (hotelDesc.Length >= 1000)
                        {
                            hotelDesc = hotelDesc.Remove(999, hotelDesc.Length - 1000);
                        }

                        tHotel.Description = hotelDesc;
                        string headline = th.Name;
                        if (headline.Length > 50)
                        {
                            headline = headline.Remove(49, headline.Length - 50);
                        }

                        tHotel.Email = "";
                        tHotel.Fax = th.LocalFax;


                        tHotel.Headline = "";



                        tHotel.Name = th.Name;
                        tHotel.Phone = th.LocalPhone;
                        tHotel.PlaceId = th.CityCode;
                        tHotel.Position_Latitude = "";
                        tHotel.Position_Longitude = "";
                        tHotel.RoomGroups[tHotel.Id.ToUpperInvariant()] = new RoomGroup();
                        tHotel.RoomGroups[tHotel.Id.ToUpperInvariant()].RoomGroupId = tHotel.Id;
                        if (th.Country != null && th.CityCode != null)
                            cities[String.Format("{0}.{1}", th.Country, th.CityCode)].Hotels.Add(tHotel.Id.ToUpperInvariant(), tHotel);

                        if (tParser.HotelRates != null && tParser.HotelRates.ContainsKey(tHotel.Id.ToUpperInvariant()))
                        {
                            foreach (TravalcoHotelRateCategory thc in tParser.HotelRates[tHotel.Id.ToUpperInvariant()].Categories)
                            {
                                if (!categoryTranslations.ContainsKey(thc.CategoryCode))
                                {
                                    CategoryTranslations.Add(thc.CategoryCode, thc.CategoryDescription);
                                }

                                foreach (TravalcoHotelRateCategoryPeriod period in thc.Periods)
                                {
                                    foreach (TravalcoHotelRateCategoryRoomType rType in period.Roomtypes)
                                    {
                                        string roomId = String.Format("{0}.{1}.{2}", tHotel.Id.ToUpperInvariant(), rType.Type, thc.CategoryCode);
                                        RoomType rt;
                                        if (!root.RoomTypes.ContainsKey(roomId))
                                        {
                                            rt = new RoomType();
                                            rt.Beds = rType.Adultoccupancy;
                                            rt.ExtraBeds = rType.Maxoccupancy - rType.Adultoccupancy;

                                            if (rt.ExtraBeds < 0)
                                            {
                                                //Must be declared wrong in the document when this happens since its wierd to require more adults than there is room for in the room
                                                rt.ExtraBeds = 0;
                                            }
                                            rt.Type_Id = roomId;
                                            rt.Bookable_Id = roomId;
                                            rt.Description = TranslateRoomTypeIdDescription(rt.Type_Id);
                                            root.RoomTypes.Add(rt.Bookable_Id, rt);
                                        }
                                        else
                                        {
                                            rt = root.RoomTypes[roomId];
                                        }

                                        Room r;
                                        if (!tHotel.RoomGroups[tHotel.Id.ToUpperInvariant()].Rooms.ContainsKey(roomId))
                                        {
                                            r = new Room();
                                            r.RoomId = roomId;
                                            r.TypeId = roomId;
                                            r.Description = TranslateRoomTypeIdDescription(roomId);
                                            tHotel.RoomGroups[tHotel.Id.ToUpperInvariant()].Rooms.Add(roomId, r);
                                        }
                                        else
                                        {
                                            r = tHotel.RoomGroups[tHotel.Id.ToUpperInvariant()].Rooms[roomId];
                                        }


                                        DateTime startDate = DateTime.Parse(period.Start);
                                        DateTime endDate = DateTime.Parse(period.End);



                                        DateTime currentDate = startDate;

                                        while (currentDate <= endDate && currentDate <= DateTime.Today.AddDays(configuration.NrDaysInCache))
                                        {
                                            if (currentDate >= DateTime.Today)
                                            {
                                                AvailabilityPeriod per = ParsePeriod(currentDate, tParser, tHotel, thc, rType, rt, period);
                                                if (per != null)
                                                {
                                                    if (!r.AvailabilityPeriods.ContainsKey(currentDate))
                                                    {
                                                        r.AvailabilityPeriods.Add(currentDate, per);
                                                    }
                                                }
                                            }

                                            currentDate = currentDate.AddDays(1);
                                        }


                                    }
                                }
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine("Error with hotel " + th.Name);
                        Console.WriteLine(e.ToString());
                        Console.WriteLine("Skipping hotel");
                        log.WriteLine("Error with hotel " + th.Name + ", skipping - " + e.ToString());
                    }
                }
                log.WriteLine("All hotels processed");

                Optimizer.OptimizeAvailabilityPeriods(root);
                Console.ForegroundColor = ConsoleColor.DarkYellow;
                Console.WriteLine("======================================================");
                Console.WriteLine(string.Format("************* End {0} ****************", DateTime.Now));
                Console.WriteLine("======================================================");
                log.WriteLine("Process finished");

            }
            catch (Exception e)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Error in TravalcoXML.getData()");
                Console.WriteLine(e.ToString());
                log.WriteLine("Error in TravalcoXML.getData() - " + e.ToString());
                throw;
            }

        }

        private AvailabilityPeriod ParsePeriod(DateTime currentDate, TravalcoParser tParser, Hotel tHotel, TravalcoHotelRateCategory thc, TravalcoHotelRateCategoryRoomType rType, RoomType rt, TravalcoHotelRateCategoryPeriod period)
        {
            AvailabilityPeriod per = new AvailabilityPeriod();

            per.DateFrom = currentDate;
            per.DateTo = currentDate;

            if (tParser.Invt.ContainsKey(tHotel.Id.ToUpperInvariant()))
            {
                foreach (TravalcoInvtRow inventory in tParser.Invt[tHotel.Id.ToUpperInvariant()].InvtRows)
                {
                    DateTime dateFrom = DateTime.Parse(inventory.Date);

                    if (inventory.Av == "A" && thc.CategoryCode == inventory.Cat)
                    {
                        if (dateFrom == per.DateFrom)// && dateFrom == per.DateTo)
                        {
                            per.AvailabilityAvailableUnits = Int16.Parse(inventory.Amt);
                            per.MinimumStay = Int16.Parse(inventory.Minstay);

                            TimeSpan releaseDaysSpan = DateTime.Parse(inventory.Date).Subtract(DateTime.Parse(inventory.Release));

                            per.ReleaseDays = (Int16)releaseDaysSpan.Days;

                            break;
                        }
                    }
                }
            }


            if (rType.Rate2Weekdays.ContainsKey(per.DateFrom.DayOfWeek.ToString()))
            {
                double ratePrice2 = (double)rType.Rate2 * rt.Beds;
                per.Pricing_BasePrice = getBasePrice(tHotel.Id, thc.CategoryCode, tParser, per, ratePrice2).ToString();//"###.0", usCulture);
            }
            else
            {
                double ratePrice1 = (double)rType.Rate * rt.Beds;
                per.Pricing_BasePrice = getBasePrice(tHotel.Id, thc.CategoryCode, tParser, per, ratePrice1).ToString();//"###.0", usCulture);
            }

            List<TravalcoHotelRateCategorySupplement> supplementList = period.Supplements;

            double childCost = 0;
            int childMaxAge = 0;

            double juniorCost = 0;
            int juniorMinAge = 0;
            int juniorMaxAge = 0;

            if (supplementList.Count > 0)
            {
                foreach (TravalcoHotelRateCategorySupplement sup in supplementList)
                {

                    if (sup.Type == "JR")
                    {
                        juniorCost = (double)sup.Rate;

                        juniorMinAge = sup.Minage;
                        juniorMaxAge = sup.Maxage;

                    }
                    else if (sup.Type == "CHD")
                    {
                        childCost = (double)sup.Rate;
                        childMaxAge = sup.Maxage;
                    }
                }

                if (rt.ExtraBeds > 0)
                {
                    if (childMaxAge >= 12)
                    {
                        //use the childprice for all children
                        per.Pricing_ExtrabedChild = childCost.ToString();
                    }
                    else if (juniorMaxAge >= 12)
                    {
                        //Use the juniorprice instead if it exists
                        per.Pricing_ExtrabedChild = juniorCost.ToString();
                    }
                    else
                    {
                        //children pay as much as adults
                        double finalChildPrice = double.Parse(per.Pricing_BasePrice) / rt.Beds;
                        per.Pricing_ExtrabedChild = finalChildPrice.ToString();
                    }
                }
            }
            else
            {
                if (rt.ExtraBeds > 0)
                {
                    //children pay as much as adults
                    double finalChildPrice = double.Parse(per.Pricing_BasePrice) / rt.Beds;
                    per.Pricing_ExtrabedChild = finalChildPrice.ToString();
                }

            }
            
     
            per.Pricing_BaseIncludedBoard = "none";
            per.Pricing_BookableBoardId = "RO";
            

            bool hasCancellationPolicies = false;
            //int additiveCount = 0;


            //Dictionary<string, int> cancelPoliciesTypeCount = new Dictionary<string, int>();
            if (tParser.CancellationFees.ContainsKey(tHotel.Id.ToUpperInvariant()))
            {
                hasCancellationPolicies = true;
                //if standard:
                //if cancelled before nightsprior or cxldate then nothing
                foreach (CancellationFeeCategory category in tParser.CancellationFees[tHotel.Id.ToUpperInvariant()].Categories)
                {
                    foreach (CancellationPeriod cPeriod in category.Periods)
                    {
                        DateTime feeStartDate = DateTime.Parse(cPeriod.Start);
                        DateTime feeEndDate = DateTime.Parse(cPeriod.End);
                        if (per.DateFrom.CompareTo(feeStartDate) >= 0 && per.DateTo.CompareTo(feeEndDate) <= 0)
                        {
                            foreach (CancellationPenalty penalty in cPeriod.Penalties)
                            {
                                //Noshow rooms are allways charged fully by sunhotels, so no need for policies here
                                if (penalty.Type == "noshow") continue;

                                CancellationPolicy policy = new CancellationPolicy();
                                //this cancellation policy is aplied if one cancells after this date
                                if (penalty.Cxldate != null)
                                {
                                    policy.Id = string.Format("{0}.{1}|{2}|{3}", penalty.Type, penalty.Cxldate, penalty.Percentage, penalty.Flat);
                                    policy.Deadline_Value = (int?)(feeStartDate.Subtract(DateTime.Parse(penalty.Cxldate)).TotalHours);
                                }
                                else
                                {
                                    policy.Id = string.Format("{0}.{1}|{2}|{3}|{4}", penalty.Type, penalty.Nights, penalty.Nightsprior, penalty.Percentage, penalty.Flat);
                                    policy.Deadline_Value = Int32.Parse(penalty.Nightsprior) * 24;
                                }

                                //additiveCount = 0;

                                string penaltyFeeType = "";
                                if (Int32.Parse(penalty.Nights) > 0)
                                {
                                    //additiveCount++;
                                    penaltyFeeType = "nights";
                                }
                                if (Double.Parse(penalty.Percentage) > 0)
                                {
                                    //additiveCount++;
                                    penaltyFeeType = "percentage";
                                }
                                if (Double.Parse(penalty.Flat) > 0)
                                {
                                    //additiveCount++;
                                    penaltyFeeType = "flat";
                                }

                                //if (!cancelPoliciesTypeCount.ContainsKey(policy.Id))
                                //{
                                //    cancelPoliciesTypeCount.Add(policy.Id, additiveCount);
                                //}

                                if (penaltyFeeType == "nights")
                                {
                                    policy.Penalty_Basis = "nights";
                                    policy.Penalty_Value = Int32.Parse(penalty.Nights);
                                }
                                else if (penaltyFeeType == "percentage")
                                {
                                    policy.Penalty_Basis = "full_stay";
                                    policy.Penalty_Value = (int)Double.Parse(penalty.Percentage);
                                }
                                else if (penaltyFeeType == "flat")
                                {
                                    policy.Penalty_Basis = "fixed_amount";
                                    policy.Penalty_Value = (int)Double.Parse(penalty.Flat);
                                }


                                policy.Deadline_Basis = "arrival";
                                policy.Deadline_Unit = "hours";

                                if (!per.CancellationPolicies.Contains(policy.Id))
                                {
                                    per.CancellationPolicies.Add(policy.Id);
                                }

                                if (!root.CancellationsPolicies.ContainsKey(policy.Id))
                                {
                                    root.CancellationsPolicies.Add(policy.Id, policy);
                                }
                            }
                        }
                    }
                }
            }

            if (per.AvailabilityAvailableUnits > 0 && hasCancellationPolicies)
            {
                return per;
            }
            return null;
        }

        private void DownloadFiles()
        {
            FTPCommunication ftpCom;
            Dictionary<string, string> directoryList = new Dictionary<string, string>();

            Console.WriteLine("Connecting to Travalco FTP server");
            ftpCom = new FTPCommunication("ftpsunh", "marcos");
            ftpCom.GetDirectoryList("ftp://ftp.travalco.com/XML-Live/", directoryList);
            Console.WriteLine("Connection successfull");

            //Connect to Travalco FTP server and fetch the Travalco files 
            Console.WriteLine("Downloading files from the server");

            //Download every zipfile in the directory list
            foreach (System.Collections.Generic.KeyValuePair<string, string> fileInDir in directoryList)
            {
                if (fileInDir.Value.Contains(".zip"))
                {
                    if (fileInDir.Value != "v4.2c.zip" && fileInDir.Value != "Images.zip")
                    {
                        ftpCom.DownloadFile(fileInDir.Key.ToString(), string.Format("./Tmp/Travalco/{0}", fileInDir.Value.ToString()));
                    }
                }
            }
            //A special case, this file is needed to calculate discounts for sunhotels
            ftpCom.DownloadFile("ftp://ftp.travalco.com/XML-Live/408062/Commissions_408062.zip", string.Format("./Tmp/Travalco/{0}", "Commissions_408062.zip"));

            FileInfo[] zipFileList = tempDirectory.GetFiles("*.zip");

            foreach (FileInfo zipFile in zipFileList)
            {
                UnZip(zipFile.FullName, "./Tmp/Travalco/");
                System.IO.File.Delete(zipFile.FullName);
            }
        }

        protected override void getProviderDefinition(Root root)
        {
            root.ProviderDefinition.Name = "TravalcoXML";
            root.ProviderDefinition.Currency = "USD";
        }

        //Translates the short room description for a more verbose and informative one
        private string TranslateRoomTypeIdDescription(string roomTypeId)
        {
            string[] roomtypeIdParts = roomTypeId.Split('.');

            string part1Translation = "";
            string part2Translation = "";
            string roomtype = roomtypeIdParts[1].ToString();

            switch (roomtype)
            {
                case "SGL": part1Translation = "Single"; break;
                case "DBL": part1Translation = "Double"; break;
                case "TPL": part1Translation = "Triple"; break;
                case "QUA": part1Translation = "Quadraple"; break;
                default: part1Translation = ""; break;
            }

            part2Translation = categoryTranslations[roomtypeIdParts[2]];

            string finalRoomTypeDesc = string.Format("{0} {1}", part1Translation, part2Translation);
            return finalRoomTypeDesc;
        }

        //Returns the final baseprice of the room with discounts applied if anyare available
        private double getBasePrice(string hotelCode, string categoryCode, TravalcoParser tParser, AvailabilityPeriod per, double rate)
        {
            double finalPrice = rate;
            if (tParser.Commissions.ContainsKey(hotelCode.ToUpperInvariant()))
            {
                foreach (CommissionCategory category in tParser.Commissions[hotelCode.ToUpperInvariant()].Categories)
                {
                    if (category.Code == categoryCode)
                    {
                        foreach (CommissionPeriod period in category.Periods)
                        {
                            DateTime commissionStartDate = DateTime.Parse(period.Start);
                            DateTime commissionEndDate = DateTime.Parse(period.End);
                            if (per.DateFrom.CompareTo(commissionStartDate) >= 0 && per.DateTo.CompareTo(commissionEndDate) < 0)
                            {
                                double perPercentage = double.Parse(period.Percentage);
                                if (perPercentage > 0.0)
                                {
                                    finalPrice = rate - (rate * (perPercentage / 100));
                                }
                            }
                        }
                    }
                }

            }
            return finalPrice;
        }
        private Dictionary<string, Place> GetPlaces(TravalcoParser tParser)
        {
            Dictionary<string, Place> cities = new Dictionary<string, Place>(1000);
            foreach (TravalcoCountry c in tParser.Countries.Values)
            {
                Place countryPlace = new Place();
                countryPlace.Id = c.Code;
                countryPlace.Description = c.Name;
                root.Places.Add(countryPlace);
                foreach (TravalcoHotel h in tParser.Hotels.Values)
                {
                    string cityCode = String.Format("{0}.{1}", c.Code, h.CityCode);
                    if (!cities.ContainsKey(cityCode))
                    {
                        Place cityPlace = new Place();
                        cityPlace.Id = cityCode;
                        cityPlace.Description = h.CityName;
                        countryPlace.Places.Add(cityPlace);
                        cities.Add(cityCode, cityPlace);

                    }
                }
            }
            return cities;
        }

        /// <summary>
        /// Unzip specified ZIP file content to specified target directory.
        /// </summary>
        /// <param name="sFile">The path to the zipfile.</param>
        /// <param name="sTargetDirectory">The directory used to save the extracted zipfile.</param>
        /// <returns>The list of unziped files.</returns>
        private List<string> UnZip(string sFile, string sTargetDirectory)
        {
            List<string> listFiles = new List<string>();

            using (ZipInputStream s = new ZipInputStream(File.OpenRead(sFile)))
            {
                try
                {
                    ZipEntry theEntry;
                    while ((theEntry = s.GetNextEntry()) != null)
                    {
                        string directoryName = sTargetDirectory;
                        string fileName = Path.GetFileName(theEntry.Name);

                        if (fileName != String.Empty)
                        {
                            using (FileStream streamWriter = File.Create(directoryName + theEntry.Name))
                            {
                                listFiles.Add(streamWriter.Name);

                                int size = 2048;
                                byte[] data = new byte[2048];
                                while (true)
                                {
                                    size = s.Read(data, 0, data.Length);
                                    if (size > 0)
                                    {
                                        streamWriter.Write(data, 0, size);
                                    }
                                    else
                                    {
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new ApplicationException(string.Format("Unable to unzip file {0} to directory {1}", sFile, sTargetDirectory), ex);
                }
            }
            return listFiles;
        }
    }
}
