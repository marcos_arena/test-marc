﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace SunHotels.Providers
{
    public class TravalcoCancellationFee
    {
        string hotelCode;
        public string HotelCode
        {
            get { return hotelCode; }
            set { hotelCode = value; }
        }

        List<CancellationFeeCategory> categories;
        public List<CancellationFeeCategory> Categories
        {
            get { return categories; }
            set { categories = value; }
        }

        public TravalcoCancellationFee(XmlNode cancelFeeNode)
        {
            this.hotelCode = cancelFeeNode.Attributes["code"].Value;
            this.categories = new List<CancellationFeeCategory>();

            foreach (XmlNode categoryNode in cancelFeeNode.SelectNodes("category"))
            {
                this.categories.Add(new CancellationFeeCategory(categoryNode));
            }
        }
    }

    public class CancellationFeeCategory
    {
        string code;
        public string Code
        {
            get { return code; }
            set { code = value; }
        }

        string description;
        public string Description
        {
            get { return description; }
            set { description = value; }
        }

        List<CancellationPeriod> periods;
        public List<CancellationPeriod> Periods
        {
            get { return periods; }
            set { periods = value; }
        }

        public CancellationFeeCategory(XmlNode categoryNode)
        {
            this.code = categoryNode.Attributes["code"].Value;
            this.description = (categoryNode.Attributes["description"] != null) ? categoryNode.Attributes["description"].Value : "";

            this.periods = new List<CancellationPeriod>();

            foreach (XmlNode period in categoryNode.SelectNodes("period"))
            {
                this.periods.Add(new CancellationPeriod(period));
            }
        }
    }

    public class CancellationPeriod
    {
        string start;
        public string Start
        {
            get { return start; }
            set { start = value; }
        }

        string end;
        public string End
        {
            get { return end; }
            set { end = value; }
        }

        List<CancellationPenalty> penalties;
        public List<CancellationPenalty> Penalties
        {
            get { return penalties; }
            set { penalties = value; }
        }

        public CancellationPeriod(XmlNode periodNode)
        {
            this.start = periodNode.Attributes["start"].Value;
            this.end = periodNode.Attributes["end"].Value;

            this.penalties = new List<CancellationPenalty>();

            foreach (XmlNode penalty in periodNode.SelectNodes("penalty"))
            {
                this.penalties.Add(new CancellationPenalty(penalty));
            }
        }
    }

    public class CancellationPenalty
    {
        string type;
        public string Type
        {
            get { return type; }
            set { type = value; }
        }

        string nightsprior;
        public string Nightsprior
        {
            get { return nightsprior; }
            set { nightsprior = value; }
        }

        string cxldate;
        public string Cxldate
        {
            get { return cxldate; }
            set { cxldate = value; }
        }

        string nights;
        public string Nights
        {
            get { return nights; }
            set { nights = value; }
        }

        string percentage;
        public string Percentage
        {
            get { return percentage; }
            set { percentage = value; }
        }

        string flat;
        public string Flat
        {
            get { return flat; }
            set { flat = value; }
        }

        public CancellationPenalty(XmlNode penaltyNode)
        {
            this.type = penaltyNode.Attributes["type"].Value;

            if (penaltyNode.Attributes["nightsprior"] != null)
            {
                this.nightsprior = penaltyNode.Attributes["nightsprior"].Value;
            }

            if (penaltyNode.Attributes["cxldate"] != null)
            {
                this.cxldate = penaltyNode.Attributes["cxldate"].Value;
            }

            this.nights = penaltyNode.Attributes["nights"].Value;
            this.percentage = penaltyNode.Attributes["percentage"].Value;
            this.flat = penaltyNode.Attributes["flat"].Value;
        }
    }

}
