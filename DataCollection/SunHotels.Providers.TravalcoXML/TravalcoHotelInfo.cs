using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace SunHotels.Providers
{
    public class TravalcoHotelInfo
    {
        string code;
        public string Code
        {
            get { return code; }
            set { code = value; }
        }

        string generalInformation;
        public string GeneralInformation
        {
            get { return generalInformation; }
            set { generalInformation = value; }
        }

        string mealInformation;
        public string MealInformation
        {
            get { return mealInformation; }
            set { mealInformation = value; }
        }

        public TravalcoHotelInfo(XmlNode hotelInfoNode)
        {
            this.code = hotelInfoNode.Attributes["code"].Value;
            if (hotelInfoNode.SelectSingleNode("infosections/infosection/writeup") != null)
            {
                XmlNodeList infoSection = hotelInfoNode.SelectNodes("infosections/infosection");

                foreach (XmlNode info in infoSection)
                {

                    if (info.Attributes["code"] != null && info.Attributes["code"].Value == "GENINFO")
                    {
                        this.generalInformation = hotelInfoNode.SelectSingleNode("infosections/infosection/writeup").InnerText;
                    }
                    if (info.SelectSingleNode("writeup").Attributes["title"] != null && info.SelectSingleNode("writeup").Attributes["title"].Value == "Meals")
                    {
                        this.MealInformation = info.InnerText;
                    }
                }
            }
        }
    }

    public class TravalcoAmenity
    {
        string code;
        public string Code
        {
            get { return code; }
            set { code = value; }
        }

        string dataItem1;
        public string DataItem1
        {
            get { return dataItem1; }
            set { dataItem1 = value; }
        }

        string dataitem2;
        public string Dataitem2
        {
            get { return dataitem2; }
            set { dataitem2 = value; }
        }

        public TravalcoAmenity(XmlNode amenityNode)
        {
            this.code = amenityNode.Attributes["code"].Value;

            foreach (XmlNode dataItem in amenityNode.SelectNodes("./dataitem"))
            {
                if (dataItem.Attributes["id"].Value == "1")
                {
                    this.dataItem1 = dataItem.Attributes["value"].Value;
                }
                else if (dataItem.Attributes["id"].Value == "2")
                {
                    this.dataitem2 = dataItem.Attributes["value"].Value;
                }
            }
        }
    }
}
