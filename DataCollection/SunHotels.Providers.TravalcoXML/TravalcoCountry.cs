using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace SunHotels.Providers
{
    public class TravalcoCountry
    {
        string code;
        public string Code
        {
            get { return code; }
            set { code = value; }
        }
        
        string name;
        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public TravalcoCountry(XmlNode countryNode)
        {
            this.code = countryNode.Attributes["code"].Value;
            this.name = countryNode.Attributes["name"].Value;
        }
        public TravalcoCountry(string code, string name)
        {
            this.code = code;
            this.name = name;
        }
    }
}
