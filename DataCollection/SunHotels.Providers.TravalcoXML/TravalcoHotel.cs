using SunHotels.XML.HelpClasses;
using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace SunHotels.Providers
{
    public class TravalcoHotel : TravalcoXML
    {
        string code;
        
        public string Code
        {
            get { return code; }
            set { code = value; }
        }

        string name;
        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        string adress1;
        public string Adress1
        {
            get { return adress1; }
            set { adress1 = value; }
        }
        
        string adress2;
        public string Adress2
        {
            get { return adress2; }
            set { adress2 = value; }
        }
        
        string adress3;
        public string Adress3
        {
            get { return adress3; }
            set { adress3 = value; }
        }
        
        string city;
        public string City
        {
            get { return city; }
            set { city = value; }
        }
        
        string state;
        public string State
        {
            get { return state; }
            set { state = value; }
        }
        
        string zip;
        public string Zip
        {
            get { return zip; }
            set { zip = value; }
        }
        
        string country;
        public string Country
        {
            get { return country; }
            set { country = value; }
        }
        
        string localPhone;
        public string LocalPhone
        {
            get { return localPhone; }
            set { localPhone = value; }
        }
        
        string localFax;
        public string LocalFax
        {
            get { return localFax; }
            set { localFax = value; }
        }
        
        string cityCode;
        public string CityCode
        {
            get { return cityCode; }
            set { cityCode = value; }
        }
       
        string cityName;
        public string CityName
        {
            get { return cityName; }
            set { cityName = value; }
        }
        
        string airport;
        public string Airport
        {
            get { return airport; }
            set { airport = value; }
        }
        
        string airportDesc;
        public string AirportDesc
        {
            get { return airportDesc; }
            set { airportDesc = value; }
        }
        
        string region;
        public string Region
        {
            get { return region; }
            set { region = value; }
        }
        
        string regionDesc;
        public string RegionDesc
        {
            get { return regionDesc; }
            set { regionDesc = value; }
        }
        
        string type;
        public string Type
        {
            get { return type; }
            set { type = value; }
        }
        
        string typeDesc;
        public string TypeDesc
        {
            get { return typeDesc; }
            set { typeDesc = value; }
        }
        
        string rating;
        public string Rating
        {
            get { return rating; }
            set { rating = value; }
        }
        
        string ratingDesc;
        public string RatingDesc
        {
            get { return ratingDesc; }
            set { ratingDesc = value; }
        }
        
        string childDesc;
        public string ChildDesc
        {
            get { return childDesc; }
            set { childDesc = value; }
        }
        
        string invtRoom;
        public string InvtRoom
        {
            get { return invtRoom; }
            set { invtRoom = value; }
        }
        
        string taxRatePercentage;
        public string TaxRatePercentage
        {
            get { return taxRatePercentage; }
            set { taxRatePercentage = value; }
        }
        
        string taxRatePerRoom;
        public string TaxRatePerRoom
        {
            get { return taxRatePerRoom; }
            set { taxRatePerRoom = value; }
        }
        
        string taxRatePerPerson;
        public string TaxRatePerPerson
        {
            get { return taxRatePerPerson; }
            set { taxRatePerPerson = value; }
        }

        public TravalcoHotel(XmlNode hotelNode)
        {
            try
            {
                this.code = hotelNode.Attributes["code"].Value;
                this.name = hotelNode.Attributes["name"].Value;

                XmlNode hotelAdress = hotelNode.SelectSingleNode("Address");
                if (hotelAdress != null)
                {
                    this.adress1 = hotelAdress.Attributes["Address1"].Value;
                    this.adress2 = hotelAdress.Attributes["Address2"].Value;
                    this.adress3 = hotelAdress.Attributes["Address3"].Value;
                    this.city = hotelAdress.Attributes["City"].Value;
                    this.state = hotelAdress.Attributes["State"].Value;
                    this.zip = hotelAdress.Attributes["Zip"].Value;
                    this.country = hotelAdress.Attributes["Country"].Value;
                }

                XmlNode Phone = hotelNode.SelectSingleNode("Phone");
                if (Phone != null)
                {
                    localPhone = Phone.Attributes["Local-Phone"].Value;
                    localFax = Phone.Attributes["Local-Fax"].Value;
                }

                XmlNode Location = hotelNode.SelectSingleNode("Location");
                cityCode = Location.Attributes["City-Code"].Value;
                cityName = Location.Attributes["City-Name"].Value;
                airport = Location.Attributes["Airport"].Value;
                airportDesc = Location.Attributes["Airport-Desc."].Value;
                region = Location.Attributes["Region"].Value;
                regionDesc = Location.Attributes["Region-Desc."].Value;

                XmlNode hotelType = hotelNode.SelectSingleNode("Type");
                type = (hotelType != null) ? hotelType.Attributes["Type"].Value : "";
                typeDesc = (hotelType != null) ? hotelType.Attributes["Type-Desc."].Value : "";

                XmlNode hotelRating = hotelNode.SelectSingleNode("Rating");
                rating = (hotelRating != null) ? hotelRating.Attributes["Rating"].Value : "";
                ratingDesc = (hotelRating != null) ? hotelRating.Attributes["Rating-Desc."].Value : "";

                XmlNode hotelGeneral = hotelNode.SelectSingleNode("General");
                childDesc = hotelGeneral.Attributes["Child-Desc."].Value;
                if (hotelGeneral.Attributes["InvtRoom"] != null)
                {
                    invtRoom = hotelGeneral.Attributes["InvtRoom"].Value;
                }

                XmlNode hotelTax = hotelNode.SelectSingleNode("Tax");
                taxRatePercentage = hotelTax.Attributes["Tax-Rate-Percentage"].Value;
                taxRatePerRoom = hotelTax.Attributes["Tax-Rate-Per-Room"].Value;
                taxRatePerPerson = hotelTax.Attributes["Tax-Rate-Per-Person"].Value;
            }
            catch  
            {                                
                string message = string.Format("Discard hotel - Name: {0}. Code: {1}",this.name, this.code);
                log.WriteLine(message);
                Console.WriteLine(message);
            }
           
        }

    }
}
