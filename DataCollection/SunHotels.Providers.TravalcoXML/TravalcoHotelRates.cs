using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Globalization;

namespace SunHotels.Providers
{
    public class TravalcoHotelRates
    {
        string hotelCode;
        public string HotelCode
        {
            get { return hotelCode; }
            set { hotelCode = value; }
        }

        string hotelName;
        public string HotelName
        {
            get { return hotelName; }
            set { hotelName = value; }
        }

        Dictionary<string, string> categoryTranslations = new Dictionary<string, string>();
        public Dictionary<string, string> CategoryTranslations
        {
            get { return categoryTranslations; }
            set { categoryTranslations = value; }
        }

        List<TravalcoHotelRateCategory> categories = new List<TravalcoHotelRateCategory>();
        public List<TravalcoHotelRateCategory> Categories
        {
            get { return categories; }
            set { categories = value; }
        }

        public TravalcoHotelRates(XmlNode hotelRateNode)
        {
            this.hotelCode = hotelRateNode.Attributes["code"].Value;
            this.hotelName = hotelRateNode.Attributes["name"].Value;
            foreach (XmlNode hotelRateCategory in hotelRateNode.SelectNodes("category"))
            {
                this.Categories.Add(new TravalcoHotelRateCategory(hotelRateCategory));

            }
        }
    }

    public class TravalcoHotelRateCategory
    {
        string categoryCode;
        public string CategoryCode
        {
            get { return categoryCode; }
            set { categoryCode = value; }
        }

        string categoryDescription;
        public string CategoryDescription
        {
            get { return categoryDescription; }
            set { categoryDescription = value; }
        }

        List<TravalcoHotelRateCategoryPeriod> periods = new List<TravalcoHotelRateCategoryPeriod>();
        public List<TravalcoHotelRateCategoryPeriod> Periods
        {
            get { return periods; }
            set { periods = value; }
        }

        public TravalcoHotelRateCategory(XmlNode hotelRateCategoryNode)
        {
            this.categoryCode = hotelRateCategoryNode.Attributes["code"].Value;
            this.categoryDescription = hotelRateCategoryNode.Attributes["description"].Value;

            foreach (XmlNode categoryPeriod in hotelRateCategoryNode.SelectNodes("period"))
            {
                this.Periods.Add(new TravalcoHotelRateCategoryPeriod(categoryPeriod));
            }
        }
    }

    public class TravalcoHotelRateCategoryPeriod
    {
        string start;
        public string Start
        {
            get { return start; }
            set { start = value; }
        }

        string end;
        public string End
        {
            get { return end; }
            set { end = value; }
        }

        List<TravalcoHotelRateCategoryRoomType> roomtypes = new List<TravalcoHotelRateCategoryRoomType>();
        public List<TravalcoHotelRateCategoryRoomType> Roomtypes
        {
            get { return roomtypes; }
            set { roomtypes = value; }
        }

        List<TravalcoHotelRateCategorySupplement> supplements = new List<TravalcoHotelRateCategorySupplement>();
        public List<TravalcoHotelRateCategorySupplement> Supplements
        {
            get { return supplements; }
            set { supplements = value; }
        }

        public TravalcoHotelRateCategoryPeriod(XmlNode categoryPeriod)
        {
            this.start = categoryPeriod.Attributes["start"].Value;
            this.end = categoryPeriod.Attributes["end"].Value;

            foreach (XmlNode roomtype in categoryPeriod.SelectNodes("bookings/rates/room"))
            {
                this.Roomtypes.Add(new TravalcoHotelRateCategoryRoomType(roomtype));
            }
            foreach (XmlNode supplement in categoryPeriod.SelectNodes("bookings/rates/supplement"))
            {
                this.Supplements.Add(new TravalcoHotelRateCategorySupplement(supplement));
            }
        }
    }

    public class TravalcoHotelRateCategoryRoomType
    {
        IFormatProvider culture = new CultureInfo("en-GB", true);
        IFormatProvider usCulture = new CultureInfo("en-US", true);

        string type;
        public string Type
        {
            get { return type; }
            set { type = value; }
        }

        int adultoccupancy;
        public int Adultoccupancy
        {
            get { return adultoccupancy; }
            set { adultoccupancy = value; }
        }

        int maxoccupancy;
        public int Maxoccupancy
        {
            get { return maxoccupancy; }
            set { maxoccupancy = value; }
        }

        decimal rate;
        public decimal Rate
        {
            get { return rate; }
            set { rate = value; }
        }

        decimal rate2;
        public decimal Rate2
        {
            get { return rate2; }
            set { rate2 = value; }
        }

        Dictionary<string, bool> rate2Weekdays = new Dictionary<string, bool>();
        public Dictionary<string, bool> Rate2Weekdays
        {
            get { return rate2Weekdays; }
            set { rate2Weekdays = value; }
        }

        bool rateCommissionable;
        bool rate2Commissionable;
        decimal rateCommission;
        decimal rate2Commission;

        public TravalcoHotelRateCategoryRoomType(XmlNode roomtype)
        {
            this.type = roomtype.Attributes["type"].Value;
            this.adultoccupancy = Int32.Parse(roomtype.Attributes["adultoccupancy"].Value);
			this.maxoccupancy = this.adultoccupancy;
            this.rate = decimal.Parse(roomtype.Attributes["rate"].Value, culture);

            if (roomtype.ParentNode.Attributes["mon"] != null && roomtype.ParentNode.Attributes["mon"].Value == "rate2")
            {
                Rate2Weekdays.Add("Monday", true);
            }
            if (roomtype.ParentNode.Attributes["tue"] != null && roomtype.ParentNode.Attributes["tue"].Value == "rate2")
            {
                Rate2Weekdays.Add("Tuesday", true);
            }
            if (roomtype.ParentNode.Attributes["wed"] != null && roomtype.ParentNode.Attributes["wed"].Value == "rate2")
            {
                Rate2Weekdays.Add("Wednesday", true);
            }
            if (roomtype.ParentNode.Attributes["thu"] != null && roomtype.ParentNode.Attributes["thu"].Value == "rate2")
            {
                Rate2Weekdays.Add("Thursday", true);
            }
            if (roomtype.ParentNode.Attributes["fri"] != null && roomtype.ParentNode.Attributes["fri"].Value == "rate2")
            {
                Rate2Weekdays.Add("Friday", true);
            }
            if (roomtype.ParentNode.Attributes["sat"] != null && roomtype.ParentNode.Attributes["sat"].Value == "rate2")
            {
                Rate2Weekdays.Add("Saturday", true);
            }
            if (roomtype.ParentNode.Attributes["sun"] != null && roomtype.ParentNode.Attributes["sun"].Value == "rate2")
            {
                Rate2Weekdays.Add("Sunday", true);
            }

            if (roomtype.Attributes["rate2"] != null)
            {
                this.rate2 = decimal.Parse(roomtype.Attributes["rate2"].Value, culture);
            }

            XmlNodeList commissionsList = roomtype.ParentNode.SelectNodes("./commissions/rate");

            foreach (XmlNode rateNode in commissionsList)
            {
                if (rateNode.Name == "rate")
                {
                    if (rateNode.Attributes["commissionable"] != null && rateNode.Attributes["commissionpct"] != null)
                    {
                        this.rateCommissionable = bool.Parse(rateNode.Attributes["commissionable"].Value);
                        this.rateCommission = decimal.Parse(rateNode.Attributes["commissionpct"].Value, culture);
                    }
                }
                else if (rateNode.Name == "rate2")
                {
                    if (rateNode.Attributes["commissionable"] != null && rateNode.Attributes["commissionpct"] != null)
                    {
                        this.rate2Commissionable = bool.Parse(rateNode.Attributes["commissionable"].Value);
                        this.rate2Commission = decimal.Parse(rateNode.Attributes["commissionpct"].Value, culture);
                    }
                }
            }


            if (this.rateCommissionable)
            {
                this.rate = this.rate - (this.rate * (this.rateCommission / 100));
            }
            if (this.rate2Commissionable)
            {
                this.rate2 = this.rate2 - (this.rate2 * (this.rate2Commission / 100));
            }
        }
    }

    public class TravalcoHotelRateCategorySupplement
    {
        IFormatProvider culture = new CultureInfo("en-GB", true);
        IFormatProvider usCulture = new CultureInfo("en-US", true);
        string type;
        public string Type
        {
            get { return type; }
            set { type = value; }
        }

        int minage;
        public int Minage
        {
            get { return minage; }
            set { minage = value; }
        }

        int maxage;
        public int Maxage
        {
            get { return maxage; }
            set { maxage = value; }
        }

        decimal rate;
        public decimal Rate
        {
            get { return rate; }
            set { rate = value; }
        }


        public TravalcoHotelRateCategorySupplement(XmlNode supplement)
        {
            this.type = supplement.Attributes["type"].Value;
            this.minage = Int32.Parse(supplement.Attributes["minage"].Value);
            this.maxage = Int32.Parse(supplement.Attributes["maxage"].Value);
            this.rate = decimal.Parse(supplement.Attributes["rate"].Value, culture);
        }
    }
}
