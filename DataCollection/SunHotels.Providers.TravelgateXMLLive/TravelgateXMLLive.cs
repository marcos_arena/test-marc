using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Linq;
using System.Linq;
using SunHotels.XML;
using SunHotels.XML.Data;
using SunHotels.Providers.ProviderCommon.DataLoader;
using SunHotels.Providers.Xmltravelgate;
using System.Net;
using System.ServiceModel;

namespace SunHotels.Providers
{
    public class TravelgateXMLLive : XMLProvider, IStaticDataLogger
    {
        #region Global Variable Declaration

        // Create dictionaries for fast search
        Dictionary<string, Place> allPlaces = new Dictionary<string, Place>();
        Dictionary<string, Hotel> allHotels = new Dictionary<string, Hotel>();
        TravelgateXmlLiveSettings travelgateXMLLiveSettings = new TravelgateXmlLiveSettings();

        #endregion

        #region XMLProvider Implementation Methods

        /// <summary>
        /// getProviderDefinition
        /// </summary>
        /// <param name="root">Root</param>
        protected override void getProviderDefinition(Root root)
        {
            root.ProviderDefinition.Name = configuration.ProviderName;
            root.ProviderDefinition.Currency = configuration.Currency;
        }

        /// <summary>
        /// Update Configuration
        /// </summary>
        /// <param name="configuration">Configuration</param>
        protected override void UpdateConfiguration(Sunhotels.Export.Configuration configuration)
        {
            configuration.WriteHotelsWithoutRooms = true;
            configuration.WriteRoomsWithoutAvailability = true;
        }

        /// <summary>
        /// Entry point for class.
        /// </summary>
        /// <param name="root">Root for Sunhotels XML structure.</param>
        protected override void getData(Root root)
        {
            try
            {
                this.root = root;

                // ------------------------------------------------------------------------------------------------
                // Create log stream
                // ------------------------------------------------------------------------------------------------
                try
                {
                    Directory.CreateDirectory(configuration.LogOutput);
                }
                catch (Exception ex)
                {
                    throw new ApplicationException("Unable to create TravelgateXMLLive log file", ex);
                }
                Log(ILoggerStaticDataSeverity.Info, "Starting process");
                // Get all static data 
                GetStaticData();
                Log(ILoggerStaticDataSeverity.Info, "Processing all hotels");
                foreach (var item in allHotels)
                {
                    if (!allPlaces.ContainsKey(item.Value.PlaceId.ToUpper()))
                        Log(ILoggerStaticDataSeverity.Info, "Problem: place '{0}' not found?", item.Value.PlaceId);

                    allPlaces[item.Value.PlaceId.ToUpper()].Hotels.Add(item.Value.Id, item.Value);
                }
                Log(ILoggerStaticDataSeverity.Info, "Process finished");
            }
            catch (Exception ex)
            {
                Log(ILoggerStaticDataSeverity.Info, "Unhanded exception [{0}] ", ex.ToString());
                throw;
            }

        }

        /// <summary>
        /// Get Global Feature mapping for the provider
        /// </summary>
        /// <param name="root">Root</param>
        protected override void getFeatureTypes(Root root)
        {
            try
            {
                // Get SH features mapped with the provider
                if (DataMapper != null && DataMapper.SunHotelsFeatures != null)
                    DataMapper.SunHotelsFeatures.Join(DataMapper["Feature"], o => o.Key, i => i.Key, (o, i) => o)
                        .All(f =>
                        {
                            root.Features.Add(new Feature()
                            {
                                Id = f.Key,
                                Value = f.Value,
                                Name = f.Value
                            });
                            return true;
                        });
            }
            catch (Exception ex)
            {
                Log(ILoggerStaticDataSeverity.Error, string.Format("Error while getting feature types: {0}", ex.ToString()));
            }
        }

        /// <summary>
        /// Log function
        /// </summary>
        /// <param name="severity">ILoggerStaticDataSeverity</param>
        /// <param name="message">message</param>
        /// <param name="args">arguments</param>
        public void Log(ILoggerStaticDataSeverity severity, string message, params object[] args)
        {
            var s = XML.HelpClasses.Log.MessageType.Informational;
            if (severity == ILoggerStaticDataSeverity.Error)
                s = XML.HelpClasses.Log.MessageType.Error;
            if (severity == ILoggerStaticDataSeverity.Warning)
                s = XML.HelpClasses.Log.MessageType.Warning;
            LogEntry(s, message, args);
        }

        #endregion

        #region Private void

        /// <summary>
        /// Get all static data from text files and bind with Dictionary objects.
        /// </summary>
        private void GetStaticData()
        {
            try
            {
                var features = DataMapper.SunHotelsFeatures.Join(DataMapper["Feature"], o => o.Key, i => i.Key,
                    (o, i) => new { SHFeature = o, ProviderFeature = i })
                    .SelectMany(o => o.ProviderFeature.Value.Select(of => new { ProviderId = of, o.SHFeature }))
                    .ToDictionary(o => o.ProviderId, o => o.SHFeature);
                Log(ILoggerStaticDataSeverity.Info, "Getting Providers information started");
                GetProviders();
                Log(ILoggerStaticDataSeverity.Info, "Getting Providers information finished");
                travelgateXMLLiveSettings.Providers.Where(c => !string.IsNullOrEmpty(c.Code)).All(d =>
                {
                    var providerCode = d.Code;
                    Log(ILoggerStaticDataSeverity.Info, string.Format("Processing Providers:{0},Started", providerCode));

                    string continuationToken = "";

                    //HotelListRS hotelListRs;
                    var HotelResult = new List<Business.Hotel>();

                    do
                    {
                        var hotelListRs = GetHotelList(providerCode, continuationToken);

                        if (!string.IsNullOrEmpty(hotelListRs.providerRS.rs))
                        {
                            var hotelList = GeneralUtilities.DeserializeXml<Business.HotelListRS>(hotelListRs.providerRS.rs);

                            HotelResult.AddRange(hotelList.Hotels.ToList());

                            continuationToken = hotelList?.ContinuationToken?.Token;
                        }
                        else
                            continuationToken = ""; // exit

                    } while (!string.IsNullOrEmpty(continuationToken));

                    Log(ILoggerStaticDataSeverity.Info, string.Format("Ready for start proccessing: {0} hotels", HotelResult.Count));
                    /// ready for proccessing

                    if (HotelResult.Count > 0 && HotelResult.Any(c => c != null && !string.IsNullOrEmpty(c.Code)))
                    {
                        var allHotels = HotelResult.Where(e => e != null && !string.IsNullOrEmpty(e.Code)).ToList();
                        Log(ILoggerStaticDataSeverity.Info, string.Format("Hotels to process for {1}: {0}", allHotels.Count, providerCode));
                        allHotels.AsParallel().WithDegreeOfParallelism(configuration.MaxDegreeOfParallelism).All(e =>
                        {
                            GetHotelDetails(e, providerCode, e.Code, features);
                            return true;
                        });
                    }
                    Log(ILoggerStaticDataSeverity.Info, string.Format("Processing Providers:{0},Finished", providerCode));
                    return true;

                });

            }
            catch (Exception ex)
            {
                Log(ILoggerStaticDataSeverity.Error, string.Format("Error while getting provider static data.{0}", ex.ToString()));
            }
        }

        /// <summary>
        /// Get Providers details
        /// </summary>
        private void GetProviders()
        {
            travelgateXMLLiveSettings = GetTravelgateXmlLiveSettings();
        }

        /// <summary>
        /// Get Hotel Details
        /// </summary>
        /// <param name="providerCode">provider Code</param>
        /// <param name="hotelCode">hotel Code</param>
        /// <param name="features">Dictionary of string, KeyValuePair of string, string </param>
        private void GetHotelDetails(Business.Hotel hotelData, string providerCode, string hotelCode, Dictionary<string, KeyValuePair<string, string>> features)
        {
            try
            {
                var descriptiveInfos = new Dictionary<string, DescriptiveInfoRS>();
                Business.DescriptiveInfoRS descriptiveInfoRs = null;

                if (string.IsNullOrEmpty(travelgateXMLLiveSettings.PerformDescriptiveInfoRequest) || travelgateXMLLiveSettings.PerformDescriptiveInfoRequest == "1")
                {
                    descriptiveInfos = GetDescriptiveInfos(providerCode, hotelCode);
                    if (descriptiveInfos != null && descriptiveInfos.Any(c => c.Key == travelgateXMLLiveSettings.RequestLanguageCode && c.Value != null))
                        descriptiveInfoRs = GeneralUtilities.DeserializeXml<Business.DescriptiveInfoRS>(descriptiveInfos.FirstOrDefault(c => c.Key == travelgateXMLLiveSettings.RequestLanguageCode).Value.providerRS.rs);


                    if (descriptiveInfoRs == null || descriptiveInfoRs.Hotel == null || string.IsNullOrEmpty(descriptiveInfoRs.Hotel.Code)) return;

                }
                else
                {
                    descriptiveInfoRs = new Business.DescriptiveInfoRS() { Hotel = hotelData };
                }

                var hotel = descriptiveInfoRs.Hotel;
                var place = GetPlace(hotel.CountryISOCode, hotel.CountryISOCode, hotel.Town, hotel.Town);

                if (place == null)
                {
                    Log(ILoggerStaticDataSeverity.Info, "Hotel {0} discarded - town not specified", hotel.Code);
                    return;
                }

                var hotelImages = GetHotelImages(providerCode, hotelCode, descriptiveInfoRs);
                var countryIsoCode = hotel.CountryISOCode ?? string.Empty;

                var translationsHeadlines = new List<TranslationsHeadline>();
                var translationsDescriptions = new List<TranslationsDescription>();
                GetTranslations(descriptiveInfos, countryIsoCode, translationsHeadlines, translationsDescriptions);

                var description = GetHotelDescription(hotel);
                var newHotel = CreateHotel(features, hotel, place, hotelImages, countryIsoCode, translationsHeadlines, translationsDescriptions, description);

                if (!allHotels.ContainsKey(hotelCode))
                {
                    lock (allHotels)
                    {
                        allHotels.Add(newHotel.Id, newHotel);
                        hotelsProcessed += 1;
                        if (lastProcessed.AddMinutes(3) < DateTime.Now)
                        {
                            lastProcessed = DateTime.Now;
                            Log(ILoggerStaticDataSeverity.Info, "{0} hotels processed", hotelsProcessed.ToString());
                        }
                    }
                }
                else
                    Log(ILoggerStaticDataSeverity.Info, "Hotel Code: {0} already exist in the hotel collection", newHotel.Id);
            }
            catch (Exception ex)
            {
                Log(ILoggerStaticDataSeverity.Error, string.Format("Error while getting provider: {0}, Hotel Details:{1}, {2}", providerCode, hotelCode, ex.ToString()));
            }
        }

        private int hotelsProcessed = 0;
        private DateTime lastProcessed = DateTime.MinValue;

        #endregion

        #region Private static

        /// <summary>
        /// Get Translations
        /// </summary>
        /// <param name="descriptiveInfos">Dictionary of string, DescriptiveInfoRS</param>
        /// <param name="countryIsoCode">ISO country Code</param>
        /// <param name="translationsHeadlines">List of TranslationsHeadline</param>
        /// <param name="translationsDescriptions">List of TranslationsDescription</param>
        private void GetTranslations(Dictionary<string, DescriptiveInfoRS> descriptiveInfos, string countryIsoCode, List<TranslationsHeadline> translationsHeadlines, List<TranslationsDescription> translationsDescriptions)
        {
            descriptiveInfos.Where(c => !string.IsNullOrEmpty(c.Key) && c.Value != null).All(d =>
            {
                var descriptiveInfo = GeneralUtilities.DeserializeXml<Business.DescriptiveInfoRS>(d.Value.providerRS.rs);
                if (!IsValidDescriptiveInfoRs(descriptiveInfo))
                    return true;
                var hotelDescription = GetHotelDescription(descriptiveInfo.Hotel);
                translationsHeadlines.Add(new TranslationsHeadline { country = countryIsoCode, lang = d.Key, Value = hotelDescription });
                translationsDescriptions.Add(new TranslationsDescription { country = countryIsoCode, lang = d.Key, Value = hotelDescription });
                return true;
            });
        }

        /// <summary>
        /// Valid DescriptiveInfoRS
        /// </summary>
        /// <param name="descriptiveInfo">DescriptiveInfoRS</param>
        /// <returns>boolean</returns>
        private static bool IsValidDescriptiveInfoRs(Business.DescriptiveInfoRS descriptiveInfo)
        {
            return !(descriptiveInfo == null || descriptiveInfo.Hotel == null || string.IsNullOrEmpty(descriptiveInfo.Hotel.Code));
        }

        /// <summary>
        /// Get Hotel Description
        /// </summary>
        /// <param name="hotel">Hotel</param>
        /// <returns>string</returns>
        private static string GetHotelDescription(Business.Hotel hotel)
        {
            var description = string.Concat(
                    !string.IsNullOrEmpty(hotel.LongDescription) ? hotel.LongDescription : string.Empty,
                    !string.IsNullOrEmpty(hotel.HowToGet) ? hotel.HowToGet : string.Empty,
                    !string.IsNullOrEmpty(hotel.RoomsDescription) ? hotel.RoomsDescription : string.Empty,
                    !string.IsNullOrEmpty(hotel.SituationDescription) ? hotel.SituationDescription : string.Empty,
                    !string.IsNullOrEmpty(hotel.RestaurantsDescription) ? hotel.RestaurantsDescription : string.Empty,
                    !string.IsNullOrEmpty(hotel.PoolsDescription) ? hotel.PoolsDescription : string.Empty,
                    !string.IsNullOrEmpty(hotel.ActivitiesDescription) ? hotel.ActivitiesDescription : string.Empty,
                    !string.IsNullOrEmpty(hotel.ServicesDescription) ? hotel.ServicesDescription : string.Empty,
                    !string.IsNullOrEmpty(hotel.AdditionalDetails) ? hotel.AdditionalDetails : string.Empty);
            return description;
        }

        #endregion

        #region Private Get Type

        /// <summary>
        /// Validate response from Travelgate
        /// </summary>
        /// <param name="respone">MessageTravelHotelBaseRS</param>
        /// <returns>boolean</returns>
        private bool ValidateResponse(MessageTravelHotelBaseRS respone)
        {
            try
            {
                if (respone == null)
                    return false;
                switch (respone.GetType().Name)
                {
                    case "HotelListRS":
                        var hotelList = (HotelListRS)respone;
                        return hotelList.providerRS != null && !string.IsNullOrEmpty(hotelList.providerRS.rs) &&
                               (hotelList.providerRS.hubProviderStatus != null &&
                                hotelList.providerRS.hubProviderStatus.hubProviderError == null)
                               && string.IsNullOrEmpty(hotelList.providerRS.hubProviderStatus.exceptionString);
                    case "RoomListRS":
                        var roomList = (RoomListRS)respone;
                        return roomList.providerRS != null && !string.IsNullOrEmpty(roomList.providerRS.rs) &&
                               (roomList.providerRS.hubProviderStatus != null &&
                                roomList.providerRS.hubProviderStatus.hubProviderError == null)
                               && string.IsNullOrEmpty(roomList.providerRS.hubProviderStatus.exceptionString);
                    case "DescriptiveInfoRS":
                        var descriptiveInfo = (DescriptiveInfoRS)respone;
                        return descriptiveInfo.providerRS != null &&
                               !string.IsNullOrEmpty(descriptiveInfo.providerRS.rs) &&
                               (descriptiveInfo.providerRS.hubProviderStatus != null &&
                                descriptiveInfo.providerRS.hubProviderStatus.hubProviderError == null)
                               && string.IsNullOrEmpty(descriptiveInfo.providerRS.hubProviderStatus.exceptionString);
                    case "DescriptiveInfoExtendedRS":
                        var descriptiveInfoExtended = (DescriptiveInfoExtendedRS)respone;
                        return descriptiveInfoExtended.providerRS != null &&
                               !string.IsNullOrEmpty(descriptiveInfoExtended.providerRS.rs) &&
                               (descriptiveInfoExtended.providerRS.hubProviderStatus != null &&
                                descriptiveInfoExtended.providerRS.hubProviderStatus.hubProviderError == null
                                &&
                                string.IsNullOrEmpty(
                                    descriptiveInfoExtended.providerRS.hubProviderStatus.exceptionString));
                    case "GeographicDestinationTreeRS":
                        var geographicDestinationTree = (GeographicDestinationTreeRS)respone;
                        return geographicDestinationTree.providerRS != null &&
                               !string.IsNullOrEmpty(geographicDestinationTree.providerRS.rs) &&
                               (geographicDestinationTree.providerRS.hubProviderStatus != null &&
                                geographicDestinationTree.providerRS.hubProviderStatus.hubProviderError == null
                                &&
                                string.IsNullOrEmpty(
                                    geographicDestinationTree.providerRS.hubProviderStatus.exceptionString));
                }
                return false;
            }
            catch (Exception ex)
            {
                Log(ILoggerStaticDataSeverity.Error, string.Format("Error while validating travelgate response.Method name:{0}, {1}", respone != null ? respone.GetType().Name : string.Empty, ex.Message));
                return false;
            }
        }

        /// <summary>
        /// Create Hotel
        /// </summary>
        /// <param name="features">features</param>
        /// <param name="hotel">Hotel</param>
        /// <param name="place">Place</param>
        /// <param name="hotelImages">List of Hotel.Image</param>
        /// <param name="countryIsoCode">ISO country Code</param>
        /// <param name="translationsHeadlines">List of TranslationsHeadline</param>
        /// <param name="translationsDescriptions">List of TranslationsDescription</param>
        /// <param name="description">Hotel description</param>
        /// <returns>Hotel</returns>
        private Hotel CreateHotel(Dictionary<string, KeyValuePair<string, string>> features, Business.Hotel hotel, Place place,
            List<Hotel.Image> hotelImages, string countryIsoCode, List<TranslationsHeadline> translationsHeadlines,
            List<TranslationsDescription> translationsDescriptions, string description)
        {
            if (place == null) throw new ApplicationException("Error, place is NULL");
            if (hotel == null) throw new ApplicationException("Error, hotel is NULL");

            var hot = new Hotel
            {
                Id = hotel.Code,
                PlaceId = place.Id,
                Name = !string.IsNullOrEmpty(hotel.Name) ? hotel.Name : string.Empty,
                Headline = !string.IsNullOrEmpty(hotel.ShortDescription) ? hotel.ShortDescription : string.Empty,
                Description = !string.IsNullOrEmpty(description) ? description : string.Empty,
                Adress_Street1 = !string.IsNullOrEmpty(hotel.Address) ? hotel.Address : string.Empty,
                Adress_Zipcode = !string.IsNullOrEmpty(hotel.ZipCode) ? hotel.ZipCode : string.Empty,
                Adress_City = !string.IsNullOrEmpty(hotel.Town) ? hotel.Town : string.Empty,
                Adress_Country = countryIsoCode,
                Position_Latitude = !string.IsNullOrEmpty(hotel.Latitude) ? hotel.Latitude : string.Empty,
                Position_Longitude = !string.IsNullOrEmpty(hotel.Longitude) ? hotel.Longitude : string.Empty,
                Phone = hotel.Contact != null && !string.IsNullOrEmpty(hotel.Contact.Telephone) ? hotel.Contact.Telephone : string.Empty,
                Fax = hotel.Contact != null && !string.IsNullOrEmpty(hotel.Contact.Fax) ? hotel.Contact.Fax : string.Empty,
                Email = hotel.Contact != null && !string.IsNullOrEmpty(hotel.Contact.Email) ? hotel.Contact.Email : string.Empty,
                Classification = !string.IsNullOrEmpty(hotel.CategoryCode) ? hotel.CategoryCode : string.Empty,
                BestBuy = hotel.ExclusiveDealSpecified && hotel.ExclusiveDeal,
                AdultOnly = false,
                BlockInfant = false,
                Images = hotelImages,
                Identifiers = new[] { new Identifier { Type = "provider", Value = "TravelgateXMLLive" } }.ToList()
            };

            hot.AccomodationType = GetAccomodation(hotel.Type);
            hot.Features = GetHotelFeatures(hotel, features);
            hot.Translations = !translationsHeadlines.Any() && !translationsDescriptions.Any() ?
                null : new Translations { headline = translationsHeadlines, description = translationsDescriptions };

            return hot;
        }

        /// <summary>
        /// Get Hotel Images
        /// </summary>
        /// <param name="providerCode">providerCode</param>
        /// <param name="hotelCode">hotelCode</param>
        /// <param name="descriptiveInfoRs">DescriptiveInfoRS</param>
        /// <returns>List of Hotel.Image</returns>
        private List<Hotel.Image> GetHotelImages(string providerCode, string hotelCode, Business.DescriptiveInfoRS descriptiveInfoRs)
        {
            var hotelImages = new List<Hotel.Image>();
            try
            {
                if (descriptiveInfoRs.Hotel.Images != null && descriptiveInfoRs.Hotel.Images.Any(c => c != null && !string.IsNullOrEmpty(c.URL)))
                    descriptiveInfoRs.Hotel.Images.Where(c => c != null && !string.IsNullOrEmpty(c.URL)).All(c =>
                    {
                        hotelImages.Add(new Hotel.Image
                        {
                            ImageVariants = new List<Hotel.ImageVariant>() {
                                new Hotel.ImageVariant() { URL = c.URL, Height=travelgateXMLLiveSettings.ImageHeight, Width=travelgateXMLLiveSettings.ImageWidth } }
                        });
                        return true;
                    });
            }
            catch (Exception ex)
            {
                Log(ILoggerStaticDataSeverity.Error, string.Format("Error while getting hotel images: Provider:{0}, Hotel:{1}, {2}", providerCode, hotelCode, ex.ToString()));
            }
            return hotelImages;
        }

        /// <summary>
        /// Get Hotel Features
        /// </summary>
        /// <param name="hotel">Hotel</param>
        /// <param name="features">features Dictionary key pair value</param>
        /// <returns></returns>
        private List<Feature> GetHotelFeatures(Business.Hotel hotel, Dictionary<string, KeyValuePair<string, string>> features)
        {
            try
            {
                var featureCollection = new List<Feature>();
                if (features == null || hotel == null || hotel.Attributes == null)
                    return featureCollection;
                hotel.Attributes.Select(c =>
                {
                    if (!features.ContainsKey(c.Code)) return true;
                    var shfeature = features[c.Code];
                    if (featureCollection.All(f => f.Id != shfeature.Key))
                    {
                        featureCollection.Add(new Feature()
                        {
                            Id = shfeature.Key,
                            Name = shfeature.Value,
                            Value = shfeature.Value
                        });
                    }
                    return true;
                }).ToList();
                return featureCollection;
            }
            catch (Exception ex)
            {
                Log(ILoggerStaticDataSeverity.Error,
                    string.Format("Error while getting hotel Features.Hotel:{0}:{1}", hotel != null ? hotel.Code : string.Empty, ex.ToString()));
                return null;
            }
        }

        /// <summary>
        /// Get Accommodation type 
        /// </summary>
        /// <param name="accomodationType">accomodationType in string format</param>
        /// <returns>AccomodationType</returns>
        private AccomodationType GetAccomodation(string accomodationType)
        {
            try
            {
                switch (accomodationType)
                {
                    case "H":
                    case "C":
                    case "AT":
                    case "HS":
                        return AccomodationType.Hotel;
                    case "A":
                    case "AH":
                    case "B":
                        return AccomodationType.Apartment;
                    case "V":
                    case "CA":
                        return AccomodationType.Villa;
                    default:
                        return AccomodationType.Hotel;
                }
            }
            catch (Exception ex)
            {
                Log(ILoggerStaticDataSeverity.Error, string.Format("Error while processing AccomodationType function in:{0}", ex.ToString()));
                return AccomodationType.Hotel;
            }
        }

        /// <summary>
        /// Get TravelgateXMLLiveSettings
        /// </summary>
        /// <param name="fileName">fileName</param>
        /// <returns></returns>
        private TravelgateXmlLiveSettings GetTravelgateXmlLiveSettings(string fileName = "TravelgateXMLLiveSettings")
        {
            try
            {
                return GeneralUtilities.Deserialize<TravelgateXmlLiveSettings>(XDocument.Load(configuration.ProviderConfigRoot + "\\" + fileName + ".xml"));
            }
            catch (Exception ex)
            {
                Log(ILoggerStaticDataSeverity.Error, string.Format("Error while getting TravelgateXmlLiveSettings:{0}", ex.ToString()));
                return null;
            }
        }

        /// <summary>
        /// Get DescriptiveInfo
        /// </summary>
        /// <param name="providerCode">provider Code</param>
        /// <param name="hotelCode">hotel Code</param>
        /// <returns>DescriptiveInfoRS</returns>
        private Dictionary<string, DescriptiveInfoRS> GetDescriptiveInfos(string providerCode, string hotelCode)
        {
            try
            {
                return travelgateXMLLiveSettings.LanguageCodes.LanguageCode.ToDictionary(code => code, code => GetDescription(providerCode, hotelCode, code));
            }
            catch (Exception ex)
            {
                Log(ILoggerStaticDataSeverity.Error, string.Format("Error while getting DescriptiveInfos. provider:{0}, Hotel:{1}, {2}", providerCode, hotelCode, ex.ToString()));
                return null;
            }
        }

        /// <summary>
        /// Get language specific description
        /// </summary>
        /// <param name="providerCode">providerCode</param>
        /// <param name="hotelCode">hotelCode</param>
        /// <param name="language">language</param>
        /// <returns></returns>
        private DescriptiveInfoRS GetDescription(string providerCode, string hotelCode, string language)
        {
            try
            {
                if (string.IsNullOrEmpty(providerCode) || string.IsNullOrEmpty(hotelCode))
                    return null;
                var client = GetServiceClient(configuration.WebServiceUrl, travelgateXMLLiveSettings.RequestTimeout);
                var rawRequest = GeneralUtilities.GetXmlFromObject(new Business.DescriptiveInfoRQ
                {
                    timeoutMilliseconds = travelgateXMLLiveSettings.RequestTimeout,
                    source = new Business.Source { languageCode = language },
                    Configuration = GetProviderConfigurationData(providerCode),
                    filterAuditData = new Business.FilterAuditData
                    {
                        registerTransactions = travelgateXMLLiveSettings.RegisterTransactions
                    },
                    Hotel = new Business.CodeD { Code = hotelCode }
                });
                if (travelgateXMLLiveSettings.SaveRequestAndResponseInLogs)
                    Log(ILoggerStaticDataSeverity.Warning, "DescriptiveInfo Request: {0}", rawRequest);

                var request = new DescriptiveInfoRQ
                {
                    providerRQ = new ProviderRQ
                    {
                        id = "1",
                        code = providerCode,
                        rqXML = XDocument.Parse(rawRequest).Descendants("DescriptiveInfoRQ").FirstOrDefault()
                    },
                    timeoutMilliseconds = travelgateXMLLiveSettings.RequestTimeout,
                    version = travelgateXMLLiveSettings.RequestVersion
                };
                var response = client.DescriptiveInfo(request);

                if (travelgateXMLLiveSettings.SaveRequestAndResponseInLogs)
                    Log(ILoggerStaticDataSeverity.Warning, "DescriptiveInfo Response: {0}", GeneralUtilities.GetXmlFromObject(response));

                return !ValidateResponse(response) ? null : response;
            }
            catch (Exception ex)
            {
                Log(ILoggerStaticDataSeverity.Error, string.Format("Error while getting language specific DescriptiveInfo. provider:{0}, Hotel:{1}, Language:{2}, {3}", providerCode, hotelCode, language, ex.ToString()));
                return null;
            }
        }

        /// <summary>
        /// Get DescriptiveInfoExtended
        /// </summary>
        /// <param name="providerCode">provider Code</param>
        /// <param name="hotelCode">hotel Code</param>
        /// <returns>DescriptiveInfoExtendedRS</returns>
        public DescriptiveInfoExtendedRS GetDescriptiveInfoExtended(string providerCode, string hotelCode)
        {
            try
            {
                if (string.IsNullOrEmpty(providerCode) || string.IsNullOrEmpty(hotelCode))
                    return null;
                var client = GetServiceClient(configuration.WebServiceUrl, travelgateXMLLiveSettings.RequestTimeout);
                var request = new DescriptiveInfoExtendedRQ
                {
                    providerRQ = new ProviderRQ
                    {
                        id = "1",
                        code = providerCode,
                        rqXML = XDocument.Parse(GeneralUtilities.GetXmlFromObject(new Common.DescriptiveInfoExtendedRQ
                        {
                            timeoutMilliseconds = travelgateXMLLiveSettings.RequestTimeout,
                            source = new Business.Source { languageCode = travelgateXMLLiveSettings.RequestLanguageCode },
                            Configuration = GetProviderConfigurationData(providerCode),
                            filterAuditData = new Business.FilterAuditData { registerTransactions = travelgateXMLLiveSettings.RegisterTransactions },
                            Hotel = new Business.CodeD { Code = hotelCode },
                            Filters = new Common.Filters { LanguageCodes = travelgateXMLLiveSettings.LanguageCodes }
                        })).Descendants("DescriptiveInfoExtendedRQ").FirstOrDefault()
                    },
                    timeoutMilliseconds = travelgateXMLLiveSettings.RequestTimeout,
                    version = travelgateXMLLiveSettings.RequestVersion
                };
                var response = client.DescriptiveInfoExtended(request);
                return !ValidateResponse(response) ? null : response;
            }
            catch (Exception ex)
            {
                Log(ILoggerStaticDataSeverity.Error, string.Format("Error while getting DescriptiveInfoExtended provider:{0}, Hotel:{1}, {2}", providerCode, hotelCode, ex.ToString()));
                return null;
            }
        }

        /// <summary>
        /// To get the list of hotels of a provider
        /// </summary>
        /// <param name="providerCode">provider code</param>        
        /// <returns>HotelListRS</returns>
        public HotelListRS GetHotelList(string providerCode, string continuationToken )
        {
            if (string.IsNullOrEmpty(providerCode))
                return null;
            try
            {
                Log(ILoggerStaticDataSeverity.Info, "Getting HotelList for Provider:" + providerCode + ",Started");
                var client = GetServiceClient(configuration.WebServiceUrl, travelgateXMLLiveSettings.RequestTimeout);

                var rawRequest = GeneralUtilities.GetXmlFromObject(new Business.HotelListRQ
                {
                    ContinuationToken = new Business.C_ContinuationToken() { Token = continuationToken,  expectedRange = "100000"},
                    timeoutMilliseconds = travelgateXMLLiveSettings.RequestTimeout,
                    source = new Business.Source { languageCode = travelgateXMLLiveSettings.RequestLanguageCode },
                    Configuration = GetProviderConfigurationData(providerCode),
                    filterAuditData = new Business.FilterAuditData { registerTransactions = travelgateXMLLiveSettings.RegisterTransactions },
                });
                if (travelgateXMLLiveSettings.SaveRequestAndResponseInLogs)
                    Log(ILoggerStaticDataSeverity.Warning, "HotelList request:" + rawRequest);

                var request = new HotelListRQ
                {
                    providerRQ = new ProviderRQ
                    {
                        id = "1",
                        code = providerCode,
                        rqXML = XDocument.Parse(rawRequest).Descendants("HotelListRQ").FirstOrDefault()
                    },
                    timeoutMilliseconds = travelgateXMLLiveSettings.RequestTimeout,
                    version = travelgateXMLLiveSettings.RequestVersion
                };

                System.Net.ServicePointManager.DefaultConnectionLimit = 10000;

                var response = client.HotelList(request);
                Log(ILoggerStaticDataSeverity.Info, "Getting HotelList for Provider:" + providerCode + ",Finished");

                if (travelgateXMLLiveSettings.SaveRequestAndResponseInLogs)
                    Log(ILoggerStaticDataSeverity.Info, "HotelList response:  " + GeneralUtilities.Serialize(response));

                return !ValidateResponse(response) ? null : response;
            }
            catch (Exception ex)
            {
                Log(ILoggerStaticDataSeverity.Error, string.Format("Error while getting HotelList for Provider:{0}, {1}", providerCode, ex.ToString()));
                return null;
            }
        }

        /// <summary>
        /// Get previously created place or create new.
        /// </summary>
        /// <param name="sCountryName">The country code.</param>
        /// <param name="sCountryCode">The country code.</param>
        /// <param name="sCityName">The city name.</param>
        /// <param name="sCityCode">The city code.</param>
        /// <returns>The requested place.</returns>
        private Place GetPlace(string sCountryName, string sCountryCode, string sCityName, string sCityCode)
        {
            try
            {
                if (string.IsNullOrEmpty(sCityCode))
                    return null;

                if (sCountryCode.Length > 50) sCountryCode = sCountryCode.Substring(0, 50);
                if (sCityCode.Length > 50) sCityCode = sCityCode.Substring(0, 50);

                sCityCode = sCityCode.Replace("�", "ae"); // Error in Imperatours... duplicates the places and gives an error in SQL :/
                sCityCode = sCityCode.Trim();
                sCountryCode = sCountryCode.Trim();

                lock (allPlaces)
                {
                    // Check or create country level place            
                    if (!allPlaces.ContainsKey(sCountryCode.ToUpper()))
                    {
                        var newPlace = new Place { Id = sCountryCode, Description = sCountryName };

                        allPlaces.Add(sCountryCode.ToUpper(), newPlace);

                        lock (root.Places)
                            root.Places.Add(newPlace);
                    }
                    // Check or create state level place if state is specified            
                    if (allPlaces.ContainsKey(sCityCode.ToUpper())) return allPlaces[sCityCode.ToUpper()];

                    var place = new Place { Id = sCityCode, Description = sCityName };

                    if (!allPlaces.ContainsKey(sCityCode.ToUpper()))
                        allPlaces.Add(sCityCode.ToUpper(), place);

                    if (!allPlaces[sCountryCode.ToUpper()].Places.Contains(place))
                        allPlaces[sCountryCode.ToUpper()].Places.Add(place);
                }
                return allPlaces[sCityCode.ToUpper()];
            }
            catch (Exception ex)
            {
                Log(ILoggerStaticDataSeverity.Error, string.Format("Error while getting Place:{0}", ex.ToString()));
                return null;
            }
        }

        /// <summary>
        /// Get Provider wise Configuration data
        /// </summary>
        /// <param name="providername">provider name</param>
        /// <returns>Configuration</returns>
        private Business.Configuration GetProviderConfigurationData(string providername)
        {
            try
            {
                return GeneralUtilities.Deserialize<Business.Configuration>(XDocument.Load(configuration.ProviderConfigRoot + "\\" + providername + ".xml"));
            }
            catch (Exception ex)
            {
                Log(ILoggerStaticDataSeverity.Error, string.Format("Error while getting ProviderConfigurationData:{0}", ex.ToString()));
                return null;
            }
        }

        /// <summary>
        /// Get Service Client instance based on the URL and timeout.
        /// </summary>
        /// <param name="url">URL</param>
        /// <param name="timeout">timeout</param>
        /// <returns>Travelgate ServiceHotelClient</returns>
        private ServiceHotelBatchClient GetServiceClient(string url, int timeout)
        {
            try
            {
                ServicePointManager.ServerCertificateValidationCallback = GeneralUtilities.TrustCertificatesCallback;
                var client = new ServiceHotelBatchClient(GeneralUtilities.CreateBinding(timeout), new EndpointAddress(new Uri(url)));
                if (client.ClientCredentials == null) return client;
                client.ClientCredentials.UserName.UserName = configuration.UserId;
                client.ClientCredentials.UserName.Password = configuration.Password;
                return client;
            }
            catch (Exception ex)
            {
                Log(ILoggerStaticDataSeverity.Error, string.Format("Error while getting ServiceClient:{0}", ex.ToString()));
                return null;
            }
        }

        #endregion
    }
}
