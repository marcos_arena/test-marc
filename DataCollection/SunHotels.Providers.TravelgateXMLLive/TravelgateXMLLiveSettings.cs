﻿using SunHotels.Providers.Common;
using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace SunHotels.Providers
{
    [SerializableAttribute()]
    public class TravelgateXmlLiveSettings
    {
        public int RequestTimeout { get; set; }
        public string RequestVersion { get; set; }
        [XmlElement(ElementName = "Providers")]
        public List<Provider> Providers { get; set; }
        public string RequestLanguageCode { get; set; }
        public bool RegisterTransactions { get; set; }
        [XmlElement(ElementName = "LanguageCodes")]
        public LanguageCodes LanguageCodes { get; set; }
        public int ImageHeight { get; set; }
        public int ImageWidth { get; set; }

        // CC-2955
        // For RTSXMLLive, if set to 0 it won't call the DescriptiveInfo method.
        // If empty or other value, it will do it (default behaviour)
        public string PerformDescriptiveInfoRequest { get; set; }
        public bool SaveRequestAndResponseInLogs { get; set; }
    }

    [SerializableAttribute()]
    public class Provider
    {
        [XmlElement(ElementName = "Code")]
        public string Code { get; set; }
    }
}
