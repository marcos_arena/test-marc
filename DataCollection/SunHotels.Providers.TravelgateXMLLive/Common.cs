﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace SunHotels.Providers.Common
{
    [XmlRoot(ElementName = "DescriptiveInfoExtendedRQ")]
    public class DescriptiveInfoExtendedRQ : Business.DescriptiveInfoRQ
    {
        [XmlElement(ElementName = "Filters")]
        public Filters Filters { get; set; }
    }

    [XmlRoot(ElementName = "LanguageCodes")]
    public class LanguageCodes
    {
        [XmlElement(ElementName = "LanguageCode")]
        public List<string> LanguageCode { get; set; }
    }

    [XmlRoot(ElementName = "Filters")]
    public class Filters
    {
        [XmlElement(ElementName = "LanguageCodes")]
        public LanguageCodes LanguageCodes { get; set; }
    }
}
