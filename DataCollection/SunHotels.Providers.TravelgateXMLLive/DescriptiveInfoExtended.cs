﻿using System.Collections.Generic;

namespace SunHotels.Providers.Extended
{
    public class DescriptiveInfoExtendedRS
    {
        public Hotel hotel { get; set; }
    }

    public class DestinoNivelMinimo
    {
        public string codigo { get; set; }
        public string nombre { get; set; }
        public List<object> destinoHijo { get; set; }
    }

    public class DestinoGeograficoMinimo
    {
        public string codigo { get; set; }
        public string nombre { get; set; }
        public bool disponibilidad { get; set; }
        public List<object> destinoHijo { get; set; }
    }

    public class Text
    {
        public string value { get; set; }
    }

    public class PropertyType
    {
        public string code { get; set; }
        public List<Text> text { get; set; }
    }

    public class HotelAttributeContextText
    {
        public string languageCode { get; set; }
        public string name { get; set; }
        public string description { get; set; }
    }

    public class ContextItem
    {
        public string id { get; set; }
        public string parentRefId { get; set; }
        public string Schema { get; set; }
        public List<HotelAttributeContextText> text { get; set; }
    }

    public class Context
    {
        public List<ContextItem> contextItem { get; set; }
    }

    public class Value
    {
        public string value { get; set; }
    }

    public class HotelAttributeDataText
    {
        public string languageCode { get; set; }
        public string name { get; set; }
        public string description { get; set; }
    }

    public class Data
    {
        public string id { get; set; }
        public string type { get; set; }
        public List<Value> value { get; set; }
        public List<HotelAttributeDataText> text { get; set; }
    }

    public class HotelAttribute
    {
        public List<Context> context { get; set; }
        public Data data { get; set; }
    }

    public class HotelDescriptionContextText
    {
        public string languageCode { get; set; }
        public string name { get; set; }
        public string description { get; set; }
    }

    public class HotelDescriptionContextItem
    {
        public string id { get; set; }
        public string parentRefId { get; set; }
        public string Schema { get; set; }
        public List<HotelDescriptionContextText> text { get; set; }
    }

    public class HotelDescriptionContext
    {
        public List<HotelDescriptionContextItem> contextItem { get; set; }
    }

    public class HotelDescriptionText
    {
        public string languageCode { get; set; }
        public string value { get; set; }
    }

    public class HotelDescription
    {
        public List<HotelDescriptionContext> context { get; set; }
        public List<HotelDescriptionText> text { get; set; }
    }

    public class Description
    {
        public List<HotelDescription> description { get; set; }
    }

    public class HotelDescriptions
    {
        public Description description { get; set; }
    }

    public class RoomTypeText
    {
        public string languageCode { get; set; }
        public string value { get; set; }
    }

    public class Name
    {
        public List<RoomTypeText> text { get; set; }
    }

    public class RoomTypeContextItemText
    {
        public string languageCode { get; set; }
        public string name { get; set; }
        public string description { get; set; }
    }

    public class RoomTypeContextItem
    {
        public string id { get; set; }
        public string parentRefId { get; set; }
        public string Schema { get; set; }
        public List<RoomTypeContextItemText> text { get; set; }
    }

    public class RoomTypeContext
    {
        public List<RoomTypeContextItem> contextItem { get; set; }
    }

    public class RoomTypeDescriptionText
    {
        public string languageCode { get; set; }
        public string value { get; set; }
    }

    public class RoomTypeDescription
    {
        public List<RoomTypeContext> context { get; set; }
        public List<RoomTypeDescriptionText> text { get; set; }
    }

    public class Descriptions
    {
        public List<RoomTypeDescription> description { get; set; }
    }

    public class ValuebleAttributeContextItemText
    {
        public string languageCode { get; set; }
        public string name { get; set; }
        public string description { get; set; }
    }

    public class ValuebleAttributeContextItem
    {
        public string id { get; set; }
        public string parentRefId { get; set; }
        public string Schema { get; set; }
        public List<ValuebleAttributeContextItemText> text { get; set; }
    }

    public class ValuebleAttributeContext
    {
        public List<ValuebleAttributeContextItem> contextItem { get; set; }
    }

    public class ValubleAtributeDataValue
    {
        public string value { get; set; }
    }

    public class ValubleAtributeDataText
    {
        public string languageCode { get; set; }
        public string name { get; set; }
        public string description { get; set; }
    }

    public class ValuebleAttributeData
    {
        public string code { get; set; }
        public string id { get; set; }
        public string type { get; set; }
        public List<ValubleAtributeDataValue> value { get; set; }
        public List<ValubleAtributeDataText> text { get; set; }
    }

    public class ValuableAttribute
    {
        public List<ValuebleAttributeContext> context { get; set; }
        public ValuebleAttributeData data { get; set; }
    }

    public class RoomType
    {
        public string code { get; set; }
        public string typeId { get; set; }
        public Name name { get; set; }
        public Descriptions descriptions { get; set; }
        public List<ValuableAttribute> valuableAttribute { get; set; }
    }

    public class RoomTypes
    {
        public List<RoomType> roomType { get; set; }
    }

    public class MediumContextItemText
    {
        public string languageCode { get; set; }
        public string name { get; set; }
        public string description { get; set; }
    }

    public class MediumContextItem
    {
        public string code { get; set; }
        public string id { get; set; }
        public string parentRefId { get; set; }
        public string Schema { get; set; }
        public List<MediumContextItemText> text { get; set; }
    }

    public class MediumContext
    {
        public List<MediumContextItem> contextItem { get; set; }
    }

    public class MediumValuebleAttributeContextItemText
    {
        public string languageCode { get; set; }
        public string name { get; set; }
        public string description { get; set; }
    }

    public class MediumValuebleAttributeContextItem
    {
        public string id { get; set; }
        public string parentRefId { get; set; }
        public string Schema { get; set; }
        public List<MediumValuebleAttributeContextItemText> text { get; set; }
    }

    public class MediumValuebleAttributeContext
    {
        public List<MediumValuebleAttributeContextItem> contextItem { get; set; }
    }

    public class MediumValuableAttributeDataValue
    {
        public string value { get; set; }
    }

    public class MediumValuableAttributeDataText
    {
        public string languageCode { get; set; }
        public string name { get; set; }
        public string description { get; set; }
    }

    public class MediumValuableAttributeData
    {
        public string id { get; set; }
        public string type { get; set; }
        public List<MediumValuableAttributeDataValue> value { get; set; }
        public List<MediumValuableAttributeDataText> text { get; set; }
    }

    public class MediumValuableAttribute
    {
        public List<MediumValuebleAttributeContext> context { get; set; }
        public MediumValuableAttributeData data { get; set; }
    }

    public class MediumDescriptionContextItemText
    {
        public string languageCode { get; set; }
        public string name { get; set; }
        public string description { get; set; }
    }

    public class MediumDescriptionContextItem
    {
        public string id { get; set; }
        public string parentRefId { get; set; }
        public string Schema { get; set; }
        public List<MediumDescriptionContextItemText> text { get; set; }
    }

    public class MediumDescriptionContext
    {
        public List<MediumDescriptionContextItem> contextItem { get; set; }
    }

    public class MediumDescriptionText
    {
        public string languageCode { get; set; }
        public string value { get; set; }
    }

    public class MediumDescription
    {
        public List<MediumDescriptionContext> context { get; set; }
        public List<MediumDescriptionText> text { get; set; }
    }

    public class Photo
    {
        public string witdh { get; set; }
        public string height { get; set; }
        public bool thumbnail { get; set; }
        public string url { get; set; }
    }

    public class Photos
    {
        public List<Photo> photo { get; set; }
    }

    public class Medium
    {
        public string id { get; set; }
        public List<MediumContext> context { get; set; }
        public List<MediumValuableAttribute> valuableAttribute { get; set; }
        public MediumDescription description { get; set; }
        public Photos photos { get; set; }
    }

    public class Medias
    {
        public List<Medium> media { get; set; }
    }

    public class Hotel
    {
        public string codigo { get; set; }
        public List<object> codigosAlternativos { get; set; }
        public string nombre { get; set; }
        public string direccion { get; set; }
        public string poblacion { get; set; }
        public string codigoPostal { get; set; }
        public string codigoPaisISO3166_1_alfa_2 { get; set; }
        public DestinoNivelMinimo destinoNivelMinimo { get; set; }
        public DestinoGeograficoMinimo destinoGeograficoMinimo { get; set; }
        public string latitud { get; set; }
        public string longitud { get; set; }
        public string codigoCategoria { get; set; }
        public string numberOfRooms { get; set; }
        public PropertyType propertyType { get; set; }
        public List<HotelAttribute> hotelAttributes { get; set; }
        public HotelDescriptions hotelDescriptions { get; set; }
        public RoomTypes roomTypes { get; set; }
        public Medias medias { get; set; }
    }   
}