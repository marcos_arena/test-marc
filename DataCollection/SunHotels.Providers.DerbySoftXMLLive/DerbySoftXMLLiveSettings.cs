﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace SunHotels.Providers
{
    [SerializableAttribute()]
    public class DerbySoftXmlLiveSettings
    {
        public int RequestTimeout { get; set; }
        public string RequestVersion { get; set; }
        public string Method { get; set; }
        public string ProtocolVersion { get; set; }
        public string RequestContentType { get; set; }
        public string RequestLanguageCode { get; set; }        
        public int LastRunInDays { get; set; }
        public int ImageHeight { get; set; }
        public int ImageWidth { get; set; }
        public int MaxHotelPerRequest { get; set; }
        public bool SaveRequestAndResponseInLogs { get; set; }

        [XmlArray("Suppliers")] 
        [XmlArrayItem("Code")]
        public string[] Suppliers { get; set; }

    }
    
}
