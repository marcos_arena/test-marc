using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using SunHotels.XML;
using SunHotels.XML.Data;
using SunHotels.Providers.ProviderCommon.DataLoader;
using System.Text.RegularExpressions;
using System.Xml.Linq;
using System.Globalization;
using SunHotels.Providers.Business.HotelContentChangeRs;
using SunHotels.Providers.Business.HotelDescriptiveInfoRs;
using Hotel = SunHotels.XML.Data.Hotel;
using System.Threading.Tasks;

namespace SunHotels.Providers
{
    public class DerbySoftXMLLive : XMLProvider, IStaticDataLogger
    {
        #region Global Variable Declaration

        // Create dictionaries for fast search
        Dictionary<string, Place> allPlaces = new Dictionary<string, Place>();
        Dictionary<string, Hotel> allHotels = new Dictionary<string, Hotel>();
        DerbySoftXmlLiveSettings derbySoftXMLLiveSettings = new DerbySoftXmlLiveSettings();

        #endregion

        #region XMLProvider Implementation Methods

        /// <summary>
        /// getProviderDefinition
        /// </summary>
        /// <param name="root">Root</param>
        protected override void getProviderDefinition(Root root)
        {
            root.ProviderDefinition.Name = configuration.ProviderName;
            root.ProviderDefinition.Currency = configuration.Currency;
        }

        /// <summary>
        /// Update Configuration
        /// </summary>
        /// <param name="configuration">Configuration</param>
        protected override void UpdateConfiguration(Sunhotels.Export.Configuration configuration)
        {
            configuration.WriteHotelsWithoutRooms = true;
            configuration.WriteRoomsWithoutAvailability = true;
        }

        /// <summary>
        /// Entry point for class.
        /// </summary>
        /// <param name="root">Root for Sunhotels XML structure.</param>
        protected override void getData(Root root)
        {
            try
            {
                this.root = root;
                // ------------------------------------------------------------------------------------------------
                // Create log stream
                // ------------------------------------------------------------------------------------------------
                try
                {
                    Directory.CreateDirectory(configuration.LogOutput);
                }
                catch (Exception ex)
                {
                    throw new ApplicationException("Unable to create DerbySoftXMLLive log file", ex);
                }
                Log(ILoggerStaticDataSeverity.Info, "Starting process");
                // Get all static data 
                GetStaticData();
                Log(ILoggerStaticDataSeverity.Info, "Processing all hotels");
                foreach (var item in allHotels)
                {
                    allPlaces[item.Value.PlaceId].Hotels.Add(item.Value.Id, item.Value);
                }
                Log(ILoggerStaticDataSeverity.Info, "Process finished");
            }
            catch (Exception ex)
            {
                Log(ILoggerStaticDataSeverity.Info, "Unhanded exception [{0}] ", ex.ToString());
                throw;
            }
        }

        /// <summary>
        /// Get Global Feature mapping for the provider
        /// </summary>
        /// <param name="root">Root</param>
        protected override void getFeatureTypes(Root root)
        {
            try
            {
                //Get SH features mapped with the provider
                if (DataMapper != null && DataMapper.SunHotelsFeatures != null)
                    DataMapper.SunHotelsFeatures.Join(DataMapper["Feature"], o => o.Key, i => i.Key, (o, i) => o)
                        .All(f =>
                        {
                            root.Features.Add(new Feature()
                            {
                                Id = f.Key,
                                Value = f.Value,
                                Name = f.Value
                            });
                            return true;
                        });
            }
            catch (Exception ex)
            {
                Log(ILoggerStaticDataSeverity.Error, string.Format("Error while getting feature types:{0}", ex.Message));
            }
        }

        /// <summary>
        /// Log function
        /// </summary>
        /// <param name="severity">ILoggerStaticDataSeverity</param>
        /// <param name="message">message</param>
        /// <param name="args">arguments</param>
        public void Log(ILoggerStaticDataSeverity severity, string message, params object[] args)
        {
            var s = XML.HelpClasses.Log.MessageType.Informational;
            if (severity == ILoggerStaticDataSeverity.Error)
                s = XML.HelpClasses.Log.MessageType.Error;
            if (severity == ILoggerStaticDataSeverity.Warning)
                s = XML.HelpClasses.Log.MessageType.Warning;
            LogEntry(s, message, args);
        }

        #endregion

        #region Private void

        /// <summary>
        /// Get all static data from text files and bind with Dictionary objects.
        /// </summary>              
        private void GetStaticData()
        {
            try
            {
                var features = DataMapper.SunHotelsFeatures.Join(DataMapper["Feature"], o => o.Key, i => i.Key,
                    (o, i) => new { SHFeature = o, ProviderFeature = i })
                    .SelectMany(o => o.ProviderFeature.Value.Select(of => new { ProviderId = of, o.SHFeature }))
                    .ToDictionary(o => o.ProviderId, o => o.SHFeature);

                Log(ILoggerStaticDataSeverity.Info, "Getting Providers information started");
                GetProviders();
                Log(ILoggerStaticDataSeverity.Info, "Getting Providers information finished");

                List<string> hotelCodes = null;
                var timestamp = DateTimeToUnixTimestamp(DateTime.UtcNow.AddDays(-derbySoftXMLLiveSettings.LastRunInDays));
                var hotelContentChangeResponse = GetHotelContentChangeResponse(WithoutFractionPart(timestamp));
                if (hotelContentChangeResponse != null && hotelContentChangeResponse.HotelCodes != null
                    && hotelContentChangeResponse.HotelCodes.HotelCode != null && hotelContentChangeResponse.HotelCodes.HotelCode.Any())
                    hotelCodes = hotelContentChangeResponse.HotelCodes.HotelCode.Select(c => c).ToList();

                var chainCodes = new HashSet<string>();
                if (derbySoftXMLLiveSettings.Suppliers != null)
                {
                    derbySoftXMLLiveSettings.Suppliers.Where(s => !string.IsNullOrEmpty(s)).ToList().ForEach(s => chainCodes.Add(s.ToUpper().Trim()));
                }

                var tasks = new List<Task<bool>>();
                if (hotelCodes != null)
                {
                    Log(ILoggerStaticDataSeverity.Info, "Received " + hotelCodes.Count + " hotel codes");
                    Log(ILoggerStaticDataSeverity.Info, "Filtering for chains: " + string.Join(", ", chainCodes.ToArray()));

                    // Filter by chain code
                    hotelCodes = hotelCodes.Where(h => !string.IsNullOrEmpty(h) && chainCodes.Contains(h.Split('-').First())).ToList();

                    Log(ILoggerStaticDataSeverity.Info, hotelCodes.Count + " hotels filtered");

                    int hotelCount = hotelCodes.Count;
                    for (int i = 0; i < hotelCount; )
                    {
                        var taskHotels = hotelCodes.Skip(i).Take(derbySoftXMLLiveSettings.MaxHotelPerRequest);
                        tasks.Add(Task.Factory.StartNew(() => ProcessHotels(taskHotels, features)));
                        i = i + derbySoftXMLLiveSettings.MaxHotelPerRequest;
                    }
                    Task.WaitAll(tasks.ToArray());
                }
            }
            catch (Exception ex)
            {
                Log(ILoggerStaticDataSeverity.Error, string.Format("Error while getting provider static data.{0}", ex.Message));
            }
        }

        /// <summary>
        /// Get Providers details
        /// </summary>
        private void GetProviders()
        {
            derbySoftXMLLiveSettings = GetDerbySoftXmlLiveSettings();
        }

        /// <summary>
        /// Create Hotel Details
        /// </summary>
        /// <param name="hotelDescriptiveContent">HotelDescriptiveContent</param>
        /// <param name="features">features, Dictionary key pair value</param>
        private void CreateHotelDetails(HotelDescriptiveContent hotelDescriptiveContent, Dictionary<string, KeyValuePair<string, string>> features)
        {
            try
            {
                if (hotelDescriptiveContent == null || string.IsNullOrEmpty(hotelDescriptiveContent.HotelCode)
                    || hotelDescriptiveContent.ContactInfos == null || hotelDescriptiveContent.ContactInfos.ContactInfo == null
                    || hotelDescriptiveContent.ContactInfos.ContactInfo.Addresses == null
                    || hotelDescriptiveContent.ContactInfos.ContactInfo.Addresses.Address == null
                    || hotelDescriptiveContent.ContactInfos.ContactInfo.Addresses.Address.CountryName == null)
                    return;
                Log(ILoggerStaticDataSeverity.Info, string.Format("Create hotels details started for hotel: {0}", hotelDescriptiveContent.HotelCode));
                var address = hotelDescriptiveContent.ContactInfos.ContactInfo.Addresses.Address;
                var place = GetPlace(address.CountryName.Text, address.CountryName.Code, address.StateProv, address.CityName);
                List<Hotel.Image> hotelImages = null;
                if (hotelDescriptiveContent.MultiMediaDescriptions != null &&
                    hotelDescriptiveContent.MultiMediaDescriptions.MultiMediaDescription != null &&
                    hotelDescriptiveContent.MultiMediaDescriptions.MultiMediaDescription.ImageItems != null &&
                    hotelDescriptiveContent.MultiMediaDescriptions.MultiMediaDescription.ImageItems.ImageItem != null)
                    hotelImages = GetHotelImages(hotelDescriptiveContent.HotelCode, hotelDescriptiveContent.MultiMediaDescriptions.MultiMediaDescription.ImageItems.ImageItem);

                var newHotel = CreateHotel(hotelDescriptiveContent, features, place, hotelImages);
                if (newHotel != null)
                {
                    if (!allHotels.ContainsKey(hotelDescriptiveContent.HotelCode))
                    {
                        lock (allHotels)
                        {
                            allHotels.Add(newHotel.Id, newHotel);
                        }
                    }
                    else
                        Log(ILoggerStaticDataSeverity.Info, "Hotel Code: {0} already exist in the hotel collection", newHotel.Id);
                }
                Log(ILoggerStaticDataSeverity.Info, string.Format("Create hotels details finished for hotel: {0}", hotelDescriptiveContent.HotelCode));
            }
            catch (Exception ex)
            {
                Log(ILoggerStaticDataSeverity.Error, string.Format("Error while create Hotel:{0}, {1}", hotelDescriptiveContent != null ? hotelDescriptiveContent.HotelCode : string.Empty, ex.Message));
            }
        }

        #endregion

        #region Private Get Type

        /// <summary>
        /// Process Hotels
        /// </summary>
        /// <param name="hotelCodes">IEnumerable of string</param>
        /// <param name="features">features, Dictionary key pair value</param>
        /// <returns>boolean</returns>
        private bool ProcessHotels(IEnumerable<string> hotelCodes, Dictionary<string, KeyValuePair<string, string>> features)
        {
            try
            {
                var hotelDescriptiveInfoResponse = GetHotelDescriptiveInfoResponse(hotelCodes);
                if (hotelDescriptiveInfoResponse == null ||
                    hotelDescriptiveInfoResponse.HotelDescriptiveContents == null ||
                    hotelDescriptiveInfoResponse.HotelDescriptiveContents.HotelDescriptiveContent == null ||
                    !hotelDescriptiveInfoResponse.HotelDescriptiveContents.HotelDescriptiveContent.Any())
                    return true;
                var hotelinfo = hotelDescriptiveInfoResponse.HotelDescriptiveContents.HotelDescriptiveContent;
                hotelinfo.AsParallel().Select(c =>
                {
                    CreateHotelDetails(c, features);
                    return true;
                }).ToArray();

                return true;
            }
            catch (Exception ex)
            {
                Log(ILoggerStaticDataSeverity.Error, string.Format("Error while process Hotels, {0}", ex.Message));
                return false;
            }
        }

        /// <summary>
        /// Create Hotel
        /// </summary>
        /// <param name="hotel">HotelDescriptiveContent</param>
        /// <param name="features">features, Dictionary key pair value</param>
        /// <param name="place">Place</param>
        /// <param name="hotelImages">List Hotel Image</param>
        /// <returns>Sunhotel Hotel</returns>
        private Hotel CreateHotel(HotelDescriptiveContent hotel,
            Dictionary<string, KeyValuePair<string, string>> features, Place place,
            List<Hotel.Image> hotelImages)
        {
            try
            {
                Log(ILoggerStaticDataSeverity.Info, string.Format("Creating hotel: {0}", hotel.HotelCode));
                string country = string.Empty, description = string.Empty, street = string.Empty,
                    postalCode = string.Empty, cityName = string.Empty, stateProv = string.Empty,
                    latitude = string.Empty, longitude = string.Empty;

                var hotelName = !string.IsNullOrEmpty(hotel.HotelName) ? hotel.HotelName : string.Empty;
                if (hotel.ContactInfos != null && hotel.ContactInfos.ContactInfo != null &&
                    hotel.ContactInfos.ContactInfo.Addresses != null && hotel.ContactInfos.ContactInfo.Addresses.Address != null)
                {
                    var address = hotel.ContactInfos.ContactInfo.Addresses.Address;
                    if (address.CountryName != null)
                        country = !string.IsNullOrEmpty(address.CountryName.Text) ? address.CountryName.Text : address.CountryName.Code;
                    street = !string.IsNullOrEmpty(address.StreetNmbr) ? address.StreetNmbr : string.Empty;
                    postalCode = !string.IsNullOrEmpty(address.PostalCode) ? address.PostalCode : string.Empty;
                    cityName = !string.IsNullOrEmpty(address.CityName) ? address.CityName : string.Empty;
                    stateProv = !string.IsNullOrEmpty(address.StateProv) ? address.StateProv : string.Empty;
                }
                if (hotel.HotelInfo != null)
                {
                    description = hotel.HotelInfo.Descriptions != null && !string.IsNullOrEmpty(hotel.HotelInfo.Descriptions.DescriptiveText)
                                  ? hotel.HotelInfo.Descriptions.DescriptiveText : string.Empty;
                    if (hotel.HotelInfo.Position != null)
                    {
                        latitude = !string.IsNullOrEmpty(hotel.HotelInfo.Position.Latitude) ? hotel.HotelInfo.Position.Latitude : string.Empty;
                        longitude = !string.IsNullOrEmpty(hotel.HotelInfo.Position.Longitude) ? hotel.HotelInfo.Position.Longitude : string.Empty;
                    }

                }
                var translationsHeadline = new TranslationsHeadline { country = country, lang = derbySoftXMLLiveSettings.RequestLanguageCode, Value = description };
                var translationsDescription = new TranslationsDescription { country = country, lang = derbySoftXMLLiveSettings.RequestLanguageCode, Value = description };
                return new Hotel
                {
                    Id = hotel.HotelCode,
                    PlaceId = place.Id,
                    Name = hotelName,
                    AccomodationType = AccomodationType.Hotel,
                    Headline = hotelName,
                    Description = description,
                    Adress_Street1 = street,
                    Adress_Zipcode = postalCode,
                    Adress_City = cityName,
                    Adress_State = stateProv,
                    Adress_Country = country,
                    Position_Latitude = latitude,
                    Position_Longitude = longitude,
                    Phone = string.Empty,
                    Fax = string.Empty,
                    Email = string.Empty,
                    Classification = string.Empty,
                    BestBuy = false,
                    Features = GetHotelFeatures(hotel, features),
                    AdultOnly = false,
                    BlockInfant = false,
                    Images = hotelImages,
                    Identifiers = new[] { new Identifier { Type = "provider", Value = "DerbySoftXMLLive" } }.ToList(),
                    Translations = new Translations { headline = new List<TranslationsHeadline> { translationsHeadline }, description = new List<TranslationsDescription> { translationsDescription } }
                };
            }
            catch (Exception ex)
            {
                Log(ILoggerStaticDataSeverity.Error, string.Format("Error while processing hotel response, Hotel:{0},{1}", hotel.HotelCode, ex.Message));
                return null;
            }
        }

        /// <summary>
        /// Get Hotel Images
        /// </summary>
        /// <param name="hotelCode">hotelCode</param>
        /// <param name="imageItems">ImageItem</param>
        /// <returns>List of Hotel.Image</returns>
        private List<Hotel.Image> GetHotelImages(string hotelCode, List<ImageItem> imageItems)
        {
            var hotelImages = new List<Hotel.Image>();
            try
            {
                if (imageItems == null || !imageItems.Any())
                    return null;
                Log(ILoggerStaticDataSeverity.Info, string.Format("Prepare hotel images started, Hotel:{0}", hotelCode));
                imageItems.Where(c => c.ImageFormat != null && !string.IsNullOrEmpty(c.ImageFormat.URL)).AsParallel().All(c =>
                {
                    lock (hotelImages)
                    {
                        hotelImages.Add(new Hotel.Image
                        {
                            Id = c.Description != null && !string.IsNullOrEmpty(c.Description.Caption) ? c.Description.Caption : string.Empty,
                            ImageVariants = new List<Hotel.ImageVariant>() { 
                            new Hotel.ImageVariant() { URL = c.ImageFormat.URL, Height=derbySoftXMLLiveSettings.ImageHeight, Width=derbySoftXMLLiveSettings.ImageWidth } }
                        });
                    }
                    return true;
                });
            }
            catch (Exception ex)
            {
                Log(ILoggerStaticDataSeverity.Error, string.Format("Error while prepare hotel images, Hotel:{0},{1}", hotelCode, ex.Message));
            }
            Log(ILoggerStaticDataSeverity.Info, string.Format("Prepare hotel images finished, Hotel:{0}", hotelCode));
            return hotelImages;
        }

        /// <summary>
        /// Get Hotel Features
        /// </summary>
        /// <param name="hotel">HotelDescriptiveContent</param>
        /// <param name="features">Dictionary of KeyValuePair, features</param>
        /// <returns>List of Feature</returns>
        private List<Feature> GetHotelFeatures(HotelDescriptiveContent hotel, Dictionary<string, KeyValuePair<string, string>> features)
        {
            try
            {
                var featureCollection = new List<Feature>();
                if (features == null || hotel == null || hotel.HotelInfo == null || hotel.HotelInfo.Services == null
                    || hotel.HotelInfo.Services.Service == null || !hotel.HotelInfo.Services.Service.Any())
                    return featureCollection;
                hotel.HotelInfo.Services.Service.AsParallel().Select(c =>
                {
                    string key = c.Code;
                    if (string.IsNullOrEmpty(key))
                        return true;
                    if (!features.ContainsKey(key)) return true;
                    var shfeature = features[key];
                    lock (featureCollection)
                    {
                        if (featureCollection.All(f => f.Id != shfeature.Key))
                        {
                            featureCollection.Add(new Feature()
                            {
                                Id = shfeature.Key,
                                Name = shfeature.Value,
                                Value = shfeature.Value
                            });
                        }
                    }
                    return true;
                }).ToList();
                return featureCollection;
            }
            catch (Exception ex)
            {
                Log(ILoggerStaticDataSeverity.Error,
                    string.Format("Error while getting hotel Features, Hotel:{0}, {1}", hotel != null ? hotel.HotelCode : string.Empty, ex.Message));
                return null;
            }
        }

        /// <summary>
        /// Get DerbySoftXMLLiveSettings
        /// </summary>
        /// <param name="fileName">fileName</param>
        /// <returns></returns>
        private DerbySoftXmlLiveSettings GetDerbySoftXmlLiveSettings(string fileName = "DerbySoftXMLLiveSettings")
        {
            try
            {
                return GeneralUtilities.Deserialize<DerbySoftXmlLiveSettings>(XDocument.Load(configuration.ProviderConfigRoot + "\\" + fileName + ".xml"));
            }
            catch (Exception ex)
            {
                Log(ILoggerStaticDataSeverity.Error, string.Format("Error while getting DerbySoftXmlLiveSettings:{0}", ex.Message));
                return null;
            }
        }

        /// <summary>
        /// Get Response
        /// </summary>
        /// <param name="request">request</param>
        /// <returns>string</returns>
        private string GetResponse(object request)
        {
            try
            {
                var api = new RestApiUtils
                {
                    RequestUrl = configuration.WebServiceUrl,
                    ProtocolVersion = "",
                    Timeout = derbySoftXMLLiveSettings.RequestTimeout,
                    HttpHeader = new Dictionary<string, string>(),
                    MethodType = derbySoftXMLLiveSettings.Method,
                    RequestContentType = derbySoftXMLLiveSettings.RequestContentType,
                    PostParameter = GeneralUtilities.GetXmlFromObject(request)
                };
                api.PostParameter = Regex.Replace(api.PostParameter, "utf-16", "utf-8");
                var xmlresponse = (string)api.Post();
                return xmlresponse;
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Get HotelDescriptiveInfoResponse
        /// </summary>
        /// <param name="hotelCodes">hotelCodes</param>
        /// <returns>HotelDescriptiveInfoResponse</returns>
        public HotelDescriptiveInfoResponse GetHotelDescriptiveInfoResponse(IEnumerable<string> hotelCodes)
        {
            var hotelCodesArray = hotelCodes as string[] ?? hotelCodes.ToArray();
            try
            {
                Log(ILoggerStaticDataSeverity.Info, string.Format("Getting Hotel description Started,Hotel Code:{0}", string.Join(",", hotelCodesArray)));
                var request = new Business.HotelDescriptiveInfoRq.HotelDescriptiveInfoRequest
                {
                    UserName = configuration.UserId,
                    Password = configuration.Password,
                    Token = GeneralUtilities.GenerateToken(),
                    HotelDescriptiveInfos = new Business.HotelDescriptiveInfoRq.HotelDescriptiveInfos
                    {
                        HotelDescriptiveInfo = hotelCodesArray.Select(c => new Business.HotelDescriptiveInfoRq.HotelDescriptiveInfo
                        {
                            HotelCode = c,
                            HotelCodeContext = string.Empty
                        }).ToList()
                    }
                };
                var xmlrequest = GeneralUtilities.Serialize(request);
                var xmlresponse = GetResponse(request);

                if (derbySoftXMLLiveSettings.SaveRequestAndResponseInLogs)
                {
                    Log(ILoggerStaticDataSeverity.Error, string.Format("URL: {0}", configuration.WebServiceUrl));
                    Log(ILoggerStaticDataSeverity.Error, string.Format("Request to DerbySoft: {0}", xmlrequest));
                    Log(ILoggerStaticDataSeverity.Error, string.Format("Request from DerbySoft: {0}", xmlresponse));
                }

                var response = GeneralUtilities.DeserializeXml<HotelDescriptiveInfoResponse>(xmlresponse);
                Log(ILoggerStaticDataSeverity.Info, string.Format("Getting Hotel Description Finished,Hotel Code:{0}", string.Join(",", hotelCodesArray)));
                return response.Status != "Successful" ? null : response;
            }
            catch (Exception ex)
            {
                Log(ILoggerStaticDataSeverity.Error, string.Format("Error while getting Hotel Description,Hotel Code:{0},{1}", string.Join(",", hotelCodesArray), ex.ToString()));
                return null;
            }
        }

        /// <summary>
        /// Get HotelContentChangeResponse
        /// </summary>
        /// <param name="lastModifyDateTime">lastModifyDateTime</param>
        /// <returns>HotelContentChangeResponse</returns>
        public HotelContentChangeResponse GetHotelContentChangeResponse(string lastModifyDateTime)
        {
            try
            {
                Log(ILoggerStaticDataSeverity.Info, string.Format("Getting Hotel Content Change Started,lastModifyDateTime:{0}", lastModifyDateTime));
                var request = new Business.HotelContentChangeRq.HotelContentChangeRequest
                {
                    UserName = configuration.UserId,
                    Password = configuration.Password,
                    Token = GeneralUtilities.GenerateToken(),
                    Criteria = new Business.HotelContentChangeRq.Criteria
                    {
                        Criterion = new Business.HotelContentChangeRq.Criterion
                        {
                            LastModifyDateTime = lastModifyDateTime
                        }
                    }
                };                
                
                var xmlrequest = GeneralUtilities.Serialize(request);
                var xmlresponse = GetResponse(request);

                if (derbySoftXMLLiveSettings.SaveRequestAndResponseInLogs)
                {
                    Log(ILoggerStaticDataSeverity.Error, string.Format("URL: {0}", configuration.WebServiceUrl));
                    Log(ILoggerStaticDataSeverity.Error, string.Format("Request to DerbySoft: {0}", xmlrequest));
                    Log(ILoggerStaticDataSeverity.Error, string.Format("Request from DerbySoft: {0}", xmlresponse));
                }

                var response = GeneralUtilities.DeserializeXml<HotelContentChangeResponse>(xmlresponse);
                Log(ILoggerStaticDataSeverity.Info, string.Format("Getting Hotel Content Change Finished,lastModifyDateTime:{0}", lastModifyDateTime));
                return response.Status != "Successful" ? null : response;
            }
            catch (Exception ex)
            {
                Log(ILoggerStaticDataSeverity.Error, string.Format("Error while getting Hotel Content Change,lastModifyDateTime:{0},{1}", lastModifyDateTime, ex.Message));
                return null;
            }
        }

        /// <summary>
        /// Get previously created place or create new.
        /// </summary>
        /// <param name="countryName">countryName</param>
        /// <param name="countryCode">countryCode</param>
        /// <param name="state">state</param>
        /// <param name="city">city</param>
        /// <returns>Place</returns>
        private Place GetPlace(string countryName, string countryCode, string state, string city)
        {
            try
            {
                if (string.IsNullOrEmpty(city))
                    return null;
                countryName = string.IsNullOrEmpty(countryName) ? countryCode : countryName;
                Log(ILoggerStaticDataSeverity.Info, string.Format("Get places started for city: {0}, in country: {1}", city, countryName));

                lock (allPlaces)
                {
                    // Check or create country level place            
                    countryCode = countryCode.ToLower();
                    if (!allPlaces.ContainsKey(countryCode))
                    {
                        var newPlace = new Place { Id = countryCode, Description = countryName.Trim().ToLower() };
                        allPlaces.Add(countryCode, newPlace);
                        lock (root.Places)
                        {
                            if (!root.Places.Contains(newPlace))
                                root.Places.Add(newPlace);
                        }
                    }

                    // Check or create state level place if state is specified   
                    bool validState = false;
                    if (!string.IsNullOrEmpty(state))
                    {
                        validState = true;
                        state = state.Trim().ToLower();
                        if (!allPlaces.ContainsKey(state))
                        {
                            var splace = new Place { Id = state, Description = state };
                            allPlaces.Add(state, splace);
                            if (!allPlaces[countryCode].Places.Contains(splace))
                                allPlaces[countryCode].Places.Add(splace);
                        }
                    }

                    // Check or create city level place if city is specified             
                    city = city.Trim().ToLower();
                    if (allPlaces.ContainsKey(city))
                        return allPlaces[city];

                    var place = new Place { Id = city, Description = city };
                    if (!allPlaces.ContainsKey(city))
                        allPlaces.Add(city, place);

                    var key = validState ? state : countryCode;
                    if (!allPlaces[key].Places.Contains(place))
                        allPlaces[key].Places.Add(place);
                }
                Log(ILoggerStaticDataSeverity.Info, string.Format("Get places finished for city: {0}, in country: {1}", city, countryName));
                return allPlaces[city];
            }
            catch (Exception ex)
            {
                Log(ILoggerStaticDataSeverity.Error, string.Format("Error while getting Place:{0}", ex.Message));
                return null;
            }
        }

        #endregion

        #region Private Static

        /// <summary>
        /// DateTime To Unix Time stamp
        /// </summary>
        /// <param name="dateTime">DateTime</param>
        /// <returns>double</returns>
        public static double DateTimeToUnixTimestamp(DateTime dateTime)
        {
            return Math.Round((TimeZoneInfo.ConvertTimeToUtc(dateTime) - new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc)).TotalSeconds, 0);
        }

        /// <summary>
        /// Without Fraction Part from double
        /// </summary>
        /// <param name="instance">double</param>
        /// <returns>string</returns>
        public static string WithoutFractionPart(double instance)
        {
            var result = string.Empty;
            var ic = CultureInfo.InvariantCulture;
            var splits = instance.ToString(ic).Split(new[] { ic.NumberFormat.NumberDecimalSeparator }, StringSplitOptions.RemoveEmptyEntries);
            if (splits.Any())
                result = splits[0];
            return result;
        }

        #endregion
    }
}
