﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace SunHotels.Providers.Business.HotelContentChangeRs
{
    [XmlRoot(ElementName = "HotelCodes", Namespace = "http://www.derbysoft.com/doorway")]
    public class HotelCodes
    {
        [XmlElement(ElementName = "HotelCode", Namespace = "http://www.derbysoft.com/doorway")]
        public List<string> HotelCode { get; set; }
    }

    [XmlRoot(ElementName = "HotelContentChangeResponse", Namespace = "http://www.derbysoft.com/doorway")]
    public class HotelContentChangeResponse
    {
        [XmlElement(ElementName = "HotelCodes", Namespace = "http://www.derbysoft.com/doorway")]
        public HotelCodes HotelCodes { get; set; }
        [XmlAttribute(AttributeName = "Status")]
        public string Status { get; set; }
        [XmlAttribute(AttributeName = "Token")]
        public string Token { get; set; }
        [XmlAttribute(AttributeName = "xmlns")]
        public string Xmlns { get; set; }
    }
}
