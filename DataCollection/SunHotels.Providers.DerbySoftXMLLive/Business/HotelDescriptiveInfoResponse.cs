﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace SunHotels.Providers.Business.HotelDescriptiveInfoRs
{
    [XmlRoot(ElementName = "Descriptions", Namespace = "http://www.derbysoft.com/doorway")]
    public class Descriptions
    {
        [XmlElement(ElementName = "DescriptiveText", Namespace = "http://www.derbysoft.com/doorway")]
        public string DescriptiveText { get; set; }
    }

    [XmlRoot(ElementName = "Position", Namespace = "http://www.derbysoft.com/doorway")]
    public class Position
    {
        [XmlAttribute(AttributeName = "Latitude")]
        public string Latitude { get; set; }
        [XmlAttribute(AttributeName = "Longitude")]
        public string Longitude { get; set; }
    }

    [XmlRoot(ElementName = "Service", Namespace = "http://www.derbysoft.com/doorway")]
    public class Service
    {
        [XmlAttribute(AttributeName = "CodeDetail")]
        public string CodeDetail { get; set; }
        [XmlAttribute(AttributeName = "Code")]
        public string Code { get; set; }
        [XmlAttribute(AttributeName = "ProximityCode")]
        public string ProximityCode { get; set; }
        [XmlAttribute(AttributeName = "BusinessServiceCode")]
        public string BusinessServiceCode { get; set; }
    }

    [XmlRoot(ElementName = "Services", Namespace = "http://www.derbysoft.com/doorway")]
    public class Services
    {
        [XmlElement(ElementName = "Service", Namespace = "http://www.derbysoft.com/doorway")]
        public List<Service> Service { get; set; }
    }

    [XmlRoot(ElementName = "HotelInfo", Namespace = "http://www.derbysoft.com/doorway")]
    public class HotelInfo
    {
        [XmlElement(ElementName = "RelativePositions", Namespace = "http://www.derbysoft.com/doorway")]
        public string RelativePositions { get; set; }
        [XmlElement(ElementName = "Descriptions", Namespace = "http://www.derbysoft.com/doorway")]
        public Descriptions Descriptions { get; set; }
        [XmlElement(ElementName = "Position", Namespace = "http://www.derbysoft.com/doorway")]
        public Position Position { get; set; }
        [XmlElement(ElementName = "Services", Namespace = "http://www.derbysoft.com/doorway")]
        public Services Services { get; set; }
    }

    [XmlRoot(ElementName = "RoomType", Namespace = "http://www.derbysoft.com/doorway")]
    public class RoomType
    {
        [XmlAttribute(AttributeName = "RoomTypeCode")]
        public string RoomTypeCode { get; set; }
        [XmlAttribute(AttributeName = "Name")]
        public string Name { get; set; }
    }

    [XmlRoot(ElementName = "GuestRoom", Namespace = "http://www.derbysoft.com/doorway")]
    public class GuestRoom
    {
        [XmlElement(ElementName = "RoomType", Namespace = "http://www.derbysoft.com/doorway")]
        public RoomType RoomType { get; set; }
        [XmlElement(ElementName = "Amenities", Namespace = "http://www.derbysoft.com/doorway")]
        public string Amenities { get; set; }
        [XmlElement(ElementName = "Features", Namespace = "http://www.derbysoft.com/doorway")]
        public string Features { get; set; }
        [XmlElement(ElementName = "DescriptiveText", Namespace = "http://www.derbysoft.com/doorway")]
        public string DescriptiveText { get; set; }
        [XmlAttribute(AttributeName = "MaxOccupancy")]
        public string MaxOccupancy { get; set; }
    }

    [XmlRoot(ElementName = "GuestRooms", Namespace = "http://www.derbysoft.com/doorway")]
    public class GuestRooms
    {
        [XmlElement(ElementName = "GuestRoom", Namespace = "http://www.derbysoft.com/doorway")]
        public List<GuestRoom> GuestRoom { get; set; }
    }

    [XmlRoot(ElementName = "FacilityInfo", Namespace = "http://www.derbysoft.com/doorway")]
    public class FacilityInfo
    {
        [XmlElement(ElementName = "GuestRooms", Namespace = "http://www.derbysoft.com/doorway")]
        public GuestRooms GuestRooms { get; set; }
    }

    [XmlRoot(ElementName = "PolicyInfo", Namespace = "http://www.derbysoft.com/doorway")]
    public class PolicyInfo
    {
        [XmlAttribute(AttributeName = "CheckInTime")]
        public string CheckInTime { get; set; }
        [XmlAttribute(AttributeName = "CheckOutTime")]
        public string CheckOutTime { get; set; }
        [XmlAttribute(AttributeName = "AcceptedGuestType")]
        public string AcceptedGuestType { get; set; }
        [XmlAttribute(AttributeName = "KidsStayFree")]
        public string KidsStayFree { get; set; }
        [XmlAttribute(AttributeName = "MaxChildAge")]
        public string MaxChildAge { get; set; }
    }

    [XmlRoot(ElementName = "Policy", Namespace = "http://www.derbysoft.com/doorway")]
    public class Policy
    {
        [XmlElement(ElementName = "PolicyInfo", Namespace = "http://www.derbysoft.com/doorway")]
        public PolicyInfo PolicyInfo { get; set; }
    }

    [XmlRoot(ElementName = "Policies", Namespace = "http://www.derbysoft.com/doorway")]
    public class Policies
    {
        [XmlElement(ElementName = "Policy", Namespace = "http://www.derbysoft.com/doorway")]
        public Policy Policy { get; set; }
    }

    [XmlRoot(ElementName = "Recreation", Namespace = "http://www.derbysoft.com/doorway")]
    public class Recreation
    {
        [XmlAttribute(AttributeName = "CodeDetail")]
        public string CodeDetail { get; set; }
        [XmlAttribute(AttributeName = "Code")]
        public string Code { get; set; }
    }

    [XmlRoot(ElementName = "Recreations", Namespace = "http://www.derbysoft.com/doorway")]
    public class Recreations
    {
        [XmlElement(ElementName = "Recreation", Namespace = "http://www.derbysoft.com/doorway")]
        public Recreation Recreation { get; set; }
    }

    [XmlRoot(ElementName = "AreaInfo", Namespace = "http://www.derbysoft.com/doorway")]
    public class AreaInfo
    {
        [XmlElement(ElementName = "Recreations", Namespace = "http://www.derbysoft.com/doorway")]
        public Recreations Recreations { get; set; }
    }

    [XmlRoot(ElementName = "ImageFormat", Namespace = "http://www.derbysoft.com/doorway")]
    public class ImageFormat
    {
        [XmlElement(ElementName = "URL", Namespace = "http://www.derbysoft.com/doorway")]
        public string URL { get; set; }
    }

    [XmlRoot(ElementName = "Description", Namespace = "http://www.derbysoft.com/doorway")]
    public class Description
    {
        [XmlAttribute(AttributeName = "Caption")]
        public string Caption { get; set; }
    }

    [XmlRoot(ElementName = "ImageItem", Namespace = "http://www.derbysoft.com/doorway")]
    public class ImageItem
    {
        [XmlElement(ElementName = "ImageFormat", Namespace = "http://www.derbysoft.com/doorway")]
        public ImageFormat ImageFormat { get; set; }
        [XmlElement(ElementName = "Description", Namespace = "http://www.derbysoft.com/doorway")]
        public Description Description { get; set; }
        [XmlAttribute(AttributeName = "Category")]
        public string Category { get; set; }
    }

    [XmlRoot(ElementName = "ImageItems", Namespace = "http://www.derbysoft.com/doorway")]
    public class ImageItems
    {
        [XmlElement(ElementName = "ImageItem", Namespace = "http://www.derbysoft.com/doorway")]
        public List<ImageItem> ImageItem { get; set; }
    }

    [XmlRoot(ElementName = "MultiMediaDescription", Namespace = "http://www.derbysoft.com/doorway")]
    public class MultiMediaDescription
    {
        [XmlElement(ElementName = "ImageItems", Namespace = "http://www.derbysoft.com/doorway")]
        public ImageItems ImageItems { get; set; }
    }

    [XmlRoot(ElementName = "MultiMediaDescriptions", Namespace = "http://www.derbysoft.com/doorway")]
    public class MultiMediaDescriptions
    {
        [XmlElement(ElementName = "MultiMediaDescription", Namespace = "http://www.derbysoft.com/doorway")]
        public MultiMediaDescription MultiMediaDescription { get; set; }
    }

    [XmlRoot(ElementName = "CountryName", Namespace = "http://www.derbysoft.com/doorway")]
    public class CountryName
    {
        [XmlAttribute(AttributeName = "Code")]
        public string Code { get; set; }
        [XmlAttribute(AttributeName = "Text")]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "Address", Namespace = "http://www.derbysoft.com/doorway")]
    public class Address
    {
        [XmlElement(ElementName = "StreetNmbr", Namespace = "http://www.derbysoft.com/doorway")]
        public string StreetNmbr { get; set; }
        [XmlElement(ElementName = "CityName", Namespace = "http://www.derbysoft.com/doorway")]
        public string CityName { get; set; }
        [XmlElement(ElementName = "PostalCode", Namespace = "http://www.derbysoft.com/doorway")]
        public string PostalCode { get; set; }
        [XmlElement(ElementName = "StateProv", Namespace = "http://www.derbysoft.com/doorway")]
        public string StateProv { get; set; }
        [XmlElement(ElementName = "CountryName", Namespace = "http://www.derbysoft.com/doorway")]
        public CountryName CountryName { get; set; }
    }

    [XmlRoot(ElementName = "Addresses", Namespace = "http://www.derbysoft.com/doorway")]
    public class Addresses
    {
        [XmlElement(ElementName = "Address", Namespace = "http://www.derbysoft.com/doorway")]
        public Address Address { get; set; }
    }

    [XmlRoot(ElementName = "URLs", Namespace = "http://www.derbysoft.com/doorway")]
    public class URLs
    {
        [XmlElement(ElementName = "URL", Namespace = "http://www.derbysoft.com/doorway")]
        public string URL { get; set; }
    }

    [XmlRoot(ElementName = "ContactInfo", Namespace = "http://www.derbysoft.com/doorway")]
    public class ContactInfo
    {
        [XmlElement(ElementName = "Addresses", Namespace = "http://www.derbysoft.com/doorway")]
        public Addresses Addresses { get; set; }
        [XmlElement(ElementName = "URLs", Namespace = "http://www.derbysoft.com/doorway")]
        public URLs URLs { get; set; }
    }

    [XmlRoot(ElementName = "ContactInfos", Namespace = "http://www.derbysoft.com/doorway")]
    public class ContactInfos
    {
        [XmlElement(ElementName = "ContactInfo", Namespace = "http://www.derbysoft.com/doorway")]
        public ContactInfo ContactInfo { get; set; }
    }

    [XmlRoot(ElementName = "RatePlan", Namespace = "http://www.derbysoft.com/doorway")]
    public class RatePlan
    {
        [XmlAttribute(AttributeName = "RatePlanCode")]
        public string RatePlanCode { get; set; }
        [XmlAttribute(AttributeName = "RatePlanName")]
        public string RatePlanName { get; set; }
        [XmlAttribute(AttributeName = "DescriptiveText")]
        public string DescriptiveText { get; set; }
    }

    [XmlRoot(ElementName = "TPA_Extension", Namespace = "http://www.derbysoft.com/doorway")]
    public class TPA_Extension
    {
        [XmlElement(ElementName = "RatePlan", Namespace = "http://www.derbysoft.com/doorway")]
        public List<RatePlan> RatePlan { get; set; }
    }

    [XmlRoot(ElementName = "HotelDescriptiveContent", Namespace = "http://www.derbysoft.com/doorway")]
    public class HotelDescriptiveContent
    {
        [XmlElement(ElementName = "HotelInfo", Namespace = "http://www.derbysoft.com/doorway")]
        public HotelInfo HotelInfo { get; set; }
        [XmlElement(ElementName = "FacilityInfo", Namespace = "http://www.derbysoft.com/doorway")]
        public FacilityInfo FacilityInfo { get; set; }
        [XmlElement(ElementName = "Policies", Namespace = "http://www.derbysoft.com/doorway")]
        public Policies Policies { get; set; }
        [XmlElement(ElementName = "AreaInfo", Namespace = "http://www.derbysoft.com/doorway")]
        public AreaInfo AreaInfo { get; set; }
        [XmlElement(ElementName = "MultiMediaDescriptions", Namespace = "http://www.derbysoft.com/doorway")]
        public MultiMediaDescriptions MultiMediaDescriptions { get; set; }
        [XmlElement(ElementName = "ContactInfos", Namespace = "http://www.derbysoft.com/doorway")]
        public ContactInfos ContactInfos { get; set; }
        [XmlElement(ElementName = "TPA_Extension", Namespace = "http://www.derbysoft.com/doorway")]
        public TPA_Extension TPA_Extension { get; set; }
        [XmlAttribute(AttributeName = "HotelCode")]
        public string HotelCode { get; set; }
        [XmlAttribute(AttributeName = "HotelName")]
        public string HotelName { get; set; }
    }

    [XmlRoot(ElementName = "HotelDescriptiveContents", Namespace = "http://www.derbysoft.com/doorway")]
    public class HotelDescriptiveContents
    {
        [XmlElement(ElementName = "HotelDescriptiveContent", Namespace = "http://www.derbysoft.com/doorway")]
        public List<HotelDescriptiveContent> HotelDescriptiveContent { get; set; }
    }

    [XmlRoot(ElementName = "HotelDescriptiveInfoResponse", Namespace = "http://www.derbysoft.com/doorway")]
    public class HotelDescriptiveInfoResponse
    {
        [XmlElement(ElementName = "HotelDescriptiveContents", Namespace = "http://www.derbysoft.com/doorway")]
        public HotelDescriptiveContents HotelDescriptiveContents { get; set; }
        [XmlAttribute(AttributeName = "Token")]
        public string Token { get; set; }
        [XmlAttribute(AttributeName = "Status")]
        public string Status { get; set; }
        [XmlAttribute(AttributeName = "xmlns")]
        public string Xmlns { get; set; }
    }
}
