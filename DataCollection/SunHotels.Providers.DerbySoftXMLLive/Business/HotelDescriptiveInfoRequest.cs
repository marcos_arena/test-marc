﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace SunHotels.Providers.Business.HotelDescriptiveInfoRq
{
    [XmlRoot(ElementName = "HotelDescriptiveInfo")]
    public class HotelDescriptiveInfo
    {
        [XmlAttribute(AttributeName = "HotelCode")]
        public string HotelCode { get; set; }
        [XmlAttribute(AttributeName = "HotelCodeContext")]
        public string HotelCodeContext { get; set; }
    }

    [XmlRoot(ElementName = "HotelDescriptiveInfos")]
    public class HotelDescriptiveInfos
    {
        [XmlElement(ElementName = "HotelDescriptiveInfo")]
        public List<HotelDescriptiveInfo> HotelDescriptiveInfo { get; set; }
    }

    [XmlRoot(ElementName = "HotelDescriptiveInfoRequest")]
    public class HotelDescriptiveInfoRequest
    {
        [XmlElement(ElementName = "HotelDescriptiveInfos")]
        public HotelDescriptiveInfos HotelDescriptiveInfos { get; set; }
        [XmlAttribute(AttributeName = "Token")]
        public string Token { get; set; }
        [XmlAttribute(AttributeName = "UserName")]
        public string UserName { get; set; }
        [XmlAttribute(AttributeName = "Password")]
        public string Password { get; set; }
    }

}
