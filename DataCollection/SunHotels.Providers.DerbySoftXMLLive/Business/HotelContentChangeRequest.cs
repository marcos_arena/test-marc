﻿using System.Xml.Serialization;

namespace SunHotels.Providers.Business.HotelContentChangeRq
{
    [XmlRoot(ElementName = "Criterion")]
    public class Criterion
    {
        [XmlAttribute(AttributeName = "LastModifyDateTime")]
        public string LastModifyDateTime { get; set; }
    }

    [XmlRoot(ElementName = "Criteria")]
    public class Criteria
    {
        [XmlElement(ElementName = "Criterion")]
        public Criterion Criterion { get; set; }
    }

    [XmlRoot(ElementName = "HotelContentChangeRequest")]
    public class HotelContentChangeRequest
    {
        [XmlElement(ElementName = "Criteria")]
        public Criteria Criteria { get; set; }
        [XmlAttribute(AttributeName = "Token")]
        public string Token { get; set; }
        [XmlAttribute(AttributeName = "UserName")]
        public string UserName { get; set; }
        [XmlAttribute(AttributeName = "Password")]
        public string Password { get; set; }
    }
}
