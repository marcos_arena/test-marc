﻿using System;
using System.IO;
using System.Linq;
using System.Net.Security;
using System.Reflection;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace SunHotels.Providers
{
    public static class GeneralUtilities
    {
        #region Private Method
        /// <summary>
        /// Read the resource file.
        /// </summary>
        /// <param name="fileName">File Name</param>
        /// <param name="type">Assembly Type</param>
        /// <returns>Stream of data</returns>
        private static Stream ReadResource(string fileName, Type type)
        {
            var assembly = Assembly.GetAssembly(type);
            var resourceName = string.Format("{0}.{1}", assembly.GetName().Name, fileName);
            var fileStream = assembly.GetManifestResourceNames()
                .Where(c => c.Equals(resourceName, StringComparison.InvariantCultureIgnoreCase))
                .Select(d => assembly.GetManifestResourceStream(d)).FirstOrDefault();
            if (fileStream == null)
            {
                throw new ApplicationException(string.Format("The resource could not be found [{0}]", fileName));
            }
            return fileStream;
        }

        /// <summary>
        /// Convert string to UTF8 array.
        /// </summary>
        /// <param name="pXmlString">XML string</param>
        /// <returns>UTF8 array</returns>
        private static byte[] StringToUtf8ByteArray(string pXmlString)
        {
            var encoding = new UTF8Encoding();
            var byteArray = encoding.GetBytes(pXmlString);
            return byteArray;
        }
        #endregion Private Method

        #region Public Method
        /// <summary>
        /// Get the unique id
        /// </summary>
        /// <returns></returns>
        public static string GenerateToken()
        {
            return Guid.NewGuid().ToString();
        }

        /// <summary>
        /// Adding string to string builder.
        /// </summary>
        /// <param name="sb">StringBuilder</param>
        /// <param name="message">string message</param>
        public static void AppendString(this StringBuilder sb, string message)
        {
            if (sb.ToString() == string.Empty || sb.Length == 0)
                sb.Append(message);
            else
                sb.Append("," + message);
        }

        /// <summary>
        /// Get XML string From Object
        /// </summary>
        /// <param name="response">Object</param>
        /// <returns>XML string</returns>
        public static string GetXmlFromObject(object response)
        {
            var serializer = new XmlSerializer(response.GetType());
            using (var writer = new StringWriter())
            {
                serializer.Serialize(writer, response);
                return writer.ToString();
            }
        }

        /// <summary>
        /// Get Data from XML file.
        /// </summary>
        /// <typeparam name="T">Output class name</typeparam>
        /// <param name="fileName">XML file name</param>
        /// <param name="type">ProviderConfigurationType.Ex: TEST or string.empty</param>
        /// <returns>Output object in specified cast</returns>
        public static T GetDataFromXml<T>(string fileName, Type type) where T : class
        {
            using (var fileStream = ReadResource(fileName + ".xml", type))
            {
                var serializer = new XmlSerializer(typeof(T));
                var response = (T)serializer.Deserialize(fileStream);
                if (fileStream != null)
                    fileStream.Dispose();
                return response;
            }
        }

        public static T Deserialize<T>(XDocument doc)
        {
            var xmlSerializer = new XmlSerializer(typeof(T));
            if (doc.Root == null) return default(T);
            using (var reader = doc.Root.CreateReader())
                return (T)xmlSerializer.Deserialize(reader);
        }

        public static XDocument Serialize<T>(T value)
        {
            var xmlSerializer = new XmlSerializer(typeof(T));
            var doc = new XDocument();
            using (var writer = doc.CreateWriter())
                xmlSerializer.Serialize(writer, value);
            return doc;
        }

        /// <summary>
        /// XML to object conversion
        /// </summary>
        /// <typeparam name="T">expected output type</typeparam>
        /// <param name="pXmlizedString">XML string</param>
        /// <returns>Output in expected type</returns>
        public static T DeserializeXml<T>(string pXmlizedString) where T : class
        {
            MemoryStream memory = null;
            try
            {
                var serializer = new XmlSerializer(typeof(T));
                memory = new MemoryStream(StringToUtf8ByteArray(pXmlizedString));
                return (T)serializer.Deserialize(memory);
            }
            finally
            {
                if (memory != null)
                    memory.Dispose();
            }
        }
        
        /// <summary>
        /// Trust Certificates Callback function
        /// </summary>
        /// <param name="sender">Sender object</param>
        /// <param name="cert">X509Certificate</param>
        /// <param name="chain">X509Chain</param>
        /// <param name="errors">SslPolicyErrors</param>
        /// <returns>boolean</returns>
        public static bool TrustCertificatesCallback(object sender, X509Certificate cert, X509Chain chain, SslPolicyErrors errors)
        {
            return true;
        }
        #endregion Public Method

    }
}
