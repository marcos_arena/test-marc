﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;

namespace SunHotels.Providers
{
    public class RestApiUtils
    {
        public string RequestUrl { get; set; }
        public string MethodType { get; set; }
        public string RequestContentType { get; set; }
        public string ProtocolVersion { get; set; }
        public string PostParameter { get; set; }
        public int Timeout { get; set; }
        public Dictionary<string, string> HttpHeader { get; set; }

        public string Get()
        {
            try
            {
                var http = (HttpWebRequest)WebRequest.Create(RequestUrl);
                var httpWebResponse = (HttpWebResponse)http.GetResponse();
                http.ProtocolVersion = ProtocolVersion.Equals("1.1") ? HttpVersion.Version11 : HttpVersion.Version10;
                http.Timeout = Timeout;
                using (var sr = new StreamReader(httpWebResponse.GetResponseStream()))
                    return sr.ReadToEnd();
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }
        public object Post()
        {
            try
            {
                IgnoreBadCertificates();
                var httpRequest = (HttpWebRequest)WebRequest.Create(RequestUrl);
                httpRequest.Method = MethodType;
                httpRequest.ContentType = RequestContentType;
                httpRequest.MediaType = RequestContentType;
                httpRequest.Accept = RequestContentType;
                httpRequest.Timeout = Timeout;
                httpRequest.ProtocolVersion = ProtocolVersion.Equals("1.1") ? HttpVersion.Version11 : HttpVersion.Version10;
                // httpRequest.Headers.Add("Accept-Encoding", "gzip,deflate");
                foreach (var header in HttpHeader)
                    httpRequest.Headers.Add(header.Key, header.Value);
                var requestStream = new StreamWriter(httpRequest.GetRequestStream());
                requestStream.Write(PostParameter);
                requestStream.Close();
                var httpResponse = httpRequest.GetResponse();
                //var responseStream = new GZipStream(httpResponse.GetResponseStream(), CompressionMode.Decompress);
                var responseStream = httpResponse.GetResponseStream();
                if (responseStream == null) return null;
                var streamReader = new StreamReader(responseStream);
                var response = streamReader.ReadToEnd();
                responseStream.Close();
                streamReader.Close();
                requestStream.Close();
                return response;
            }
            catch (Exception)
            {
                return null;
            }
        }
        public void IgnoreBadCertificates()
        {
            ServicePointManager.ServerCertificateValidationCallback = AcceptAllCertifications;
        }
        private static bool AcceptAllCertifications(object sender, System.Security.Cryptography.X509Certificates.X509Certificate certification, System.Security.Cryptography.X509Certificates.X509Chain chain, System.Net.Security.SslPolicyErrors sslPolicyErrors)
        {
            return true;
        }
    }
}
