using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using SunHotels.XML.Data;
using SunHotels.XML;
using System.Threading;
using System.Globalization;
using System.IO;
using System.Linq;
using Sunhotels.Export;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Xml.Serialization;
using SunHotels.XML.Infrastructure;
using Newtonsoft.Json;
using SunHotels.XML.Infrastructure.Models;
using System.Reflection;

namespace SunHotels.Providers
{
    class InternalExport : XMLProvider
    {

        private Dictionary<int, string> giataCodes = new Dictionary<int, string>();
        private Dictionary<string, Dictionary<string, List<string[]>>> globalTypes;
        private static List<string> GlobalTypesHotelToRoom = new List<string> { "airconditioning", "telephone", "tv" };
        private static List<string> GlobalTypesHotelToRoomCode = new List<string> { "GT05-AICO", "GT05-TELP", "GT05-TELE" };
        private SortedDictionary<decimal, List<string[]>> GlobalTypesDistancesBeach;
        private SortedDictionary<decimal, List<string[]>> GlobalTypesDistancesCity;
        private HashSet<int> safePassageRoomIds = new HashSet<int>();
        private List<int> specificDestinations = new List<int>();
        private Dictionary<int, string> roomTypeNames = null;
        private InternalExportSettings _localConfig;

        private static IRoomConfigurationProvider RoomConfigProvider;
        private static IStaticInformationProvider StaticInfoProvider;

        private static JsonSerializer serializer = new JsonSerializer();

        protected InternalExportSettings Settings
        {
            get
            {
                if (_localConfig == null)
                {
                    var providerConfigFullPath = configuration.ExportConfigFileFullPath;
                    if (File.Exists(providerConfigFullPath))
                    {
                        var serializer = new XmlSerializer(typeof(InternalExportSettings));
                        var s = new FileStream(providerConfigFullPath, FileMode.Open, FileAccess.Read);
                        _localConfig = (InternalExportSettings)serializer.Deserialize(s);
                    }
                    else
                    {
                        _localConfig = new InternalExportSettings();
                    }
                }
                return _localConfig;
            }
        }

        public enum Languages
        {
            English, Swedish, Norwegian, Danish, German, French, Spanish, Polish, Finish, Russian, Chinese, Italian, Portugese, ChineseT, Korean, Dutch, Hungarian, Czech
        };

        private Languages currentLanguage;


        protected override void UpdateConfiguration(Configuration configuration)
        {
            configuration.AutoValidateOutput = false;
            configuration.WriteRoomsWithoutAvailability = true;
        }

        private static SemaphoreSlim ProcessCountriesSemaphore;
        private static SemaphoreSlim ProcessGetDestinationsSemaphore;
        private static SemaphoreSlim ProcessGetResortsSemaphore;


        protected override void getData(Root root)
        {
            try
            {
                InitializeProvider();

                hotelsProcessedTimeStamp = DateTime.UtcNow;
                Thread.CurrentThread.CurrentCulture = new CultureInfo("en-GB");

                DateTime startTime = DateTime.Now;
                LogEntryAndConsoleOutput(XML.HelpClasses.Log.MessageType.Informational, "Start:" + startTime);
                GetCurrentLanguage();

                Task.Run(async () => { await getCountry(root.Places); }).Wait();

                giataCodes = GiataHotelCodes();
                safePassageRoomIds = GetSafePassageRoomIds();
                roomTypeNames = GetRoomTypes(configuration.Language);
                var hotelFilter = GetHotelsToExport();
                if (!string.IsNullOrEmpty(configuration.CountryNotesOutput))
                {
                    LogEntryAndConsoleOutput(XML.HelpClasses.Log.MessageType.Informational, "Fetching Country notes...");
                    root.CountryNotes = GetCountryNotes();
                    LogEntryAndConsoleOutput(XML.HelpClasses.Log.MessageType.Informational, "Fetching Country notes done.");
                }

                if (!string.IsNullOrEmpty(configuration.DestinationNotesOutput))
                {
                    LogEntryAndConsoleOutput(XML.HelpClasses.Log.MessageType.Informational, "Fetching Destination notes...");
                    root.DestinationNotes = GetDestinationNotes();
                    LogEntryAndConsoleOutput(XML.HelpClasses.Log.MessageType.Informational, "Fetching Destination notes done.");
                }

                if (!string.IsNullOrEmpty(configuration.ResortNotesOutput))
                {
                    LogEntryAndConsoleOutput(XML.HelpClasses.Log.MessageType.Informational, "Fetching Resort notes...");
                    root.ResortNotes = GetResortNotes();
                    LogEntryAndConsoleOutput(XML.HelpClasses.Log.MessageType.Informational, "Fetching Resort notes done.");
                }

                GetGlobalTypes();

                LogEntryAndConsoleOutput(XML.HelpClasses.Log.MessageType.Informational, "Fetching Country Hotels..");
                var getDestinationTasks = root.Places.Select(country => getDestinations(country, hotelFilter, Settings.BlockedHotels));
                Task.WaitAll(getDestinationTasks.ToArray());
                LogEntryAndConsoleOutput(XML.HelpClasses.Log.MessageType.Informational, "Fetching Country Hotels done.");

                LogEntryAndConsoleOutput(XML.HelpClasses.Log.MessageType.Informational, "Fill Room Configuration started.");
                Task.Run(async () => { await RoomConfigProvider.FillRoomConfigurationAsync(root.AllHotels().SelectMany(T => T.Rooms.Values), root, configuration.Language); }).Wait();
                LogEntryAndConsoleOutput(XML.HelpClasses.Log.MessageType.Informational, "Fill Room Configuration finished.");

                LogEntryAndConsoleOutput(XML.HelpClasses.Log.MessageType.Informational, "Fill Root Configuration started.");
                RoomConfigProvider.FillRootRoomConfiguration(root);
                LogEntryAndConsoleOutput(XML.HelpClasses.Log.MessageType.Informational, "Fill Root Configuration  finished.");

                TimeSpan timeElapsed = DateTime.Now - startTime;
                LogEntryAndConsoleOutput(XML.HelpClasses.Log.MessageType.Informational, "Time Elapsed:" + timeElapsed);

            }
            catch (Exception ex)
            {
                LogEntryAndConsoleOutput(XML.HelpClasses.Log.MessageType.Error, "Error getData:" + ex.ToString());
                throw;
            }
        }

        private void InitializeProvider()
        {
            var staticInformationDal = new StaticInformationDal(Properties.Settings.Default.sunhotelsConnectionString);
            var hotelDal = new HotelDal(Properties.Settings.Default.sunhotelsConnectionString);
            var roomConfigurationDal = new RoomConfigurationDal(Properties.Settings.Default.sunhotelsConnectionString);

            RoomConfigProvider = new RoomConfigurationProvider(roomConfigurationDal);
            StaticInfoProvider = new StaticInformationProvider(staticInformationDal, hotelDal);

            if (!string.IsNullOrEmpty(configuration.SpecificDestinations))
            {
                var specificDestination = configuration.SpecificDestinations.Split(',').Distinct().Select(s => int.Parse(s)).ToList();
                specificDestinations.AddRange(specificDestination);
            }

            ProcessCountriesSemaphore = new SemaphoreSlim(configuration.MaxDegreeOfParallelism);
            ProcessGetDestinationsSemaphore = new SemaphoreSlim(configuration.MaxDegreeOfParallelism);
            ProcessGetResortsSemaphore = new SemaphoreSlim(configuration.MaxDegreeOfParallelism);
        }

        private Task getDestinations(Place country, HashSet<int> hotelFilter, List<int> blockedHotels)
        {
            return getDestinations(country.Places, country.Id, country.Description, hotelFilter, blockedHotels);
        }

        private string addRoomTypes(int roomtype_id, int beds, int extrabeds, string description, bool sharedRoom, bool sharedFacilities)
        {
            string id = String.Format("{0}.{1}.{2}", roomtype_id, beds, extrabeds);
            lock (root.RoomTypes)
            {

                if (!root.RoomTypes.ContainsKey(id))
                {
                    RoomType rType = new RoomType();
                    rType.Beds = beds; ;
                    rType.ExtraBeds = extrabeds;
                    rType.Type_Id = id;
                    rType.Description = description;
                    rType.SharedRoom = sharedRoom;
                    rType.SharedFacilities = sharedFacilities;
                    root.RoomTypes[rType.Type_Id] = rType;
                }

            }
            return id;
        }

        //Gets all countries from the database        
        private async Task getCountry(List<Place> countryList)
        {

            IEnumerable<CountryDataRecord> countries = new List<CountryDataRecord>();

            if (specificDestinations.Any())
            {
                countries = await StaticInfoProvider.GetCountriesByDestinationAsync(configuration.Language, specificDestinations.ToArray());
            }
            else
            {
                countries = await StaticInfoProvider.GetCountriesAsync(configuration.Language);
            }

            foreach (var cRow in countries)
            {
                Place country = new Place();
                country.Id = cRow.Id.ToString();
                country.Description = cRow.Name;
                country.Description = country.Description.Trim();
                country.Codes.Add(new PlaceCodes() { Type = "ISO-COUNTRY", Value = cRow.Code });
                countryList.Add(country);
            }
        }


        private void RetrieveDataTableAndCacheOnDisk<T>(ref T table, string folder, string fileName, Func<T> getTableFromDB) where T : DataTable
        {
            // This is executed several times for different languages... so we will "cache" the "non-language" data in XML files in order to 
            // free the DB and avoid queries
            var folderPath = Path.Combine("Cache\\Temp\\", folder);
            var file = Path.Combine(folderPath, fileName);

            if (File.Exists(file))
            {
                table.ReadXml(file);
            }
            else
            {
                table = getTableFromDB();

                if (!Directory.Exists(folderPath))
                    Directory.CreateDirectory(folderPath);
                table.WriteXml(file);
            }
        }


        //Gets all destinations from the database


        private async Task getDestinations(List<Place> destinationList, string countryCode, string countryDesc, HashSet<int> hotelFilter, List<int> blockedHotels)
        {
            try
            {
                await ProcessCountriesSemaphore.WaitAsync();

                LogEntryAndConsoleOutput(XML.HelpClasses.Log.MessageType.Informational, string.Format("Getting Destinations for Country: {0} ", countryDesc));

                var countryCodeInt = int.Parse(countryCode);

                IEnumerable<DestinationDataRecord> destinationDataTable = new List<DestinationDataRecord>();
                if (specificDestinations.Any())
                {
                    destinationDataTable = await StaticInfoProvider.GetCountryDestinationAsync(countryCodeInt, specificDestinations.ToArray(), configuration.Language); ;
                }
                else
                {
                    destinationDataTable = await StaticInfoProvider.GetCountryDestinationAsync(countryCodeInt, configuration.Language);
                }

                if (destinationDataTable.Any())
                {
                    var getHotelsTask = GetHotels(countryCodeInt);
                    var getPicturesTask = GetPictures(countryCodeInt);
                    var getMealLabelsTask = GetMealLabels(countryCodeInt);
                    var getRoomsTask = GetRooms(countryCodeInt);

                    await Task.WhenAll(getHotelsTask, getPicturesTask, getMealLabelsTask, getRoomsTask);

                    var dictHotelData = await getHotelsTask;
                    var picturesDictionary = await getPicturesTask;
                    var mealsDictionary = await getMealLabelsTask;
                    var roomsDictionary = await getRoomsTask;

                    var currentTasks = destinationDataTable.Select(dRow => ProcessGetDestinations(destinationList, dRow, dictHotelData, picturesDictionary, mealsDictionary, roomsDictionary, hotelFilter, blockedHotels));
                    await Task.WhenAll(currentTasks.ToArray());
                }

                LogEntryAndConsoleOutput(XML.HelpClasses.Log.MessageType.Informational, string.Format("Destinations for: {0} completed.", countryDesc));

            }
            finally
            {
                ProcessCountriesSemaphore.Release();
            }
        }


        private async Task ProcessGetDestinations(List<Place> destinationList, DestinationDataRecord dRow, Dictionary<int, HotelDataRecord> dictHotelData
                                                , Dictionary<int, List<HotelImageDataRecord>> picturesDictionary, Dictionary<int, List<MealLabel>> mealsDictionary
                                                , Dictionary<int, List<Room>> roomsDictionary, HashSet<int> hotelFilter,
                                                  List<int> blockedHotels)
        {
            try
            {
                await ProcessGetDestinationsSemaphore.WaitAsync();

                var dest = new Place();
                dest.Id = dRow.Id.ToString();
                dest.Description = dRow.Name;

                if (!string.IsNullOrEmpty(dRow.Code))
                {
                    string destCode = dRow.Code.Trim();
                    if (configuration.Version < 130)
                    {
                        dest.Description += string.Format("[{0}]", destCode);
                    }
                    dest.Codes.Add(new PlaceCodes() { Type = "IATA", Value = destCode });
                }

                dest.Description = dest.Description.Trim();

                await getResorts(dest.Places, dRow.Id, dictHotelData, picturesDictionary, mealsDictionary, roomsDictionary, hotelFilter, blockedHotels);

                lock (destinationList)
                    destinationList.Add(dest);

            }
            finally
            {
                ProcessGetDestinationsSemaphore.Release();
            }

        }

        //Gets all resorts from the database
        private async Task getResorts(List<Place> resortList, int destinationID,
                                            Dictionary<int, HotelDataRecord> cacheWithHotelDataWithoutLanguage,
                                            Dictionary<int, List<HotelImageDataRecord>> cachePictures,
                                            Dictionary<int, List<MealLabel>> cacheMeals,
                                            Dictionary<int, List<Room>> cacheRooms,
                                            HashSet<int> hotelFilter,
                                            List<int> blockedHotels)
        {

            var resorts = await StaticInfoProvider.GetResortsAsync(destinationID, configuration.Language, IsTrue(configuration.ExtraOptions), IsTrue(configuration.IncludeAllResorts));
            var currentTasks = resorts.Select(resRow => ProcessGetResorts(resortList, cacheWithHotelDataWithoutLanguage, cachePictures, cacheMeals, cacheRooms, resRow, hotelFilter, blockedHotels));
            await Task.WhenAll(currentTasks.ToArray());

        }

        private async Task ProcessGetResorts(List<Place> resortList, Dictionary<int, HotelDataRecord> cacheWithHotelDataWithoutLanguage
            , Dictionary<int, List<HotelImageDataRecord>> cachePictures, Dictionary<int, List<MealLabel>> cacheMeals
            , Dictionary<int, List<Room>> cacheRooms, ResortDataRecord resRow, HashSet<int> hotelFilter,
                                List<int> blockedHotels)
        {
            try
            {

                await ProcessGetResortsSemaphore.WaitAsync();

                Place reso = new Place();
                reso.Id = resRow.Id.ToString();
                reso.Description = resRow.Name;
                reso.Description = reso.Description.Trim();

                await getHotels(reso.Hotels, resRow.Id, cacheWithHotelDataWithoutLanguage, cachePictures, cacheMeals, cacheRooms, hotelFilter, blockedHotels);

                lock (resortList)
                    resortList.Add(reso);
            }
            finally
            {
                ProcessGetResortsSemaphore.Release();
            }

        }

        private static string[] IsTrueMap = new string[] { "true", "1" };
        private bool IsTrue(string input)
        {
            return IsTrueMap.Contains(input.ToLower());
        }

        private async Task getHotels(Dictionary<string, Hotel> hotelList, int resortID,
                                Dictionary<int, HotelDataRecord> cacheWithHotelDataWithoutLanguage,
                                Dictionary<int, List<HotelImageDataRecord>> cachePictures,
                                Dictionary<int, List<MealLabel>> cacheMeals,
                                Dictionary<int, List<Room>> cacheRooms, HashSet<int> hotelFilter,
                                List<int> blockedHotels)
        {

            var mealsDictionary = cacheMeals;


            var thomasCookMeals = new Dictionary<int, List<MealLabel>>();
            if (configuration.ProviderName == "Thomas Cook") // Only used for thomas cook
                thomasCookMeals = await GetMealLabelsNew(resortID);

            var roomsDictionary = cacheRooms;

            IEnumerable<HotelDataRecord> hotelDataTable = new List<HotelDataRecord>();

            hotelDataTable = await StaticInfoProvider.GetCountryHotelsAsync(resortID, configuration.Language);

            foreach (var hRow2 in hotelDataTable)
            {
                HotelDataRecord hRow = null;
                if (hotelFilter != null && !hotelFilter.Contains(hRow2.id))
                {
                    LogEntryAndConsoleOutput(XML.HelpClasses.Log.MessageType.Warning, "Hotel " + hRow2.id + " not found in filter list");
                    continue;
                }
                if (blockedHotels?.Contains(hRow2.id) == true)
                {
                    LogEntryAndConsoleOutput(XML.HelpClasses.Log.MessageType.Warning, "Hotel " + hRow2.id + " blocked by configuration settings");
                    continue;
                }

                if (!cacheWithHotelDataWithoutLanguage.TryGetValue(hRow2.id, out hRow))
                {
                    LogEntryAndConsoleOutput(XML.HelpClasses.Log.MessageType.Warning, "Hotel " + hRow2.id + " not found in destination cache");
                    continue;
                }

                if (!this.configuration.SkipContentLanguageControl)
                {
                    if (hRow.ShowinEN <= 0) continue;
                    //if (currentLanguage == Languages.Danish && (hRow.ShowinDKxml <= 0 || hRow.ShowinDK <= 0)) continue;
                    //if (currentLanguage == Languages.English && (hRow.ShowinENxml <= 0 || hRow.ShowinEN <= 0)) continue;
                    //if (currentLanguage == Languages.Finish && (hRow.ShowinFIxml <= 0 || hRow.ShowinFI <= 0)) continue;
                    //if (currentLanguage == Languages.French && (hRow.ShowinFRxml <= 0 || hRow.ShowinFR <= 0)) continue;
                    //if (currentLanguage == Languages.German && (hRow.ShowinGExml <= 0 || hRow.ShowinGE <= 0)) continue;
                    //if (currentLanguage == Languages.Norwegian && (hRow.ShowinNOxml <= 0 || hRow.ShowinNO <= 0)) continue;
                    //if (currentLanguage == Languages.Polish && (hRow.ShowinPLxml <= 0 || hRow.ShowinPL <= 0)) continue;
                    //if (currentLanguage == Languages.Russian && (hRow.ShowinRUxml <= 0 || hRow.ShowinRU <= 0)) continue;
                    //if (currentLanguage == Languages.Spanish && (hRow.ShowinESxml <= 0 || hRow.ShowinES <= 0)) continue;
                    //if (currentLanguage == Languages.Swedish && (hRow.ShowinSVxml <= 0 || hRow.ShowinSV <= 0)) continue;
                    //if (currentLanguage == Languages.Chinese && (hRow.ShowinZHxml <= 0 || hRow.ShowinZH <= 0)) continue;
                    //if (currentLanguage == Languages.Italian && (hRow.ShowinITxml <= 0 || hRow.ShowinIT <= 0)) continue;
                    //if (currentLanguage == Languages.Portugese && (hRow.ShowinPTxml <= 0 || hRow.ShowinPT <= 0)) continue;
                }

                Hotel hotel = new Hotel();
                switch (hRow.accType)
                {
                    case "Hotel": hotel.AccomodationType = AccomodationType.Hotel; break;
                    case "Apartment": hotel.AccomodationType = AccomodationType.Apartment; break;
                    case "Villa": hotel.AccomodationType = AccomodationType.Villa; break;

                    default: hotel.AccomodationType = AccomodationType.Hotel; break;
                }

                hotel.Id = hRow.id.ToString();
                hotel.Headline = GetXmlSafeOutput(hRow2.HotelHeadline); // this field is language dependent, we get it from hrow2
                hotel.Description = GetXmlSafeOutput(hRow2.HotelDescription); // this field is language dependent, we get it from hrow2

                if (configuration.ProviderName == "Thomas Cook")
                {
                    if (!giataCodes.ContainsKey(int.Parse(hotel.Id)))
                    {
                        LogEntryAndConsoleOutput(XML.HelpClasses.Log.MessageType.Informational, "Discarted hotel: " + hotel.Id);
                        continue;
                    }

                    hotel.GiataCode = giataCodes[int.Parse(hotel.Id)];
                }


                hotel.Headline = GetXmlSafeOutput(hotel.Headline.Trim());

                char[] trimChar = { '\t', ' ', ',' };

                hotel.Adress_City = GetXmlSafeOutput(hRow.addr_city.Trim(trimChar));
                hotel.Adress_State = GetXmlSafeOutput(hRow.addr_state.Trim(trimChar));
                hotel.Adress_Country = GetXmlSafeOutput(hRow.addr_country.Trim(trimChar));
                hotel.Adress_Street1 = GetXmlSafeOutput(hRow.addr_1.Trim(trimChar));
                hotel.Adress_Street2 = GetXmlSafeOutput(hRow.addr_2.Trim(trimChar));
                hotel.Adress_Zipcode = hRow.addr_zip.Trim(trimChar);
                hotel.Classification = hRow.sunClass.Trim();
                if (!String.IsNullOrEmpty(hRow.classPlus))
                {
                    hotel.Classification += hRow.classPlus;
                }
                hotel.Fax = hRow.fax.Trim();
                hotel.Name = hRow.hotelName.Trim();
                hotel.Phone = hRow.telephone.Trim();
                hotel.PlaceId = resortID.ToString();
                //hotel.Tag = hRow.notes.Trim();

                if (hRow.BlockInfant)
                    hotel.BlockInfant = hRow.BlockInfant;
                if (hRow.AdultOnly)
                    hotel.AdultOnly = hRow.AdultOnly;


                if (configuration.ProviderName == "Thomas Cook")
                {
                    List<MealLabel> meals;
                    if (thomasCookMeals.TryGetValue(hRow.id, out meals))
                        hotel.MealLabels = meals;
                    else
                        hotel.MealLabels = new List<MealLabel>();
                }
                else
                {
                    List<MealLabel> meals;
                    if (mealsDictionary.TryGetValue(hRow.id, out meals))
                        hotel.MealLabels = meals;
                    else
                        hotel.MealLabels = new List<MealLabel>();
                }

                if (cachePictures?.ContainsKey(hRow.id) == true)
                {
                    var pictureList = cachePictures[hRow.id].ToList();//.OrderBy(p => p.ImageId) removed because it comes sorted from database
                    foreach (var picRow in pictureList)
                    {
                        Hotel.Image image = new Hotel.Image() { Id = picRow.ImageId.ToString() };

                        Hotel.ImageVariant smallImage = new Hotel.ImageVariant()
                        {
                            URL = (configuration.SmallImageUrlPrefix ?? "https://hotelimages.sunhotels.net/HotelInfo/hotelImage.aspx?id=") + picRow.ImageId
                        };
                        image.ImageVariants.Add(smallImage);
                        if (picRow.SmallWidth != 0 && picRow.SmallHeight != 0)
                        {
                            smallImage.Width = picRow.SmallWidth;
                            smallImage.Height = picRow.SmallHeight;
                        }

                        if (picRow.LargeID != 0)
                        {
                            Hotel.ImageVariant largeImage = new Hotel.ImageVariant()
                            {
                                URL = (configuration.LargeImageUrlPrefix ?? "https://hotelimages.sunhotels.net/HotelInfo/hotelImage.aspx?full=1&id=") + picRow.ImageId
                            };
                            image.ImageVariants.Add(largeImage);
                            if (picRow.LargeWidth != 0 && picRow.LargeHeight != 0)
                            {
                                largeImage.Width = picRow.LargeWidth;
                                largeImage.Height = picRow.LargeHeight;
                            }
                        }

                        hotel.Images.Add(image);
                    }
                }

                try
                {
                    if (hRow.GLat != 0 && hRow.GLng != 0)
                    {
                        hotel.Position_Latitude = hRow.GLat.ToString(CultureInfo.InvariantCulture);
                        hotel.Position_Longitude = hRow.GLng.ToString(CultureInfo.InvariantCulture);
                    }
                }
                catch
                {
                    LogEntryAndConsoleOutput(XML.HelpClasses.Log.MessageType.Informational, string.Format("Longitude or latidute DBNull on hotel with id: {0}", hotel.Id));
                }


                Decimal dCent = ParseDistance(hRow.distCentre);
                if (dCent >= 0)
                {
                    hotel.Distances.Add(new Hotel.Distance("center", dCent));
                }
                var dAirP = ParseDistances("airport", hRow.distAirport);
                if (dAirP.Count >= 0)
                {
                    hotel.Distances.AddRange(dAirP);
                }
                Decimal dBeach = ParseDistance(hRow.distBeach);
                if (dBeach >= 0)
                {
                    hotel.Distances.Add(new Hotel.Distance("beach", dBeach));
                }

                string feat = hRow.features.ToString();
                if (feat.Length > 0)
                {
                    if (feat.Contains("Air Conditioning"))
                    {
                        hotel.Features.Add(new Feature() { Name = "airconditioning", Value = "true" });
                    }
                    if (feat.Contains("Elevator"))
                    {
                        hotel.Features.Add(new Feature() { Name = "elevator", Value = "true" });
                    }
                    if (feat.Contains("Bar"))
                    {
                        hotel.Features.Add(new Feature() { Name = "bar", Value = "true" });
                    }
                    if (feat.Contains("Telephone"))
                    {
                        hotel.Features.Add(new Feature() { Name = "telephone", Value = "true" });
                    }
                    if (feat.Contains("Pool Children"))
                    {
                        hotel.Features.Add(new Feature() { Name = "childrens_pool", Value = "true" });
                    }
                    if (feat.Contains("Pool"))
                    {
                        hotel.Features.Add(new Feature() { Name = "pool", Value = "true" });
                    }
                    if (feat.Contains("Safe"))
                    {
                        hotel.Features.Add(new Feature() { Name = "safe", Value = "true" });
                    }
                    //if (feat.Contains("Seaview"))
                    //{
                    //    hotel.Features.Add(new Feature() { Name = "sea_view", Value = "true" });
                    //}
                    if (feat.Contains("Balcony"))
                    {
                        hotel.Features.Add(new Feature() { Name = "balcony", Value = "true" });
                    }
                    if (feat.Contains("Restaurant"))
                    {
                        hotel.Features.Add(new Feature() { Name = "restaurant", Value = "true" });
                    }
                    if (feat.Contains("TV"))
                    {
                        hotel.Features.Add(new Feature() { Name = "tv", Value = "true" });
                    }
                    if (feat.Contains("Wireless internet"))
                    {
                        hotel.Features.Add(new Feature() { Name = "wireless internet", Value = "true" });
                    }
                }

                lock (root.Themes)
                    foreach (var theme in root.Themes)
                        if ((hRow.flag & theme.Id) != 0)
                            hotel.Themes.Add(theme);

                if (hRow.bestbuy)
                {
                    hotel.BestBuy = true;
                }

                if (hRow.ShowReview && hRow.ReviewAvgRating != -1)
                {
                    hotel.Reviews.Add(new Review() { Provider = "CHR", Type = "ReviewAvgRating", Value = hRow.ReviewAvgRating.ToString("0.00", CultureInfo.InvariantCulture) });
                    hotel.Reviews.Add(new Review() { Provider = "CHR", Type = "ReviewAvgPriceValue", Value = hRow.ReviewAvgPriceValue.ToString("0.00", CultureInfo.InvariantCulture) });
                    hotel.Reviews.Add(new Review() { Provider = "CHR", Type = "ReviewCount", Value = hRow.ReviewCount.ToString() });
                    hotel.Reviews.Add(new Review() { Provider = "CHR", Type = "ReviewRecommendCount", Value = hRow.ReviewRecommendCount.ToString() });
                }

                if (roomsDictionary.ContainsKey(hRow.id))
                {
                    foreach (var room in roomsDictionary[hRow.id])
                    {
                        room.AdultOnlyHotel = hotel.AdultOnly;
                        hotel.Rooms.Add(room.RoomId, room);
                    }
                }

                SetGlobalTypes(hotel);

                lock (hotelList)
                    hotelList.Add(hotel.Id, hotel);

                Interlocked.Add(ref hotelsProcessed, 1);

                var milliSecondsSinceLastMessage = DateTime.UtcNow.Subtract(hotelsProcessedTimeStamp).TotalMilliseconds;
                if (milliSecondsSinceLastMessage > periodMessageInMilliseconds)
                {
                    lock (hotelsProcessedLock)
                    {
                        milliSecondsSinceLastMessage = DateTime.UtcNow.Subtract(hotelsProcessedTimeStamp).TotalMilliseconds;
                        if (milliSecondsSinceLastMessage > periodMessageInMilliseconds)
                        {
                            hotelsProcessedTimeStamp = DateTime.UtcNow;
                            var numberProcessedSinceLastTimestamp = hotelsProcessed - previousHotelsProcessed;
                            var speed = 1000 * numberProcessedSinceLastTimestamp / milliSecondsSinceLastMessage;

                            LogEntryAndConsoleOutput(XML.HelpClasses.Log.MessageType.Informational, hotelsProcessed + " hotels processed (" + speed.ToString("F0") + " hotels/second)");
                            previousHotelsProcessed += numberProcessedSinceLastTimestamp;

                        }
                    }
                }
            }

        }

        private void SetGlobalTypes(Hotel hotel)
        {
            if (configuration.ProviderName == "Thomas Cook" && globalTypes != null && globalTypes.Any())
            {
                /*
                  1 features              5 accommodation_type   9 distance_type_city
                  2 themes                6 description          10 meals
                  3 distance_type_beach   7 meal_labels          11 calendar_notes
                  4 room_types            8 adult_only
                 */
                Dictionary<string, List<string[]>> mapping;
                List<string[]> mapTypes;
                hotel.GlobalTypes = new List<string[]>();
                if (hotel.Features.Any() && globalTypes.TryGetValue("features", out mapping))
                {
                    foreach (var f in hotel.Features)
                    {
                        if (mapping.TryGetValue(f.Name, out mapTypes)) hotel.GlobalTypes.AddRange(mapTypes);
                    }
                }
                if (hotel.Themes.Any() && globalTypes.TryGetValue("themes", out mapping))
                {
                    foreach (var t in hotel.Themes)
                    {
                        if (mapping.TryGetValue(t.Value, out mapTypes)) hotel.GlobalTypes.AddRange(mapTypes);
                    }
                }
                if (hotel.Distances.Any(d => d.Name == "beach") && globalTypes.TryGetValue("distance_type_beach", out mapping))
                {
                    foreach (var d in hotel.Distances.Where(d => d.Name == "beach"))
                    {
                        if (mapping.TryGetValue(d.Value, out mapTypes))
                        {
                            hotel.GlobalTypes.AddRange(mapTypes);
                        }
                        else
                        {
                            hotel.GlobalTypes.AddRange(GlobalTypeDistanceGetNextAvailable(d.Value, GlobalTypesDistancesBeach));
                        }
                    }
                }
                if (hotel.Distances.Any(d => d.Name == "center") && globalTypes.TryGetValue("distance_type_city", out mapping))
                {
                    foreach (var d in hotel.Distances.Where(d => d.Name == "center"))
                    {
                        if (mapping.TryGetValue(d.Value, out mapTypes))
                        {
                            hotel.GlobalTypes.AddRange(mapTypes);
                        }
                        else
                        {
                            hotel.GlobalTypes.AddRange(GlobalTypeDistanceGetNextAvailable(d.Value, GlobalTypesDistancesCity));
                        }
                    }
                }
                if (hotel.AccomodationType.HasValue && globalTypes.TryGetValue("accommodation_type", out mapping))
                {
                    string accType = null;
                    switch (hotel.AccomodationType)
                    {
                        case AccomodationType.Apartment: accType = "Apartment"; break;
                        case AccomodationType.Villa: accType = "Villa"; break;
                        case AccomodationType.Hotel:
                        default: accType = "Hotel"; break;
                    }
                    if (mapping.TryGetValue(accType.ToLower(), out mapTypes)) hotel.GlobalTypes.AddRange(mapTypes);
                }
                if (!string.IsNullOrEmpty(hotel.Description) && globalTypes.TryGetValue("description", out mapping))
                {
                    foreach (var mp in mapping)
                    {
                        if (hotel.Description.Contains(mp.Key)) hotel.GlobalTypes.AddRange(mp.Value);
                    }
                }
                if (hotel.MealLabels.Any(ml => ml.MealLabelId > 0) && globalTypes.TryGetValue("meal_labels", out mapping))
                {
                    foreach (var ml in hotel.MealLabels.Where(ml => ml.MealLabelId > 0))
                    {
                        if (mapping.TryGetValue(ml.MealLabelId.ToString(), out mapTypes)) hotel.GlobalTypes.AddRange(mapTypes);
                    }
                }
                if (hotel.AdultOnly && globalTypes.TryGetValue("adult_only", out mapping))
                {
                    if (mapping.TryGetValue("adults_only", out mapTypes)) hotel.GlobalTypes.AddRange(mapTypes);
                }
                if (hotel.MealLabels.Any(ml => ml.MealId > 0) && globalTypes.TryGetValue("meals", out mapping))
                {
                    foreach (var ml in hotel.MealLabels.Where(ml => ml.MealId > 0))
                    {
                        if (mapping.TryGetValue(ml.MealId.ToString(), out mapTypes)) hotel.GlobalTypes.AddRange(mapTypes);
                    }
                }

                if (hotel.Rooms != null && hotel.Rooms.Any())
                {
                    foreach (var room in hotel.Rooms.Values)
                    {
                        room.GlobalTypes = new List<string[]>();
                        if (
                            (room.Features.Any() || hotel.Features.Any(f => GlobalTypesHotelToRoom.Contains(f.Name)))
                            && globalTypes.TryGetValue("features", out mapping)
                        )
                        {
                            foreach (var f in room.Features)
                            {
                                if (mapping.TryGetValue(f.Name, out mapTypes)) room.GlobalTypes.AddRange(mapTypes);
                            }
                            foreach (var f in hotel.Features.Where(f => GlobalTypesHotelToRoom.Contains(f.Name)))
                            {
                                mapTypes = new List<string[]> { new string[3] { GlobalTypesHotelToRoomCode[GlobalTypesHotelToRoom.IndexOf(f.Name)], string.Empty, string.Empty } };
                                room.GlobalTypes.AddRange(mapTypes);
                            }
                        }
                        if (room.Themes.Any() && globalTypes.TryGetValue("themes", out mapping))
                        {
                            foreach (var t in room.Themes)
                            {
                                if (mapping.TryGetValue(t.Value, out mapTypes)) room.GlobalTypes.AddRange(mapTypes);
                            }
                        }
                        if (globalTypes.TryGetValue("room_types", out mapping))
                        {
                            var rt = root.RoomTypes[room.TypeId];
                            if (mapping.TryGetValue(rt.Type_Id.Substring(0, rt.Type_Id.IndexOf('.')), out mapTypes)) room.GlobalTypes.AddRange(mapTypes);
                        }
                        if (!string.IsNullOrEmpty(room.Description) && globalTypes.TryGetValue("description", out mapping))
                        {
                            foreach (var mp in mapping)
                            {
                                if (room.Description.Contains(mp.Key)) room.GlobalTypes.AddRange(mp.Value);
                            }
                        }
                    }
                }
            }
        }

        private List<string[]> GlobalTypeDistanceGetNextAvailable(string distance, SortedDictionary<decimal, List<string[]>> globalTypeDistances)
        {
            var mapTypes = new List<string[]>();
            var dist = ParseDistance(distance + "km");
            for (var i = 0; i < globalTypeDistances.Count; i++)
            {
                var currentDistance = globalTypeDistances.ElementAt(i);
                if (i + 1 < globalTypeDistances.Count)
                {
                    var nextDistance = globalTypeDistances.ElementAt(i + 1);
                    if (currentDistance.Key < dist && dist <= nextDistance.Key)
                    {
                        mapTypes.AddRange(nextDistance.Value);
                        break;
                    }
                }
                else
                {
                    if (currentDistance.Key >= 101m && currentDistance.Key <= dist) // City 'ST03-D101KM'
                    {
                        mapTypes.AddRange(currentDistance.Value);
                    }
                }
            }
            return mapTypes;
        }

        public Dictionary<int, string> GiataHotelCodes()
        {
            var giataCodes = new Dictionary<int, string>();
            using (var connection = new SqlConnection(Properties.Settings.Default.sunhotelsConnectionString))
            {
                try
                {
                    connection.Open();
                    using (var command = new SqlCommand())
                    {
                        command.Connection = connection;
                        command.CommandText = "SELECT hotelId, value FROM dbo.hotelCode with(nolock) WHERE hotelCodeTypeId = 2"; //Type 2 = GIATA

                        using (var reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                var hotelId = (int)reader["hotelId"];
                                var value = reader["value"].ToString();
                                if (!giataCodes.ContainsKey(hotelId))
                                    giataCodes.Add(hotelId, value);
                            }

                        }
                    }
                }
                finally
                {
                    if (connection.State == ConnectionState.Open)
                        connection.Close();
                }

            }
            return giataCodes;
        }

        private HashSet<int> GetSafePassageRoomIds()
        {
            var roomIds = new HashSet<int>();
            if (configuration.ProviderName == "Thomas Cook" && Settings.RoomProviderNameContains != null && Settings.RoomProviderNameContains.Length > 0)
            {
                var names = Settings.RoomProviderNameContains;

                var queryNames = string.Join(" AND ", names.Select(name => $" name LIKE '%{name}%' "));
                var query = $@" SELECT r.Id as RoomId FROM rooms r with(nolock)
                    INNER JOIN roomprovider rp with(nolock) ON r.roomprovider_id = rp.id
                    WHERE  {queryNames} ";

                using (var connection = new SqlConnection(Properties.Settings.Default.sunhotelsConnectionString))
                {
                    try
                    {
                        connection.Open();
                        using (var command = new SqlCommand())
                        {
                            command.Connection = connection;
                            command.CommandText = query;
                            using (var reader = command.ExecuteReader())
                            {
                                while (reader.Read())
                                    roomIds.Add((int)reader["RoomId"]);
                            }
                        }
                    }
                    finally
                    {
                        if (connection.State == ConnectionState.Open)
                            connection.Close();
                    }

                }
            }
            return roomIds;
        }


        private Dictionary<int, string> GetRoomTypes(string language)
        {
            var query = @"SELECT rt.id as Id, 
                    ISNULL(NULLIF (dbo.GetLangRoomTypeOnRoomTypeID(rt.id, @lang), ''), dbo.GetLangRoomTypeOnRoomTypeID(rt.id, N'en')) AS RoomTypeName 
                    FROM roomtypes rt with(nolock)";

            var roomTypes = new Dictionary<int, string>();
            using (var connection = new SqlConnection(Properties.Settings.Default.sunhotelsConnectionString))
            {
                try
                {
                    connection.Open();
                    using (var command = new SqlCommand(query, connection))
                    {
                        var sqlParameter = new SqlParameter() { Size = 2, ParameterName = "lang", SqlDbType = SqlDbType.VarChar, Value = language };
                        command.Parameters.Add(sqlParameter);

                        using (var reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                var rtId = (int)reader["Id"];
                                var rtName = reader["RoomTypeName"].ToString();
                                roomTypes[rtId] = rtName;
                            }
                        }
                    }
                }
                finally
                {
                    if (connection.State == ConnectionState.Open)
                        connection.Close();
                }
            }

            return roomTypes;
        }

        private async Task<Dictionary<int, List<MealLabel>>> GetMealLabelsNew(int resortID)
        {
            var mealLabels = await RetrieveJsonOnDisk<List<MealDataRecord>>("ThomasCookMeal", $"{resortID}.json", async () => await StaticInfoProvider.GetMealLabelForThomasCookAsync(resortID));
            return mealLabels.GroupBy(t => t.HotelId).ToDictionary(t => t.Key, t => t.Select(m => new MealLabel(m.MealId, m.MealLabelId)).ToList());
        }

        public void GetGlobalTypes()
        {
            if (configuration.ProviderName == "Thomas Cook")
            {
                globalTypes = new Dictionary<string, Dictionary<string, List<string[]>>>();
                using (var connection = new SqlConnection(Properties.Settings.Default.sunhotelsConnectionString))
                {
                    connection.Open();
                    try
                    {
                        using (var command = new SqlCommand())
                        {
                            var query = @"SELECT 
                                gt.GlobalType, 
                                gt.Subtype, gt.AdditionalType, 
                                gt.SHFeatureCode, 
                                ft.FeatureTypeDesc 
                            FROM dbo.DRV_GlobalTypeMapping gt WITH(NOLOCK)
                            INNER JOIN dbo.SHFeatureType ft WITH(NOLOCK) ON ft.ID = gt.SHFeatureTypeId";
                            command.Connection = connection;
                            command.CommandText = query;

                            using (var reader = command.ExecuteReader())
                            {
                                while (reader.Read())
                                {
                                    var globalType = (string)reader["GlobalType"];
                                    var subtype = (string)reader["Subtype"];
                                    var additionalType = (string)reader["AdditionalType"];
                                    var shFeatureCode = (string)reader["SHFeatureCode"];
                                    var featureTypeDesc = (string)reader["FeatureTypeDesc"];

                                    Dictionary<string, List<string[]>> mapping;
                                    List<string[]> listMapping;
                                    if (!globalTypes.TryGetValue(featureTypeDesc, out mapping))
                                    {
                                        mapping = new Dictionary<string, List<string[]>>();
                                        globalTypes.Add(featureTypeDesc, mapping);
                                    }
                                    if (!mapping.TryGetValue(shFeatureCode, out listMapping))
                                    {
                                        listMapping = new List<string[]>();
                                        mapping.Add(shFeatureCode, listMapping);
                                    }
                                    var mapType = new string[3] { globalType, subtype, additionalType };
                                    listMapping.Add(mapType);
                                }
                            }
                        }
                    }
                    finally
                    {
                        if (connection.State == ConnectionState.Open)
                            connection.Close();
                    }

                }

                Dictionary<string, List<string[]>> mappingDistance;
                if (globalTypes.TryGetValue("distance_type_beach", out mappingDistance))
                {
                    GlobalTypesDistancesBeach = new SortedDictionary<decimal, List<string[]>>(mappingDistance.ToDictionary(e => ParseDistance(e.Key + "km"), e => e.Value));
                }
                if (globalTypes.TryGetValue("distance_type_city", out mappingDistance))
                {
                    GlobalTypesDistancesCity = new SortedDictionary<decimal, List<string[]>>(mappingDistance.ToDictionary(e => ParseDistance(e.Key + "km"), e => e.Value));
                }
            }
        }

        private int periodMessageInMilliseconds = 10000;
        private int hotelsProcessed = 0;
        private int previousHotelsProcessed = 0;
        private DateTime hotelsProcessedTimeStamp;
        private object hotelsProcessedLock = new object();


        public static string GetXmlSafeOutput(string value)
        {
            if (string.IsNullOrEmpty(value)) return string.Empty;
            var xmlSafeOutput = System.Web.HttpUtility.HtmlDecode(value);
            return xmlSafeOutput.Trim();
        }


        protected override void getProviderDefinition(Root root)
        {
            root.ProviderDefinition.Name = "InternalExport";
            root.ProviderDefinition.Currency = "EUR";
        }

        protected override void getPlaceCodeTypes(Root root)
        {
            root.PlaceCodeTypes.Add("IATA");
            root.PlaceCodeTypes.Add("ISO-COUNTRY");
        }

        private void GetCurrentLanguage()
        {
            switch (configuration.Language)
            {
                case "en": currentLanguage = Languages.English; break;
                case "fi": currentLanguage = Languages.Finish; break;
                case "sv": currentLanguage = Languages.Swedish; break;
                case "ge": currentLanguage = Languages.German; break;
                case "no": currentLanguage = Languages.Norwegian; break;
                case "pl": currentLanguage = Languages.Polish; break;
                case "es": currentLanguage = Languages.Spanish; break;
                case "dk": currentLanguage = Languages.Danish; break;
                case "fr": currentLanguage = Languages.French; break;
                case "ru": currentLanguage = Languages.Russian; break;
                case "it": currentLanguage = Languages.Italian; break;
                case "pt": currentLanguage = Languages.Portugese; break;
                case "zh": currentLanguage = Languages.Chinese; break;
                case "zt": currentLanguage = Languages.ChineseT; break;
                case "ko": currentLanguage = Languages.Korean; break;
                case "nl": currentLanguage = Languages.Dutch; break;
                case "hu": currentLanguage = Languages.Hungarian; break;
                case "cs": currentLanguage = Languages.Czech; break;
                default: currentLanguage = Languages.English; break;
            }
        }


        /// <summary>
        /// parse distances with multiple distances
        /// </summary>
        /// <param name="distance"></param>
        /// <returns></returns>
        private List<Hotel.Distance> ParseDistances(string distanceType, string distance)
        {
            var distances = new List<Hotel.Distance>();

            if (distanceType == "airport")
            {
                if (distance == null) return distances;

                var split = Regex.Split(distance, @"/\s*(?=\d+)");
                foreach (var dist in split)
                {
                    decimal distToAirport;
                    var dta = Regex.Match(dist, @"[\d]{1,3}([\.,]\d{1,3})?");
                    var iataCode = Regex.Match(dist, @"[A-Z]{3}");
                    if (!decimal.TryParse(dta.Value, out distToAirport)) continue;
                    if (distToAirport > 0)
                        distances.Add(new Hotel.Distance { Name = distanceType, Value = distToAirport.ToString(CultureInfo.InvariantCulture), IataCode = iataCode.Value });
                }

                if (distances.Count > 0) return distances;
            }

            var value = ParseDistance(distance);
            if (value > 0) distances.Add(new Hotel.Distance(distanceType, value));

            return distances;
        }

        private static char[] numericChar = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', ' ', '\t', ',', '.' };
        private Decimal ParseDistance(string distance)
        {
            if (distance == null) return -1;
            distance = distance.ToLower();

            try
            {
                String distWONumeric = distance.TrimStart(numericChar);
                if (distWONumeric == distance.Trim())
                    return -1;
                if (distWONumeric.StartsWith("km"))
                {
                    var tmp = distance.Replace(',', '.').Substring(0, distance.IndexOf(distWONumeric)).Split(new string[] { "km" }, StringSplitOptions.RemoveEmptyEntries)[0];
                    return Decimal.Parse(tmp.Replace(" ", "").Replace("..","."), System.Globalization.CultureInfo.InvariantCulture);
                }
                else if (distWONumeric.StartsWith("m"))
                {
                    if (distance.StartsWith("mil"))
                    {
                        File.AppendAllText("testParseDistance.txt", distance);
                        return -1;
                    }
                    var tmp = distance.Replace(',', '.').Substring(0, distance.IndexOf(distWONumeric)).Split(new string[] { "m" }, StringSplitOptions.RemoveEmptyEntries)[0];
                    return Decimal.Parse(tmp.Replace(" ", "").Replace("..", "."), System.Globalization.CultureInfo.InvariantCulture) / 1000;
                }
                else if (distance.Trim() == "0")
                {
                    return 0;
                }
                else if (distance.StartsWith("0 "))
                {
                    return 0;
                }
                else if (distance.Trim() == String.Empty)
                {
                    return -1;
                }
                else
                {
                    return -1;
                    //return Decimal.Parse(distance);
                }
            }
            catch
            {
                //File.AppendAllText("c:\\testParseDistance.txt", distance);
                return -1;
            }
        }

        private async Task<Dictionary<int, HotelDataRecord>> GetHotels(int countryCode)
        {
            var hotelDataTableNoLang = new List<HotelDataRecord>();
            hotelDataTableNoLang = await RetrieveJsonOnDisk<List<HotelDataRecord>>("Hotels", countryCode + ".json", async () => await StaticInfoProvider.GetCountryHotelsAsync(countryCode));
            return hotelDataTableNoLang.Distinct().ToDictionary(t => t.id, t => t);
        }

        private async Task<Dictionary<int, List<HotelImageDataRecord>>> GetPictures(int countryCode)
        {
            var hotelPicturesDataTable = new List<HotelImageDataRecord>();
            hotelPicturesDataTable = await RetrieveJsonOnDisk<List<HotelImageDataRecord>>("Pictures", $"{countryCode}.json", async () => await StaticInfoProvider.GetHotelImageByCountryAsync(countryCode));
            return hotelPicturesDataTable?.GroupBy(t => t.HotelId).ToDictionary(t => t.Key, t => t.ToList());
        }

        private async Task<Dictionary<int, List<Room>>> GetRooms(int countryCode)
        {
            var processSafePassage = configuration.ProviderName == "Thomas Cook";
            var roomsDictionary = new Dictionary<int, List<Room>>();
            var roomsDataTable = new List<RoomStaticDataRecord>();

            roomsDataTable = await RetrieveJsonOnDisk<List<RoomStaticDataRecord>>("Rooms", countryCode + ".json", async () => await StaticInfoProvider.GetCountryRoomStaticAsync(countryCode));
            foreach (var roomRow in roomsDataTable)
            {
                Room room = new Room();
                room.RoomId = roomRow.RoomId.ToString();

                // We don't have the language in the datatable, we must get it from "roomTypeNames"
                //room.Description = roomRow.RoomTypeName.Trim();
                var roomName = string.Empty;
                if (!roomTypeNames.TryGetValue(roomRow.RoomTypeId, out roomName))
                {
                    LogEntryAndConsoleOutput(XML.HelpClasses.Log.MessageType.Warning, "Room " + roomRow.RoomId + " doesnt have a translation for " + configuration.Language);
                }
                room.Description = roomName;
                room.MealSupplementRestriction = roomRow.MealSupplementRestricted;

                if (processSafePassage)
                    room.HasSafePassage = safePassageRoomIds.Contains(int.Parse(room.RoomId));

                room.BestBuy = roomRow.BestBuy;
                room.NoRefundable = roomRow.NoRefundable;

                string roomTypeId = addRoomTypes(roomRow.RoomTypeId, roomRow.Beds, roomRow.ExtraBeds, room.Description, roomRow.SharedRoom, roomRow.SharedFacilities);
                room.TypeId = roomTypeId.Trim();

                if (!roomsDictionary.ContainsKey(roomRow.HotelId))
                    roomsDictionary.Add(roomRow.HotelId, new List<Room>());
                roomsDictionary[roomRow.HotelId].Add(room);
            }

            return roomsDictionary;
        }
        /// <summary>
        /// Starts the given tasks and waits for them to complete. This will run, at most, the specified number of tasks in parallel.
        /// <para>NOTE: If one of the given tasks has already been started, an exception will be thrown.</para>
        /// </summary>
        /// <param name="tasksToRun">The tasks to run.</param>
        /// <param name="maxTasksToRunInParallel">The maximum number of tasks to run in parallel.</param>
        /// <param name="cancellationToken">The cancellation token.</param>
        public static void StartAndWaitAllThrottled(IEnumerable<Task> tasksToRun, int maxTasksToRunInParallel, CancellationToken cancellationToken = new CancellationToken())
        {
            StartAndWaitAllThrottled(tasksToRun, maxTasksToRunInParallel, -1, cancellationToken);
        }

        /// <summary>
        /// Starts the given tasks and waits for them to complete. This will run, at most, the specified number of tasks in parallel.
        /// <para>NOTE: If one of the given tasks has already been started, an exception will be thrown.</para>
        /// </summary>
        /// <param name="tasksToRun">The tasks to run.</param>
        /// <param name="maxTasksToRunInParallel">The maximum number of tasks to run in parallel.</param>
        /// <param name="timeoutInMilliseconds">The maximum milliseconds we should allow the max tasks to run in parallel before allowing another task to start. Specify -1 to wait indefinitely.</param>
        /// <param name="cancellationToken">The cancellation token.</param>
        public static void StartAndWaitAllThrottled(IEnumerable<Task> tasksToRun, int maxTasksToRunInParallel, int timeoutInMilliseconds, CancellationToken cancellationToken = new CancellationToken())
        {
            // Convert to a list of tasks so that we don&#39;t enumerate over it multiple times needlessly.
            var tasks = tasksToRun.ToList();

            using (var throttler = new SemaphoreSlim(maxTasksToRunInParallel))
            {
                var postTaskTasks = new List<Task>();

                // Have each task notify the throttler when it completes so that it decrements the number of tasks currently running.
                tasks.ForEach(t => postTaskTasks.Add(t.ContinueWith(tsk => throttler.Release())));

                // Start running each task.
                foreach (var task in tasks)
                {
                    // Increment the number of tasks currently running and wait if too many are running.
                    throttler.Wait(timeoutInMilliseconds, cancellationToken);

                    cancellationToken.ThrowIfCancellationRequested();
                    task.Start();
                }

                // Wait for all of the provided tasks to complete.
                // We wait on the list of "post" tasks instead of the original tasks, otherwise there is a potential race condition where the throttler&#39;s using block is exited before some Tasks have had their "post" action completed, which references the throttler, resulting in an exception due to accessing a disposed object.
                Task.WaitAll(postTaskTasks.ToArray(), cancellationToken);
            }
        }
        private async Task<T> RetrieveJsonOnDisk<T>(string folder, string fileName, Func<Task<object>> callback)
        {
            var folderPath = Path.Combine("Cache\\Temp\\", folder);
            var filePath = Path.Combine(folderPath, fileName);
            if (File.Exists(filePath))
            {
                try
                {
                    using (var st = File.OpenText(filePath))
                    {
                        var result = (T)serializer.Deserialize(st, typeof(T));
                        if (result != null)
                            return result;
                    }
                }
                catch (Exception) { }
            }

            var table = await callback();

            if (!Directory.Exists(folderPath))
                Directory.CreateDirectory(folderPath);

            using (var file = File.Open(filePath, FileMode.Create))
            {
                using (var memoryStream = new MemoryStream())
                {
                    using (var writer = new StreamWriter(memoryStream))
                    {
                        var serializer = JsonSerializer.CreateDefault();
                        serializer.Serialize(writer, table);
                        await writer.FlushAsync().ConfigureAwait(false);
                        memoryStream.Seek(0, SeekOrigin.Begin);
                        await memoryStream.CopyToAsync(file).ConfigureAwait(false);
                    }
                }
                await file.FlushAsync().ConfigureAwait(false);
            }

            return (T)table;
        }

        private async Task<Dictionary<int, List<MealLabel>>> GetMealLabels(int countryCode)
        {
            var mealLabels = new List<MealDataRecord>();
            mealLabels = await RetrieveJsonOnDisk<List<MealDataRecord>>("Meals", countryCode + ".json", async () => await StaticInfoProvider.GetMealLabelsByCountryAsync(countryCode));
            return mealLabels.GroupBy(p => p.HotelId).ToDictionary(key => key.Key, r => r.SelectMany(v => new MealLabel[] { new MealLabel(v.MealId, v.MealLabelId) }).ToList());
        }

        #region Notes (country, destination and resort)
        static readonly int COUNTRY_DEST_TYPE = 0;
        static readonly int DESTINATION_DEST_TYPE = 1;
        static readonly int RESORT_DEST_TYPE = 2;
        /// <summary>
        /// Returns notes for all the countries
        /// </summary>
        /// <returns></returns>
        private Dictionary<int, List<Note>> GetCountryNotes()
        {
            return GetDestinationTypeNotes(COUNTRY_DEST_TYPE);
        }
        /// <summary>
        /// Returns notes for all destinations
        /// </summary>
        /// <returns></returns>
        private Dictionary<int, List<Note>> GetDestinationNotes()
        {
            return GetDestinationTypeNotes(DESTINATION_DEST_TYPE);
        }
        /// <summary>
        /// Returns notes for all resorts
        /// </summary>
        /// <returns></returns>
        private Dictionary<int, List<Note>> GetResortNotes()
        {
            return GetDestinationTypeNotes(RESORT_DEST_TYPE);
        }
        /// <summary>
        /// Common destination notes
        /// </summary>
        /// <param name="type">Type of notes: country, destination, resort</param>
        /// <returns></returns>
        private Dictionary<int, List<Note>> GetDestinationTypeNotes(int type)
        {
            var from = DateTime.Today;
            var to = from.AddDays(configuration.NrDaysInCache);
            var today = DateTime.Today;

            using (var countryNotes = new InternalExportDataSetTableAdapters.DestinationNotesTableAdapter().GetData(type, to, from))
            {
                return countryNotes
                        .GroupBy(n => n.DestinationId)
                        .ToDictionary(n => n.Key, n => n.SelectMany(note => Note.Create(note.Id, note.StartDate, note.EndDate, configuration.NrDaysInCache, today)).ToList());
            }


        }
        #endregion

        public HashSet<int> GetHotelsToExport()
        {
            if (string.IsNullOrEmpty(Settings.HotelFilterFile))
                return null;
            string assemblyFolder = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            string sqlFileName = Path.Combine(assemblyFolder, Settings.HotelFilterFile);
            if (!File.Exists(sqlFileName))
                return null;
            var sql = File.ReadAllText(sqlFileName);

            var hotelsToExport = new HashSet<int>();
            using (var connection = new SqlConnection(Properties.Settings.Default.sunhotelsConnectionString))
            {
                using (var command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandText = sql;
                    using (var dataTable = new DataTable())
                    {
                        using (var sqlDataAdapter = new SqlDataAdapter())
                        {
                            sqlDataAdapter.SelectCommand = command;
                            sqlDataAdapter.SelectCommand.CommandTimeout = 6 * 60;//time in seconds
                            sqlDataAdapter.Fill(dataTable);
                        }

                        if (dataTable.Rows.Count > 0)
                        {
                            for (var i = 0; i < dataTable.Rows.Count; i++)
                            {
                                var row = dataTable.Rows[i];
                                var hotelId = (int)row["Hotelid"];
                                hotelsToExport.Add(hotelId);
                            }
                        }
                    }
                }
            }
            return hotelsToExport.Count > 0 ? hotelsToExport : null;
        }
    }
}