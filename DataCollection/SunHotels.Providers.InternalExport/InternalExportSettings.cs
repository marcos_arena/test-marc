﻿using System.Collections.Generic;

namespace SunHotels.Providers
{
    public class InternalExportSettings
    {      
        public string[] RoomProviderNameContains { get; set; }
        public string HotelFilterFile { get; set; }
        public List<int> BlockedHotels { get; set; }
    }

}
