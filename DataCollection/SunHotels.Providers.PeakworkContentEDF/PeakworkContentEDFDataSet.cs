﻿namespace SunHotels.Providers.PeakworkContentEDFDataSetTableAdapters
{
    public partial class hotelStandardTableAdapter
    {
        public string GetCommandText(int num)
        {
            return CommandCollection[num].CommandText;
        }
        public void SetCommandText(int num, string sql)
        {
            CommandCollection[num].CommandText = sql;
        }

        public void SetCommandTimeout(int num, int timeout)
        {
            CommandCollection[num].CommandTimeout = timeout;
        }
    }

    public partial class DestinationsTableAdapter
    {
        public string GetCommandText(int num)
        {
            return CommandCollection[num].CommandText;
        }
        public void SetCommandText(int num, string sql)
        {
            CommandCollection[num].CommandText = sql;
        }

        public void SetCommandTimeout(int num, int timeout)
        {
            CommandCollection[num].CommandTimeout = timeout;
        }
    }

    public partial class ResortsTableAdapter
    {
        public string GetCommandText(int num)
        {
            return CommandCollection[num].CommandText;
        }
        public void SetCommandText(int num, string sql)
        {
            CommandCollection[num].CommandText = sql;
        }
        public void SetCommandTimeout(int num, int timeout)
        {
            CommandCollection[num].CommandTimeout = timeout;
        }
    }
}
namespace SunHotels.Providers
{
    partial class PeakworkContentEDFDataSet
    {
        partial class hotelStandardDataTable
        {
        }
    }
}
