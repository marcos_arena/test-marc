﻿using Sunhotels.Export;
using SunHotels.Export;
using SunHotels.XML.Data;
using System;
using System.Collections.Concurrent;
using System.Globalization;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Schema;

namespace SunHotels.Providers
{
    public class PeakworkContentEDFExporter : BaseFileExporter
    {
        #region Property and Field

        private Root _root;
        private readonly bool _isDeltaFile;
        private readonly PeakworkContentEDFSettings _localConfig;
        private readonly ConcurrentDictionary<string, Hotel> _hotelList = new ConcurrentDictionary<string, Hotel>();

        private readonly ConcurrentDictionary<string, string> _notValidHotels =
            new ConcurrentDictionary<string, string>();

        private readonly ConcurrentDictionary<string, Place> _resortIdDestination =
            new ConcurrentDictionary<string, Place>();

        private XmlReaderSettings _hotelSettings;
        public DateTime? StartDate { get; private set; }
        public DateTime? EndDate { get; private set; }
        public string SeasonStartDate { get; private set; }
        public string SeasonEndDate { get; private set; }
        public string LanguageCode { get; private set; }

        #endregion Property and Field

        #region Constructor

        public PeakworkContentEDFExporter(Configuration configuration, PeakworkContentEDFSettings localConfig,
            bool isDeltaFile) : base(configuration)
        {
            _localConfig = localConfig;
            _isDeltaFile = isDeltaFile;
        }

        #endregion

        #region Override

        public override void Write(ExportData data)
        {
            Console.WriteLine("Write XML EDF Start: " + DateTime.Now);
            _root = data as Root;
            SetDates();
            SeasonStartDate = DateTime.Today.Year - 1 + "-11-01";
            SeasonEndDate = DateTime.Today.Year + 1 + "-10-31";
            LanguageCode = _localConfig.AgentLanguageCode != "ge" ? "EN" : "DE";

            //Loading XSD from the assembly
            var myAssembly = Assembly.GetExecutingAssembly();

            _hotelSettings = new XmlReaderSettings { ValidationType = ValidationType.Schema };
            _hotelSettings.ValidationEventHandler += ValidationEventHandler;
            _hotelSettings.ValidationFlags = XmlSchemaValidationFlags.ReportValidationWarnings;
            //XmlSchema hotelCommonSchema;
            using (var schemaStream = myAssembly.GetManifestResourceStream(_localConfig.HotelXsd))
            {
                if (schemaStream != null)
                {
                    var hotelSchema = XmlSchema.Read(schemaStream, ValidationEventHandler);
                    _hotelSettings.Schemas.Add(hotelSchema);
                }
            }
            _hotelSettings.Schemas.Compile();

            if (_root != null)
            {
                _root.Places.SelectMany(country => country.Places).ToList().ForEach(destination =>
                {
                    destination.Places.ForEach(resort =>
                    {
                        _resortIdDestination.TryAdd(resort.Id, destination);
                    });
                });

                try
                {
                    var cacheOutputDirectory = Path.GetDirectoryName(configuration.CacheOutput);
                    if (Directory.Exists(cacheOutputDirectory))
                        Directory.Delete(cacheOutputDirectory, true);
                }
                catch 
                {
                    // ignored
                }


                // CC-3048
                var processingHotels = new System.Collections.Generic.Dictionary<string, object>();

                var handledHotels = new ConcurrentBag<string>();
                Parallel.ForEach(_root.AllPlaces(), p =>
                {
                    if (p.Hotels == null || p.Hotels.Count == 0)
                        return;

                    Parallel.ForEach(p.Hotels.Values, hotel =>
                    {
                        Thread.CurrentThread.CurrentCulture = Thread.CurrentThread.CurrentUICulture = CultureInfo.InvariantCulture;
                        _hotelList.TryAdd(hotel.Id, hotel);
                        if (handledHotels.Contains(hotel.Id)) return;
                        handledHotels.Add(hotel.Id);
                        if (!configuration.WriteHotelsWithoutRooms) return;
                        if (_isDeltaFile) return;

                        var hotelRoot = string.Format(configuration.CacheOutput, _localConfig.TourOperatorCode, hotel.Id);

                        // Avoid concurrency problems (CC-3048)
                        var hotelLock = new object();
                        if (!processingHotels.TryGetValue(hotelRoot, out hotelLock))
                        {
                            lock (processingHotels)
                            {
                                hotelLock = new object();
                                processingHotels[hotelRoot] = hotelLock;
                            }    
                        }

                        lock (hotelLock)
                            CreateXmlFile(WriteXml, hotelRoot, true);
                    });
                });
            }


            Console.WriteLine("Write Xml EDF Stop: " + DateTime.Now);
            Console.WriteLine("Write ZIP Start: " + DateTime.Now);
            CompressEDF();
            Console.WriteLine("Write ZIP Stop: " + DateTime.Now);
        }


        private void CompressEDF()
        {
            var zipName = _localConfig.ZipName;
            if (string.IsNullOrEmpty(zipName)) zipName = _localConfig.TourOperatorCode;

            var destinationFolder = configuration.CacheOutput.Substring(0, configuration.CacheOutput.IndexOf(configuration.Provider)) + configuration.Provider + @"\";
            var zipFileName = destinationFolder + zipName + ".zip";
            var zipFolder = destinationFolder + @"hotels\";

            if (!Directory.Exists(zipFolder)) return;

            if (File.Exists(zipFileName))
            {
                File.Delete(zipFileName);
            }
            ZipFile.CreateFromDirectory(zipFolder, zipFileName, CompressionLevel.Optimal, true);
            Directory.Delete(zipFolder, true);

            var fileDone = File.Create(destinationFolder + zipName + ".done");
            fileDone.WriteByte(Convert.ToByte(1));
            fileDone.Close();
        }

        public override void Validate()
        {
            //XML already validated 'WriteCacheXML' and 'WriteAvailabilityCacheXML'
            if (_notValidHotels.Count > 0)
            {
                Console.WriteLine("The following hotels can't be exported:");
                foreach (var key in _notValidHotels.Keys)
                    Console.WriteLine(key + ": " + _notValidHotels[key]);
                throw new ApplicationException("Hotels with errors");
            }
            Console.WriteLine("All hotels exported successfully to EDF file format.");
        }

        #endregion Override

        #region XML Creation functions

        /// <summary>
        /// Gets the hotel id from the file name
        /// </summary>
        /// <param name="writer"></param>
        /// <returns></returns>
        private string WriterHotelId(XmlTextWriter writer)
        {
            var indexFileName = _localConfig.TourOperatorCode + "-";
            var file = writer.BaseStream as FileStream;
            if (file == null) return string.Empty;
            var idHotelIni = file.Name.LastIndexOf(indexFileName, StringComparison.Ordinal) + indexFileName.Length;
            var idHotelEnd = file.Name.LastIndexOf(".", StringComparison.Ordinal);
            return file.Name.Substring(idHotelIni, idHotelEnd - idHotelIni);
        }

        /// <summary>
        /// Write XML File
        /// </summary>
        /// <param name="writer"></param>
        private void WriteXml(XmlTextWriter writer)
        {
            var idHotel = WriterHotelId(writer);
            Hotel currentHotel;
            _hotelList.TryGetValue(idHotel, out currentHotel);
            if (currentHotel == null)
                return;

            //EDF document
            var doc = new XmlDocument();
            var nHotelRoot = doc.CreateElement("ContentRoot");
            nHotelRoot.SetAttribute("xmlns", _localConfig.HotelRootNamespace);
            nHotelRoot.SetAttribute("SchemaVersion", _localConfig.EdfVersionHotel);
            nHotelRoot.SetAttribute("CreationDate", DateTime.Now.ToString("s") + DateTime.Now.ToString("zzz"));
            doc.AppendChild(nHotelRoot);

            #region BasicData

            var nBasicData = doc.CreateElement("BasicData");
            nBasicData.SetAttribute("Code", currentHotel.Id);
            nBasicData.SetAttribute("TourOperatorCode", _localConfig.TourOperatorCode);
            nBasicData.SetAttribute("TourOperatorName", _localConfig.TourOperatorName);
            nBasicData.SetAttribute("GlobalType", GetGlobalTypeBasedOnAccomodationType(currentHotel.AccomodationType));
            nHotelRoot.AppendChild(nBasicData);

            //Description
            var nDescriptiveData = doc.CreateElement("DescriptiveData");
            var nDescriptions = doc.CreateElement("Descriptions");
            if (currentHotel.AdultOnly)
            {
                var nGlobalTypes = doc.CreateElement("GlobalTypes");
                var nGlobalType = doc.CreateElement("GlobalType");
                var nGTAttribute = doc.CreateElement("GTAttribute");
                nGTAttribute.SetAttribute("GT", "GT03-ADON");
                nGlobalType.AppendChild(nGTAttribute);
                nGlobalTypes.AppendChild(nGlobalType);
                nDescriptiveData.AppendChild(nGlobalTypes);
            }

            if (currentHotel.Translations?.description != null && currentHotel.Translations.description.Count > 0)
            {
                foreach (var description in currentHotel.Translations.description)
                {
                    var nDescription = doc.CreateElement("Description");
                    nDescription.SetAttribute("Language", description.lang);
                    var nParagraphs = doc.CreateElement("Paragraphs");
                    var nParagraph = doc.CreateElement("Paragraph");
                    nParagraph.SetAttribute("Category", _localConfig.ParagraphCategory);
                    var nText = doc.CreateElement("Text");
                    nText.AppendChild(doc.CreateTextNode(description.Value));
                    nParagraph.AppendChild(nText);
                    nParagraphs.AppendChild(nParagraph);
                    nDescription.AppendChild(nParagraphs);
                    nDescriptions.AppendChild(nDescription);
                }
            }
            nDescriptiveData.AppendChild(nDescriptions);

            //MultiMedia
            var nMultiMedia = doc.CreateElement("MultiMedia");
            var nImages = doc.CreateElement("Images");
            var nMultiMediaLinks = doc.CreateElement("MultiMediaLinks");
            if (currentHotel.Images?.Any() == true)
            {
                foreach (var image in currentHotel.Images.OrderBy(c => int.Parse(c.Id)))
                {
                    const int index = 1;
                    //Pick just the higher resolution image (avoid thumbnails
                    var variant = image.ImageVariants.OrderByDescending(x=>x.Height).First();

                    var id = image.Id + "-" + index;

                    var nMultiMediaLink = doc.CreateElement("MultiMediaLink");
                    nMultiMediaLink.SetAttribute("ID", id);
                    nMultiMediaLink.SetAttribute("Type", _localConfig.MultiMediaLinkType);
                    nMultiMediaLinks.AppendChild(nMultiMediaLink);

                    var nImage = doc.CreateElement("Image");
                    nImage.SetAttribute("ID", id);
                    var nUrl = doc.CreateElement("URL");
                    nUrl.AppendChild(doc.CreateTextNode(variant.URL));
                    nImage.AppendChild(nUrl);
                    if (variant.Height.HasValue && variant.Height.Value > 0
                        && variant.Width.HasValue && variant.Width.Value > 0)
                    {
                        var nMultiMediaAttributes = doc.CreateElement("MultiMediaAttributes");
                        var nResolution = doc.CreateElement("Resolution");
                        nResolution.SetAttribute("Height", variant.Height.ToString());
                        nResolution.SetAttribute("Width", variant.Width.ToString());
                        nMultiMediaAttributes.AppendChild(nResolution);
                        nImage.AppendChild(nMultiMediaAttributes);
                    }
                    nImages.AppendChild(nImage);
                }

                int sortIndex = 1;
                foreach (XmlElement image in nImages.ChildNodes)
                {
                    image.SetAttribute("SortIndex", sortIndex.ToString());
                    sortIndex++;
                }
                nMultiMedia.AppendChild(nImages);
                nDescriptiveData.AppendChild(nMultiMediaLinks);
            }

            // Features
            var nFeatureLinks = doc.CreateElement("FeatureLinks");
            var nFeatures = doc.CreateElement("Features");
            if (currentHotel.Features?.Any() == true)
            {
                foreach (var feature in currentHotel.Features.OrderBy(c => int.Parse(c.Id)))
                {
                    var nFeatureLink = doc.CreateElement("FeatureLink");
                    nFeatureLink.SetAttribute("ID", feature.Id);
                    nFeatureLinks.AppendChild(nFeatureLink);

                    var nFeature = doc.CreateElement("Feature");
                    nFeature.SetAttribute("ID", feature.Id);

                    var nFeatureName = doc.CreateElement("Name");
                    nFeatureName.AppendChild(doc.CreateTextNode(feature.Name));
                    nFeature.AppendChild(nFeatureName);
                    nFeatures.AppendChild(nFeature);
                }
                nDescriptiveData.AppendChild(nFeatureLinks);
            }
            //Reviews (Star rating)
            if (!string.IsNullOrEmpty(currentHotel.Classification))
            {
                var nReviews = doc.CreateElement("Reviews");
                var nReview = doc.CreateElement("Review");
                nReview.SetAttribute("Type", _localConfig.ReviewType);
                var nReviewValue = doc.CreateElement("Value");
                nReviewValue.AppendChild(doc.CreateTextNode(currentHotel.Classification));
                nReview.AppendChild(nReviewValue);
                nReviews.AppendChild(nReview);
                nDescriptiveData.AppendChild(nReviews);
            }

            nHotelRoot.AppendChild(nDescriptiveData);
            if (currentHotel.Images?.Any() == true)
                nHotelRoot.AppendChild(nMultiMedia);
            if (currentHotel.Features?.Any() == true)
                nHotelRoot.AppendChild(nFeatures);

            //Hotel's name
            var nName = doc.CreateElement("Name");
            nName.AppendChild(doc.CreateTextNode(currentHotel.Name));
            nBasicData.AppendChild(nName);

            var nReferences = doc.CreateElement("References");
            var nGiataCode = doc.CreateElement("ID");
            nGiataCode.SetAttribute("Type", _localConfig.ReferencesType);
            nGiataCode.AppendChild(doc.CreateTextNode(currentHotel.Identifiers[0].Value));
            nReferences.AppendChild(nGiataCode);
            nBasicData.AppendChild(nReferences);

            //Hotel's address
            if (
                !string.IsNullOrEmpty(currentHotel.Adress_Street1)
                || !string.IsNullOrEmpty(currentHotel.Adress_Street2)
                || !string.IsNullOrEmpty(currentHotel.Adress_Zipcode)
                || !string.IsNullOrEmpty(currentHotel.Adress_City)
                || !string.IsNullOrEmpty(currentHotel.Adress_Country)
                || !string.IsNullOrEmpty(currentHotel.Adress_State)
                || !string.IsNullOrEmpty(currentHotel.Phone)
                || !string.IsNullOrEmpty(currentHotel.Fax)
            )
            {
                var nAddress = doc.CreateElement("Address");

                if (!string.IsNullOrEmpty(currentHotel.Adress_Street1)
                    || !string.IsNullOrEmpty(currentHotel.Adress_Street2))
                {
                    var nStreet = doc.CreateElement("Street");
                    var street = currentHotel.Adress_Street1 + (!string.IsNullOrEmpty(currentHotel.Adress_Street2) ? " " + currentHotel.Adress_Street2 : "");
                    nStreet.AppendChild(doc.CreateTextNode(GetXmlSafeOutput(street)));
                    nAddress.AppendChild(nStreet);
                }

                if (!string.IsNullOrEmpty(currentHotel.Adress_Zipcode))
                {
                    var nZipCode = doc.CreateElement("ZipCode");
                    nZipCode.AppendChild(doc.CreateTextNode(currentHotel.Adress_Zipcode));
                    nAddress.AppendChild(nZipCode);
                }

                if (!string.IsNullOrEmpty(currentHotel.Adress_City))
                {
                    var nCity = doc.CreateElement("City");
                    nCity.AppendChild(doc.CreateTextNode(GetXmlSafeOutput(currentHotel.Adress_City)));
                    nAddress.AppendChild(nCity);
                }

                if (!string.IsNullOrEmpty(currentHotel.Adress_Country))
                {
                    var nCountry = doc.CreateElement("Country");
                    nCountry.AppendChild(doc.CreateTextNode(GetXmlSafeOutput(currentHotel.Adress_Country)));
                    nAddress.AppendChild(nCountry);
                }

                if (!string.IsNullOrEmpty(currentHotel.Phone))
                {
                    var nPhone = doc.CreateElement("Phone");
                    nPhone.AppendChild(doc.CreateTextNode(currentHotel.Phone));
                    nAddress.AppendChild(nPhone);
                }

                if (!string.IsNullOrEmpty(currentHotel.Fax))
                {
                    var nFax = doc.CreateElement("Fax");
                    nFax.AppendChild(doc.CreateTextNode(currentHotel.Fax));
                    nAddress.AppendChild(nFax);
                }

                if (!string.IsNullOrEmpty(currentHotel.Adress_State))
                {
                    var nState = doc.CreateElement("State");
                    nState.AppendChild(doc.CreateTextNode(currentHotel.Adress_State));
                    nAddress.AppendChild(nState);
                }

                nBasicData.AppendChild(nAddress);
            }

            //Hotel's location
            if (
                !string.IsNullOrEmpty(currentHotel.Adress_State)
                || !string.IsNullOrEmpty(currentHotel.Position_Latitude)
                || !string.IsNullOrEmpty(currentHotel.Position_Longitude)
            )
            {
                var nGeoInfos = doc.CreateElement("GeoInfos");
                //Location
                if (!string.IsNullOrEmpty(currentHotel.Adress_State))
                {
                    var nLocation = doc.CreateElement("Location");
                    nLocation.SetAttribute("Name", GetXmlSafeOutput(currentHotel.Adress_State));
                    nLocation.SetAttribute("Type", _localConfig.LocationType);
                    nGeoInfos.AppendChild(nLocation);
                }
                //GeoCode
                if (!string.IsNullOrEmpty(currentHotel.Position_Latitude) || !string.IsNullOrEmpty(currentHotel.Position_Longitude))
                {
                    var nGeoCode = doc.CreateElement("GeoCode");
                    if (!string.IsNullOrEmpty(currentHotel.Position_Latitude))
                        nGeoCode.SetAttribute("Latitude", currentHotel.Position_Latitude);
                    if (!string.IsNullOrEmpty(currentHotel.Position_Longitude))
                        nGeoCode.SetAttribute("Longitude", currentHotel.Position_Longitude);
                    nGeoInfos.AppendChild(nGeoCode);
                }

                nBasicData.AppendChild(nGeoInfos);
            }

            //Airports
            Place hotelDestination;
            if (_resortIdDestination.TryGetValue(currentHotel.PlaceId, out hotelDestination) && hotelDestination.Codes?.Any(c => c.Type == "IATA") == true)
            {
                var nArrivalAirports = doc.CreateElement("Airports");
                foreach (var code in hotelDestination.Codes.Where(c => c.Type == _localConfig.DestinationPlaceCodesType))
                {
                    var nAirport = doc.CreateElement("Airport");
                    nAirport.SetAttribute("IataCode", code.Value);
                    nArrivalAirports.AppendChild(nAirport);
                }
                nBasicData.AppendChild(nArrivalAirports);
            }

            #endregion

            //Validate hotel using the temporal document
            try
            {
                var xmlRaw = doc.OuterXml;
                using (var validatingReader = XmlReader.Create(new StringReader(xmlRaw), _hotelSettings))
                    while (validatingReader.Read()) { /* just loop through document */ }
                writer.WriteRaw(xmlRaw);
            }
            catch (Exception e)
            {
                if (typeof(ApplicationException) == e.GetType() || typeof(XmlSchemaValidationException) == e.GetType())
                    _notValidHotels.TryAdd(currentHotel.Name + "(" + currentHotel.Id + ")", "(CacheXML) - " + e.Message);
                else
                    Console.WriteLine(e.StackTrace);
            }
        }

        #endregion XML Creation functions

        #region Utilities 

        /// <summary>
        /// Sets the start date and the end date and limits the exported data
        /// </summary>
        private void SetDates()
        {
            if (_root.AffectedStartDateAdjustment.HasValue)
                StartDate = DateTime.Now.Date.AddDays(_root.AffectedStartDateAdjustment.Value);
            if (_root.AffectedEndDateAdjustment.HasValue)
                EndDate = DateTime.Now.Date.AddDays(_root.AffectedEndDateAdjustment.Value);
        }

        /// <summary>
        /// Get Safe XML using HttpUtility HtmlDecode method
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        private static string GetXmlSafeOutput(string value)
        {
            if (string.IsNullOrEmpty(value)) return string.Empty;
            var xmlSafeOutput = System.Web.HttpUtility.HtmlDecode(value);
            return xmlSafeOutput.Trim();
        }

        /// <summary>
        /// Get GlobalType based on accommodation type
        /// </summary>
        /// <param name="accomodationType"></param>
        /// <returns></returns>
        private static string GetGlobalTypeBasedOnAccomodationType(AccomodationType? accomodationType)
        {
            switch (accomodationType)
            {
                case AccomodationType.Hotel: return "HO";
                case AccomodationType.Apartment: return "AP";
                case AccomodationType.Villa: return "VH";
                case null:
                    return "HO";
                default: return "HO";
            }
        }

        #endregion Utilities 
    }
}
