﻿using SunHotels.Export;
using SunHotels.Providers.PeakworkContentEDFDataSetTableAdapters;
using SunHotels.Providers.ProviderCommon.DataLoader;
using SunHotels.XML;
using SunHotels.XML.Data;
using SunHotels.XML.Optimize;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace SunHotels.Providers
{
    public class PeakworkContentEDF : XMLProvider
    {
        #region Property and Field

        private const int GeneralTimeout = 45 * 60;
        private const int QueryElements = 1000;
        private bool IsDeltaFile => configuration.IsDeltaFile;
        private BaseOptimizer _optimizer;
        private BaseFileExporter _exporter;
        private readonly Dictionary<int, Hotel> _hotelSearch = new Dictionary<int, Hotel>();
        private ConcurrentBag<string> _languages;
        private PeakworkContentEDFSettings _localConfig;
        protected PeakworkContentEDFSettings Settings
        {
            get
            {
                if (_localConfig != null) return _localConfig;
                var providerConfigFullPath = configuration.ExportConfigFileFullPath;
                if (File.Exists(providerConfigFullPath))
                {
                    var serializer = new XmlSerializer(typeof(PeakworkContentEDFSettings));
                    var fs = new FileStream(providerConfigFullPath, FileMode.Open, FileAccess.Read);
                    _localConfig = (PeakworkContentEDFSettings)serializer.Deserialize(fs);
                }
                else
                    _localConfig = new PeakworkContentEDFSettings();
                return _localConfig;
            }
        }
        public static PeakworkContentEDFDataSet.agentRow Agent { get; private set; }

        #endregion Property and Field

        #region override Method

        protected override BaseOptimizer Optimizer
           => _optimizer ?? (_optimizer = new PeakworkContentEDFOptimizer(configuration));
        protected override BaseFileExporter Exporter
          => _exporter ?? (_exporter = new PeakworkContentEDFExporter(configuration, Settings, IsDeltaFile));

        /// <summary>
        /// Get Provider Definition
        /// </summary>
        /// <param name="rootData">Root</param>
        protected override void getProviderDefinition(Root rootData)
        {
            rootData.ProviderDefinition.Name = configuration.Provider;
            rootData.ProviderDefinition.Currency = configuration.Currency;
            configuration.WriteHotelsWithoutRooms = true;
            configuration.WriteRoomsWithoutAvailability = true;
        }

        /// <summary>
        /// Get Global Feature mapping for the provider
        /// </summary>
        /// <param name="rootData">Root</param>
        protected override void getFeatureTypes(Root rootData)
        {
            try
            {
                // Get SH features mapped with the provider
                DataMapper?.SunHotelsFeatures?.Join(DataMapper["Feature"], o => o.Key, i => i.Key, (o, i) => o)
                    .All(f =>
                    {
                        rootData.Features.Add(new Feature
                        {
                            Id = f.Key,
                            Value = f.Value,
                            Name = f.Value
                        });
                        return true;
                    });
            }
            catch (Exception ex)
            {
                Log(ILoggerStaticDataSeverity.Error, $"Error while getting feature types:{ex.Message}");
            }
        }

        /// <summary>
        /// Get Data
        /// </summary>
        /// <param name="rootData">Root</param>
        protected override void getData(Root rootData)
        {
            // Create log stream
            try
            {
                Directory.CreateDirectory(configuration.LogOutput);
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Unable to create PeakworkContentEDF log file", ex);
            }
            try
            {
                var dStart = DateTime.Now;
                Log(ILoggerStaticDataSeverity.Info, "Start : {0}", dStart);

                Thread.CurrentThread.CurrentCulture =
                    Thread.CurrentThread.CurrentUICulture = CultureInfo.InvariantCulture;

                var agent = new agentTableAdapter().GetData(int.Parse(configuration.UserId));
                if (agent == null || agent.Rows.Count != 1)
                    throw new Exception($"Agent with id {configuration.UserId} not found.");
                Agent = agent[0];
                Settings.AgentLanguageCode = Agent.cc != "ge" ? "en" : Agent.cc;

                Log(ILoggerStaticDataSeverity.Info, "Fetching countries, destinations and resorts started");
                GetPlaces(rootData);
                Log(ILoggerStaticDataSeverity.Info, "Fetching countries, destinations and resorts done");

                Log(ILoggerStaticDataSeverity.Info, "Fetching Hotels started");

                _languages = GetLanguages();

                var features = new ConcurrentDictionary<string, KeyValuePair<string, string>>();
                DataMapper.SunHotelsFeatures.Join(DataMapper["Feature"], o => o.Key, i => i.Key,
                        (o, i) => new { SHFeature = o, ProviderFeature = i })
                    .SelectMany(o => o.ProviderFeature.Value.Select(of => new { ProviderId = of, o.SHFeature }))
                    .ToList().ForEach(o => { features.TryAdd(o.ProviderId, o.SHFeature); });

                GetHotels(rootData, features);
                Log(ILoggerStaticDataSeverity.Info, "Fetching Hotels done");
                Log(ILoggerStaticDataSeverity.Info, "End : {0}", DateTime.Now);
            }
            catch (Exception ex)
            {
                Log(ILoggerStaticDataSeverity.Error, $"Error while getting data:{ex.Message}");
            }
        }

        #endregion override Method

        #region Void

        /// <summary>
        /// Get Hotels
        /// </summary>
        /// <param name="dataRoot">Root</param>
        /// <param name="features">features Dictionary</param>
        private void GetHotels(Root dataRoot, IReadOnlyDictionary<string, KeyValuePair<string, string>> features)
        {
            try
            {
                var bHotels = !string.IsNullOrEmpty(configuration.BlockHotels)
                    ? configuration.BlockHotels.Split(',').Select(int.Parse).ToList()
                    : new List<int>();
                if (bHotels.Count > 0)
                    Settings.BlockedHotels.AddRange(bHotels);

                var hotelsdata = new hotelTableAdapter()
                    .GetData(configuration.SpecificDestinations, int.Parse(configuration.UserId), Agent.isPushOut);

                var giataCodes = GiataHotelCodes();
                if (giataCodes == null)
                    return;
                var hotelsSuspended = HotelsSuspended();
                var resorts = dataRoot.Places
                    .SelectMany(c => c.Places).SelectMany(d => d.Places).ToDictionary(el => el.Id, el => el);

                foreach (var hotelRow in hotelsdata)
                {
                    var hotelId = hotelRow.id;
                    if (hotelsSuspended.Contains(hotelId)) continue;
                    var placeId = hotelRow.resort.ToString();
                    if (Settings.BlockedHotels.Contains(hotelId)) continue;
                    Place currentPlace;
                    if (!resorts.TryGetValue(placeId, out currentPlace)) continue;
                    if (!giataCodes.ContainsKey(hotelId)) continue;
                    var giataCode = giataCodes[hotelId];
                    if (hotelRow.ShowinEN <= 0) continue;

                    var idAsString = hotelId.ToString(CultureInfo.InvariantCulture);
                    var hotel = new Hotel { Id = idAsString, Name = hotelRow.hotelName, PlaceId = placeId };
                    currentPlace.Hotels.Add(idAsString, hotel);
                    hotel.Identifiers.Add(new Identifier { Type = Settings.ReferencesType, Value = giataCode });
                    _hotelSearch.Add(hotelId, hotel);
                }
                var hotelsFilteredByRoomProvider = HotelsFilteredByRoomProvider(root.AllHotels());

                var hotelList = hotelsFilteredByRoomProvider.Keys;
                var resortsWithHotels = resorts.Values.Where(r => r.Hotels.Count > 0);
                foreach (var resort in resortsWithHotels)
                {
                    resort.Hotels.Keys
                        .Where(hotelId => !hotelList.Contains(hotelId))
                        .ToList()
                        .ForEach(hotelId => {
                            resort.Hotels.Remove(hotelId);
                            _hotelSearch.Remove(Int32.Parse(hotelId));
                        });
                }
                int hotelcount = root.AllHotels().Count();
                Log(ILoggerStaticDataSeverity.Info, "START -> Standard Data Processing-> {0}", DateTime.Now);
                GetStandardData(features);
                Log(ILoggerStaticDataSeverity.Info, "END -> Standard Data Processing -> {0}", DateTime.Now);
                Log(ILoggerStaticDataSeverity.Info, "Total Number of Hotels Processed -> " + hotelcount);
            }
            catch (Exception ex)
            {
                Log(ILoggerStaticDataSeverity.Error, $"Error while getting hotels:{ex.Message}");
            }
        }

        private Dictionary<string, List<string>> HotelsFilteredByRoomProvider(IEnumerable<Hotel> allHotels)
        {
            var agentRoomProviderBlocks = AgentRoomProviderBlocks(Agent.id);
            var roomProviderMarketBlocks = RoomProviderMarketBlocks(Agent.countryId);
            var b2CRoomProviderBlocks = Agent.B2C
                    ? B2CRoomProviderBlocks()
                    : new List<int>();
            var roomProviderBlocks = agentRoomProviderBlocks
                    .Concat(roomProviderMarketBlocks)
                    .Concat(b2CRoomProviderBlocks)
                    .Distinct()
                    .ToList();
            return GetHotelRooms(allHotels, roomProviderBlocks);
        }

        private Dictionary<string, List<string>> GetHotelRooms(IEnumerable<Hotel> allHotels, List<int> roomProviderBlocks)
        {
            var hotelRooms = new Dictionary<string, List<string>>();
            foreach (var hotelList in MultipleQueryLists(allHotels.Select(h => h.Id)))
            {
                using (var connection = new SqlConnection(Properties.Settings.Default.sunhotelsConnectionString))
                {
                    using (var command = new SqlCommand())
                    {
                        command.Connection = connection;
                        command.CommandText =
                            @"SELECT r.ID, r.hotelId, r.roomprovider_id
                            FROM rooms r WITH (NOLOCK)
                            WHERE
                                r.isLiveRoom = 0
                                AND ISNULL(r.SuspendRoom, 0) = 0
                                AND ISNULL(r.deleted, 0) = 0
                                AND ISNULL(r.roomprovider_id, 0) > 0
                                AND r.hotelId IN (" + hotelList + @")
                                AND EXISTS(
                                        SELECT TOP 1 c.datum
                                        FROM calendar c WITH(NOLOCK)
                                        WHERE
                                            r.ID = c.roomId
                                            AND c.datum > (GETDATE() - 1)
				                            AND (c.units - c.bookedUnits) > 0
                                )";
                        connection.Open();
                        using (var rdr = command.ExecuteReader(CommandBehavior.SequentialAccess))
                        {
                            while (rdr.Read())
                            {
                                var row = rdr;
                                var roomId = row["ID"].ToString();
                                var hotelId = row["hotelId"].ToString();
                                var roomproviderId = (int)row["roomprovider_id"];
                                if (!roomProviderBlocks.Contains(roomproviderId))
                                {
                                    List<string> roomList;
                                    if (!hotelRooms.TryGetValue(hotelId, out roomList))
                                    {
                                        roomList = new List<string>();
                                        hotelRooms.Add(hotelId, roomList);
                                    }
                                    if (!roomList.Contains(roomId))
                                    {
                                        roomList.Add(roomId);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return hotelRooms;
        }

        private List<int> B2CRoomProviderBlocks()
        {
            var b2CRoomProviderBlocks = new List<int>();
            using (var connection = new SqlConnection(Properties.Settings.Default.sunhotelsConnectionString))
            {
                using (var command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandText = "SELECT id FROM roomprovider where blockB2CAgents = 1";
                    connection.Open();
                    using (var rdr = command.ExecuteReader(CommandBehavior.SequentialAccess))
                    {
                        while (rdr.Read())
                        {
                            var row = rdr;
                            var roomProviderId = (int)row["id"];
                            if (!b2CRoomProviderBlocks.Contains(roomProviderId))
                            {
                                b2CRoomProviderBlocks.Add(roomProviderId);
                            }
                        }
                    }
                }
            }
            return b2CRoomProviderBlocks;
        }

        private List<int> RoomProviderMarketBlocks(int agentCountryId)
        {
            var roomProviderMarketBlocks = new List<int>();
            using (var connection = new SqlConnection(Properties.Settings.Default.sunhotelsConnectionString))
            {
                using (var command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandText = "EXEC spWSDL_GetRoomProviderMarketBlocks";
                    connection.Open();
                    using (var rdr = command.ExecuteReader(CommandBehavior.SequentialAccess))
                    {
                        while (rdr.Read())
                        {
                            var row = rdr;
                            var roomProviderId = (int)row["roomProviderId"];
                            var countryId = (int)row["countryId"];

                            if (agentCountryId <= 0 || agentCountryId == countryId)
                            {
                                if (!roomProviderMarketBlocks.Contains(roomProviderId))
                                {
                                    roomProviderMarketBlocks.Add(roomProviderId);
                                }
                            }
                        }
                    }
                }
            }
            return roomProviderMarketBlocks;
        }

        private List<int> AgentRoomProviderBlocks(int agentId)
        {
            var agentRoomProviderBlocks = new List<int>();
            using (var connection = new SqlConnection(Properties.Settings.Default.sunhotelsConnectionString))
            {
                using (var command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandText = "SELECT roomproviderid FROM agentroomprovider_block WHERE agentid = " + agentId.ToString();
                    connection.Open();
                    using (var rdr = command.ExecuteReader(CommandBehavior.SequentialAccess))
                    {
                        while (rdr.Read())
                        {
                            var row = rdr;
                            var roomProviderId = (int)row["roomproviderid"];
                            if (!agentRoomProviderBlocks.Contains(roomProviderId))
                            {
                                agentRoomProviderBlocks.Add(roomProviderId);
                            }
                        }
                    }
                }
            }
            return agentRoomProviderBlocks;
        }


        /// <summary>
        /// Get Standard Data
        /// </summary>
        /// <param name="features">features Dictionary</param>
        private void GetStandardData(IReadOnlyDictionary<string, KeyValuePair<string, string>> features)
        {
            try
            {
                foreach (var hotelList in MultipleQueryLists(root.AllHotels().Select(h => h.Id)))
                {
                    var hotelStandard = new hotelStandardTableAdapter();
                    hotelStandard.SetCommandTimeout(0, GeneralTimeout);
                    var command = hotelStandard.GetCommandText(0)
                        .Replace("'replaceToken' = 'replaceToken2'", "h.id IN (" + hotelList + ")");
                    hotelStandard.SetCommandText(0, command);
                    var hotelStaticContent = hotelStandard.GetData();

                    var parallelOptions = new ParallelOptions
                    {
                        MaxDegreeOfParallelism = configuration.MaxDegreeOfParallelism
                    };
                    Parallel.ForEach(hotelStaticContent, parallelOptions, hotelRow =>
                    {
                        Hotel hotel;

                        lock (_hotelSearch)
                        {
                            if (!_hotelSearch.TryGetValue(hotelRow.id, out hotel))
                                return;
                        }

                        PrepareHotelBasicData(hotelRow, hotel);
                        PrepareHotelTranslations(hotelRow, hotel);
                        PrepareHotelFeatures(features, hotelRow, hotel);
                        PrepareHotelImages(hotelRow, hotel);
                        try
                        {
                            if (!hotelRow.IsGLatNull() && !hotelRow.IsGLngNull())
                            {
                                hotel.Position_Latitude = hotelRow.GLat.ToString(CultureInfo.InvariantCulture);
                                hotel.Position_Longitude = hotelRow.GLng.ToString(CultureInfo.InvariantCulture);
                            }
                        }
                        catch
                        {
                            Log(ILoggerStaticDataSeverity.Error, "Longitude or latitude DBNull on hotel with id: {0}",
                                hotel.Id);
                        }
                    });
                }
            }
            catch (Exception ex)
            {
                Log(ILoggerStaticDataSeverity.Error, $"Error while getting standard data:{ex.Message}");
            }

        }

        /// <summary>
        /// Preparing Hotel Basic Data
        /// </summary>
        /// <param name="hotelRow">hotelStandardRow</param>
        /// <param name="hotel">Hotel</param>
        private void PrepareHotelBasicData(PeakworkContentEDFDataSet.hotelStandardRow hotelRow, Hotel hotel)
        {
            try
            {
                if (hotelRow.BlockInfant)
                    hotel.BlockInfant = hotelRow.BlockInfant;
                if (hotelRow.AdultOnly)
                    hotel.AdultOnly = hotelRow.AdultOnly;

                hotel.Adress_Street1 = hotelRow.addr_1.Trim();
                hotel.Adress_Street2 = hotelRow.addr_2.Trim();
                hotel.Adress_City = hotelRow.addr_city.Trim();
                hotel.Adress_State = hotelRow.addr_state.Trim();
                hotel.Adress_Country = hotelRow.addr_country.Trim();
                hotel.Adress_Zipcode = hotelRow.addr_zip.Trim();
                hotel.Classification = hotelRow.sunClass.Trim();
                hotel.Fax = hotelRow.fax.Trim();
                hotel.Phone = hotelRow.telephone.Trim();
                hotel.AccomodationType = !string.IsNullOrEmpty(hotelRow.accType)
                    ? (AccomodationType)Enum.Parse(typeof(AccomodationType), hotelRow.accType.Trim())
                    : AccomodationType.Hotel;
            }
            catch (Exception ex)
            {
                Log(ILoggerStaticDataSeverity.Error, $"Error while getting hotel[HotelId:{hotel.Id}] basic data:{ex.Message}");
            }
        }

        /// <summary>
        /// Preparing Hotel Translations
        /// </summary>
        /// <param name="hotelRow">hotelStandardRow</param>
        /// <param name="hotel">Hotel</param>
        private void PrepareHotelTranslations(DataRow hotelRow, Hotel hotel)
        {
            try
            {
                var hotelTranslations = new List<TranslationsDescription>();
                foreach (var lan in _languages)
                {
                    hotelTranslations.Add(new TranslationsDescription { lang = lan, Value = hotelRow["HotelDescription" + lan.ToUpper()].ToString() });
                }
                hotel.Translations.description = hotelTranslations;
            }
            catch (Exception ex)
            {
                Log(ILoggerStaticDataSeverity.Error, $"Error while getting hotel[HotelId:{hotel.Id}] translations:{ex.Message}");
            }
        }

        /// <summary>
        /// Preparing Hotel Features
        /// </summary>
        /// <param name="features">sunhotels features list </param>
        /// <param name="hotelRow">hotelStandardRow</param>
        /// <param name="hotel">Hotel</param>
        private void PrepareHotelFeatures(IReadOnlyDictionary<string, KeyValuePair<string, string>> features,
            PeakworkContentEDFDataSet.hotelStandardRow hotelRow, Hotel hotel)
        {
            try
            {
                if (string.IsNullOrEmpty(hotelRow.features) || hotelRow.features.Split(',').Length <= 0) return;
                var featureList = new List<Feature>();
                var hotelfeatures = hotelRow.features.Split(',');
                foreach(var c in hotelfeatures)
                {
                    KeyValuePair<string, string> shfeature;
                    if (!features.TryGetValue(c, out shfeature)) continue;

                    if (featureList.All(f => f.Id != shfeature.Key))
                        featureList.Add(new Feature
                        {
                            Id = shfeature.Key,
                            Name = shfeature.Value,
                            Value = shfeature.Value
                        });
                }
                hotel.Features = featureList;
            }
            catch (Exception ex)
            {
                Log(ILoggerStaticDataSeverity.Error, $"Error while getting hotel[HotelId:{hotel.Id}] features:{ex.Message}");
            }
        }

        /// <summary>
        /// Preparing the Hotel Images
        /// </summary>
        /// <param name="hotelRow">hotelStandardRow</param>
        /// <param name="hotel">Hotel</param>
        private void PrepareHotelImages(PeakworkContentEDFDataSet.hotelStandardRow hotelRow, Hotel hotel)
        {
            try
            {
                using (var hotelPicturesAdapter = new hotelPicturesTableAdapter())
                {
                    var hotelPictures = hotelPicturesAdapter.GetData(hotelRow.id);
                    var hotelimages = new List<Hotel.Image>();
                    foreach (var picRow in hotelPictures)
                    {
                        int? width = null;
                        int? height = null;
                        if (picRow.SmallWidth != 0 && picRow.SmallHeight != 0)
                        {
                            width = picRow.SmallWidth;
                            height = picRow.SmallHeight;
                        }
                        var image = new Hotel.Image
                        {
                            Id = picRow.ID.ToString(),
                            ImageVariants = new[] {
                                    new Hotel.ImageVariant
                                    {
                                        URL = _localConfig.SmallImageUrlPrefix+ picRow.ID,
                                        Height = height,
                                        Width = width
                                    }
                            }.ToList()
                        };

                        if (picRow.LargeID != 0)
                        {
                            int? largeWidth = null;
                            int? largeheight = null;
                            if (picRow.LargeWidth != 0 && picRow.LargeHeight != 0)
                            {
                                largeWidth = picRow.LargeWidth;
                                largeheight = picRow.LargeHeight;
                            }
                            image.ImageVariants.Add(new Hotel.ImageVariant
                            {
                                URL = _localConfig.LargeImageUrlPrefix + picRow.ID,
                                Width = largeWidth,
                                Height = largeheight
                            });
                        }
                        hotelimages.Add(image);
                    }
                    hotel.Images = hotelimages;
                }
            }
            catch (Exception ex)
            {
                Log(ILoggerStaticDataSeverity.Error, $"Error while getting hotel[HotelId:{hotel.Id}] Images:{ex.Message}");
            }
        }

        /// <summary>
        /// Get countries, destinations and resorts
        /// </summary>
        private void GetPlaces(Root dataRoot)
        {
            GetCountry(dataRoot);
            GetDestinations(dataRoot);
            GetResorts(dataRoot);
        }

        /// <summary>
        /// Gets all countries from the database
        /// </summary>
        private void GetCountry(Root dataRoot)
        {
            try
            {
                var countryDataTable = new countriesTableAdapter().GetData(_localConfig.AgentLanguageCode);
                foreach (var cRow in countryDataTable)
                {
                    dataRoot.Places.Add(new Place
                    {
                        Id = cRow.id.ToString(),
                        Description = cRow.CountryName.Trim(),
                        Codes = new List<PlaceCodes>
                        {
                            new PlaceCodes
                            {
                                Type = _localConfig.CountryPlaceCodesType,
                                Value = cRow.countrycode
                            }
                        }
                    });
                }
            }
            catch (Exception ex)
            {
                Log(ILoggerStaticDataSeverity.Error, $"Error while getting country:{ex.Message}");
            }
        }

        /// <summary>
        /// Gets all destinations from the database
        /// </summary>
        private void GetDestinations(Root dataRoot)
        {
            try
            {
                var countries = dataRoot.Places.ToDictionary(c => c.Id, c => c);
                foreach (var countryList in MultipleQueryLists(countries.Keys))
                {
                    var destinationAdapter = new DestinationsTableAdapter();
                    destinationAdapter.SetCommandTimeout(0, GeneralTimeout);
                    var command = destinationAdapter.GetCommandText(0).Replace("'replaceToken' = 'replaceToken16'", "countries.id IN (" + countryList + ")");
                    destinationAdapter.SetCommandText(0, command);
                    var destinationDataTable = destinationAdapter.GetData(_localConfig.AgentLanguageCode);

                    foreach (var dRow in destinationDataTable)
                    {
                        Place countryPlace;
                        if (countries.TryGetValue(dRow.countryId.ToString(), out countryPlace))
                        {
                            var dest = new Place
                            {
                                Id = dRow.ID.ToString(),
                                Description = dRow.DestinationName.Trim()
                            };
                            var destCode = !string.IsNullOrEmpty(dRow.destination_code) ? dRow.destination_code.Trim() : string.Empty;
                            var destCode2 = !string.IsNullOrEmpty(dRow.destination_code2) ? dRow.destination_code2.Trim() : string.Empty;
                            var destCode3 = !string.IsNullOrEmpty(dRow.destination_code3) ? dRow.destination_code3.Trim() : string.Empty;
                            var destCode4 = !string.IsNullOrEmpty(dRow.destination_code4) ? dRow.destination_code4.Trim() : string.Empty;

                            if (!string.IsNullOrEmpty(destCode))
                                AddPlace(dest, destCode);
                            if (!string.IsNullOrEmpty(destCode2))
                                AddPlace(dest, destCode2);
                            if (!string.IsNullOrEmpty(destCode3))
                                AddPlace(dest, destCode3);
                            if (!string.IsNullOrEmpty(destCode4))
                                AddPlace(dest, destCode4);

                            countryPlace.Places.Add(dest);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log(ILoggerStaticDataSeverity.Error, $"Error while getting destinations:{ex.Message}");
            }
        }

        /// <summary>
        /// Add Place
        /// </summary>
        /// <param name="place">Place</param>
        /// <param name="code">code</param>
        private void AddPlace(Place place, string code)
        {
            place.Codes.Add(new PlaceCodes { Type = _localConfig.DestinationPlaceCodesType, Value = code });
        }

        /// <summary>
        /// Gets all resorts from the database
        /// </summary>
        private void GetResorts(Root dataRoot)
        {
            try
            {
                var destinations = dataRoot.Places.SelectMany(c => c.Places).ToDictionary(d => d.Id, d => d);
                foreach (var destinationList in MultipleQueryLists(destinations.Keys))
                {
                    var resortAdapter = new ResortsTableAdapter();
                    resortAdapter.SetCommandTimeout(0, GeneralTimeout);
                    var command = resortAdapter.GetCommandText(0).Replace("'replaceToken' = 'replaceToken17'", "Resorts.destId IN (" + destinationList + ")");
                    resortAdapter.SetCommandText(0, command);
                    var resortDataTable = resortAdapter.GetData(_localConfig.AgentLanguageCode);

                    foreach (var resRow in resortDataTable)
                    {
                        Place destinationPlace;
                        if (destinations.TryGetValue(resRow.destId.ToString(), out destinationPlace))
                        {
                            var resort = new Place
                            {
                                Id = resRow.ID.ToString(),
                                Description = resRow.ResortName.Trim()
                            };
                            //Copy object "Codes" from the destination in order to send the list of airports available
                            resort.Codes.AddRange(destinationPlace.Codes);
                            destinationPlace.Places.Add(resort);
                        }
                        
                    }
                }
            }
            catch (Exception ex)
            {
                Log(ILoggerStaticDataSeverity.Error, $"Error while getting resorts:{ex.Message}");
            }
        }

        /// <summary>
        /// Log function
        /// </summary>
        /// <param name="severity">ILoggerStaticDataSeverity</param>
        /// <param name="message">message</param>
        /// <param name="args">arguments</param>
        private void Log(ILoggerStaticDataSeverity severity, string message, params object[] args)
        {
            var s = XML.HelpClasses.Log.MessageType.Informational;
            if (severity == ILoggerStaticDataSeverity.Error)
                s = XML.HelpClasses.Log.MessageType.Error;
            if (severity == ILoggerStaticDataSeverity.Warning)
                s = XML.HelpClasses.Log.MessageType.Warning;
            LogEntry(s, message, args);
        }

        #endregion Void

        #region User Defined 

        /// <summary>
        /// Get Languages code for translation preparation 
        /// </summary>
        /// <returns>IEnumerable of string</returns>
        private ConcurrentBag<string> GetLanguages()
        {
            try
            {
                var languages = new ConcurrentBag<string>();
                var languageDataTable = new languageTableAdapter().GetData();
                foreach (var lan in languageDataTable)
                {
                    languages.Add(lan.LanguageCode);
                }
                return languages;
            }
            catch (Exception ex)
            {
                Log(ILoggerStaticDataSeverity.Error, $"Error while getting languages:{ex.Message}");
                return null;
            }
        }

        /// <summary>
        /// Get GIATA hotel codes
        /// </summary>
        /// <returns>Dictionary of hotel id and GIATA</returns>
        private Dictionary<int, string> GiataHotelCodes()
        {
            try
            {
                var hotelCodeDataTable = new hotelCodeTableAdapter().GetData();
                var giataCodes = new Dictionary<int, string>();
                foreach (var hotelCode in hotelCodeDataTable)
                {
                    giataCodes.Add(hotelCode.hotelId, hotelCode.value);
                }
                return giataCodes;
            }
            catch (Exception ex)
            {
                Log(ILoggerStaticDataSeverity.Error, $"Error while getting hotel GIATA codes:{ex.Message}");
                return null;
            }
        }

        /// <summary>
        /// Get suspended hotels id
        /// </summary>
        /// <returns></returns>
        private IEnumerable<int> HotelsSuspended()
        {
            try
            {
                var hotelsSuspended = new List<int>();
                var hotelStatusDataTable = new hotelStatusTableAdapter().GetData();
                foreach (var hotelStatus in hotelStatusDataTable)
                {
                    hotelsSuspended.Add(hotelStatus.id);
                }
                return hotelsSuspended;
            }
            catch (Exception ex)
            {
                Log(ILoggerStaticDataSeverity.Error, $"Error while getting suspended hotels:{ex.Message}");
                return null;
            }
        }

        /// <summary>
        /// Splits the source list (individual elements) into several string lists (joined), each list with maximum QUERY_ELEMENTS, joined by ','
        /// </summary>
        /// <param name="sourceList"></param>
        /// <returns></returns>
        private static IEnumerable<string> MultipleQueryLists(IEnumerable<string> sourceList)
        {
            var tmpLists = new Dictionary<int, List<string>>();
            if (sourceList == null) return tmpLists.Values.Select(tmp => string.Join(",", tmp)).ToList();
            foreach (var element in sourceList)
            {
                if (tmpLists.Count == 0)
                    tmpLists.Add(0, new List<string>());
                //Limit queries by a certain number of elements
                if (tmpLists[tmpLists.Count - 1].Count > QueryElements)
                    tmpLists.Add(tmpLists.Count, new List<string>());
                tmpLists[tmpLists.Count - 1].Add(element);
            }
            return tmpLists.Values.Select(tmp => string.Join(",", tmp)).ToList();
        }

        #endregion User Defined 
    }
}
