﻿using System.Collections.Generic;

namespace SunHotels.Providers
{
    public class PeakworkContentEDFSettings
    {
        public List<int> BlockedHotels { get; set; }
        public string EdfVersionHotel { get; set; }
        public string HotelRootNamespace { get; set; }
        public string TourOperatorCode { get; set; }
        public string TourOperatorName { get; set; }
        public string ZipName { get; set; }
        public string HotelXsd { get; set; }
        public string AgentLanguageCode { get; set; }
        public string ReferencesType { get; set; }
        public string SmallImageUrlPrefix { get; set; }
        public string LargeImageUrlPrefix { get; set; }
        public string ParagraphCategory { get; set; }
        public string MultiMediaLinkType { get; set; }
        public string CountryPlaceCodesType { get; set; }
        public string DestinationPlaceCodesType { get; set; }
        public string LocationType { get; set; }
        public string ReviewType { get; set; }

    }
}
