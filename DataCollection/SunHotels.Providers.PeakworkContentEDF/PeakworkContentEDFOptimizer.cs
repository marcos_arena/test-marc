﻿using Sunhotels.Export;
using SunHotels.XML.Optimize;

namespace SunHotels.Providers
{
    public class PeakworkContentEDFOptimizer : ProviderOptimizer
    {
        public PeakworkContentEDFOptimizer(Configuration configuration) : base(configuration)
        {
        }
    }
}
