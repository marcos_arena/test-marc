using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Net;

namespace SunHotels.Providers
{
    public class HotelBedsConnectHttp : IHotelBedsConnect
    {
        public string BaseUrl { get; set; }
        public string User { get; set; }
        public string Password { get; set; }
        public Dictionary<string, string> Headers { get; set; }

        public void GetResource(string resourceName, string destinationFile)
        {
            var fileVersion = String.Concat(destinationFile, HotelBedsSettings.FileVersionSufix);

            if (File.Exists(fileVersion))
            {
                File.Delete(fileVersion);
            }

            var webRequest = (HttpWebRequest)WebRequest.Create(String.Concat(BaseUrl, resourceName));

            webRequest.Method = "GET";
            webRequest.ContentType = "application/x-www-form-urlencoded";
            webRequest.Timeout = 3000000;
            webRequest.ProtocolVersion = HttpVersion.Version11;

            webRequest.Headers.Add("Accept-Encoding", "gzip");
            webRequest.Headers.Add("X-Username", User);
            webRequest.Headers.Add("X-Password", Password);

            if (Headers != null)
            {
                foreach (var header in Headers)
                {
                    webRequest.Headers.Add(header.Key, header.Value);
                }
            }

            var webResponse = (HttpWebResponse)webRequest.GetResponse();

            if (webResponse.StatusCode == HttpStatusCode.OK)
            {
                var noContentResponse = false;
                var receiveStream = webResponse.GetResponseStream();
                if (receiveStream != null)
                {
                    if (webResponse.ContentEncoding.Contains("gzip"))
                    {
                        receiveStream = new GZipStream(receiveStream, CompressionMode.Decompress);
                    }
                    else if (webResponse.ContentEncoding.Contains("deflate"))
                    {
                        receiveStream = new DeflateStream(receiveStream, CompressionMode.Decompress);
                    }

                    var oWriter = new FileStream(destinationFile, FileMode.Create);
                    receiveStream.CopyTo(oWriter);
                    receiveStream.Dispose();
                    var deleteFile = (oWriter.Length == 0);
                    oWriter.Dispose();
                    if (deleteFile)
                    {
                        noContentResponse = true;
                        File.Delete(destinationFile);
                    }
                }

                if (!noContentResponse)
                {
                    string contentDisposition = webResponse.GetResponseHeader("Content-Disposition");
                    string lookFor = "filename=";
                    if (!String.IsNullOrEmpty(contentDisposition) && contentDisposition.IndexOf(lookFor) >= 0)
                    {
                        int index = contentDisposition.IndexOf(lookFor, StringComparison.InvariantCultureIgnoreCase);
                        string reference = contentDisposition.Substring(index + lookFor.Length).Replace("\"", "");
                        File.WriteAllText(fileVersion, String.Concat(destinationFile, HotelBedsSettings.FileVersionChar, reference));
                    }
                }
            }
            else
            {
                if (webResponse.StatusCode != HttpStatusCode.NoContent)
                {
                    throw new Exception(webResponse.StatusDescription);
                }
            }

            if (webResponse.Headers.Count > 0)
            {
                if (Headers != null)
                {
                    Headers.Clear();
                }
                else
                {
                    Headers = new Dictionary<string, string>();
                }
                foreach (var header in webResponse.Headers.AllKeys)
                {
                    Headers.Add(header, webResponse.Headers.Get(header));
                }
            }
        }
    }
}