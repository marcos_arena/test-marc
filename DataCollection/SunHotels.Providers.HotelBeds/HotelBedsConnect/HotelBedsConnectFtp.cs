﻿using SunHotels.XML;
using System;
using System.Collections.Generic;

namespace SunHotels.Providers
{
    public class HotelBedsConnectFtp : IHotelBedsConnect
    {
        public string BaseUrl { get; set; }
        public string User { get; set; }
        public string Password { get; set; }
        public Dictionary<string, string> Headers { get; set; }

        public void GetResource(string resourceName, string destinationFile)
        {
            var strSource = String.Concat(BaseUrl, resourceName);

            var ftpCom = new FTPCommunication(User, Password, true);
            ftpCom.DownloadFile(strSource, destinationFile);
        }
    }
}
