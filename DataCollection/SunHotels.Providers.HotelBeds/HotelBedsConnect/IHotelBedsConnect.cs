﻿using System.Collections.Generic;

namespace SunHotels.Providers
{
    public interface IHotelBedsConnect
    {
        string BaseUrl { get; set; }
        string User { get; set; }
        string Password { get; set; }
        Dictionary<string, string> Headers { get; set; }

        void GetResource(string resourceName, string destinationFile);
    }
}
