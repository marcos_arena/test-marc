﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Xml;
using Sunhotels.Export;

namespace SunHotels.Providers
{
    public class HotelBedsSettings
    {
        public Configuration Base { get; set; }
        public String XmlProviderId { get; set; }
        public Int32 MaxTempFiles { get; set; }
        public Boolean Multithread { get; set; }
        public Int32? ParallelDownloads { get; set; }
        public Int32? RetryDownloads { get; set; }
        public Int32? SecondsBetweenRetries { get; set; }
        public Int32? CommonThreads { get; set; }
        public String FtpUrl { get; set; }
        public String FtpUser { get; set; }
        public String FtpPassword { get; set; }
        public String Aif2Url { get; set; }
        public String WebServiceUser { get; set; }
        public String WebServicePassword { get; set; }
        public Int32? MaximumStayDaysFilter { get; set; }
        public String ImportMarkets { get; set; }

        public void ImportBaseConfig(Configuration configuration, Stream dataStream)
        {
            Base = configuration;

            var infosHb = typeof(HotelBedsSettings).GetProperties();
            var xmlDoc = new XmlDocument();
            xmlDoc.Load(dataStream);
            var doc = xmlDoc.DocumentElement;
            if (doc != null)
            {
                foreach (var infoHb in infosHb)
                {
                    var node = doc.SelectSingleNode(infoHb.Name);
                    if (node != null && node.HasChildNodes && node.ChildNodes.Count == 1 && node.FirstChild != null && !String.IsNullOrEmpty(node.FirstChild.Value))
                    {
                        var conv = TypeDescriptor.GetConverter(infoHb.PropertyType);
                        infoHb.SetValue(this, conv.ConvertFrom(node.FirstChild.Value));
                    }
                }
            }
            else
            {
                throw new ApplicationException("Error loading HotelBeds settings file");
            }
        }

        #region Static definitions and functionality

        public const string FileCreatePrefix = "HotelBeds_";
        public const string UncompressFolderSufix = "_tmp\\";
        public const string FileVersionSufix = ".ref.txt";
        public const char FileVersionChar = '~';
        public const string Aif2DateFormat = "yyyyMMdd";
        public const char Aif2TableCharSeparator = '~';
        public const char Aif2FieldCharSeparator = ':';
        public const char PlaceSeparator = '~';
        public const char CombinationSeparator = '~';
        public const char IdFieldCharSeparator = '-';
        public const char XmlRowCharSeparator = '~';
        public const char XmlColumnCharSeparator = '¬';
        public const int SunHotelsChildAgeBooking = 11;
        public const int SunHotelsAdultAgeBooking = 30;
        public const int SeniorAge = 50;
        public const int AdultMaxAge = 99;

        //Any change of the room id structure must be done in the availability and booking xml integration and in the DB sp_UpdateDefaultMapping ('%:[G0-9]%:[YN]:[YN]:[0-9]%:[0-9]%'), sp_GetHotelDetails, sp_ImportRoom, HotelImportControl.ascx (opaque), AutoMapping (JOB)
        public const char RoomCharSeparator = ':';
        public const string GenericRateCode = "G";

        public enum Languages
        {
            English, Swedish, Norwegian, Danish, German, French, Spanish, Polish, Finish, Russian, Chinese, Italian, Portugese
        }

        public enum BasicFiles
        {
            Destinations, Hotels
        }

        public enum StaticFiles
        {
            Extended, Contents
        }

        public enum CacheFiles
        {
            ActiveInventoryFilesServices
        }

        public enum UpdateCacheFiles
        {
            ActiveInventoryFilesServices
        }

        public enum ConfirmCacheFiles
        {
            ActiveInventoryFilesServices
        }

        public enum ChildFiles
        {
            AllFiles, RoomDescriptions, CharacteristicDescriptions, HotelIssues
        }

        public enum CompressionType
        {
            Zip
        }

        public enum ReaderType
        {
            Unknown, Excel, Csv, Xml, Aif2
        }

        public static readonly Dictionary<Languages, string> LanguageInternalCode = new Dictionary<Languages, string>
        {
            {Languages.English, "en"},
            {Languages.Swedish, "sv"},
            {Languages.Norwegian, "no"},
            {Languages.Danish, "dk"},
            {Languages.German, "ge"},
            {Languages.French, "fr"},
            {Languages.Spanish, "es"},
            {Languages.Polish, "pl"},
            {Languages.Finish, "fi"},
            {Languages.Russian, "ru"},
            {Languages.Chinese, "zh"},
            {Languages.Italian, "it"},
            {Languages.Portugese, "pt"}
        };

        public static readonly Dictionary<Languages, string> LanguageExternalCode = new Dictionary<Languages, string>
        {
            {Languages.English, "ENG"},
            {Languages.Swedish, "SUE"},
            {Languages.Norwegian, "NOR"},
            {Languages.Danish, "DAN"},
            {Languages.German, "ALE"},
            {Languages.French, "FRA"},
            {Languages.Spanish, "CAS"},
            {Languages.Polish, "POL"},
            {Languages.Finish, "FIN"},
            {Languages.Russian, "RUS"},
            {Languages.Chinese, "CHI"},
            {Languages.Italian, "ITA"},
            {Languages.Portugese, "POR"}
        };

        public static readonly Dictionary<Languages, string> LanguageCountryCode = new Dictionary<Languages, string>
        {
            {Languages.English, "GB"},
            {Languages.Swedish, "SE"},
            {Languages.Norwegian, "NO"},
            {Languages.Danish, "DK"},
            {Languages.German, "DE"},
            {Languages.French, "FR"},
            {Languages.Spanish, "ES"},
            {Languages.Polish, "PL"},
            {Languages.Finish, "FI"},
            {Languages.Russian, "RU"},
            {Languages.Chinese, "CN"},
            {Languages.Italian, "IT"},
            {Languages.Portugese, "PT"}
        };

        public static readonly List<Languages> LanguageHotelDescriptionTranslation = new List<Languages>
        {
            Languages.English,
            Languages.Danish,
            Languages.German,
            Languages.French,
            Languages.Spanish,
            Languages.Italian,
            Languages.Portugese
        };

        public static readonly Dictionary<string, ReaderType> ExtensionReaderType = new Dictionary<string, ReaderType>
        {
            {".xls", ReaderType.Excel},
            {".csv", ReaderType.Csv},
            {".xml", ReaderType.Xml}
        };

        public static readonly List<Enum> LanguageFiles = new List<Enum>
        {
            BasicFiles.Destinations, BasicFiles.Hotels, StaticFiles.Contents
        };

        public static readonly Dictionary<Enum, Dictionary<Languages, string>> LanguageFilesName = new Dictionary<Enum, Dictionary<Languages, string>>
        {
            {BasicFiles.Destinations, new Dictionary<Languages, string>
                {
                    {Languages.English, "Destinations_ING.xls"},
                    {Languages.Swedish, "Destinations_ING.xls"},
                    {Languages.Norwegian, "Destinations_ING.xls"},
                    {Languages.Danish, "Destinations_ING.xls"},
                    {Languages.German, "Destinations_ING.xls"},
                    {Languages.French, "Destinations_ING.xls"},
                    {Languages.Spanish, "Destinations_CAS.xls"},
                    {Languages.Polish, "Destinations_ING.xls"},
                    {Languages.Finish, "Destinations_ING.xls"},
                    {Languages.Russian, "Destinations_ING.xls"},
                    {Languages.Chinese, "Destinations_ING.xls"},
                    {Languages.Italian, "Destinations_ITA.xls"},
                    {Languages.Portugese, "Destinations_POR.xls"}
                }
            },
            
            {BasicFiles.Hotels, new Dictionary<Languages, string>
                {
                    {Languages.English, "New_Hotel_List_ING.xls"},
                    {Languages.Swedish, "New_Hotel_List_ING.xls"},
                    {Languages.Norwegian, "New_Hotel_List_ING.xls"},
                    {Languages.Danish, "New_Hotel_List_DAN.xls"},
                    {Languages.German, "New_Hotel_List_ALE.xls"},
                    {Languages.French, "New_Hotel_List_FRA.xls"},
                    {Languages.Spanish, "New_Hotel_List_CAS.xls"},
                    {Languages.Polish, "New_Hotel_List_ING.xls"},
                    {Languages.Finish, "New_Hotel_List_ING.xls"},
                    {Languages.Russian, "New_Hotel_List_ING.xls"},
                    {Languages.Chinese, "New_Hotel_List_ING.xls"},
                    {Languages.Italian, "New_Hotel_List_ITA.xls"},
                    {Languages.Portugese, "New_Hotel_List_POR.xls"}
                }
            },

            {StaticFiles.Contents, new Dictionary<Languages, string>
                {
                    {Languages.English, "Hotel_contents/ING.zip"},
                    {Languages.Swedish, "Hotel_contents/ING.zip"},
                    {Languages.Norwegian, "Hotel_contents/ING.zip"},
                    {Languages.Danish, "Hotel_contents/DAN.zip"},
                    {Languages.German, "Hotel_contents/ALE.zip"},
                    {Languages.French, "Hotel_contents/FRA.zip"},
                    {Languages.Spanish, "Hotel_contents/ESP.zip"},
                    {Languages.Polish, "Hotel_contents/ING.zip"},
                    {Languages.Finish, "Hotel_contents/ING.zip"},
                    {Languages.Russian, "Hotel_contents/ING.zip"},
                    {Languages.Chinese, "Hotel_contents/ING.zip"},
                    {Languages.Italian, "Hotel_contents/ITA.zip"},
                    {Languages.Portugese, "Hotel_contents/POR.zip"}
                }
            }
        };

        public static readonly Dictionary<Enum, CompressionType> FilesCompression = new Dictionary<Enum, CompressionType>
        {
            {CacheFiles.ActiveInventoryFilesServices, CompressionType.Zip},
            {UpdateCacheFiles.ActiveInventoryFilesServices, CompressionType.Zip},
            {StaticFiles.Extended, CompressionType.Zip},
            {StaticFiles.Contents, CompressionType.Zip}
        };

        public static readonly Dictionary<Enum, List<ChildFiles>> FilesChild = new Dictionary<Enum, List<ChildFiles>>
        {
            {CacheFiles.ActiveInventoryFilesServices, new List<ChildFiles>{ChildFiles.AllFiles}},
            {UpdateCacheFiles.ActiveInventoryFilesServices, new List<ChildFiles>{ChildFiles.AllFiles}},
            {StaticFiles.Contents, new List<ChildFiles>{ChildFiles.AllFiles}},
            {StaticFiles.Extended, new List<ChildFiles>{ChildFiles.RoomDescriptions, ChildFiles.CharacteristicDescriptions, ChildFiles.HotelIssues}}
        };

        public static readonly Dictionary<Enum, string> Files = new Dictionary<Enum, string>
        {
            {CacheFiles.ActiveInventoryFilesServices, "aif2-pub-ws/files/full"},
            {UpdateCacheFiles.ActiveInventoryFilesServices, "aif2-pub-ws/files/update"},
            {ConfirmCacheFiles.ActiveInventoryFilesServices, "aif2-pub-ws/files/confirm"},
            {StaticFiles.Extended, "ExportCSV/Extended.zip"},
            {ChildFiles.RoomDescriptions, "Extended/RoomDescriptions.csv"},
            {ChildFiles.CharacteristicDescriptions, "Extended/CharacteristicDescriptions.csv"},
            {ChildFiles.HotelIssues, "Extended/HotelIssues.csv"}
        };

        public static readonly Dictionary<Enum, ReaderType> FilesReaderType = new Dictionary<Enum, ReaderType>
        {
            {CacheFiles.ActiveInventoryFilesServices, ReaderType.Aif2},
            {UpdateCacheFiles.ActiveInventoryFilesServices, ReaderType.Aif2}
        };

        public static Languages LanguageFromConfiguration(string language)
        {
            var configLanguage = Languages.English;
            switch (language)
			{
                case "en": configLanguage = Languages.English; break;
                case "sv": configLanguage = Languages.Swedish; break;
                case "no": configLanguage = Languages.Norwegian; break;
                case "dk": configLanguage = Languages.Danish; break;
                case "ge": configLanguage = Languages.German; break;
                case "fr": configLanguage = Languages.French; break;
                case "es": configLanguage = Languages.Spanish; break;
                case "pl": configLanguage = Languages.Polish; break;
                case "fi": configLanguage = Languages.Finish; break;
                case "ru": configLanguage = Languages.Russian; break;
                case "zh": configLanguage = Languages.Chinese; break;
                case "it": configLanguage = Languages.Italian; break;
                case "pt": configLanguage = Languages.Portugese; break;
			}
            return configLanguage;
        }

        public static ReaderType ReaderTypeFromFullFileName(string fileName)
        {
            var extensionType = ReaderType.Unknown;
            var extension = Path.GetExtension(fileName);
            if (!String.IsNullOrEmpty(extension) && ExtensionReaderType.ContainsKey(extension.ToLowerInvariant()))
            {
                extensionType = ExtensionReaderType[extension.ToLowerInvariant()];
            }
            return extensionType;
        }

        public static ReaderType ReaderTypeFromResource(Enum resource)
        {
            var extensionType = ReaderType.Unknown;
            if (FilesReaderType.ContainsKey(resource))
            {
                extensionType = FilesReaderType[resource];
            }
            return extensionType;
        }

        #endregion

        #region Mappings

        public static Dictionary<string, string> MapCurrency = new Dictionary<string, string>
        {
            {"EUR", "EUR"}, {"GBP", "GBP"}, {"USD", "USD"}
        };

        public static Dictionary<string, string> MapMeals = new Dictionary<string, string>
        {
            {"RO", "none"},
            {"SC", "none"},
            {"BB", "breakfast"},
            {"CB", "breakfast"},
            {"DB", "breakfast"},
            {"EB", "breakfast"},
            {"GB", "breakfast"},
            {"IB", "breakfast"},
            {"SB", "breakfast"},
            {"AB", "breakfast"},
            {"HB", "half_board"},
            {"MB", "half_board"},
            {"FB", "full_board"},
            {"PB", "full_board"},
            {"AI", "all_inclusive"},
            {"AS", "all_inclusive"}
        };

        public static Dictionary<string, string> MapInternalMeals = new Dictionary<string, string>
        {
            {"none", "No meals"},
            {"breakfast", "Breakfast"},
            {"half_board", "Half Board"},
            {"full_board", "Full Board"},
            {"all_inclusive", "All"}
        };

        public static Dictionary<string, string> MapPushMeals = new Dictionary<string, string>
        {
            {"none", "None"},
            {"breakfast", "Breakfast"},
            {"half_board", "Half_Board"},
            {"full_board", "Full_Board"},
            {"all_inclusive", "All"}
        };

        public static Dictionary<string, string> MapFeatures = new Dictionary<string, string>
        {
            {"60" + XmlColumnCharSeparator + "170", "1"},  //Centrally regulated air conditioning
            {"60" + XmlColumnCharSeparator + "180", "1"},  //Individually adjustable air conditioning
            {"70" + XmlColumnCharSeparator + "70", "4"},   //Lift access
            {"71" + XmlColumnCharSeparator + "130", "3"},  //Bar
            {"60" + XmlColumnCharSeparator + "50", "10"},  //Direct dial telephone
            {"73" + XmlColumnCharSeparator + "385", "6"},  //Children's swimming area
            {"73" + XmlColumnCharSeparator + "360", "5"},  //Indoor freshwater pool
            {"73" + XmlColumnCharSeparator + "361", "5"},  //Indoor saltwater pool
            {"73" + XmlColumnCharSeparator + "362", "5"},  //Indoor heated pool
            {"73" + XmlColumnCharSeparator + "363", "5"},  //Outdoor freshwater pool
            {"73" + XmlColumnCharSeparator + "364", "5"},  //Outdoor saltwater pool
            {"73" + XmlColumnCharSeparator + "365", "5"},  //Outdoor heated pool
            {"60" + XmlColumnCharSeparator + "200", "8"},  //Safe
            {"60" + XmlColumnCharSeparator + "230", "2"},  //Balcony
            {"60" + XmlColumnCharSeparator + "240", "2"},  //Terrace
            {"71" + XmlColumnCharSeparator + "200", "7"},  //Restaurant
            {"60" + XmlColumnCharSeparator + "55", "11"},  //TV
            {"60" + XmlColumnCharSeparator + "261", "12"}  //Wi-fi
        };

        public static Dictionary<string, string> MapDistances = new Dictionary<string, string>
        {
            {"40" + XmlColumnCharSeparator + "10", "2"}, //City centre
            {"40" + XmlColumnCharSeparator + "80", "3"}, //Airport
            {"40" + XmlColumnCharSeparator + "40", "1"}  //Beach
        };

        #endregion

    }
}
