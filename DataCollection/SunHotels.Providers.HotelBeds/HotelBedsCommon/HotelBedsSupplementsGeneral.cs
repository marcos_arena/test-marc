﻿using System;
using System.Collections.Generic;
using SunHotels.XML.Data;

namespace SunHotels.Providers
{
    public class HotelBedsSupplementsGeneral
    {
        public HotelBedsSupplementsParams SupplementsParams { get; set; }
        public string TxtAmount { get; set; }
        public string TxtPercentage { get; set; }
        public string TxtBoardCode { get; set; }
        public string SupplementType { get; set; }
        public string ApplicationType { get; set; }
        public bool IsPerPax { get; set; }
        public string LengthOfStay { get; set; }
        public string LimitDate { get; set; }
        public Dictionary<DateTime, Dictionary<string, Dictionary<string, List<string>>>> TypeSupplementProcessed { get; set; }
        public DateTime Date { get; set; }
        public Room Room { get; set; }
        public AvailabilityPeriod AvailabilityPeriod { get; set; }
        public string Rate { get; set; }
        public string BoardCode { get; set; }
        public bool IsAdultPrice { get; set; }
        public bool IsChildPrice { get; set; }
        public int DiscountPeriodId { get; set; }
        public DateTime? DiscountLimitDate { get; set; }
    }
}
