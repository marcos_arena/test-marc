﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Text;
using System.Threading;
using SunHotels.XML.Data;

namespace SunHotels.Providers
{
    public class HotelBedsCommonFunctions
    {
        private readonly string _doubleChar;
        private readonly List<string> _occupancyCombinations;
        private readonly List<string> _typeCombinations;
        private readonly List<string> _typeGeneralCombination;
        private readonly List<string> _supTypes;
        private readonly List<string> _appTypes;
        private readonly List<string> _groupExceptColumns;
        private int _discountPeriodId;

        public HotelBedsCommonFunctions()
        {
            //doubleChar must be a string "::"
            _doubleChar = HotelBedsSettings.RoomCharSeparator + HotelBedsSettings.RoomCharSeparator.ToString();
            _occupancyCombinations = new List<string> { "Adults", "Pax order", "Minimum age", "Maximum age" };
            _typeCombinations = new List<string> { "Type" };
            _typeGeneralCombination = new List<string> { "supplementGeneral" };
            _supTypes = new List<string> { "N", "F", "C", "I", "L", "B", "E", "M", "O", "G", "K", "V", "U" };
            _appTypes = new List<string> { "B", "R", "N", "A", "T", "U", "M" };
            _groupExceptColumns = new List<string> { "Amount", "Percentage", "Minimum age", "Maximum age", "Net price", "Price" };
            _discountPeriodId = 0;
        }

        #region Performance functions

        public List<string> SplitGroupedValues(string groupedValues)
        {
            var elements = new List<string>();
            var strBuilder = new StringBuilder();
            for (var i = 1; i <= groupedValues.Length - 2; i++)
            {
                var ch = groupedValues[i];
                if (!ch.Equals(')'))
                {
                    strBuilder.Append(ch);
                }
                else
                {
                    elements.Add(strBuilder.ToString());
                    strBuilder.Clear();
                    //Avoid read next char '(', HotelBeds groups values "(xx,yy)(xx,zz)"
                    i++;
                }
            }
            if (strBuilder.Length > 0)
            {
                elements.Add(strBuilder.ToString());
            }
            return elements;
        }

        #endregion

        #region Cache Table functions

        public int GetNextDiscountPeriodId()
        {
            return Interlocked.Increment(ref _discountPeriodId);
        }

        public void PaxApplyPrice(string minimumAge, string maximumAge, out bool adultPrice, out bool childPrice)
        {
            adultPrice = false;
            childPrice = false;
            if (!String.IsNullOrEmpty(minimumAge) || !String.IsNullOrEmpty(maximumAge))
            {
                var minAge = !String.IsNullOrEmpty(minimumAge) ? Int32.Parse(minimumAge) : 0;
                var maxAge = !String.IsNullOrEmpty(maximumAge) ? Int32.Parse(maximumAge) : HotelBedsSettings.AdultMaxAge;

                if (minAge < 0 || maxAge < 0) return;
                if (minAge <= HotelBedsSettings.SunHotelsChildAgeBooking && maxAge >= HotelBedsSettings.SunHotelsAdultAgeBooking)
                {
                    adultPrice = true;
                    childPrice = true;
                }
                else if (minAge <= maxAge)
                {
                    adultPrice = HotelBedsSettings.SunHotelsChildAgeBooking < minAge && minAge < HotelBedsSettings.SeniorAge;
                    childPrice = minAge <= HotelBedsSettings.SunHotelsChildAgeBooking;
                }
            }
            else
            {
                adultPrice = true;
                childPrice = true;
            }
        }

        public void LogLine(ConcurrentDictionary<string, ConcurrentDictionary<string, ConcurrentBag<string>>> logList, string hotelIdName, string reason, string reasonValue)
        {
            if (string.IsNullOrEmpty(reasonValue))
                reasonValue = "+";

            if (!logList.ContainsKey(reason))
            {
                logList.TryAdd(reason, new ConcurrentDictionary<string, ConcurrentBag<string>>());
            }

            ConcurrentDictionary<string, ConcurrentBag<string>> list;
            logList.TryGetValue(reason, out list);

            if(!list .ContainsKey(reasonValue))
            {
                list.TryAdd(reasonValue, new ConcurrentBag<string>());
            }

            ConcurrentBag<string> items;
            list.TryGetValue(reasonValue, out items);
            items.Add(hotelIdName);
        }

        public List<Dictionary<string, string>> OrderDataTable(IHotelBedsDataTable table, string fieldNameRoomType, string fieldNameCharacteristic, string fieldNameRate, string fieldNameBoardCode, string fieldNameMarket)
        {
            var sortedList = new SortedList<string, List<Dictionary<string, string>>>();
            foreach (var line in table.Rows)
            {
                var market = (!String.IsNullOrEmpty(fieldNameMarket) ? line[fieldNameMarket] : String.Empty);
                var roomTypeCode = (!String.IsNullOrEmpty(fieldNameRoomType) ? line[fieldNameRoomType] : String.Empty);
                var roomCharacteristic = (!String.IsNullOrEmpty(fieldNameCharacteristic) ? line[fieldNameCharacteristic] : String.Empty);
                var rate = (!String.IsNullOrEmpty(fieldNameRate) ? line[fieldNameRate] : String.Empty);
                var boardCode = (!String.IsNullOrEmpty(fieldNameBoardCode) ? line[fieldNameBoardCode] : String.Empty);

                var keyOrder = String.Concat(
                    !String.IsNullOrEmpty(market) ? "0" : "1",
                    !String.IsNullOrEmpty(rate) ? "0" : "1",
                    !String.IsNullOrEmpty(roomTypeCode) ? "0" : "1",
                    !String.IsNullOrEmpty(roomCharacteristic) ? "0" : "1",
                    !String.IsNullOrEmpty(boardCode) ? "0" : "1"
                );

                if (!sortedList.ContainsKey(keyOrder))
                {
                    sortedList.Add(keyOrder, new List<Dictionary<string, string>>());
                }
                sortedList[keyOrder].Add(line);
            }
            var sortedRows = new List<Dictionary<string, string>>();
            foreach (var list in sortedList.Values)
            {
                foreach (var line in list)
                {
                    sortedRows.Add(line);
                }
            }
            return sortedRows;
        }

        public List<string> RowCombinations(Dictionary<string, List<string>> distinctCombinations, string roomTypeCode, string roomCharacteristic, string rate, string boardCode, string market, SortedList<int, Dictionary<string, string>> sortedRates, IHotelBedsDataTable roomTypesTable, List<string> boardsList, List<string> contractMarkets)
        {
            var keyCombination = String.Join(
                HotelBedsSettings.CombinationSeparator.ToString(),
                String.IsNullOrEmpty(market) ? String.Empty : market,
                String.IsNullOrEmpty(rate) ? String.Empty : rate,
                String.IsNullOrEmpty(roomTypeCode) ? String.Empty : roomTypeCode,
                String.IsNullOrEmpty(roomCharacteristic) ? String.Empty : roomCharacteristic,
                String.IsNullOrEmpty(boardCode) ? String.Empty : boardCode
            );

            List<string> rowCombinations;
            if (distinctCombinations.ContainsKey(keyCombination))
            {
                rowCombinations = distinctCombinations[keyCombination];
            }
            else
            {
                rowCombinations = new List<string>();

                var marketList = new List<string>();
                if (market == null || !String.Empty.Equals(market))
                {
                    marketList.Add(market);
                }
                else
                {
                    //Empty value means all possible values
                    foreach (var mkt in contractMarkets)
                    {
                        marketList.Add(mkt);
                    }
                }

                var rateList = new List<string>();
                if (rate == null || !String.Empty.Equals(rate))
                {
                    rateList.Add(rate);
                }
                else
                {
                    //Empty value means all possible values
                    foreach (var rateLine in sortedRates.Values)
                    {
                        rateList.Add(rateLine["Rate"]);
                    }
                }

                var validRoomCombination = new List<string>();
                foreach (var roomLine in roomTypesTable.Rows)
                {
                    var rTypeCode = roomLine["Room type"];
                    var rCharacteristic = roomLine["Characteristic"];
                    var rComb = String.Join(HotelBedsSettings.RoomCharSeparator.ToString(), rTypeCode, rCharacteristic);
                    if (!validRoomCombination.Contains(rComb))
                    {
                        validRoomCombination.Add(rComb);
                    }
                }

                var roomTypeList = new List<string>();
                if (roomTypeCode == null || !String.Empty.Equals(roomTypeCode))
                {
                    roomTypeList.Add(roomTypeCode);
                }
                else
                {
                    //Empty value means all possible values
                    foreach (var roomLine in roomTypesTable.Rows)
                    {
                        if (!roomTypeList.Contains(roomLine["Room type"]))
                        {
                            roomTypeList.Add(roomLine["Room type"]);
                        }
                    }
                }

                var roomCharacteristicList = new List<string>();
                if (roomCharacteristic == null || !String.Empty.Equals(roomCharacteristic))
                {
                    roomCharacteristicList.Add(roomCharacteristic);
                }
                else
                {
                    //Empty value means all possible values
                    foreach (var roomType in roomTypeList)
                    {
                        foreach (var roomCharacteristicLine in roomTypesTable.Rows)
                        {
                            if (roomType.Equals(roomCharacteristicLine["Room type"]) && !roomCharacteristicList.Contains(roomCharacteristicLine["Characteristic"]))
                            {
                                roomCharacteristicList.Add(roomCharacteristicLine["Characteristic"]);
                            }
                        }
                    }
                }

                var boardList = new List<string>();
                if (boardCode == null || !String.Empty.Equals(boardCode))
                {
                    boardList.Add(boardCode);
                }
                else
                {
                    //Empty value means all possible values
                    foreach (var board in boardsList)
                    {
                        boardList.Add(board);
                    }
                }

                foreach (var marketItem in marketList)
                {
                    foreach (var rateItem in rateList)
                    {
                        foreach (var roomTypeItem in roomTypeList)
                        {
                            foreach (var roomCharacteristicItem in roomCharacteristicList)
                            {
                                if (
                                    roomTypeItem != null
                                    && roomCharacteristicItem != null
                                    && !validRoomCombination.Contains(String.Join(HotelBedsSettings.RoomCharSeparator.ToString(), roomTypeItem, roomCharacteristicItem))
                                )
                                {
                                    continue;
                                }

                                foreach (var boardItem in boardList)
                                {
                                    var combination = String.Join(
                                        HotelBedsSettings.CombinationSeparator.ToString(),
                                        marketItem ?? String.Empty,
                                        rateItem ?? String.Empty,
                                        roomTypeItem ?? String.Empty,
                                        roomCharacteristicItem ?? String.Empty,
                                        boardItem ?? String.Empty
                                    );
                                    if (!rowCombinations.Contains(combination))
                                    {
                                        rowCombinations.Add(combination);
                                    }
                                }
                            }
                        }
                    }
                }

                distinctCombinations.Add(keyCombination, rowCombinations);
            }

            return rowCombinations;
        }

        public Dictionary<string, string> SplitCombination(Dictionary<string, Dictionary<string, string>> splittedCombinations, string combination, List<string> extraColumnsCombinations)
        {
            Dictionary<string, string> values;
            if (!splittedCombinations.ContainsKey(combination))
            {
                values = new Dictionary<string, string>();
                var columns = new List<string> { "Market", "Rate", "Room type", "Characteristic", "Board" };
                if (extraColumnsCombinations != null)
                {
                    columns.AddRange(extraColumnsCombinations);
                }
                var fields = combination.Split(HotelBedsSettings.CombinationSeparator);
                for (var i = 0; i < fields.Length; i++)
                {
                    values.Add(columns[i], fields[i]);
                }
                splittedCombinations.Add(combination, values);
            }
            else
            {
                values = splittedCombinations[combination];
            }

            return values;
        }

        public List<string> FilterCombinations(Dictionary<DateTime, Dictionary<string, bool>> tableCombinations, List<string> rowCombinations, DateTime date)
        {
            List<string> combNotProcessed;
            Dictionary<string, bool> combDict;
            if (tableCombinations.TryGetValue(date, out combDict))
            {
                combNotProcessed = new List<string>();
                foreach (var rowComb in rowCombinations)
                {
                    var foundComb = false;
                    bool processed;
                    if (combDict.TryGetValue(rowComb, out processed) && processed)
                    {
                        foundComb = true;
                    }
                    if (!foundComb)
                    {
                        combNotProcessed.Add(rowComb);
                    }
                }
                if (combNotProcessed.Count > 0)
                {
                    foreach (var cnp in combNotProcessed)
                    {
                        if (!combDict.ContainsKey(cnp))
                        {
                            combDict.Add(cnp, false);
                        }
                    }
                }
            }
            else
            {
                combDict = new Dictionary<string, bool>();
                tableCombinations.Add(date, combDict);
                foreach (var rowComb in rowCombinations)
                {
                    combDict.Add(rowComb, false);
                }
                combNotProcessed = rowCombinations;
            }
            return combNotProcessed;
        }

        public List<string> AddCombinations(List<string> combinations, Dictionary<string, string> line, List<string> fields)
        {
            var newCombinations = new List<string>();
            var sb = new StringBuilder();
            foreach (var field in fields)
            {
                if (line.ContainsKey(field))
                {
                    sb.Append(HotelBedsSettings.CombinationSeparator);
                    sb.Append(line[field]);
                }
            }

            var str = sb.ToString();
            foreach (var combination in combinations)
            {
                newCombinations.Add(combination + str);
            }
            return newCombinations;
        }

        public List<Dictionary<string, string>> GroupDataTablePerAge(List<Dictionary<string, string>> sortedRows)
        {
            var groupedRows = new List<Dictionary<string, string>>();
            foreach (var line in sortedRows)
            {
                var groupLine = true;
                foreach (var groupedRow in groupedRows)
                {
                    var sameRow = true;
                    foreach (var col in groupedRow.Keys)
                    {
                        if (!_groupExceptColumns.Contains(col) && line[col] != groupedRow[col])
                        {
                            sameRow = false;
                            break;
                        }
                    }

                    if (sameRow)
                    {
                        //Check if we need to group by age
                        bool lineAdultPrice, lineChildPrice, groupedAdultPrice, groupedChildPrice;
                        PaxApplyPrice(line["Minimum age"], line["Maximum age"], out lineAdultPrice, out lineChildPrice);
                        PaxApplyPrice(groupedRow["Minimum age"], groupedRow["Maximum age"], out groupedAdultPrice, out groupedChildPrice);

                        if (lineAdultPrice == groupedAdultPrice || lineChildPrice == groupedChildPrice)
                        {
                            var vOk = (!String.IsNullOrEmpty(line["Amount"]) == !String.IsNullOrEmpty(groupedRow["Amount"]));
                            vOk = vOk && (!String.IsNullOrEmpty(line["Percentage"]) == !String.IsNullOrEmpty(groupedRow["Percentage"]));
                            if (!vOk)
                            {
                                if (
                                    (!String.IsNullOrEmpty(line["Percentage"]) && !line["Percentage"].Equals("-100.000") && !line["Percentage"].Equals("-100.0") && !line["Percentage"].Equals("-100.00"))
                                    || (!String.IsNullOrEmpty(groupedRow["Percentage"]) && !groupedRow["Percentage"].Equals("-100.000") && !groupedRow["Percentage"].Equals("-100.0") && !groupedRow["Percentage"].Equals("-100.00"))
                                )
                                {
                                    throw new ApplicationException("Group rows per age mismatch");
                                }
                                if (!String.IsNullOrEmpty(line["Amount"]))
                                {
                                    groupedRow["Amount"] = "0";
                                    groupedRow["Percentage"] = String.Empty;
                                }
                                else
                                {
                                    line["Amount"] = "0";
                                    line["Percentage"] = String.Empty;
                                }
                            }

                            var adultPrice = lineAdultPrice || groupedAdultPrice;
                            var childPrice = lineChildPrice || groupedChildPrice;

                            if (adultPrice && !childPrice)
                            {
                                groupedRow["Minimum age"] = HotelBedsSettings.SunHotelsAdultAgeBooking.ToString();
                                groupedRow["Maximum age"] = HotelBedsSettings.SunHotelsAdultAgeBooking.ToString();
                            }
                            else if (!adultPrice && childPrice)
                            {
                                groupedRow["Minimum age"] = HotelBedsSettings.SunHotelsChildAgeBooking.ToString();
                                groupedRow["Maximum age"] = HotelBedsSettings.SunHotelsChildAgeBooking.ToString();
                            }
                            else
                            {
                                groupedRow["Minimum age"] = HotelBedsSettings.SunHotelsChildAgeBooking.ToString();
                                groupedRow["Maximum age"] = HotelBedsSettings.SunHotelsAdultAgeBooking.ToString();
                            }

                            if (!String.IsNullOrEmpty(line["Amount"]))
                            {
                                var lineAmount = Decimal.Parse(line["Amount"], NumberFormatInfo.InvariantInfo);
                                var groupedAmount = Decimal.Parse(groupedRow["Amount"], NumberFormatInfo.InvariantInfo);
                                groupedRow["Amount"] = (lineAmount > groupedAmount ? lineAmount : groupedAmount).ToString(NumberFormatInfo.InvariantInfo);
                            }
                            else
                            {
                                var linePercentage = Decimal.Parse(line["Percentage"], NumberFormatInfo.InvariantInfo);
                                var groupedPercentage = Decimal.Parse(groupedRow["Percentage"], NumberFormatInfo.InvariantInfo);
                                groupedRow["Percentage"] = (linePercentage > groupedPercentage ? linePercentage : groupedPercentage).ToString(NumberFormatInfo.InvariantInfo);
                            }

                            groupLine = false;
                            break;
                        }
                    }
                }
                if (groupLine)
                {
                    groupedRows.Add(line);
                }
            }
            return groupedRows;
        }

        #endregion

        #region Supplements functions

        public void ProcessSupplement(HotelBedsSupplementsParams p)
        {
            var genericTableCombinations = new Dictionary<DateTime, Dictionary<string, bool>>();
            var countTypeGeneral = 0;

            //Group supplements for process them later only if all of them have the same values
            var supplementsGroup = new Dictionary<DateTime, Dictionary<Room, List<Dictionary<string, string>>>>();
            var typeSupplementProcessed = new Dictionary<DateTime, Dictionary<string, Dictionary<string, List<string>>>>();

            //Pre-process child supplements to know how many childs are transformed to adults
            PreProcessChildSupplement(p);

            //Validate supplements
            foreach (var supplementOrder in p.SortedSupplements.Keys)
            {
                var line = p.SortedSupplements[supplementOrder];
                var applicationStartDate = ParseDate(line["Application initial date"]);
                var applicationFinalDate = !String.IsNullOrEmpty(line["Application final date"]) ? ParseDate(line["Application final date"]) : p.CacheEnd;
                if (String.IsNullOrEmpty(line["Application initial date"]) || applicationStartDate > p.TodayDate || p.TodayDate > applicationFinalDate)
                {
                    LogLine(p.LogList, p.HotelIdName, "supplement not applied filtered by application dates", line["Application initial date"]);
                    continue;
                }
                var txtRoomTypeCode = line["Room type"];
                var txtRoomCharacteristic = line["Characteristic"];
                var txtRate = line["Rate"];
                var txtBoardCode = line["Board"];
                var supplementDates = SupplementDates(p.HotelSupplementDates, line, p.CacheIni, p.CacheEnd, p.ContractDates);
                var supplementCode = line["Supplement or discount code"];
                var supplementType = line["Type"];
                var isPerPax = line["Is per pax"].Equals("Y");
                var applyOpaque = line["Opaque"].Equals("Y");
                var applicationType = line["Application type"];
                var txtAmount = line["Amount"];
                var txtPercentage = line["Percentage"];
                var isCumulative = line["Is cumulative"].Equals("Y");
                var adults = line["Adults"];
                var paxOrder = line["Pax order"];
                var minimumAge = line["Minimum age"];
                var maximumAge = line["Maximum age"];
                var numberOfDays = line["Number of days"];
                var lengthOfStay = line["Length of stay"];
                var limitDate = line["Limit date"];
                var onMonday = line["On Monday"].Equals("Y");
                var onTuesday = line["On Tuesday"].Equals("Y");
                var onWednesday = line["On Wednesday"].Equals("Y");
                var onThursday = line["On Thursday"].Equals("Y");
                var onFriday = line["On Friday"].Equals("Y");
                var onSaturday = line["On Saturday"].Equals("Y");
                var onSunday = line["On Sunday"].Equals("Y");
                var netPrice = line["Net price"];
                var price = line["Price"];
                var txtMarket = line["Market price"];

                var occupancySupplement = supplementType.Equals("N") || supplementType.Equals("C") || supplementType.Equals("I");

                var positiveValue = SupplementPositiveValue(txtAmount, txtPercentage, occupancySupplement, applicationType);

                if (
                    !_supTypes.Contains(supplementType) || !_appTypes.Contains(applicationType) || supplementType.Equals("F") //Infants
                    || (applyOpaque && !p.Opaque.Equals("P")) || (String.IsNullOrEmpty(txtAmount) && String.IsNullOrEmpty(txtPercentage))
                    || (!String.IsNullOrEmpty(txtBoardCode) && !HotelBedsSettings.MapMeals.ContainsKey(txtBoardCode))
                    || (isCumulative && !positiveValue)
                    || (supplementType.Equals("N") && minimumAge.Equals("0") && (maximumAge.Equals("1") || maximumAge.Equals("2") || maximumAge.Equals("0")) && (txtPercentage.Equals("-100.000") || txtPercentage.Equals("-100.0") || txtPercentage.Equals("-100.00")))
                )
                {
                    //Don't log infant supplements with 100% discount
                    if (!(supplementType.Equals("F") || supplementType.Equals("N")) && !txtPercentage.Equals("-100.000") && !txtPercentage.Equals("-100.0") && !txtPercentage.Equals("-100.00"))
                    {
                        LogLine(p.LogList, p.HotelIdName, "supplement not supported", supplementType);
                    }
                    continue;
                }

                if (!positiveValue && (applicationType.Equals("U") || applicationType.Equals("T")))
                {
                    //Negative price and application type 'U' or 'T'
                    LogLine(p.LogList, p.HotelIdName, "supplement application type restriction ignored", applicationType);
                    continue;
                }

                if (!String.IsNullOrEmpty(txtMarket) && !p.ContractMarkets.Contains(txtMarket))
                {
                    LogLine(p.LogList, p.HotelIdName, "supplement market ignored", txtMarket);
                    continue;
                }

                //Check combinable offers
                var lineCombOfferList = new Dictionary<object, int>();
                if (!occupancySupplement && CombinationOfferCodeExists(p.CombOffers, supplementCode))
                {
                    CombinationExcludeCheckList(p.CombOffers, p.HotelSupplementDatesApply, line, supplementCode, lineCombOfferList, true);
                    CombinationExcludeCheckList(p.CombOffers, p.HotelFreeDatesApply, line, supplementCode, lineCombOfferList, false);
                }

                bool adultPrice;
                bool childPrice;
                PaxApplyPrice(minimumAge, maximumAge, out adultPrice, out childPrice);

                var discountPeriodId = 0;
                DateTime? discountDateLimit = null;
                if (!occupancySupplement && supplementDates.Count > 0)
                {
                    discountPeriodId = GetNextDiscountPeriodId();
                    discountDateLimit = (new List<DateTime>(supplementDates))[supplementDates.Count - 1];
                }

                var rowCombinations = RowCombinations(p.DistinctCombinations, txtRoomTypeCode, txtRoomCharacteristic, txtRate, txtBoardCode, txtMarket, p.SortedRates, p.RoomTypesTable, p.BoardsList, p.ContractMarkets);

                if (occupancySupplement)
                {
                    rowCombinations = AddCombinations(rowCombinations, line, _occupancyCombinations);
                }
                else if (!supplementType.Equals("G"))
                {
                    rowCombinations = AddCombinations(rowCombinations, line, _typeCombinations);
                }
                else
                {
                    //Type general can be applied many times
                    countTypeGeneral += 1;
                    rowCombinations = AddCombinations(rowCombinations, new Dictionary<string, string> { { _typeGeneralCombination[0], "GEN" + countTypeGeneral } }, _typeGeneralCombination);
                }

                int? stayLength = null;
                int? dateLimitCompareResult = null;
                DateTime? daysNumberDate = null;
                if (!occupancySupplement && !supplementType.Equals("G"))
                {
                    if (!String.IsNullOrEmpty(lengthOfStay))
                    {
                        stayLength = Int32.Parse(lengthOfStay);
                    }
                    if (!String.IsNullOrEmpty(limitDate))
                    {
                        dateLimitCompareResult = DateTime.Compare(p.TodayDate, ParseDate(limitDate));
                    }
                    if (!String.IsNullOrEmpty(numberOfDays))
                    {
                        daysNumberDate = p.TodayDate.AddDays(Int32.Parse(numberOfDays));
                    }
                }

                foreach (var date in supplementDates)
                {
                    if (date.DayOfWeek.Equals(DayOfWeek.Monday) && !onMonday) continue;
                    if (date.DayOfWeek.Equals(DayOfWeek.Tuesday) && !onTuesday) continue;
                    if (date.DayOfWeek.Equals(DayOfWeek.Wednesday) && !onWednesday) continue;
                    if (date.DayOfWeek.Equals(DayOfWeek.Thursday) && !onThursday) continue;
                    if (date.DayOfWeek.Equals(DayOfWeek.Friday) && !onFriday) continue;
                    if (date.DayOfWeek.Equals(DayOfWeek.Saturday) && !onSaturday) continue;
                    if (date.DayOfWeek.Equals(DayOfWeek.Sunday) && !onSunday) continue;

                    var combNotProcessed = FilterCombinations(genericTableCombinations, rowCombinations, date);
                    foreach (var cnp in combNotProcessed)
                    {
                        Dictionary<string, string> fields;
                        if (occupancySupplement)
                        {
                            fields = SplitCombination(p.SplittedCombinations, cnp, _occupancyCombinations);
                        }
                        else if (!supplementType.Equals("G"))
                        {
                            fields = SplitCombination(p.SplittedCombinations, cnp, _typeCombinations);
                        }
                        else
                        {
                            fields = SplitCombination(p.SplittedCombinations, cnp, _typeGeneralCombination);
                        }
                        var marketCode = fields["Market"];
                        var roomTypeCode = fields["Room type"];
                        var roomCharacteristic = fields["Characteristic"];
                        var rate = fields["Rate"];
                        var boardCode = fields["Board"];
                        var roomRate = p.FixRate ? rate : HotelBedsSettings.GenericRateCode;

                        //Check combinable offers per day
                        if (!occupancySupplement && !positiveValue && lineCombOfferList.Count > 0)
                        {
                            var cApply = CombinationOfferApply(p.CombOffers, p.HotelSupplementDatesApply, p.HotelFreeDatesApply, lineCombOfferList, supplementCode, date, marketCode, roomTypeCode, roomCharacteristic, boardCode, rate, true);
                            if (!cApply)
                            {
                                continue;
                            }
                        }
                        //Check general supplements except type "G"
                        if (!occupancySupplement && !supplementType.Equals("G"))
                        {
                            if (!SupplementGeneralValidateDate(supplementType, date, daysNumberDate, stayLength, dateLimitCompareResult))
                            {
                                continue;
                            }

                            if (
                                !p.HotelGeneralDatesApply.ContainsKey(supplementType)
                                || !p.HotelGeneralDatesApply[supplementType].ContainsKey(date)
                                || !p.HotelGeneralDatesApply[supplementType][date].ContainsKey(cnp)
                            )
                            {
                                continue;
                            }
                        }

                        var roomGroupId = CreateRoomGroupId(p.ContractRoomId, roomTypeCode, roomCharacteristic, roomRate, p.ContractRoomIdType);
                        var roomList = RoomList(p.BaseRooms, p.BaseRoomGroups, roomGroupId, p.BaseRoomsIndex);

                        foreach (var room in roomList)
                        {
                            if (
                                !p.BaseRoomsPrice.ContainsKey(room.RoomId) || !p.BaseRoomsPrice[room.RoomId].ContainsKey(date)
                                || (!String.IsNullOrEmpty(rate) && !p.BaseRoomsPriceRate[room.RoomId][date].Equals(rate))
                            )
                            {
                                continue;
                            }

                            if (!room.AvailabilityPeriods.ContainsKey(date))
                            {
                                LogLine(p.LogList, p.HotelIdName, "availability not created", "AvailabilityPeriods doesn't contain " + date.ToShortDateString());
                                continue;
                            }
                            var ap = room.AvailabilityPeriods[date];
                            if (ap.AvailabilityAvailableUnits == 0)
                            {
                                continue;
                            }

                            var roomAdults = p.BaseRoomTypes[room.TypeId].Beds;
                            var roomChilds = p.BaseRoomTypes[room.TypeId].ExtraBeds;
                            
                            if (
                                (supplementType.Equals("N") && !childPrice) //Childs
                                || ((supplementType.Equals("C") || supplementType.Equals("I")) && !adultPrice) //Adults
                                || (!occupancySupplement && !adultPrice && childPrice && roomChilds == 0)
                            )
                            {
                                LogLine(p.LogList, p.HotelIdName, "supplement age restriction ignored", supplementType);
                                continue;
                            }

                            if (!adultPrice && !childPrice) //Senior prices or infants
                            {
                                LogLine(p.LogList, p.HotelIdName, "supplement age restriction", "adultPrice:" + adultPrice + ", childPrice:" + childPrice);
                                ap.AvailabilityAvailableUnits = 0;
                                continue;
                            }

                            if (occupancySupplement)
                            {
                                var groupSupplementsByDateRoom = false;
                                int orderPax;
                                if (!Int32.TryParse(paxOrder, out orderPax))
                                {
                                    orderPax = 0;
                                }

                                //If there is no supplement for a child (type 'N') then this child is considered adult for the additional bed (type 'C') or single use (type 'I') supplement...
                                //Number of HB Childs transformed to HB Adults
                                var supChToAd = SupplementChildToAdult(p.SupplementChildPaxApplied, date, room.RoomId, roomChilds);

                                var adultsCheck = roomAdults + supChToAd;

                                //Child supplement
                                if (supplementType.Equals("N"))
                                {
                                    if (
                                        orderPax == 0
                                        || roomChilds == 0
                                        || (!String.IsNullOrEmpty(adults) && Int32.Parse(adults) > adultsCheck)
                                        || orderPax > roomChilds
                                    )
                                    {
                                        continue;
                                    }
                                    groupSupplementsByDateRoom = true;
                                }

                                //Additional bed supplement
                                if (supplementType.Equals("C"))
                                {
                                    if (orderPax == 0 || adultsCheck <= p.BaseRoomsCapacity[room.RoomId] || orderPax <= p.BaseRoomsCapacity[room.RoomId])
                                    {
                                        continue;
                                    }
                                    groupSupplementsByDateRoom = true;
                                }

                                //Single use supplement
                                if (supplementType.Equals("I"))
                                {
                                    if (orderPax == 0 || adultsCheck >= p.BaseRoomsCapacity[room.RoomId] || orderPax >= p.BaseRoomsCapacity[room.RoomId])
                                    {
                                        continue;
                                    }
                                    groupSupplementsByDateRoom = true;
                                }

                                if (groupSupplementsByDateRoom)
                                {
                                    var addLine = true;
                                    if (!supplementsGroup.ContainsKey(date))
                                    {
                                        supplementsGroup.Add(date, new Dictionary<Room, List<Dictionary<string, string>>>());
                                    }
                                    if (!supplementsGroup[date].ContainsKey(room))
                                    {
                                        supplementsGroup[date].Add(room, new List<Dictionary<string, string>>());
                                    }
                                    foreach (var otherLine in supplementsGroup[date][room])
                                    {
                                        //General check
                                        if (
                                            supplementType.Equals(otherLine["Type"])
                                            && !String.IsNullOrEmpty(txtAmount) == !String.IsNullOrEmpty(otherLine["Amount"])
                                            && !String.IsNullOrEmpty(txtPercentage) == !String.IsNullOrEmpty(otherLine["Percentage"])
                                            && boardCode.Equals(otherLine["Board"])
                                            && rate.Equals(otherLine["Rate"])
                                        )
                                        {
                                            //Check application type. "N" Night price, "B" Base price and "R" Board price must be grouped
                                            if (!applicationType.Equals(otherLine["Application type"]))
                                            {
                                                if (
                                                    (!applicationType.Equals("N") && !applicationType.Equals("B") && !applicationType.Equals("R"))
                                                    || (!otherLine["Application type"].Equals("N") && !otherLine["Application type"].Equals("B") && !otherLine["Application type"].Equals("R"))
                                                )
                                                {
                                                    continue;
                                                }
                                                otherLine["Application type"] = (applicationType.Equals("R") || otherLine["Application type"].Equals("R")) ? "R" : "B";
                                            }

                                            if (!String.IsNullOrEmpty(txtAmount))
                                            {
                                                var amount = Decimal.Parse(txtAmount, NumberFormatInfo.InvariantInfo);
                                                var otherAmount = Decimal.Parse(otherLine["Amount"], NumberFormatInfo.InvariantInfo);
                                                otherLine["Amount"] = (amount > otherAmount ? amount : otherAmount).ToString(NumberFormatInfo.InvariantInfo);
                                            }
                                            else
                                            {
                                                var percentage = Decimal.Parse(txtPercentage, NumberFormatInfo.InvariantInfo);
                                                var otherPercentage = Decimal.Parse(otherLine["Percentage"], NumberFormatInfo.InvariantInfo);
                                                otherLine["Percentage"] = (percentage > otherPercentage ? percentage : otherPercentage).ToString(NumberFormatInfo.InvariantInfo);
                                            }
                                            addLine = false;
                                            break;
                                        }
                                    }
                                    if (addLine)
                                    {
                                        var compare = new Dictionary<string, string>();
                                        compare.Add("Type", supplementType);
                                        compare.Add("Amount", txtAmount);
                                        compare.Add("Percentage", txtPercentage);
                                        compare.Add("Board", boardCode);
                                        compare.Add("Rate", rate);
                                        compare.Add("Application type", applicationType);
                                        supplementsGroup[date][room].Add(compare);
                                    }
                                }
                            }
                            else
                            {
                                if (
                                    !positiveValue
                                    && (
                                        (supplementType.Equals("E") && applicationType.Equals("R"))
                                        || supplementType.Equals("L") || supplementType.Equals("M") || supplementType.Equals("V")
                                        //Type "L", "M" and "V" applies to "All nights of stay" and we don't support this
                                    )
                                )
                                {
                                    continue;
                                }

                                //Other supplements
                                var generalParams = new HotelBedsSupplementsGeneral
                                {
                                    SupplementsParams = p,
                                    TxtAmount = txtAmount,
                                    TxtPercentage = txtPercentage,
                                    TxtBoardCode = txtBoardCode,
                                    SupplementType = supplementType,
                                    ApplicationType = applicationType,
                                    IsPerPax = isPerPax,
                                    LengthOfStay = lengthOfStay,
                                    LimitDate = limitDate,
                                    TypeSupplementProcessed = typeSupplementProcessed,
                                    Date = date,
                                    Room = room,
                                    AvailabilityPeriod = ap,
                                    Rate = rate,
                                    BoardCode = boardCode,
                                    IsAdultPrice = adultPrice,
                                    IsChildPrice = childPrice,
                                    DiscountPeriodId = discountPeriodId,
                                    DiscountLimitDate = discountDateLimit
                                };
                                SupplementGeneral(generalParams);
                            }
                        }

                        //Mark combination processed
                        genericTableCombinations[date][cnp] = true;
                    }
                }
            }

            //Apply occupancy supplements
            SupplementOccupancyGroup(p, supplementsGroup);
        }

        private void SupplementOccupancyGroup(
            HotelBedsSupplementsParams p,
            Dictionary<DateTime, Dictionary<Room, List<Dictionary<string, string>>>> supplementsGroup
        )
        {
            if (
                (p.SupplementType.Equals("N") || p.SupplementType.Equals("C") || p.SupplementType.Equals("I"))
                && supplementsGroup.Count > 0
            )
            {
                foreach (var date in supplementsGroup.Keys)
                {
                    foreach (var room in supplementsGroup[date].Keys)
                    {
                        for (var i = 0; i < supplementsGroup[date][room].Count; i++)
                        {
                            var line = supplementsGroup[date][room][i];
                            var txtAmount = line["Amount"];
                            var txtPercentage = line["Percentage"];
                            var supplementType = line["Type"];
                            var applicationType = line["Application type"];
                            if (p.SupplementType.Equals(supplementType))
                            {
                                var rate = line["Rate"];
                                var boardCode = line["Board"];

                                if (p.BaseRoomsPriceRate[room.RoomId][date].Equals(rate))
                                {
                                    var ap = room.AvailabilityPeriods[date];
                                    var foundBoard = false;
                                    if (ap.Pricing_BookableBoardId.Equals(boardCode))
                                    {
                                        foundBoard = true;
                                    }
                                    else
                                    {
                                        foreach (var addBoard in ap.AdditionalBoards)
                                        {
                                            if (addBoard.BookableBoardId.Equals(boardCode))
                                            {
                                                foundBoard = true;
                                                break;
                                            }
                                        }
                                    }
                                    if (foundBoard)
                                    {
                                        //Number of HB Childs transformed to HB Adults
                                        var supChToAd = SupplementChildToAdult(p.SupplementChildPaxApplied, date, room.RoomId, p.BaseRoomTypes[room.TypeId].ExtraBeds);

                                        var supAppPriceOcc = new HotelBedsSupplementsPriceParams
                                        {
                                            BaseRoomTypes = p.BaseRoomTypes,
                                            BaseRoomsCapacity = p.BaseRoomsCapacity,
                                            BaseRoomsPrice = p.BaseRoomsPrice,
                                            BaseRoomsPriceIsPerPax = p.BaseRoomsPriceIsPerPax,
                                            BaseRoomsPriceBoard = p.BaseRoomsPriceBoard,
                                            BaseRoomsPriceBoardIsPerPax = p.BaseRoomsPriceBoardIsPerPax,
                                            Date = date,
                                            Room = room,
                                            SupplementType = p.SupplementType,
                                            Rate = rate,
                                            BoardCode = boardCode,
                                            TxtAmount = txtAmount,
                                            TxtPercentage = txtPercentage,
                                            ApplicationType = applicationType,
                                            SupplementChildToAdult = supChToAd
                                        };
                                        SupplementApplyPriceOcuppancy(supAppPriceOcc);
                                    }
                                }
                            }
                            else
                            {
                                LogLine(p.LogList, p.HotelIdName, "supplement conditions mismatch", p.SupplementType + "<>" + supplementType);
                            }
                        }
                    }
                }
            }
        }

        private void SupplementApplyPriceOcuppancy(HotelBedsSupplementsPriceParams p)
        {
            var ap = p.Room.AvailabilityPeriods[p.Date];

            if (
                (!ap.Pricing_BookableBoardId.Equals(p.BoardCode) && !p.ApplicationType.Equals("R"))
                || (ap.Pricing_BookableBoardId.Equals(p.BoardCode) && p.ApplicationType.Equals("R"))
            )
            {
                return;
            }

            decimal? amount = null;
            decimal? percentage = null;
            if (!String.IsNullOrEmpty(p.TxtAmount))
            {
                amount = Decimal.Parse(p.TxtAmount, NumberFormatInfo.InvariantInfo);
            }
            else
            {
                percentage = Decimal.Parse(p.TxtPercentage, NumberFormatInfo.InvariantInfo);
            }

            //Occupancy
            var stdCapacity = p.BaseRoomsCapacity[p.Room.RoomId];
            var adults = p.BaseRoomTypes[p.Room.TypeId].Beds;
            var childs = p.BaseRoomTypes[p.Room.TypeId].ExtraBeds;

            //Room base price
            var baseIsPerPax = p.BaseRoomsPriceIsPerPax[p.Room.RoomId][p.Date];
            var basePrice = p.BaseRoomsPrice[p.Room.RoomId][p.Date][0].Value;
            var basePriceChild = p.BaseRoomsPrice[p.Room.RoomId][p.Date][1];
            if (baseIsPerPax && adults > 1)
            {
                basePrice = Decimal.Divide(basePrice, adults);
            }
            if (!baseIsPerPax && childs > 0 && basePriceChild.HasValue && basePriceChild == Decimal.Zero && p.SupplementType.Equals("N"))
            {
                basePriceChild = basePrice;
            }

            //Board price
            var basePriceBoardIsPerPax = false;
            var basePriceBoard = Decimal.Zero;
            var basePriceBoardChild = Decimal.Zero;
            if (
                p.BaseRoomsPriceBoard.ContainsKey(p.Room.RoomId)
                && p.BaseRoomsPriceBoard[p.Room.RoomId].ContainsKey(p.Date)
                && p.BaseRoomsPriceBoard[p.Room.RoomId][p.Date].ContainsKey(p.Rate)
                && p.BaseRoomsPriceBoard[p.Room.RoomId][p.Date][p.Rate].ContainsKey(p.BoardCode)
            )
            {
                basePriceBoardIsPerPax = p.BaseRoomsPriceBoardIsPerPax[p.Room.RoomId][p.Date][p.Rate][p.BoardCode];
                basePriceBoard = p.BaseRoomsPriceBoard[p.Room.RoomId][p.Date][p.Rate][p.BoardCode][0].Value;
                if (basePriceBoardIsPerPax)
                {
                    basePriceBoard = Decimal.Divide(basePriceBoard, adults) - (baseIsPerPax ? basePrice : Decimal.Divide(basePrice, adults));
                }
                else
                {
                    basePriceBoard = basePriceBoard - (baseIsPerPax ? (basePrice * adults) : basePrice);
                }
                if (basePriceChild.HasValue && p.BaseRoomsPriceBoard[p.Room.RoomId][p.Date][p.Rate][p.BoardCode][1].HasValue)
                {
                    basePriceBoardChild = p.BaseRoomsPriceBoard[p.Room.RoomId][p.Date][p.Rate][p.BoardCode][1].Value - basePriceChild.Value;
                }
            }

            //We select which base price must be processed
            decimal priceBase;
            decimal priceBaseBoard;
            if (p.SupplementType.Equals("N"))
            {
                if (!basePriceChild.HasValue)
                {
                    return;
                }
                priceBase = basePriceChild.Value;
                priceBaseBoard = basePriceBoardChild;
            }
            else
            {
                priceBase = basePrice;
                priceBaseBoard = basePriceBoard;
            }

            decimal? percentPrice = null;
            decimal? percentPriceBoard = null;
            if (percentage.HasValue)
            {
                percentPrice = Decimal.Divide(priceBase * percentage.Value, 100);
                percentPriceBoard = Decimal.Divide(priceBaseBoard * percentage.Value, 100);
            }

            //This calculations are extracted form the documentation
            decimal supPrice;
            if (baseIsPerPax)
            {
                if (basePriceBoardIsPerPax)
                {
                    switch (p.ApplicationType)
                    {
                        case "B":
                            if (amount.HasValue)
                            { supPrice = (priceBase + amount.Value) + priceBaseBoard; }
                            else
                            { supPrice = (priceBase + percentPrice.Value) + priceBaseBoard; }
                            break;

                        case "R":
                            if (amount.HasValue)
                            { supPrice = priceBase + (priceBaseBoard + amount.Value); }
                            else
                            { supPrice = priceBase + (priceBaseBoard + percentPriceBoard.Value); }
                            break;

                        case "A":
                        case "M": //Absolute amount + board supplement
                            if (amount.HasValue)
                            { supPrice = amount.Value; }
                            else
                            { supPrice = Decimal.Divide((priceBase + priceBaseBoard) * percentage.Value, 100); }
                            if (p.ApplicationType == "M") { supPrice += priceBaseBoard; }
                            break;

                        default:
                            //'N', 'U', 'T'
                            if (amount.HasValue)
                            { supPrice = (priceBase + amount.Value) + priceBaseBoard; }
                            else
                            { supPrice = (priceBase + percentPrice.Value) + (priceBaseBoard + percentPriceBoard.Value); }
                            break;
                    }
                }
                else
                {
                    switch (p.ApplicationType)
                    {
                        case "B":
                            if (amount.HasValue)
                            { supPrice = (priceBase + amount.Value) + Decimal.Divide(priceBaseBoard, stdCapacity); }
                            else
                            { supPrice = (priceBase + percentPrice.Value) + Decimal.Divide(priceBaseBoard, stdCapacity); }
                            break;

                        case "R":
                            if (amount.HasValue)
                            { supPrice = priceBase + (Decimal.Divide(priceBaseBoard, stdCapacity) + amount.Value); }
                            else
                            { supPrice = priceBase + (Decimal.Divide(priceBaseBoard, stdCapacity) + Decimal.Divide(percentPriceBoard.Value, stdCapacity)); }
                            break;

                        case "A":
                        case "M": //Absolute amount + board supplement
                            if (amount.HasValue)
                            { supPrice = amount.Value; }
                            else
                            { supPrice = Decimal.Divide((priceBase + Decimal.Divide(priceBaseBoard, stdCapacity)) * percentage.Value, 100); }
                            if (p.ApplicationType == "M") { supPrice += priceBaseBoard; }
                            break;

                        default:
                            //'N', 'U', 'T'
                            if (amount.HasValue)
                            { supPrice = (priceBase + amount.Value) + Decimal.Divide(priceBaseBoard, stdCapacity); }
                            else
                            { supPrice = (priceBase + percentPrice.Value) + (Decimal.Divide(priceBaseBoard, stdCapacity) + Decimal.Divide(percentPriceBoard.Value, stdCapacity)); }
                            break;
                    }
                }
            }
            else
            {
                if (basePriceBoardIsPerPax)
                {
                    switch (p.ApplicationType)
                    {
                        case "B":
                            if (amount.HasValue)
                            { supPrice = (Decimal.Divide(priceBase, stdCapacity) + amount.Value) + priceBaseBoard; }
                            else
                            { supPrice = (Decimal.Divide(priceBase, stdCapacity) + Decimal.Divide(percentPrice.Value, stdCapacity)) + priceBaseBoard; }
                            break;

                        case "R":
                            if (amount.HasValue)
                            { supPrice = Decimal.Divide(priceBase, stdCapacity) + (priceBaseBoard + amount.Value); }
                            else
                            { supPrice = Decimal.Divide(priceBase, stdCapacity) + (priceBaseBoard + percentPriceBoard.Value); }
                            break;

                        case "A":
                        case "M": //Absolute amount + board supplement
                            if (amount.HasValue)
                            { supPrice = amount.Value; }
                            else
                            { supPrice = Decimal.Divide((Decimal.Divide(priceBase, stdCapacity) + priceBaseBoard) * percentage.Value, 100); }
                            if (p.ApplicationType == "M") { supPrice += priceBaseBoard; }
                            break;

                        default:
                            //'N', 'U', 'T'
                            if (amount.HasValue)
                            { supPrice = (Decimal.Divide(priceBase, stdCapacity) + amount.Value) + priceBaseBoard; }
                            else
                            { supPrice = (Decimal.Divide(priceBase, stdCapacity) + Decimal.Divide(percentPrice.Value, stdCapacity)) + (priceBaseBoard + percentPriceBoard.Value); }
                            break;
                    }
                }
                else
                {
                    switch (p.ApplicationType)
                    {
                        case "B":
                            if (amount.HasValue)
                            { supPrice = (Decimal.Divide(priceBase, stdCapacity) + amount.Value) + Decimal.Divide(priceBaseBoard, stdCapacity); }
                            else
                            { supPrice = (Decimal.Divide(priceBase, stdCapacity) + Decimal.Divide(percentPrice.Value, stdCapacity)) + Decimal.Divide(priceBaseBoard, stdCapacity); }
                            break;

                        case "R":
                            if (amount.HasValue)
                            { supPrice = Decimal.Divide(priceBase, stdCapacity) + (Decimal.Divide(priceBaseBoard, stdCapacity) + amount.Value); }
                            else
                            { supPrice = Decimal.Divide(priceBase, stdCapacity) + (Decimal.Divide(priceBaseBoard, stdCapacity) + Decimal.Divide(percentPriceBoard.Value, stdCapacity)); }
                            break;

                        case "A":
                        case "M": //Absolute amount + board supplement
                            if (amount.HasValue)
                            { supPrice = amount.Value; }
                            else
                            { supPrice = Decimal.Divide((Decimal.Divide(priceBase, stdCapacity) + Decimal.Divide(priceBaseBoard, stdCapacity)) * percentage.Value, 100); }
                            if (p.ApplicationType == "M") { supPrice += priceBaseBoard; }
                            break;

                        default:
                            //'N', 'U', 'T'
                            if (amount.HasValue)
                            { supPrice = (Decimal.Divide(priceBase, stdCapacity) + amount.Value) + Decimal.Divide(priceBaseBoard, stdCapacity); }
                            else
                            { supPrice = (Decimal.Divide(priceBase, stdCapacity) + Decimal.Divide(percentPrice.Value, stdCapacity)) + (Decimal.Divide(priceBaseBoard, stdCapacity) + Decimal.Divide(percentPriceBoard.Value, stdCapacity)); }
                            break;
                    }
                }
            }

            //supPrice is price per pax
            if (supPrice < 0)
            {
                supPrice = 0;
            }

            if (ap.Pricing_BookableBoardId.Equals(p.BoardCode))
            {
                if (!p.SupplementType.Equals("N"))
                {
                    adults += p.SupplementChildToAdult;
                }
                switch (p.SupplementType)
                {
                    case "N": ap.Pricing_ExtrabedChild = supPrice.ToString(NumberFormatInfo.InvariantInfo); break;
                    case "C": ap.Pricing_BasePrice = (Decimal.Parse(ap.Pricing_BasePrice, NumberFormatInfo.InvariantInfo) + (supPrice * (adults - stdCapacity))).ToString(NumberFormatInfo.InvariantInfo); break;
                    case "I": ap.Pricing_BasePrice = (Decimal.Parse(ap.Pricing_BasePrice, NumberFormatInfo.InvariantInfo) + (supPrice * (stdCapacity - adults))).ToString(NumberFormatInfo.InvariantInfo); break;
                }
            }
            else
            {
                foreach (var addBoard in ap.AdditionalBoards)
                {
                    if (addBoard.BookableBoardId.Equals(p.BoardCode))
                    {
                        switch (p.SupplementType)
                        {
                            case "N": addBoard.Child = (supPrice).ToString(NumberFormatInfo.InvariantInfo); break;
                            case "C": addBoard.Adult = (supPrice).ToString(NumberFormatInfo.InvariantInfo); break;
                            case "I": addBoard.Adult = (supPrice).ToString(NumberFormatInfo.InvariantInfo); break;
                        }
                        break;
                    }
                }
            }
        }

        public bool SupplementPositiveValue(string txtAmount, string txtPercentage, bool occupancySupplement, string applicationType)
        {
            //Amount
            if (!String.IsNullOrEmpty(txtAmount))
            {
                return !txtAmount[0].Equals('-');
            }

            //Percent
            if (occupancySupplement)
            {
                return (!applicationType.Equals("A") && !applicationType.Equals("M"))
                    || ((applicationType.Equals("A") || applicationType.Equals("M")) && !txtPercentage[0].Equals('-'));
            }
            return !txtPercentage[0].Equals('-');
        }

        private int SupplementChildToAdult(Dictionary<DateTime, Dictionary<string, List<int>>> supplementChildPaxApplied, DateTime date, string roomId, int extraBeds)
        {
            var supChToAd = 0;
            if (
                supplementChildPaxApplied.ContainsKey(date)
                && supplementChildPaxApplied[date].ContainsKey(roomId)
                && supplementChildPaxApplied[date][roomId].Count > 0
            )
            {
                if (extraBeds > supplementChildPaxApplied[date][roomId].Count)
                {
                    supChToAd = extraBeds - supplementChildPaxApplied[date][roomId].Count;
                }
            }
            else if (extraBeds > 0)
            {
                supChToAd = extraBeds;
            }
            return supChToAd;
        }

        private void PreProcessChildSupplement(HotelBedsSupplementsParams p)
        {
            //If there is no supplement for a child (type 'N') then this child is considered adult for the additional bed (type 'C') or single use (type 'I') supplement...
            if (p.SupplementType.Equals("N") && p.SortedSupplements.Count > 0)
            {
                var genericTableCombinations = new Dictionary<DateTime, Dictionary<string, bool>>();

                foreach (var supplementOrder in p.SortedSupplements.Keys)
                {
                    var line = p.SortedSupplements[supplementOrder];
                    var applicationStartDate = ParseDate(line["Application initial date"]);
                    var applicationFinalDate = !String.IsNullOrEmpty(line["Application final date"]) ? ParseDate(line["Application final date"]) : p.CacheEnd;
                    if (String.IsNullOrEmpty(line["Application initial date"]) || applicationStartDate > p.TodayDate || p.TodayDate > applicationFinalDate)
                    {
                        continue;
                    }
                    var txtRoomTypeCode = line["Room type"];
                    var txtRoomCharacteristic = line["Characteristic"];
                    var txtRate = line["Rate"];
                    var txtBoardCode = line["Board"];
                    var supplementDates = SupplementDates(p.HotelSupplementDates, line, p.CacheIni, p.CacheEnd, p.ContractDates);
                    var applyOpaque = line["Opaque"].Equals("Y");
                    var paxOrder = line["Pax order"];
                    var onMonday = line["On Monday"].Equals("Y");
                    var onTuesday = line["On Tuesday"].Equals("Y");
                    var onWednesday = line["On Wednesday"].Equals("Y");
                    var onThursday = line["On Thursday"].Equals("Y");
                    var onFriday = line["On Friday"].Equals("Y");
                    var onSaturday = line["On Saturday"].Equals("Y");
                    var onSunday = line["On Sunday"].Equals("Y");
                    var txtMarket = line["Market price"];

                    if (applyOpaque && !p.Opaque.Equals("P"))
                    {
                        continue;
                    }

                    if (!String.IsNullOrEmpty(txtMarket) && !p.ContractMarkets.Contains(txtMarket))
                    {
                        continue;
                    }

                    int orderPax;
                    if (!Int32.TryParse(paxOrder, out orderPax))
                    {
                        orderPax = 0;
                    }

                    var rowCombinations = RowCombinations(p.DistinctCombinations, txtRoomTypeCode, txtRoomCharacteristic, txtRate, txtMarket, txtBoardCode, p.SortedRates, p.RoomTypesTable, p.BoardsList, p.ContractMarkets);
                    rowCombinations = AddCombinations(rowCombinations, line, _occupancyCombinations);

                    foreach (var date in supplementDates)
                    {
                        if (date.DayOfWeek.Equals(DayOfWeek.Monday) && !onMonday) continue;
                        if (date.DayOfWeek.Equals(DayOfWeek.Tuesday) && !onTuesday) continue;
                        if (date.DayOfWeek.Equals(DayOfWeek.Wednesday) && !onWednesday) continue;
                        if (date.DayOfWeek.Equals(DayOfWeek.Thursday) && !onThursday) continue;
                        if (date.DayOfWeek.Equals(DayOfWeek.Friday) && !onFriday) continue;
                        if (date.DayOfWeek.Equals(DayOfWeek.Saturday) && !onSaturday) continue;
                        if (date.DayOfWeek.Equals(DayOfWeek.Sunday) && !onSunday) continue;

                        var combNotProcessed = FilterCombinations(genericTableCombinations, rowCombinations, date);
                        foreach (var cnp in combNotProcessed)
                        {
                            var fields = SplitCombination(p.SplittedCombinations, cnp, _occupancyCombinations);
                            var marketCode = fields["Market"];
                            var roomTypeCode = fields["Room type"];
                            var roomCharacteristic = fields["Characteristic"];
                            var rate = fields["Rate"];
                            var boardCode = fields["Board"];
                            var roomRate = p.FixRate ? rate : HotelBedsSettings.GenericRateCode;

                            var roomGroupId = CreateRoomGroupId(p.ContractRoomId, roomTypeCode, roomCharacteristic, roomRate, p.ContractRoomIdType);
                            var roomList = RoomList(p.BaseRooms, p.BaseRoomGroups, roomGroupId, p.BaseRoomsIndex);

                            foreach (var room in roomList)
                            {
                                if (!p.SupplementChildPaxApplied.ContainsKey(date))
                                {
                                    p.SupplementChildPaxApplied.Add(date, new Dictionary<string, List<int>>());
                                }
                                if (!p.SupplementChildPaxApplied[date].ContainsKey(room.RoomId))
                                {
                                    p.SupplementChildPaxApplied[date].Add(room.RoomId, new List<int>());
                                }
                                if (!p.SupplementChildPaxApplied[date][room.RoomId].Contains(orderPax))
                                {
                                    p.SupplementChildPaxApplied[date][room.RoomId].Add(orderPax);
                                }
                            }

                            //Mark combination processed
                            genericTableCombinations[date][cnp] = true;
                        }
                    }
                }
            }
        }

        private void SupplementGeneral(HotelBedsSupplementsGeneral p)
        {
            if (!p.TypeSupplementProcessed.ContainsKey(p.Date))
            {
                p.TypeSupplementProcessed.Add(p.Date, new Dictionary<string, Dictionary<string, List<string>>>());
            }
            if (!p.TypeSupplementProcessed[p.Date].ContainsKey(p.SupplementType))
            {
                p.TypeSupplementProcessed[p.Date].Add(p.SupplementType, new Dictionary<string, List<string>>());
            }
            if (!p.TypeSupplementProcessed[p.Date][p.SupplementType].ContainsKey(p.Room.RoomId))
            {
                p.TypeSupplementProcessed[p.Date][p.SupplementType].Add(p.Room.RoomId, new List<string>());
            }
            if (p.TypeSupplementProcessed[p.Date][p.SupplementType][p.Room.RoomId].Contains(p.BoardCode) && !p.SupplementType.Equals("G"))
            {
                //Discount for this board already applied, Type "G" can be applied more than one time
                return;
            }

            var isDiscount = false;
            decimal? amount = null;
            decimal? percentage = null;

            int? stayLength = null;
            if (!String.IsNullOrEmpty(p.LengthOfStay))
            {
                stayLength = Int32.Parse(p.LengthOfStay);
                if (p.SupplementType.Equals("K") || p.SupplementType.Equals("L"))
                {
                    stayLength += 1;
                }
                if (p.SupplementType.Equals("M") && stayLength.Value > 1)
                {
                    stayLength -= 1;
                }
            }

            if (!String.IsNullOrEmpty(p.TxtAmount))
            {
                amount = Decimal.Parse(p.TxtAmount, NumberFormatInfo.InvariantInfo);

                //If the boardCode is not specified then we only create one discount for this date, if it is specified we discount it directly
                if (
                    String.IsNullOrEmpty(p.TxtBoardCode) && amount.Value < 0 && p.IsAdultPrice
                    && (
                        p.ApplicationType.Equals("N") //Per night
                        || p.SupplementType.Equals("E") //Fixed stay
                        || (p.SupplementType.Equals("K") && p.ApplicationType.Equals("B") && stayLength.HasValue) //Turbo Early Booking applied to the base price with length of stay
                    )
                )
                {
                    isDiscount = true;
                    amount = amount.Value * -1;
                }
            }
            else
            {
                percentage = Decimal.Parse(p.TxtPercentage, NumberFormatInfo.InvariantInfo);

                //If the boardCode is not specified then we only create one discount for this date, if it is specified we discount it directly
                if (
                    String.IsNullOrEmpty(p.TxtBoardCode) && percentage.Value < 0 && p.IsAdultPrice
                    && (
                        p.ApplicationType.Equals("N") //Per night
                        || p.SupplementType.Equals("E") //Fixed stay
                        || (p.SupplementType.Equals("K") && p.ApplicationType.Equals("B") && stayLength.HasValue) //Turbo Early Booking applied to the base price with length of stay
                    )
                )
                {
                    isDiscount = true;
                    percentage = percentage.Value * -1;
                }
            }

            var adults = p.SupplementsParams.BaseRoomTypes[p.Room.TypeId].Beds;
            var childs = p.SupplementsParams.BaseRoomTypes[p.Room.TypeId].ExtraBeds;

            if (isDiscount)
            {
                var countDiscounts = p.AvailabilityPeriod.Discounts.Count;
                Discount discount = null;

                var includeExtraBed = p.IsPerPax && p.IsChildPrice && childs > 0;

                //Early Booking - "B"
                if (p.SupplementType.Equals("B"))
                {
                    if (amount.HasValue)
                    {
                        var newAmount = amount.Value * (p.IsPerPax ? adults : 1);
                        if (newAmount > 0)
                        {
                            discount = Discount.ValueAmount(1, newAmount, p.DiscountPeriodId, countDiscounts);
                        }
                    }
                    else
                    {
                        discount = Discount.ValuePercent(1, percentage.Value, p.DiscountPeriodId, countDiscounts, false, false, includeExtraBed);
                    }
                }

                //Turbo Early Booking - "K", Last Minute - "U"
                if (p.SupplementType.Equals("K") || p.SupplementType.Equals("U"))
                {
                    if (amount.HasValue)
                    {
                        var newAmount = amount.Value * (p.IsPerPax ? adults : 1);
                        if (newAmount > 0)
                        {
                            discount = Discount.ValueAmount(stayLength.GetValueOrDefault(1), newAmount, p.DiscountPeriodId, countDiscounts);
                        }
                    }
                    else
                    {
                        discount = Discount.ValuePercent(stayLength.GetValueOrDefault(1), percentage.Value, p.DiscountPeriodId, countDiscounts, false, false, includeExtraBed);
                    }
                }

                //Long Stay - "L", Minimum Stay - "M"
                if (p.SupplementType.Equals("L") || p.SupplementType.Equals("M"))
                {
                    if (amount.HasValue)
                    {
                        var newAmount = amount.Value * (p.IsPerPax ? adults : 1);
                        if (newAmount > 0)
                        {
                            discount = Discount.EarlyBookingAmount(stayLength.GetValueOrDefault(1), newAmount, p.DiscountPeriodId, countDiscounts);
                        }
                    }
                    else
                    {
                        discount = Discount.EarlyBookingProcent(stayLength.GetValueOrDefault(1), percentage.Value, p.DiscountPeriodId, countDiscounts, false, false, includeExtraBed);
                    }
                }

                //Operation Dates - "O", General - "G"
                if (p.SupplementType.Equals("O") || p.SupplementType.Equals("G"))
                {
                    if (amount.HasValue)
                    {
                        var newAmount = amount.Value * (p.IsPerPax ? adults : 1);
                        if (newAmount > 0)
                        {
                            discount = Discount.ValueAmount(1, newAmount, p.DiscountPeriodId, countDiscounts);
                        }
                    }
                    else
                    {
                        discount = Discount.ValuePercent(1, percentage.Value, p.DiscountPeriodId, countDiscounts, false, false, includeExtraBed);
                    }
                }

                //Arrival Day - "V"
                if (p.SupplementType.Equals("V"))
                {
                    if (amount.HasValue)
                    {
                        var newAmount = amount.Value * (p.IsPerPax ? adults : 1);
                        if (newAmount > 0)
                        {
                            discount = Discount.EarlyBookingAmount(stayLength.GetValueOrDefault(1), newAmount, p.DiscountPeriodId, countDiscounts);
                        }
                    }
                    else
                    {
                        discount = Discount.EarlyBookingProcent(stayLength.GetValueOrDefault(1), percentage.Value, p.DiscountPeriodId, countDiscounts, false, false, includeExtraBed);
                    }
                }

                //Fixed Stay - "E" ("Only the number of nights specified")
                if (p.SupplementType.Equals("E"))
                {
                    decimal totalPercent;

                    if (!p.ApplicationType.Equals("N") || amount.HasValue)
                    {
                        if (p.ApplicationType.Equals("R"))
                        {
                            return;
                        }

                        //Room base price
                        var basePrice = p.SupplementsParams.BaseRoomsPrice[p.Room.RoomId][p.Date][0].Value;
                        var baseAmountField = Decimal.Parse(p.AvailabilityPeriod.Pricing_BasePrice, NumberFormatInfo.InvariantInfo);
                        
                        if (amount.HasValue)
                        {
                            var newAmount = amount.Value * (p.IsPerPax ? adults : 1);
                            totalPercent = Decimal.Divide(newAmount * 100, p.ApplicationType.Equals("B") ? basePrice : baseAmountField);
                        }
                        else
                        {
                            var bigValue = baseAmountField >= basePrice ? baseAmountField : basePrice;
                            var lowValue = baseAmountField < basePrice ? baseAmountField : basePrice;
                            totalPercent = percentage.Value * Decimal.Divide(lowValue, bigValue);
                        }
                    }
                    else
                    {
                        totalPercent = percentage.Value;
                    }
                    if (totalPercent > 0)
                    {
                        discount = Discount.MinMaxPercent(stayLength.GetValueOrDefault(1), stayLength.GetValueOrDefault(1), totalPercent, p.DiscountPeriodId, countDiscounts, false, false, includeExtraBed);
                    }
                }

                if (discount != null)
                {
                    discount.LastDate = (!String.IsNullOrEmpty(p.LimitDate) ? ParseDate(p.LimitDate) : p.DiscountLimitDate);
                    p.AvailabilityPeriod.Discounts.TryAdd(discount);
                }

                //If we have created a discount, we can't create another for this boards
                if (p.AvailabilityPeriod.Discounts.Count > countDiscounts)
                {
                    foreach (var board in p.SupplementsParams.BoardsList)
                    {
                        if (!p.TypeSupplementProcessed[p.Date][p.SupplementType][p.Room.RoomId].Contains(board))
                        {
                            p.TypeSupplementProcessed[p.Date][p.SupplementType][p.Room.RoomId].Add(board);
                        }
                    }
                }
            }
            else
            {
                if (!p.TypeSupplementProcessed[p.Date][p.SupplementType][p.Room.RoomId].Contains(p.BoardCode))
                {
                    p.TypeSupplementProcessed[p.Date][p.SupplementType][p.Room.RoomId].Add(p.BoardCode);
                }

                if (
                    (!p.AvailabilityPeriod.Pricing_BookableBoardId.Equals(p.BoardCode) && !p.ApplicationType.Equals("R"))
                    || (p.AvailabilityPeriod.Pricing_BookableBoardId.Equals(p.BoardCode) && p.ApplicationType.Equals("R"))
                )
                {
                    return;
                }

                //Room base price
                var baseIsPerPax = p.SupplementsParams.BaseRoomsPriceIsPerPax[p.Room.RoomId][p.Date];
                var basePrice = p.SupplementsParams.BaseRoomsPrice[p.Room.RoomId][p.Date][0].Value;
                var basePriceChild = p.SupplementsParams.BaseRoomsPrice[p.Room.RoomId][p.Date][1];
                if (baseIsPerPax && adults > 1)
                {
                    basePrice = Decimal.Divide(basePrice, adults);
                }
                if (!baseIsPerPax && childs > 0 && basePriceChild.HasValue && basePriceChild == Decimal.Zero && p.IsChildPrice)
                {
                    if (!String.IsNullOrEmpty(p.AvailabilityPeriod.Pricing_ExtrabedChild))
                    {
                        var childAmountField = Decimal.Parse(p.AvailabilityPeriod.Pricing_ExtrabedChild, NumberFormatInfo.InvariantInfo);
                        if (childAmountField > 0)
                        {
                            basePriceChild = childAmountField;
                        }
                    }
                }

                //Board price
                var basePriceBoard = Decimal.Zero;
                var basePriceBoardChild = Decimal.Zero;
                if (
                    p.SupplementsParams.BaseRoomsPriceBoard.ContainsKey(p.Room.RoomId)
                    && p.SupplementsParams.BaseRoomsPriceBoard[p.Room.RoomId].ContainsKey(p.Date)
                    && p.SupplementsParams.BaseRoomsPriceBoard[p.Room.RoomId][p.Date].ContainsKey(p.Rate)
                    && p.SupplementsParams.BaseRoomsPriceBoard[p.Room.RoomId][p.Date][p.Rate].ContainsKey(p.BoardCode)
                )
                {
                    var basePriceBoardIsPerPax = p.SupplementsParams.BaseRoomsPriceBoardIsPerPax[p.Room.RoomId][p.Date][p.Rate][p.BoardCode];
                    basePriceBoard = p.SupplementsParams.BaseRoomsPriceBoard[p.Room.RoomId][p.Date][p.Rate][p.BoardCode][0].Value;
                    if (basePriceBoardIsPerPax)
                    {
                        basePriceBoard = Decimal.Divide(basePriceBoard, adults) - (baseIsPerPax ? basePrice : Decimal.Divide(basePrice, adults));
                    }
                    else
                    {
                        basePriceBoard = basePriceBoard - (baseIsPerPax ? (basePrice * adults) : basePrice);
                    }
                    if (basePriceChild.HasValue && p.SupplementsParams.BaseRoomsPriceBoard[p.Room.RoomId][p.Date][p.Rate][p.BoardCode][1].HasValue)
                    {
                        basePriceBoardChild = p.SupplementsParams.BaseRoomsPriceBoard[p.Room.RoomId][p.Date][p.Rate][p.BoardCode][1].Value - basePriceChild.Value;
                    }
                }

                if (amount.HasValue && p.IsPerPax && p.IsAdultPrice && p.AvailabilityPeriod.Pricing_BookableBoardId.Equals(p.BoardCode))
                {
                    amount = amount * adults;
                }

                decimal? amountChild = null;
                if (percentage.HasValue)
                {
                    if (!p.ApplicationType.Equals("R"))
                    {
                        amount = Decimal.Divide(basePrice * percentage.Value, 100);
                        amountChild = Decimal.Divide((basePriceChild.HasValue ? basePriceChild.Value : 0) * percentage.Value, 100);
                        percentage = null;
                    }
                    else
                    {
                        amount = Decimal.Divide(basePriceBoard * percentage.Value, 100);
                        amountChild = Decimal.Divide(basePriceBoardChild * percentage.Value, 100);
                        percentage = null;
                    }
                }

                //Price with length of stay restriction
                if (stayLength.HasValue && stayLength.Value > p.AvailabilityPeriod.MinimumStay)
                {
                    p.AvailabilityPeriod.MinimumStay = stayLength.Value;
                }

                if (p.AvailabilityPeriod.Pricing_BookableBoardId.Equals(p.BoardCode))
                {
                    if (p.IsAdultPrice)
                    {
                        p.AvailabilityPeriod.Pricing_BasePrice = ApplyExtraCharge(p.AvailabilityPeriod.Pricing_BasePrice, amount, percentage);
                    }
                    if (childs > 0 && p.IsChildPrice)
                    {
                        p.AvailabilityPeriod.Pricing_ExtrabedChild = ApplyExtraCharge(p.AvailabilityPeriod.Pricing_ExtrabedChild, amountChild, percentage);
                    }
                }
                else
                {
                    foreach (var addBoard in p.AvailabilityPeriod.AdditionalBoards)
                    {
                        if (addBoard.BookableBoardId.Equals(p.BoardCode))
                        {
                            if (p.IsAdultPrice)
                            {
                                addBoard.Adult = ApplyExtraCharge(addBoard.Adult, amount, percentage);
                            }
                            if (childs > 0 && p.IsChildPrice)
                            {
                                addBoard.Child = ApplyExtraCharge(addBoard.Child, amountChild, percentage);
                            }
                        }
                    }
                }
            }
        }

        public void SupplementDailyGeneral(
            SortedList<int, Dictionary<string, string>> sortedGeneralSupplements,
            ConcurrentDictionary<string, ConcurrentDictionary<string, ConcurrentBag<string>>> logList,
            string hotelIdName,
            Dictionary<string, List<string>> distinctCombinations,
            Dictionary<string, Dictionary<string, string>> splittedCombinations,
            SortedList<int, Dictionary<string, string>> sortedRates,
            IHotelBedsDataTable roomTypesTable,
            List<string> boardsList,
            List<string> contractMarkets,
            bool fixRate,
            string opaque,
            DateTime cacheIni,
            DateTime cacheEnd,
            HashSet<DateTime> contractDates,
            DateTime todayDate,
            Dictionary<object, HashSet<DateTime>> hotelSupplementDates,
            Dictionary<string, Dictionary<DateTime, Dictionary<string, Dictionary<string, string>>>> hotelGeneralDatesApply
        )
        {
            var genericTableCombinations = new Dictionary<DateTime, Dictionary<string, bool>>();

            foreach (var supplementOrder in sortedGeneralSupplements.Keys)
            {
                var line = sortedGeneralSupplements[supplementOrder];
                var supplementType = line["Type"];
                //General supplements type G can be applied many times so we skip them
                if (!supplementType.Equals("G"))
                {
                    var applicationStartDate = ParseDate(line["Application initial date"]);
                    var applicationFinalDate = !String.IsNullOrEmpty(line["Application final date"]) ? ParseDate(line["Application final date"]) : cacheEnd;
                    if (String.IsNullOrEmpty(line["Application initial date"]) || applicationStartDate > todayDate || todayDate > applicationFinalDate)
                    {
                        continue;
                    }
                    var txtRoomTypeCode = line["Room type"];
                    var txtRoomCharacteristic = line["Characteristic"];
                    var txtRate = line["Rate"];
                    var txtBoardCode = line["Board"];
                    var txtMarket = line["Market price"];
                    var supplementDates = SupplementDates(hotelSupplementDates, line, cacheIni, cacheEnd, contractDates);
                    var txtAmount = line["Amount"];
                    var txtPercentage = line["Percentage"];
                    var numberOfDays = line["Number of days"];
                    var lengthOfStay = line["Length of stay"];
                    var limitDate = line["Limit date"];
                    var applyOpaque = line["Opaque"].Equals("Y");
                    var onMonday = line["On Monday"].Equals("Y");
                    var onTuesday = line["On Tuesday"].Equals("Y");
                    var onWednesday = line["On Wednesday"].Equals("Y");
                    var onThursday = line["On Thursday"].Equals("Y");
                    var onFriday = line["On Friday"].Equals("Y");
                    var onSaturday = line["On Saturday"].Equals("Y");
                    var onSunday = line["On Sunday"].Equals("Y");

                    if (applyOpaque && !opaque.Equals("P"))
                    {
                        continue;
                    }

                    if (!hotelGeneralDatesApply.ContainsKey(supplementType))
                    {
                        hotelGeneralDatesApply.Add(supplementType, new Dictionary<DateTime, Dictionary<string, Dictionary<string, string>>>());
                    }
                    var supplementDatesApply = hotelGeneralDatesApply[supplementType];

                    var lineOrder = !String.IsNullOrEmpty(line["Order"]) ? Int32.Parse(line["Order"]) : supplementOrder;

                    var rowCombinations = RowCombinations(distinctCombinations, txtRoomTypeCode, txtRoomCharacteristic, txtRate, txtBoardCode, txtMarket, sortedRates, roomTypesTable, boardsList, contractMarkets);

                    rowCombinations = AddCombinations(rowCombinations, line, _typeCombinations);

                    int? stayLength = null;
                    if (!String.IsNullOrEmpty(lengthOfStay))
                    {
                        stayLength = Int32.Parse(lengthOfStay);
                    }

                    int? dateLimitCompareResult = null;
                    DateTime? dateLimit = null;
                    if (!String.IsNullOrEmpty(limitDate))
                    {
                        dateLimit = ParseDate(limitDate);
                        dateLimitCompareResult = DateTime.Compare(todayDate, dateLimit.Value);
                    }

                    int? daysNumber = null;
                    DateTime? daysNumberDate = null;
                    if (!String.IsNullOrEmpty(numberOfDays))
                    {
                        daysNumber = Int32.Parse(numberOfDays);
                        daysNumberDate = todayDate.AddDays(daysNumber.Value);
                    }

                    foreach (var date in supplementDates)
                    {
                        if (date.DayOfWeek.Equals(DayOfWeek.Monday) && !onMonday) continue;
                        if (date.DayOfWeek.Equals(DayOfWeek.Tuesday) && !onTuesday) continue;
                        if (date.DayOfWeek.Equals(DayOfWeek.Wednesday) && !onWednesday) continue;
                        if (date.DayOfWeek.Equals(DayOfWeek.Thursday) && !onThursday) continue;
                        if (date.DayOfWeek.Equals(DayOfWeek.Friday) && !onFriday) continue;
                        if (date.DayOfWeek.Equals(DayOfWeek.Saturday) && !onSaturday) continue;
                        if (date.DayOfWeek.Equals(DayOfWeek.Sunday) && !onSunday) continue;

                        if (!SupplementGeneralValidateDate(supplementType, date, daysNumberDate, stayLength, dateLimitCompareResult))
                        {
                            continue;
                        }

                        var combNotProcessed = FilterCombinations(genericTableCombinations, rowCombinations, date);
                        foreach (var cnp in combNotProcessed)
                        {
                            if (!supplementDatesApply.ContainsKey(date))
                            {
                                supplementDatesApply.Add(date, new Dictionary<string, Dictionary<string, string>>());
                            }
                            if (!supplementDatesApply[date].ContainsKey(cnp))
                            {
                                var newFields = new Dictionary<string, string>
                                {
                                    { "Order", lineOrder.ToString() },
                                    { "Amount", txtAmount },
                                    { "Percentage", txtPercentage },
                                    { "Number of days", numberOfDays },
                                    { "Length of stay", lengthOfStay },
                                    { "Limit date", limitDate }
                                };
                                supplementDatesApply[date].Add(cnp, newFields);
                            }
                            else
                            {
                                var compareSup = supplementDatesApply[date][cnp];
                                var compareOrder = Int32.Parse(compareSup["Order"]);
                                if (lineOrder != compareOrder)
                                {
                                    continue;
                                }
                                var compareAmount = compareSup["Amount"];
                                var comparePercentage = compareSup["Percentage"];
                                var compareNumberOfDays = compareSup["Number of days"];
                                var compareLengthOfStay = compareSup["Length of stay"];
                                var compareLimitDate = compareSup["Limit date"];

                                var vOk = (!String.IsNullOrEmpty(txtAmount) == !String.IsNullOrEmpty(compareAmount));
                                vOk = vOk && (!String.IsNullOrEmpty(txtPercentage) == !String.IsNullOrEmpty(comparePercentage));
                                if (!vOk)
                                {
                                    LogLine(logList, hotelIdName, "supplement general conditions mismatch", "txtAmount:" + txtAmount + ", compareAmount:" + compareAmount + "txtPercentage:" + txtPercentage + ", comparePercentage:" + comparePercentage);
                                    continue;
                                }

                                var checkAmount = false;
                                var changeFields = false;

                                DateTime? compareDateLimit = null;
                                if (!String.IsNullOrEmpty(compareLimitDate))
                                {
                                    compareDateLimit = ParseDate(compareLimitDate);
                                }

                                int? compareDaysNumber = null;
                                if (!String.IsNullOrEmpty(compareNumberOfDays))
                                {
                                    compareDaysNumber = Int32.Parse(compareNumberOfDays);
                                }

                                int? compareStayLength = null;
                                if (!String.IsNullOrEmpty(compareLengthOfStay))
                                {
                                    compareStayLength = Int32.Parse(compareLengthOfStay);
                                }

                                //Early Booking - "B"
                                if (supplementType.Equals("B"))
                                {
                                    if (Nullable.Equals(daysNumber, compareDaysNumber) && Nullable.Equals(dateLimit, compareDateLimit))
                                    {
                                        checkAmount = true;
                                    }
                                    else if (
                                        DefVal(daysNumber, compareDaysNumber) < DefVal(compareDaysNumber, daysNumber)
                                        || DateTime.Compare(DefVal(dateLimit, compareDateLimit), DefVal(compareDateLimit, dateLimit)) < 0
                                    )
                                    {
                                        changeFields = true;
                                    }
                                }

                                //Turbo Early Booking - "K"
                                if (supplementType.Equals("K"))
                                {
                                    if (Nullable.Equals(stayLength, compareStayLength) && Nullable.Equals(daysNumber, compareDaysNumber) && Nullable.Equals(dateLimit, compareDateLimit))
                                    {
                                        checkAmount = true;
                                    }
                                    else if (
                                        DefVal(stayLength, compareStayLength) > DefVal(compareStayLength, stayLength)
                                        || DefVal(daysNumber, compareDaysNumber) < DefVal(compareDaysNumber, daysNumber)
                                        || DateTime.Compare(DefVal(dateLimit, compareDateLimit), DefVal(compareDateLimit, dateLimit)) < 0
                                    )
                                    {
                                        changeFields = true;
                                    }
                                }

                                //Last Minute - "U"
                                if (supplementType.Equals("U"))
                                {
                                    if (Nullable.Equals(daysNumber, compareDaysNumber) && Nullable.Equals(dateLimit, compareDateLimit))
                                    {
                                        checkAmount = true;
                                    }
                                    else if (
                                        DefVal(daysNumber, compareDaysNumber) < DefVal(compareDaysNumber, daysNumber)
                                        || DateTime.Compare(DefVal(dateLimit, compareDateLimit), DefVal(compareDateLimit, dateLimit)) > 0
                                    )
                                    {
                                        changeFields = true;
                                    }
                                }

                                //Long Stay - "L", Minimum Stay - "M", Operation Dates - "O", Fixed Stay - "E", Arrival Day - "V"
                                if (supplementType.Equals("L") || supplementType.Equals("M") || supplementType.Equals("O") || supplementType.Equals("E") || supplementType.Equals("V"))
                                {
                                    if (Nullable.Equals(stayLength, compareStayLength))
                                    {
                                        checkAmount = true;
                                    }
                                    else if (supplementType.Equals("L") && DefVal(stayLength, compareStayLength) > DefVal(compareStayLength, stayLength))
                                    {
                                        changeFields = true;
                                    }
                                    else if (!supplementType.Equals("L") && DefVal(stayLength, compareStayLength) < DefVal(compareStayLength, stayLength))
                                    {
                                        changeFields = true;
                                    }
                                }
                                
                                if (checkAmount)
                                {
                                    if (!String.IsNullOrEmpty(txtAmount))
                                    {
                                        var lineAmount = Decimal.Parse(txtAmount, NumberFormatInfo.InvariantInfo);
                                        var groupedAmount = Decimal.Parse(compareAmount, NumberFormatInfo.InvariantInfo);
                                        compareSup["Amount"] = (lineAmount > groupedAmount ? lineAmount : groupedAmount).ToString(NumberFormatInfo.InvariantInfo);
                                    }
                                    else
                                    {
                                        var linePercentage = Decimal.Parse(txtPercentage, NumberFormatInfo.InvariantInfo);
                                        var groupedPercentage = Decimal.Parse(comparePercentage, NumberFormatInfo.InvariantInfo);
                                        compareSup["Percentage"] = (linePercentage > groupedPercentage ? linePercentage : groupedPercentage).ToString(NumberFormatInfo.InvariantInfo);
                                    }
                                }
                                if (changeFields)
                                {
                                    compareSup["Order"] = lineOrder.ToString();
                                    if (!checkAmount)
                                    {
                                        compareSup["Amount"] = txtAmount;
                                        compareSup["Percentage"] = txtPercentage;
                                    }
                                    compareSup["Number of days"] = numberOfDays;
                                    compareSup["Length of stay"] = lengthOfStay;
                                    compareSup["Limit date"] = limitDate;
                                }
                            }
                        }
                    }
                }
            }
        }

        private int DefVal(int? p1, int? p2)
        {
            return p1.GetValueOrDefault(p2.GetValueOrDefault(int.MaxValue));
        }

        private DateTime DefVal(DateTime? p1, DateTime? p2)
        {
            return p1.GetValueOrDefault(p2.GetValueOrDefault(DateTime.MaxValue));
        }

        public bool SupplementGeneralValidateDate(
            string supplementType,
            DateTime date,
            DateTime? daysNumberDate,
            int? stayLength,
            int? dateLimitCompareResult
        )
        {
            var dateOk = false;

            if (supplementType.Equals("B") || supplementType.Equals("K") || supplementType.Equals("U"))
            {
                int? daysNumberCompareResult = null;
                if (daysNumberDate.HasValue)
                {
                    daysNumberCompareResult = DateTime.Compare(daysNumberDate.Value, date);
                }

                //Early Booking - "B", Turbo Early Booking - "K"
                if ((supplementType.Equals("B") || supplementType.Equals("K")) && ((daysNumberCompareResult.HasValue && daysNumberCompareResult.Value <= 0) || (dateLimitCompareResult.HasValue && dateLimitCompareResult.Value <= 0)))
                {
                    dateOk = true;
                }

                //Last Minute - "U"
                if (supplementType.Equals("U") && ((daysNumberCompareResult.HasValue && daysNumberCompareResult.Value >= 0) || (dateLimitCompareResult.HasValue && dateLimitCompareResult.Value >= 0)))
                {
                    dateOk = true;
                }
            }
            else if ((supplementType.Equals("L") || supplementType.Equals("M")) && stayLength.HasValue)
            {
                //Long Stay - "L", Minimum Stay - "M"
                dateOk = true;
            }
            else if (supplementType.Equals("O") || supplementType.Equals("G") || supplementType.Equals("E") || supplementType.Equals("V"))
            {
                //Operation Dates - "O", General - "G", Fixed Stay - "E", Arrival Day - "V"
                dateOk = true;
            }
            
            return dateOk;
        }

        public HashSet<DateTime> SupplementDates(
            Dictionary<object, HashSet<DateTime>> hotelSupplementDates,
            Dictionary<string, string> line,
            DateTime cacheIni,
            DateTime cacheEnd,
            HashSet<DateTime> contractDates
        )
        {
            HashSet<DateTime> supplementDates;
            if (hotelSupplementDates.ContainsKey(line))
            {
                supplementDates = hotelSupplementDates[line];
            }
            else
            {
                var startDate = ParseDate(line["Initial date"]);
                var finalDate = ParseDate(line["Final date"]);
                supplementDates = ListValidDates(startDate, finalDate, cacheIni, cacheEnd, contractDates);
                hotelSupplementDates.Add(line, supplementDates);
            }
            return supplementDates;
        }

        #endregion

        #region Dates functions

        public bool ValidateDates(DateTime iniDate, DateTime endDate, DateTime cacheIni, DateTime cacheEnd)
        {
            bool dateOk = DateTime.Compare(iniDate, endDate) <= 0
                          && DateTime.Compare(cacheIni, cacheEnd) <= 0
                          && (
                              (DateTime.Compare(cacheIni, iniDate) <= 0 && DateTime.Compare(endDate, cacheEnd) <= 0)
                              || (DateTime.Compare(iniDate, cacheIni) <= 0 && DateTime.Compare(cacheIni, endDate) <= 0)
                              || (DateTime.Compare(iniDate, cacheEnd) <= 0 && DateTime.Compare(cacheEnd, endDate) <= 0)
                              || (DateTime.Compare(iniDate, cacheIni) <= 0 && DateTime.Compare(cacheEnd, endDate) <= 0)
                              );
            return dateOk;
        }

        public HashSet<DateTime> ListValidDates(DateTime iniDate, DateTime endDate, DateTime cacheIni, DateTime cacheEnd, HashSet<DateTime> filterDates)
        {
            var listDates = new HashSet<DateTime>();
            if (ValidateDates(iniDate, endDate, cacheIni, cacheEnd))
            {
                var startDate = cacheIni <= iniDate ? iniDate : cacheIni;
                var finalDate = cacheEnd >= endDate ? endDate : cacheEnd;
                var currentDate = startDate;
                while (currentDate <= finalDate)
                {
                    if (filterDates == null || filterDates.Contains(currentDate))
                    {
                        listDates.Add(currentDate);
                    }
                    currentDate = currentDate.AddDays(1);
                }
            }
            return listDates;
        }

        public Dictionary<DateTime, string> DailyValues(DateTime startDate, DateTime finalDate, List<string> values, HashSet<DateTime> filterDates)
        {
            var dailyValues = new Dictionary<DateTime, string>();
            if (
                DateTime.Compare(startDate, finalDate) <= 0
                && ((finalDate - startDate).TotalDays + 1) == values.Count
            )
            {
                var i = 0;
                var currentDate = startDate;
                while (currentDate <= finalDate)
                {
                    if (filterDates == null || filterDates.Contains(currentDate))
                    {
                        dailyValues.Add(currentDate, values[i]);
                    }
                    i++;
                    currentDate = currentDate.AddDays(1);
                }
            }
            return dailyValues;
        }

        public DateTime ParseDate(string hotelBedsDate)
        {
            DateTime date;
            if (!String.IsNullOrEmpty(hotelBedsDate))
            {
                date = DateTime.ParseExact(hotelBedsDate, HotelBedsSettings.Aif2DateFormat, CultureInfo.InvariantCulture);
            }
            else
            {
                throw new ArgumentException("Date null or empty");
            }
            return date;
        }

        #endregion

        #region Hotels functions

        public bool CheckHotelRooms(Hotel hotel)
        {
            //Group rooms
            var listHotelRooms = new Dictionary<string, Room>();
            foreach (var roomId in hotel.Rooms.Keys)
            {
                listHotelRooms.Add(roomId, hotel.Rooms[roomId]);
            }
            foreach (var rgKey in hotel.RoomGroups.Keys)
            {
                foreach (var roomId in hotel.RoomGroups[rgKey].Rooms.Keys)
                {
                    listHotelRooms.Add(roomId, hotel.RoomGroups[rgKey].Rooms[roomId]);
                }
            }
            //Check rooms
            var removeRoomList = new List<string>();
            foreach (var roomId in listHotelRooms.Keys)
            {
                if (listHotelRooms[roomId].AvailabilityPeriods == null || listHotelRooms[roomId].AvailabilityPeriods.Count == 0)
                {
                    removeRoomList.Add(roomId);
                }
            }
            foreach (var roomId in removeRoomList)
            {
                if (hotel.Rooms.ContainsKey(roomId))
                {
                    hotel.Rooms.Remove(roomId);
                }
                else
                {
                    foreach (var rgKey in hotel.RoomGroups.Keys)
                    {
                        if (hotel.RoomGroups[rgKey].Rooms.ContainsKey(roomId))
                        {
                            hotel.RoomGroups[rgKey].Rooms.Remove(roomId);
                            break;
                        }
                    }
                }
            }
            //Check room groups
            var removeRoomGroups = new List<string>();
            foreach (var rgKey in hotel.RoomGroups.Keys)
            {
                if (hotel.RoomGroups[rgKey].Rooms.Count == 1)
                {
                    //Room group with just one room it's not correct, we move the room to the hotel object
                    foreach (var roomId in hotel.RoomGroups[rgKey].Rooms.Keys)
                    {
                        hotel.Rooms.Add(roomId, hotel.RoomGroups[rgKey].Rooms[roomId]);
                    }
                    hotel.RoomGroups[rgKey].Rooms.Clear();
                }
                if (hotel.RoomGroups[rgKey].Rooms.Count == 0)
                {
                    removeRoomGroups.Add(rgKey);
                }
            }
            foreach (var rgKey in removeRoomGroups)
            {
                hotel.RoomGroups.Remove(rgKey);
            }

            return hotel.Rooms.Count == 0 && hotel.RoomGroups.Count == 0;
        }

        public void OptimizeHotelRoomsAvailabilityPrices(Dictionary<string, Room> baseRooms, bool oneRateNotMapped)
        {
            //If it's a full file (mapped and unmapped) then we keep only ONE rate per room (not available for booking but for mapping)
            if (oneRateNotMapped)
            {
                foreach (var room in baseRooms.Values)
                {
                    if (room.AvailabilityPeriods.Count > 0)
                    {
                        var en = room.AvailabilityPeriods.GetEnumerator();
                        en.MoveNext();
                        var ap = en.Current.Value;

                        CheckAvailabilityPeriod(ap);

                        if (String.IsNullOrEmpty(ap.Pricing_ExtrabedChild))
                        {
                            ap.Pricing_ExtrabedChild = "10000.00";
                        }
                        if (ap.Pricing_BookableBoardId.StartsWith(HotelBedsSettings.RoomCharSeparator.ToString()))
                        {
                            ap.Pricing_BookableBoardId = "RO" + ap.Pricing_BookableBoardId;
                        }
                        ap.DateTo = ap.DateFrom;
                        ap.AvailabilityAvailableUnits = 1;
                        ap.MinimumStay = 10;

                        room.AvailabilityPeriods.Clear();
                        room.AvailabilityPeriods.Add(ap.DateFrom, ap);
                    }
                }

                //Exits optimization if it's not mapped
                return;
            }

            foreach (var room in baseRooms.Values)
            {
                AvailabilityPeriod availabilityPeriod = null;
                var listDates = new List<DateTime>(room.AvailabilityPeriods.Keys);
                foreach (var date in listDates)
                {
                    var ap = room.AvailabilityPeriods[date];
                    if (availabilityPeriod == null)
                    {
                        availabilityPeriod = ap;
                    }
                    else
                    {
                        //Basic check
                        var vOk = availabilityPeriod.AvailabilityAvailableUnits.Equals(ap.AvailabilityAvailableUnits);
                        vOk = vOk && availabilityPeriod.AvailabilityTotalAllotment.Equals(ap.AvailabilityTotalAllotment);
                        vOk = vOk && availabilityPeriod.IsArrivalPossible.Equals(ap.IsArrivalPossible);
                        vOk = vOk && availabilityPeriod.IsWeeklyStay.Equals(ap.IsWeeklyStay);
                        vOk = vOk && availabilityPeriod.MinimumStay.Equals(ap.MinimumStay);
                        vOk = vOk && availabilityPeriod.ReleaseDays.Equals(ap.ReleaseDays);
                        vOk = vOk && availabilityPeriod.BoardTypeId.Equals(ap.BoardTypeId);
                        vOk = vOk && String.IsNullOrEmpty(availabilityPeriod.Pricing_BaseIncludedBoard).Equals(String.IsNullOrEmpty(ap.Pricing_BaseIncludedBoard));
                        if (vOk && !String.IsNullOrEmpty(availabilityPeriod.Pricing_BaseIncludedBoard) && !String.IsNullOrEmpty(ap.Pricing_BaseIncludedBoard))
                        {
                            vOk = availabilityPeriod.Pricing_BaseIncludedBoard.Equals(ap.Pricing_BaseIncludedBoard);
                        }
                        vOk = vOk && availabilityPeriod.Pricing_BasePrice.Equals(ap.Pricing_BasePrice);
                        vOk = vOk && String.IsNullOrEmpty(availabilityPeriod.Pricing_BookableBoardId).Equals(String.IsNullOrEmpty(ap.Pricing_BookableBoardId));
                        if (vOk && !String.IsNullOrEmpty(availabilityPeriod.Pricing_BookableBoardId) && !String.IsNullOrEmpty(ap.Pricing_BookableBoardId))
                        {
                            vOk = availabilityPeriod.Pricing_BookableBoardId.Equals(ap.Pricing_BookableBoardId);
                        }
                        vOk = vOk && availabilityPeriod.Pricing_ExtrabedAdult.Equals(ap.Pricing_ExtrabedAdult);
                        vOk = vOk && availabilityPeriod.Pricing_ExtrabedChild.Equals(ap.Pricing_ExtrabedChild);

                        //Check lists
                        vOk = vOk && availabilityPeriod.AdditionalBoards.Count.Equals(ap.AdditionalBoards.Count);
                        vOk = vOk && availabilityPeriod.CancellationPolicies.Count.Equals(ap.CancellationPolicies.Count);
                        vOk = vOk && availabilityPeriod.Discounts.Count.Equals(ap.Discounts.Count);
                        if (vOk)
                        {
                            for (var i = 0; i < availabilityPeriod.AdditionalBoards.Count && vOk; i++)
                            {
                                vOk = availabilityPeriod.AdditionalBoards[i].Type.Equals(ap.AdditionalBoards[i].Type);
                                vOk = vOk && availabilityPeriod.AdditionalBoards[i].TypeId.Equals(ap.AdditionalBoards[i].TypeId);
                                vOk = vOk && availabilityPeriod.AdditionalBoards[i].BookableBoardId.Equals(ap.AdditionalBoards[i].BookableBoardId);
                                vOk = vOk && availabilityPeriod.AdditionalBoards[i].Adult.Equals(ap.AdditionalBoards[i].Adult);
                                vOk = vOk && availabilityPeriod.AdditionalBoards[i].Child.Equals(ap.AdditionalBoards[i].Child);
                            }
                            for (var i = 0; i < availabilityPeriod.CancellationPolicies.Count && vOk; i++)
                            {
                                vOk = availabilityPeriod.CancellationPolicies[i].Equals(ap.CancellationPolicies[i]);
                            }
                            var availabilityEnum = availabilityPeriod.Discounts.GetEnumerator();
                            var apEnum = ap.Discounts.GetEnumerator();
                            availabilityEnum.Reset();
                            apEnum.Reset();
                            for (var i = 0; i < availabilityPeriod.Discounts.Count && vOk; i++)
                            {
                                availabilityEnum.MoveNext();
                                apEnum.MoveNext();
                                var availabilityDiscount = availabilityEnum.Current;
                                var apDiscount = apEnum.Current;
                                vOk = availabilityDiscount.AccumulativeGroup.Equals(apDiscount.AccumulativeGroup);
                                vOk = vOk && availabilityDiscount.Amount.Equals(apDiscount.Amount);
                                vOk = vOk && availabilityDiscount.CheckinBased.Equals(apDiscount.CheckinBased);
                                vOk = vOk && availabilityDiscount.IncludeExtraBed.Equals(apDiscount.IncludeExtraBed);
                                vOk = vOk && availabilityDiscount.IncludeMeal.Equals(apDiscount.IncludeMeal);
                                vOk = vOk && availabilityDiscount.LastDate.Equals(apDiscount.LastDate);
                                vOk = vOk && availabilityDiscount.MaxDays.Equals(apDiscount.MaxDays);
                                vOk = vOk && availabilityDiscount.MaxPeriods.Equals(apDiscount.MaxPeriods);
                                vOk = vOk && availabilityDiscount.MinDays.Equals(apDiscount.MinDays);
                                vOk = vOk && availabilityDiscount.Percent.Equals(apDiscount.Percent);
                                vOk = vOk && availabilityDiscount.Period_ID.Equals(apDiscount.Period_ID);
                                vOk = vOk && availabilityDiscount.Type.Equals(apDiscount.Type);
                                vOk = vOk && availabilityDiscount.TypeId.Equals(apDiscount.TypeId);
                            }
                        }

                        if (vOk)
                        {
                            availabilityPeriod.DateTo = date;
                            room.AvailabilityPeriods.Remove(date);
                        }
                        else
                        {
                            availabilityPeriod = ap;
                        }
                    }
                }

                //Check values and correct them to validate the XSD
                listDates = new List<DateTime>(room.AvailabilityPeriods.Keys);
                foreach (var date in listDates)
                {
                    var ap = room.AvailabilityPeriods[date];

                    CheckAvailabilityPeriod(ap);

                    //Last step, delete availability 0 if the export is the cache file. If it is push we must update the stop sales
                    if (ap.AvailabilityAvailableUnits == 0)
                    {
                        room.AvailabilityPeriods.Remove(date);
                    }
                }
            }
        }

        public List<string> HotelDestinationFilter(HotelBedsSettings settings)
        {
            var specialHotels = new List<string>();
            if (!String.IsNullOrEmpty(settings.Base.SpecificDestinations))
            {
                using (var connection = new SqlConnection(settings.Base.CurrencyConnectionString))
                {
                    using (var command = new SqlCommand())
                    {
                        command.Connection = connection;
                        command.CommandText = @"SELECT DISTINCT hpr.XMLhotelid
                                                FROM hotel as h
                                                    INNER JOIN hotelProviders as hpr ON h.id = hpr.hotelid
                                                    INNER JOIN xmlProvider as xpr ON xpr.providerid = hpr.XMLProviderid
                                                    INNER JOIN Destinations as d ON d.id = h.destination
                                                WHERE
                                                    xpr.providername = '" + settings.Base.Provider + @"'
                                                    AND d.ID IN (" + settings.Base.SpecificDestinations + ")";
                        using (var dataTable = new DataTable())
                        {
                            using (var sqlDataAdapter = new SqlDataAdapter())
                            {
                                sqlDataAdapter.SelectCommand = command;
                                sqlDataAdapter.Fill(dataTable);
                            }

                            if (dataTable.Rows.Count > 0)
                            {
                                for (var i = 0; i < dataTable.Rows.Count; i++)
                                {
                                    var row = dataTable.Rows[i];
                                    var xmlHotelId = (string)row["XMLhotelid"];
                                    specialHotels.Add(xmlHotelId);
                                }
                            }
                        }
                    }
                }
            }
            return specialHotels;
        }


        private void CheckAvailabilityPeriod(AvailabilityPeriod ap)
        {
            //XSD validation
            ap.Pricing_ExtrabedAdult = null;
            if (String.IsNullOrEmpty(ap.Pricing_BasePrice) || ap.Pricing_BasePrice.Equals("0.0") || ap.Pricing_BasePrice.Equals("0.00") || ap.Pricing_BasePrice[0].Equals('-'))
            {
                ap.Pricing_BasePrice = "0";
            }
            if (ap.Pricing_BasePrice.Equals("0"))
            {
                //XSD restriction "positiveDoubleNotZero"
                ap.Pricing_BasePrice = "10000.00";

                //Price not valid so we set as not available
                ap.AvailabilityAvailableUnits = 0;
            }
            if (String.IsNullOrEmpty(ap.Pricing_BaseIncludedBoard))
            {
                ap.Pricing_BaseIncludedBoard = "none";
            }
            var adultBoardPrice = 1.00M;
            foreach (var addBoard in ap.AdditionalBoards)
            {
                if (String.IsNullOrEmpty(addBoard.Adult) || addBoard.Adult.Equals("0") || addBoard.Adult.Equals("0.0") || addBoard.Adult.Equals("0.00"))
                {
                    //XSD restriction "positive not zero"
                    addBoard.Adult = adultBoardPrice.ToString("F", NumberFormatInfo.InvariantInfo);

                    //For the push we increment in one to avoid the error "Adult board rates must be rising or 0"
                    adultBoardPrice = Decimal.Add(adultBoardPrice, Decimal.One);
                }
            }
        }

        #endregion

        #region Rooms functions

        public string OpaqueValue(string opaque)
        {
            //Any change of the room id structure must be done in the availability and booking xml integration and in the DB sp_UpdateDefaultMapping ('%:[G0-9]%:[YN]:[YN]:[0-9]%:[0-9]%'), sp_GetHotelDetails, sp_ImportRoom, HotelImportControl.ascx (opaque), AutoMapping (JOB)
            return opaque.Equals("P") ? "Y" : "N";
        }

        public string NonRefundableValue(bool nonRefundable)
        {
            //Any change of the room id structure must be done in the availability and booking xml integration and in the DB sp_UpdateDefaultMapping ('%:[G0-9]%:[YN]:[YN]:[0-9]%:[0-9]%'), sp_GetHotelDetails, sp_ImportRoom, HotelImportControl.ascx (opaque), AutoMapping (JOB)
            return nonRefundable ? "Y" : "N";
        }

        public string CreateContractRoomId(string officeCode, string contractNumber, string companyCode)
        {
            //Any change of the room id structure must be done in the availability and booking xml integration and in the DB sp_UpdateDefaultMapping ('%:[G0-9]%:[YN]:[YN]:[0-9]%:[0-9]%'), sp_GetHotelDetails, sp_ImportRoom, HotelImportControl.ascx (opaque), AutoMapping (JOB)
            var contractRoomId = String.Join(HotelBedsSettings.RoomCharSeparator.ToString(), officeCode, contractNumber, companyCode);
            if (contractRoomId.Length > 50)
            {
                throw new ApplicationException("ContractRoomId is larger than 50 characters -> " + contractRoomId);
            }
            return contractRoomId;
        }

        public string CreateRoomGroupId(string contractRoomId, string roomTypeCode, string roomCharacteristic, string rateCode, string contractRoomIdType)
        {
            //Any change of the room id structure must be done in the availability and booking xml integration and in the DB sp_UpdateDefaultMapping ('%:[G0-9]%:[YN]:[YN]:[0-9]%:[0-9]%'), sp_GetHotelDetails, sp_ImportRoom, HotelImportControl.ascx (opaque), AutoMapping (JOB)
            //RoomGroupId -> officeCode:contractNumber:companyCode:roomType:roomCharacteristic:rateCode:nonRefundable:opaque
            var roomGroupId = new StringBuilder(contractRoomId);
            roomGroupId.Append(HotelBedsSettings.RoomCharSeparator);
            if (!String.IsNullOrEmpty(roomTypeCode))
            {
                roomGroupId.Append(roomTypeCode);
            }
            roomGroupId.Append(HotelBedsSettings.RoomCharSeparator);
            if (!String.IsNullOrEmpty(roomCharacteristic))
            {
                roomGroupId.Append(roomCharacteristic);
            }
            roomGroupId.Append(HotelBedsSettings.RoomCharSeparator);
            if (!String.IsNullOrEmpty(rateCode))
            {
                roomGroupId.Append(rateCode);
            }
            roomGroupId.Append(contractRoomIdType);
            if (roomGroupId.Length > 50)
            {
                throw new ApplicationException("RoomGroupId is larger than 50 characters -> " + roomGroupId);
            }
            return roomGroupId.ToString();
        }

        public string CreateRoomTypeId(string companyCode, string roomTypeCode, string roomCharacteristic, string rateCode, string contractRoomIdType, int adults, int childs)
        {
            //Any change of the room id structure must be done in the availability and booking xml integration and in the DB sp_UpdateDefaultMapping ('%:[G0-9]%:[YN]:[YN]:[0-9]%:[0-9]%'), sp_GetHotelDetails, sp_ImportRoom, HotelImportControl.ascx (opaque), AutoMapping (JOB)
            var roomTypeId = String.Join(HotelBedsSettings.RoomCharSeparator.ToString(), companyCode, roomTypeCode, roomCharacteristic, rateCode) + contractRoomIdType + HotelBedsSettings.RoomCharSeparator + String.Join(HotelBedsSettings.RoomCharSeparator.ToString(), adults, childs);
            if (roomTypeId.Length > 50)
            {
                throw new ApplicationException("RoomTypeId is larger than 50 characters -> " + roomTypeId);
            }
            return roomTypeId;
        }

        public string CreateRoomId(string contractRoomId, string roomTypeId)
        {
            //Any change of the room id structure must be done in the availability and booking xml integration and in the DB sp_UpdateDefaultMapping ('%:[G0-9]%:[YN]:[YN]:[0-9]%:[0-9]%'), sp_GetHotelDetails, sp_ImportRoom, HotelImportControl.ascx (opaque), AutoMapping (JOB)
            var roomId = String.Join(HotelBedsSettings.RoomCharSeparator.ToString(), contractRoomId, roomTypeId.Substring(roomTypeId.IndexOf(HotelBedsSettings.RoomCharSeparator) + 1));
            if (roomId.Length > 50)
            {
                throw new ApplicationException("RoomId is larger than 50 characters -> " + roomId);
            }
            return roomId;
        }

        public string CreateContractRoomIdType(bool nonRefundable, string opaque)
        {
            return HotelBedsSettings.RoomCharSeparator + NonRefundableValue(nonRefundable) + HotelBedsSettings.RoomCharSeparator + OpaqueValue(opaque);
        }

        public Dictionary<string, string> SplitRoomId(string roomId)
        {
            //Any change of the room id structure must be done in the availability and booking xml integration and in the DB sp_UpdateDefaultMapping ('%:[G0-9]%:[YN]:[YN]:[0-9]%:[0-9]%'), sp_GetHotelDetails, sp_ImportRoom, HotelImportControl.ascx (opaque), AutoMapping (JOB)
            var splitValues = new Dictionary<string, string>();

            var fields = roomId.Split(HotelBedsSettings.RoomCharSeparator);
            var isRoomTypeId = (fields.Length == 8);

            for (var i = 0; i < fields.Length; i++)
            {
                var field = fields[i];
                if (isRoomTypeId)
                {
                    if (i == 0)
                    {
                        splitValues.Add("officeCode", String.Empty);
                        splitValues.Add("contractNumber", String.Empty);
                    }
                    switch (i)
                    {
                        case 0: splitValues.Add("companyCode", field); break;
                        case 1: splitValues.Add("roomTypeCode", field); break;
                        case 2: splitValues.Add("roomCharacteristic", field); break;
                        case 3: splitValues.Add("rateCode", field); break;
                        case 4: splitValues.Add("nonRefundable", field); break;
                        case 5: splitValues.Add("opaque", field); break;
                        case 6: splitValues.Add("adults", field); break;
                        case 7: splitValues.Add("childs", field); break;
                    }
                }
                else
                {
                    switch (i)
                    {
                        case 0: splitValues.Add("officeCode", field); break;
                        case 1: splitValues.Add("contractNumber", field); break;
                        case 2: splitValues.Add("companyCode", field); break;
                        case 3: splitValues.Add("roomTypeCode", field); break;
                        case 4: splitValues.Add("roomCharacteristic", field); break;
                        case 5: splitValues.Add("rateCode", field); break;
                        case 6: splitValues.Add("nonRefundable", field); break;
                        case 7: splitValues.Add("opaque", field); break;
                        case 8: splitValues.Add("adults", field); break;
                        case 9: splitValues.Add("childs", field); break;
                    }
                }
            }
            return splitValues;
        }

        public IEnumerable<Room> RoomList(Dictionary<string, Room> baseRooms, Dictionary<string, RoomGroup> baseRoomGroups, string roomKeyId, Dictionary<string, List<Room>> baseRoomsIndex)
        {
            IEnumerable<Room> roomList;
            RoomGroup roomGroup;
            if (baseRoomGroups.TryGetValue(roomKeyId, out roomGroup))
            {
                roomList = roomGroup.Rooms.Values;
            }
            else
            {
                List<Room> rooms;
                if (!baseRoomsIndex.TryGetValue(roomKeyId, out rooms))
                {
                    //RoomGroupId -> officeCode:contractNumber:companyCode:roomType:roomCharacteristic:rateCode:nonRefundable:opaque
                    var splitKey = roomKeyId.IndexOf(_doubleChar, StringComparison.Ordinal) >= 0;
                    rooms = new List<Room>();
                    foreach (var roomId in baseRooms.Keys)
                    {
                        if (!splitKey)
                        {
                            var compare = true;
                            for (var i = 0; i < roomKeyId.Length; i++)
                            {
                                if (roomId[i] != roomKeyId[i])
                                {
                                    compare = false;
                                    break;
                                }
                            }
                            if (compare && roomId[roomKeyId.Length].Equals(HotelBedsSettings.RoomCharSeparator))
                            {
                                rooms.Add(baseRooms[roomId]);
                            }
                        }
                        else
                        {
                            var splitRoomKey = roomId.Split(HotelBedsSettings.RoomCharSeparator);
                            var splitRoomGrp = roomKeyId.Split(HotelBedsSettings.RoomCharSeparator);
                            //Contract Room Id -> officeCode:contractNumber:companyCode
                            if (splitRoomKey[0].Equals(splitRoomGrp[0]) && splitRoomKey[1].Equals(splitRoomGrp[1]) && splitRoomKey[2].Equals(splitRoomGrp[2]))
                            {
                                var startIndex = 3;
                                var allOk = true;
                                for (var i = startIndex; i < splitRoomGrp.Length; i++)
                                {
                                    if (!String.IsNullOrEmpty(splitRoomGrp[i]) && !splitRoomKey[i].Equals(splitRoomGrp[i]))
                                    {
                                        allOk = false;
                                        break;
                                    }
                                }
                                if (allOk)
                                {
                                    rooms.Add(baseRooms[roomId]);
                                }
                            }
                        }
                    }
                    baseRoomsIndex.Add(roomKeyId, rooms);
                }
                roomList = rooms;
            }
            return roomList;
        }

        #endregion

        #region Prices functions

        public void PriceBoard(AvailabilityPeriod ap, string boardPriceType, decimal amount, decimal? extraBedChild, bool isPerPax, int adults)
        {
            if (String.IsNullOrEmpty(ap.Pricing_BookableBoardId))
            {
                ap.Pricing_BookableBoardId = boardPriceType;
                ap.Pricing_BaseIncludedBoard = HotelBedsSettings.MapMeals[boardPriceType];
                ap.Pricing_BasePrice = amount.ToString(NumberFormatInfo.InvariantInfo);
                ap.Pricing_ExtrabedAdult = "0";
                if (extraBedChild.HasValue)
                {
                    ap.Pricing_ExtrabedChild = extraBedChild.Value.ToString(NumberFormatInfo.InvariantInfo);
                }
                else
                {
                    ap.Pricing_ExtrabedChild = "0";
                }
            }
            else if (ap.Pricing_BookableBoardId.Equals(boardPriceType))
            {
                var addBoard = PriceBoardManager(ap, boardPriceType, amount, extraBedChild, false, isPerPax, adults);
                if (!addBoard)
                {
                    //LogLine(logList, indexLog, "override board price failed", line);
                }
            }
            else
            {
                var createBoard = true;
                foreach (var additionalBoard in ap.AdditionalBoards)
                {
                    if (additionalBoard.BookableBoardId.Equals(boardPriceType))
                    {
                        createBoard = false;
                        break;
                    }
                }
                //Sometimes there are 2 codes (BB, CB) refering to the same SunHotels board (breakfast). Add will fail because We can't support both. We choose the first one
                var addBoard = PriceBoardManager(ap, boardPriceType, amount, extraBedChild, createBoard, isPerPax, adults);
                if (!addBoard)
                {
                    //LogLine(logList, indexLog, "add board failed", line);
                }
            }
        }

        private bool PriceBoardManager(AvailabilityPeriod ap, string boardPriceType, decimal amount, decimal? extraBedChild, bool create, bool isPerPax, int adults)
        {
            var addBoard = false;

            var sunHotelsBoard = HotelBedsSettings.MapMeals[boardPriceType];

            //Get type id
            var availabilityPeriod = new AvailabilityPeriod();
            availabilityPeriod.Pricing_BaseIncludedBoard = sunHotelsBoard;
            var boardPriceTypeId = availabilityPeriod.BoardTypeId;

            //List
            var listBoards = new SortedList<int, Dictionary<string, string>>();

            //Fill base values
            listBoards.Add(ap.BoardTypeId, new Dictionary<string, string> {
                {"Pricing_BaseIncludedBoard", ap.Pricing_BaseIncludedBoard},
                {"Pricing_BasePrice", ap.Pricing_BasePrice},
                {"Pricing_BookableBoardId", ap.Pricing_BookableBoardId},
                {"Pricing_ExtrabedAdult", ap.Pricing_ExtrabedAdult},
                {"Pricing_ExtrabedChild", ap.Pricing_ExtrabedChild}
            });

            //Fill additional boards
            foreach (var additionalBoard in ap.AdditionalBoards)
            {
                listBoards.Add(additionalBoard.TypeId, new Dictionary<string, string> {
                    {"Pricing_BaseIncludedBoard", additionalBoard.Type},
                    {"Pricing_BasePrice", ap.Pricing_BasePrice},
                    {"Pricing_BookableBoardId", additionalBoard.BookableBoardId},
                    {"Pricing_ExtrabedAdult", additionalBoard.Adult},
                    {"Pricing_ExtrabedChild", additionalBoard.Child}
                });
            }

            //Check board
            if (create && !listBoards.ContainsKey(boardPriceTypeId))
            {
                addBoard = true;

                //Add the new board
                listBoards.Add(boardPriceTypeId, new Dictionary<string, string> {
                    {"Pricing_BaseIncludedBoard", sunHotelsBoard},
                    {"Pricing_BasePrice", amount.ToString(NumberFormatInfo.InvariantInfo)},
                    {"Pricing_BookableBoardId", boardPriceType},
                    {"Pricing_ExtrabedAdult", "0"},
                    {"Pricing_ExtrabedChild", extraBedChild.HasValue ? extraBedChild.Value.ToString(NumberFormatInfo.InvariantInfo) : "0"}
                });
            }
            else if (!create && listBoards.ContainsKey(boardPriceTypeId))
            {
                addBoard = true;

                listBoards[boardPriceTypeId]["Pricing_BasePrice"] = amount.ToString(NumberFormatInfo.InvariantInfo);
                if (extraBedChild.HasValue)
                {
                    listBoards[boardPriceTypeId]["Pricing_ExtrabedChild"] = extraBedChild.Value.ToString(NumberFormatInfo.InvariantInfo);
                }
            }

            if (addBoard)
            {
                //Order new list of boards
                decimal basePrice = 0;
                decimal basePriceChild = 0;
                var baseCreated = false;
                ap.AdditionalBoards.Clear();
                foreach (var bType in listBoards.Keys)
                {
                    if (!baseCreated)
                    {
                        ap.Pricing_BaseIncludedBoard = listBoards[bType]["Pricing_BaseIncludedBoard"];
                        ap.Pricing_BasePrice = listBoards[bType]["Pricing_BasePrice"];
                        ap.Pricing_BookableBoardId = listBoards[bType]["Pricing_BookableBoardId"];
                        ap.Pricing_ExtrabedAdult = listBoards[bType]["Pricing_ExtrabedAdult"];
                        ap.Pricing_ExtrabedChild = listBoards[bType]["Pricing_ExtrabedChild"];
                        basePrice = Decimal.Parse(ap.Pricing_BasePrice, NumberFormatInfo.InvariantInfo);
                        basePriceChild = Decimal.Parse(ap.Pricing_ExtrabedChild, NumberFormatInfo.InvariantInfo);
                        baseCreated = true;
                    }
                    else
                    {
                        var oldBasePrice = Decimal.Parse(listBoards[bType]["Pricing_BasePrice"], NumberFormatInfo.InvariantInfo);
                        var oldAdult = oldBasePrice + ((isPerPax ? adults : 1 ) * Decimal.Parse(listBoards[bType]["Pricing_ExtrabedAdult"], NumberFormatInfo.InvariantInfo));
                        var oldChild = Decimal.Parse(listBoards[bType]["Pricing_ExtrabedChild"], NumberFormatInfo.InvariantInfo);
                        var newAdult = oldAdult - basePrice;
                        var newChild = oldChild - basePriceChild;
                        if (newAdult < 0)
                        {
                            newAdult = 0;
                        }
                        else if (isPerPax)
                        {
                            newAdult = Decimal.Divide(newAdult, adults);
                        }
                        if (newChild < 0)
                        {
                            newChild = 0;
                        }

                        var additionalBoard = new AvailabilityPeriod.AdditionalBoard(
                                listBoards[bType]["Pricing_BaseIncludedBoard"],
                                newAdult.ToString(NumberFormatInfo.InvariantInfo),
                                newChild.ToString(NumberFormatInfo.InvariantInfo),
                                listBoards[bType]["Pricing_BookableBoardId"]
                        );
                        ap.AdditionalBoards.Add(additionalBoard);
                    }
                }
            }

            return addBoard;
        }

        public string ApplyExchangeRate(string field, decimal rateExchange)
        {
            var txt = String.Empty;
            if (!String.IsNullOrEmpty(field))
            {
                var amount = Decimal.Round((Decimal.Parse(field, NumberFormatInfo.InvariantInfo) * Decimal.Divide(Decimal.One, rateExchange)), 2, MidpointRounding.AwayFromZero);
                txt = amount.ToString(NumberFormatInfo.InvariantInfo);
            }
            return txt;
        }

        public string ApplyRoundPrice(string field)
        {
            var txt = String.Empty;
            if (!String.IsNullOrEmpty(field))
            {
                var amount = Decimal.Round(Decimal.Parse(field, NumberFormatInfo.InvariantInfo), 2, MidpointRounding.AwayFromZero);
                txt = amount.ToString(NumberFormatInfo.InvariantInfo);
            }
            return txt;
        }

        public string ApplyExtraCharge(string field, decimal? amount, decimal? percent)
        {
            var txt = String.Empty;
            if (!String.IsNullOrEmpty(field))
            {
                var amountField = Decimal.Parse(field, NumberFormatInfo.InvariantInfo);
                amountField = amountField + (amount.HasValue ? amount.Value : (percent.HasValue ? Decimal.Divide(amountField * percent.Value, 100) : Decimal.Zero));
                if (amountField < 0)
                {
                    amountField = 0;
                }
                txt = amountField.ToString(NumberFormatInfo.InvariantInfo);
            }
            return txt;
        }

        public Dictionary<string, decimal> GetExchangeRate(HotelBedsSettings settings)
        {
            var exchangeRate = new Dictionary<string, decimal>();
            using (var connection = new SqlConnection(settings.Base.CurrencyConnectionString))
            {
                using (var command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandText = "SELECT currency, rate FROM exchange_rates_margin WHERE currency IN ('" + String.Join("','", HotelBedsSettings.MapCurrency.Values) + "')";
                    using (var dataTable = new DataTable())
                    {
                        using (var sqlDataAdapter = new SqlDataAdapter())
                        {
                            sqlDataAdapter.SelectCommand = command;
                            sqlDataAdapter.Fill(dataTable);
                        }

                        if (dataTable.Rows.Count > 0)
                        {
                            for (var i = 0; i < dataTable.Rows.Count; i++)
                            {
                                var row = dataTable.Rows[i];
                                var currency = (string)row["currency"];
                                var rate = (decimal)row["rate"];

                                foreach (var hotelBedsCurrency in HotelBedsSettings.MapCurrency.Keys)
                                {
                                    if (HotelBedsSettings.MapCurrency[hotelBedsCurrency] == currency)
                                    {
                                        exchangeRate.Add(hotelBedsCurrency, rate);
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return exchangeRate;
        }

        public void PriceBaseRoomsBoard(
            Room room,
            DateTime date,
            string rate,
            string boardCode,
            decimal priceBase,
            decimal? priceChild,
            bool isPerPax,
            Dictionary<string, Dictionary<DateTime, Dictionary<string, Dictionary<string, decimal?[]>>>> baseRoomsPriceBoard,
            Dictionary<string, Dictionary<DateTime, Dictionary<string, Dictionary<string, bool>>>> baseRoomsPriceBoardIsPerPax
        )
        {
            if (!baseRoomsPriceBoard.ContainsKey(room.RoomId))
            {
                baseRoomsPriceBoard.Add(room.RoomId, new Dictionary<DateTime, Dictionary<string, Dictionary<string, decimal?[]>>>());
                baseRoomsPriceBoardIsPerPax.Add(room.RoomId, new Dictionary<DateTime, Dictionary<string, Dictionary<string, bool>>>());
            }
            if (!baseRoomsPriceBoard[room.RoomId].ContainsKey(date))
            {
                baseRoomsPriceBoard[room.RoomId].Add(date, new Dictionary<string, Dictionary<string, decimal?[]>>());
                baseRoomsPriceBoardIsPerPax[room.RoomId].Add(date, new Dictionary<string, Dictionary<string, bool>>());
            }
            if (!baseRoomsPriceBoard[room.RoomId][date].ContainsKey(rate))
            {
                baseRoomsPriceBoard[room.RoomId][date].Add(rate, new Dictionary<string, decimal?[]>());
                baseRoomsPriceBoardIsPerPax[room.RoomId][date].Add(rate, new Dictionary<string, bool>());
            }
            if (!baseRoomsPriceBoard[room.RoomId][date][rate].ContainsKey(boardCode))
            {
                baseRoomsPriceBoard[room.RoomId][date][rate].Add(boardCode, new[] { priceBase, priceChild });
                baseRoomsPriceBoardIsPerPax[room.RoomId][date][rate].Add(boardCode, isPerPax);
            }
            else
            {
                if (baseRoomsPriceBoard[room.RoomId][date][rate][boardCode][0].Value < priceBase)
                {
                    baseRoomsPriceBoard[room.RoomId][date][rate][boardCode][0] = priceBase;
                }
                if (
                    priceChild.HasValue
                    && (
                        !baseRoomsPriceBoard[room.RoomId][date][rate][boardCode][1].HasValue
                        || baseRoomsPriceBoard[room.RoomId][date][rate][boardCode][1].Value < priceChild.Value
                    )
                )
                {
                    baseRoomsPriceBoard[room.RoomId][date][rate][boardCode][1] = priceChild;
                }
            }
        }

        #endregion

        #region Cancellation policies functions

        public string CancellationPolicyId(string roomRate, string contractRoomIdType, string penaltyBasis, int deadlineValue, int penaltyValue)
        {
            return roomRate + contractRoomIdType + HotelBedsSettings.RoomCharSeparator + String.Join(HotelBedsSettings.RoomCharSeparator.ToString(), penaltyBasis, deadlineValue.ToString(), penaltyValue.ToString());
        }

        public CancellationPolicy CancellationPolicyCreate(Dictionary<string, CancellationPolicy> baseCancellationPolicies, string canPolId, string penaltyBasis, int deadlineValue, int penaltyValue)
        {
            CancellationPolicy canPol;
            if (!baseCancellationPolicies.ContainsKey(canPolId))
            {
                canPol = new CancellationPolicy();
                canPol.Id = canPolId;
                canPol.Penalty_Basis = penaltyBasis;
                canPol.Penalty_Value = penaltyValue;
                canPol.Deadline_Basis = "arrival";
                canPol.Deadline_Unit = "hours";
                canPol.Deadline_Value = deadlineValue;
            }
            else
            {
                canPol = baseCancellationPolicies[canPolId];
            }
            return canPol;
        }

        public int CancellationPolicyRoundHours(int hours, int maxDeadlineHours)
        {
            var result = hours;

            if (hours > 0 && hours < maxDeadlineHours)
            {
                if (hours < 12)
                {
                    result = 12;
                }
                else if (hours > 12 && hours < 24)
                {
                    result = 24;
                }
                else if (hours > 24)
                {
                    result = ((hours / 24) * 24) + (hours % 24 > 0 ? 24 : 0);
                }
            }
            else if (hours < 0 || hours > maxDeadlineHours)
            {
                result = maxDeadlineHours;
            }
            //We allow result = 0, 12, 24 and multiples of 24

            return result;
        }

        public CancellationPolicy CancellationPolicyResolveConflicts(
            string roomRate,
            string contractRoomIdType,
            Dictionary<string, CancellationPolicy> baseCancellationPolicies,
            List<string> cancellationPolicies,
            ref bool hasConflict,
            ref int indexPolicy,
            string penaltyBasis,
            int deadlineValue,
            int penaltyValue
        )
        {
            //Cancellation policies must be ordered by deadline to be able to resolve conflicts
            CancellationPolicy canPol = null;
            if (cancellationPolicies != null && indexPolicy < cancellationPolicies.Count)
            {
                var conflictPol = String.Empty;
                var newCanPol = false;
                var newCanPolPenaltyBasis = String.Empty;
                var newCanPolDeadline = 0;
                var newCanPolPenalty = 0;
                
                var cp = baseCancellationPolicies[cancellationPolicies[indexPolicy]];

                //Check duplicate
                if (penaltyBasis == cp.Penalty_Basis && deadlineValue == cp.Deadline_Value.Value && penaltyValue == cp.Penalty_Value)
                {
                    hasConflict = true;
                    return null;
                }

                //Different type
                if (penaltyBasis != cp.Penalty_Basis)
                {
                    //Same deadline, set as NRF (can't know exactly which one is the most restrictive)
                    //if fixed_amount 500EUR is more restrictive than full_stay 30% or 2 nights...
                    if (deadlineValue == cp.Deadline_Value.Value)
                    {
                        conflictPol = cp.Id;
                        newCanPol = true;
                        newCanPolPenaltyBasis = "full_stay";
                        newCanPolDeadline = deadlineValue;
                        newCanPolPenalty = 100;
                    }
                    else
                    {
                        //Different deadline, more restrictive 100% full_stay
                        if (penaltyBasis == "full_stay" && deadlineValue > cp.Deadline_Value.Value)
                        {
                            conflictPol = cp.Id;
                            newCanPol = true;
                            newCanPolPenaltyBasis = "full_stay";
                            newCanPolDeadline = deadlineValue;
                            newCanPolPenalty = 100;
                        }
                        else if (cp.Penalty_Basis == "full_stay" && deadlineValue < cp.Deadline_Value.Value)
                        {
                            conflictPol = cp.Id;
                            //Conflict and it won't be created
                        }
                    }
                }
                else
                {
                    //Same type
                    if (
                        (deadlineValue != cp.Deadline_Value.Value || penaltyValue != cp.Penalty_Value)
                        && !(deadlineValue > cp.Deadline_Value.Value && penaltyValue < cp.Penalty_Value) //higher deadline and lower penalty, create new policy
                        && !(deadlineValue < cp.Deadline_Value.Value && penaltyValue > cp.Penalty_Value) //lower deadline and higher penalty, create new policy
                    )
                    {
                        var sameDeadlineHighPenalty = deadlineValue == cp.Deadline_Value.Value && penaltyValue > cp.Penalty_Value;
                        var samePenaltyHighDeadline = deadlineValue > cp.Deadline_Value.Value && penaltyValue == cp.Penalty_Value;
                        var highDeadlineHighPenalty = deadlineValue > cp.Deadline_Value.Value && penaltyValue > cp.Penalty_Value;
                        //lower deadline and lower penalty, conflict and it won't be created
                        conflictPol = cp.Id;
                        newCanPol = sameDeadlineHighPenalty || samePenaltyHighDeadline || highDeadlineHighPenalty;
                        if (newCanPol)
                        {
                            newCanPolPenaltyBasis = cp.Penalty_Basis;
                            newCanPolDeadline = samePenaltyHighDeadline || highDeadlineHighPenalty ? deadlineValue : cp.Deadline_Value.Value;
                            newCanPolPenalty = sameDeadlineHighPenalty || highDeadlineHighPenalty ? penaltyValue : cp.Penalty_Value;
                        }
                    }
                }

                if (newCanPol || String.IsNullOrEmpty(conflictPol))
                {
                    if (newCanPol && !String.IsNullOrEmpty(conflictPol))
                    {
                        cancellationPolicies.Remove(conflictPol);
                    }

                    if ((newCanPol && !String.IsNullOrEmpty(conflictPol)) || (String.IsNullOrEmpty(conflictPol) && indexPolicy + 1 < cancellationPolicies.Count))
                    {
                        if (String.IsNullOrEmpty(conflictPol))
                        {
                            //No conflict, check next
                            indexPolicy += 1;
                        }
                        if (newCanPol && !String.IsNullOrEmpty(conflictPol) && indexPolicy > 0)
                        {
                            //Conflict, start checking again from the first one
                            indexPolicy = 0;
                        }
                        canPol = CancellationPolicyResolveConflicts(
                            roomRate,
                            contractRoomIdType,
                            baseCancellationPolicies,
                            cancellationPolicies,
                            ref hasConflict,
                            ref indexPolicy,
                            newCanPol ? newCanPolPenaltyBasis : penaltyBasis,
                            newCanPol ? newCanPolDeadline : deadlineValue,
                            newCanPol ? newCanPolPenalty : penaltyValue
                        );
                    }

                    if (!hasConflict && canPol == null)
                    {
                        if (String.IsNullOrEmpty(conflictPol))
		                {
		                    //Create a new policy
		                    newCanPolPenaltyBasis = penaltyBasis;
		                    newCanPolDeadline = deadlineValue;
		                    newCanPolPenalty = penaltyValue;
                        }

                        var canPolId = CancellationPolicyId(roomRate, contractRoomIdType, newCanPolPenaltyBasis, newCanPolDeadline, newCanPolPenalty);
                        canPol = CancellationPolicyCreate(baseCancellationPolicies, canPolId, newCanPolPenaltyBasis, newCanPolDeadline, newCanPolPenalty);
                    }
                }
                else
                {
                    hasConflict = true;
                }
            }
            return canPol;
        }

        #endregion

        #region Boards functions

        public int SunHotelsBoardTypeId(string boardCode)
        {
            var boardPriceTypeId = -1;
            if (HotelBedsSettings.MapMeals.ContainsKey(boardCode))
            {
                var sunHotelsBoard = HotelBedsSettings.MapMeals[boardCode];

                //Get type id
                var availabilityPeriod = new AvailabilityPeriod();
                availabilityPeriod.Pricing_BaseIncludedBoard = sunHotelsBoard;
                boardPriceTypeId = availabilityPeriod.BoardTypeId;
            }
            return boardPriceTypeId;
        }

        #endregion

        #region Combinable offers functions

        public bool CombinationOfferCodeExists(List<object>[] combOffers, string code)
        {
            return (combOffers[0].Contains(code) || combOffers[1].Contains(code));
        }

        private int CombinationOfferIndex(List<object>[] combOffers, string code1, string code2)
        {
            for (var i = 0; i < combOffers[0].Count; i++)
            {
                if ((string)combOffers[0][i] == code1 && (string)combOffers[1][i] == code2)
                {
                    return i;
                }
            }
            return -1;
        }

        private bool CombinationOfferExclusionCode(List<object>[] combOffers, int index, string code)
        {
            return ((string)combOffers[1][index] == code);
        }

        private bool CombinationOfferExclusion(List<object>[] combOffers, int index)
        {
            return (bool)combOffers[2][index];
        }

        private bool CombinationOfferMixed(List<object>[] combOffers, int index)
        {
            return (bool)combOffers[3][index];
        }

        public void CombinationExcludeCheckList(
            List<object>[] combOffers,
            Dictionary<object, Dictionary<DateTime, Dictionary<string, Dictionary<string, string>>>> hotelLineDatesApply,
            Dictionary<string, string> line,
            string code,
            Dictionary<object, int> lineCombOfferList,
            bool isSupplement
        )
        {
            foreach (Dictionary<string, string> otherLine in hotelLineDatesApply.Keys)
            {
                if (line != otherLine)
                {
                    var indexComb1 = CombinationOfferIndex(combOffers, otherLine[(isSupplement ? "Supplement or discount code" : "Free code")], code);
                    var indexComb2 = CombinationOfferIndex(combOffers, code, otherLine[(isSupplement ? "Supplement or discount code" : "Free code")]);
                    if ((indexComb1 >= 0 || indexComb2 >= 0) && !lineCombOfferList.ContainsKey(otherLine))
                    {
                        lineCombOfferList.Add(otherLine, indexComb1 >= 0 ? indexComb1 : indexComb2);
                    }
                }
            }
        }

        public bool CombinationOfferApply(
            List<object>[] combOffers,
            Dictionary<object, Dictionary<DateTime, Dictionary<string, Dictionary<string, string>>>> hotelSupplementDatesApply,
            Dictionary<object, Dictionary<DateTime, Dictionary<string, Dictionary<string, string>>>> hotelFreeDatesApply,
            Dictionary<object, int> lineCombOfferList,
            string code,
            DateTime date,
            string marketCode,
            string roomTypeCode,
            string roomCharacteristic,
            string boardCode,
            string rate,
            bool isSupplement
        )
        {
            foreach (var line in lineCombOfferList.Keys)
            {
                var suppOk = CombinationOfferApplyCheck(combOffers, hotelSupplementDatesApply, lineCombOfferList, code, date, line, marketCode, roomTypeCode, roomCharacteristic, boardCode, rate, isSupplement);
                var freeOk = CombinationOfferApplyCheck(combOffers, hotelFreeDatesApply, lineCombOfferList, code, date, line, marketCode, roomTypeCode, roomCharacteristic, boardCode, rate, isSupplement);
                if (!suppOk || !freeOk)
                {
                    return false;
                }
            }
            return true;
        }

        private bool CombinationOfferApplyCheck(
            List<object>[] combOffers,
            Dictionary<object, Dictionary<DateTime, Dictionary<string, Dictionary<string, string>>>> hotelLineDatesApply,
            Dictionary<object, int> lineCombOfferList,
            string code,
            DateTime date,
            object line,
            string marketCode,
            string roomTypeCode,
            string roomCharacteristic,
            string boardCode,
            string rate,
            bool isSupplement
        )
        {
            if (hotelLineDatesApply.ContainsKey(line) && hotelLineDatesApply[line].ContainsKey(date))
            {
                foreach (var fields in hotelLineDatesApply[line][date].Values)
                {
                    if (
                        fields["Market"] == marketCode && fields["Room type"] == roomTypeCode
                        && fields["Characteristic"] == roomCharacteristic && fields["Board"] == boardCode
                        && fields["Rate"] == rate
                    )
                    {
                        var indexComb = lineCombOfferList[line];
                        var excludeCode = CombinationOfferExclusionCode(combOffers, indexComb, code);
                        var mixedOffer = CombinationOfferMixed(combOffers, indexComb);
                        var excludeCombOffer = CombinationOfferExclusion(combOffers, indexComb);
                        if ((isSupplement && mixedOffer && excludeCombOffer) || (excludeCombOffer && !mixedOffer && excludeCode))
                        {
                            return false;
                        }
                    }
                }
            }
            return true;
        }

        public void DailyCombinationSupplement(
            SortedList<int, Dictionary<string, string>> sortedSupplements,
            Dictionary<string, List<string>> distinctCombinations,
            Dictionary<string, Dictionary<string, string>> splittedCombinations,
            SortedList<int, Dictionary<string, string>> sortedRates,
            IHotelBedsDataTable roomTypesTable,
            List<string> boardsList,
            List<string> contractMarkets,
            bool fixRate,
            string opaque,
            DateTime cacheIni,
            DateTime cacheEnd,
            HashSet<DateTime> contractDates,
            List<object>[] combOffers,
            DateTime todayDate,
            Dictionary<object, HashSet<DateTime>> hotelSupplementDates,
            Dictionary<object, Dictionary<DateTime, Dictionary<string, Dictionary<string, string>>>> hotelSupplementDatesApply
        )
        {
            var genericTableCombinations = new Dictionary<DateTime, Dictionary<string, bool>>();

            foreach (var supplementOrder in sortedSupplements.Keys)
            {
                var line = sortedSupplements[supplementOrder];
                var supplementCode = line["Supplement or discount code"];
                if (CombinationOfferCodeExists(combOffers, supplementCode) && !hotelSupplementDatesApply.ContainsKey(line))
                {
                    var applicationStartDate = ParseDate(line["Application initial date"]);
                    var applicationFinalDate = !String.IsNullOrEmpty(line["Application final date"]) ? ParseDate(line["Application final date"]) : cacheEnd;
                    if (String.IsNullOrEmpty(line["Application initial date"]) || applicationStartDate > todayDate || todayDate > applicationFinalDate)
                    {
                        continue;
                    }
                    var txtRoomTypeCode = line["Room type"];
                    var txtRoomCharacteristic = line["Characteristic"];
                    var txtRate = line["Rate"];
                    var txtBoardCode = line["Board"];
                    var txtMarket = line["Market price"];
                    var supplementDates = SupplementDates(hotelSupplementDates, line, cacheIni, cacheEnd, contractDates);
                    var applyOpaque = line["Opaque"].Equals("Y");
                    var onMonday = line["On Monday"].Equals("Y");
                    var onTuesday = line["On Tuesday"].Equals("Y");
                    var onWednesday = line["On Wednesday"].Equals("Y");
                    var onThursday = line["On Thursday"].Equals("Y");
                    var onFriday = line["On Friday"].Equals("Y");
                    var onSaturday = line["On Saturday"].Equals("Y");
                    var onSunday = line["On Sunday"].Equals("Y");

                    if (applyOpaque && !opaque.Equals("P"))
                    {
                        continue;
                    }

                    var supplementDatesApply = new Dictionary<DateTime, Dictionary<string, Dictionary<string, string>>>();
                    hotelSupplementDatesApply.Add(line, supplementDatesApply);

                    var rowCombinations = RowCombinations(distinctCombinations, txtRoomTypeCode, txtRoomCharacteristic, txtRate, txtBoardCode, txtMarket, sortedRates, roomTypesTable, boardsList, contractMarkets);

                    foreach (var date in supplementDates)
                    {
                        if (date.DayOfWeek.Equals(DayOfWeek.Monday) && !onMonday) continue;
                        if (date.DayOfWeek.Equals(DayOfWeek.Tuesday) && !onTuesday) continue;
                        if (date.DayOfWeek.Equals(DayOfWeek.Wednesday) && !onWednesday) continue;
                        if (date.DayOfWeek.Equals(DayOfWeek.Thursday) && !onThursday) continue;
                        if (date.DayOfWeek.Equals(DayOfWeek.Friday) && !onFriday) continue;
                        if (date.DayOfWeek.Equals(DayOfWeek.Saturday) && !onSaturday) continue;
                        if (date.DayOfWeek.Equals(DayOfWeek.Sunday) && !onSunday) continue;

                        var combNotProcessed = FilterCombinations(genericTableCombinations, rowCombinations, date);
                        foreach (var cnp in combNotProcessed)
                        {
                            var fields = SplitCombination(splittedCombinations, cnp, null);

                            if (!supplementDatesApply.ContainsKey(date))
                            {
                                supplementDatesApply.Add(date, new Dictionary<string, Dictionary<string, string>>());
                            }
                            if (!supplementDatesApply[date].ContainsKey(cnp))
                            {
                                supplementDatesApply[date].Add(cnp, fields);
                            }
                        }
                    }
                }
            }
        }

        public void DailyCombinationFree(
            IHotelBedsDataTable freesTable,
            Dictionary<string, List<string>> distinctCombinations,
            Dictionary<string, Dictionary<string, string>> splittedCombinations,
            SortedList<int, Dictionary<string, string>> sortedRates,
            IHotelBedsDataTable roomTypesTable,
            List<string> boardsList,
            List<string> contractMarkets,
            bool fixRate,
            DateTime cacheIni,
            DateTime cacheEnd,
            HashSet<DateTime> contractDates,
            List<object>[] combOffers,
            DateTime todayDate,
            Dictionary<object, HashSet<DateTime>> hotelFreeDates,
            Dictionary<object, Dictionary<DateTime, Dictionary<string, Dictionary<string, string>>>> hotelFreeDatesApply
        )
        {
            var sortedFreesTable = OrderDataTable(freesTable, "Room type", "Characteristic", "Rate", "Board", String.Empty);
            var genericTableCombinations = new Dictionary<DateTime, Dictionary<string, bool>>();

            foreach (var line in sortedFreesTable)
            {
                var freeCode = line["Free code"];
                if (CombinationOfferCodeExists(combOffers, freeCode) && !hotelFreeDatesApply.ContainsKey(line))
                {
                    var applicationStartDate = !String.IsNullOrEmpty(line["Application initial date"]) ? ParseDate(line["Application initial date"]) : cacheIni;
                    var applicationFinalDate = !String.IsNullOrEmpty(line["Application final date"]) ? ParseDate(line["Application final date"]) : cacheEnd;
                    if (String.IsNullOrEmpty(line["Application initial date"]) || applicationStartDate > todayDate || todayDate > applicationFinalDate)
                    {
                        continue;
                    }
                    var txtRoomTypeCode = line["Room type"];
                    var txtRoomCharacteristic = line["Characteristic"];
                    var txtRate = line["Rate"];
                    var txtBoardCode = line["Board"];
                    var freeDates = SupplementDates(hotelFreeDates, line, cacheIni, cacheEnd, contractDates);
                    var onMonday = line["On Monday"].Equals("Y");
                    var onTuesday = line["On Tuesday"].Equals("Y");
                    var onWednesday = line["On Wednesday"].Equals("Y");
                    var onThursday = line["On Thursday"].Equals("Y");
                    var onFriday = line["On Friday"].Equals("Y");
                    var onSaturday = line["On Saturday"].Equals("Y");
                    var onSunday = line["On Sunday"].Equals("Y");

                    //According to their doc. "If all the days has N value, then every day of the week can be applied"
                    if (!onMonday && !onTuesday && !onWednesday && !onThursday && !onFriday && !onSaturday && !onSunday)
                    {
                        onMonday = onTuesday = onWednesday = onThursday = onFriday = onSaturday = onSunday = true;
                    }

                    var freeDatesApply = new Dictionary<DateTime, Dictionary<string, Dictionary<string, string>>>();
                    hotelFreeDatesApply.Add(line, freeDatesApply);

                    var rowCombinations = RowCombinations(distinctCombinations, txtRoomTypeCode, txtRoomCharacteristic, txtRate, txtBoardCode, String.Empty, sortedRates, roomTypesTable, boardsList, contractMarkets);

                    foreach (var date in freeDates)
                    {
                        if (date.DayOfWeek.Equals(DayOfWeek.Monday) && !onMonday) continue;
                        if (date.DayOfWeek.Equals(DayOfWeek.Tuesday) && !onTuesday) continue;
                        if (date.DayOfWeek.Equals(DayOfWeek.Wednesday) && !onWednesday) continue;
                        if (date.DayOfWeek.Equals(DayOfWeek.Thursday) && !onThursday) continue;
                        if (date.DayOfWeek.Equals(DayOfWeek.Friday) && !onFriday) continue;
                        if (date.DayOfWeek.Equals(DayOfWeek.Saturday) && !onSaturday) continue;
                        if (date.DayOfWeek.Equals(DayOfWeek.Sunday) && !onSunday) continue;

                        var combNotProcessed = FilterCombinations(genericTableCombinations, rowCombinations, date);
                        foreach (var cnp in combNotProcessed)
                        {
                            var fields = SplitCombination(splittedCombinations, cnp, null);

                            if (!freeDatesApply.ContainsKey(date))
                            {
                                freeDatesApply.Add(date, new Dictionary<string, Dictionary<string, string>>());
                            }
                            if (!freeDatesApply[date].ContainsKey(cnp))
                            {
                                freeDatesApply[date].Add(cnp, fields);
                            }
                        }
                    }
                }
            }
        }

        #endregion

    }
}
