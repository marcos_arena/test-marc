﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Globalization;
using SunHotels.XML.Data;

namespace SunHotels.Providers
{
    public class HotelBedsCommonTables
    {
        private HotelBedsSettings Settings { get; set; }
        private HotelBedsCommonFunctions Functions { get; set; }
        private DateTime TodayDate { get; set; }
        private List<string> ReadMarkets { get; set; }
        private readonly string _priceFieldName;
        private readonly string _allotmentFieldName;

        public HotelBedsCommonTables(HotelBedsSettings settings, HotelBedsCommonFunctions functions, DateTime todayDate)
        {
            Settings = settings;
            Functions = functions;
            TodayDate = todayDate;

            ReadMarkets = new List<string>();
            if (!String.IsNullOrEmpty(Settings.ImportMarkets))
            {
                ReadMarkets.AddRange(Settings.ImportMarkets.Split(','));
            }

            _priceFieldName = String.Join(HotelBedsSettings.Aif2TableCharSeparator.ToString(), "Is price per pax", "Net price", "Price", "Specific Rate", "Base board", "Amount");
            _allotmentFieldName = String.Join(HotelBedsSettings.Aif2TableCharSeparator.ToString(), "Release", "Allotment");
        }

        #region Valid markets

        public void TableMarkets(
            IHotelBedsDataTable marketsTable,
            List<string> contractMarkets
        )
        {
            if (marketsTable.Rows.Count == 0)
            {
                contractMarkets.AddRange(ReadMarkets);
            }
            else
            {
                //If the contract has markets we check if we have to process them
                foreach (var line in marketsTable.Rows)
                {
                    var countryCode = line["Country code"];
                    var validMarket = line["Valid for a country"].Equals("Y");
                    if (validMarket && (ReadMarkets.Count == 0 || ReadMarkets.Contains(countryCode)))
                    {
                        contractMarkets.Add(countryCode);
                    }
                }
            }
            if (contractMarkets.Count == 0)
            {
                throw new ApplicationException("Contract without a valid market");
            }
            //Remove this condition after we support more than one market
            if (contractMarkets.Count > 1)
            {
                throw new ApplicationException("Contract with more than one valid market");
            }
        }

        #endregion

        #region Rates

        public void TableRates(
            IHotelBedsDataTable rateCodesTable,
            bool fixRate,
            SortedList<int, Dictionary<string, string>> sortedRates,
            List<string> roomRatesList
        )
        {
            foreach (var line in rateCodesTable.Rows)
            {
                //Order can be 3 digit long so we start with a default of 4 digit
                var rateCount = 1000;
                var rateOrder = 0;
                if (!String.IsNullOrEmpty(line["Order"]))
                {
                    rateOrder = Int32.Parse(line["Order"]);
                }
                else
                {
                    rateOrder = rateCount;
                }
                while (sortedRates.ContainsKey(rateOrder))
                {
                    rateOrder++;
                }
                sortedRates.Add(rateOrder, line);
            }

            if (fixRate)
            {
                foreach (var lineKey in sortedRates.Keys)
                {
                    var line = sortedRates[lineKey];
                    roomRatesList.Add(line["Rate"]);
                }
            }
            if (roomRatesList.Count == 0)
            {
                //Generic rate
                roomRatesList.Add(HotelBedsSettings.GenericRateCode);
            }
        }

        #endregion

        #region Room Price Rate

        public void TableRoomPriceRate(
            IHotelBedsDataTable pricesTable,
            ConcurrentDictionary<string, ConcurrentDictionary<string, ConcurrentBag<string>>> logList,
            string hotelIdName,
            Dictionary<string, List<string>> distinctCombinations,
            Dictionary<string, Dictionary<string, string>> splittedCombinations,
            SortedList<int, Dictionary<string, string>> sortedRates,
            IHotelBedsDataTable roomTypesTable,
            List<string> boardsList,
            bool fixRate,
            string contractBoard,
            HashSet<DateTime> contractDates,
            Dictionary<string, bool> roomPriceRateComb,
            Dictionary<string, Dictionary<DateTime, string[]>> pricesLine,
            List<string> contractMarkets
        )
        {
            var sortedPricesTable = Functions.OrderDataTable(pricesTable, "Room type", "Characteristic", "Generic Rate", String.Empty, "Market price");
            var genericTableCombinations = new Dictionary<DateTime, Dictionary<string, bool>>();

            foreach (var line in sortedPricesTable)
            {
                var txtRoomTypeCode = line["Room type"];
                var txtRoomCharacteristic = line["Characteristic"];
                var txtRate = line["Generic Rate"];
                var txtMarket = line["Market price"];
                var price = line[_priceFieldName];
                var startDate = Functions.ParseDate(line["Initial date"]);
                var finalDate = Functions.ParseDate(line["Final date"]);
                var elements = Functions.SplitGroupedValues(price);
                var priceDates = Functions.DailyValues(startDate, finalDate, elements, contractDates);

                if (!String.IsNullOrEmpty(txtMarket) && !contractMarkets.Contains(txtMarket))
                {
                    line.Remove(_priceFieldName);
                    Functions.LogLine(logList, hotelIdName, "price market ignored", txtMarket);
                    continue;
                }

                if (priceDates.Count > 0)
                {
                    var txtRoomRate = fixRate ? txtRate : HotelBedsSettings.GenericRateCode;

                    var rowCombinations = Functions.RowCombinations(distinctCombinations, txtRoomTypeCode, txtRoomCharacteristic, txtRoomRate, null, txtMarket, sortedRates, roomTypesTable, boardsList, contractMarkets);

                    foreach (var date in priceDates.Keys)
                    {
                        var combNotProcessed = Functions.FilterCombinations(genericTableCombinations, rowCombinations, date);
                        foreach (var cnp in combNotProcessed)
                        {
                            var fields = Functions.SplitCombination(splittedCombinations, cnp, null);
                            var marketCode = fields["Market"];
                            var roomTypeCode = fields["Room type"];
                            var roomCharacteristic = fields["Characteristic"];
                            var rate = fields["Rate"];
                            var boardCode = fields["Board"];
                            var roomRate = fixRate ? rate : HotelBedsSettings.GenericRateCode;

                            var roomPriceComb = String.Join(HotelBedsSettings.RoomCharSeparator.ToString(), roomTypeCode, roomCharacteristic, roomRate, marketCode);
                            var lineComb = String.Join(HotelBedsSettings.RoomCharSeparator.ToString(), line["Initial date"], line["Final date"], roomPriceComb);

                            var priceFields = priceDates[date].Split(',');
                            var isPricePerPax = priceFields[0].Equals("Y");
                            /*var netPrice = priceFields[1];
                            var mandatoryPrice = priceFields[2];*/
                            var priceRate = (!fixRate && !String.IsNullOrEmpty(priceFields[3]) ? priceFields[3] : roomRate);
                            var boardPriceType = (!String.IsNullOrEmpty(priceFields[4]) ? priceFields[4] : contractBoard);
                            var amount = Decimal.Parse(priceFields[5], NumberFormatInfo.InvariantInfo);

                            if (amount <= 0)
                            {
                                continue;
                            }

                            if (!HotelBedsSettings.MapMeals.ContainsKey(boardPriceType))
                            {
                                line.Remove(_priceFieldName);
                                Functions.LogLine(logList, hotelIdName, "board not supported", boardPriceType);
                                continue;
                            }

                            if (!roomPriceRateComb.ContainsKey(roomPriceComb))
                            {
                                roomPriceRateComb.Add(roomPriceComb, false);
                                pricesLine.Add(lineComb, new Dictionary<DateTime, string[]>());
                            }
                            pricesLine[lineComb].Add(date, priceFields);

                            if (isPricePerPax)
                            {
                                roomPriceRateComb[roomPriceComb] = true;
                            }

                            //Mark combination processed
                            genericTableCombinations[date][cnp] = true;
                        }
                    }
                }
            }
        }

        #endregion

        #region Generate rooms

        public void TableRooms(
            IHotelBedsDataTable roomTypesTable,
            ConcurrentDictionary<string, ConcurrentDictionary<string, ConcurrentBag<string>>> logList,
            string hotelIdName,
            ref bool infantRestrictions,
            ref bool adultOnly,
            bool contractHasChilds,
            bool fixRate,
            string contractRoomId,
            string contractRoomIdType,
            string companyCode,
            bool nonRefundable,
            List<string> roomRatesList,
            Dictionary<string, bool> roomPriceRateComb,
            Dictionary<string, RoomType> baseRoomTypes,
            Dictionary<string, RoomGroup> baseRoomGroups,
            Dictionary<string, Room> baseRooms,
            Dictionary<string, int> baseRoomsCapacity,
            List<string> contractMarkets
        )
        {
            foreach (var line in roomTypesTable.Rows)
            {
                var roomTypeCode = line["Room type"];
                var roomCharacteristic = line["Characteristic"];
                //HotelBeds said in a response mail that standard capacity and minimum pax should have the same value, but it's not true...
                var standardCapacity = Int32.Parse(line["Standard capacity"]);
                var minimumPax = Int32.Parse(line["Minimum pax"]);
                var maximumPax = Int32.Parse(line["Maximum pax"]);
                var maximumAdult = Int32.Parse(line["Maximum adult"]);
                var maximumChild = Int32.Parse(line["Maximum children"]);
                var maximumInfants = (!String.IsNullOrEmpty(line["Maximum infants"]) ? Int32.Parse(line["Maximum infants"]) : 0);
                var minimumAdults = (!String.IsNullOrEmpty(line["Minimum adults"]) ? Int32.Parse(line["Minimum adults"]) : 0);
                var minimumChildren = (!String.IsNullOrEmpty(line["Minimum children"]) ? Int32.Parse(line["Minimum children"]) : 0);

                if (minimumPax == 0)
                {
                    Functions.LogLine(logList, hotelIdName, "Room minimum pax is zero", "0");
                    continue;
                }

                if (minimumChildren > 0)
                {
                    Functions.LogLine(logList, hotelIdName, "Room with minimum children", minimumChildren.ToString());
                    continue;
                }

                if (!String.IsNullOrEmpty(line["Maximum infants"]) && maximumInfants == 0)
                {
                    infantRestrictions = true;
                }
                if (maximumChild > 0)
                {
                    adultOnly = false;
                }

                var paxExtra = maximumPax - minimumPax;
                var createRoomGroup = paxExtra > 0 || fixRate;

                for (var i = 0; i <= paxExtra; i++)
                {
                    var paxes = minimumPax + i;
                    if (
                        (minimumAdults <= paxes && paxes <= maximumAdult)
                        || (maximumAdult < paxes && paxes <= maximumPax)
                    )
                    {
                        var adults = (paxes <= maximumAdult) ? paxes : maximumAdult;
                        var childs = !contractHasChilds ? 0 : (maximumPax - adults <= maximumChild) ? (maximumPax - adults) : maximumChild;
                        //Import file only accepts maximun 255 beds or 255 extrabeds (unsigned byte)
                        if (adults <= 255 && childs <= 255)
                        {
                            foreach (var market in contractMarkets)
                            {
                                foreach (var roomRate in roomRatesList)
                                {
                                    //Room rate is not per pax we only create one room for the max occupancy
                                    var roomPriceComb = String.Join(HotelBedsSettings.RoomCharSeparator.ToString(), roomTypeCode, roomCharacteristic, roomRate, market);
                                    if (!roomPriceRateComb.ContainsKey(roomPriceComb) || (!roomPriceRateComb[roomPriceComb] && paxes != maximumAdult))
                                    {
                                        Functions.LogLine(logList, hotelIdName, "room combination not created: Room rate is not per pax we only create one room for the max occupancy", roomPriceComb);
                                        break;
                                    }

                                    var roomGroupId = Functions.CreateRoomGroupId(contractRoomId, roomTypeCode, roomCharacteristic, roomRate, contractRoomIdType);
                                    var roomTypeId = Functions.CreateRoomTypeId(companyCode, roomTypeCode, roomCharacteristic, roomRate, contractRoomIdType, adults, childs);
                                    var roomId = Functions.CreateRoomId(contractRoomId, roomTypeId);

                                    if (
                                        baseRooms.ContainsKey(roomId)
                                        && (
                                            !createRoomGroup || (createRoomGroup && baseRoomGroups.ContainsKey(roomGroupId))
                                        )
                                    )
                                    {
                                        //Room already created
                                        continue;
                                    }

                                    Room room = null;
                                    if (!baseRooms.ContainsKey(roomId))
                                    {
                                        room = new Room();
                                        room.RoomId = roomId;
                                        room.TypeId = roomTypeId;
                                        //Must be true for avoiding price errors when requesting availability with fake pax
                                        room.MealSupplementRestriction = true;
                                        baseRooms.Add(roomId, room);

                                        baseRoomsCapacity.Add(roomId, standardCapacity);
                                    }
                                    else
                                    {
                                        room = baseRooms[roomId];
                                    }

                                    RoomType roomType = null;
                                    if (!baseRoomTypes.ContainsKey(roomTypeId))
                                    {
                                        roomType = new RoomType();
                                        roomType.Type_Id = roomTypeId;
                                        roomType.Beds = adults;
                                        roomType.ExtraBeds = childs;
                                        roomType.IsWeeklyStay = false;
                                        roomType.MinimumStay = 1;
                                        roomType.SharedFacilities = false;
                                        roomType.SharedRoom = false;
                                        roomType.NonRefundable = nonRefundable;
                                        baseRoomTypes.Add(roomTypeId, roomType);
                                    }
                                    else
                                    {
                                        roomType = baseRoomTypes[roomTypeId];
                                    }

                                    if (createRoomGroup)
                                    {
                                        RoomGroup roomGroup = null;
                                        if (!baseRoomGroups.ContainsKey(roomGroupId))
                                        {
                                            roomGroup = new RoomGroup();
                                            roomGroup.RoomGroupId = roomGroupId;
                                            baseRoomGroups.Add(roomGroupId, roomGroup);
                                        }
                                        else
                                        {
                                            roomGroup = baseRoomGroups[roomGroupId];
                                        }
                                        if (!roomGroup.Rooms.ContainsKey(roomId))
                                        {
                                            roomGroup.Rooms.Add(roomId, room);
                                        }
                                        //Check previously added rooms if they fit in
                                        foreach (var otherRoom in baseRooms.Values)
                                        {
                                            if (otherRoom != room)
                                            {
                                                if (
                                                    !roomGroup.Rooms.ContainsKey(otherRoom.RoomId)
                                                    && otherRoom.RoomId.Contains(roomGroupId + HotelBedsSettings.RoomCharSeparator)
                                                )
                                                {
                                                    roomGroup.Rooms.Add(otherRoom.RoomId, otherRoom);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            Functions.LogLine(logList, hotelIdName, "room combination not supported", "adults:" + adults + ", childs:" + childs);
                        }
                    }
                    else
                    {
                        if (minimumAdults <= paxes)
                        {
                            Functions.LogLine(logList, hotelIdName, "room not supported", "minimumAdults:" + minimumAdults + ",paxes:" + paxes);
                        }
                    }
                }
            }

            //Delete room groups that has only one or zero rooms
            var deleteRoomsGroup = new List<string>();
            foreach (var roomGroupKey in baseRoomGroups.Keys)
            {
                if (baseRoomGroups[roomGroupKey].Rooms.Count <= 1)
                {
                    deleteRoomsGroup.Add(roomGroupKey);
                }
            }
            foreach (var roomGroupKey in deleteRoomsGroup)
            {
                baseRoomGroups.Remove(roomGroupKey);
            }
        }

        #endregion

        #region Generate boards

        public void TableBoards(
            IHotelBedsDataTable boardsTable,
            List<string> boardsList,
            string contractBoard
        )
        {
            var listBoards = new List<string>();
            var listBoardsSh = new List<int>();
            
            listBoards.Add(contractBoard);
            listBoardsSh.Add(Functions.SunHotelsBoardTypeId(contractBoard));
            foreach (var line in boardsTable.Rows)
            {
                var boardCode = line["Board code"];
                if (!listBoards.Contains(boardCode))
                {
                    listBoards.Add(boardCode);
                    listBoardsSh.Add(Functions.SunHotelsBoardTypeId(boardCode));
                }
            }

            //Important to sort the boards per type
            var boardCopyList = new List<int>();
            boardCopyList.AddRange(listBoardsSh);
            boardCopyList.Sort();
            foreach (var bTypeId in boardCopyList)
            {
                for (var i = 0; i < listBoardsSh.Count; i++)
                {
                    if (bTypeId == listBoardsSh[i] && !boardsList.Contains(listBoards[i]))
                    {
                        boardsList.Add(listBoards[i]);
                        break;
                    }
                }
            }
        }

        #endregion

        #region Fill availability

        public void TableAvailability(
            IHotelBedsDataTable inventoryTable,
            bool fixRate,
            string contractRoomId,
            string contractRoomIdType,
            HashSet<DateTime> contractDates,
            Dictionary<string, List<Room>> baseRoomsIndex,
            Dictionary<string, RoomGroup> baseRoomGroups,
            Dictionary<string, Room> baseRooms,
            List<string> contractMarkets
        )
        {
            foreach (var line in inventoryTable.Rows)
            {
                var roomTypeCode = line["Room type"];
                var roomCharacteristic = line["Characteristic"];
                var rate = line["Rate"];
                var roomRate = fixRate ? rate : HotelBedsSettings.GenericRateCode;
                var allotments = line[_allotmentFieldName];
                var startDate = Functions.ParseDate(line["Initial date"]);
                var finalDate = Functions.ParseDate(line["Final date"]);
                var elements = Functions.SplitGroupedValues(allotments);
                var inventoryDates = Functions.DailyValues(startDate, finalDate, elements, contractDates);

                foreach (var market in contractMarkets)
                {
                    var roomGroupId = Functions.CreateRoomGroupId(contractRoomId, roomTypeCode, roomCharacteristic, roomRate, contractRoomIdType);
                    var roomList = Functions.RoomList(baseRooms, baseRoomGroups, roomGroupId, baseRoomsIndex);

                    foreach (var date in inventoryDates.Keys)
                    {
                        var fields = inventoryDates[date].Split(',');
                        var releaseHB = short.Parse(fields[0]);
                        var allotmentHB = short.Parse(fields[1]);
                        var release = releaseHB >= 0 ? releaseHB : 0;
                        var allotment = allotmentHB >= 0 ? allotmentHB : 0;

                        //Apply settings
                        if (Settings.Base.ReleaseDaysAdded.HasValue)
                        {
                            release += (short)Settings.Base.ReleaseDaysAdded.Value;
                        }
                        if (Settings.Base.MaxUnitsImported.HasValue && allotment > Settings.Base.MaxUnitsImported.Value)
                        {
                            allotment = (short)Settings.Base.MaxUnitsImported.Value;
                        }

                        foreach (var room in roomList)
                        {
                            if (!room.AvailabilityPeriods.ContainsKey(date))
                            {
                                var availabilityPeriod = new AvailabilityPeriod();
                                availabilityPeriod.MinimumStay = 1;
                                availabilityPeriod.DateFrom = date;
                                availabilityPeriod.DateTo = date;
                                availabilityPeriod.ReleaseDays = release;
                                availabilityPeriod.AvailabilityAvailableUnits = allotment;
                                availabilityPeriod.IsArrivalPossible = true;
                                availabilityPeriod.IsWeeklyStay = false;

                                room.AvailabilityPeriods.Add(date, availabilityPeriod);
                            }
                        }
                    }
                }
            }
        }

        #endregion

        #region Fill prices

        public void TablePrices(
            ConcurrentDictionary<string, ConcurrentDictionary<string, ConcurrentBag<string>>> logList,
            string hotelIdName,
            Dictionary<string, List<string>> distinctCombinations,
            Dictionary<string, Dictionary<string, string>> splittedCombinations,
            SortedList<int, Dictionary<string, string>> sortedRates,
            IHotelBedsDataTable roomTypesTable,
            List<string> boardsList,
            bool fixRate,
            string contractRoomId,
            string contractRoomIdType,
            string contractBoard,
            HashSet<DateTime> contractDates,
            Dictionary<string, List<Room>> baseRoomsIndex,
            Dictionary<string, RoomType> baseRoomTypes,
            Dictionary<string, RoomGroup> baseRoomGroups,
            Dictionary<string, Room> baseRooms,
            Dictionary<string, Dictionary<DateTime, decimal?[]>> baseRoomsPrice,
            Dictionary<string, Dictionary<DateTime, bool>> baseRoomsPriceIsPerPax,
            Dictionary<string, Dictionary<DateTime, string>> baseRoomsPriceRate,
            Dictionary<string, Dictionary<DateTime, string[]>> pricesLine,
            List<string> contractMarkets
        )
        {
            foreach (var lineComb in pricesLine.Keys)
            {
                var fields = lineComb.Split(HotelBedsSettings.RoomCharSeparator);
                var marketCode = fields[5];
                var roomTypeCode = fields[2];
                var roomCharacteristic = fields[3];
                var roomRate = fields[4];

                var roomGroupId = Functions.CreateRoomGroupId(contractRoomId, roomTypeCode, roomCharacteristic, roomRate, contractRoomIdType);
                var roomList = Functions.RoomList(baseRooms, baseRoomGroups, roomGroupId, baseRoomsIndex);

                foreach (var date in pricesLine[lineComb].Keys)
                {
                    var priceFields = pricesLine[lineComb][date];
                    var isPricePerPax = priceFields[0].Equals("Y");
                    /*var netPrice = priceFields[1];
                    var mandatoryPrice = priceFields[2];*/
                    var priceRate = (!fixRate && !String.IsNullOrEmpty(priceFields[3]) ? priceFields[3] : roomRate);
                    var boardPriceType = (!String.IsNullOrEmpty(priceFields[4]) ? priceFields[4] : contractBoard);
                    var amount = Decimal.Parse(priceFields[5], NumberFormatInfo.InvariantInfo);

                    foreach (var room in roomList)
                    {
                        if (!room.AvailabilityPeriods.ContainsKey(date))
                        {
                            Functions.LogLine(logList, hotelIdName, "availability not created", date.ToString());
                            continue;
                        }

                        var adults = baseRoomTypes[room.TypeId].Beds;
                        var childs = baseRoomTypes[room.TypeId].ExtraBeds;

                        decimal baseAmount;
                        decimal? baseAmountChilds = null;

                        if (isPricePerPax && adults > 0)
                        {
                            baseAmount = amount * adults;
                        }
                        else
                        {
                            baseAmount = amount;
                        }
                        if (childs > 0)
                        {
                            baseAmountChilds = isPricePerPax ? amount : Decimal.Zero;
                        }

                        //Creating base price
                        if (!baseRoomsPrice.ContainsKey(room.RoomId))
                        {
                            baseRoomsPrice.Add(room.RoomId, new Dictionary<DateTime, decimal?[]>());
                            baseRoomsPriceIsPerPax.Add(room.RoomId, new Dictionary<DateTime, bool>());
                            baseRoomsPriceRate.Add(room.RoomId, new Dictionary<DateTime, string>());
                        }
                        if (!baseRoomsPrice[room.RoomId].ContainsKey(date))
                        {
                            baseRoomsPrice[room.RoomId][date] = new [] { baseAmount, baseAmountChilds };
                            baseRoomsPriceIsPerPax[room.RoomId][date] = isPricePerPax;
                            baseRoomsPriceRate[room.RoomId][date] = priceRate;
                        }

                        var availabilityPeriod = room.AvailabilityPeriods[date];

                        Functions.PriceBoard(availabilityPeriod, boardPriceType, baseAmount, baseAmountChilds, isPricePerPax, adults);
                    }
                }
            }
        }

        #endregion

        #region Board supplements and discounts

        public void TableBoardSupplements(
            IHotelBedsDataTable boardsTable,
            ConcurrentDictionary<string, ConcurrentDictionary<string, ConcurrentBag<string>>> logList,
            string hotelIdName,
            Dictionary<string, List<string>> distinctCombinations,
            Dictionary<string, Dictionary<string, string>> splittedCombinations,
            SortedList<int, Dictionary<string, string>> sortedRates,
            IHotelBedsDataTable roomTypesTable,
            List<string> boardsList,
            bool fixRate,
            string contractRoomId,
            string contractRoomIdType,
            string contractBoard,
            DateTime cacheIni,
            DateTime cacheEnd,
            HashSet<DateTime> contractDates,
            Dictionary<string, List<Room>> baseRoomsIndex,
            Dictionary<string, RoomType> baseRoomTypes,
            Dictionary<string, RoomGroup> baseRoomGroups,
            Dictionary<string, Room> baseRooms,
            Dictionary<string, int> baseRoomsCapacity,
            Dictionary<string, Dictionary<DateTime, decimal?[]>> baseRoomsPrice,
            Dictionary<string, Dictionary<DateTime, bool>> baseRoomsPriceIsPerPax,
            Dictionary<string, Dictionary<DateTime, string>> baseRoomsPriceRate,
            Dictionary<string, Dictionary<DateTime, Dictionary<string, Dictionary<string, decimal?[]>>>> baseRoomsPriceBoard,
            Dictionary<string, Dictionary<DateTime, Dictionary<string, Dictionary<string, bool>>>> baseRoomsPriceBoardIsPerPax,
            List<string> contractMarkets
        )
        {
            var sortedBoardTable = Functions.OrderDataTable(boardsTable, "Room type", "Characteristic", "Rate", "Board code", "Market price");
            sortedBoardTable = Functions.GroupDataTablePerAge(sortedBoardTable);
            var genericTableCombinations = new Dictionary<DateTime, Dictionary<string, bool>>();
            var extraCombinations = new List<string> { "Minimum age", "Maximum age" };

            foreach (var line in sortedBoardTable)
            {
                var txtRoomTypeCode = line["Room type"];
                var txtRoomCharacteristic = line["Characteristic"];
                var txtRate = line["Rate"];
                var txtBoardCode = line["Board code"];
                var startDate = Functions.ParseDate(line["Initial date"]);
                var finalDate = Functions.ParseDate(line["Final date"]);
                var boardDates = Functions.ListValidDates(startDate, finalDate, cacheIni, cacheEnd, contractDates);
                var isPerPax = line["Is per pax"].Equals("Y");
                var txtAmount = line["Amount"];
                var txtPercentage = line["Percentage"];
                var minimumAge = line["Minimum age"];
                var maximumAge = line["Maximum age"];
                var onMonday = line["On Monday"].Equals("Y");
                var onTuesday = line["On Tuesday"].Equals("Y");
                var onWednesday = line["On Wednesday"].Equals("Y");
                var onThursday = line["On Thursday"].Equals("Y");
                var onFriday = line["On Friday"].Equals("Y");
                var onSaturday = line["On Saturday"].Equals("Y");
                var onSunday = line["On Sunday"].Equals("Y");
                var netPrice = line["Net price"];
                var price = line["Price"];
                var txtMarket = line["Market price"];

                if (!HotelBedsSettings.MapMeals.ContainsKey(txtBoardCode) || (String.IsNullOrEmpty(txtAmount) && String.IsNullOrEmpty(txtPercentage)))
                {
                    Functions.LogLine(logList, hotelIdName, "board not supported", txtBoardCode);
                    continue;
                }

                if (!String.IsNullOrEmpty(txtMarket) && !contractMarkets.Contains(txtMarket))
                {
                    Functions.LogLine(logList, hotelIdName, "board supplement market ignored", txtMarket);
                    continue;
                }

                bool adultPrice;
                bool childPrice;
                Functions.PaxApplyPrice(minimumAge, maximumAge, out adultPrice, out childPrice);

                var rowCombinations = Functions.RowCombinations(distinctCombinations, txtRoomTypeCode, txtRoomCharacteristic, txtRate, txtBoardCode, txtMarket, sortedRates, roomTypesTable, boardsList, contractMarkets);

                rowCombinations = Functions.AddCombinations(rowCombinations, line, extraCombinations);

                foreach (var date in boardDates)
                {
                    if (date.DayOfWeek.Equals(DayOfWeek.Monday) && !onMonday) continue;
                    if (date.DayOfWeek.Equals(DayOfWeek.Tuesday) && !onTuesday) continue;
                    if (date.DayOfWeek.Equals(DayOfWeek.Wednesday) && !onWednesday) continue;
                    if (date.DayOfWeek.Equals(DayOfWeek.Thursday) && !onThursday) continue;
                    if (date.DayOfWeek.Equals(DayOfWeek.Friday) && !onFriday) continue;
                    if (date.DayOfWeek.Equals(DayOfWeek.Saturday) && !onSaturday) continue;
                    if (date.DayOfWeek.Equals(DayOfWeek.Sunday) && !onSunday) continue;

                    var combNotProcessed = Functions.FilterCombinations(genericTableCombinations, rowCombinations, date);
                    foreach (var cnp in combNotProcessed)
                    {
                        var fields = Functions.SplitCombination(splittedCombinations, cnp, extraCombinations);
                        var marketCode = fields["Market"];
                        var roomTypeCode = fields["Room type"];
                        var roomCharacteristic = fields["Characteristic"];
                        var rate = fields["Rate"];
                        var boardCode = fields["Board"];
                        var roomRate = fixRate ? rate : HotelBedsSettings.GenericRateCode;

                        var roomGroupId = Functions.CreateRoomGroupId(contractRoomId, roomTypeCode, roomCharacteristic, roomRate, contractRoomIdType);
                        var roomList = Functions.RoomList(baseRooms, baseRoomGroups, roomGroupId, baseRoomsIndex);

                        foreach (var room in roomList)
                        {
                            if (
                                !baseRoomsPrice.ContainsKey(room.RoomId) || !baseRoomsPrice[room.RoomId].ContainsKey(date)
                                || (!String.IsNullOrEmpty(rate) && !baseRoomsPriceRate[room.RoomId][date].Equals(rate))
                            )
                            {
                                continue;
                            }

                            if (!room.AvailabilityPeriods.ContainsKey(date))
                            {
                                Functions.LogLine(logList, hotelIdName, "availability not created", date.ToString());
                                continue;
                            }
                            var availabilityPeriod = room.AvailabilityPeriods[date];
                            if (availabilityPeriod.AvailabilityAvailableUnits == 0)
                            {
                                continue;
                            }

                            var stdCapacity = baseRoomsCapacity[room.RoomId];
                            var adults = baseRoomTypes[room.TypeId].Beds;
                            var childs = baseRoomTypes[room.TypeId].ExtraBeds;
                            var baseIsPerPax = baseRoomsPriceIsPerPax[room.RoomId][date];

                            if (!adultPrice && childPrice && childs == 0)
                            {
                                Functions.LogLine(logList, hotelIdName, "board supplement age restriction ignored", "!adultPrice && childPrice && childs == 0");
                                continue;
                            }

                            //Can't apply senior prices (we only have 1 type of "adult") and also infants (they are free in our system)
                            if (!adultPrice && !childPrice)
                            {
                                //Only log if price > 0
                                if (
                                    (!String.IsNullOrEmpty(txtAmount) && !txtAmount.Equals("0.000") && !txtAmount.Equals("0.0") && !txtAmount.Equals("0.00"))
                                    || (!String.IsNullOrEmpty(txtPercentage) && !txtPercentage.Equals("-100.000") && !txtPercentage.Equals("-100.0") && !txtPercentage.Equals("-100.00"))
                                )
                                {
                                    Functions.LogLine(logList, hotelIdName, "board supplement age restriction", "txtAmount:" + txtAmount + ", txtPercentage:" + txtPercentage);
                                }

                                availabilityPeriod.AvailabilityAvailableUnits = 0;
                                continue;
                            }

                            int countAdult;
                            if ((baseIsPerPax || isPerPax) && contractBoard.Equals(boardCode) && stdCapacity >= adults)
                            {
                                //HB Doc: If exists a board supplement or discount for the base board, these kind of record applies only to the passengers that exceeds
                                //the standard capacity of the room.
                                continue;
                            }
                            if ((baseIsPerPax || isPerPax) && contractBoard.Equals(boardCode))
                            {
                                countAdult = adults - stdCapacity;
                            }
                            else
                            {
                                countAdult = adults;
                            }

                            var basePrice = baseRoomsPrice[room.RoomId][date][0].Value;
                            var basePriceChilds = baseRoomsPrice[room.RoomId][date][1];

                            decimal priceTotal;
                            decimal? priceChild = null;
                            if (!String.IsNullOrEmpty(txtAmount))
                            {
                                var amount = Decimal.Parse(txtAmount, NumberFormatInfo.InvariantInfo);
                                if ((!baseIsPerPax && !isPerPax) || (baseIsPerPax && !isPerPax))
                                {
                                    priceTotal = basePrice + amount;
                                    if (basePriceChilds.HasValue) { priceChild = basePriceChilds.Value; }
                                }
                                else
                                {
                                    priceTotal = basePrice + (amount * countAdult);
                                    if (basePriceChilds.HasValue) { priceChild = basePriceChilds.Value + amount; }
                                }
                            }
                            else
                            {
                                decimal amountPercent;
                                var percentage = Decimal.Parse(txtPercentage, NumberFormatInfo.InvariantInfo);
                                if (!baseIsPerPax && !isPerPax)
                                {
                                    amountPercent = Decimal.Divide(basePrice * percentage, 100);
                                    priceTotal = basePrice + amountPercent;
                                    if (basePriceChilds.HasValue) { priceChild = basePriceChilds.Value; }
                                }
                                else if (!baseIsPerPax && isPerPax)
                                {
                                    amountPercent = Decimal.Divide(Decimal.Divide(basePrice, stdCapacity) * percentage, 100);
                                    priceTotal = basePrice + (amountPercent * countAdult);
                                    if (basePriceChilds.HasValue) { priceChild = basePriceChilds.Value + amountPercent; }
                                }
                                else if (baseIsPerPax && !isPerPax)
                                {
                                    amountPercent = Decimal.Divide((basePrice * stdCapacity) * percentage, 100);
                                    priceTotal = basePrice + amountPercent;
                                    if (basePriceChilds.HasValue) { priceChild = basePriceChilds.Value; }
                                }
                                else
                                {
                                    amountPercent = Decimal.Divide(basePrice * percentage, 100);
                                    priceTotal = basePrice + (amountPercent * countAdult);
                                    if (basePriceChilds.HasValue) { priceChild = basePriceChilds.Value + amountPercent; }
                                }
                            }
                            if (priceTotal < 0)
                            {
                                Functions.LogLine(logList, hotelIdName, "board price is less than zero", priceTotal.ToString());
                                continue;
                            }
                            if (priceChild.HasValue && priceChild.Value < 0)
                            {
                                priceChild = 0;
                            }

                            Functions.PriceBaseRoomsBoard(room, date, rate, boardCode, priceTotal, priceChild, isPerPax, baseRoomsPriceBoard, baseRoomsPriceBoardIsPerPax);

                            Functions.PriceBoard(availabilityPeriod, boardCode, priceTotal, priceChild, isPerPax, adults);
                        }

                        //Mark combination processed
                        genericTableCombinations[date][cnp] = true;
                    }
                }
            }
        }

        #endregion

        #region Stop sales

        public void TableStopSales(
            IHotelBedsDataTable stopSalesTable,
            ConcurrentDictionary<string, ConcurrentDictionary<string, ConcurrentBag<string>>> logList,
            string hotelIdName,
            Dictionary<string, List<string>> distinctCombinations,
            Dictionary<string, Dictionary<string, string>> splittedCombinations,
            SortedList<int, Dictionary<string, string>> sortedRates,
            IHotelBedsDataTable roomTypesTable,
            List<string> boardsList,
            bool fixRate,
            string contractRoomId,
            string contractRoomIdType,
            DateTime cacheIni,
            DateTime cacheEnd,
            HashSet<DateTime> contractDates,
            Dictionary<string, List<Room>> baseRoomsIndex,
            Dictionary<string, RoomGroup> baseRoomGroups,
            Dictionary<string, Room> baseRooms,
            List<string> contractMarkets
        )
        {
            var sortedStopSalesTable = Functions.OrderDataTable(stopSalesTable, "Room type", "Characteristic", "Rate", "Board", String.Empty);
            var genericTableCombinations = new Dictionary<DateTime, Dictionary<string, bool>>();

            foreach (var line in sortedStopSalesTable)
            {
                var txtRoomTypeCode = line["Room type"];
                var txtRoomCharacteristic = line["Characteristic"];
                var txtRate = line["Rate"];
                var txtBoardCode = line["Board"];
                var startDate = Functions.ParseDate(line["Initial date"]);
                var finalDate = Functions.ParseDate(line["Final date"]);
                var stopSalesDates = Functions.ListValidDates(startDate, finalDate, cacheIni, cacheEnd, contractDates);

                var rowCombinations = Functions.RowCombinations(distinctCombinations, txtRoomTypeCode, txtRoomCharacteristic, txtRate, txtBoardCode, String.Empty, sortedRates, roomTypesTable, boardsList, contractMarkets);

                foreach (var date in stopSalesDates)
                {
                    var combNotProcessed = Functions.FilterCombinations(genericTableCombinations, rowCombinations, date);
                    foreach (var cnp in combNotProcessed)
                    {
                        var fields = Functions.SplitCombination(splittedCombinations, cnp, null);
                        var marketCode = fields["Market"];
                        var roomTypeCode = fields["Room type"];
                        var roomCharacteristic = fields["Characteristic"];
                        var rate = fields["Rate"];
                        var boardCode = fields["Board"];
                        var roomRate = fixRate ? rate : HotelBedsSettings.GenericRateCode;

                        var roomGroupId = Functions.CreateRoomGroupId(contractRoomId, roomTypeCode, roomCharacteristic, roomRate, contractRoomIdType);
                        var roomList = Functions.RoomList(baseRooms, baseRoomGroups, roomGroupId, baseRoomsIndex);

                        foreach (var room in roomList)
                        {
                            if (!room.AvailabilityPeriods.ContainsKey(date))
                            {
                                Functions.LogLine(logList, hotelIdName, "availability not created", date.ToString());
                                continue;
                            }
                            var availabilityPeriod = room.AvailabilityPeriods[date];
                            if (availabilityPeriod.AvailabilityAvailableUnits == 0)
                            {
                                continue;
                            }

                            if (!String.IsNullOrEmpty(boardCode) && !HotelBedsSettings.MapMeals.ContainsKey(boardCode))
                            {
                                Functions.LogLine(logList, hotelIdName, "stop sales not supported", boardCode);
                                continue;
                            }

                            if (!String.IsNullOrEmpty(boardCode))
                            {
                                if (!String.IsNullOrEmpty(availabilityPeriod.Pricing_BookableBoardId) && availabilityPeriod.Pricing_BookableBoardId.Equals(boardCode))
                                {
                                    availabilityPeriod.AvailabilityAvailableUnits = 0;
                                }
                                else
                                {
                                    foreach (var additionalBoard in availabilityPeriod.AdditionalBoards)
                                    {
                                        if (!String.IsNullOrEmpty(additionalBoard.BookableBoardId) && additionalBoard.BookableBoardId.Equals(boardCode))
                                        {
                                            availabilityPeriod.AvailabilityAvailableUnits = 0;
                                            break;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                availabilityPeriod.AvailabilityAvailableUnits = 0;
                            }
                        }

                        //Mark combination processed
                        genericTableCombinations[date][cnp] = true;
                    }
                }
            }
        }

        #endregion

        #region Minimum and maximum stay

        public void TableMinimumMaximumStay(
            IHotelBedsDataTable minimumMaximumStayTable,
            ConcurrentDictionary<string, ConcurrentDictionary<string, ConcurrentBag<string>>> logList,
            string hotelIdName,
            Dictionary<string, List<string>> distinctCombinations,
            Dictionary<string, Dictionary<string, string>> splittedCombinations,
            SortedList<int, Dictionary<string, string>> sortedRates,
            IHotelBedsDataTable roomTypesTable,
            List<string> boardsList,
            bool fixRate,
            string contractRoomId,
            string contractRoomIdType,
            DateTime cacheIni,
            DateTime cacheEnd,
            HashSet<DateTime> contractDates,
            Dictionary<string, List<Room>> baseRoomsIndex,
            Dictionary<string, RoomGroup> baseRoomGroups,
            Dictionary<string, Room> baseRooms,
            List<string> contractMarkets
        )
        {
            var sortedMinimumMaximumStayTable = Functions.OrderDataTable(minimumMaximumStayTable, "Room type", "Characteristic", "Rate", "Board", String.Empty);
            var genericTableCombinations = new Dictionary<DateTime, Dictionary<string, bool>>();

            foreach (var line in sortedMinimumMaximumStayTable)
            {
                var applicationDate = !String.IsNullOrEmpty(line["Application date"]) ? Functions.ParseDate(line["Application date"]) : cacheIni;
                if (applicationDate > TodayDate)
                {
                    Functions.LogLine(logList, hotelIdName, "min max not applied filtered by application dates", applicationDate.ToString());
                    continue;
                }
                var txtRoomTypeCode = line["Room type"];
                var txtRoomCharacteristic = line["Characteristic"];
                var txtRate = line["Rate"];
                var txtBoardCode = line["Board"];
                var startDate = Functions.ParseDate(line["Initial date"]);
                var finalDate = Functions.ParseDate(line["Final date"]);
                var minMaxDates = Functions.ListValidDates(startDate, finalDate, cacheIni, cacheEnd, contractDates);
                var type = line["Type"];
                var minimumDays = short.Parse(line["Minimum days"], NumberFormatInfo.InvariantInfo);
                var maximumDays = !String.IsNullOrEmpty(line["Maximum days"]) ? short.Parse(line["Maximum days"], NumberFormatInfo.InvariantInfo) : 99;
                var onMonday = line["On Monday"].Equals("Y");
                var onTuesday = line["On Tuesday"].Equals("Y");
                var onWednesday = line["On Wednesday"].Equals("Y");
                var onThursday = line["On Thursday"].Equals("Y");
                var onFriday = line["On Friday"].Equals("Y");
                var onSaturday = line["On Saturday"].Equals("Y");
                var onSunday = line["On Sunday"].Equals("Y");

                var rowCombinations = Functions.RowCombinations(distinctCombinations, txtRoomTypeCode, txtRoomCharacteristic, txtRate, txtBoardCode, String.Empty, sortedRates, roomTypesTable, boardsList, contractMarkets);

                foreach (var date in minMaxDates)
                {
                    if (date.DayOfWeek.Equals(DayOfWeek.Monday) && !onMonday) continue;
                    if (date.DayOfWeek.Equals(DayOfWeek.Tuesday) && !onTuesday) continue;
                    if (date.DayOfWeek.Equals(DayOfWeek.Wednesday) && !onWednesday) continue;
                    if (date.DayOfWeek.Equals(DayOfWeek.Thursday) && !onThursday) continue;
                    if (date.DayOfWeek.Equals(DayOfWeek.Friday) && !onFriday) continue;
                    if (date.DayOfWeek.Equals(DayOfWeek.Saturday) && !onSaturday) continue;
                    if (date.DayOfWeek.Equals(DayOfWeek.Sunday) && !onSunday) continue;

                    var combNotProcessed = Functions.FilterCombinations(genericTableCombinations, rowCombinations, date);
                    foreach (var cnp in combNotProcessed)
                    {
                        var fields = Functions.SplitCombination(splittedCombinations, cnp, null);
                        var marketCode = fields["Market"];
                        var roomTypeCode = fields["Room type"];
                        var roomCharacteristic = fields["Characteristic"];
                        var rate = fields["Rate"];
                        var boardCode = fields["Board"];
                        var roomRate = fixRate ? rate : HotelBedsSettings.GenericRateCode;

                        var roomGroupId = Functions.CreateRoomGroupId(contractRoomId, roomTypeCode, roomCharacteristic, roomRate, contractRoomIdType);
                        var roomList = Functions.RoomList(baseRooms, baseRoomGroups, roomGroupId, baseRoomsIndex);

                        foreach (var room in roomList)
                        {
                            if (!room.AvailabilityPeriods.ContainsKey(date))
                            {
                                Functions.LogLine(logList, hotelIdName, "availability not created", date.ToString());
                                continue;
                            }
                            var availabilityPeriod = room.AvailabilityPeriods[date];
                            if (availabilityPeriod.AvailabilityAvailableUnits == 0)
                            {
                                continue;
                            }

                            if (!String.IsNullOrEmpty(boardCode) && !HotelBedsSettings.MapMeals.ContainsKey(boardCode))
                            {
                                Functions.LogLine(logList, hotelIdName, "min max not supported board code restriction", boardCode);
                                continue;
                            }

                            if (Settings.MaximumStayDaysFilter.HasValue && maximumDays < Settings.MaximumStayDaysFilter.Value)
                            {
                                Functions.LogLine(logList, hotelIdName, "min max not supported maximum days restriction", maximumDays.ToString());
                                availabilityPeriod.AvailabilityAvailableUnits = 0;
                                continue;
                            }

                            if (minimumDays > availabilityPeriod.MinimumStay)
                            {
                                if (!String.IsNullOrEmpty(boardCode))
                                {
                                    if (
                                        !String.IsNullOrEmpty(availabilityPeriod.Pricing_BasePrice)
                                        && availabilityPeriod.Pricing_BookableBoardId.Equals(boardCode)
                                    )
                                    {
                                        availabilityPeriod.MinimumStay = minimumDays;
                                    }
                                    else
                                    {
                                        foreach (var additionalBoard in availabilityPeriod.AdditionalBoards)
                                        {
                                            if (additionalBoard.BookableBoardId.Equals(boardCode))
                                            {
                                                availabilityPeriod.MinimumStay = minimumDays;
                                                break;
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    availabilityPeriod.MinimumStay = minimumDays;
                                }
                            }
                        }

                        //Mark combination processed
                        genericTableCombinations[date][cnp] = true;
                    }
                }
            }
        }

        #endregion

        #region Check in and check out

        public void TableCheckInCheckOut(
            IHotelBedsDataTable checkInCheckOutTable,
            ConcurrentDictionary<string, ConcurrentDictionary<string, ConcurrentBag<string>>> logList,
            string hotelIdName,
            Dictionary<string, List<string>> distinctCombinations,
            Dictionary<string, Dictionary<string, string>> splittedCombinations,
            SortedList<int, Dictionary<string, string>> sortedRates,
            IHotelBedsDataTable roomTypesTable,
            List<string> boardsList,
            bool fixRate,
            string contractRoomId,
            string contractRoomIdType,
            DateTime cacheIni,
            DateTime cacheEnd,
            HashSet<DateTime> contractDates,
            Dictionary<string, List<Room>> baseRoomsIndex,
            Dictionary<string, RoomGroup> baseRoomGroups,
            Dictionary<string, Room> baseRooms,
            List<string> contractMarkets
        )
        {
            var sortedCheckInCheckOutTable = Functions.OrderDataTable(checkInCheckOutTable, "Room type", "Characteristic", "Rate", "Board", String.Empty);
            var genericTableCombinations = new Dictionary<DateTime, Dictionary<string, bool>>();

            foreach (var line in sortedCheckInCheckOutTable)
            {
                var applicationDate = !String.IsNullOrEmpty(line["Application date"]) ? Functions.ParseDate(line["Application date"]) : cacheIni;
                if (applicationDate > TodayDate)
                {
                    Functions.LogLine(logList, hotelIdName, "check in out not applied filtered by application dates", applicationDate.ToString());
                    continue;
                }
                var txtRoomTypeCode = line["Room type"];
                var txtRoomCharacteristic = line["Characteristic"];
                var txtRate = line["Rate"];
                var txtBoardCode = line["Board"];
                var startDate = Functions.ParseDate(line["Initial date"]);
                var finalDate = Functions.ParseDate(line["Final date"]);
                var inOutDates = Functions.ListValidDates(startDate, finalDate, cacheIni, cacheEnd, contractDates);
                var type = line["Type"];
                var onMonday = line["On Monday"].Equals("Y");
                var onTuesday = line["On Tuesday"].Equals("Y");
                var onWednesday = line["On Wednesday"].Equals("Y");
                var onThursday = line["On Thursday"].Equals("Y");
                var onFriday = line["On Friday"].Equals("Y");
                var onSaturday = line["On Saturday"].Equals("Y");
                var onSunday = line["On Sunday"].Equals("Y");

                var rowCombinations = Functions.RowCombinations(distinctCombinations, txtRoomTypeCode, txtRoomCharacteristic, txtRate, txtBoardCode, String.Empty, sortedRates, roomTypesTable, boardsList, contractMarkets);

                foreach (var date in inOutDates)
                {
                    var combNotProcessed = Functions.FilterCombinations(genericTableCombinations, rowCombinations, date);
                    foreach (var cnp in combNotProcessed)
                    {
                        var fields = Functions.SplitCombination(splittedCombinations, cnp, null);
                        var marketCode = fields["Market"];
                        var roomTypeCode = fields["Room type"];
                        var roomCharacteristic = fields["Characteristic"];
                        var rate = fields["Rate"];
                        var boardCode = fields["Board"];
                        var roomRate = fixRate ? rate : HotelBedsSettings.GenericRateCode;

                        var roomGroupId = Functions.CreateRoomGroupId(contractRoomId, roomTypeCode, roomCharacteristic, roomRate, contractRoomIdType);
                        var roomList = Functions.RoomList(baseRooms, baseRoomGroups, roomGroupId, baseRoomsIndex);

                        foreach (var room in roomList)
                        {
                            if (!room.AvailabilityPeriods.ContainsKey(date))
                            {
                                Functions.LogLine(logList, hotelIdName, "availability not created", date.ToString());
                                continue;
                            }
                            var availabilityPeriod = room.AvailabilityPeriods[date];
                            if (availabilityPeriod.AvailabilityAvailableUnits == 0)
                            {
                                continue;
                            }

                            if (!String.IsNullOrEmpty(boardCode) && !HotelBedsSettings.MapMeals.ContainsKey(boardCode))
                            {
                                Functions.LogLine(logList, hotelIdName, "check in out not supported board code restriction", boardCode);
                                continue;
                            }

                            if (!type.Equals("I"))
                            {
                                Functions.LogLine(logList, hotelIdName, "check in out not supported type restriction", type);
                                availabilityPeriod.AvailabilityAvailableUnits = 0;
                                continue;
                            }

                            var boardFound = false;
                            if (!String.IsNullOrEmpty(boardCode))
                            {
                                if (
                                    !String.IsNullOrEmpty(availabilityPeriod.Pricing_BasePrice)
                                    && availabilityPeriod.Pricing_BookableBoardId.Equals(boardCode)
                                )
                                {
                                    boardFound = true;
                                }
                                else
                                {
                                    foreach (var additionalBoard in availabilityPeriod.AdditionalBoards)
                                    {
                                        if (additionalBoard.BookableBoardId.Equals(boardCode))
                                        {
                                            boardFound = true;
                                            break;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                boardFound = true;
                            }

                            if (boardFound)
                            {
                                switch (date.DayOfWeek)
                                {
                                    case DayOfWeek.Monday: availabilityPeriod.IsArrivalPossible = onMonday; break;
                                    case DayOfWeek.Tuesday: availabilityPeriod.IsArrivalPossible = onTuesday; break;
                                    case DayOfWeek.Wednesday: availabilityPeriod.IsArrivalPossible = onWednesday; break;
                                    case DayOfWeek.Thursday: availabilityPeriod.IsArrivalPossible = onThursday; break;
                                    case DayOfWeek.Friday: availabilityPeriod.IsArrivalPossible = onFriday; break;
                                    case DayOfWeek.Saturday: availabilityPeriod.IsArrivalPossible = onSaturday; break;
                                    case DayOfWeek.Sunday: availabilityPeriod.IsArrivalPossible = onSunday; break;
                                }
                            }
                        }

                        //Mark combination processed
                        genericTableCombinations[date][cnp] = true;
                    }
                }
            }
        }

        #endregion

        #region Combinable offers

        public void TableCombinableOffers(
            IHotelBedsDataTable combinableOffersTable,
            List<object>[] combOffers,
            IHotelBedsDataTable supplementsTable,
            IHotelBedsDataTable freesTable
        )
        {
            foreach (var line in combinableOffersTable.Rows)
            {
                var suppCode = false;
                var freeCode = false;
                foreach (var suppLine in supplementsTable.Rows)
                {
                    if (suppLine["Supplement or discount code"] == line["Code1"] || suppLine["Supplement or discount code"] == line["Code2"])
                    {
                        suppCode = true;
                        break;
                    }
                }
                foreach (var freeLine in freesTable.Rows)
                {
                    if (freeLine["Free code"] == line["Code1"] || freeLine["Free code"] == line["Code2"])
                    {
                        freeCode = true;
                        break;
                    }
                }
                combOffers[0].Add(line["Code1"]);
                combOffers[1].Add(line["Code2"]);
                combOffers[2].Add(line["Is excluded"].Equals("Y"));
                combOffers[3].Add(suppCode && freeCode);
                //Mixed combination, according to their documentation "A free has priority over a supplement/discount when combinable offers"
            }
        }

        #endregion

        #region General supplements and discounts

        public void TableSupplementsAndFrees(
            IHotelBedsDataTable supplementsTable,
            ConcurrentDictionary<string, ConcurrentDictionary<string, ConcurrentBag<string>>> logList,
            string hotelIdName,
            Dictionary<string, List<string>> distinctCombinations,
            Dictionary<string, Dictionary<string, string>> splittedCombinations,
            SortedList<int, Dictionary<string, string>> sortedRates,
            IHotelBedsDataTable roomTypesTable,
            List<string> boardsList,
            bool fixRate,
            string contractRoomId,
            string contractRoomIdType,
            string opaque,
            DateTime cacheIni,
            DateTime cacheEnd,
            HashSet<DateTime> contractDates,
            Dictionary<string, List<Room>> baseRoomsIndex,
            Dictionary<string, RoomType> baseRoomTypes,
            Dictionary<string, RoomGroup> baseRoomGroups,
            Dictionary<string, Room> baseRooms,
            Dictionary<string, int> baseRoomsCapacity,
            Dictionary<string, Dictionary<DateTime, decimal?[]>> baseRoomsPrice,
            Dictionary<string, Dictionary<DateTime, bool>> baseRoomsPriceIsPerPax,
            Dictionary<string, Dictionary<DateTime, string>> baseRoomsPriceRate,
            Dictionary<string, Dictionary<DateTime, Dictionary<string, Dictionary<string, decimal?[]>>>> baseRoomsPriceBoard,
            Dictionary<string, Dictionary<DateTime, Dictionary<string, Dictionary<string, bool>>>> baseRoomsPriceBoardIsPerPax,
            List<object>[] combOffers,
            List<string> contractMarkets,
            IHotelBedsDataTable freesTable
        )
        {
            var sortedSupplementsTable = Functions.OrderDataTable(supplementsTable, "Room type", "Characteristic", "Rate", "Board", "Market price");
            sortedSupplementsTable = Functions.GroupDataTablePerAge(sortedSupplementsTable);
            
            var supChild = new SortedList<int, Dictionary<string, string>>();
            var supAdditionalBed = new SortedList<int, Dictionary<string, string>>();
            var supSingleUse = new SortedList<int, Dictionary<string, string>>();
            var supGeneral = new SortedList<int, Dictionary<string, string>>();

            var hotelSupplementDates = new Dictionary<object, HashSet<DateTime>>();
            var hotelSupplementDatesApply = new Dictionary<object, Dictionary<DateTime, Dictionary<string, Dictionary<string, string>>>>();
            var hotelFreeDates = new Dictionary<object, HashSet<DateTime>>();
            var hotelFreeDatesApply = new Dictionary<object, Dictionary<DateTime, Dictionary<string, Dictionary<string, string>>>>();
            var hotelGeneralDatesApply = new Dictionary<string, Dictionary<DateTime, Dictionary<string, Dictionary<string, string>>>>();

            foreach (var line in sortedSupplementsTable)
            {
                var supplementType = line["Type"];

                //Infants free in our system
                if (supplementType.Equals("F"))
                {
                    //Don't log infant supplements with 100% discount
                    var txtPercentage = line["Percentage"];
                    if (!txtPercentage.Equals("-100.000") && !txtPercentage.Equals("-100.0") && !txtPercentage.Equals("-100.00"))
                    {
                        Functions.LogLine(logList, hotelIdName, "supplement not supported", txtPercentage);
                    }
                    continue;
                }

                var occupancySupplement = supplementType.Equals("N") || supplementType.Equals("C") || supplementType.Equals("I");

                //'N' Child supplement, 'C' Additional bed supplement, 'I' Single use supplement
                if (occupancySupplement)
                {
                    //Order can be 3 digit long so we start with a default of 4 digit
                    const int paxCount = 1000;
                    var paxOrder = !String.IsNullOrEmpty(line["Pax order"]) ? Int32.Parse(line["Pax order"]) * paxCount : paxCount;
                    SortedList<int, Dictionary<string, string>> supPaxOrder = null;
                    switch (supplementType)
                    {
                        case "N": supPaxOrder = supChild; break;
                        case "C": supPaxOrder = supAdditionalBed; break;
                        case "I": supPaxOrder = supSingleUse; break;
                    }
                    while (supPaxOrder.ContainsKey(paxOrder))
                    {
                        paxOrder++;
                    }
                    switch (supplementType)
                    {
                        case "N": supChild.Add(paxOrder, line); break;
                        case "C": supAdditionalBed.Add(paxOrder, line); break;
                        case "I": supSingleUse.Add(paxOrder, line); break;
                    }
                }
                else
                {
                    //Order can be 8 digit long so we start with a default of 9 digit
                    const int supplementCount = 100000000;
                    var supplementOrder = !String.IsNullOrEmpty(line["Order"]) ? Int32.Parse(line["Order"]) : supplementCount;
                    while (supGeneral.ContainsKey(supplementOrder))
                    {
                        supplementOrder++;
                    }
                    supGeneral.Add(supplementOrder, line);
                }
            }

            //Separate supplements to be processed according HB
            var supplementParams = new HotelBedsSupplementsParams
            {
                SortedSupplements = supChild,
                DistinctCombinations = distinctCombinations,
                SplittedCombinations = splittedCombinations,
                SortedRates = sortedRates,
                RoomTypesTable = roomTypesTable,
                BoardsList = boardsList,
                CacheIni = cacheIni,
                CacheEnd = cacheEnd,
                ContractDates = contractDates,
                BaseRoomsIndex = baseRoomsIndex,
                BaseRoomsCapacity = baseRoomsCapacity,
                BaseRooms = baseRooms,
                BaseRoomTypes = baseRoomTypes,
                BaseRoomGroups = baseRoomGroups,
                BaseRoomsPrice = baseRoomsPrice,
                BaseRoomsPriceIsPerPax = baseRoomsPriceIsPerPax,
                BaseRoomsPriceRate = baseRoomsPriceRate,
                BaseRoomsPriceBoard = baseRoomsPriceBoard,
                BaseRoomsPriceBoardIsPerPax = baseRoomsPriceBoardIsPerPax,
                SupplementChildPaxApplied = new Dictionary<DateTime, Dictionary<string, List<int>>>(),
                SupplementType = "N",
                FixRate = fixRate,
                ContractRoomId = contractRoomId,
                ContractRoomIdType = contractRoomIdType,
                Opaque = opaque,
                LogList = logList,
                HotelIdName = hotelIdName,
                TodayDate = TodayDate,
                ContractMarkets = contractMarkets,
                CombOffers = combOffers,
                HotelSupplementDates = hotelSupplementDates,
                HotelSupplementDatesApply = hotelSupplementDatesApply,
                HotelFreeDatesApply = hotelFreeDatesApply,
                HotelGeneralDatesApply = hotelGeneralDatesApply
            };
            Functions.ProcessSupplement(supplementParams);
            supplementParams.SortedSupplements = supAdditionalBed;
            supplementParams.SupplementType = "C";
            Functions.ProcessSupplement(supplementParams);
            supplementParams.SortedSupplements = supSingleUse;
            supplementParams.SupplementType = "I";
            Functions.ProcessSupplement(supplementParams);

            if (combOffers[0].Count > 0)
            {
                Functions.DailyCombinationSupplement(supGeneral, distinctCombinations, splittedCombinations, sortedRates, roomTypesTable, boardsList, contractMarkets, fixRate, opaque, cacheIni, cacheEnd, contractDates, combOffers, TodayDate, hotelSupplementDates, hotelSupplementDatesApply);
                Functions.DailyCombinationFree(freesTable, distinctCombinations, splittedCombinations, sortedRates, roomTypesTable, boardsList, contractMarkets, fixRate, cacheIni, cacheEnd, contractDates, combOffers, TodayDate, hotelFreeDates, hotelFreeDatesApply);
            }
            if (supGeneral.Count > 0)
            {
                Functions.SupplementDailyGeneral(supGeneral, logList, hotelIdName, distinctCombinations, splittedCombinations, sortedRates, roomTypesTable, boardsList, contractMarkets, fixRate, opaque, cacheIni, cacheEnd, contractDates, TodayDate, hotelSupplementDates, hotelGeneralDatesApply);
            }

            supplementParams.SortedSupplements = supGeneral;
            supplementParams.SupplementType = "";
            Functions.ProcessSupplement(supplementParams);

            //TableFrees(freesTable, logList, indexLog, distinctCombinations, splittedCombinations, sortedRates, roomTypesTable, boardsList, fixRate, contractRoomId, contractRoomIdType, cacheIni, cacheEnd, contractDates, baseRoomsIndex, baseRoomGroups, baseRooms, baseRoomsPrice, baseRoomsPriceRate, combOffers, contractMarkets, hotelFreeDates, hotelFreeDatesApply, hotelSupplementDatesApply);
        }

        #endregion

        #region Frees

        public void TableFrees(
            IHotelBedsDataTable freesTable,
            ConcurrentDictionary<string, ConcurrentDictionary<string, ConcurrentBag<string>>> logList,
            string hotelIdName,
            Dictionary<string, List<string>> distinctCombinations,
            Dictionary<string, Dictionary<string, string>> splittedCombinations,
            SortedList<int, Dictionary<string, string>> sortedRates,
            IHotelBedsDataTable roomTypesTable,
            List<string> boardsList,
            bool fixRate,
            string contractRoomId,
            string contractRoomIdType,
            DateTime cacheIni,
            DateTime cacheEnd,
            HashSet<DateTime> contractDates,
            Dictionary<string, List<Room>> baseRoomsIndex,
            Dictionary<string, RoomGroup> baseRoomGroups,
            Dictionary<string, Room> baseRooms,
            Dictionary<string, Dictionary<DateTime, decimal?[]>> baseRoomsPrice,
            Dictionary<string, Dictionary<DateTime, string>> baseRoomsPriceRate,
            List<object>[] combOffers,
            List<string> contractMarkets,
            Dictionary<object, HashSet<DateTime>> hotelFreeDates,
            Dictionary<object, Dictionary<DateTime, Dictionary<string, Dictionary<string, string>>>> hotelFreeDatesApply,
            Dictionary<object, Dictionary<DateTime, Dictionary<string, Dictionary<string, string>>>> hotelSupplementDatesApply
        )
        {
            var sortedFreesTable = Functions.OrderDataTable(freesTable, "Room type", "Characteristic", "Rate", "Board", String.Empty);
            var genericTableCombinations = new Dictionary<DateTime, Dictionary<string, bool>>();
            var typeSupplementProcessed = new Dictionary<DateTime, Dictionary<string, Dictionary<string, List<string>>>>();

            foreach (var line in sortedFreesTable)
            {
                var applicationStartDate = !String.IsNullOrEmpty(line["Application initial date"]) ? Functions.ParseDate(line["Application initial date"]) : cacheIni;
                var applicationFinalDate = !String.IsNullOrEmpty(line["Application final date"]) ? Functions.ParseDate(line["Application final date"]) : cacheEnd;
                if (String.IsNullOrEmpty(line["Application initial date"]) || applicationStartDate > TodayDate || TodayDate > applicationFinalDate)
                {
                    Functions.LogLine(logList, hotelIdName, "frees not applied filtered by application dates", applicationStartDate + "-" + applicationFinalDate);
                    continue;
                }
                var txtRoomTypeCode = line["Room type"];
                var txtRoomCharacteristic = line["Characteristic"];
                var txtRate = line["Rate"];
                var txtBoardCode = line["Board"];
                var startDate = Functions.ParseDate(line["Initial date"]);
                var finalDate = Functions.ParseDate(line["Final date"]);
                var freeDates = Functions.ListValidDates(startDate, finalDate, cacheIni, cacheEnd, contractDates);
                var minimumDays = Int32.Parse(line["Minimum days"]);
                var maximumDays = Int32.Parse(line["Maximum days"]);
                var freeNights = line["Frees"];
                var freeCode = line["Free code"];
                var discount = line["Discount"];
                var applicationBaseType = line["Application base type"];
                var applicationBoardType = line["Application board type"];
                var applicationDiscountType = line["Application discount type"];
                var applicationStayType = line["Application stay type"];
                var onMonday = line["On Monday"].Equals("Y");
                var onTuesday = line["On Tuesday"].Equals("Y");
                var onWednesday = line["On Wednesday"].Equals("Y");
                var onThursday = line["On Thursday"].Equals("Y");
                var onFriday = line["On Friday"].Equals("Y");
                var onSaturday = line["On Saturday"].Equals("Y");
                var onSunday = line["On Sunday"].Equals("Y");
                var weekDayValidationType = line["Week Day validation type"];

                var discountPeriodId = Functions.GetNextDiscountPeriodId();

                //Check combinable offers
                var freeCombOffer = Functions.CombinationOfferCodeExists(combOffers, freeCode);
                var lineCombOfferList = new Dictionary<object, int>();
                if (freeCombOffer)
                {
                    Functions.CombinationExcludeCheckList(combOffers, hotelSupplementDatesApply, line, freeCode, lineCombOfferList, true);
                    Functions.CombinationExcludeCheckList(combOffers, hotelFreeDatesApply, line, freeCode, lineCombOfferList, false);
                }

                //According to their doc. "If all the days has N value, then every day of the week can be applied"
                if (!onMonday && !onTuesday && !onWednesday && !onThursday && !onFriday && !onSaturday && !onSunday)
                {
                    onMonday = onTuesday = onWednesday = onThursday = onFriday = onSaturday = onSunday = true;
                }

                var rowCombinations = Functions.RowCombinations(distinctCombinations, txtRoomTypeCode, txtRoomCharacteristic, txtRate, txtBoardCode, String.Empty, sortedRates, roomTypesTable, boardsList, contractMarkets);

                foreach (var date in freeDates)
                {
                    if (date.DayOfWeek.Equals(DayOfWeek.Monday) && !onMonday) continue;
                    if (date.DayOfWeek.Equals(DayOfWeek.Tuesday) && !onTuesday) continue;
                    if (date.DayOfWeek.Equals(DayOfWeek.Wednesday) && !onWednesday) continue;
                    if (date.DayOfWeek.Equals(DayOfWeek.Thursday) && !onThursday) continue;
                    if (date.DayOfWeek.Equals(DayOfWeek.Friday) && !onFriday) continue;
                    if (date.DayOfWeek.Equals(DayOfWeek.Saturday) && !onSaturday) continue;
                    if (date.DayOfWeek.Equals(DayOfWeek.Sunday) && !onSunday) continue;

                    var combNotProcessed = Functions.FilterCombinations(genericTableCombinations, rowCombinations, date);
                    foreach (var cnp in combNotProcessed)
                    {
                        var fields = Functions.SplitCombination(splittedCombinations, cnp, null);
                        var marketCode = fields["Market"];
                        var roomTypeCode = fields["Room type"];
                        var roomCharacteristic = fields["Characteristic"];
                        var rate = fields["Rate"];
                        var boardCode = fields["Board"];
                        var roomRate = fixRate ? rate : HotelBedsSettings.GenericRateCode;

                        //Check combinable offers per day
                        if (lineCombOfferList.Count > 0)
                        {
                            var fApply = Functions.CombinationOfferApply(combOffers, hotelSupplementDatesApply, hotelFreeDatesApply, lineCombOfferList, freeCode, date, marketCode, roomTypeCode, roomCharacteristic, boardCode, rate, false);
                            if (!fApply)
                            {
                                continue;
                            }
                        }

                        var roomGroupId = Functions.CreateRoomGroupId(contractRoomId, roomTypeCode, roomCharacteristic, roomRate, contractRoomIdType);
                        var roomList = Functions.RoomList(baseRooms, baseRoomGroups, roomGroupId, baseRoomsIndex);

                        foreach (var room in roomList)
                        {
                            if (
                                !baseRoomsPrice.ContainsKey(room.RoomId) || !baseRoomsPrice[room.RoomId].ContainsKey(date)
                                || (!String.IsNullOrEmpty(rate) && !baseRoomsPriceRate[room.RoomId][date].Equals(rate))
                            )
                            {
                                continue;
                            }

                            if (!room.AvailabilityPeriods.ContainsKey(date))
                            {
                                Functions.LogLine(logList, hotelIdName, "availability not created", date.ToString());
                                continue;
                            }
                            var availabilityPeriod = room.AvailabilityPeriods[date];
                            if (availabilityPeriod.AvailabilityAvailableUnits == 0)
                            {
                                continue;
                            }

                            if (!typeSupplementProcessed.ContainsKey(date))
                            {
                                typeSupplementProcessed.Add(date, new Dictionary<string, Dictionary<string, List<string>>>());
                            }
                            if (!typeSupplementProcessed[date].ContainsKey(applicationStayType))
                            {
                                typeSupplementProcessed[date].Add(applicationStayType, new Dictionary<string, List<string>>());
                            }
                            if (!typeSupplementProcessed[date][applicationStayType].ContainsKey(room.RoomId))
                            {
                                typeSupplementProcessed[date][applicationStayType].Add(room.RoomId, new List<string>());
                            }
                            if (typeSupplementProcessed[date][applicationStayType][room.RoomId].Contains(boardCode))
                            {
                                //Discount for this board already applied
                                continue;
                            }

                            var otherFree = false;
                            foreach (var d in availabilityPeriod.Discounts)
                            {
                                if (d.Type == DiscountType.PayStay || d.Type == DiscountType.MinMax)
                                {
                                    otherFree = true;
                                    break;
                                }
                            }
                            if (otherFree)
                            {
                                Functions.LogLine(logList, hotelIdName, "frees not applied filtered by other discount with same type", "otherFree");
                                continue;
                            }

                            var countDiscounts = availabilityPeriod.Discounts.Count;
                            Discount disc;

                            if (!String.IsNullOrEmpty(freeNights))
                            {
                                var nightsFree = Decimal.Parse(freeNights, NumberFormatInfo.InvariantInfo);
                                //applicationBaseType "T" -> affects to the total price
                                if (!applicationBaseType.Equals("T"))
                                {
                                    nightsFree -= 0.5M;
                                }
                                disc = Discount.PayStay(minimumDays, 1, nightsFree, discountPeriodId, countDiscounts);
                            }
                            else if (!String.IsNullOrEmpty(discount) && discount[0].Equals('-'))
                            {
                                var percentage = Decimal.Parse(discount, NumberFormatInfo.InvariantInfo);
                                percentage = percentage * -1;
                                disc = Discount.MinMaxPercent(minimumDays, maximumDays, percentage, discountPeriodId, countDiscounts);
                            }
                            else
                            {
                                Functions.LogLine(logList, hotelIdName, "frees not supported", countDiscounts.ToString());
                                continue;
                            }

                            if (disc != null)
                            {
                                disc.LastDate = finalDate;
                                availabilityPeriod.Discounts.TryAdd(disc);
                            }

                            //If we have created a discount, we can't create another for this boards
                            if (availabilityPeriod.Discounts.Count > countDiscounts)
                            {
                                foreach (var board in boardsList)
                                {
                                    if (!typeSupplementProcessed[date][applicationStayType][room.RoomId].Contains(board))
                                    {
                                        typeSupplementProcessed[date][applicationStayType][room.RoomId].Add(board);
                                    }
                                }
                            }
                        }

                        //Mark combination processed
                        genericTableCombinations[date][cnp] = true;
                    }
                }
            }
        }

        #endregion

        #region Cancellation fees

        public void TableCancellationFees(
            IHotelBedsDataTable cancellationFeesTable,
            ConcurrentDictionary<string, ConcurrentDictionary<string, ConcurrentBag<string>>> logList,
            string indexLog,
            bool fixRate,
            string contractRoomId,
            string contractRoomIdType,
            DateTime cacheIni,
            DateTime cacheEnd,
            HashSet<DateTime> contractDates,
            Dictionary<string, List<Room>> baseRoomsIndex,
            Dictionary<string, RoomGroup> baseRoomGroups,
            Dictionary<string, Room> baseRooms,
            Dictionary<string, CancellationPolicy> baseCancellationPolicies,
            int maxDeadlineHours
        )
        {
            foreach (var line in cancellationFeesTable.Rows)
            {
                var applicationStartDate = !String.IsNullOrEmpty(line["Application date"]) ? Functions.ParseDate(line["Application date"]) : cacheIni;
                var applicationFinalDate = !String.IsNullOrEmpty(line["Application end date"]) ? Functions.ParseDate(line["Application end date"]) : cacheEnd;
                if (applicationStartDate > TodayDate || TodayDate > applicationFinalDate)
                {
                    Functions.LogLine(logList, indexLog, "cancellation policy not applied filtered by application dates", applicationStartDate.ToString());
                    continue;
                }
                var rate = line["Rate"];
                var roomRate = fixRate ? rate : HotelBedsSettings.GenericRateCode;
                var startDate = Functions.ParseDate(line["Initial date"]);
                var finalDate = Functions.ParseDate(line["Final date"]);
                var cancellationDates = Functions.ListValidDates(startDate, finalDate, cacheIni, cacheEnd, contractDates);
                var type = line["Type"];
                var days = line["Days"];
                var hours = line["Hours"];
                var amountFirstNight = Decimal.Parse(line["Amount first night"], NumberFormatInfo.InvariantInfo);
                var percentageFirstNight = Decimal.Parse(line["Percentage first night"], NumberFormatInfo.InvariantInfo);
                var amountPerNight = Decimal.Parse(line["Amount per night"], NumberFormatInfo.InvariantInfo);
                var percentagePerNight = Decimal.Parse(line["Percentage per night"], NumberFormatInfo.InvariantInfo);

                var roomGroupId = Functions.CreateRoomGroupId(contractRoomId, String.Empty, String.Empty, roomRate, contractRoomIdType);
                var roomList = Functions.RoomList(baseRooms, baseRoomGroups, roomGroupId, baseRoomsIndex);

                var penaltyBasis = String.Empty;
                var penaltyValue = Decimal.Zero;
                var hour = 0;
                if (!String.IsNullOrEmpty(days) || !String.IsNullOrEmpty(hours))
                {
                    if (!String.IsNullOrEmpty(days))
                    {
                        hour += Int32.Parse(days) * 24;
                    }
                    if (!String.IsNullOrEmpty(hours))
                    {
                        hour += Int32.Parse(hours);
                    }

                    hour = Functions.CancellationPolicyRoundHours(hour, maxDeadlineHours);
                }
                else
                {
                    hour = Functions.CancellationPolicyRoundHours(-1, maxDeadlineHours);
                }
                if (percentagePerNight != Decimal.Zero)
                {
                    penaltyBasis = "full_stay";
                    penaltyValue = percentagePerNight;
                }
                if (amountPerNight != Decimal.Zero)
                {
                    penaltyBasis = "full_stay";
                    penaltyValue = 100;
                }
                if (percentageFirstNight != Decimal.Zero)
                {
                    penaltyBasis = "nights";
                    penaltyValue = Math.Truncate(percentageFirstNight / 100) + (percentageFirstNight % 100 > Decimal.Zero ? Decimal.One : Decimal.Zero);
                    //1% to 100% one night, 101% to 200% two nigths...
                }
                if (amountFirstNight != Decimal.Zero)
                {
                    penaltyBasis = "fixed_amount";
                    penaltyValue = amountFirstNight;
                }

                if (penaltyValue == Decimal.Zero)
                {
                    Functions.LogLine(logList, indexLog, "cancellation policy penalty is zero", "0");
                    continue;
                }

                var intPenaltyValue = Convert.ToInt32(Decimal.Round(penaltyValue, 0, MidpointRounding.AwayFromZero));
                var nrfPolicyId = Functions.CancellationPolicyId(roomRate, contractRoomIdType, "full_stay", maxDeadlineHours, 100);

                foreach (var date in cancellationDates)
                {
                    foreach (var room in roomList)
                    {
                        if (!room.AvailabilityPeriods.ContainsKey(date))
                        {
                            Functions.LogLine(logList, indexLog, "availability not created", date.ToString());
                            continue;
                        }
                        var availabilityPeriod = room.AvailabilityPeriods[date];
                        if (availabilityPeriod.AvailabilityAvailableUnits == 0)
                        {
                            continue;
                        }

                        if (penaltyValue > 100 && penaltyBasis.Equals("full_stay"))
                        {
                            Functions.LogLine(logList, indexLog, "cancellation policy penalty is over 100%", penaltyValue.ToString());
                            availabilityPeriod.AvailabilityAvailableUnits = 0;
                            continue;
                        }

                        var canPolId = String.Empty;
                        CancellationPolicy canPol = null;
                        if (availabilityPeriod.CancellationPolicies.Count != 0)
                        {
                            if (!availabilityPeriod.CancellationPolicies.Contains(nrfPolicyId))
                            {
                                var hasConflict = false;
                                var indexPolicy = 0;
                                canPol = Functions.CancellationPolicyResolveConflicts(roomRate, contractRoomIdType, baseCancellationPolicies, availabilityPeriod.CancellationPolicies, ref hasConflict, ref indexPolicy, penaltyBasis, hour, intPenaltyValue);
                                if (!hasConflict && canPol != null)
                                {
                                    canPolId = canPol.Id;
                                    if (canPolId == nrfPolicyId && availabilityPeriod.CancellationPolicies.Count > 0)
                                    {
                                        availabilityPeriod.CancellationPolicies.Clear();
                                    }
                                }
                            }
                        }
                        else
                        {
                            canPolId = Functions.CancellationPolicyId(roomRate, contractRoomIdType, penaltyBasis, hour, intPenaltyValue);
                            canPol = Functions.CancellationPolicyCreate(baseCancellationPolicies, canPolId, penaltyBasis, hour, intPenaltyValue);
                        }

                        if (!String.IsNullOrEmpty(canPolId) && canPol != null)
                        {
                            if (!baseCancellationPolicies.ContainsKey(canPolId))
                            {
                                baseCancellationPolicies.Add(canPolId, canPol);
                            }
                            if (!availabilityPeriod.CancellationPolicies.Contains(canPolId))
                            {
                                availabilityPeriod.CancellationPolicies.Add(canPolId);

                                //Sort cancellation policies, lower deadline higher penalty
                                if (availabilityPeriod.CancellationPolicies.Count > 1)
                                {
                                    var sCanPolicies = new SortedList<int, string>();
                                    foreach (var idPol in availabilityPeriod.CancellationPolicies)
                                    {
                                        sCanPolicies.Add(baseCancellationPolicies[idPol].Deadline_Value.Value, idPol);
                                    }
                                    availabilityPeriod.CancellationPolicies.Clear();
                                    availabilityPeriod.CancellationPolicies.AddRange(sCanPolicies.Values);
                                }
                            }
                        }
                    }
                }
            }
        }

        #endregion

        #region Handling fees

        public void TableHandlingFees(
            IHotelBedsDataTable handlingFeesTable,
            ConcurrentDictionary<string, ConcurrentDictionary<string, ConcurrentBag<string>>> logList,
            string indexLog,
            Dictionary<string, List<string>> distinctCombinations,
            Dictionary<string, Dictionary<string, string>> splittedCombinations,
            SortedList<int, Dictionary<string, string>> sortedRates,
            IHotelBedsDataTable roomTypesTable,
            List<string> boardsList,
            bool fixRate,
            string contractRoomId,
            string contractRoomIdType,
            DateTime cacheIni,
            DateTime cacheEnd,
            HashSet<DateTime> contractDates,
            Dictionary<string, List<Room>> baseRoomsIndex,
            Dictionary<string, RoomGroup> baseRoomGroups,
            Dictionary<string, Room> baseRooms,
            List<string> contractMarkets
        )
        {
            var sortedHandlingFeesTable = Functions.OrderDataTable(handlingFeesTable, String.Empty, String.Empty, "Rate", String.Empty, String.Empty);
            sortedHandlingFeesTable = Functions.GroupDataTablePerAge(sortedHandlingFeesTable);
            var genericTableCombinations = new Dictionary<DateTime, Dictionary<string, bool>>();

            foreach (var line in sortedHandlingFeesTable)
            {
                var txtRate = line["Rate"];
                var startDate = Functions.ParseDate(line["Initial date"]);
                var finalDate = Functions.ParseDate(line["Final date"]);
                var handlingDates = Functions.ListValidDates(startDate, finalDate, cacheIni, cacheEnd, contractDates);
                var type = line["Type"];
                var code = line["Code"];
                var amount = line["Amount"];
                var percentage = line["Percentage"];
                var adultAmount = line["Adult amount"];
                var childAmount = line["Child amount"];
                var minimumAge = line["Minimum age"];
                var maximumAge = line["Maximum age"];
                var ageAmount = line["Age Amount"];
                var isPerService = line["Is per service"].Equals("Y");

                bool adultPrice;
                bool childPrice;
                Functions.PaxApplyPrice(minimumAge, maximumAge, out adultPrice, out childPrice);

                if (!type.Equals("D") || (!adultPrice && !childPrice))
                {
                    Functions.LogLine(logList, indexLog, "handling fee not supported", type);
                    continue;
                }

                var finalAmount = Decimal.Zero;
                if (!String.IsNullOrEmpty(ageAmount))
                {
                    finalAmount = Decimal.Parse(ageAmount, NumberFormatInfo.InvariantInfo);
                }
                else if (!String.IsNullOrEmpty(adultAmount))
                {
                    finalAmount = Decimal.Parse(adultAmount, NumberFormatInfo.InvariantInfo);
                }
                /*else if (!String.IsNullOrEmpty(childAmount))
                {
                    finalAmount = Decimal.Parse(childAmount, NumberFormatInfo.InvariantInfo);
                }*/

                var rowCombinations = Functions.RowCombinations(distinctCombinations, String.Empty, String.Empty, txtRate, String.Empty, String.Empty, sortedRates, roomTypesTable, boardsList, contractMarkets);

                foreach (var date in handlingDates)
                {
                    var combNotProcessed = Functions.FilterCombinations(genericTableCombinations, rowCombinations, date);
                    foreach (var cnp in combNotProcessed)
                    {
                        var fields = Functions.SplitCombination(splittedCombinations, cnp, null);
                        var marketCode = fields["Market"];
                        var roomTypeCode = fields["Room type"];
                        var roomCharacteristic = fields["Characteristic"];
                        var rate = fields["Rate"];
                        var boardCode = fields["Board"];
                        var roomRate = fixRate ? rate : HotelBedsSettings.GenericRateCode;

                        var roomGroupId = Functions.CreateRoomGroupId(contractRoomId, roomTypeCode, roomCharacteristic, roomRate, contractRoomIdType);
                        var roomList = Functions.RoomList(baseRooms, baseRoomGroups, roomGroupId, baseRoomsIndex);

                        foreach (var room in roomList)
                        {
                            foreach (var availabilityPeriod in room.AvailabilityPeriods.Values)
                            {
                                availabilityPeriod.Pricing_BasePrice = Functions.ApplyExtraCharge(availabilityPeriod.Pricing_BasePrice, finalAmount, null);
                                availabilityPeriod.Pricing_ExtrabedAdult = Functions.ApplyExtraCharge(availabilityPeriod.Pricing_ExtrabedAdult, finalAmount, null);
                                availabilityPeriod.Pricing_ExtrabedChild = Functions.ApplyExtraCharge(availabilityPeriod.Pricing_ExtrabedChild, finalAmount, null);

                                foreach (var additionalBoard in availabilityPeriod.AdditionalBoards)
                                {
                                    additionalBoard.Adult = Functions.ApplyExtraCharge(additionalBoard.Adult, finalAmount, null);
                                    additionalBoard.Child = Functions.ApplyExtraCharge(additionalBoard.Child, finalAmount, null);
                                }
                            }
                        }

                        //Mark combination processed
                        genericTableCombinations[date][cnp] = true;
                    }
                }
            }
        }

        #endregion

    }
}
