﻿using SunHotels.XML.Data;
using System;
using System.Collections.Generic;

namespace SunHotels.Providers
{
    public class HotelBedsSupplementsPriceParams
    {
        public Dictionary<string, RoomType> BaseRoomTypes { get; set; }
        public Dictionary<string, int> BaseRoomsCapacity { get; set; }
        public Dictionary<string, Dictionary<DateTime, decimal?[]>> BaseRoomsPrice { get; set; }
        public Dictionary<string, Dictionary<DateTime, bool>> BaseRoomsPriceIsPerPax { get; set; }
        public Dictionary<string, Dictionary<DateTime, Dictionary<string, Dictionary<string, decimal?[]>>>> BaseRoomsPriceBoard { get; set; }
        public Dictionary<string, Dictionary<DateTime, Dictionary<string, Dictionary<string, bool>>>> BaseRoomsPriceBoardIsPerPax { get; set; }
        public DateTime Date { get; set; }
        public Room Room { get; set; }
        public string SupplementType { get; set; }
        public string Rate { get; set; }
        public string BoardCode { get; set; }
        public string TxtAmount { get; set; }
        public string TxtPercentage { get; set; }
        public string ApplicationType { get; set; }
        public int SupplementChildToAdult { get; set; }
    }
}
