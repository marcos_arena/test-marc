﻿using Sunhotels.Export;
using SunHotels.XML.Exporters;
using SunHotels.XML.Data;
using System.Xml;
using System.IO;
using System.Text;
using System.Collections.Concurrent;
using System.Collections.Generic;
using SunHotels.Export;

namespace SunHotels.Providers
{
    public class HotelBedsCommonExporter : XmlExporter
    {
        private string subDirectory = "tempHotelFiles";
        private string path;

        public HotelBedsCommonExporter(Configuration configuration) : base(configuration)
        {
            var fileInfo = new FileInfo(configuration.CacheOutput);
            path = fileInfo.Directory.FullName + @"\" + subDirectory;
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
        }

        public void WriteHotel(ConcurrentDictionary<string, RoomType> roomTypes, Hotel h, List<Room> roomList)
        {
            var dataRoot = new Root();

            foreach (var r in roomList)
            {
                RoomType rt;
                if (!dataRoot.RoomTypes.ContainsKey(r.TypeId) && roomTypes.TryGetValue(r.TypeId, out rt))
                {
                    dataRoot.RoomTypes.Add(r.TypeId, rt);
                }
            }

            using (var fs = new FileStream(path + @"\" + h.Id + ".xml", FileMode.Create))
            {
                using (var writer = new XmlTextWriter(fs, new UTF8Encoding(false)) { Formatting = Formatting.Indented, IndentChar = '\t', Indentation = 1 })
                {
                    lock (subDirectory)
                    {
                        WriteHotelNode(writer, h, h.PlaceId, CacheXsdWriterType.Common, dataRoot);
                    }
                }
            }
        }

        public override void Write(ExportData data)
        {
            base.Write(data);

            File.WriteAllText(configuration.CacheOutput,
                File.ReadAllText(configuration.CacheOutput)
                .Replace("</sunhotels_cache_file>", "")
                .Replace("<hotels />", "<hotels>"));

            using (var output = File.Open(configuration.CacheOutput, FileMode.Open, FileAccess.ReadWrite, FileShare.None))
            {
                output.Seek(0, SeekOrigin.End);
                foreach (var file in Directory.GetFiles(path))
                {
                    using (var input = File.OpenRead(file))
                    {
                        input.CopyTo(output);
                    }

                    File.Delete(file);
                }
            }

            File.AppendAllText(configuration.CacheOutput, "</hotels></sunhotels_cache_file>");

            Directory.Delete(path);
        }
    }
}
