﻿using SunHotels.XML.Data;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;

namespace SunHotels.Providers
{
    public class HotelBedsSupplementsParams
    {
        public SortedList<int, Dictionary<string, string>> SortedSupplements { get; set; }
        public Dictionary<string, List<string>> DistinctCombinations { get; set; }
        public Dictionary<string, Dictionary<string, string>> SplittedCombinations { get; set; }
        public SortedList<int, Dictionary<string, string>> SortedRates { get; set; }
        public IHotelBedsDataTable RoomTypesTable { get; set; }
        public List<string> BoardsList { get; set; }
        public DateTime CacheIni { get; set; }
        public DateTime CacheEnd { get; set; }
        public HashSet<DateTime> ContractDates { get; set; }
        public Dictionary<string, List<Room>> BaseRoomsIndex { get; set; }
        public Dictionary<string, int> BaseRoomsCapacity { get; set; }
        public Dictionary<string, Room> BaseRooms { get; set; }
        public Dictionary<string, RoomType> BaseRoomTypes { get; set; }
        public Dictionary<string, RoomGroup> BaseRoomGroups { get; set; }
        public Dictionary<string, Dictionary<DateTime, decimal?[]>> BaseRoomsPrice { get; set; }
        public Dictionary<string, Dictionary<DateTime, bool>> BaseRoomsPriceIsPerPax { get; set; }
        public Dictionary<string, Dictionary<DateTime, string>> BaseRoomsPriceRate { get; set; }
        public Dictionary<string, Dictionary<DateTime, Dictionary<string, Dictionary<string, decimal?[]>>>> BaseRoomsPriceBoard { get; set; }
        public Dictionary<string, Dictionary<DateTime, Dictionary<string, Dictionary<string, bool>>>> BaseRoomsPriceBoardIsPerPax { get; set; }
        public Dictionary<DateTime, Dictionary<string, List<int>>> SupplementChildPaxApplied { get; set; }
        public string SupplementType { get; set; }
        public bool FixRate { get; set; }
        public string ContractRoomId { get; set; }
        public string ContractRoomIdType { get; set; }
        public string Opaque { get; set; }
        public ConcurrentDictionary<string, ConcurrentDictionary<string, ConcurrentBag<string>>> LogList { get; set; }
        public string HotelIdName { get; set; }
        public DateTime TodayDate { get; set; }
        public List<string> ContractMarkets { get; set; }
        public List<object>[] CombOffers { get; set; }
        public Dictionary<object, HashSet<DateTime>> HotelSupplementDates { get; set; }
        public Dictionary<object, Dictionary<DateTime, Dictionary<string, Dictionary<string, string>>>> HotelSupplementDatesApply { get; set; }
        public Dictionary<object, Dictionary<DateTime, Dictionary<string, Dictionary<string, string>>>> HotelFreeDatesApply { get; set; }
        public Dictionary<string, Dictionary<DateTime, Dictionary<string, Dictionary<string, string>>>> HotelGeneralDatesApply { get; set; }
    }
}
