﻿using ICSharpCode.SharpZipLib.Core;
using ICSharpCode.SharpZipLib.Zip;
using Sunhotels.Export;
using SunHotels.XML.Data;
using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Runtime;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using SunHotels.XML.HelpClasses;
using SunHotels.XML;
using System.Linq;

namespace SunHotels.Providers
{
    public class HotelBedsCommon
    {
        public List<HotelBedsSettings.Languages> Languages;
        public HotelBedsSettings.Languages MainLanguage;
        public HotelBedsSettings Settings;

        //Cache resources, directory files list or files name for better performance
        private Dictionary<Enum, Dictionary<string, List<HotelBedsSettings.Languages>>> CacheResourcesByLanguage;
        private ConcurrentDictionary<Enum, Dictionary<string, List<string>>> CacheResourceFileLanguages;
        private ConcurrentDictionary<string, HashSet<string>> CacheDirectoryFileList;
        private ConcurrentDictionary<string, string> CacheFileName;

        private Semaphore ParallelDownloadSemaphore;
        private String TempDirectory;

        public HotelBedsCommon(Configuration configuration)
        {
            CacheResourcesByLanguage = new Dictionary<Enum, Dictionary<string, List<HotelBedsSettings.Languages>>>();
            CacheResourceFileLanguages = new ConcurrentDictionary<Enum, Dictionary<string, List<string>>>();
            CacheDirectoryFileList = new ConcurrentDictionary<string, HashSet<string>>();
            CacheFileName = new ConcurrentDictionary<string, string>();

            //Configuration
            if (File.Exists(configuration.ExportConfigFileFullPath))
            {
                using (var s = new FileStream(configuration.ExportConfigFileFullPath, FileMode.Open, FileAccess.Read))
                {
                    Settings = new HotelBedsSettings();
                    Settings.ImportBaseConfig(configuration, s);
                }
            }

            //Languages
            Languages = new List<HotelBedsSettings.Languages>();
            if (!String.IsNullOrEmpty(Settings.Base.Language))
            {
                var configLanguage = HotelBedsSettings.LanguageFromConfiguration(Settings.Base.Language);
                Languages.Add(configLanguage);
            }
            else
            {
                foreach (var obj in Enum.GetValues(typeof(HotelBedsSettings.Languages)))
                {
                    var configLanguage = (HotelBedsSettings.Languages)obj;
                    Languages.Add(configLanguage);
                }
            }
            MainLanguage = Languages[0];

            var parallelDownloads = Settings.ParallelDownloads.HasValue ? Settings.ParallelDownloads.Value : 4;
            ParallelDownloadSemaphore = new Semaphore(parallelDownloads, parallelDownloads);

            if (!Directory.Exists(Settings.Base.LocalTemp))
            {
                TempDirectory = Directory.CreateDirectory(Settings.Base.LocalTemp).FullName;
            }
            else
            {
                TempDirectory = Settings.Base.LocalTemp;
            }

            //Clean backup dir
            CleanBackupFiles();

            //Compress log files per day and clean log dir
            CompressLogs();
            CleanLogs();
        }

        #region Backup and log files

        private void CleanBackupFiles()
        {
            var tempFiles = Directory.GetFiles(TempDirectory, HotelBedsSettings.FileCreatePrefix + "*", SearchOption.TopDirectoryOnly);
            var sortTempFiles = new SortedList<DateTime, string>();
            foreach (var tempFile in tempFiles)
            {
                var modified = File.GetLastWriteTime(tempFile);
                while (sortTempFiles.ContainsKey(modified))
                {
                    modified = modified.AddSeconds(1);
                }
                sortTempFiles.Add(modified, tempFile);
            }
            var countTempFiles = sortTempFiles.Count;
            foreach (var sortTempFile in sortTempFiles)
            {
                countTempFiles -= 1;
                if (countTempFiles > Settings.MaxTempFiles)
                {
                    File.Delete(sortTempFile.Value);
                }
                else
                {
                    break;
                }
            }
        }

        private void CompressLogs()
        {
            var logFiles = Directory.GetFiles(Settings.Base.LogOutput, Settings.Base.Provider + "*.log", SearchOption.TopDirectoryOnly);
            var sortLogFiles = new SortedList<DateTime, List<string>>();
            var logDateFilter = DateTime.Today.AddDays(-1);
            foreach (var logFile in logFiles)
            {
                var logDate = File.GetLastWriteTime(logFile).Date;
                if (logDate <= logDateFilter)
                {
                    if (!sortLogFiles.ContainsKey(logDate))
                    {
                        sortLogFiles.Add(logDate, new List<string>());
                    }
                    sortLogFiles[logDate].Add(logFile);
                }
            }

            foreach (var sortLogFile in sortLogFiles)
            {
                var logFileName = Settings.Base.LogOutput + Settings.Base.Provider + sortLogFile.Key.ToString("yyyy-MM-dd") + ".log.zip";
                if (!File.Exists(logFileName))
                {
                    var fsOut = File.Create(logFileName);
                    var zipStream = new ZipOutputStream(fsOut);
                    zipStream.SetLevel(9);
                    zipStream.UseZip64 = UseZip64.Off;
                    var folderOffset = Settings.Base.LogOutput.Length + (Settings.Base.LogOutput.EndsWith("\\") ? 0 : 1);
                    foreach (var logFile in sortLogFile.Value)
                    {
                        var fi = new FileInfo(logFile);
                        var entryName = logFile.Substring(folderOffset);
                        entryName = ZipEntry.CleanName(entryName);
                        var newEntry = new ZipEntry(entryName);
                        newEntry.DateTime = fi.LastWriteTime;
                        newEntry.Size = fi.Length;
                        zipStream.PutNextEntry(newEntry);
                        var buffer = new byte[4096];
                        using (var streamReader = File.OpenRead(logFile))
                        {
                            StreamUtils.Copy(streamReader, zipStream, buffer);
                        }
                        zipStream.CloseEntry();
                        File.Delete(logFile);
                    }
                    zipStream.IsStreamOwner = true;
                    zipStream.Close();
                }
            }
        }

        private void CleanLogs()
        {
            var logFiles = Directory.GetFiles(Settings.Base.LogOutput, Settings.Base.Provider + "*.log.zip", SearchOption.TopDirectoryOnly);
            var sortLogFiles = new SortedList<DateTime, string>();
            foreach (var logFile in logFiles)
            {
                var modified = File.GetLastWriteTime(logFile);
                while (sortLogFiles.ContainsKey(modified))
                {
                    modified = modified.AddSeconds(1);
                }
                sortLogFiles.Add(modified, logFile);
            }
            var countLogFiles = sortLogFiles.Count;
            foreach (var sortLogFile in sortLogFiles)
            {
                countLogFiles -= 1;
                if (countLogFiles > Settings.MaxTempFiles)
                {
                    File.Delete(sortLogFile.Value);
                }
                else
                {
                    break;
                }
            }
        }

        #endregion

        #region DataBase functions

        public ConcurrentDictionary<HotelBedsSettings.ReaderType, IHotelBedsDataLayer> CreateDataBase()
        {
            return new ConcurrentDictionary<HotelBedsSettings.ReaderType, IHotelBedsDataLayer>();
        }

        public void DisposeDataBase(ConcurrentDictionary<HotelBedsSettings.ReaderType, IHotelBedsDataLayer> dataBase)
        {
            foreach (var reader in dataBase.Keys)
            {
                dataBase[reader].Dispose(null);
            }

#if !DEBUG
            var directories = new List<string>();
            var files = new List<string>();
            foreach (var resource in HotelBedsSettings.FilesCompression.Keys)
            {
                if (HotelBedsSettings.LanguageFiles.Contains(resource))
                {
                    foreach (var language in Languages)
                    {
                        var resourceName = HotelBedsSettings.LanguageFilesName[resource][language];
                        directories.Add(FullUncompressedFolder(resourceName));
                        files.Add(FullFileLocation(resourceName));
                    }
                }
                else
                {
                    var resourceName = HotelBedsSettings.Files[resource];
                    directories.Add(FullUncompressedFolder(resourceName));
                    files.Add(FullFileLocation(resourceName));
                }
            }
            foreach (var directory in directories)
            {
                if (Directory.Exists(directory))
                {
                    Directory.Delete(directory, true);
                }
            }
            if (!String.IsNullOrEmpty(Settings.Aif2Url))
            {
                foreach (var file in files)
                {
                    if (File.Exists(file))
                    {
                        File.Delete(file);
                    }
                }
            }
#endif
        }

        #endregion

        #region Performance functions

        public HashSet<string> CacheDirectoryFileListManager(string directory)
        {
            if (!CacheDirectoryFileList.ContainsKey(directory))
            {
                var files = new HashSet<string>(Directory.GetFiles(directory));
                CacheDirectoryFileList.TryAdd(directory, files);

                foreach (var file in files)
                {
                    CacheFileNameManager(file, true);
                }
            }

            HashSet<string> filesList;
            CacheDirectoryFileList.TryGetValue(directory, out filesList);

            return filesList;
        }

        public string CacheFileNameManager(string fullLocationFileName, bool cache)
        {
            string fileName = String.Empty;
            if (cache && !CacheFileName.ContainsKey(fullLocationFileName))
            {
                var indexInvert = fullLocationFileName.LastIndexOf(@"\");
                var indexNormal = fullLocationFileName.LastIndexOf(@"/");
                var index = indexInvert >= indexNormal ? indexInvert : indexNormal;
                fileName = fullLocationFileName.Substring((index >= 0 ? index + 1 : 0));
                CacheFileName.TryAdd(fullLocationFileName, fileName);
            }
            else
            {
                CacheFileName.TryGetValue(fullLocationFileName, out fileName);
            }
            return fileName;
        }

        #endregion

        #region Languages functions

        public Dictionary<Enum, Dictionary<string, List<HotelBedsSettings.Languages>>> ResourcesByLanguages(ArrayList resources)
        {
            var resourceDictionary = new Dictionary<Enum, Dictionary<string, List<HotelBedsSettings.Languages>>>();

            if (resources.Count > 0)
            {
                foreach (var rsrc in resources)
                {
                    var resource = (Enum)rsrc;
                    if (CacheResourcesByLanguage.ContainsKey(resource))
                    {
                        resourceDictionary.Add(resource, CacheResourcesByLanguage[resource]);
                    }
                    else
                    {
                        var fileLanguage = new List<KeyValuePair<string, HotelBedsSettings.Languages>>();
                        if (HotelBedsSettings.LanguageFiles.Contains(resource))
                        {
                            foreach (var language in Languages)
                            {
                                if (HotelBedsSettings.LanguageFilesName[resource].ContainsKey(language))
                                {
                                    fileLanguage.Add(new KeyValuePair<string, HotelBedsSettings.Languages>(HotelBedsSettings.LanguageFilesName[resource][language], language));
                                }
                            }
                        }
                        else if (HotelBedsSettings.Files.ContainsKey(resource))
                        {
                            fileLanguage.Add(new KeyValuePair<string, HotelBedsSettings.Languages>(HotelBedsSettings.Files[resource], MainLanguage));
                        }

                        foreach (var kvp in fileLanguage)
                        {
                            var resourceName = kvp.Key;
                            var language = kvp.Value;

                            if (!resourceDictionary.ContainsKey(resource))
                            {
                                resourceDictionary.Add(resource, new Dictionary<string, List<HotelBedsSettings.Languages>>());
                            }
                            if (!resourceDictionary[resource].ContainsKey(resourceName))
                            {
                                resourceDictionary[resource].Add(resourceName, new List<HotelBedsSettings.Languages>());
                            }
                            if (!resourceDictionary[resource][resourceName].Contains(language))
                            {
                                resourceDictionary[resource][resourceName].Add(language);
                            }
                        }

                        CacheResourcesByLanguage.Add(resource, resourceDictionary[resource]);
                    }
                }
            }

            return resourceDictionary;
        }

        public Dictionary<HotelBedsSettings.Languages, Dictionary<string, IHotelBedsDataTable>> GetLanguageTable(ConcurrentDictionary<HotelBedsSettings.ReaderType, IHotelBedsDataLayer> dataBase, Enum resource, string filterFileName)
        {
            var languageTable = new Dictionary<HotelBedsSettings.Languages, Dictionary<string, IHotelBedsDataTable>>();
            foreach (var reader in dataBase.Keys)
            {
                IHotelBedsDataLayer dataLayer;
                dataBase.TryGetValue(reader, out dataLayer);
                if (!String.IsNullOrEmpty(filterFileName))
                {
                    if (!dataLayer.DataTables.ContainsKey(filterFileName))
                    {
                        continue;
                    }
                    else
                    {
                        Enum dataLayerResource;
                        dataLayer.DataResource.TryGetValue(filterFileName, out dataLayerResource);
                        if (Enum.Equals(dataLayerResource, resource))
                        {
                            LanguageTable(languageTable, dataLayer, resource, filterFileName);
                        }
                    }
                }
                else
                {
                    foreach (var fileName in dataLayer.DataTables.Keys)
                    {
                        Enum dataLayerResource;
                        dataLayer.DataResource.TryGetValue(fileName, out dataLayerResource);
                        if (Enum.Equals(dataLayerResource, resource))
                        {
                            LanguageTable(languageTable, dataLayer, resource, fileName);
                        }
                    }
                }
            }
            return languageTable;
        }

        private void LanguageTable(Dictionary<HotelBedsSettings.Languages, Dictionary<string, IHotelBedsDataTable>> languageTable, IHotelBedsDataLayer readerDataBase, Enum resource, string fileName)
        {
            var fileOk = false;
            var languageList = new List<HotelBedsSettings.Languages>();
            if (HotelBedsSettings.FilesCompression.ContainsKey(resource))
            {
                if (HotelBedsSettings.LanguageFiles.Contains(resource))
                {
                    FileStream dataLayerFile;
                    readerDataBase.DataFiles.TryGetValue(fileName, out dataLayerFile);
                    var index = dataLayerFile.Name.LastIndexOf(@"\");
                    var hardDiskFileName = dataLayerFile.Name.Substring((index >= 0 ? index + 1 : 0));
                    foreach (var fileLanguage in HotelBedsSettings.LanguageFilesName[resource].Keys)
                    {
                        if (Languages.Contains(fileLanguage))
                        {
                            var resourceName = HotelBedsSettings.LanguageFilesName[resource][fileLanguage];
                            var languageFileName = LanguageFileName(resource, resourceName, hardDiskFileName);
                            if (fileName.Equals(languageFileName))
                            {
                                fileOk = true;
                                languageList.Add(fileLanguage);
                            }
                        }
                    }

                }
                else if (HotelBedsSettings.FilesChild.ContainsKey(resource))
                {
                    if (HotelBedsSettings.FilesChild[resource][0] == HotelBedsSettings.ChildFiles.AllFiles)
                    {
                        fileOk = true;
                        languageList.Add(MainLanguage);
                    }
                    else
                    {
                        foreach (var childFile in HotelBedsSettings.FilesChild[resource])
                        {
                            if (HotelBedsSettings.Files.ContainsKey(childFile) && Path.GetFileName(HotelBedsSettings.Files[childFile]).Equals(fileName))
                            {
                                fileOk = true;
                                languageList.Add(MainLanguage);
                                break;
                            }
                        }
                    }
                }
            }
            else
            {
                if (HotelBedsSettings.LanguageFiles.Contains(resource))
                {
                    foreach (var fileLanguage in HotelBedsSettings.LanguageFilesName[resource].Keys)
                    {
                        if (
                            Languages.Contains(fileLanguage)
                            && fileName.Equals(Path.GetFileName(HotelBedsSettings.LanguageFilesName[resource][fileLanguage]))
                        )
                        {
                            fileOk = true;
                            languageList.Add(fileLanguage);
                        }
                    }
                }
                else if (HotelBedsSettings.Files.ContainsKey(resource) && fileName.Equals(Path.GetFileName(HotelBedsSettings.Files[resource])))
                {
                    fileOk = true;
                    languageList.Add(MainLanguage);
                }
            }

            if (fileOk && languageList.Count > 0)
            {
                foreach (var lang in languageList)
                {
                    if (!languageTable.ContainsKey(lang))
                    {
                        languageTable.Add(lang, new Dictionary<string, IHotelBedsDataTable>());
                    }

                    Dictionary<string, IHotelBedsDataTable> dataTables;
                    readerDataBase.DataTables.TryGetValue(fileName, out dataTables);
                    foreach (var tableName in dataTables.Keys)
                    {
                        if (!languageTable[lang].ContainsKey(tableName))
                        {
                            languageTable[lang].Add(tableName, dataTables[tableName]);
                        }
                    }
                }
            }
        }

        public string LanguageFileName(Enum resource, string resourceName, string fileName)
        {
            string languageFileName = fileName;
            if (HotelBedsSettings.LanguageFiles.Contains(resource))
            {
                foreach (var language in HotelBedsSettings.LanguageFilesName[resource].Keys)
                {
                    if (HotelBedsSettings.LanguageFilesName[resource][language].Equals(resourceName))
                    {
                        languageFileName = HotelBedsSettings.LanguageExternalCode[language] + "_" + languageFileName;
                        break;
                    }
                }
            }
            return languageFileName;
        }

        public bool LanguageLoadData(IHotelBedsDataLayer readerDataBase, Enum resource, string fullFileLocation)
        {
            var allFilesLoaded = true;
            var fileName = CacheFileNameManager(fullFileLocation, false);
            var languageFiles = LanguageFilesName(resource, fileName);
            foreach (var loadFile in languageFiles)
            {
                if (readerDataBase.DataFiles.ContainsKey(loadFile))
                {
                    allFilesLoaded = readerDataBase.LoadDataFile(loadFile);
                }
                if (!allFilesLoaded)
                {
                    break;
            }
        }
            return allFilesLoaded;
        }

        public void LanguageDisposeData(IHotelBedsDataLayer readerDataBase, Enum resource, string fullFileLocation)
        {
            var fileName = CacheFileNameManager(fullFileLocation, false);
            var languageFiles = LanguageFilesName(resource, fileName);
            foreach (var loadFile in languageFiles)
            {
                if (readerDataBase.DataFiles.ContainsKey(loadFile))
                {
                    readerDataBase.Dispose(loadFile);
                }
            }
        }

        private List<string> LanguageFilesName(Enum resource, string fileName)
        {
            List<string> languageFiles = null;
            if (CacheResourceFileLanguages.ContainsKey(resource))
            {
                Dictionary<string, List<string>> filesResource = null;
                CacheResourceFileLanguages.TryGetValue(resource, out filesResource);
                if (filesResource.ContainsKey(fileName))
                {
                    languageFiles = filesResource[fileName];
                }
            }

            if (languageFiles == null)
            {
                languageFiles = new List<string>();
                if (HotelBedsSettings.FilesCompression.ContainsKey(resource))
                {
                    if (HotelBedsSettings.LanguageFiles.Contains(resource))
                    {
                        foreach (var fileLanguage in HotelBedsSettings.LanguageFilesName[resource].Keys)
                        {
                            if (Languages.Contains(fileLanguage))
                            {
                                var resourceName = HotelBedsSettings.LanguageFilesName[resource][fileLanguage];
                                var files = CacheDirectoryFileListManager(FullUncompressedFolder(resourceName));
                                var languageFileFullLocation = FullUncompressedFolder(resourceName) + fileName;
                                if (files.Contains(languageFileFullLocation))
                                {
                                    var languageFileName = LanguageFileName(resource, resourceName, fileName);
                                    if (!languageFiles.Contains(languageFileName))
                                    {
                                        languageFiles.Add(languageFileName);
                                    }
                                }
                            }
                        }

                    }
                    else if (HotelBedsSettings.FilesChild.ContainsKey(resource))
                    {
                        if (HotelBedsSettings.FilesChild[resource][0] == HotelBedsSettings.ChildFiles.AllFiles)
                        {
                            languageFiles.Add(fileName);
                        }
                        else
                        {
                            foreach (var childFile in HotelBedsSettings.FilesChild[resource])
                            {
                                if (HotelBedsSettings.Files.ContainsKey(childFile) && Path.GetFileName(HotelBedsSettings.Files[childFile]).Equals(fileName))
                                {
                                    languageFiles.Add(fileName);
                                    break;
                                }
                            }
                        }
                    }
                }
                else
                {
                    if (HotelBedsSettings.LanguageFiles.Contains(resource))
                    {
                        foreach (var fileLanguage in HotelBedsSettings.LanguageFilesName[resource].Keys)
                        {
                            if (
                                Languages.Contains(fileLanguage)
                                && fileName.Equals(Path.GetFileName(HotelBedsSettings.LanguageFilesName[resource][fileLanguage]))
                                && !languageFiles.Contains(fileName)
                            )
                            {
                                languageFiles.Add(fileName);
                            }
                        }
                    }
                    else if (HotelBedsSettings.Files.ContainsKey(resource) && fileName.Equals(Path.GetFileName(HotelBedsSettings.Files[resource])))
                    {
                        languageFiles.Add(fileName);
                    }
                }

                lock (CacheResourceFileLanguages)
                {
                    if (!CacheResourceFileLanguages.ContainsKey(resource))
                    {
                        CacheResourceFileLanguages.TryAdd(resource, new Dictionary<string, List<string>>());
                    }
                    Dictionary<string, List<string>> fileLanguages;
                    CacheResourceFileLanguages.TryGetValue(resource, out fileLanguages);
                    if (!fileLanguages.ContainsKey(fileName))
                    {
                        fileLanguages.Add(fileName, languageFiles);
                    }
                }
            }

            return languageFiles;
        }

        #endregion

        #region Download Resource

        public void ResourceManager(IHotelBedsConnect hotelBedsConnect, string baseUrl, string user, string password, Enum resource, string resourceName)
        {
#if DEBUG
            Console.WriteLine("****** SUPER WARNING ******* RUNNING IN DEBUG MODE, IF THIS IS LIVE ENVIRONMENT, PLEASE REDEPLOY IN RELEASE MODE");
            if (Directory.Exists(TempDirectory) && Directory.GetFiles(TempDirectory).Length > 0)
                return;
#endif
            //Download
            var strDestination = FullFileLocation(resourceName);

            var downloadCompleted = false;
            var countDownload = Settings.RetryDownloads.GetValueOrDefault(3);
            var secondsBetweenRetries = Settings.SecondsBetweenRetries.GetValueOrDefault(3);
            var errorTxt = "";
            while (!downloadCompleted && countDownload > 0)
            {
                try
                {
                    ParallelDownloadSemaphore.WaitOne();
                    if (!String.IsNullOrEmpty(baseUrl) || !File.Exists(strDestination))
                    {
                        ResourceDownload(hotelBedsConnect, baseUrl, user, password, resourceName);
                    }
                    downloadCompleted = true;
                }
                catch (Exception e)
                {
                    errorTxt += System.Environment.NewLine + "Call retry:" + countDownload.ToString() + " > " +  e.Message + " " + e.StackTrace;
                    System.Threading.Thread.Sleep(secondsBetweenRetries * 1000);
                }
                finally
                {
                    countDownload -= 1;
                    ParallelDownloadSemaphore.Release();
                }
            }

            if (!downloadCompleted)
            {
                throw new ApplicationException("Error downloading: " + resourceName + " - " + errorTxt);
            }

            //Uncompress
            if (HotelBedsSettings.FilesCompression.ContainsKey(resource) && HotelBedsSettings.FilesChild.ContainsKey(resource) && File.Exists(strDestination))
            {
                var resourceStream = new FileStream(strDestination, FileMode.Open, FileAccess.Read, FileShare.Read);
                ResourceUncompress(resourceStream, resource, resourceName);
            }
        }

        private void ResourceDownload(IHotelBedsConnect hotelBedsConnect, string baseUrl, string user, string password, string resourceName)
        {
            Console.WriteLine("Connect Start: {0} ({1} -> {2})", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), hotelBedsConnect.GetType().Name, resourceName);
            hotelBedsConnect.BaseUrl = baseUrl;
            hotelBedsConnect.User = user;
            hotelBedsConnect.Password = password;
            hotelBedsConnect.GetResource(resourceName, FullFileLocation(resourceName));
            Console.WriteLine("Connect End:   {0} ({1} -> {2})", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), hotelBedsConnect.GetType().Name, resourceName);
        }

        private void ResourceUncompress(FileStream resourceStream, Enum resource, string resourceName)
        {
            var compressionType = HotelBedsSettings.FilesCompression[resource];
            Console.WriteLine("Uncompression Start: {0} ({1} -> {2})", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), Enum.GetName(typeof(HotelBedsSettings.CompressionType), compressionType), resourceName);
            var folderDestination = FullUncompressedFolder(resourceName);
            if (Directory.Exists(folderDestination))
            {
                Directory.Delete(folderDestination, true);
            }
            Directory.CreateDirectory(folderDestination);

            var files = new List<string>();
            if (HotelBedsSettings.FilesChild.ContainsKey(resource) && HotelBedsSettings.FilesChild[resource][0] != HotelBedsSettings.ChildFiles.AllFiles)
            {
                foreach (var childFile in HotelBedsSettings.FilesChild[resource])
                {
                    files.Add(HotelBedsSettings.Files[childFile]);
                }
            }

            switch (compressionType)
            {
                case HotelBedsSettings.CompressionType.Zip:
                    try
                    {
                        if (files.Count > 0)
                        {
                            using (var zipFile = new ZipFile(resourceStream))
                            {
                                foreach (var file in files)
                                {
                                    var zipEntry = zipFile.GetEntry(file);
                                    var strDestination = String.Concat(folderDestination, Path.GetFileName(file));
                                    using (var oWriter = new FileStream(strDestination, FileMode.Create))
                                    {
                                        zipFile.GetInputStream(zipEntry).CopyTo(oWriter);
                                    }
                                }
                            }
                        }
                        else
                        {
                            using (var zipFile = new ZipInputStream(resourceStream))
                            {
                                ZipEntry theEntry;
                                while ((theEntry = zipFile.GetNextEntry()) != null)
                                {
                                    if (!theEntry.IsFile)
                                    {
                                        continue;
                                    }
                                    var fileName = Path.GetFileName(theEntry.Name);
                                    var strDestination = String.Concat(folderDestination, fileName);
                                    using (var oWriter = new FileStream(strDestination, FileMode.Create))
                                    {
                                        zipFile.CopyTo(oWriter);
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ApplicationException($"Trying to unzip file '{resourceStream.Name}'", ex);
                    }
                    break;
            }
            Console.WriteLine("Uncompression End:   {0} ({1} -> {2})", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), Enum.GetName(typeof(HotelBedsSettings.CompressionType), compressionType), resourceName);
        }

        #endregion

        #region Load Resource

        public string FullFileLocation(string resourceName)
        {
            var fileName = Path.GetFileName(resourceName);
            var fileLocation = String.Concat(TempDirectory, HotelBedsSettings.FileCreatePrefix, fileName);
            return fileLocation;
        }

        public string FullUncompressedFolder(string resourceName)
        {
            var fileName = FullFileLocation(Path.GetFileNameWithoutExtension(resourceName));
            var directoryLocation = String.Concat(fileName, HotelBedsSettings.UncompressFolderSufix);
            return directoryLocation;
        }

        public void LoadData(ConcurrentDictionary<HotelBedsSettings.ReaderType, IHotelBedsDataLayer> dataBase, ArrayList resources, bool useThreads, bool loadFullContent)
        {
            var actions = new Dictionary<string, Action>();
            var threads = new Dictionary<string, Task>();

            var generalResources = ResourcesByLanguages(resources);

            foreach (var rscr in generalResources.Keys)
            {
                var resource = rscr;
                var fileLanguages = generalResources[resource];

                foreach (var rscrName in fileLanguages.Keys)
                {
                    var resourceName = rscrName;
                    actions.Add(resourceName, () => LoadDataThread(dataBase, resource, resourceName, loadFullContent));
                }
            }

            foreach (var actionName in actions.Keys)
            {
                if (Settings.Multithread && useThreads)
                {
                    var actionThread = actions[actionName];
                    var taskName = actionName;
                    var thread = new Task(() =>
                    {
                        Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;
                        Thread.CurrentThread.Name = taskName;
                        actionThread();
                    });
                    thread.Start();
                    threads.Add(taskName, thread);
                }
                else
                {
                    actions[actionName]();
                }
            }

            if (Settings.Multithread && useThreads)
            {
                //Wait for all threads to finish
                foreach (var taskName in threads.Keys)
                {
                    threads[taskName].Wait();
                }
            }
        }

        private void LoadDataThread(ConcurrentDictionary<HotelBedsSettings.ReaderType, IHotelBedsDataLayer> dataBase, Enum resource, string resourceName, bool loadFullContent)
        {
            Console.WriteLine("LoadData Start: {0} ({1})", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), resourceName);

            if (HotelBedsSettings.FilesCompression.ContainsKey(resource))
            {
                var filesStream = new Dictionary<string, FileStream>();
                var files = CacheDirectoryFileListManager(FullUncompressedFolder(resourceName));

                foreach (var file in files)
                {
                    var fileName = CacheFileNameManager(file, false);

                    var fileOk = false;
                    if (HotelBedsSettings.FilesChild.ContainsKey(resource) && !String.IsNullOrEmpty(fileName))
                    {
                        if (HotelBedsSettings.FilesChild[resource][0] == HotelBedsSettings.ChildFiles.AllFiles)
                        {
                            fileOk = true;
                        }
                        else
                        {
                            foreach (var childFile in HotelBedsSettings.FilesChild[resource])
                            {
                                if (HotelBedsSettings.Files.ContainsKey(childFile) && Path.GetFileName(HotelBedsSettings.Files[childFile]).Equals(fileName))
                                {
                                    fileOk = true;
                                    break;
                                }
                            }
                        }
                    }

                    if (fileOk)
                    {
                        var languageOk = false;

                        //Some files per language has the same name so we change the index (fileName)
                        var languageFileName = LanguageFileName(resource, resourceName, fileName);
                        if (languageFileName.Equals(fileName))
                        {
                            languageOk = true;
                        }
                        else
                        {
                            //Check file it is already loaded
                            try
                            {
                                using (var testOpen = new FileStream(file, FileMode.Open, FileAccess.Write, FileShare.None))
                                {
                                    languageOk = true;
                                }
                            }
                            catch
                            {
                                languageOk = false;
                            }
                        }

                        if (languageOk)
                        {
                            var fileStream = new FileStream(file, FileMode.Open, FileAccess.Read, FileShare.Read);
                            filesStream.Add(languageFileName, fileStream);
                        }
                    }
                }
                if (filesStream.Count > 0)
                {
                    ResourceLoadData(dataBase, resource, filesStream, loadFullContent);
                }
            }
            else
            {
                var strDestination = FullFileLocation(resourceName);
                var resourceStream = new FileStream(strDestination, FileMode.Open, FileAccess.Read, FileShare.Read);
                ResourceLoadData(dataBase, resource, new Dictionary<string, FileStream> { { resourceName, resourceStream } }, loadFullContent);
            }
            Console.WriteLine("LoadData End: {0} ({1})", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), resourceName);
        }

        private void ResourceLoadData(ConcurrentDictionary<HotelBedsSettings.ReaderType, IHotelBedsDataLayer> dataBase, Enum resource, Dictionary<string, FileStream> filesStream, bool loadFullContent)
        {
            foreach (var file in filesStream.Keys)
            {
                HotelBedsSettings.ReaderType reader;
                var readerFile = HotelBedsSettings.ReaderTypeFromFullFileName(file);
                var readerResource = HotelBedsSettings.ReaderTypeFromResource(resource);

                if (readerFile == HotelBedsSettings.ReaderType.Unknown && readerResource == HotelBedsSettings.ReaderType.Unknown)
                {
                    continue;
                }
                else if (readerFile != HotelBedsSettings.ReaderType.Unknown)
                {
                    reader = readerFile;
                }
                else
                {
                    reader = readerResource;
                }

                IHotelBedsDataLayer resourceData = null;
                if (!dataBase.ContainsKey(reader))
                {
                    switch (reader)
                    {
                        case HotelBedsSettings.ReaderType.Csv:
                            resourceData = new HotelBedsDataLayerCsv();
                            break;

                        case HotelBedsSettings.ReaderType.Excel:
                            resourceData = new HotelBedsDataLayerExcel();
                            break;

                        case HotelBedsSettings.ReaderType.Xml:
                            resourceData = new HotelBedsDataLayerXml();
                            break;

                        case HotelBedsSettings.ReaderType.Aif2:
                            resourceData = new HotelBedsDataLayerAif2();
                            break;
                    }
                    dataBase.TryAdd(reader, resourceData);
                }
                var count = 0;
                while (!dataBase.TryGetValue(reader, out resourceData) && count < 100)
                {
                    count++;
                    Thread.Sleep(100);
                }
                var fileLoaded = resourceData.LoadBasicDataFile(file, resource, filesStream[file]);

                if (fileLoaded && loadFullContent)
                {
                    resourceData.LoadDataFile(file);
                }
            }
        }

        #endregion

        #region Backup Aif

        public void ConfirmUpdate(string version)
        {
            if (!String.IsNullOrEmpty(Settings.Aif2Url))
            {
                var confirmResourceName = HotelBedsSettings.Files[HotelBedsSettings.ConfirmCacheFiles.ActiveInventoryFilesServices];
                var confirm = new HotelBedsConnectHttp();
                confirm.BaseUrl = Settings.Aif2Url;
                confirm.User = Settings.WebServiceUser;
                confirm.Password = Settings.WebServicePassword;
                confirm.Headers = new Dictionary<string, string>();
                confirm.Headers.Add("X-Version", version);
                confirm.GetResource(confirmResourceName, FullFileLocation(confirmResourceName));
            }
        }

        public string FileVersion(Enum resource)
        {
            string version;
            var fileVersionLocation = FullFileLocation(HotelBedsSettings.Files[resource]) + HotelBedsSettings.FileVersionSufix;
            if (File.Exists(fileVersionLocation))
            {
                var file = new StreamReader(new FileStream(fileVersionLocation, FileMode.Open, FileAccess.ReadWrite, FileShare.None));
                var fileContent = file.ReadLine();
                file.Close();
                var start = fileContent.IndexOf("-", fileContent.IndexOf(HotelBedsSettings.FileVersionChar) + 1) + 1;
                var end = fileContent.LastIndexOf('.');
                version = fileContent.Substring(start, end - start);
            }
            else
            {
                version = String.Empty;
            }
            return version;
        }

        public void BackupAif(Enum resource, string fileVersion)
        {
            if (!String.IsNullOrEmpty(Settings.Base.LocalTemp) && resource != null && !String.IsNullOrEmpty(fileVersion))
            {
                DirectoryInfo dirInfo = null;
                if (!Directory.Exists(Settings.Base.LocalTemp))
                {
                    dirInfo = Directory.CreateDirectory(Settings.Base.LocalTemp);
                }
                else
                {
                    dirInfo = new DirectoryInfo(Settings.Base.LocalTemp);
                }
                var resourceName = HotelBedsSettings.Files[resource];
                var file = FullFileLocation(resourceName);
                if (File.Exists(file))
                {
                    var fileLocation = String.Concat(dirInfo.FullName, HotelBedsSettings.FileCreatePrefix, Path.GetFileName(resourceName), fileVersion);
                    using (var fileReader = new FileStream(file, FileMode.Open, FileAccess.Read, FileShare.None))
                    {
                        using (var fileBackup = new FileStream(fileLocation, FileMode.Create, FileAccess.Write, FileShare.None))
                        {
                            fileReader.CopyTo(fileBackup);
                        }
                    }                    
                }
            }
        }

        #endregion

        #region CreateHotelsCache

        public void ProcessHotels(Root dataRoot, ConcurrentDictionary<HotelBedsSettings.ReaderType, IHotelBedsDataLayer> dataBase, Dictionary<string, Place> resortIndex, 
                                  Dictionary<string, Hotel> hotelIndex, bool v, Dictionary<string, int> _MappedHotels, Logger logger, Action<XML.HelpClasses.Log.MessageType, string> LogEntry, 
                                  HotelBedsCommonExporter exporter,
                                  int hotelsMapped, int hotelsUnMapped, string configurationContentType)
        {
            LogEntry(XML.HelpClasses.Log.MessageType.Informational, "Process CreateHotelsCache Start");
            ProcessHotel(dataRoot, dataBase, resortIndex, hotelIndex, _MappedHotels, logger, LogEntry, exporter, hotelsMapped, hotelsUnMapped, configurationContentType);
            LogEntry(XML.HelpClasses.Log.MessageType.Informational, "Process CreateHotelsCache End");

            if (Settings.Base.Build.typeSpecified && (Settings.Base.Build.type.Equals(Sunhotels.Export.ConfigurationBuildType.full) || Settings.Base.Build.type.Equals(Sunhotels.Export.ConfigurationBuildType.availability)))
            {
                var downloadedVersion = FileVersion(HotelBedsSettings.CacheFiles.ActiveInventoryFilesServices);

                //Confirm Update
                LogEntry(XML.HelpClasses.Log.MessageType.Informational, "Confirmation Start");
#if !DEBUG
                ConfirmUpdate(downloadedVersion);
#endif
                LogEntry(XML.HelpClasses.Log.MessageType.Informational, "Confirmation End");

                //Backup data
                LogEntry(XML.HelpClasses.Log.MessageType.Informational, "Process BackupAif Start");
                BackupAif(HotelBedsSettings.CacheFiles.ActiveInventoryFilesServices, downloadedVersion);
                LogEntry(XML.HelpClasses.Log.MessageType.Informational, "Process BackupAif End");
            }
        }

        private void ProcessHotel(Root dataRoot,
                        ConcurrentDictionary<HotelBedsSettings.ReaderType, IHotelBedsDataLayer> dataBase,
                        Dictionary<string, Place> resortIndex,
                        Dictionary<string, Hotel> hotelIndex,
                        Dictionary<string, int> mappedHotels,
                        XML.HelpClasses.Logger Logger,
                        Action<XML.HelpClasses.Log.MessageType, string> LogEntry,
                        HotelBedsCommonExporter exporter,
                        int hotelsMapped, int hotelsUnMapped, string configurationContentType)
        {
            var canProcessCache = Settings.Base.Build.typeSpecified && (Settings.Base.Build.type.Equals(Sunhotels.Export.ConfigurationBuildType.full) || Settings.Base.Build.type.Equals(Sunhotels.Export.ConfigurationBuildType.availability));
            var canProcessStatic = Settings.Base.Build.typeSpecified && (Settings.Base.Build.type.Equals(Sunhotels.Export.ConfigurationBuildType.full) || Settings.Base.Build.type.Equals(Sunhotels.Export.ConfigurationBuildType.@static));

            var functions = new HotelBedsCommonFunctions();
            var roomTypes = new ConcurrentDictionary<string, RoomType>();

            var todayDate = DateTime.Today;
            var cacheIni = todayDate.AddDays(Settings.Base.ReleaseDaysAdded.HasValue ? Settings.Base.ReleaseDaysAdded.Value : 0);
            var cacheEnd = cacheIni.AddDays(Settings.Base.NrDaysInCache);

            Dictionary<string, decimal> exchangeRate = null;
            ConcurrentDictionary<string, ConcurrentDictionary<string, ConcurrentBag<string>>> logList = null;
            ConcurrentBag<string> errorLog = null;
            ConcurrentDictionary<string, CancellationPolicy> cancellationPolicies = null;
            Dictionary<string, List<string>> hotelsContracts = null;
            string resourceDirectory = null;
            IHotelBedsDataLayer dataLayer = null;
            var resourceAif2 = HotelBedsSettings.CacheFiles.ActiveInventoryFilesServices; //HotelBedsSettings.UpdateCacheFiles.ActiveInventoryFilesServices
            
            var hotelsProcessed = 0;
            var hotelsWithoutContract = 0;
            var hotelsDropped = 0;
            var maxDeadlineHours = Settings.Base.NrDaysInCache * 24;

            if (canProcessCache)
            {
                var resourceNameAif2 = HotelBedsSettings.Files[resourceAif2];
                var readerType = HotelBedsSettings.ReaderTypeFromResource(resourceAif2);
                resourceDirectory = FullUncompressedFolder(resourceNameAif2);

                LoadData(dataBase, new ArrayList() { resourceAif2 }, Settings.Multithread, false);

                if (!dataBase.TryGetValue(readerType, out dataLayer))
                {
                    Logger.RecordMessage("ERROR: datalayer is NULL for " + readerType.ToString() + "!!!!", XML.HelpClasses.Log.MessageType.Error);
                    throw new ApplicationException("Unexpected error: datalayer is null for " + readerType.ToString());
                }

                logList = new ConcurrentDictionary<string, ConcurrentDictionary<string, ConcurrentBag<string>>>();
                errorLog = new ConcurrentBag<string>();
                cancellationPolicies = new ConcurrentDictionary<string, CancellationPolicy>();
                
                exchangeRate = functions.GetExchangeRate(Settings);

                Logger.RecordMessage("CreateHotelsCache: Loading contracts", XML.HelpClasses.Log.MessageType.Informational);

                //HotelBeds can create multiple contract files per hotel, we group them
                hotelsContracts = new Dictionary<string, List<string>>();
                var hotelsFiltered = functions.HotelDestinationFilter(Settings);
                foreach (var fileName in dataLayer.FileHotels.Keys)
                {
                    List<string> hotels;
                    dataLayer.FileHotels.TryGetValue(fileName, out hotels);
                    foreach (var hotelId in hotels)
                    {
                        if (hotelsFiltered.Count > 0 && !hotelsFiltered.Contains(hotelId))
                        {
                            continue;
                        }
                        if (!hotelsContracts.ContainsKey(hotelId))
                        {
                            hotelsContracts.Add(hotelId, new List<string>());
                        }
                        List<string> fileNames;
                        hotelsContracts.TryGetValue(hotelId, out fileNames);
                        if (!fileNames.Contains(fileName))
                        {
                            fileNames.Add(fileName);
                        }
                    }
                }
            }

            var resourceContents = HotelBedsSettings.StaticFiles.Contents;
            Dictionary<string, string> hotelFiles = null;
            Dictionary<string, string> hotelFileNames = null;
            IHotelBedsDataLayer staticDataLayer = null;
            Dictionary<string, string> rDescriptionsIndex = null;
            Dictionary<string, string> rCharacteristicIndex = null;
            ConcurrentDictionary<string, string> roomTypeDescription = null;
            Dictionary<string, Dictionary<HotelBedsSettings.Languages, string>> hIssueIndex = null;
            if (canProcessStatic)
            {
                hotelFiles = new Dictionary<string, string>();
                hotelFileNames = new Dictionary<string, string>();

                var resourceExtend = HotelBedsSettings.StaticFiles.Extended;
                var resourceNameContents = HotelBedsSettings.LanguageFilesName[resourceContents][MainLanguage];

                LoadData(dataBase, new ArrayList() { resourceExtend }, Settings.Multithread, true);
                var extendedTables = GetLanguageTable(dataBase, resourceExtend, null);

                var roomDescriptionsTableName = Path.GetFileNameWithoutExtension(HotelBedsSettings.Files[HotelBedsSettings.ChildFiles.RoomDescriptions]);
                var roomDescriptions = extendedTables[MainLanguage][roomDescriptionsTableName];
                var roomCharacteristicsTableName = Path.GetFileNameWithoutExtension(HotelBedsSettings.Files[HotelBedsSettings.ChildFiles.CharacteristicDescriptions]);
                var roomCharacteristics = extendedTables[MainLanguage][roomCharacteristicsTableName];
                rDescriptionsIndex = new Dictionary<string, string>();
                rCharacteristicIndex = new Dictionary<string, string>();
                roomTypeDescription = new ConcurrentDictionary<string, string>();
                if (roomDescriptions.Rows.Count > 0 && roomCharacteristics.Rows.Count > 0)
                {
                    foreach (var rd in roomDescriptions.Rows)
                    {
                        if (!rDescriptionsIndex.ContainsKey(rd["0"]) && rd["1"].Equals(HotelBedsSettings.LanguageExternalCode[MainLanguage]))
                        {
                            rDescriptionsIndex.Add(rd["0"], rd["3"]);
                        }
                    }
                    foreach (var cd in roomCharacteristics.Rows)
                    {
                        if (!rCharacteristicIndex.ContainsKey(cd["0"]) && cd["1"].Equals(HotelBedsSettings.LanguageExternalCode[MainLanguage]))
                        {
                            rCharacteristicIndex.Add(cd["0"], cd["2"]);
                        }
                    }
                }

                var hotelIssuesTableName = Path.GetFileNameWithoutExtension(HotelBedsSettings.Files[HotelBedsSettings.ChildFiles.HotelIssues]);
                var hotelIssues = extendedTables[MainLanguage][hotelIssuesTableName];
                hIssueIndex = new Dictionary<string, Dictionary<HotelBedsSettings.Languages, string>>();
                foreach (var issue in hotelIssues.Rows)
                {
                    var hotelIssue = issue["0"];
                    if (hotelIndex.ContainsKey(hotelIssue))
                    {
                        //We include the issue in the hotel description until it ends
                        var startDate = DateTime.ParseExact(issue["2"], "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture);
                        var endDate = DateTime.ParseExact(issue["3"], "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture);
                        if (DateTime.Compare(DateTime.Today, endDate) <= 0)
                        {
                            if (!hIssueIndex.ContainsKey(hotelIssue))
                            {
                                hIssueIndex.Add(hotelIssue, new Dictionary<HotelBedsSettings.Languages, string>());
                            }
                            foreach (var lang in Languages)
                            {
                                if (issue["4"].Equals(HotelBedsSettings.LanguageExternalCode[lang]))
                                {
                                    var issueStr = "(" + startDate.ToString("yyyy-MM-dd") + " -> " + endDate.ToString("yyyy-MM-dd") + ": " + issue["5"] + ")";
                                    if (!hIssueIndex[hotelIssue].ContainsKey(lang))
                                    {
                                        //First issue
                                        hIssueIndex[hotelIssue].Add(lang, issueStr);
                                    }
                                    else
                                    {
                                        //Other issues
                                        hIssueIndex[hotelIssue][lang] = hIssueIndex[hotelIssue][lang] + Environment.NewLine + issueStr;
                                    }
                                    break;
                                }
                            }
                        }
                    }
                }

                LoadData(dataBase, new ArrayList() { resourceContents }, Settings.Multithread, false);
                var files = CacheDirectoryFileListManager(FullUncompressedFolder(resourceNameContents));
                foreach (var file in files)
                {
                    var readerType = HotelBedsSettings.ReaderTypeFromFullFileName(file);
                    var fileName = LanguageFileName(resourceContents, resourceNameContents, CacheFileNameManager(file, false));
                    List<string> hotelList = null;
                    staticDataLayer = dataBase[readerType];
                    staticDataLayer.FileHotels.TryGetValue(fileName, out hotelList);

                    foreach (var hotelId in hotelList)
                    {
                        if (hotelIndex.ContainsKey(hotelId))
                        {
                            hotelFiles.Add(hotelId, file);
                            hotelFileNames.Add(hotelId, fileName);
                        }
                    }
                }
            }
            var parallelOptions = new ParallelOptions();
            if (!Settings.Multithread || !Settings.CommonThreads.HasValue)
            {
                parallelOptions.MaxDegreeOfParallelism = 1;
            }
            else
            {
                parallelOptions.MaxDegreeOfParallelism = Settings.CommonThreads.Value;
            }

            var indexHotel = new ConcurrentDictionary<string, Hotel>(hotelIndex);
            var hotelsIndexed = hotelIndex.Count;
            hotelIndex.Clear();

            Parallel.ForEach(indexHotel.Keys, parallelOptions, hotelId =>
            {
                Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;

                Hotel hotel;
                if (indexHotel.TryRemove(hotelId, out hotel))
                {
                    if (canProcessCache)
                    {
                        if (!hotelsContracts.ContainsKey(hotel.Id))
                        {
                            functions.LogLine(logList, hotel.Id + ":" + hotel.Name, "Hotel without contract downloaded from hotelbeds", null);
                            Interlocked.Increment(ref hotelsWithoutContract);
                            return;
                        }
                        ProcessHotelCache(hotel, mappedHotels, resourceAif2, resourceDirectory, hotelsContracts, dataBase, dataLayer, functions, logList, errorLog, todayDate, cacheIni, cacheEnd, 
                                            roomTypes, cancellationPolicies, exchangeRate, maxDeadlineHours, 
                                            ref hotelsProcessed, ref hotelsDropped);

                        if (functions.CheckHotelRooms(hotel))
                        {
                            functions.LogLine(logList, hotel.Id + ":" + hotel.Name, "Hotel dropped", "Hotel without rooms or roomgroups");
                            Interlocked.Increment(ref hotelsDropped);
                            return;
                        }
                    }

                    var roomList = new List<Room>();
                    roomList.AddRange(hotel.Rooms.Values);
                    if (hotel.RoomGroups.Count > 0)
                    {
                        foreach (var roomGroup in hotel.RoomGroups.Values)
                        {
                            roomList.AddRange(roomGroup.Rooms.Values);
                        }
                    }

                    if (canProcessStatic)
                    {
                        ProcessHotelStatic(hotel, resourceContents, hotelFiles, hotelFileNames, dataBase, staticDataLayer, functions, errorLog, hIssueIndex, roomTypes, rDescriptionsIndex, rCharacteristicIndex, roomTypeDescription, roomList, dataRoot);
                    }

                    exporter.WriteHotel(roomTypes, hotel, roomList);
                }
            });

            if (canProcessCache)
            {
                Logger.RecordMessage("CreateHotelsCache: Setting roomtypes", XML.HelpClasses.Log.MessageType.Informational);

                //Set RoomTypes
                foreach (var rt in roomTypes)
                {
                    dataRoot.RoomTypes.Add(rt.Key, rt.Value);
                }

                Logger.RecordMessage("CreateHotelsCache: Setting cancellation policies", XML.HelpClasses.Log.MessageType.Informational);

                //Set Cancellation policies
                foreach (var cp in cancellationPolicies)
                {
                    dataRoot.CancellationsPolicies.Add(cp.Key, cp.Value);
                }

                Console.WriteLine("Writting log start");
                //Record Log
                //LogEntry(Log.MessageType.Informational, "Contracts Processed: " + contractsProcessed);
                //LogEntry(Log.MessageType.Informational, "Contracts Dropped: " + contractsDropped);
                LogEntry(Log.MessageType.Informational, "Hotels in hotelbed file: " + (hotelsMapped + hotelsUnMapped));
                LogEntry(Log.MessageType.Informational, ">> Hotels mapped: " + hotelsMapped);
                LogEntry(Log.MessageType.Informational, ">> Hotels unmapped: " + hotelsUnMapped);
                LogEntry(Log.MessageType.Informational, "Hotels Checked (" + configurationContentType + "):" + hotelsIndexed);
                LogEntry(Log.MessageType.Informational, ">> Hotels Cache Imported OK: " + hotelsProcessed);
                LogEntry(Log.MessageType.Informational, ">> Hotels without contract: " + hotelsWithoutContract);
                LogEntry(Log.MessageType.Informational, ">> Hotels Dropped: " + hotelsDropped);

                foreach (var reason in logList.Keys)
                {
                    var sb = new System.Text.StringBuilder();
                    sb.AppendLine("REASON >> " + reason);
                    foreach (var reasonValue in logList[reason].Keys)
                    {
                        sb.AppendLine(" >> " + reasonValue);
                        foreach (var item in logList[reason][reasonValue].Distinct())
                        {
                            sb.AppendLine(" >>>> " + item);
                        }
                    }
                    LogEntry(Log.MessageType.Warning, sb.ToString());
                }
                foreach (var msg in errorLog)
                {
                    LogEntry(Log.MessageType.Error, msg);
                }
                Console.WriteLine("Writting log end");
            }

            foreach (var p in resortIndex.Values)
            {
                if (!p.Id.Contains(HotelBedsSettings.PlaceSeparator.ToString()))
                {
                    dataRoot.Places.Add(p);
                }
            }
        }
        private void ProcessHotelCache(Hotel hotel,
            Dictionary<string, int> mappedHotels,
            HotelBedsSettings.CacheFiles resourceAif2,
            string resourceDirectory,
            Dictionary<string, List<string>> hotelsContracts,
            ConcurrentDictionary<HotelBedsSettings.ReaderType, IHotelBedsDataLayer> dataBase,
            IHotelBedsDataLayer dataLayer,
            HotelBedsCommonFunctions functions,
            ConcurrentDictionary<string, ConcurrentDictionary<string, ConcurrentBag<string>>> logList,
            ConcurrentBag<string> errorLog,
            DateTime todayDate,
            DateTime cacheIni,
            DateTime cacheEnd,
            ConcurrentDictionary<string, RoomType> roomTypes,
            ConcurrentDictionary<string, CancellationPolicy> cancellationPolicies,
            Dictionary<string, decimal> exchangeRate,
            int maxDeadlineHours,
            ref int hotelsProcessed,
            ref int hotelsDropped)
        {
            //Important sort by contract file
            List<string> fileNames;
            if (!hotelsContracts.TryGetValue(hotel.Id, out fileNames))
            {
                return;
            }
            fileNames.Sort();

            //If it's an unmapped file then we keep only ONE rate per room (not available for booking but for mapping)
            var oneRateNotMapped = Settings.Base.Content.type.Equals(ConfigurationContentType.unmapped)
                                    && mappedHotels != null
                                    && !mappedHotels.ContainsKey(hotel.Id);

            var infantRestrictions = false;
            var adultOnly = true;

            var baseRoomTypes = new Dictionary<string, RoomType>();
            var baseRoomGroups = new Dictionary<string, RoomGroup>();
            var baseRooms = new Dictionary<string, Room>();
            var baseCancellationPolicies = new Dictionary<string, CancellationPolicy>();
            var baseRoomsIndex = new Dictionary<string, List<Room>>();

            foreach (var fileName in fileNames)
            {
                var fullLocation = resourceDirectory + fileName;
                if (!LanguageLoadData(dataLayer, resourceAif2, fullLocation))
                {
                    errorLog.Add("HotelBeds hotel code " + hotel.Id + ": Can't read contract file " + fileName);
                    continue;
                }

                var cacheTables = GetLanguageTable(dataBase, resourceAif2, fileName);
                var prefixTableName = Path.GetFileNameWithoutExtension(fileName) + HotelBedsSettings.Aif2TableCharSeparator;

                var hotelIdName = String.Join(HotelBedsSettings.Aif2FieldCharSeparator.ToString(), hotel.Id, hotel.Name);

                var officeCode = String.Empty;
                var contractNumber = String.Empty;
                var companyCode = String.Empty;

                var contractMarkets = new List<string>();
                var baseRoomsCapacity = new Dictionary<string, int>();
                var baseRoomsPrice = new Dictionary<string, Dictionary<DateTime, decimal?[]>>();
                var baseRoomsPriceIsPerPax = new Dictionary<string, Dictionary<DateTime, bool>>();
                var baseRoomsPriceRate = new Dictionary<string, Dictionary<DateTime, string>>();
                var baseRoomsPriceBoard = new Dictionary<string, Dictionary<DateTime, Dictionary<string, Dictionary<string, decimal?[]>>>>();
                var baseRoomsPriceBoardIsPerPax = new Dictionary<string, Dictionary<DateTime, Dictionary<string, Dictionary<string, bool>>>>();

                var distinctCombinations = new Dictionary<string, List<string>>();
                var splittedCombinations = new Dictionary<string, Dictionary<string, string>>();
                var roomRatesList = new List<string>();
                var roomPriceRateComb = new Dictionary<string, bool>();
                var pricesLine = new Dictionary<string, Dictionary<DateTime, string[]>>();
                var boardsList = new List<string>();
                var combOffers = new[] { new List<object>(), new List<object>(), new List<object>(), new List<object>() };
                var sortedRates = new SortedList<int, Dictionary<string, string>>();

                try
                {

                    #region Load contract and data tables

                    var contractTable = cacheTables[MainLanguage][prefixTableName + "CONTRACT HEADER"];
                    var rateCodesTable = cacheTables[MainLanguage][prefixTableName + "RATE CODES"];
                    var roomTypesTable = cacheTables[MainLanguage][prefixTableName + "ROOM TYPES"];
                    var inventoryTable = cacheTables[MainLanguage][prefixTableName + "INVENTORY"];
                    var stopSalesTable = cacheTables[MainLanguage][prefixTableName + "STOP SALES"];
                    var minimumMaximumStayTable = cacheTables[MainLanguage][prefixTableName + "MINIMUM AND MAXIMUM STAY"];
                    var checkInCheckOutTable = cacheTables[MainLanguage][prefixTableName + "CHECK IN AND CHECK OUT"];
                    var pricesTable = cacheTables[MainLanguage][prefixTableName + "PRICES"];
                    var boardsTable = cacheTables[MainLanguage][prefixTableName + "BOARD SUPPLEMENTS AND DISCOUNTS"];
                    var combinableOffersTable = cacheTables[MainLanguage][prefixTableName + "COMBINABLE OFFERS"];
                    var supplementsTable = cacheTables[MainLanguage][prefixTableName + "SUPPLEMENTS AND DISCOUNTS"];
                    var freesTable = cacheTables[MainLanguage][prefixTableName + "FREES"];
                    //var noHotelContractsTable = cacheTables[MainLanguage][prefixTableName + "NO HOTEL CONTRACTS"];
                    var cancellationFeesTable = cacheTables[MainLanguage][prefixTableName + "CANCELLATION FEES"];
                    //var promotionsTable = cacheTables[MainLanguage][prefixTableName + "PROMOTIONS"];
                    var handlingFeesTable = cacheTables[MainLanguage][prefixTableName + "HANDLING FEES"];
                    //var taxBreakdownTable = cacheTables[MainLanguage][prefixTableName + "TAX BREAKDOWN"];
                    var marketsTable = cacheTables[MainLanguage][prefixTableName + "VALID MARKETS"];
                    //var suppliersAvailabilityPricesTable = cacheTables[MainLanguage][prefixTableName + "SUPPLIERS INTEGRATION AVAILABILITY AND PRICES"];
                    //var suppliersCancellationFeesITable = cacheTables[MainLanguage][prefixTableName + "SUPPLIERS INTEGRATION CANCELLATION FEES"];
                    //var suppliersInventoryTable = cacheTables[MainLanguage][prefixTableName + "SUPPLIERS INTEGRATION INVENTORY"];
                    //var suppliersMinimumStayTable = cacheTables[MainLanguage][prefixTableName + "SUPPLIERS INTEGRATION MINIMUM STAY"];

                    var contractLine = contractTable.Rows[0];

                    #region Check empty tables
                    if (roomTypesTable.Rows.Count == 0)
                    {
                        functions.LogLine(logList, hotelIdName, "No room types", "roomTypesTable.Rows.Count == 0");
                        throw new ApplicationException("No room types");
                    }
                    if (inventoryTable.Rows.Count == 0)
                    {
                        functions.LogLine(logList, hotelIdName, "No availability", "inventoryTable.Rows.Count == 0");
                        throw new ApplicationException("No availability");
                    }
                    if (pricesTable.Rows.Count == 0)
                    {
                        functions.LogLine(logList, hotelIdName, "No prices", "pricesTable.Rows.Count == 0");
                        throw new ApplicationException("No prices");
                    }
                    #endregion

                    officeCode = contractLine["Office code"];
                    contractNumber = contractLine["Contract number"];
                    companyCode = contractLine["Company code"];

                    var contractRoomId = functions.CreateContractRoomId(officeCode, contractNumber, companyCode);
                    var contractName = contractLine["Contract name"];
                    var currency = contractLine["Currency"];
                    var fixRate = contractLine["Fix rate"].Equals("Y");
                    var contractBoard = contractLine["Base board"];
                    var opaque = contractLine["Opaque"];
                    var nonRefundable = !String.IsNullOrEmpty(contractLine["Classification"]) && contractLine["Classification"].Equals("NRF");
                    var contractHasChilds = HotelBedsSettings.SunHotelsChildAgeBooking <= Int32.Parse(contractLine["Maximum child age"]);
                    var contractRoomIdType = functions.CreateContractRoomIdType(nonRefundable, opaque);

                    var txtContractIniDate = contractLine["Initial date"];
                    var txtContractEndDate = contractLine["End date"];
                    var contractIniDate = functions.ParseDate(txtContractIniDate);
                    var contractEndDate = functions.ParseDate(txtContractEndDate);
                    var contractDates = functions.ListValidDates(contractIniDate, contractEndDate, cacheIni, cacheEnd, null);

                    #endregion

                    #region General validation of the contract files

                    var externalInventory = contractLine["External Inventory"];
                    var typeOfService = contractLine["Type of service"];
                    var noHotel = contractLine["No hotel"];
                    var paymentModel = contractLine["Payment model"];
                    var contractType = contractLine["Contract type"];
                    var sellingPrice = contractLine["Selling price"];
                    var dailyPrice = contractLine["Daily price"];

                    var checkInventory = externalInventory.Equals("N"); //No external inventory. Just HotelBeds direct contracts
                    var checkTypeOfService = typeOfService.Equals("H"); //Hotels only
                    var checkNoHotel = noHotel.Equals("N"); //Refers to "Roulette contract"
                    var checkExchangeRate = exchangeRate.ContainsKey(currency); //Check if we have the currency of this contract
                    var checkPaymentModel = paymentModel.Equals("M"); //"Merchant model" Net price.
                    var checkBoard = HotelBedsSettings.MapMeals.ContainsKey(contractBoard); //Check if we have this base board
                    var checkDates = functions.ValidateDates(contractIniDate, contractEndDate, cacheIni, cacheEnd); //Check for valid dates
                    var checkOpaque = (String.IsNullOrEmpty(opaque) || (!String.IsNullOrEmpty(opaque) && (opaque.Equals("P") || opaque.Equals("B") || opaque.Equals("H")))); //Validate opaque values
                    var checkContractType = contractType.Equals("U"); //Validate values
                    var checkSellingPrice = sellingPrice.Equals("N"); //Prices are not mandatory as the final price when sold to public
                    var checkDailyPrice = dailyPrice.Equals("N"); //Price is extracted from each night of the stay (and not the first night of stay and multiplied by the number of nights)

                    if (
                        checkInventory && checkTypeOfService && checkNoHotel && checkExchangeRate && checkPaymentModel && checkBoard
                        && checkDates && checkOpaque && checkContractType && checkSellingPrice && checkDailyPrice
                    )
                    {
                        if (fixRate && rateCodesTable.Rows.Count == 0)
                        {
                            functions.LogLine(logList, hotelIdName, "No rate codes", "fixRate && rateCodesTable.Rows.Count == 0");
                            throw new ApplicationException("No rate codes");
                        }
                    }
                    else
                    {
                        const string reason = "Contract not valid - ";
                        if (!checkInventory) functions.LogLine(logList, hotelIdName, reason + "external inventory not supported", externalInventory);
                        if (!checkTypeOfService) functions.LogLine(logList, hotelIdName, reason + "type of service not supported", typeOfService);
                        if (!checkNoHotel) functions.LogLine(logList, hotelIdName, reason + "no hotel code specified", noHotel);
                        if (!checkExchangeRate) functions.LogLine(logList, hotelIdName, reason + "currency not supported", currency);
                        if (!checkPaymentModel) functions.LogLine(logList, hotelIdName, reason + "payment model other than net price", paymentModel);
                        if (!checkBoard) functions.LogLine(logList, hotelIdName, reason + "base board not found", contractBoard);
                        if (!checkDates) functions.LogLine(logList, hotelIdName, reason + "invalid dates", "contractIniDate " + contractIniDate.ToShortDateString() + ", contractEndDate " + contractEndDate.ToShortDateString() + ", cacheIni " + cacheIni.ToShortDateString() + ", cacheEnd " + cacheEnd.ToShortDateString());
                        if (!checkOpaque) functions.LogLine(logList, hotelIdName, reason + "opaque code", opaque);
                        if (!checkContractType) functions.LogLine(logList, hotelIdName, reason + "contract type not supported", contractType);
                        if (!checkSellingPrice) functions.LogLine(logList, hotelIdName, reason + "selling price is mandatory", sellingPrice);
                        if (!checkDailyPrice) functions.LogLine(logList, hotelIdName, reason + "checkin price per entire stay not supported", dailyPrice);

                        //The 'continue' still executes the finally statement and disposes the contract file
                        continue;
                    }

                    #endregion

                    var tables = new HotelBedsCommonTables(Settings, functions, todayDate);

                    tables.TableMarkets(marketsTable, contractMarkets);

                    tables.TableRates(rateCodesTable, fixRate, sortedRates, roomRatesList);

                    tables.TableBoards(boardsTable, boardsList, contractBoard);

                    tables.TableRoomPriceRate(pricesTable, logList, hotelIdName, distinctCombinations, splittedCombinations, sortedRates, roomTypesTable, boardsList, fixRate, contractBoard, contractDates, roomPriceRateComb, pricesLine, contractMarkets);

                    tables.TableRooms(roomTypesTable, logList, hotelIdName, ref infantRestrictions, ref adultOnly, contractHasChilds, fixRate, contractRoomId, contractRoomIdType, companyCode, nonRefundable, roomRatesList, roomPriceRateComb, baseRoomTypes, baseRoomGroups, baseRooms, baseRoomsCapacity, contractMarkets);

                    tables.TableAvailability(inventoryTable, fixRate, contractRoomId, contractRoomIdType, contractDates, baseRoomsIndex, baseRoomGroups, baseRooms, contractMarkets);

                    tables.TablePrices(logList, hotelIdName, distinctCombinations, splittedCombinations, sortedRates, roomTypesTable, boardsList, fixRate, contractRoomId, contractRoomIdType, contractBoard, contractDates, baseRoomsIndex, baseRoomTypes, baseRoomGroups, baseRooms, baseRoomsPrice, baseRoomsPriceIsPerPax, baseRoomsPriceRate, pricesLine, contractMarkets);

                    if (!oneRateNotMapped)
                    {
                        tables.TableBoardSupplements(boardsTable, logList, hotelIdName, distinctCombinations, splittedCombinations, sortedRates, roomTypesTable, boardsList, fixRate, contractRoomId, contractRoomIdType, contractBoard, cacheIni, cacheEnd, contractDates, baseRoomsIndex, baseRoomTypes, baseRoomGroups, baseRooms, baseRoomsCapacity, baseRoomsPrice, baseRoomsPriceIsPerPax, baseRoomsPriceRate, baseRoomsPriceBoard, baseRoomsPriceBoardIsPerPax, contractMarkets);

                        tables.TableStopSales(stopSalesTable, logList, hotelIdName, distinctCombinations, splittedCombinations, sortedRates, roomTypesTable, boardsList, fixRate, contractRoomId, contractRoomIdType, cacheIni, cacheEnd, contractDates, baseRoomsIndex, baseRoomGroups, baseRooms, contractMarkets);

                        tables.TableMinimumMaximumStay(minimumMaximumStayTable, logList, hotelIdName, distinctCombinations, splittedCombinations, sortedRates, roomTypesTable, boardsList, fixRate, contractRoomId, contractRoomIdType, cacheIni, cacheEnd, contractDates, baseRoomsIndex, baseRoomGroups, baseRooms, contractMarkets);

                        tables.TableCheckInCheckOut(checkInCheckOutTable, logList, hotelIdName, distinctCombinations, splittedCombinations, sortedRates, roomTypesTable, boardsList, fixRate, contractRoomId, contractRoomIdType, cacheIni, cacheEnd, contractDates, baseRoomsIndex, baseRoomGroups, baseRooms, contractMarkets);

                        tables.TableCombinableOffers(combinableOffersTable, combOffers, supplementsTable, freesTable);

                        tables.TableSupplementsAndFrees(supplementsTable, logList, hotelIdName, distinctCombinations, splittedCombinations, sortedRates, roomTypesTable, boardsList, fixRate, contractRoomId, contractRoomIdType, opaque, cacheIni, cacheEnd, contractDates, baseRoomsIndex, baseRoomTypes, baseRoomGroups, baseRooms, baseRoomsCapacity, baseRoomsPrice, baseRoomsPriceIsPerPax, baseRoomsPriceRate, baseRoomsPriceBoard, baseRoomsPriceBoardIsPerPax, combOffers, contractMarkets, freesTable);

                        tables.TableCancellationFees(cancellationFeesTable, logList, hotelIdName, fixRate, contractRoomId, contractRoomIdType, cacheIni, cacheEnd, contractDates, baseRoomsIndex, baseRoomGroups, baseRooms, baseCancellationPolicies, maxDeadlineHours);

                        tables.TableHandlingFees(handlingFeesTable, logList, hotelIdName, distinctCombinations, splittedCombinations, sortedRates, roomTypesTable, boardsList, fixRate, contractRoomId, contractRoomIdType, cacheIni, cacheEnd, contractDates, baseRoomsIndex, baseRoomGroups, baseRooms, contractMarkets);
                    }

                    #region Apply Exchange Rate or Round and verify prices less than zero

                    var exRate = exchangeRate[currency];
                    if (exRate != Decimal.One)
                    {
                        var roomList = functions.RoomList(baseRooms, baseRoomGroups, contractRoomId, baseRoomsIndex);

                        foreach (var room in roomList)
                        {
                            foreach (var availabilityPeriod in room.AvailabilityPeriods.Values)
                            {
                                availabilityPeriod.Pricing_BasePrice = functions.ApplyExchangeRate(availabilityPeriod.Pricing_BasePrice, exRate);
                                availabilityPeriod.Pricing_ExtrabedAdult = functions.ApplyExchangeRate(availabilityPeriod.Pricing_ExtrabedAdult, exRate);
                                availabilityPeriod.Pricing_ExtrabedChild = functions.ApplyExchangeRate(availabilityPeriod.Pricing_ExtrabedChild, exRate);

                                foreach (var additionalBoard in availabilityPeriod.AdditionalBoards)
                                {
                                    additionalBoard.Adult = functions.ApplyExchangeRate(additionalBoard.Adult, exRate);
                                    additionalBoard.Child = functions.ApplyExchangeRate(additionalBoard.Child, exRate);
                                }
                            }
                        }
                    }
                    else
                    {
                        var roomList = functions.RoomList(baseRooms, baseRoomGroups, contractRoomId, baseRoomsIndex);

                        foreach (var room in roomList)
                        {
                            foreach (var availabilityPeriod in room.AvailabilityPeriods.Values)
                            {
                                availabilityPeriod.Pricing_BasePrice = functions.ApplyRoundPrice(availabilityPeriod.Pricing_BasePrice);
                                availabilityPeriod.Pricing_ExtrabedAdult = functions.ApplyRoundPrice(availabilityPeriod.Pricing_ExtrabedAdult);
                                availabilityPeriod.Pricing_ExtrabedChild = functions.ApplyRoundPrice(availabilityPeriod.Pricing_ExtrabedChild);

                                foreach (var additionalBoard in availabilityPeriod.AdditionalBoards)
                                {
                                    additionalBoard.Adult = functions.ApplyRoundPrice(additionalBoard.Adult);
                                    additionalBoard.Child = functions.ApplyRoundPrice(additionalBoard.Child);
                                }
                            }
                        }
                    }

                    #endregion

                    #region Add 'contractName' to the 'BookableBoardId' for the availability and booking xml integration

                    //Any change of the room id structure must be done in the availability and booking xml integration and in the DB sp_UpdateDefaultMapping ('%:[G0-9]%:[YN]:[YN]:[0-9]%:[0-9]%'), sp_GetHotelDetails, sp_ImportRoom, HotelImportControl.ascx (opaque), AutoMapping (JOB)
                    foreach (var room in functions.RoomList(baseRooms, baseRoomGroups, contractRoomId, baseRoomsIndex))
                    {
                        foreach (var availabilityPeriod in room.AvailabilityPeriods.Values)
                        {
                            availabilityPeriod.Pricing_BookableBoardId += HotelBedsSettings.RoomCharSeparator + contractName;
                            foreach (var additionalBoard in availabilityPeriod.AdditionalBoards)
                            {
                                additionalBoard.BookableBoardId += HotelBedsSettings.RoomCharSeparator + contractName;
                            }
                        }
                    }

                    #endregion

                }
                catch (Exception ex)
                {

                    var errorMsg = "HotelBeds hotel code: " + hotel.Id
                            + Environment.NewLine + "FileName: " + fileName
                            + Environment.NewLine + "Error: " + ex.Message;

                    if (!(ex.GetType() == typeof(ApplicationException)))
                    {
                        errorMsg += Environment.NewLine + ex.StackTrace;
                    }
                    errorLog.Add(errorMsg);

                    #region Delete contract rooms

                    if (!String.IsNullOrEmpty(officeCode) && !String.IsNullOrEmpty(contractNumber) && !String.IsNullOrEmpty(companyCode))
                    {
                        var roomIdList = new List<string>();
                        var contractRoomId = functions.CreateContractRoomId(officeCode, contractNumber, companyCode);
                        var roomId = contractRoomId + HotelBedsSettings.RoomCharSeparator;

                        foreach (var roomKey in baseRooms.Keys)
                        {
                            if (roomKey.StartsWith(roomId) && !roomIdList.Contains(roomId))
                            {
                                roomIdList.Add(roomKey);
                            }
                        }
                        foreach (var roomKey in baseRoomGroups.Keys)
                        {
                            if (roomKey.StartsWith(roomId) && !roomIdList.Contains(roomId))
                            {
                                roomIdList.Add(roomKey);
                            }
                        }
                        foreach (var roomKey in baseRoomsIndex.Keys)
                        {
                            if (roomKey.StartsWith(roomId) && !roomIdList.Contains(roomId))
                            {
                                roomIdList.Add(roomKey);
                            }
                        }
                        foreach (var roomKey in roomIdList)
                        {
                            if (baseRooms.ContainsKey(roomKey))
                            {
                                baseRooms.Remove(roomKey);
                            }
                            if (baseRoomGroups.ContainsKey(roomKey))
                            {
                                baseRoomGroups.Remove(roomKey);
                            }
                            if (baseRoomsIndex.ContainsKey(roomKey))
                            {
                                baseRoomsIndex.Remove(roomKey);
                            }
                        }
                    }

                    #endregion

                }
                finally
                {
                    LanguageDisposeData(dataLayer, resourceAif2, fullLocation);
                }
            }

            if (baseRooms.Count > 0)
            {
                try
                {
                    //Optimize all Rooms
                    functions.OptimizeHotelRoomsAvailabilityPrices(baseRooms, oneRateNotMapped);

                    //CC-3717 remove duplicates - Violation of PRIMARY KEY constraint 'PK_availability'
                    if (baseRooms.Count > 1)
                    {
                        var separator = HotelBedsSettings.CombinationSeparator.ToString();
                        //hotel_id, room_type_id, room_id, period_start, period_end
                        var availabilityPK = new HashSet<string>();
                        foreach (var room in baseRooms.Values)
                        {
                            var listDates = new List<DateTime>(room.AvailabilityPeriods.Keys);
                            foreach (var date in listDates)
                            {
                                var ap = room.AvailabilityPeriods[date];
                                var pk = string.Join(separator, hotel.Id, room.TypeId, room.RoomId, ap.DateFrom.ToString("yyyy-MM-dd"), ap.DateTo.ToString("yyyy-MM-dd"));
                                if (!availabilityPK.Contains(pk))
                                {
                                    availabilityPK.Add(pk);
                                }
                                else
                                {
                                    room.AvailabilityPeriods.Remove(date);
                                }
                            }
                        }
                    }

                    //Delete cancellation policies finally unused
                    var deleteCancellationPoliciesList = new List<string>();
                    foreach (var canPolId in baseCancellationPolicies.Keys)
                    {
                        var used = false;
                        foreach (var room in baseRooms.Values)
                        {
                            foreach (var ap in room.AvailabilityPeriods.Values)
                            {
                                if (ap.CancellationPolicies.Contains(canPolId))
                                {
                                    used = true;
                                    break;
                                }
                            }
                            if (used)
                            {
                                break;
                            }
                        }
                        if (!used)
                        {
                            deleteCancellationPoliciesList.Add(canPolId);
                        }
                    }
                    foreach (var canPolId in deleteCancellationPoliciesList)
                    {
                        baseCancellationPolicies.Remove(canPolId);
                    }

                    //Delete room types finally unused
                    var deleteRoomTypeList = new List<string>();
                    foreach (var roomTypeKey in baseRoomTypes.Keys)
                    {
                        var used = false;
                        foreach (var room in baseRooms.Values)
                        {
                            if (room.TypeId.Equals(roomTypeKey) && room.AvailabilityPeriods.Count > 0)
                            {
                                used = true;
                                break;
                            }
                        }
                        if (!used)
                        {
                            deleteRoomTypeList.Add(roomTypeKey);
                        }
                    }
                    foreach (var roomTypeKey in deleteRoomTypeList)
                    {
                        baseRoomTypes.Remove(roomTypeKey);
                    }

                    //Final step of optimization. Delete room reference if it exists in a room group, the room object still exists, XSD validation
                    foreach (var rGroup in baseRoomGroups.Values)
                    {
                        foreach (var rRoomId in rGroup.Rooms.Keys)
                        {
                            baseRooms.Remove(rRoomId);
                        }
                    }

                    hotel.BlockInfant = infantRestrictions;
                    hotel.AdultOnly = adultOnly;
                    hotel.RoomGroups = baseRoomGroups;
                    hotel.Rooms = baseRooms;

                    foreach (var roomType in baseRoomTypes)
                    {
                        if (!roomTypes.ContainsKey(roomType.Key))
                        {
                            roomTypes.TryAdd(roomType.Key, roomType.Value);

                            //Update room types description
                            if (Settings.Base.Build.type.Equals(ConfigurationBuildType.full))
                            {
                                if (String.IsNullOrEmpty(roomType.Value.Description))
                                {
                                    var rIdValues = functions.SplitRoomId(roomType.Value.Type_Id);
                                    var description = rIdValues["roomTypeCode"] + " " + roomType.Value.Beds + "+" + roomType.Value.ExtraBeds;
                                    var characteristic = " (" + (rIdValues["nonRefundable"].Equals("Y") ? "NRF " : String.Empty) + (rIdValues["opaque"].Equals("Y") ? "PKG " : String.Empty) + rIdValues["roomCharacteristic"] + ")";
                                    roomType.Value.Description = description + characteristic;
                                }
                            }
                        }
                    }

                    foreach (var canPol in baseCancellationPolicies)
                    {
                        if (!cancellationPolicies.ContainsKey(canPol.Key))
                        {
                            cancellationPolicies.TryAdd(canPol.Key, canPol.Value);
                        }
                    }

                    Interlocked.Increment(ref hotelsProcessed);
                    if (Settings.Base.Build.type.Equals(ConfigurationBuildType.availability))
                    {
                        Console.WriteLine("Hotel OK: ({0}) {1}", hotel.Id, hotel.Name);
                    }
                }
                catch (Exception ex)
                {
                    Interlocked.Increment(ref hotelsDropped);

                    var errorMsg = "HotelBeds hotel code: " + hotel.Id
                            + Environment.NewLine + "Error: " + ex.Message
                            + Environment.NewLine + ex.StackTrace;
                    errorLog.Add(errorMsg);
                }
            }
        }

        public void ProcessHotelStatic(Hotel hotel,
            HotelBedsSettings.StaticFiles resourceContents,
            Dictionary<string, string> hotelFiles,
            Dictionary<string, string> hotelFileNames,
            ConcurrentDictionary<HotelBedsSettings.ReaderType, IHotelBedsDataLayer> dataBase,
            IHotelBedsDataLayer dataLayer,
            HotelBedsCommonFunctions functions,
            ConcurrentBag<string> errorLog,
            Dictionary<string, Dictionary<HotelBedsSettings.Languages, string>> hIssueIndex,
            ConcurrentDictionary<string, RoomType> roomTypes,
            Dictionary<string, string> rDescriptionsIndex,
            Dictionary<string, string> rCharacteristicIndex,
            ConcurrentDictionary<string, string> roomTypeDescription,
            List<Room> roomList,
            Root dataRoot)
        {
            string file = null;
            string fileName = null;
            if (!hotelFiles.TryGetValue(hotel.Id, out file) || !hotelFileNames.TryGetValue(hotel.Id, out fileName))
            {
                return;
            }

            try
            {
                var resourceLoaded = LanguageLoadData(dataLayer, resourceContents, file);
                if (!resourceLoaded)
                {
                    errorLog.Add(string.Format("Can't read hotel static content ({0}) {1}", hotel.Id, file));
                    return;
                }

                var contentsTables = GetLanguageTable(dataBase, resourceContents, fileName);

                var tableName = Path.GetFileNameWithoutExtension(fileName);
                var staticTable = contentsTables[MainLanguage][tableName];
                if (staticTable.Rows.Count == 0 || !hotel.Id.Equals(staticTable.Rows[0]["hotelId"]))
                {
                    errorLog.Add(string.Format("Error reading hotel static content ({0}) {1}", hotel.Id, file));
                    return;
                }

                //hotel.AccomodationType
                hotel.Adress_City = staticTable.Rows[0]["city"];
                hotel.Adress_Street1 = staticTable.Rows[0]["address"];
                //hotel.Adress_Street2
                hotel.Adress_Zipcode = staticTable.Rows[0]["postalCode"];
                //hotel.BestBuy
                hotel.Description = staticTable.Rows[0]["description"];
                hotel.Email = staticTable.Rows[0]["email"];
                hotel.Fax = staticTable.Rows[0]["fax"];
                //hotel.Headline
                //hotel.MealLabels
                hotel.Phone = staticTable.Rows[0]["phoneHotel"];
                hotel.Position_Latitude = staticTable.Rows[0]["latitude"];
                hotel.Position_Longitude = staticTable.Rows[0]["longitude"];
                //hotel.Reviews
                //hotel.Tag
                //hotel.Themes
                //hotel.Notes

                //Add description translations
                var languageDescription = new Dictionary<HotelBedsSettings.Languages, TranslationsDescription>();
                var languageTableName = new Dictionary<HotelBedsSettings.Languages, string>();
                foreach (var langContent in contentsTables.Keys)
                {
                    languageTableName.Add(langContent, tableName);
                }
                foreach (var lang in Languages)
                {
                    if (!contentsTables.ContainsKey(lang))
                    {
                        var newResourceNameContents = HotelBedsSettings.LanguageFilesName[resourceContents][lang];
                        var newFileName = LanguageFileName(resourceContents, newResourceNameContents, CacheFileNameManager(file, false));
                        var newContentsTable = GetLanguageTable(dataBase, resourceContents, newFileName);
                        var newTableName = Path.GetFileNameWithoutExtension(newFileName);
                        foreach (var langContent in newContentsTable.Keys)
                        {
                            if (!contentsTables.ContainsKey(langContent))
                            {
                                contentsTables.Add(langContent, newContentsTable[langContent]);
                            }
                            if (!languageTableName.ContainsKey(langContent))
                            {
                                languageTableName.Add(langContent, newTableName);
                            }
                        }
                    }
                    if (HotelBedsSettings.LanguageHotelDescriptionTranslation.Contains(lang) && languageTableName.ContainsKey(lang))
                    {
                        var fileTableName = languageTableName[lang];
                        var langTable = contentsTables[lang][fileTableName];
                        if (langTable.Rows.Count > 0)
                        {
                            var langDescription = new TranslationsDescription();
                            langDescription.Value = langTable.Rows[0]["description"];
                            langDescription.lang = HotelBedsSettings.LanguageInternalCode[lang];
                            langDescription.country = HotelBedsSettings.LanguageCountryCode[lang];
                            languageDescription.Add(lang, langDescription);
                            hotel.Translations.description.Add(langDescription);
                        }
                    }
                }

                //Hotel issues added to the description
                if (staticTable.Rows[0]["issues"].Equals("Y") && hIssueIndex.ContainsKey(hotel.Id))
                {
                    foreach (var lang in Languages)
                    {
                        if (hIssueIndex[hotel.Id].ContainsKey(lang))
                        {
                            if (lang.Equals(MainLanguage))
                            {
                                if (!String.IsNullOrEmpty(hotel.Description))
                                {
                                    hotel.Description += Environment.NewLine;
                                }
                                hotel.Description += hIssueIndex[hotel.Id][lang];
                            }
                            if (languageDescription.ContainsKey(lang))
                            {
                                if (!String.IsNullOrEmpty(languageDescription[lang].Value))
                                {
                                    languageDescription[lang].Value += Environment.NewLine;
                                }
                                languageDescription[lang].Value += hIssueIndex[hotel.Id][lang];
                            }
                        }
                    }
                }

                //Room types description
                foreach (var room in roomList)
                {
                    RoomType rType;
                    if (roomTypes.TryGetValue(room.TypeId, out rType))
                    {
                        var rIdValues = functions.SplitRoomId(room.TypeId);
                        var rIdType = String.Join(HotelBedsSettings.RoomCharSeparator.ToString(), rIdValues["companyCode"], rIdValues["roomTypeCode"], rIdValues["roomCharacteristic"], rIdValues["nonRefundable"], rIdValues["opaque"]);
                        if (roomTypeDescription.ContainsKey(rIdType))
                        {
                            rType.Description = roomTypeDescription[rIdType];
                        }
                        else
                        {
                            var roomTypeId = String.Join(HotelBedsSettings.IdFieldCharSeparator.ToString(), rIdValues["roomTypeCode"], rIdValues["companyCode"]);
                            var roomCharacteristicId = String.Join(HotelBedsSettings.IdFieldCharSeparator.ToString(), rIdValues["companyCode"], rIdValues["roomCharacteristic"]);
                            var characteristic = " (" + (rIdValues["nonRefundable"].Equals("Y") ? "NRF " : String.Empty) + (rIdValues["opaque"].Equals("Y") ? "PKG " : String.Empty) + (rCharacteristicIndex.ContainsKey(roomCharacteristicId) ? rCharacteristicIndex[roomCharacteristicId] : rIdValues["roomCharacteristic"]) + ")";
                            if (rDescriptionsIndex.ContainsKey(roomTypeId))
                            {
                                roomTypeDescription.TryAdd(rIdType, rDescriptionsIndex[roomTypeId] + characteristic);
                                rType.Description = roomTypeDescription[rIdType];
                            }
                            else
                            {
                                var description = rIdValues["roomTypeCode"] + " " + rType.Beds + "+" + rType.ExtraBeds;
                                roomTypeDescription.TryAdd(rIdType, description + characteristic);
                                rType.Description = roomTypeDescription[rIdType];
                            }
                        }
                    }
                }

                //hotel.Images
                if (!String.IsNullOrEmpty(staticTable.Rows[0]["images"]))
                {
                    var images = staticTable.Rows[0]["images"].Split(HotelBedsSettings.XmlRowCharSeparator);
                    foreach (var imageItem in images)
                    {
                        var imageFields = imageItem.Split(HotelBedsSettings.XmlColumnCharSeparator);
                        var img = new Hotel.Image();
                        img.Id = imageFields[0];
                        var imgVariant = new Hotel.ImageVariant();
                        imgVariant.URL = imageFields[1];
                        imgVariant.Height = 480;
                        imgVariant.Width = 640;
                        img.ImageVariants.Add(imgVariant);
                        hotel.Images.Add(img);
                    }
                }

                //hotel.Features and hotel.Distances (beach, center, airport)
                if (!String.IsNullOrEmpty(staticTable.Rows[0]["facilities"]))
                {
                    var facilities = staticTable.Rows[0]["facilities"].Split(HotelBedsSettings.XmlRowCharSeparator);
                    foreach (var facilityStr in facilities)
                    {
                        var charIndex = facilityStr.IndexOf(HotelBedsSettings.XmlColumnCharSeparator, facilityStr.IndexOf(HotelBedsSettings.XmlColumnCharSeparator) + 1);
                        var facilityCode = facilityStr.Substring(0, charIndex);
                        var facilityDescription = facilityStr.Substring(charIndex + 1);
                        if (HotelBedsSettings.MapFeatures.ContainsKey(facilityCode))
                        {
                            var featureCode = HotelBedsSettings.MapFeatures[facilityCode];
                            var found = false;
                            foreach (var feature in hotel.Features)
                            {
                                if (feature.Id.Equals(featureCode))
                                {
                                    found = true;
                                    break;
                                }
                            }
                            if (!found)
                            {
                                var featureName = String.Empty;
                                foreach (var rFeature in dataRoot.Features)
                                {
                                    if (rFeature.Id.Equals(featureCode))
                                    {
                                        featureName = rFeature.Name;
                                        break;
                                    }
                                }
                                var feature = new Feature();
                                feature.Id = featureCode;
                                feature.Name = featureName;
                                hotel.Features.Add(feature);
                            }
                        }
                        else
                        {
                            var distanceCode = Int32.Parse(HotelBedsSettings.MapDistances[facilityCode]);
                            Hotel.Distance hDistance = null;
                            foreach (var distance in hotel.Distances)
                            {
                                if (distance.Id.Equals(distanceCode))
                                {
                                    hDistance = distance;
                                    break;
                                }
                            }
                            if (hDistance == null)
                            {
                                var distanceName = String.Empty;
                                foreach (var rDistance in dataRoot.Distance)
                                {
                                    if (rDistance.Id.Equals(distanceCode))
                                    {
                                        distanceName = rDistance.Name;
                                        break;
                                    }
                                }
                                var distance = new Hotel.Distance();
                                distance.Id = distanceCode;
                                distance.Name = distanceName;
                                distance.Value = Decimal.Divide(Int32.Parse(facilityDescription), 1000).ToString(NumberFormatInfo.InvariantInfo);
                                hotel.Distances.Add(distance);
                            }
                            else
                            {
                                //We update the minimum value of the same distance type
                                var distanceKM = Decimal.Divide(Int32.Parse(facilityDescription), 1000);
                                if (Decimal.Parse(hDistance.Value, NumberFormatInfo.InvariantInfo) > distanceKM)
                                {
                                    hDistance.Value = distanceKM.ToString(NumberFormatInfo.InvariantInfo);
                                }
                            }
                        }
                    }
                }

                //hotel.Distances (airport)
                if (!String.IsNullOrEmpty(staticTable.Rows[0]["distances"]))
                {
                    var distances = staticTable.Rows[0]["distances"].Split(HotelBedsSettings.XmlRowCharSeparator);
                    foreach (var distanceStr in distances)
                    {
                        //'airport' code 3
                        var distanceCode = 3;
                        Hotel.Distance hDistance = null;
                        foreach (var distance in hotel.Distances)
                        {
                            if (distance.Id.Equals(distanceCode))
                            {
                                hDistance = distance;
                                break;
                            }
                        }
                        if (hDistance == null)
                        {
                            var distanceName = String.Empty;
                            foreach (var rDistance in dataRoot.Distance)
                            {
                                if (rDistance.Id.Equals(distanceCode))
                                {
                                    distanceName = rDistance.Name;
                                    break;
                                }
                            }
                            var distance = new Hotel.Distance();
                            distance.Id = distanceCode;
                            distance.Name = distanceName;
                            distance.Value = distanceStr;
                            hotel.Distances.Add(distance);
                        }
                        else
                        {
                            //We update the minimum value of the same distance type
                            var distanceKM = Decimal.Parse(distanceStr, NumberFormatInfo.InvariantInfo);
                            if (Decimal.Parse(hDistance.Value, NumberFormatInfo.InvariantInfo) > distanceKM)
                            {
                                hDistance.Value = distanceKM.ToString(NumberFormatInfo.InvariantInfo);
                            }
                        }
                    }
                }
                Console.WriteLine("Hotel OK: ({0}) {1}", hotel.Id, hotel.Name);
            }
            catch(Exception ex)
            {
                var errorMsg = "HotelBeds hotel code: " + hotel.Id
                            + Environment.NewLine + "Error: " + ex.Message
                            + Environment.NewLine + ex.StackTrace;
                errorLog.Add(errorMsg);
            }
            finally
            {
                LanguageDisposeData(dataLayer, resourceContents, file);
            }
        }

        #endregion

    }
}
