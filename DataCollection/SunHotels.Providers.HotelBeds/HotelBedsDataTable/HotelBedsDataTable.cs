﻿using System.Collections.Generic;

namespace SunHotels.Providers
{
    public class HotelBedsDataTable : IHotelBedsDataTable
    {
        public List<string> Columns { get; private set; }
        public List<Dictionary<string, string>> Rows { get; private set; }

        public HotelBedsDataTable()
        {
            Columns = new List<string>();
            Rows = new List<Dictionary<string, string>>();
        }

        public void Clear()
        {
            Rows.Clear();
        }

        public Dictionary<string, string> NewRow()
        {
            var row = new Dictionary<string, string>();
            foreach (var column in Columns)
            {
                row.Add(column, null);
            }
            return row;
        }
    }
}
