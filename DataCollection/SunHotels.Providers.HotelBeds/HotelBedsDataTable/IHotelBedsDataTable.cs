﻿using System.Collections.Generic;

namespace SunHotels.Providers
{
    public interface IHotelBedsDataTable
    {
        List<string> Columns { get; }
        List<Dictionary<string, string>> Rows { get; }

        void Clear();
        Dictionary<string, string> NewRow();
    }
}
