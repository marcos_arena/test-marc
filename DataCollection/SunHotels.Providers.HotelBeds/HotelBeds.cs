﻿using Sunhotels.Export;
using SunHotels.Export;
using SunHotels.XML;
using SunHotels.XML.Data;
using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace SunHotels.Providers
{
    public class HotelBeds : XMLProvider
    {
        private HotelBedsCommon Common;
        private Dictionary<string, int> _MappedHotels;

        protected override void getProviderDefinition(Root dataRoot)
        {
            Common = new HotelBedsCommon(configuration);

            dataRoot.ProviderDefinition.Name = configuration.Provider;
            dataRoot.ProviderDefinition.Currency = configuration.Currency;
        }

        private BaseFileExporter _exporter;
        protected override BaseFileExporter Exporter
        {
            get { return _exporter ?? (_exporter = new HotelBedsCommonExporter(configuration)); }
        }

        protected override void getData(Root dataRoot)
        {
            try
            {

                Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;

                var actions = new Dictionary<string, Action>();
                var threads = new Dictionary<string, Task>();

                var basicResources = Common.ResourcesByLanguages(ArrayList.Adapter(Enum.GetValues(typeof(HotelBedsSettings.BasicFiles))));
                foreach (var rscr in basicResources.Keys)
                {
                    var resource = rscr;
                    var fileLanguages = basicResources[resource];
                    foreach (var resourceName in fileLanguages.Keys)
                    {
                        actions.Add(resourceName, () => Common.ResourceManager(new HotelBedsConnectFtp(), Common.Settings.FtpUrl, Common.Settings.FtpUser, Common.Settings.FtpPassword, resource, resourceName));
                    }
                }

                if (Common.Settings.Base.Build.typeSpecified && (Common.Settings.Base.Build.type.Equals(Sunhotels.Export.ConfigurationBuildType.full) || Common.Settings.Base.Build.type.Equals(Sunhotels.Export.ConfigurationBuildType.@static)))
                {
                    var staticResources = Common.ResourcesByLanguages(ArrayList.Adapter(Enum.GetValues(typeof(HotelBedsSettings.StaticFiles))));
                    foreach (var rscr in staticResources.Keys)
                    {
                        var resource = rscr;
                        var fileLanguages = staticResources[resource];
                        foreach (var resourceName in fileLanguages.Keys)
                        {
                            actions.Add(resourceName, () => Common.ResourceManager(new HotelBedsConnectFtp(), Common.Settings.FtpUrl, Common.Settings.FtpUser, Common.Settings.FtpPassword, resource, resourceName));
                        }
                    }
                }

                if (Common.Settings.Base.Build.typeSpecified && (Common.Settings.Base.Build.type.Equals(Sunhotels.Export.ConfigurationBuildType.full) || Common.Settings.Base.Build.type.Equals(Sunhotels.Export.ConfigurationBuildType.availability)))
                {
                    var resource = HotelBedsSettings.CacheFiles.ActiveInventoryFilesServices;
                    var resourceName = HotelBedsSettings.Files[resource];
                    actions.Add(resourceName, () => Common.ResourceManager(new HotelBedsConnectHttp(), Common.Settings.Aif2Url, Common.Settings.WebServiceUser, Common.Settings.WebServicePassword, resource, resourceName));
                }

                foreach (var actionName in actions.Keys)
                {
                    LogEntry(XML.HelpClasses.Log.MessageType.Informational, "Starting Action ({0})", actionName);
                    if (Common.Settings.Multithread)
                    {
                        var actionThread = actions[actionName];
                        var taskName = actionName;
                        var thread = new Task(() =>
                        {
                            Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;
                            Thread.CurrentThread.Name = taskName;
                            actionThread();
                        });
                        thread.Start();
                        threads.Add(taskName, thread);
                    }
                    else
                    {
                        LogEntry(XML.HelpClasses.Log.MessageType.Informational, "Starting Action ({0})", actionName);
                        actions[actionName]();
                        LogEntry(XML.HelpClasses.Log.MessageType.Informational, "Action finished ({0})", actionName);
                    }
                }

                if (Common.Settings.Multithread)
                {
                    //Wait for all threads to finish
                    foreach (var taskName in threads.Keys)
                    {
                        LogEntry(XML.HelpClasses.Log.MessageType.Informational, "Waiting action to finish ({0})", taskName);
                        threads[taskName].Wait();
                        LogEntry(XML.HelpClasses.Log.MessageType.Informational, "Action finished ({0})", taskName);
                    }
                }

                //Process all the data and fills the root object
                ProcessDataBase(dataRoot);

            }
            catch (System.Exception exc)
            {
                LogEntry(XML.HelpClasses.Log.MessageType.Error, "Fatal error in HotelBeds.getData: {0}", exc.ToString());
                throw;
            }
        }

        private void ProcessDataBase(Root dataRoot)
        {
            LogEntry(XML.HelpClasses.Log.MessageType.Informational, "Process DataBase Start");
            var dataBase = Common.CreateDataBase();

            //Create Places
            LogEntry(XML.HelpClasses.Log.MessageType.Informational, "Process CreatePlaces Start");
            var resortIndex = CreatePlaces(dataBase);
            LogEntry(XML.HelpClasses.Log.MessageType.Informational, "Process CreatePlaces End");

            //Create Hotels
            LogEntry(XML.HelpClasses.Log.MessageType.Informational, "Process CreateHotels Start");
            int hotelsMapped;
            int hotelsUnMapped;
            string configurationContentType;
            var hotelIndex = CreateHotels(dataBase, resortIndex, out hotelsMapped, out hotelsUnMapped, out configurationContentType);
            LogEntry(XML.HelpClasses.Log.MessageType.Informational, "Process CreateHotels End");

            Common.ProcessHotels(dataRoot, dataBase, resortIndex, hotelIndex, false, _MappedHotels, Logger, 
                                 (XML.HelpClasses.Log.MessageType severity, string message) => LogEntry(severity, message), 
                                 (HotelBedsCommonExporter)Exporter,
                                 hotelsMapped, hotelsUnMapped, configurationContentType);

            LogEntry(XML.HelpClasses.Log.MessageType.Informational, "Process DataBase End");

            //Try to free memory and delete the downloaded files
            LogEntry(XML.HelpClasses.Log.MessageType.Informational, "Dispose DataBase Start");
            Common.DisposeDataBase(dataBase);
            LogEntry(XML.HelpClasses.Log.MessageType.Informational, "Dispose DataBase End");
        }

        private Dictionary<string, Place> CreatePlaces(ConcurrentDictionary<HotelBedsSettings.ReaderType, IHotelBedsDataLayer> dataBase)
        {
            var resourceDestinations = HotelBedsSettings.BasicFiles.Destinations;
            var resourceNameDestinations = HotelBedsSettings.LanguageFilesName[resourceDestinations][Common.MainLanguage];
            var resortIndex = new Dictionary<string, Place>();
            var countryList = new List<Place>();

            //Load Basic Files (without threads, reading excels is faster one after one)
            Common.LoadData(dataBase, new ArrayList() { resourceDestinations }, false, true);

            var destinationTables = Common.GetLanguageTable(dataBase, resourceDestinations, null);
            var destinationTableName = Path.GetFileNameWithoutExtension(resourceNameDestinations);
            if (!destinationTables.ContainsKey(Common.MainLanguage))
            {
                LogEntry(XML.HelpClasses.Log.MessageType.Warning, "Destination table [{0}] not found in file [{1}]", Common.MainLanguage.ToString(), resourceNameDestinations);
                throw new ApplicationException($"Destination table [{Common.MainLanguage.ToString()}] not found in file [{resourceNameDestinations}]");
            }
            var destinationTable = destinationTables[Common.MainLanguage][destinationTableName];

            for (var i = 0; i < destinationTable.Rows.Count; i++)
            {
                var row = destinationTable.Rows[i];
                var countryId = row["Country Code"];
                var countryDescription = row["Country Name"];
                var destinationId = row["Destination Code"];
                var destinationDescription = row["Destination Name"];
                var resortId = row["Zone Code"];
                var resortDescription = row["Zone Name"];
                var globalDestinationId = String.Join(HotelBedsSettings.PlaceSeparator.ToString(), countryId, destinationId);
                var globalResortId = String.Join(HotelBedsSettings.PlaceSeparator.ToString(), globalDestinationId, resortId);

                Place country = null;
                foreach (var c in countryList)
                {
                    if (c.Id.Equals(countryId))
                    {
                        country = c;
                        break;
                    }
                }
                if (country == null)
                {
                    country = new Place();
                    country.Id = countryId;
                    country.Description = countryDescription;
                    countryList.Add(country);
                }

                Place destination = null;
                foreach (var d in country.Places)
                {
                    if (d.Id.Equals(globalDestinationId))
                    {
                        destination = d;
                        break;
                    }
                }
                if (destination == null)
                {
                    destination = new Place();
                    destination.Id = globalDestinationId;
                    destination.Description = destinationDescription;
                    country.Places.Add(destination);
                }

                Place resort = null;
                foreach (var r in destination.Places)
                {
                    if (r.Id.Equals(globalResortId))
                    {
                        resort = r;
                        break;
                    }
                }
                if (resort == null)
                {
                    resort = new Place();
                    resort.Id = globalResortId;
                    resort.Description = resortDescription;
                    destination.Places.Add(resort);
                }

                //Populate index
                if (!resortIndex.ContainsKey(country.Id))
                {
                    resortIndex.Add(country.Id, country);
                }
                if (!resortIndex.ContainsKey(globalDestinationId))
                {
                    resortIndex.Add(globalDestinationId, destination);
                }
                if (!resortIndex.ContainsKey(globalResortId))
                {
                    resortIndex.Add(globalResortId, resort);
                }
            }

            return resortIndex;
        }

        private Dictionary<string, Hotel> CreateHotels(ConcurrentDictionary<HotelBedsSettings.ReaderType, IHotelBedsDataLayer> dataBase, Dictionary<string, Place> resortIndex,
                                                       out int hotelsMapped, out int hotelsUnmapped, out string configurationContentType)
        {
            var resourceHotels = HotelBedsSettings.BasicFiles.Hotels;
            var resourceNameHotels = HotelBedsSettings.LanguageFilesName[resourceHotels][Common.MainLanguage];
            var hotelIndex = new Dictionary<string, Hotel>();

            _MappedHotels = MappedHotels();

            //Load Basic Files (without threads, reading excels is faster one after one)
            Common.LoadData(dataBase, new ArrayList() { resourceHotels }, false, true);

            var hotelTables = Common.GetLanguageTable(dataBase, resourceHotels, null);
            var hotelTableName = Path.GetFileNameWithoutExtension(resourceNameHotels);
            var hotelTable = hotelTables[Common.MainLanguage][hotelTableName];
            hotelsMapped = 0;
            hotelsUnmapped = 0;
            configurationContentType = Common.Settings.Base.Content.type.ToString();

            for (var i = 0; i < hotelTable.Rows.Count; i++)
            {
                var row = hotelTable.Rows[i];
                var hotelId = row["Hotel Code"];
                var hotelIsMapped = _MappedHotels.ContainsKey(hotelId);
                if (hotelIsMapped)
                    hotelsMapped += 1;
                else
                    hotelsUnmapped += 1;

                var hotelImportOk = Common.Settings.Base.Content.type == ConfigurationContentType.all
                                    || (Common.Settings.Base.Content.type == ConfigurationContentType.mapped && hotelIsMapped)
                                    || (Common.Settings.Base.Content.type == ConfigurationContentType.unmapped && !hotelIsMapped);

                if (hotelImportOk)
                {
                    var hotelName = row["Hotel Name"];
                    var hotelCategory = row["Category Code"];
                    var countryId = row["Country Code"];
                    var destinationId = row["Destination Code"];
                    var resortId = row["Zone Code"];

                    var globalResortId = String.Join(HotelBedsSettings.PlaceSeparator.ToString(), countryId, destinationId, resortId);
                    if (resortIndex.ContainsKey(globalResortId) && !hotelIndex.ContainsKey(hotelId))
                    {
                        var hotel = new Hotel();
                        hotel.Id = hotelId;
                        hotel.Name = hotelName;
                        hotel.Classification = hotelCategory;
                        hotel.PlaceId = globalResortId;
                        hotel.Adress_Country = resortIndex[countryId].Description;
                        hotel.Adress_State = resortIndex[String.Join(HotelBedsSettings.PlaceSeparator.ToString(), countryId, destinationId)].Description;

                        hotelIndex.Add(hotelId, hotel);
                    }
                }
            }

            return hotelIndex;
        }
    }
}
