﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;

namespace SunHotels.Providers
{
    public interface IHotelBedsDataLayer
    {
        HotelBedsSettings.ReaderType Type { get; }
        ConcurrentDictionary<string, Enum> DataResource { get; }
        ConcurrentDictionary<string, FileStream> DataFiles { get; }
        ConcurrentDictionary<string, Dictionary<string, IHotelBedsDataTable>> DataTables { get; }
        ConcurrentDictionary<string, List<string>> FileHotels { get; }

        bool LoadBasicDataFile(string fileName, Enum resource, FileStream resourceStream);
        bool LoadDataFile(string fileName);
        void LoadExceptionHandler(string fileName);
        void Dispose(string filterFileName);
    }
}
