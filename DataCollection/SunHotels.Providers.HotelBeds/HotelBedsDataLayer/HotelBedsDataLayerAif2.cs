﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;

namespace SunHotels.Providers
{
    public class HotelBedsDataLayerAif2 : IHotelBedsDataLayer
    {
        public HotelBedsSettings.ReaderType Type { get; private set; }
        public ConcurrentDictionary<string, Enum> DataResource { get; private set; }
        public ConcurrentDictionary<string, FileStream> DataFiles { get; private set; }
        public ConcurrentDictionary<string, Dictionary<string, IHotelBedsDataTable>> DataTables { get; private set; }
        public ConcurrentDictionary<string, List<string>> FileHotels { get; private set; }

        public HotelBedsDataLayerAif2()
        {
            Type = HotelBedsSettings.ReaderType.Aif2;
            DataResource = new ConcurrentDictionary<string, Enum>();
            DataFiles = new ConcurrentDictionary<string, FileStream>();
            DataTables = new ConcurrentDictionary<string, Dictionary<string, IHotelBedsDataTable>>();
            FileHotels = new ConcurrentDictionary<string, List<string>>();
        }

        public bool LoadBasicDataFile(string fileName, Enum resource, FileStream resourceStream)
        {
            bool fileOk = true;
            try
            {
                var contractInfo = fileName.Split('_');
                if (contractInfo.Length != 4)
                {
                    throw new ApplicationException("Contract not valid");
                }

                var contractAvailability = contractInfo[3];

                //'F' -> Available, 'C' -> No longer available
                var contractStatus = contractAvailability[0];

                if (contractStatus.Equals('F'))
                {
                    var contractOption = contractInfo[2];
                    if (contractOption != "O" && contractOption != "M")
                    {
                        throw new ApplicationException("Contract not valid");
                    }
                    var contractId = contractInfo[0] + '_' + contractInfo[1];

                    var conflictedContract = contractOption == "O"
                            ? contractId + "_M_" + contractAvailability
                            : contractId + "_O_" + contractAvailability;

                    //Only allow one contractId to avoid duplicates
                    if (DataFiles.ContainsKey(conflictedContract))
                    {
                        if (contractOption == "O")
                        {
                            LoadExceptionHandler(conflictedContract);
                        }
                        else
                        {
                            fileOk = false;
                            LoadExceptionHandler(fileName);
                        }
                    }

                    if (fileOk)
                    {
                        DataResource.TryAdd(fileName, resource);
                        DataFiles.TryAdd(fileName, resourceStream);
                        FileHotels.TryAdd(fileName, new List<string>());

                        HotelFiles(fileName);
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("File {0} can't be open ({1}): {2}", fileName, Enum.GetName(typeof(HotelBedsSettings.ReaderType), Type), e.Message);
                Console.WriteLine(e.StackTrace);
                fileOk = false;
                LoadExceptionHandler(fileName);
            }
            return fileOk;
        }

        public bool LoadDataFile(string fileName)
        {
            bool fileOk = true;
            try
            {
                //'F' -> Available, 'U' -> Update only allotment, 'C' -> No longer available
                var contractStatus = fileName.Substring(fileName.LastIndexOf('_') + 1, 1);

                if (contractStatus.Equals("F"))
                {
                    var tables = ReadTables(fileName);

                    DataTables.TryAdd(fileName, tables);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("File {0} can't be open ({1}): {2}", fileName, Enum.GetName(typeof(HotelBedsSettings.ReaderType), Type), e.Message);
                Console.WriteLine(e.StackTrace);
                fileOk = false;
                LoadExceptionHandler(fileName);
            }
            return fileOk;
        }

        public void LoadExceptionHandler(string fileName)
        {
            Dispose(fileName);

            Enum removeResource;
            FileStream removeResourceStream;
            Dictionary<string, IHotelBedsDataTable> removeTables;
            List<string> removeFileHotels;

            DataResource.TryRemove(fileName, out removeResource);
            DataFiles.TryRemove(fileName, out removeResourceStream);
            DataTables.TryRemove(fileName, out removeTables);
            FileHotels.TryRemove(fileName, out removeFileHotels);
        }

        public void Dispose(string filterFileName)
        {
            if (!String.IsNullOrEmpty(filterFileName))
            {
                if (DataFiles.ContainsKey(filterFileName))
                {
                    FileStream file;
                    DataFiles.TryGetValue(filterFileName, out file);
                    file.Close();
#if !DEBUG
                    File.Delete(file.Name);
#endif
                    Dictionary<string, IHotelBedsDataTable> tables;
                    DataTables.TryGetValue(filterFileName, out tables);
                    if (tables != null && tables.Count > 0)
                    {
                        foreach (var tableName in tables.Keys)
                        {
                            tables[tableName].Clear();
                        }
                    }
                }
            }
            else
            {
                foreach (var fileName in DataFiles.Keys)
                {
                    FileStream file;
                    DataFiles.TryGetValue(fileName, out file);
                    file.Close();
#if !DEBUG
                    File.Delete(file.Name);
#endif
                    Dictionary<string, IHotelBedsDataTable> tables;
                    DataTables.TryGetValue(fileName, out tables);
                    if (tables != null && tables.Count > 0)
                    {
                        foreach (var tableName in tables.Keys)
                        {
                            tables[tableName].Clear();
                        }
                    }
                }
            }
        }

        private static Dictionary<string, string> HotelBedsAif2Tables = new Dictionary<string, string>
        {
            {"CCON", "CONTRACT HEADER"},
            {"CNHA", "ROOM TYPES"},
            {"CNIN", "INVENTORY"},
            {"CNCT", "PRICES"},
            {"CNSR", "BOARD SUPPLEMENTS AND DISCOUNTS"},
            {"CNSU", "SUPPLEMENTS AND DISCOUNTS"},
            {"CNPV", "STOP SALES"},
            {"CNGR", "FREES"},
            {"CNOE", "COMBINABLE OFFERS"},
            {"CNEM", "MINIMUM AND MAXIMUM STAY"},
            {"CNTA", "RATE CODES"},
            {"CNES", "CHECK IN AND CHECK OUT"},
            {"CNNH", "NO HOTEL CONTRACTS"},
            {"CNCF", "CANCELLATION FEES"},
            {"CNPR", "PROMOTIONS"},
            {"CNHF", "HANDLING FEES"},
            {"ATAX", "TAX BREAKDOWN"},
            {"CNCL", "VALID MARKETS"},
            {"SIAP", "SUPPLIERS INTEGRATION AVAILABILITY AND PRICES"},
            {"SICF", "SUPPLIERS INTEGRATION CANCELLATION FEES"},
            {"SIIN", "SUPPLIERS INTEGRATION INVENTORY"},
            {"SIEM", "SUPPLIERS INTEGRATION MINIMUM STAY"}
        };

        private static Dictionary<string, List<string>> HotelBedsAif2Fields = new Dictionary<string, List<string>>
        {
            {"CCON", new List<string> {"External Inventory", "Destination code", "Office code", "Contract number", "Contract name", "Company code", "Type of service", "Hotel code", "GIATA Hotel code", "Initial date", "End date", "No hotel", "Currency", "Base board", "Classification", "Payment model", "Daily price", "Release days", "Minimum child age", "Maximum child age", "Opaque", "Fix rate", "Contract type", "Maximum rooms", "Hotel content", "Selling price", "Internal field", "Internal field data", "Internal classification", "Is total price per stay"}},
            {"CNHA", new List<string> {"Room type", "Characteristic", "Standard capacity", "Minimum pax", "Maximum pax", "Maximum adult", "Maximum children", "Maximum infants", "Minimum adults", "Minimum children"}},
            {"CNIN", new List<string> {"Initial date", "Final date", "Room type", "Characteristic", "Rate", String.Join(HotelBedsSettings.Aif2TableCharSeparator.ToString(), "Release", "Allotment")}},
            {"CNCT", new List<string> {"Initial date", "Final date", "Room type", "Characteristic", "Generic Rate", "Market price", String.Join(HotelBedsSettings.Aif2TableCharSeparator.ToString(), "Is price per pax", "Net price", "Price", "Specific Rate", "Base board", "Amount")}},
            {"CNSR", new List<string> {"Initial date", "Final date", "Board code", "Is per pax", "Amount", "Percentage", "Rate", "Room type", "Characteristic", "Minimum age", "Maximum age", "On Monday", "On Tuesday", "On Wednesday", "On Thursday", "On Friday", "On Saturday", "On Sunday", "Internal field", "Net price", "Price", "Market price"}},
            {"CNSU", new List<string> {"Initial date", "Final date", "Application initial date", "Application final date", "Supplement or discount code", "Type", "Is per pax", "Opaque", "Order", "Application type", "Amount", "Percentage", "Is cumulative", "Rate", "Room type", "Characteristic", "Board", "Adults", "Pax order", "Minimum age", "Maximum age", "Number of days", "Length of stay", "Limit date", "On Monday", "On Tuesday", "On Wednesday", "On Thursday", "On Friday", "On Saturday", "On Sunday", "Net price", "Price", "Market price"}},
            {"CNPV", new List<string> {"Initial date", "Final date", "Rate", "Room type", "Characteristic", "Board"}},
            {"CNGR", new List<string> {"Initial date", "Final date", "Application initial date", "Application final date", "Minimum days", "Maximum days", "Rate", "Room type", "Characteristic", "Board", "Frees", "Free code", "Discount", "Application base type", "Application board type", "Application discount type", "Application stay type", "On Monday", "On Tuesday", "On Wednesday", "On Thursday", "On Friday", "On Saturday", "On Sunday", "Week Day validation type"}},
            {"CNOE", new List<string> {"Code1", "Code2", "Is excluded"}},
            {"CNEM", new List<string> {"Application date", "Initial date", "Final date", "Type", "Rate", "Room type", "Characteristic", "Board", "Minimum days", "Maximum days", "On Monday", "On Tuesday", "On Wednesday", "On Thursday", "On Friday", "On Saturday", "On Sunday"}},
            {"CNTA", new List<string> {"Rate", "Description", "Order"}},
            {"CNES", new List<string> {"Application date", "Initial date", "Final date", "Type", "Room type", "Characteristic", "Rate", "On Monday", "On Tuesday", "On Wednesday", "On Thursday", "On Friday", "On Saturday", "On Sunday", "Board"}},
            {"CNNH", new List<string> {"Language", "Hotel Description"}},
            {"CNCF", new List<string> {"Application date", "Initial date", "Final date", "Rate", "Days", "Hours", "Amount first night", "Percentage first night", "Amount per night", "Percentage per night", "Application end date", "Type"}},
            {"CNPR", new List<string> {"Code", "Description", "Initial date", "Final date", "Application initial date", "Application final date", "Is included"}},
            {"CNHF", new List<string> {"Initial date", "Final date", "Code", "Rate", "Type", "Amount", "Percentage", "Adult amount", "Child amount", "Minimum age", "Maximum age", "Age Amount", "Is per service"}},
            {"ATAX", new List<string> {"Initial date", "Final date", "Room code", "Board code", "Tax code", "Included in price", "Maximum number of nights", "Minimum age", "Is per night", "Is per pax", "Amount", "Percentage", "Currency"}},
            {"CNCL", new List<string> {"Country code", "Valid for a country"}},
            {"SIAP", new List<string> {"Initial date", "End date", "Room type", "Characteristic", "Board type", "Length of stay (LOS)", "Adults", "Children", "Occupancy", String.Join(HotelBedsSettings.Aif2TableCharSeparator.ToString(), "Net price", "Price", "Amount")}},
            {"SICF", new List<string> {"Initial date", "End date", "Days", "Hours", "Charge type", "Amount", "Percentage"}},
            {"SIIN", new List<string> {"Initial date", "Final date", "Room type", "Characteristic", "Board", String.Join(HotelBedsSettings.Aif2TableCharSeparator.ToString(), "Release Min", "Release Max", "Allotment")}},
            {"SIEM", new List<string> {"Initial date", "Final date", "Room type", "Characteristic", "Board", "Is per check-in date", "Is per range", "Days"}}
        };

        private void HotelFiles(string fileName)
        {
            FileStream file;
            DataFiles.TryGetValue(fileName, out file);

            List<string> hotelList;
            FileHotels.TryGetValue(fileName, out hotelList);

            var reader = new StreamReader(file);
            var headerContract = reader.ReadLine();
            if (headerContract.Contains("CCON"))
            {
                var contract = reader.ReadLine();
                var fields = contract.Split(':');
                var hotelId = fields[7];
                hotelList.Add(hotelId);
            }

            file.Seek(0, SeekOrigin.Begin);
        }

        private Dictionary<string, IHotelBedsDataTable> ReadTables(string fileName)
        {
            var dataTables = new Dictionary<string, IHotelBedsDataTable>();

            var tables = new Dictionary<string, List<string[]>>();
            foreach (var table in HotelBedsAif2Tables.Keys)
            {
                tables.Add(HotelBedsAif2Tables[table], new List<string[]>());
            }

            FileStream file;
            DataFiles.TryGetValue(fileName, out file);

            var lines = new List<string>();
            using (var reader = new StreamReader(file))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    lines.Add(line);
                }
            }
            
            var fieldsNumber = -1;
            var tableCode = String.Empty;
            foreach (var line in lines)
            {
                if (!String.IsNullOrEmpty(line))
                {
                    var src = line.Trim();
                    if (src.StartsWith("{/") && src.EndsWith("}"))
                    {
                        //End reading table, reset values
                        fieldsNumber = -1;
                        tableCode = String.Empty;
                    }
                    else if (src.StartsWith("{") && src.EndsWith("}"))
                    {
                        //Start reading table
                        tableCode = src.Substring(1, src.Length - 2);
                    }
                    else
                    {
                        if (HotelBedsAif2Tables.ContainsKey(tableCode))
                        {
                            //Read data line
                            var fields = src.Split(':');

                            if (fieldsNumber == -1)
                            {
                                //First time reading line
                                fieldsNumber = fields.Length;
                            }

                            if (fieldsNumber != fields.Length)
                            {
                                //Second time or more, so we check all table lines have the same number of fields
                                throw new ApplicationException(String.Format("Error reading Aif2 file '{0}' table '{1}': Row fields doesn't have the same length", fileName, HotelBedsAif2Tables[tableCode]));
                            }

                            tables[HotelBedsAif2Tables[tableCode]].Add(fields);
                        }
                    }
                }
            }
            
            foreach (var table in HotelBedsAif2Tables.Keys)
            {
                var dataTable = CreateDataTable(table, tables[HotelBedsAif2Tables[table]]);
                dataTables.Add(Path.GetFileNameWithoutExtension(fileName) + HotelBedsSettings.Aif2TableCharSeparator + HotelBedsAif2Tables[table], dataTable);
            }

            return dataTables;
        }

        private IHotelBedsDataTable CreateDataTable(string tableCode, List<string[]> tableRows)
        {
            var dataTable = new HotelBedsDataTable();

            foreach (var col in HotelBedsAif2Fields[tableCode])
            {
                dataTable.Columns.Add(col);
            }

            Dictionary<string, string> row;
            foreach (var tableRow in tableRows)
            {
                row = dataTable.NewRow();
                for (var i = 0; i < dataTable.Columns.Count; i++)
                {
                    var value = String.Empty;
                    if (tableRow.Length > i)
                    {
                        value = tableRow[i].Trim();
                    }
                    row[dataTable.Columns[i]] = value;
                }
                dataTable.Rows.Add(row);
            }

            return dataTable;
        }
    }
}
