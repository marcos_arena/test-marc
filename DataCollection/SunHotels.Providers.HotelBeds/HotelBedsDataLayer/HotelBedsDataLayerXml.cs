﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text;
using System.Xml;

namespace SunHotels.Providers
{
    public class HotelBedsDataLayerXml : IHotelBedsDataLayer
    {
        public HotelBedsSettings.ReaderType Type { get; private set; }
        public ConcurrentDictionary<string, Enum> DataResource { get; private set; }
        public ConcurrentDictionary<string, FileStream> DataFiles { get; private set; }
        public ConcurrentDictionary<string, Dictionary<string, IHotelBedsDataTable>> DataTables { get; private set; }
        public ConcurrentDictionary<string, List<string>> FileHotels { get; private set; }

        public HotelBedsDataLayerXml()
        {
            Type = HotelBedsSettings.ReaderType.Xml;
            DataResource = new ConcurrentDictionary<string, Enum>();
            DataFiles = new ConcurrentDictionary<string, FileStream>();
            DataTables = new ConcurrentDictionary<string, Dictionary<string, IHotelBedsDataTable>>();
            FileHotels = new ConcurrentDictionary<string, List<string>>();
        }

        public bool LoadBasicDataFile(string fileName, Enum resource, FileStream resourceStream)
        {
            bool fileOk = true;
            try
            {
                DataResource.TryAdd(fileName, resource);
                DataFiles.TryAdd(fileName, resourceStream);
                FileHotels.TryAdd(fileName, new List<string>());

                if (resource.Equals(HotelBedsSettings.StaticFiles.Contents))
                {
                    HotelFiles(fileName);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("File {0} can't be open ({1}): {2}", fileName, Enum.GetName(typeof(HotelBedsSettings.ReaderType), Type), e.Message);
                Console.WriteLine(e.StackTrace);
                fileOk = false;
                LoadExceptionHandler(fileName);
            }
            return fileOk;
        }

        public bool LoadDataFile(string fileName)
        {
            bool fileOk = true;
            try
            {
                Enum resource;
                DataResource.TryGetValue(fileName, out resource);

                if (resource.Equals(HotelBedsSettings.StaticFiles.Contents))
                {
                    var tables = ReadTableContents(fileName);
                    DataTables.TryAdd(fileName, tables);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("File {0} can't be open ({1}): {2}", fileName, Enum.GetName(typeof(HotelBedsSettings.ReaderType), Type), e.Message);
                Console.WriteLine(e.StackTrace);
                fileOk = false;
                LoadExceptionHandler(fileName);
            }
            return fileOk;
        }

        public void LoadExceptionHandler(string fileName)
        {
            Dispose(fileName);

            Enum removeResource;
            FileStream removeResourceStream;
            Dictionary<string, IHotelBedsDataTable> removeTables;
            List<string> removeFileHotels;

            DataResource.TryRemove(fileName, out removeResource);
            DataFiles.TryRemove(fileName, out removeResourceStream);
            DataTables.TryRemove(fileName, out removeTables);
            FileHotels.TryRemove(fileName, out removeFileHotels);
        }

        public void Dispose(string filterFileName)
        {
            if (!String.IsNullOrEmpty(filterFileName))
            {
                if (DataFiles.ContainsKey(filterFileName))
                {
                    FileStream file;
                    DataFiles.TryGetValue(filterFileName, out file);
                    file.Close();
#if !DEBUG
                    File.Delete(file.Name);
#endif
                    Dictionary<string, IHotelBedsDataTable> tables;
                    DataTables.TryGetValue(filterFileName, out tables);
                    if (tables != null && tables.Count > 0)
                    {
                        foreach (var tableName in tables.Keys)
                        {
                            tables[tableName].Clear();
                        }
                    }
                }
            }
            else
            {
                foreach (var fileName in DataFiles.Keys)
                {
                    FileStream file;
                    DataFiles.TryGetValue(fileName, out file);
                    file.Close();
#if !DEBUG
                    File.Delete(file.Name);
#endif
                    Dictionary<string, IHotelBedsDataTable> tables;
                    DataTables.TryGetValue(fileName, out tables);
                    if (tables != null && tables.Count > 0)
                    {
                        foreach (var tableName in tables.Keys)
                        {
                            tables[tableName].Clear();
                        }
                    }
                }
            }
        }

        private void HotelFiles(string fileName)
        {
            FileStream file;
            DataFiles.TryGetValue(fileName, out file);

            List<string> hotelList;
            FileHotels.TryGetValue(fileName, out hotelList);

            var reader = new StreamReader(file);
            //<hotelId>1234567</hotelId>
            var startTag = "<hotelId>";
            var endTag = "</hotelId>";
            var hotelCode = new StringBuilder();
            var maxCapacity = startTag.Length;
            var sb = new StringBuilder(maxCapacity);
            var startHotelId = false;
            var endHotelId = false;
            var buffer = new char[1];
            while (!endHotelId)
            {
                reader.Read(buffer, 0, 1);
                if (sb.Length == maxCapacity)
                {
                    sb.Remove(0, 1);
                }
                sb.Append(buffer);

                if (sb.Length >= (!startHotelId ? startTag.Length : endTag.Length))
                {
                    var compare = true;
                    if (!startHotelId)
                    {
                        for (var i = 0; i < startTag.Length; i++)
                        {
                            compare = sb[i].Equals(startTag[i]);
                            if (!compare)
                            {
                                break;
                            }
                        }
                        if (compare)
                        {
                            startHotelId = true;
                            maxCapacity = endTag.Length;
                        }
                    }
                    else
                    {
                        hotelCode.Append(buffer);
                        for (var i = 0; i < endTag.Length; i++)
                        {
                            compare = sb[i].Equals(endTag[i]);
                            if (!compare)
                            {
                                break;
                            }
                        }
                        if (compare)
                        {
                            endHotelId = true;
                            hotelCode.Remove(hotelCode.Length - maxCapacity, maxCapacity);
                        }
                    }
                }
            }
            var hotelId = hotelCode.ToString();
            hotelList.Add(hotelId);

            file.Seek(0, SeekOrigin.Begin);
        }

        private Dictionary<string, IHotelBedsDataTable> ReadTableContents(string fileName)
        {
            var dataTables = new Dictionary<string, IHotelBedsDataTable>();
            var dataTable = new HotelBedsDataTable();
            var tableName = Path.GetFileNameWithoutExtension(fileName);

            FileStream file;
            DataFiles.TryGetValue(fileName, out file);

            var doc = new XmlDocument();
            doc.Load(file);

            dataTable.Columns.Add("hotelId");
            dataTable.Columns.Add("city");
            dataTable.Columns.Add("address");
            dataTable.Columns.Add("postalCode");
            dataTable.Columns.Add("description");
            dataTable.Columns.Add("email");
            dataTable.Columns.Add("fax");
            dataTable.Columns.Add("phoneHotel");
            dataTable.Columns.Add("latitude");
            dataTable.Columns.Add("longitude");
            dataTable.Columns.Add("images");
            dataTable.Columns.Add("issues");
            dataTable.Columns.Add("facilities");
            dataTable.Columns.Add("distances");

            var row = dataTable.NewRow();
            var rootNode = "/establishmentHotelFactSheet/";
            var city = doc.SelectSingleNode(rootNode + "locations/city");
            var number = doc.SelectSingleNode(rootNode + "locations/number");
            var address = doc.SelectSingleNode(rootNode + "locations/address");
            var postalCode = doc.SelectSingleNode(rootNode + "locations/postalCode");
            var description = doc.SelectSingleNode(rootNode + "descriptions/descriptionHotel/description");
            var email = doc.SelectSingleNode(rootNode + "locations/email");
            var fax = doc.SelectSingleNode(rootNode + "locations/fax");
            var phoneHotel = doc.SelectSingleNode(rootNode + "locations/phoneHotel");
            var latitude = doc.SelectSingleNode(rootNode + "locations/latitude");
            var longitude = doc.SelectSingleNode(rootNode + "locations/longitude");
            var images = doc.SelectNodes(rootNode + "images/image");
            var issues = doc.SelectSingleNode(rootNode + "issues/issue");
            var facilities = doc.SelectSingleNode(rootNode + "facilities");
            var distances = doc.SelectSingleNode(rootNode + "distances");

            row["hotelId"] = doc.SelectSingleNode(rootNode + "hotelDetail/hotelId").FirstChild.Value.Trim();
            row["city"] = (city != null && city.HasChildNodes) ? city.FirstChild.Value.Trim() : String.Empty;
            row["address"] = String.Concat(" ", (number != null && number.HasChildNodes) ? number.FirstChild.Value.Trim() : String.Empty, (address != null && address.HasChildNodes) ? address.FirstChild.Value.Trim() : String.Empty);
            row["postalCode"] = (postalCode != null && postalCode.HasChildNodes) ? postalCode.FirstChild.Value.Trim() : String.Empty;
            row["description"] = (description != null && description.HasChildNodes) ? description.FirstChild.Value.Trim() : String.Empty;
            row["email"] = (email != null && email.HasChildNodes) ? email.FirstChild.Value.Trim() : String.Empty;
            row["fax"] = (fax != null && fax.HasChildNodes) ? fax.FirstChild.Value.Trim() : String.Empty;
            row["phoneHotel"] = (phoneHotel != null && phoneHotel.HasChildNodes) ? phoneHotel.FirstChild.Value.Trim() : String.Empty;
            row["latitude"] = (latitude != null && latitude.HasChildNodes) ? latitude.FirstChild.Value.Trim() : String.Empty;
            row["longitude"] = (longitude != null && longitude.HasChildNodes) ? longitude.FirstChild.Value.Trim() : String.Empty;

            var sortedImages = new SortedList<int, string>();
            for (var i = 0; i < images.Count; i++)
            {
                var image = images.Item(i);
                var ordenVisual = image.SelectSingleNode("ordenVisual");
                if (ordenVisual != null && ordenVisual.HasChildNodes)
                {
                    var sortId = Int32.Parse(ordenVisual.FirstChild.Value.Trim());
                    while (sortedImages.ContainsKey(sortId))
                    {
                        sortId++;
                    }
                    var path = image.SelectSingleNode("path");
                    sortedImages.Add(sortId, path.FirstChild.Value.Trim());
                }
            }
            if (sortedImages.Count > 0)
            {
                var imageStr = new StringBuilder();
                foreach (var sortId in sortedImages.Keys)
                {
                    imageStr.Append(sortId.ToString() + HotelBedsSettings.XmlColumnCharSeparator + sortedImages[sortId] + HotelBedsSettings.XmlRowCharSeparator);
                }
                imageStr.Remove(imageStr.Length - 1, 1);
                row["images"] = imageStr.ToString();
            }
            else
            {
                row["images"] = String.Empty;
            }

            row["issues"] = (issues != null && issues.HasChildNodes) ? "Y" : "N";

            if (facilities != null && facilities.HasChildNodes)
            {
                var facilityStr = new StringBuilder();
                for (var i = 0; i < facilities.ChildNodes.Count; i++)
                {
                    var nFac = facilities.ChildNodes.Item(i);
                    //roomFacility, servicesFacility, buildingFacility, hotelTypeFacility, credCardsFacility, mealsFacility, sportFacility, highLightFacility, distancesFacility, greenFacility
                    if (nFac.NodeType == XmlNodeType.Element && !nFac.Name.Equals("highLightFacility"))
                    {
                        var nFacGroupCode = nFac.SelectSingleNode("group").FirstChild.Value.Trim();
                        var nFacCode = nFac.SelectSingleNode("code").FirstChild.Value.Trim();
                        var facilityCode = nFacGroupCode + HotelBedsSettings.XmlColumnCharSeparator + nFacCode;
                        var keyFeature = HotelBedsSettings.MapFeatures.ContainsKey(facilityCode);
                        var keyDistance = !keyFeature && HotelBedsSettings.MapDistances.ContainsKey(facilityCode);
                        if (keyFeature || keyDistance)
                        {
                            var nFacDescription = String.Empty;
                            /*if (keyFeature)
                            {
                                nFacDescription = nFac.SelectSingleNode("description").FirstChild.Value.Trim();
                                var nFacRemark = nFac.SelectSingleNode("remark");
                                int intResult;
                                if (Int32.TryParse(nFacDescription, out intResult) && nFacRemark != null && nFacRemark.FirstChild != null && !String.IsNullOrEmpty(nFacRemark.FirstChild.Value))
                                {
                                    nFacDescription = nFacRemark.FirstChild.Value.Trim();
                                }
                                nFacDescription = nFacDescription.Replace(HotelBedsSettings.XmlColumnCharSeparator, '-').Replace(HotelBedsSettings.XmlRowCharSeparator, '-');
                            }*/
                            if (keyDistance)
                            {
                                var nDistance = nFac.SelectSingleNode("distance");
                                if (nDistance != null && nDistance.FirstChild != null && !String.IsNullOrEmpty(nDistance.FirstChild.Value))
                                {
                                    nFacDescription = nDistance.FirstChild.Value.Trim();
                                }
                                else
                                {
                                    //Distances are mandatory with a correct value
                                    continue;
                                }
                            }
                            facilityStr.Append(facilityCode + HotelBedsSettings.XmlColumnCharSeparator + nFacDescription + HotelBedsSettings.XmlRowCharSeparator);
                        }
                    }
                }
                if (facilityStr.Length > 0)
                {
                    facilityStr.Remove(facilityStr.Length - 1, 1);
                }
                row["facilities"] = facilityStr.ToString();
            }
            else
            {
                row["facilities"] = String.Empty;
            }

            if (distances != null && distances.HasChildNodes)
            {
                var distanceStr = new StringBuilder();
                for (var i = 0; i < distances.ChildNodes.Count; i++)
                {
                    var nDist = distances.ChildNodes.Item(i);
                    //distanceAirPort
                    if (nDist.NodeType == XmlNodeType.Element)
                    {
                        var strKM = nDist.SelectSingleNode("distanceKM").FirstChild.Value.Trim();
                        decimal distanceKM;
                        if (!decimal.TryParse(strKM, out distanceKM))
                            distanceKM = Decimal.Parse("0" + distanceKM, NumberFormatInfo.InvariantInfo);

                        var strMI = nDist.SelectSingleNode("distanceMI").FirstChild.Value.Trim();
                        decimal distanceMI;
                        if (!decimal.TryParse(strMI, out distanceMI))
                            distanceMI = Decimal.Parse("0" + distanceMI, NumberFormatInfo.InvariantInfo);

                        var finalDistance = distanceKM > 0 ? distanceKM : distanceMI > 0 ? distanceMI * Convert.ToDecimal(1.609344) : 0;
                        distanceStr.Append(Decimal.Round(finalDistance, 2, MidpointRounding.AwayFromZero).ToString(NumberFormatInfo.InvariantInfo) + HotelBedsSettings.XmlRowCharSeparator);
                    }
                }
                if (distanceStr.Length > 0)
                {
                    distanceStr.Remove(distanceStr.Length - 1, 1);
                }
                row["distances"] = distanceStr.ToString();
            }
            else
            {
                row["distances"] = String.Empty;
            }

            dataTable.Rows.Add(row);

            dataTables.Add(tableName, dataTable);
            return dataTables;
        }
    }
}
