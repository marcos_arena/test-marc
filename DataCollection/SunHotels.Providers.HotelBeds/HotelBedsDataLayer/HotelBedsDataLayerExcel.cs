﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.IO;

namespace SunHotels.Providers
{
    public class HotelBedsDataLayerExcel : IHotelBedsDataLayer
    {
        public HotelBedsSettings.ReaderType Type { get; private set; }
        public ConcurrentDictionary<string, Enum> DataResource { get; private set; }
        public ConcurrentDictionary<string, FileStream> DataFiles { get; private set; }
        public ConcurrentDictionary<string, Dictionary<string, IHotelBedsDataTable>> DataTables { get; private set; }
        public ConcurrentDictionary<string, List<string>> FileHotels { get; private set; }

        public HotelBedsDataLayerExcel()
        {
            Type = HotelBedsSettings.ReaderType.Excel;
            DataResource = new ConcurrentDictionary<string, Enum>();
            DataFiles = new ConcurrentDictionary<string, FileStream>();
            DataTables = new ConcurrentDictionary<string, Dictionary<string, IHotelBedsDataTable>>();
            FileHotels = new ConcurrentDictionary<string, List<string>>();
        }

        public bool LoadBasicDataFile(string fileName, Enum resource, FileStream resourceStream)
        {
            bool fileOk = true;
            try
            {
                DataResource.TryAdd(fileName, resource);
                DataFiles.TryAdd(fileName, resourceStream);
                FileHotels.TryAdd(fileName, new List<string>());
            }
            catch (Exception e)
            {
                Console.WriteLine("File {0} can't be open ({1}): {2}", fileName, Enum.GetName(typeof(HotelBedsSettings.ReaderType), Type), e.Message);
                Console.WriteLine(e.StackTrace);
                fileOk = false;
                LoadExceptionHandler(fileName);
            }
            return fileOk;
        }

        public bool LoadDataFile(string fileName)
        {
            bool fileOk = true;
            try
            {
                FileStream resourceStream;
                DataFiles.TryGetValue(fileName, out resourceStream);

                using (var connection = new OleDbConnection())
                {
                    var connectionString = String.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=\"Excel 12.0; HDR=YES; IMEX=1;\"", resourceStream.Name);

                    var sheetTables = new Dictionary<string, string>();
                    var sheetColumns = new Dictionary<string, List<string>>();

                    FileConfiguration(fileName, ref connectionString, sheetTables, sheetColumns);

                    connection.ConnectionString = connectionString;
                    connection.Open();

                    var tables = ReadTables(fileName, connection, sheetTables, sheetColumns);

                    DataTables.TryAdd(fileName, tables);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("File {0} can't be open ({1}): {2}", fileName, Enum.GetName(typeof(HotelBedsSettings.ReaderType), Type), e.Message);
                Console.WriteLine(e.StackTrace);
                fileOk = false;
                LoadExceptionHandler(fileName);
            }
            return fileOk;
        }

        public void LoadExceptionHandler(string fileName)
        {
            Dispose(fileName);

            Enum removeResource;
            FileStream removeResourceStream;
            Dictionary<string, IHotelBedsDataTable> removeTables;
            List<string> removeFileHotels;

            DataResource.TryRemove(fileName, out removeResource);
            DataFiles.TryRemove(fileName, out removeResourceStream);
            DataTables.TryRemove(fileName, out removeTables);
            FileHotels.TryRemove(fileName, out removeFileHotels);
        }

        public void Dispose(string filterFileName)
        {
            if (!String.IsNullOrEmpty(filterFileName))
            {
                if (DataFiles.ContainsKey(filterFileName))
                {
                    FileStream file;
                    DataFiles.TryGetValue(filterFileName, out file);
                    file.Close();
#if !DEBUG
                    File.Delete(file.Name);
#endif
                    DisposeExtra(filterFileName, file);

                    Dictionary<string, IHotelBedsDataTable> tables;
                    DataTables.TryGetValue(filterFileName, out tables);
                    if (tables != null && tables.Count > 0)
                    {
                        foreach (var tableName in tables.Keys)
                        {
                            tables[tableName].Clear();
                        }
                    }
                }
            }
            else
            {
                foreach (var fileName in DataFiles.Keys)
                {
                    FileStream file;
                    DataFiles.TryGetValue(fileName, out file);
                    file.Close();
#if !DEBUG
                    File.Delete(file.Name);
#endif
                    DisposeExtra(fileName, file);

                    Dictionary<string, IHotelBedsDataTable> tables;
                    DataTables.TryGetValue(fileName, out tables);
                    if (tables != null && tables.Count > 0)
                    {
                        foreach (var tableName in tables.Keys)
                        {
                            tables[tableName].Clear();
                        }
                    }
                }
            }
        }

        private void DisposeExtra(string fileName, FileStream file)
        {
            switch (fileName)
            {
                case "New_Hotel_List_ING.xls":
                case "New_Hotel_List_DAN.xls":
                case "New_Hotel_List_ALE.xls":
                case "New_Hotel_List_FRA.xls":
                case "New_Hotel_List_CAS.xls":
                case "New_Hotel_List_ITA.xls":
                case "New_Hotel_List_POR.xls":
                    var fileFullLocation = file.Name;
                    var destinationFile = String.Concat(fileFullLocation, "x");
                    if (File.Exists(destinationFile))
                    {
                        File.Delete(destinationFile);
                    }
                    break;
            }
        }

        private void FileConfiguration(string fileName, ref string connectionString, Dictionary<string, string> sheetTables, Dictionary<string, List<string>> sheetColumns)
        {
            var tableName = Path.GetFileNameWithoutExtension(fileName);
            List<string> columnsList;

            switch (fileName)
            {
                case "Destinations_ING.xls":
                case "Destinations_CAS.xls":
                    sheetTables.Add(String.Concat(tableName, "_H$"), tableName);
                    columnsList = new List<string> { "Country Code", "Country Name", "Destination Code", "Destination Name", "Zone Code", "Zone Name" };
                    sheetColumns.Add(tableName, columnsList);
                    break;

                case "Destinations_ITA.xls":
                case "Destinations_POR.xls":
                    sheetTables.Add(String.Concat(tableName, "$"), tableName);
                    columnsList = new List<string> { "Country Code", "Country Name", "Destination Code", "Destination Name", "Zone Code", "Zone Name" };
                    sheetColumns.Add(tableName, columnsList);
                    break;

                case "New_Hotel_List_ING.xls":
                case "New_Hotel_List_DAN.xls":
                case "New_Hotel_List_ALE.xls":
                case "New_Hotel_List_FRA.xls":
                case "New_Hotel_List_CAS.xls":
                case "New_Hotel_List_ITA.xls":
                case "New_Hotel_List_POR.xls":
                    FileStream file;
                    DataFiles.TryGetValue(fileName, out file);
                    var fileFullLocation = file.Name;
                    var destinationFile = String.Concat(fileFullLocation, "x");
                    File.Copy(fileFullLocation, destinationFile, true);
                    connectionString = connectionString.Replace(fileFullLocation, destinationFile);

                    sheetTables.Add(String.Concat(tableName, "$"), tableName);
                    columnsList = new List<string> { "Hotel Code", "Hotel Name", "Category Code", "Country Code", "Destination Code", "Zone Code" };
                    sheetColumns.Add(tableName, columnsList);
                    break;
            }
        }

        private Dictionary<string, IHotelBedsDataTable> ReadTables(string fileName, OleDbConnection connection, Dictionary<string, string> sheetTables, Dictionary<string, List<string>> sheetColumns)
        {
            var dataTables = new Dictionary<string, IHotelBedsDataTable>();
            foreach (var sheetName in sheetTables.Keys)
            {
                var tableName = sheetTables[sheetName];
                var columns = "*";
                int number;
                if (sheetColumns[tableName].Count > 0 && !int.TryParse(sheetColumns[tableName][0], out number))
                {
                    columns = "[" + String.Join("],[", sheetColumns[tableName]) + "]";
                }

                var dataTable = new HotelBedsDataTable();
                using (var command = new OleDbCommand())
                {
                    command.Connection = connection;
                    command.CommandText = "SELECT " + columns + " FROM [" + sheetName + "]";

                    using (var excelDataTable = new DataTable())
                    {
                        using (var dataAdapter = new OleDbDataAdapter())
                        {
                            dataAdapter.SelectCommand = command;
                            dataAdapter.Fill(excelDataTable);
                        }

                        if (excelDataTable.Columns.Count == 0)
                        {
                            Console.WriteLine("No columns found in excel file: " + connection.ConnectionString);
                        }
                        //Import columns
                        for (var i = 0; i < excelDataTable.Columns.Count; i++)
                        {
                            dataTable.Columns.Add(excelDataTable.Columns[i].ColumnName);
                        }
                        if (excelDataTable.Rows.Count == 0)
                        {
                            Console.WriteLine("No Rows found in excel file: " + connection.ConnectionString);
                        }
                        //Import rows
                        for (var i = 0; i < excelDataTable.Rows.Count; i++)
                        {
                            var row = dataTable.NewRow();
                            for (var j = 0; j < dataTable.Columns.Count; j++)
                            {
                                var dbValue = excelDataTable.Rows[i][j];
                                var value = (DBNull.Value != dbValue && !String.IsNullOrEmpty((string)dbValue))
                                    ? ((string)dbValue).Trim()
                                    : String.Empty;
                                row[dataTable.Columns[j]] = value;
                            }
                            dataTable.Rows.Add(row);
                        }
                    }
                }

                dataTables.Add(tableName, dataTable);
            }
            return dataTables;
        }
    }
}
