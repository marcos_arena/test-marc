﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;

namespace SunHotels.Providers
{
    public class HotelBedsDataLayerCsv : IHotelBedsDataLayer
    {
        public HotelBedsSettings.ReaderType Type { get; private set; }
        public ConcurrentDictionary<string, Enum> DataResource { get; set; }
        public ConcurrentDictionary<string, FileStream> DataFiles { get; set; }
        public ConcurrentDictionary<string, Dictionary<string, IHotelBedsDataTable>> DataTables { get; set; }
        public ConcurrentDictionary<string, List<string>> FileHotels { get; private set; }

        public HotelBedsDataLayerCsv()
        {
            Type = HotelBedsSettings.ReaderType.Csv;
            DataResource = new ConcurrentDictionary<string, Enum>();
            DataFiles = new ConcurrentDictionary<string, FileStream>();
            DataTables = new ConcurrentDictionary<string, Dictionary<string, IHotelBedsDataTable>>();
            FileHotels = new ConcurrentDictionary<string, List<string>>();
        }

        public bool LoadBasicDataFile(string fileName, Enum resource, FileStream resourceStream)
        {
            bool fileOk = true;
            try
            {
                DataResource.TryAdd(fileName, resource);
                DataFiles.TryAdd(fileName, resourceStream);
                FileHotels.TryAdd(fileName, new List<string>());
            }
            catch (Exception e)
            {
                Console.WriteLine("File {0} can't be open ({1}): {2}", fileName, Enum.GetName(typeof(HotelBedsSettings.ReaderType), Type), e.Message);
                Console.WriteLine(e.StackTrace);
                fileOk = false;
                LoadExceptionHandler(fileName);
            }
            return fileOk;
        }

        public bool LoadDataFile(string fileName)
        {
            bool fileOk = true;
            try
            {
                FileStream resourceStream;
                DataFiles.TryGetValue(fileName, out resourceStream);

                var header = false;
                var fieldCharSplit = '|';

                var lines = new List<string>();
                using (var reader = new StreamReader(resourceStream))
                {
                    string line;
                    while ((line = reader.ReadLine()) != null)
                    {
                        lines.Add(line);
                    }
                }

                var fields = lines[0].Split(fieldCharSplit);
                var cols = fields.GetLength(0);

                var dataTable = new HotelBedsDataTable();

                for (var i = 0; i < cols; i++)
                {
                    dataTable.Columns.Add(header ? fields[i].ToLowerInvariant() : i.ToString());
                }

                Dictionary<string, string> row;
                for (var i = (header ? 1 : 0); i < lines.Count; i++)
                {
                    fields = lines[i].Split(fieldCharSplit);
                    row = dataTable.NewRow();
                    for (var f = 0; f < cols; f++)
                    {
                        row[dataTable.Columns[f]] = fields[f].Trim();
                    }
                    dataTable.Rows.Add(row);
                }

                var tableName = Path.GetFileNameWithoutExtension(fileName);
                DataTables.TryAdd(fileName, new Dictionary<string, IHotelBedsDataTable> { { tableName, dataTable } });
            }
            catch (Exception e)
            {
                Console.WriteLine("File {0} can't be open ({1}): {2}", fileName, Enum.GetName(typeof(HotelBedsSettings.ReaderType), Type), e.Message);
                Console.WriteLine(e.StackTrace);
                fileOk = false;
                LoadExceptionHandler(fileName);
            }
            return fileOk;
        }

        public void LoadExceptionHandler(string fileName)
        {
            Dispose(fileName);

            Enum removeResource;
            FileStream removeResourceStream;
            Dictionary<string, IHotelBedsDataTable> removeTables;
            List<string> removeFileHotels;

            DataResource.TryRemove(fileName, out removeResource);
            DataFiles.TryRemove(fileName, out removeResourceStream);
            DataTables.TryRemove(fileName, out removeTables);
            FileHotels.TryRemove(fileName, out removeFileHotels);
        }

        public void Dispose(string filterFileName)
        {
            if (!String.IsNullOrEmpty(filterFileName))
            {
                if (DataFiles.ContainsKey(filterFileName))
                {
                    FileStream file;
                    DataFiles.TryGetValue(filterFileName, out file);
                    file.Close();
#if !DEBUG
                    File.Delete(file.Name);
#endif
                    Dictionary<string, IHotelBedsDataTable> tables;
                    DataTables.TryGetValue(filterFileName, out tables);
                    if (tables != null && tables.Count > 0)
                    {
                        foreach (var tableName in tables.Keys)
                        {
                            tables[tableName].Clear();
                        }
                    }
                }
            }
            else
            {
                foreach (var fileName in DataFiles.Keys)
                {
                    FileStream file;
                    DataFiles.TryGetValue(fileName, out file);
                    file.Close();
#if !DEBUG
                    File.Delete(file.Name);
#endif
                    Dictionary<string, IHotelBedsDataTable> tables;
                    DataTables.TryGetValue(fileName, out tables);
                    if (tables != null && tables.Count > 0)
                    {
                        foreach (var tableName in tables.Keys)
                        {
                            tables[tableName].Clear();
                        }
                    }
                }
            }
        }
    }
}
