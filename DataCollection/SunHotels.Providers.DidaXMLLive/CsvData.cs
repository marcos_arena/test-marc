﻿using System;
using System.Text;
using System.Data;
using System.IO;

namespace SunHotels.Providers
{
    public class CsvData 
    {
        public CsvData()
        {
            TableName = "CSVData";
            SeparatorChar = ';';
            QuoteString = null;
        }

        public char SeparatorChar { get; set; }

        public string TableName { get; set; }

        public string QuoteString { get; set; }

        /// <summary>
        /// Reads the file that is given as a parameter to a <c>DataTable</c>.
        /// The table name can be set as a property on this object. The separator char can also be set as a property on this object.
        /// 
        /// Encoding of the file that is read must be windows-1252.
        /// </summary>
        public DataTable Read(string fileName)
        {
            return Read(fileName, Encoding.GetEncoding(1252));
        }

        /// <summary>
        /// Reads the file that is given as a parameter to a <c>DataTable</c>.
        /// The table name can be set as a property on this object. The separator char can also be set as a property on this object.
        /// </summary>
        public DataTable Read(string fileName, Encoding encoding)
        {
            var dataTable = new DataTable();
            var fi = new FileInfo(fileName);
            TableName = Path.GetFileNameWithoutExtension(fi.Name);
            dataTable.TableName = TableName;
            using (var streamIn = new StreamReader(fileName, encoding))
            {
                var line = streamIn.ReadLine();
                if (line != null)
                {
                    var headers = line.Split(SeparatorChar);
                    foreach (var header in headers)
                    {
                        var headerText = header.Trim();
                        try
                        {
                            dataTable.Columns.Add(headerText, Type.GetType("System.String"));
                        }
                        catch
                        {
                            throw new Exception($"A column with the name \"{header}\" was found multiple times in the file {TableName} .");
                        }
                    }
                }
                //Read the rest of the rows.
                while (!streamIn.EndOfStream)
                {
                    var nline = streamIn.ReadLine();
                    if (nline == null || nline.Replace(SeparatorChar.ToString(), "").Trim().Length == 0) continue;
                    var values = nline.Split(SeparatorChar);
                    var row = dataTable.NewRow();

                    for (int i = 0; i < values.Length; i++)
                    {
                        // Remove values in a position higher then the headers. Should this throw a exception? A setting for this maybe?
                        if (i >= dataTable.Columns.Count) continue;
                        var value = values[i].Trim();
                        // Skip empty lines.
                        if (value.Length > 0)
                        {
                            row[i] = value;
                        }
                    }
                    dataTable.Rows.Add(row);
                }
                streamIn.Close();
                streamIn.Dispose();
            }
            return dataTable;
        }
    }
}


