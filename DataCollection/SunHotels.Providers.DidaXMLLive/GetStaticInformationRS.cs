﻿using System.Xml.Serialization;
namespace SunHotels.Providers
{
    [XmlRoot(ElementName = "GetStaticInformationRS")]
    public class GetStaticInformationRs
    {
        [XmlElement(ElementName = "Url")]
        public string Url { get; set; }
    }
}
