﻿using System;
using System.Net;
using System.Threading;

namespace SunHotels.Providers
{
    public class WebClientEx : WebClient
    {
        /// <summary>
        /// Gets or sets the time out in milliseconds.
        /// </summary>
        public int TimeOut { get; set; }

        public string ProtocolVersion { get; set; }
        public string ContentType { get; set; }
        public string Accept { get; set; }
        public bool KeepAlive { get; set; }

        public CancellationToken CancellationToken;

        protected override WebRequest GetWebRequest(Uri uri)
        {
            var request = (HttpWebRequest)base.GetWebRequest(uri);
            if (request == null)
                return null;
            request.Timeout = TimeOut;
            // automatically add support for deflate..
            request.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
            request.KeepAlive = KeepAlive;

            if (!string.IsNullOrEmpty(Accept))
                request.Accept = Accept;

            if (!string.IsNullOrEmpty(ContentType))
                request.ContentType = ContentType;

            request.ProtocolVersion = ProtocolVersion.Equals("1.1") ? HttpVersion.Version11 : HttpVersion.Version10;
            request.ServicePoint.Expect100Continue = false;
            CancellationToken.Register(() =>
            {
                request.Abort();
            });
            return request;

        }
    }
}
