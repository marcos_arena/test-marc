﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using SunHotels.XML;
using SunHotels.XML.Data;
using SunHotels.Providers.ProviderCommon.DataLoader;
using System.Xml.Linq;
using Hotel = SunHotels.XML.Data.Hotel;
using System.Threading.Tasks;
using System.Threading;
using System.Net;
using System.Data;
using Empir.Data.CSV;

namespace SunHotels.Providers
{
    public class DidaXMLLive : XMLProvider, IStaticDataLogger
    {
        #region Global Variable Declaration

        // Create dictionaries for fast search
        private readonly Dictionary<string, Place> _allPlaces = new Dictionary<string, Place>();
        private readonly Dictionary<string, Hotel> _allHotels = new Dictionary<string, Hotel>();
        private DidaXMLLiveSettings _didaXmlLiveSettings = new DidaXMLLiveSettings();

        #endregion

        #region XMLProvider Implementation Methods

        /// <summary>
        /// getProviderDefinition
        /// </summary>
        /// <param name="root">Root</param>
        protected override void getProviderDefinition(Root root)
        {
            root.ProviderDefinition.Name = configuration.ProviderName;
            root.ProviderDefinition.Currency = configuration.Currency;
        }

        /// <summary>
        /// Update Configuration
        /// </summary>
        /// <param name="configuration">Configuration</param>
        protected override void UpdateConfiguration(Sunhotels.Export.Configuration configuration)
        {
            configuration.WriteHotelsWithoutRooms = true;
            configuration.WriteRoomsWithoutAvailability = true;
        }

        /// <summary>
        /// Entry point for class.
        /// </summary>
        /// <param name="root">Root for Sunhotels XML structure.</param>
        protected override void getData(Root root)
        {
            try
            {
                this.root = root;
                // ------------------------------------------------------------------------------------------------
                // Create log stream
                // ------------------------------------------------------------------------------------------------
                try
                {
                    Directory.CreateDirectory(configuration.LogOutput);
                }
                catch (Exception ex)
                {
                    throw new ApplicationException(string.Join(": ", configuration.ProviderName, "Unable to create log file."), ex);
                }

                AddToConsole(string.Join(": ", configuration.ProviderName, "Begin static data import."));
                Log(ILoggerStaticDataSeverity.Info, string.Join(": ", configuration.ProviderName, "Begin static data import."));

                // Get all static data. If files are valid and able to process then this method will return true.
                if (!GetStaticData()) return;
                AddToConsole(string.Join(": ", configuration.ProviderName, "Begin processing list of hotels and places from available static data."));
                Log(ILoggerStaticDataSeverity.Info, string.Join(": ", configuration.ProviderName, "Begin processing list of hotels and places from available static data."));
                foreach (var item in _allHotels)
                    _allPlaces[item.Value.PlaceId].Hotels.Add(item.Value.Id, item.Value);
                AddToConsole(string.Join(": ", configuration.ProviderName, "Finished processing list of hotels and places from available static data."));
                Log(ILoggerStaticDataSeverity.Info, string.Join(": ", configuration.ProviderName, "Finished processing list of hotels and places from available static data."));
            }
            catch (Exception ex)
            {
                Log(ILoggerStaticDataSeverity.Error, string.Join(string.Empty, configuration.ProviderName, ": Unhanded exception while getting data for static import: [", ex.ToString(), "]"));
                throw;
            }
        }

        /// <summary>
        /// Get Global Feature mapping for the provider
        /// </summary>
        /// <param name="root">Root</param>
        protected override void getFeatureTypes(Root root)
        {
            //Feature is not available as part of the Dida API response.
        }

        /// <summary>
        /// Log function
        /// </summary>
        /// <param name="severity">ILoggerStaticDataSeverity</param>
        /// <param name="message">message</param>
        /// <param name="args">arguments</param>
        public void Log(ILoggerStaticDataSeverity severity, string message, params object[] args)
        {
            var s = XML.HelpClasses.Log.MessageType.Informational;
            switch (severity)
            {
                case ILoggerStaticDataSeverity.Error:
                    s = XML.HelpClasses.Log.MessageType.Error;
                    break;
                case ILoggerStaticDataSeverity.Warning:
                    s = XML.HelpClasses.Log.MessageType.Warning;
                    break;
            }

            LogEntry(s, message, args);
        }

        #endregion

        #region Private 

        #region Private Boolean

        /// <summary>
        /// Get all static data from text files and bind with Dictionary objects.
        /// </summary>              
        private bool GetStaticData()
        {
            try
            {
                //Loading settings information
                _didaXmlLiveSettings = GetDidaXmlLiveSettings();

                //Downloading data file
                var filepath = DownloadStaticData();
                DataTable dt = null;
                if (!string.IsNullOrEmpty(filepath))
                    dt = ConvertDataTable(filepath);
                if (dt?.Rows == null)
                {
                    Log(ILoggerStaticDataSeverity.Info, string.Join(": ", configuration.ProviderName, "No data available to process."));
                    return false;
                }
                Parallel.ForEach(dt.AsEnumerable(), dr =>
                {
                    var hotelname = dr["Name"].ToString();
                    var hotelnamecn = dr["Name_CN"].ToString();
                    var hotelId = dr["HotelID"].ToString();
                    var cityname = dr["CityName"].ToString();
                    var citycode = dr["CityCode"].ToString();
                    var state = dr["StateCode"].ToString();
                    var countryname = dr["CountryName"].ToString();
                    var countrycode = dr["CountryCode"].ToString();
                    var address = dr["Address"].ToString();
                    var zipCode = dr["ZipCode"].ToString();
                    var longitude = dr["Longitude"].ToString();
                    var latitude = dr["Latitude"].ToString();
                    var starRating = dr["StarRating"].ToString();
                    var telephone = dr["Telephone"].ToString();

                    //Create the place
                    var place = GetPlace(countryname, countrycode, state, cityname, citycode);
                    if (place == null)
                        return;
                    var hotel = new Hotel
                    {
                        Id = hotelId,
                        PlaceId = place.Id,
                        Name = hotelname,
                        AccomodationType = AccomodationType.Hotel,
                        Headline = hotelname,
                        Description = hotelname,
                        Adress_Street1 = address,
                        Adress_Zipcode = zipCode,
                        Adress_City = cityname,
                        Adress_State = state,
                        Adress_Country = countryname,
                        Position_Latitude = latitude.Round(_didaXmlLiveSettings.GeoCodeDecimals),
                        Position_Longitude = longitude.Round(_didaXmlLiveSettings.GeoCodeDecimals),
                        Phone = telephone,
                        Fax = string.Empty,
                        Email = string.Empty,
                        Classification = starRating,
                        BestBuy = false,
                        AdultOnly = false,
                        BlockInfant = false,
                        Identifiers = new[] { new Identifier { Type = _didaXmlLiveSettings.IdentifierType, Value = configuration.ProviderName } }.ToList(),
                        Translations = new Translations
                        {
                            headline = new List<TranslationsHeadline>
                            {
                                new TranslationsHeadline {country = countrycode, lang = configuration.Language, Value = hotelname},
                                new TranslationsHeadline {country = countrycode, lang = _didaXmlLiveSettings.ChineseLangCode, Value = hotelnamecn}
                            },
                            description = new List<TranslationsDescription>
                            {
                                new TranslationsDescription {country = countrycode, lang = configuration.Language, Value = hotelname},
                                new TranslationsDescription {country = countrycode, lang = _didaXmlLiveSettings.ChineseLangCode, Value = hotelnamecn}
                            }
                        }
                    };

                    Hotel currentHotel;
                    if (!_allHotels.TryGetValue(hotel.Id, out currentHotel))
                    {
                        lock (_allHotels)
                            _allHotels.Add(hotel.Id, hotel);
                    }
                    else
                        Log(ILoggerStaticDataSeverity.Info, string.Join(": ", configuration.ProviderName, "The hotel with id", hotel.Id, " already exists in the hotel list."));
                });

                //Deleting data file after processing based on the configuration value
                if (_didaXmlLiveSettings.DeleteFileAfterProcessing)
                    FileDelete(filepath);

                return true;
            }
            catch (Exception ex)
            {
                Log(ILoggerStaticDataSeverity.Error, string.Join(string.Empty, configuration.ProviderName, ": Error occurred while processing Hotel details", ". Error: ", ex.Message));
                return false;
            }
        }

        #endregion

        #region Private void

        /// <summary>
        /// File Delete
        /// </summary>
        /// <param name="filePath">filePath</param>
        private void FileDelete(string filePath)
        {
            try
            {
                if (!File.Exists(filePath)) return;
                File.Delete(filePath);
                Log(ILoggerStaticDataSeverity.Info, string.Join(": ", configuration.ProviderName, "Deleted static data file successfully. File Path", filePath));
            }
            catch (Exception ex)
            {
                Log(ILoggerStaticDataSeverity.Error, string.Join(": ", configuration.ProviderName, "Error occurred while deleting static data file. File Path", filePath, ". Error", ex.Message));
            }
        }

        /// <summary>
        /// Add message to Console
        /// </summary>
        /// <param name="message">message</param>
        private static void AddToConsole(string message)
        {
#if DEBUG
            Console.WriteLine(message);
#endif
        }

        #endregion

        #region String

        /// <summary>
        /// Download Static Data
        /// </summary>
        /// <returns>File path</returns>
        private string DownloadStaticData()
        {
            var filepath = string.Empty;
            try
            {
                Log(ILoggerStaticDataSeverity.Info, string.Join(": ", configuration.ProviderName, "Fetching URL for download of CSV file containing static data."));
                var apiResponse = GetApiResponse(_didaXmlLiveSettings, configuration.UserId, configuration.Password).Result;

                //Replace "&" with "&amp;" to avoid de-serialization problem 
                if (apiResponse.Contains("&"))
                    apiResponse = apiResponse.Replace("&", "&amp;");

                var staticInfoRs = GeneralUtilities.DeserializeXml<GetStaticInformationRs>(apiResponse);
                
                if (!string.IsNullOrEmpty(staticInfoRs?.Url))
                {
                    using (var client = new WebClient())
                    {
                        if (!Directory.Exists(configuration.LocalTemp))
                            Directory.CreateDirectory(configuration.LocalTemp);
                        var filename = DateTime.Now.ToString(_didaXmlLiveSettings.CsvFileNameFormat);
                        filepath = string.Join(string.Empty, configuration.LocalTemp, @"\", _didaXmlLiveSettings.StaticType, "-", filename, _didaXmlLiveSettings.FileExtensions);
                        Log(ILoggerStaticDataSeverity.Info, string.Join(": ", configuration.ProviderName, "Preparing to download CSV file containing static data."));
                        Log(ILoggerStaticDataSeverity.Info, string.Join(": ", configuration.ProviderName, "Download URL", staticInfoRs.Url));
                        
                        //Download File into local
                        client.DownloadFile(new Uri(staticInfoRs.Url), filepath);
                        Log(ILoggerStaticDataSeverity.Info, string.Join(": ", configuration.ProviderName, "Completed download of CSV file containing static data at", filepath));
                    }
                }
            }
            catch (Exception ex)
            {
                Log(ILoggerStaticDataSeverity.Error, string.Join(string.Empty, configuration.ProviderName, ": Error while downloading CSV file into local.", " Error: ", ex.Message));
            }
            return filepath;
        }

        /// <summary>
        /// Get DIDA StaticInformation API response
        /// </summary>
        /// <param name="didaXmlLiveSettings">DidaXmlLiveSettings</param>
        /// <param name="clientId">clientId</param>
        /// <param name="licenseKey">licenseKey</param>
        /// <returns>Task of string</returns>
        private async Task<string> GetApiResponse(DidaXMLLiveSettings didaXmlLiveSettings, string clientId,
            string licenseKey)
        {
            try
            {
                var req = new GetStaticInformationRQ
                {
                    Header = new[]
                    {
                        new GetStaticInformationRQHeader
                        {
                            ClientID = clientId,
                            LicenseKey = licenseKey
                        }
                    },
                    StaticType = didaXmlLiveSettings.StaticType,
                    IsGetUrlOnly = didaXmlLiveSettings.IsGetUrlOnly
                };
                var api = new RestApiUtils
                {
                    ProtocolVersion = didaXmlLiveSettings.ProtocolVersion,
                    RequestContentType = didaXmlLiveSettings.RequestContentType,
                    PostParameter = GeneralUtilities.GetXmlFromObject(req),
                    RequestUrl = configuration.WebServiceUrl
                };
                var token = new CancellationTokenSource();
                var response = await api.Post(token.Token).ConfigureAwait(false);
                if (response.ToUpper().Contains("ERROR"))
                {
                    throw new ApplicationException(string.Join(" : ", "Error returned in response", response));
                }
                return response;
            }
            catch (Exception ex)
            {
                Log(ILoggerStaticDataSeverity.Error, string.Join(string.Empty, configuration.ProviderName, ": Error while Getting GetStaticInformation from DIDA API.", " Error: ", ex.Message));
                return null;
            }

        }

        #endregion

        #region Get Type

        /// <summary>
        /// Convert to DataTable from CSV
        /// </summary>
        /// <param name="filepath">file path</param>
        /// <returns>Data Table</returns>
        private DataTable ConvertDataTable(string filepath)
        {
            try
            {
                var csvData = new CSVData
                {
                    SEPARATOR_CHAR = _didaXmlLiveSettings.CsvSeparator[0]
                };
                return csvData.Read(filepath);
            }
            catch (Exception ex)
            {
                Log(ILoggerStaticDataSeverity.Error, string.Join(string.Empty, configuration.ProviderName, ": Error occurred while reading CSV file to DataTable at location: ", filepath, ". Error: ", ex.Message));
                return null;
            }
        }

        /// <summary>
        /// Get DidaXMLLiveSettings
        /// </summary>
        /// <param name="fileName">fileName</param>
        /// <returns>DidaXMLLiveSettings</returns>
        private DidaXMLLiveSettings GetDidaXmlLiveSettings(string fileName = "DidaXMLLiveSettings")
        {
            try
            {
                return GeneralUtilities.Deserialize<DidaXMLLiveSettings>(XDocument.Load(string.Join(string.Empty, configuration.ProviderConfigRoot, "\\", fileName, ".xml")));
            }
            catch (Exception ex)
            {
                Log(ILoggerStaticDataSeverity.Error, string.Join(string.Empty,
                    configuration.ProviderName, ": Error occurred while deserializing provider Live Settings xml file. File name: ",
                    fileName, ". Error: ", ex.Message));
                return null;
            }
        }

        /// <summary>
        /// Get previously created place or create new.
        /// </summary>
        /// <param name="countryName">country Name</param>
        /// <param name="countryCode">country Code</param>
        /// <param name="state">state</param>
        /// <param name="cityName">city Name</param>
        /// <param name="cityCode">city Code</param>        
        /// <returns>Place</returns>
        private Place GetPlace(string countryName, string countryCode, string state, string cityName, string cityCode)
        {
            try
            {
                if (string.IsNullOrEmpty(countryCode) || string.IsNullOrEmpty(cityCode))
                    return null;

                countryName = string.IsNullOrEmpty(countryName) ? countryCode : countryName;

                countryCode = countryCode.ToUpper().Trim();
                state = state.ToUpper().Trim();
                cityCode = cityCode.ToUpper().Trim();
                lock (_allPlaces)
                {
                    // Check or create country level place 
                    Place matchingCountryPlace;
                    if (!_allPlaces.TryGetValue(countryCode, out matchingCountryPlace))
                    {
                        var newPlace = new Place { Id = countryCode, Description = countryName };
                        _allPlaces.Add(countryCode, newPlace);
                        lock (root.Places)
                            root.Places.Add(newPlace);
                    }

                    bool validState = false;
                    if (!string.IsNullOrEmpty(state))
                    {
                        validState = true;
                        // Check or create state level place if country is specified
                        Place matchingStatePlace;
                        var statePlace = new Place { Id = state, Description = state };
                        if (!_allPlaces.TryGetValue(state, out matchingStatePlace))
                        {
                            _allPlaces.Add(state, statePlace);
                            if (!_allPlaces[countryCode].Places.Contains(statePlace))
                                _allPlaces[countryCode].Places.Add(statePlace);
                        }
                    }

                    // Check or create city level place if state is specified
                    Place matchingCityPlace;
                    if (_allPlaces.TryGetValue(cityCode, out matchingCityPlace)) return matchingCityPlace;

                    var cityplace = new Place { Id = cityCode, Description = cityName };
                    _allPlaces.Add(cityCode, cityplace);

                    var key = validState ? state : countryCode;
                    if (!_allPlaces[key].Places.Contains(cityplace))
                        _allPlaces[key].Places.Add(cityplace);
                }
                return _allPlaces[cityCode];
            }
            catch (Exception ex)
            {
                string errorMsg = string.Join(string.Empty, configuration.ProviderName, ": Error occurred while creating place with Country1: ", countryCode, ", Region:", state, " Resort: ", cityCode, ". Error: ", ex.Message);
                Log(ILoggerStaticDataSeverity.Error, errorMsg);
                return null;
            }
        }

        #endregion

        #endregion
    }
}
