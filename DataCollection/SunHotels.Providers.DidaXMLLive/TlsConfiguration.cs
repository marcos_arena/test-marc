﻿namespace SunHotels.Providers
{
    public class TlsConfiguration
    {
        /// <summary>
        /// Configures the SecurityProtocol in order to force the use of TLS and avoid using SSL3 (some providers have removed support to SSL3)
        /// </summary>
        public static void SetTlsConfiguration()
        {
            System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls;
        }

    }
}
