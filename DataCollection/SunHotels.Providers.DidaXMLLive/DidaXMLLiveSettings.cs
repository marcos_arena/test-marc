﻿namespace SunHotels.Providers
{
    public class DidaXMLLiveSettings
    {
        public string StaticType { get; set; }
        public string ProtocolVersion { get; set; }
        public string RequestContentType { get; set; }
        public string RequestUrl { get; set; }
        public string CsvFileNameFormat { get; set; }
        public string ChineseLangCode { get; set; }
        public int GeoCodeDecimals { get; set; }
        public string IdentifierType { get; set; }
        public bool DeleteFileAfterProcessing { get; set; }
        public string FileExtensions { get; set; }
        public bool IsGetUrlOnly { get; set; }
        public string CsvSeparator { get; set; }
    }
}



