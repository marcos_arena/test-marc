﻿using System.IO;
using System.Xml.Linq;
using System.Xml.Serialization;
using System.Xml;
using System.Globalization;
using System;
using System.Text;

namespace SunHotels.Providers
{
    public static class GeneralUtilities
    {
        #region Public Method

        /// <summary>
        /// Round the decimal value and convert back to string
        /// </summary>
        /// <param name="str">string</param>
        /// <param name="decimals">rounding decimals</param>
        /// <returns></returns>
        public static string Round(this string str, int decimals)
        {
            return string.IsNullOrEmpty(str) ? string.Empty :
                Math.Round(decimal.Parse(str, NumberStyles.Float | NumberStyles.AllowExponent, CultureInfo.InvariantCulture),
                decimals).ToString(CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// Get XML string From Object
        /// </summary>
        /// <param name="response">Object</param>
        /// <returns>XML string</returns>
        public static string GetXmlFromObject(object response)
        {
            var emptyNamespaces = new XmlSerializerNamespaces(new[] { XmlQualifiedName.Empty });
            var serializer = new XmlSerializer(response.GetType());
            var settings = new XmlWriterSettings
            {
                Indent = true,
                OmitXmlDeclaration = true
            };

            using (var stream = new StringWriter())
            using (var writer = XmlWriter.Create(stream, settings))
            {
                serializer.Serialize(writer, response, emptyNamespaces);
                return stream.ToString();
            }
        }

        /// <summary>
        /// This method De-serializes string to object
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="doc">XDocument</param>
        /// <returns>object</returns>
        public static T Deserialize<T>(XDocument doc)
        {
            var xmlSerializer = new XmlSerializer(typeof(T));
            if (doc.Root == null) return default(T);
            using (var reader = doc.Root.CreateReader())
                return (T)xmlSerializer.Deserialize(reader);
        }

        /// <summary>
        /// De-serialize XML
        /// </summary>
        /// <typeparam name="T">T as type</typeparam>
        /// <param name="pXmlizedString">XML string</param>
        /// <returns>T type</returns>
        public static T DeserializeXml<T>(string pXmlizedString) where T : class
        {
            MemoryStream memory = null;
            try
            {
                var serializer = new XmlSerializer(typeof(T));
                memory = new MemoryStream(StringToUtf8ByteArray(pXmlizedString));
                return (T)serializer.Deserialize(memory);
            }
            finally
            {
                if (memory != null)
                {
                    memory.Close();
                    memory.Dispose();
                }
            }
        }

        #endregion Public Method

        #region Private

        /// <summary>
        /// String To Utf8 Byte Array conversion 
        /// </summary>
        /// <param name="pXmlString">XML string</param>
        /// <returns>Byte array</returns>
        private static byte[] StringToUtf8ByteArray(string pXmlString)
        {
            var encoding = new UTF8Encoding();
            var byteArray = encoding.GetBytes(pXmlString);
            return byteArray;
        }

        #endregion
    }
}
