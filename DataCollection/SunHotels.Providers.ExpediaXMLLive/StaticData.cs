﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Xml;
using System.Linq;
using ICSharpCode.SharpZipLib.Zip;
using Empir.Data.CSV;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;

namespace SunHotels.Providers
{

    /// <summary>
    /// Class containing all static data 
    /// </summary>
    class StaticData
    {
        XmlDocument xmlExportConfig;
        string tmpWorkingDir;
        List<string> zipList;
        Dictionary<string, string> fileList = new Dictionary<string, string>(100);
        Dictionary<string, string> zipfiles = new Dictionary<string, string>();
        /// <summary>
        /// Class constructor.
        /// </summary>
        /// <param name="workingDir">Temporary working directory.</param>
        public StaticData(string workingDir, string providerConfigRoot)
        {
            if (!Directory.Exists(workingDir))
                Directory.CreateDirectory(workingDir);
            tmpWorkingDir = workingDir;
            xmlExportConfig = new XmlDocument();
            zipList = new List<string>();
            xmlExportConfig.Load(providerConfigRoot + "\\ExpediaXMLLive_ExportConfig.xml");
        }

        /// <summary>
        /// Retrieves all static data specified by ExportConfig.
        /// </summary>
        /// <returns></returns>
        public bool GetStaticData()
        {
            Download();
            UnzipFiles();
            return true;
        }

        private void Download()
        {
            try
            {
                XmlNodeList fileNodes = xmlExportConfig.SelectNodes("//Files/File");
                List<Task> tasks = new List<Task>();
                foreach (XmlNode file in fileNodes)
                {
                    if (file.Attributes["type"].Value == "online" && file.Attributes["IsActive"].Value == "1")
                    {
                        Task t = Task.Factory.StartNew(() =>
                          {
                              Download(file.Attributes["url"].Value, tmpWorkingDir + file.Attributes["FileName"].Value + ".zip");
                          });
                        tasks.Add(t);
                        zipfiles.Add(file.Attributes["url"].Value, tmpWorkingDir + file.Attributes["FileName"].Value + ".zip");
                    }
                }
                Task.WaitAll(tasks.ToArray(), Timeout.Infinite);

            }
            catch (Exception ex)
            {
                throw new ApplicationException(string.Format("Unable to download the files from given location.Expedia static data"), ex);
            }

        }
        public void Download(string url, string fileName)
        {
            try
            {
                HttpWebRequest HttpWReq = (HttpWebRequest)HttpWebRequest.Create(url);
                HttpWReq.Timeout = Timeout.Infinite;
                HttpWReq.KeepAlive = true;
                HttpWReq.Method = "GET";

                ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
                ServicePointManager.ServerCertificateValidationCallback = delegate (object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) { return true; };

                // Process the response from the server
                HttpWebResponse httpResponse = (HttpWebResponse)HttpWReq.GetResponse();


                if ((httpResponse.StatusCode == System.Net.HttpStatusCode.OK))
                {
                    // Process Zip Download
                    Stream responseStream = httpResponse.GetResponseStream();
                    // Process the response stream, streaming it directly to a file. (500K chunks at a time - You should be able to extract in bigger chunks!)
                    FileStream fs = new FileStream(fileName, FileMode.Create);
                    byte[] buffer = new byte[500000];
                    int read = -1;
                    while ((read = responseStream.Read(buffer, 0, buffer.Length)) > 0)
                    {
                        fs.Write(buffer, 0, read);
                    }
                    fs.Flush();
                    fs.Close();
                }
                httpResponse.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Unzipes the CSV files and adds them to a list
        /// </summary>
        private void UnzipFiles()
        {
            try
            {
                zipfiles
                    .AsParallel()
                    .All(sFile =>
                    {
                        WriteDebug("Unzipping file: " + sFile.Value);
                        UnZip(sFile.Value, tmpWorkingDir);
                        WriteDebug("Unzip complete!");
                        // Delete ZIP file.
                        File.Delete(sFile.Value);
                        return true;
                    });
            }
            catch (Exception ex)
            {
                throw new ApplicationException(string.Format("Error while unzipping the file"), ex);
            }
        }
        private DataTable CreateTableFromCSV(string csvFile)
        {
            try
            {
                CSVData importCSV = new CSVData();
                importCSV.SEPARATOR_CHAR = '|';
                return importCSV.Read(csvFile);
            }
            catch (Exception ex)
            {
                throw new ApplicationException(string.Format("Error while reading CSV file {0} to DataTable", csvFile), ex);
            }
        }

        /// <summary>
        /// Unzip specified ZIP file content to specified target directory.
        /// </summary>
        /// <param name="sFile">The path to the zipfile.</param>
        /// <param name="sTargetDirectory">The directory used to save the extracted zipfile.</param>
        /// <returns>The list of unziped files.</returns>
        private List<string> UnZip(string sFile, string sTargetDirectory)
        {
            List<string> listFiles = new List<string>();

            using (ZipInputStream s = new ZipInputStream(File.OpenRead(sFile)))
            {
                try
                {
                    ZipEntry theEntry;
                    while ((theEntry = s.GetNextEntry()) != null)
                    {
                        string directoryName = sTargetDirectory;
                        string fileName = Path.GetFileName(theEntry.Name);

                        if (fileName != String.Empty)
                        {
                            using (FileStream streamWriter = File.Create(directoryName + theEntry.Name))
                            {
                                int size = 2048;
                                byte[] data = new byte[2048];
                                while (true)
                                {
                                    size = s.Read(data, 0, data.Length);
                                    if (size > 0)
                                    {
                                        streamWriter.Write(data, 0, size);
                                    }
                                    else
                                    {
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new ApplicationException(string.Format("Unable to unzip file {0} to directory {1}", sFile, sTargetDirectory), ex);
                }
            }
            return listFiles;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="debugMessage"></param>
        void WriteDebug(string debugMessage)
        {
#if (DEBUG)
            Console.WriteLine("debug: " + debugMessage);
#endif
        }

    }

}
