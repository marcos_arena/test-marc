﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.IO;

namespace Empir.Data.CSV
{
    public class CSVData 
    {
        private char separator_char;
        private string tableName;
        private string quoteString;       

        public CSVData()
        {
            tableName = "CSVData";
            SEPARATOR_CHAR = ';';
            quoteString = null;
        }


        public char SEPARATOR_CHAR
        {
            get
            {
                return separator_char;
            }
            set
            {
                separator_char = value;
            }

        }

        public string TableName
        {
            get
            {
                return tableName;
            }
            set
            {
                tableName = value;
            }

        }

        public String QuoteString
        {
            get { return quoteString; }
            set { quoteString = value; }
        }
              

        public string StripQuotes(string sData)
        {
            if (quoteString != null)
            {
                if (sData.StartsWith(quoteString) && sData.EndsWith(quoteString))
                {
                    sData = sData.Substring(quoteString.Length, sData.Length - (2 * quoteString.Length));
                }
            }

            return sData;
        }

        private string AddQuotes(string sData)
        {
            if (quoteString != null)
            {
                sData = string.Format("{0}{1}{0}", quoteString, sData);
            }

            return sData;
        }

        /// <summary>
        /// Reads the file that is given as a parameter to a <c>DataTable</c>.
        /// The table name can be set as a property on this object. The separator char can also be set as a property on this object.
        /// 
        /// Encoding of the file that is read must be windows-1252.
        /// </summary>
        public DataTable Read(string fileName)
        {
            return Read(fileName, System.Text.Encoding.GetEncoding(1252));
        }

        /// <summary>
        /// Reads the file that is given as a parameter to a <c>DataTable</c>.
        /// The table name can be set as a property on this object. The separator char can also be set as a property on this object.
        /// </summary>
        public DataTable Read(string fileName, Encoding encoding)
        {
            DataTable dataTable = new DataTable();
            try
            {
                FileInfo fi = new FileInfo(fileName);
                tableName = Path.GetFileNameWithoutExtension(fi.Name);
                dataTable.TableName = tableName;
                using (System.IO.StreamReader streamIn = new System.IO.StreamReader(fileName, encoding))
                {
                    var line = streamIn.ReadLine();
                    string[] headers = line.Split(separator_char);
                    foreach (string header in headers)
                    {
                        var headerText = header.Trim();
                        try
                        {
                            dataTable.Columns.Add(headerText, System.Type.GetType("System.String"));
                        }
                        catch
                        {
                            throw new Exception(String.Format("A column with the name \"{0}\" was found multiple times in the file {1} .", header, tableName));
                        }
                    }
                    //Read the rest of the rows.
                    while (!streamIn.EndOfStream)
                    {
                        string nline = streamIn.ReadLine();
                        if (nline.Replace(separator_char.ToString(), "").Trim().Length != 0)
                        {

                            string[] values = nline.Split(separator_char);
                            DataRow row = dataTable.NewRow();

                            for (int i = 0; i < values.Length; i++)
                            {
                                // Remove values in a position higher then the headers. Should this throw a exception? A setting for this maybe?
                                if (i < dataTable.Columns.Count)
                                {
                                    var value = values[i].Trim();
                                    // Skip empty lines.
                                    if (value.Length > 0)
                                    {
                                        //value = StripQuotes(value);
                                        row[i] = value;
                                    }
                                }
                            }
                            dataTable.Rows.Add(row);
                        }
                    }
                    streamIn.Close();
                    streamIn.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dataTable;
        }

        public IEnumerable<string> SplitLine(String line)
        {
            int pos = 0, lastPos = 0;
            while (pos != -1)
            {
                pos = line.IndexOf(separator_char, pos);
                if (pos == -1)
                {
                    yield return line.Substring(lastPos);
                    break;
                }
                yield return line.Substring(lastPos, pos - lastPos);
                lastPos = ++pos;
            }
        }      
    }
}


