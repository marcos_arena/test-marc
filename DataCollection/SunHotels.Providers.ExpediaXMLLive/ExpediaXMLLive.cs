using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using System.Linq;
using SunHotels.XML;
using SunHotels.XML.Data;
using SunHotels.Providers.ProviderCommon.DataLoader;
using Sunhotels.Export;
using Empir.Data.CSV;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Collections;
using System.Text.RegularExpressions;

namespace SunHotels.Providers
{



    /// <summary>
    /// Fetch and parse information from Expedia Http. Add this information to Sunhotels XML structure.
    /// </summary>
    public class ExpediaXMLLive : XMLProvider, SunHotels.Providers.ProviderCommon.DataLoader.IStaticDataLogger
    {
        XmlDocument xmlExportConfig;
        // Create dictionaries for fast search
        Dictionary<string, Place> allPlaces = new Dictionary<string, Place>();
        Dictionary<string, Hotel> allHotels = new Dictionary<string, Hotel>();
        XmlDocument docExpediaXMLLive = new XmlDocument();
        ExpediaXMLLiveSettings expediaXMLLiveSettings = new ExpediaXMLLiveSettings();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="root"></param>
        protected override void getProviderDefinition(Root root)
        {
            root.ProviderDefinition.Name = configuration.ProviderName;
            root.ProviderDefinition.Currency = "EUR";
        }

        protected override void UpdateConfiguration(Configuration configuration)
        {
            configuration.WriteHotelsWithoutRooms = true;
            configuration.WriteRoomsWithoutAvailability = true;
        }

        /// <summary>
        /// Entry point for class.
        /// </summary>
        /// <param name="root">Root for Sunhotels XML structure.</param>
        protected override void getData(Root root)
        {
            try
            {
                this.root = root;

                // ------------------------------------------------------------------------------------------------
                // Create log stream
                // ------------------------------------------------------------------------------------------------
                try
                {
                    Directory.CreateDirectory(configuration.LogOutput);
                }
                catch (Exception ex)
                {
                    throw new ApplicationException("Unable to create ExpediaXMLLive log file", ex);
                }
                Log(ILoggerStaticDataSeverity.Info, "Stating process");
                // Get all static data 
                GetStaticData();

                Log(ILoggerStaticDataSeverity.Info, "Processing all hotels - " + allHotels.Count + " elements in the hotel list");
                foreach (var item in allHotels)
                {
                    allPlaces[item.Value.PlaceId].Hotels.Add(item.Value.Id, item.Value);
                }
                Log(ILoggerStaticDataSeverity.Info, allPlaces.Count + " elements in the places list");
                Log(ILoggerStaticDataSeverity.Info, "Process finished");
            }
            catch (Exception ex)
            {
                Log(ILoggerStaticDataSeverity.Info, "Unhandled exception [{0}] ", ex.ToString());
                throw;
            }
            finally
            {
            }
        }
        /// <summary>
        /// Get Global Feature mapping for the provider
        /// </summary>
        /// <param name="root"></param>
        protected override void getFeatureTypes(Root root)
        {
            // Get SH features mapped with the provider
            DataMapper.SunHotelsFeatures.Join(DataMapper["Feature"], o => o.Key, i => i.Key, (o, i) => o)
                .All(f =>
                {
                    root.Features.Add(new Feature()
                    {
                        Id = f.Key,
                        Value = f.Value,
                        Name = f.Value
                    });
                    return true;
                });
        }

        /// <summary>
        /// Get all static data from text files and bind with Dictionary objects.
        /// </summary>              
        private void GetStaticData()
        {
            try
            {

                XDocument xdoc = XDocument.Load(configuration.ProviderConfigRoot + "\\ExpediaXMLLiveSettings.xml");
                expediaXMLLiveSettings = (from config in xdoc.Descendants("ExpediaXMLLiveSettings")
                                          select new ExpediaXMLLiveSettings
                                          {
                                              RequestforDownload = config.Element("RequestforDownload").Value,
                                          }).First();
                xmlExportConfig = new XmlDocument();
                xmlExportConfig.Load(configuration.ProviderConfigRoot + "\\ExpediaXMLLive_ExportConfig.xml");
                // Read XML document for hotel feature mapping.
                var features = DataMapper.SunHotelsFeatures.Join(DataMapper["Feature"], o => o.Key, i => i.Key, (o, i) => new { SHFeature = o, ProviderFeature = i })
                                .SelectMany(o => o.ProviderFeature.Value.Select(
                                    of => new
                                    {
                                        ProviderId = of,
                                        SHFeature = o.SHFeature
                                    }
                                    ))
                                .ToDictionary(o => o.ProviderId, o => o.SHFeature);
                // Get all static data 
                StaticData csvData = new StaticData(configuration.LocalTemp, configuration.ProviderConfigRoot);
                //Get all list of file which may processed or downloaded.
                XmlNodeList fileNodes = xmlExportConfig.SelectNodes("//Files/File");
                //Setting "Yes" to RequestforDownload property will allow to download the file in local.
                if (expediaXMLLiveSettings.RequestforDownload == "Yes")
                {
                    Log(ILoggerStaticDataSeverity.Info, "Downloading file into local started");
                    //To download the file into local
                    csvData.GetStaticData();
                }
                //Checking all the active files are available in local before start processing.
                StringBuilder missingFiles = new StringBuilder("");
                foreach (XmlNode file in fileNodes)
                {
                    if (file.Attributes["IsActive"].Value == "1")
                    {
                        if (!IsFileAvailable(configuration.LocalTemp + file.Attributes["FileName"].Value + ".txt"))
                        {
                            if (missingFiles.ToString() == "")
                            {
                                missingFiles.Append(configuration.LocalTemp + file.Attributes["FileName"].Value + ".txt");
                            }
                            else
                            {
                                missingFiles.Append("," + configuration.LocalTemp + file.Attributes["FileName"].Value + ".txt");
                            }
                        }
                    }
                }
                if (missingFiles.ToString().Trim() != "")
                {
                    throw new Exception("file missing, file/files does not exist:" + missingFiles.ToString().Trim());
                }
                #region Process Data
                //Get the list of file orders which need to process in order. 
                IEnumerable<int> listorder = fileNodes.AsEnumerable().Where(a => a.Attributes["IsActive"].Value.ToString().Trim() == "1").Select(c => Convert.ToInt32(c.Attributes["order"].Value.ToString())).ToList().OrderBy(c => c).Distinct();
                Stopwatch s1 = null;
                CSVData cSVData = new CSVData();
                cSVData.SEPARATOR_CHAR = '|';
                Log(ILoggerStaticDataSeverity.Info, "Processs file in order start");
                DataTable parentRegions = new DataTable();
                DataTable mappingRegionsHotels = new DataTable();
                Dictionary<string, List<string>> all = new Dictionary<string, List<string>>();
                Dictionary<string, int> valid = new Dictionary<string, int>();
                foreach (int order in listorder)
                {
                    s1 = Stopwatch.StartNew();
                    string languageCode = string.Empty;
                    var fileNames = new List<string>();
                    //Select the file based on the order and Isactive check.
                    IEnumerable<XmlNode> selected = fileNodes.AsEnumerable().Where(c => c.Attributes["order"].Value == order.ToString() && c.Attributes["IsActive"].Value.ToString().Trim() == "1").Select(c => c).ToList();
                    DataTable dt = new DataTable();
                    foreach (XmlNode select in selected)
                    {
                        //Read the file into DataTable
                        fileNames.Add(select.Attributes["FileName"].Value);
                        var fileName = configuration.LocalTemp + select.Attributes["FileName"].Value + ".txt";
                        Log(ILoggerStaticDataSeverity.Info, "Reading file - " + fileName);
                        dt = cSVData.Read(fileName);
                        languageCode = select.Attributes["language"].Value;

                    }

                    switch (order)
                    {
                        //build destination tree
                        case 0:
                        case 1:
                        case 2:
                            try
                            {
                                int region = 0;
                                if (order == 0)
                                {
                                    //parent regions
                                    parentRegions = dt;
                                }
                                else if (order == 1)
                                {
                                    mappingRegionsHotels = dt;

                                    var includedRegions = new OrderedSet<int>();

                                    foreach (var x in parentRegions
                                                .AsEnumerable()
                                                .Where(r => int.TryParse(r.Field<string>("RegionID"), out region) == true) //it means that we don't want to save Point of interest
                                                .Select(s => new { Type = s.Field<string>("RegionType"), RegionId = int.Parse(s.Field<string>("RegionID")) })
                                                .Where(r => r.Type == "City")
                                                .OrderBy(r => r.Type.Length)
                                                .Select(s => s.RegionId))
                                        includedRegions.Add(x);

                                    valid = mappingRegionsHotels
                                        .AsEnumerable()
                                        .Where(r => int.TryParse(r.Field<string>("RegionID"), out region) == true)
                                        .Select(f => new { hotelId = f.Field<string>("EANHotelID"), region = int.Parse(f.Field<string>("RegionID")) })
                                        .Where(x => includedRegions.Contains(x.region))
                                        .GroupBy(x => x.hotelId)
                                        .ToDictionary(
                                            d => d.Key,
                                            d => d.Select(s => s.region).FirstOrDefault()); // multi-city > City..  

                                }
                                else
                                {
                                    int eanDestination;
                                    var htRegions = new Dictionary<int, DataRow>();

                                    foreach (DataRow regionRow in parentRegions.AsEnumerable())
                                    {
                                        if (int.TryParse(regionRow.Field<string>("RegionID"), out region))
                                            htRegions[region] = regionRow;
                                    }

                                    foreach (DataRow dr in dt.Rows)
                                    {
                                        try
                                        {

                                            string sCountryCode = dr["Country"].ToString();
                                            //Creating the place based on the city and country Data.
                                            if (!valid.TryGetValue(dr["EANHotelID"].ToString(), out eanDestination))
                                                continue; // we have been trolled - the mapping is missing.

                                            if (!htRegions.ContainsKey(eanDestination))
                                                continue;

                                            var regionRow = htRegions[eanDestination];

                                            string cityName = regionRow["RegionName"].ToString();
                                            string longCityName = regionRow["RegionNameLong"].ToString();
                                            Place hotelPlace = GetPlace(sCountryCode.Trim(), sCountryCode.Trim(), cityName, eanDestination, longCityName);

                                            Hotel newHotel = CreateHotel(dr, hotelPlace);
                                            if (!allHotels.ContainsKey(newHotel.Id))
                                            {
                                                allHotels.Add(newHotel.Id, newHotel);
                                            }
                                            else
                                            {
                                                Log(ILoggerStaticDataSeverity.Info, "Hotel Id: {0} already exist in the hotel collection", newHotel.Id);
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            Log(ILoggerStaticDataSeverity.Error, string.Format("Invalid data detected in {0}: {1}", "ActivePropertyList", ex.Message));
                                        }
                                    }
                                }

                            }
                            catch (Exception ex)
                            {
                                Log(ILoggerStaticDataSeverity.Error, string.Format("Invalid data detected in {0}: {1}", "ParentRegionList or ActivePropertyList or PropertyCategory", ex.Message));
                            }
                            finally
                            {
                                dt.Dispose();
                                Log(ILoggerStaticDataSeverity.Info, string.Format("Hotels Added:{0}.Places Added:{1}.Currently:{2}", allHotels.Count(), allPlaces.Count(), "ActivePropertyList"));
                            }
                            Log(ILoggerStaticDataSeverity.Info, string.Format("ActivePropertyList End:{0}", s1.ElapsedMilliseconds));
                            break;
                        case 3:

                            try
                            {
                                //Loading Hotel images
                                LoadImages(dt);
                            }
                            catch (Exception ex)
                            {
                                Log(ILoggerStaticDataSeverity.Error, string.Format("Invalid data detected in {0}: {1}", "HotelImageList", ex.Message));
                            }
                            finally
                            {
                                dt.Dispose();
                                Log(ILoggerStaticDataSeverity.Info, string.Format("Hotels Added:{0}.Places Added:{1}.Currently:{2}", allHotels.Count(), allPlaces.Count(), "HotelImageList"));
                            }
                            Log(ILoggerStaticDataSeverity.Info, string.Format("HotelImageList End:{0}", s1.ElapsedMilliseconds));
                            break;
                        case 4:
                            try
                            {
                                //Loading Hotel Property Description
                                LoadDescription(dt, "PropertyDescription");
                                //Loading English transulation
                                LoadTranslation("en", "US", dt);
                            }
                            catch (Exception ex)
                            {
                                Log(ILoggerStaticDataSeverity.Error, string.Format("Invalid data detected in {0}: {1}", "PropertyDescriptionList", ex.Message));
                            }
                            finally
                            {
                                dt.Dispose();
                                Log(ILoggerStaticDataSeverity.Info, string.Format("Hotels Added:{0}.Places Added:{1}.Currently:{2}", allHotels.Count(), allPlaces.Count(), "PropertyDescriptionList"));
                            }
                            Log(ILoggerStaticDataSeverity.Info, string.Format("PropertyDescriptionList End:{0}", s1.ElapsedMilliseconds));
                            break;
                        case 5:
                            try
                            {
                                //Loading Hotel Feature
                                FeatureMapping(features, dt);
                            }
                            catch (Exception ex)
                            {
                                Log(ILoggerStaticDataSeverity.Error, string.Format("Invalid data detected in {0}: {1}", "PropertyAttributeLink", ex.Message));
                            }
                            finally
                            {
                                dt.Dispose();

                                Log(ILoggerStaticDataSeverity.Info, string.Format("Hotels Added:{0}.Places Added:{1}.Currently:{2}", allHotels.Count(), allPlaces.Count(), "PropertyAttributeLink"));
                            }
                            Log(ILoggerStaticDataSeverity.Info, string.Format("PropertyAttributeLink End:{0}", s1.ElapsedMilliseconds));
                            break;

                        case 6:
                            try
                            {
                                //Loading Hotel Area Attractions Description
                                LoadDescription(dt, "AreaAttractions");

                            }
                            catch (Exception ex)
                            {
                                Log(ILoggerStaticDataSeverity.Error, string.Format("Invalid data detected in {0}: {1}", "AreaAttractionsList", ex.Message));
                            }
                            finally
                            {
                                dt.Dispose();
                                Log(ILoggerStaticDataSeverity.Info, string.Format("Hotels Added:{0}.Places Added:{1}.Currently:{2}", allHotels.Count(), allPlaces.Count(), "AreaAttractionsList"));
                            }
                            Log(ILoggerStaticDataSeverity.Info, string.Format("AreaAttractionsList End:{0}", s1.ElapsedMilliseconds));
                            break;
                        case 7:
                            try
                            {   //Loading Hotel Area spa Description
                                LoadDescription(dt, "SpaDescription");

                            }
                            catch (Exception ex)
                            {
                                Log(ILoggerStaticDataSeverity.Error, string.Format("Invalid data detected in {0}: {1}", "SpaDescriptionList", ex.Message));
                            }
                            finally
                            {
                                dt.Dispose();
                                Log(ILoggerStaticDataSeverity.Info, string.Format("Hotels Added:{0}.Places Added:{1}.Currently:{2}", allHotels.Count(), allPlaces.Count(), "SpaDescriptionList"));
                            }
                            Log(ILoggerStaticDataSeverity.Info, string.Format("SpaDescriptionList End:{0}", s1.ElapsedMilliseconds));
                            break;
                        case 8:
                            try
                            { //Loading Hotel WhatToExpect Description
                                LoadDescription(dt, "WhatToExpect");

                            }
                            catch (Exception ex)
                            {
                                Log(ILoggerStaticDataSeverity.Error, string.Format("Invalid data detected in {0}: {1}", "WhatToExpect", ex.Message));
                            }
                            finally
                            {
                                dt.Dispose();
                                Log(ILoggerStaticDataSeverity.Info, string.Format("Hotels Added:{0}.Places Added:{1}.Currently:{2}", allHotels.Count(), allPlaces.Count(), "WhatToExpectList"));
                            }
                            Log(ILoggerStaticDataSeverity.Info, string.Format("WhatToExpectList End:{0}", s1.ElapsedMilliseconds));
                            break;
                        case 9:
                            try
                            {//Loading Hotel PropertyDining Description
                                LoadDescription(dt, "PropertyDiningDescription");
                            }
                            catch (Exception ex)
                            {
                                Log(ILoggerStaticDataSeverity.Error, string.Format("Invalid data detected in {0}: {1}", "DiningDescriptionLIst", ex.Message));
                            }
                            finally
                            {
                                dt.Dispose();
                                Log(ILoggerStaticDataSeverity.Info, string.Format("Hotels Added:{0}.Places Added:{1}.Currently:{2}", allHotels.Count(), allPlaces.Count(), "DiningDescriptionLIst"));
                            }
                            Log(ILoggerStaticDataSeverity.Info, string.Format("DiningDescriptionLIst End:{0}", s1.ElapsedMilliseconds));
                            break;
                        case 10:
                            try
                            {//Loading Hotel Policy Description
                                LoadDescription(dt, "PolicyDescription");

                            }
                            catch (Exception ex)
                            {
                                Log(ILoggerStaticDataSeverity.Error, string.Format("Invalid data detected in {0}: {1}", "PolicyDescriptionList", ex.Message));
                            }
                            finally
                            {
                                dt.Dispose();
                                Log(ILoggerStaticDataSeverity.Info, string.Format("Hotels Added:{0}.Places Added:{1}.Currently:{2}", allHotels.Count(), allPlaces.Count(), "PolicyDescriptionList"));
                            }
                            Log(ILoggerStaticDataSeverity.Info, string.Format("PolicyDescriptionList End:{0}", s1.ElapsedMilliseconds));
                            break;
                        case 11:
                            try
                            {//Loading Hotel PropertyFees Description
                                LoadDescription(dt, "PropertyFeesDescription");
                            }
                            catch (Exception ex)
                            {
                                Log(ILoggerStaticDataSeverity.Error, string.Format("Invalid data detected in {0}: {1}", "PropertyFeesList", ex.Message));
                            }
                            finally
                            {
                                dt.Dispose();
                                Log(ILoggerStaticDataSeverity.Info, string.Format("Hotels Added:{0}.Places Added:{1}.Currently:{2}", allHotels.Count(), allPlaces.Count(), "PropertyFeesList"));
                            }
                            Log(ILoggerStaticDataSeverity.Info, string.Format("PropertyFeesList End:{0}", s1.ElapsedMilliseconds));
                            break;
                        case 12:
                            try
                            {//Loading Hotel Property Renovations Description
                                LoadDescription(dt, "PropertyRenovationsDescription");

                            }
                            catch (Exception ex)
                            {
                                Log(ILoggerStaticDataSeverity.Error, string.Format("Invalid data detected in {0}: {1}", "PropertyRenovationsList", ex.Message));
                            }
                            finally
                            {
                                dt.Dispose();
                                Log(ILoggerStaticDataSeverity.Info, string.Format("Hotels Added:{0}.Places Added:{1}.Currently:{2}", allHotels.Count(), allPlaces.Count(), "PropertyRenovationsList"));
                            }
                            Log(ILoggerStaticDataSeverity.Info, string.Format("PropertyFeesList End:{0}", s1.ElapsedMilliseconds));
                            break;
                        case 13:
                            try
                            {   //Loading Hotel Property Business Amenities Description
                                LoadDescription(dt, "PropertyBusinessAmenitiesDescription");

                            }
                            catch (Exception ex)
                            {
                                Log(ILoggerStaticDataSeverity.Error, string.Format("Invalid data detected in {0}: {1}", "PropertyFeesList", ex.Message));
                            }
                            finally
                            {
                                dt.Dispose();
                                Log(ILoggerStaticDataSeverity.Info, string.Format("Hotels Added:{0}.Places Added:{1}.Currently:{2}", allHotels.Count(), allPlaces.Count(), "PropertyBusinessAmenitiesList"));
                            }
                            Log(ILoggerStaticDataSeverity.Info, string.Format("PropertyFeesList End:{0}", s1.ElapsedMilliseconds));
                            break;

                        default:

                            if (fileNames.Any())
                            {
                                if (fileNames.First().ToLower().StartsWith("propertydescriptionlist_"))
                                { // Process description list by language
                                    try
                                    {
                                        //Split the languagecode code to get language(2 digit)code and country code.
                                        string[] Array = languageCode.Split('_');
                                        //Loading hotel other language Description into Translations element
                                        LoadTranslation(Array[0], Array[1], dt);
                                    }
                                    catch (Exception ex)
                                    {
                                        Log(ILoggerStaticDataSeverity.Error, string.Format("Invalid data detected in {0}: {1}", "PropertyDescription", ex.Message));
                                    }
                                    finally
                                    {
                                        dt.Dispose();
                                        Log(ILoggerStaticDataSeverity.Info, string.Format("Hotels Added:{0}.Places Added:{1}.Currently:{2}", allHotels.Count(), allPlaces.Count(), "PropertyDescriptionList_" + languageCode));
                                    }
                                    Log(ILoggerStaticDataSeverity.Info, string.Format("PropertyDescriptionList_" + languageCode + " End:{0}", s1.ElapsedMilliseconds));

                                }
                            }
                            break;

                    }
                }
                #endregion Process Data



            }
            catch (Exception ex)
            {
                Log(ILoggerStaticDataSeverity.Error, string.Format("Error while processing get data function in:{1}", "", ex.Message));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dt"></param>
        private void LoadImages(DataTable dt)
        {
            //Process all hotel images parallely.
            Parallel.ForEach(dt.AsEnumerable(), dr =>
            {
                var hotelId = dr["EANHotelID"].ToString();
                if (allHotels.ContainsKey(hotelId))
                {
                    var hotel = allHotels[hotelId];
                    lock (hotel)
                    {
                        //we must get the large images, arround 2000x2000. To get it we just should change the latest character after the last "_" from "b"
                        // for example to "z" (1000x1000)
                        var urlLargeImages = dr["URL"].ToString();
                        //i'm removing the character, and replacing for Z to get the large image.
                        urlLargeImages = urlLargeImages.Substring(0, urlLargeImages.Length - 5) + "w.jpg";
                        if (!hotel.Images.Any(i => i.ImageVariants.Any(iv => iv.URL == urlLargeImages)))

                            hotel.Images.Add(new Hotel.Image()
                            {
                                ImageVariants = new List<Hotel.ImageVariant>()
                                        {
                                            new Hotel.ImageVariant()
                                            {
                                                URL = urlLargeImages,
                                                Height = 1000,
                                                Width = 1000
                                            }
                                        }
                            });
                    }
                }
            });
        }
        /// <summary>
        /// Load the hotel features
        /// </summary>
        /// <param name="features">KeyValuePair of available hotel features list</param>
        /// <param name="dt">Property features in datatable</param>
        private void FeatureMapping(Dictionary<string, KeyValuePair<string, string>> features, DataTable dt)
        {
            //Processing all hotel features parallely.
            Parallel.ForEach(dt.AsEnumerable(), dr =>
            {
                var hotelId = dr["EANHotelID"].ToString();
                if (allHotels.ContainsKey(hotelId))
                {
                    var hotel = allHotels[hotelId];
                    lock (hotel)
                    {
                        var attribute = dr["AttributeID"].ToString();
                        if (features.ContainsKey(attribute))
                        {
                            var shfeature = features[attribute];
                            if (!hotel.Features.Any(f => f.Id == shfeature.Key))
                            {
                                hotel.Features.Add(new Feature()
                                {
                                    Id = shfeature.Key,
                                    Name = shfeature.Value,
                                    Value = shfeature.Value
                                });
                            }
                        }
                    }
                }
            });

        }
        /// <summary>
        /// Load the Hotel description 
        /// </summary>
        /// <param name="dt">Datatable of property description</param>
        /// <param name="headingname">Property name</param>
        /// <returns>return true if operation is success</returns>
        private bool LoadDescription(DataTable dt, string headingname)
        {
            if (headingname == "PropertyDescription")
            {
                dt.AsEnumerable().AsParallel().ForAll(c => CreateDescription(c.Field<string>("EANHotelID"), c.Field<string>(headingname)));
            }
            else
            {
                dt.AsEnumerable().AsParallel().ForAll(c => CreateExtraDescription(c.Field<string>("EANHotelID"), c.Field<string>(headingname), headingname));
            }
            return true;
        }
        /// <summary>
        /// Load Hotel extra description like spa,Dinning and etc.
        /// </summary>
        /// <param name="eANHotelID">EAN Hotelid</param>
        /// <param name="description">hotel Description</param>
        /// <param name="headingname">name of the description</param>
        private void CreateExtraDescription(string eANHotelID, string description, string headingname)
        {
            description = handleHTML(description);

            // Loading extra description of the hotels.
            if (allHotels.ContainsKey(eANHotelID))
            {
                var hotel = allHotels[eANHotelID];
                hotel.Description = hotel.Description + ":" + headingname + ":" + description;
            }
        }
        /// <summary>
        /// Load Hotel description
        /// </summary>
        /// <param name="eANHotelID">EAN Hotelid</param>
        /// <param name="description">hotel Description</param>
        /// <param name="headingname">name of the description</param>
        private void CreateDescription(string eANHotelID, string description)
        {
            description = handleHTML(description);

            // Loading property description of the hotels.
            if (allHotels.ContainsKey(eANHotelID))
            {
                var hotel = allHotels[eANHotelID];

                hotel.Description = description;
            }
        }

        private string handleHTML(string str)
        {
            if (string.IsNullOrEmpty(str)) return string.Empty;
            // Remove HTML tags, and the headers (in <b>) remove them and put instead two line breaks
            //str = str.Replace("<b>", "<p><b>"); 
            str = str.Replace("<p>", Environment.NewLine + Environment.NewLine);
            str = Regex.Replace(str, @"(<b(.*?)>(.*?)</b>)|<[^>]*>", String.Empty).Trim();
            return str;
        }

        /// <summary>
        /// Get previously created place or create new.
        /// </summary>
        /// <param name="hotelRow">The data row containing hotel information.</param>
        /// <param name="hotelPlace">The place under witch the hotel will be created.</param>
        /// <returns>The requested hotel.</returns>
        private Hotel CreateHotel(DataRow hotelRow, Place hotelPlace)
        {
            try
            {
                Hotel hNew = new Hotel();
                hNew.PlaceId = hotelPlace.Id;
                hNew.Id = hotelRow["EANHotelID"].ToString();
                hNew.Name = hotelRow["Name"].ToString();
                hNew.Adress_Street1 = hotelRow["Address1"].ToString();
                hNew.Adress_Street2 = hotelRow["Address2"].ToString();
                hNew.Adress_City = hotelRow["City"].ToString();
                hNew.Adress_State = hotelRow["StateProvince"].ToString();
                hNew.Adress_Zipcode = hotelRow["PostalCode"].ToString();
                hNew.Adress_Country = hotelRow["Country"].ToString();
                hNew.Description = "";
                hNew.AccomodationType = GetAccomodationType(hotelRow["PropertyCategory"].ToString());
                hNew.Position_Latitude = hotelRow["Latitude"].ToString();
                hNew.Position_Longitude = hotelRow["Longitude"].ToString();
                hNew.Classification = hotelRow["StarRating"].ToString();
                Identifier identifier = new Identifier();
                identifier.Type = "provider";
                identifier.Value = "ExpediaXMLLive";
                hNew.Identifiers.Add(identifier);
                return hNew;
            }
            catch (Exception ex)
            {
                Log(ILoggerStaticDataSeverity.Error, string.Format("Error while processing CreateHotel function in:{1}", "", ex.Message));
                return null;
            }
        }
        /// <summary>
        /// Get accomodation type based on propertycategory
        /// </summary>
        /// <param name="propertyCategory">Propertycategory</param>
        /// <returns>AccomodationType</returns>
        private AccomodationType GetAccomodationType(string propertyCategory)
        {
            XmlDocument xmlMappings = new XmlDocument();
            xmlMappings.Load(configuration.ProviderConfigRoot + "\\ExpediaXMLLive_AccomodationType.xml");
            var hotelcategory = xmlMappings.SelectNodes("//PropertyCategories/PropertyCategory[PropertyCategoryID=" + propertyCategory + "]").AsEnumerable().AsParallel().Select(c => c.ChildNodes.AsEnumerable().Where(d => d.Name == "SHAccomodationType").Select(d => d)).FirstOrDefault();
            if (hotelcategory != null)
            {
                return GetAccomodation(hotelcategory.First().InnerText.ToString());
            }

            return AccomodationType.Hotel;

        }
        /// <summary>
        /// Get Accomodation type 
        /// </summary>
        /// <param name="accomodationType">accomodationType in string format</param>
        /// <returns>AccomodationType</returns>
        private AccomodationType GetAccomodation(string accomodationType)
        {
            try
            {
                switch (accomodationType)
                {
                    case "Hotel":
                        return AccomodationType.Hotel;
                    case "Apartment":
                        return AccomodationType.Apartment;
                    case "Villa":
                        return AccomodationType.Villa;
                    default:
                        return AccomodationType.Hotel;
                }
            }
            catch (Exception ex)
            {
                Log(ILoggerStaticDataSeverity.Error, string.Format("Error while processing GetAccomodationType function in:{1}", "", ex.Message));
                return AccomodationType.Hotel;
            }
        }

        /// <summary>
        /// Get previously created place or create new.
        /// </summary>
        /// <param name="sCountryName">The country code.</param>
        /// <param name="sCountryCode">The country code.</param>
        /// <param name="sCityName">The city name.</param>
        /// <param name="sCityCode">The city code.</param>
        /// <returns>The requested palce.</returns>
        private Place GetPlace(string sCountryName, string sCountryCode, string sCityName, int iCityCode, string longCityName)
        {

            string sCityCode = iCityCode.ToString();
            // Check or create country level place        

            if (allPlaces.ContainsKey(sCountryCode.ToUpper()) != true)
            {
                Place newPlace = new Place();
                newPlace.Id = sCountryCode;
                newPlace.Description = sCountryName;
                allPlaces.Add(sCountryCode, newPlace);
                root.Places.Add(newPlace);
            }
            // Check or create state level place if state is specified            
            if (string.IsNullOrEmpty(sCityCode) != true)
            {
                if (allPlaces.ContainsKey(sCityCode.Trim()) != true)
                {
                    Place newPlace = new Place();
                    newPlace.Id = sCityCode;
                    newPlace.Description = sCityName;
                    allPlaces.Add(sCityCode, newPlace);
                    if (allPlaces[sCountryCode].Places.Any(a => a.Description == newPlace.Description))
                    {
                        string state = GetStateFromCityName(longCityName);
                        if (!string.IsNullOrEmpty(state))
                            newPlace.Description = string.Format("{0}({1})", newPlace.Description, state);
                    }
                    allPlaces[sCountryCode].Places.Add(newPlace);
                }
            }
            return allPlaces[sCityCode];

        }

        private string GetStateFromCityName(string longCityName)
        {
            int numValues = longCityName.Split(',').Count();
            if (numValues > 1)
            {
                switch (numValues)
                {
                    case 2:
                        return longCityName.Split(',')[0];
                    case 3:
                        return longCityName.Split(',')[1];
                    default:
                        return longCityName.Split(',')[1];
                }
            }
            else
                return string.Empty;

        }

        /// <summary>
        /// load (other) language description into Translations
        /// </summary>
        /// <param name="langCode">LanaguageCode(2 digit)</param>
        /// <param name="countryCode">Country Code</param>
        /// <param name="dt">Hotel description datatable(language based on langCode) </param>
        private void LoadTranslation(String langCode, String countryCode, DataTable dt)
        {
            try
            {
                Parallel.ForEach(dt.AsEnumerable(), r =>
                {
                    var id = r["EANHotelID"].ToString();
                    if (allHotels.ContainsKey(id))
                    {
                        var hotel = allHotels[id];
                        //var desc = GetTrimmedDescription(r["PropertyDescription"].ToString(), langCode);
                        var desc = handleHTML(r["PropertyDescription"].ToString()).Trim();
                        if (!String.IsNullOrEmpty(desc) && !hotel.Translations.description.Any(d => d.lang == langCode && d.country == countryCode))
                            hotel.Translations.description.Add(new TranslationsDescription()
                            {
                                lang = langCode,
                                country = countryCode,
                                Value = desc
                            });
                    }
                });
            }
            catch (Exception ex)
            {
                Log(ILoggerStaticDataSeverity.Info, string.Format("Error while processing LoadTranslation function in:{1}", "", ex.Message));
            }
        }

        //private string GetTrimmedDescription(string description, string langCode)
        //{
        //    if (string.IsNullOrEmpty(description)) return string.Empty;
        //    //I'm taking number 3 as index because always is the same.
        //    Regex r = new Regex("<[^>]*>");
        //    var replace = description.Substring(3, r.Matches(description)[3].Index + 3);
        //    return description.Replace(replace, "");
        //}


        /// <summary>
        /// To check the file is available in the specified path
        /// </summary>
        /// <param name="fileName">Filepath</param>
        /// <returns>return true if file is available</returns>
        private bool IsFileAvailable(string fileName)
        {
            if (File.Exists(fileName))
                return true;
            else
                return false;
        }
        public void Log(ILoggerStaticDataSeverity severity, string message, params object[] args)
        {
            XML.HelpClasses.Log.MessageType s = XML.HelpClasses.Log.MessageType.Informational;
            if (severity == ILoggerStaticDataSeverity.Error)
                s = XML.HelpClasses.Log.MessageType.Error;
            if (severity == ILoggerStaticDataSeverity.Warning)
                s = XML.HelpClasses.Log.MessageType.Warning;
            base.LogEntry(s, message, args);
        }

    }
}
