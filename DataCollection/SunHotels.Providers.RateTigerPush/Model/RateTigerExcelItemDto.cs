﻿using SunHotels.Providers.Parser;
using System;

namespace SunHotels.Providers.Model
{
    public class RateTigerHotelExcelRowDto : IEquatable<RateTigerHotelExcelRowDto>
    {
        public string HotelCode { get; set; }
        public string HotelName { get; set; }
        public string BrandName { get; set; }
        public string HotelAddress { get; set; }
        public string HotelEmail { get; set; }
        public string HotelClassification { get; set; }
        public string Currency { get; set; }
        public string RateCode { get; set; }
        public string RateName { get; set; }
        public string RoomType { get; set; }
        public string RoomTypeName { get; set; }
        public int MaxOccupancy { get; set; }
        public int MaxAdultsOccupancy { get; set; }
        public int MaxChildsOccupancy { get; set; }
        public string CancelPolicy { get; set; }
        public string TaxeCode { get; set; }
        public string TaxeInformation { get; set; }
        public string TaxeName { get; set; }
        public string TaxeAmount { get; set; }
        public TaxeUnit TaxeUnit { get; set; }
        public bool TaxeIncluded { get; set; }
        public TaxeCalculationRule TaxeCalculationRule { get; set; }
        public bool TaxePrePayable { get; set; }
        public bool TaxeVat { get; set; }
        public bool RoomBookable { get; set; }        
        public RateTigerHotelDto HotelInfo { get; set; }

        public bool Equals(RateTigerHotelExcelRowDto other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return string.Equals(HotelCode, other.HotelCode)
            && string.Equals(HotelName, other.HotelName)
            && string.Equals(BrandName, other.BrandName)
            && string.Equals(HotelAddress, other.HotelAddress)
            && string.Equals(HotelEmail, other.HotelEmail)
            && string.Equals(HotelClassification, other.HotelClassification)
            && string.Equals(Currency, other.Currency)
            && string.Equals(RateCode, other.RateCode)
            && string.Equals(RateName, other.RateName)
            && string.Equals(RoomType, other.RoomType)
            && string.Equals(RoomTypeName, other.RoomTypeName)
            && Equals(MaxOccupancy, other.MaxOccupancy)
            && Equals(MaxAdultsOccupancy, other.MaxAdultsOccupancy)
            && Equals(MaxChildsOccupancy, other.MaxChildsOccupancy)
            && string.Equals(CancelPolicy, other.CancelPolicy)
            && string.Equals(TaxeCode, other.TaxeCode)
            && string.Equals(TaxeInformation, other.TaxeInformation)
            && string.Equals(TaxeName, other.TaxeName)
            && Equals(TaxeAmount, other.TaxeAmount)
            && Equals(TaxeUnit, other.TaxeUnit)
            && Equals(TaxeIncluded, other.TaxeIncluded)
            && Equals(TaxeCalculationRule, other.TaxeCalculationRule)
            && Equals(TaxePrePayable, other.TaxePrePayable)
            && Equals(TaxeVat, other.TaxeVat)
            && Equals(RoomBookable, other.RoomBookable);
        }

    }
}
