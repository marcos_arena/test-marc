﻿namespace SunHotels.Providers.Model
{
    public enum TaxeUnit
    {
        Percentage,
        Amount
    }
}