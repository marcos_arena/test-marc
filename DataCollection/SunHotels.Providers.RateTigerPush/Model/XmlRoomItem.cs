﻿namespace SunHotels.Providers.Model
{
    public class XmlRoomItem
    {
        public string XmlRoomId { get; set; }
        public bool NoRefundable { get; set; }
    }
}
