﻿namespace SunHotels.Providers.Model
{
    public class RoomMapData
    {
        public int RoomId { get; set; }
        public string RoomXmlId { get; set; }
        public bool OldFormat { set; get; }
    }
}
