﻿namespace SunHotels.Providers.Model
{
    public class RateTigerSettings
    {
        public string BaseUrl { get; set; }
        public string ExcelRateFilePath { get; set; }       
        public int[] RoomProviderIds { get; set; }
        public bool Syncronize { get; set; }
        public int XmlProviderId { get; set; }
        public int MaxXmlRoomIdLength { get; set; }
        public string[] InvalidTaxCodes { get; set; }
        public string[] NonRefundableRatePlanCodes { get; set; }
    } 
}
