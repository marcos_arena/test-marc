﻿namespace SunHotels.Providers.Model
{
    public enum TaxeCalculationRule
    {
        PerNightPerPerson,
        PerRoomPerNight
    }
}