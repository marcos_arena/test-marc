﻿using SunHotels.Infrastructure.Utils;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace SunHotels.Providers
{
    public class Downloader : IDownloader
    {
        private IConfig config;

        public Downloader(IConfig config)
        {
            this.config = config;
        }
         
        public async Task<Stream> DownloadAsync(string url)
        { 
            var credentials = new NetworkCredential(string.Empty, string.Empty);
            using (var handler = new HttpClientHandler { Credentials = credentials })
            {
                using (var httpClient = new System.Net.Http.HttpClient(handler))
                {
                    var response = await httpClient.GetAsync(url);
                    response.EnsureSuccessStatusCode();
                    return await response.Content.ReadAsStreamAsync();
                }
            }

        }         
    }
}
