﻿using System.IO;
using System.Threading.Tasks;
namespace SunHotels.Providers
{
    public interface IDownloader
    {
        Task<Stream> DownloadAsync(string url); 
    }
}