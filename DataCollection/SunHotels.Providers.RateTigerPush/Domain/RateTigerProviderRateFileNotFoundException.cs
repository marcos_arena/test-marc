﻿using System;
using System.Runtime.Serialization;

namespace SunHotels.Providers.Domain
{
    [Serializable]
    internal class RateTigerProviderRateFileNotFoundException : Exception
    {
        public RateTigerProviderRateFileNotFoundException()
        {
        }

        public RateTigerProviderRateFileNotFoundException(string message) : base(message)
        {
        }

        public RateTigerProviderRateFileNotFoundException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected RateTigerProviderRateFileNotFoundException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}