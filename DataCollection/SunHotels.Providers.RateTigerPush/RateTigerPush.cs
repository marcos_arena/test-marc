﻿using SunHotels.XML;
using System;
using SunHotels.XML.Data;
using SunHotels.Infrastructure.Utils;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using SunHotels.Utils;
using System.Linq;
using SunHotels.Infrastructure.Utils.Legacy;
using System.Collections.Concurrent;
using Sunhotels.Export;
using Newtonsoft.Json;
using SunHotels.Providers.Parser;
using SunHotels.Providers.Infrastructure;
using SunHotels.Providers.Model;
using SunHotels.Providers.Domain;

namespace SunHotels.Providers
{
    public class RateTigerPush : XMLProvider, IRateTigerProvider
    {

        private IConfig config;
        private IDownloader downloader;
        private IContentParser parser;
        private IRateTigerRoomProvider rateTigerProvider;
        private IRateTigerDal rateTigerDal;
        private IXmlRoomGenerator xmlRoomIdGenerator;

        private DateTime today;
        private RateTigerSettings _localConfig;

        private ConcurrentBag<string> xmlRoomIds;

        public RateTigerPush() : base()
        {
            InitializeComponents();
        }

        public RateTigerPush(Configuration config)
            : base(config)
        {
            InitializeComponents();
        }

        private void InitializeComponents()
        {
            config = new SunHotelsGlobalConfig();
            downloader = new Downloader(config);
            parser = new ContentParser(()=> { return Settings; });
            today = DateTime.Today;
            rateTigerDal = new RateTigerDal(OpenConnection(), OnError());
            rateTigerProvider = new RateTigerRoomProvider(rateTigerDal, parser);
            xmlRoomIds = new ConcurrentBag<string>();
            xmlRoomIdGenerator = new XmlRoomGenerator(parser);
        }

        private static Func<System.Data.SqlClient.SqlConnection> OpenConnection()
        {
            return () => { return new System.Data.SqlClient.SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["SunHotels.Providers.Properties.Settings.sunhotelsConnectionString"].ConnectionString); };
        }

        private Func<Exception, string, bool> OnError()
        {
            return (ex, msg) => { LogError(ex, msg); return true; };
        }
        
        protected RateTigerSettings Settings
        {
            get
            {
                if (_localConfig == null)
                {
                    var providerConfigFullPath = configuration.ExportConfigFileFullPath;
                    if (File.Exists(providerConfigFullPath))
                    {
                        var serializer = new XmlSerializer(typeof(RateTigerSettings));
                        var s = new FileStream(providerConfigFullPath, FileMode.Open, FileAccess.Read);
                        _localConfig = (RateTigerSettings)serializer.Deserialize(s);
                    }
                    else
                    {
                        _localConfig = new RateTigerSettings();
                    }
                }
                return _localConfig;
            }
        }


        private RoomConfigurationInfantsAndChildren roomConfigurationInfantsAndChildren;
        public RoomConfigurationInfantsAndChildren RoomInfantsAndChildren
        {
            get
            {
                if (roomConfigurationInfantsAndChildren == null)
                {
                    roomConfigurationInfantsAndChildren = new RoomConfigurationInfantsAndChildren
                    {
                        Id = 1,
                        InfantCountAsChild = true,
                        MaxChildAgeInclusive = 11
                    };
                }

                return roomConfigurationInfantsAndChildren;
            }
        }

        #region " Extract Data" 

        private void AddLogEntry(string entry)
        {
            Console.WriteLine(entry);
            LogEntry(XML.HelpClasses.Log.MessageType.Informational, entry);
        }
        protected override void getData(Root root)
        {
            this.root = root;

            try
            {
                AddLogEntry("Starting GetData");
                GetData(root);
                AddLogEntry("Finishing GetData");

                AddLogEntry("Start Syncronize");
                Syncronize();
                AddLogEntry("Finishing Syncronize");
            }
            catch (RateTigerProviderRateFileNotFoundException ex)
            {
                AddLogEntry(ex.Message);
                throw ex;
            }
            finally
            {
                AddLogEntry("getData finished.");
            }
        }

        #endregion

        protected override void getProviderDefinition(Root root)
        {
            root.ProviderDefinition.Name = !string.IsNullOrEmpty(configuration.ProviderName) ? configuration.ProviderName : configuration.Provider;
            root.ProviderDefinition.Currency = configuration.Currency;
        }

        public bool GetData(Root root)
        {
            try
            {
                Task.Run(async () =>
                {
                    //var basicHotelInfo = await GetXmlFilesToBeImportedAsync(); not needed anymore as now we get the hotelInfo from the same excel
                    var taxeExcelFilePath = Settings.ExcelRateFilePath;

                    AddLogEntry($"Get Static Rates {taxeExcelFilePath} Starting");

                    var fileInfo = new FileInfo(taxeExcelFilePath);

                    if (!fileInfo.Exists)
                    {
                        var folder = Path.GetDirectoryName(taxeExcelFilePath);
                        var ext = new List<string> { ".xlsx" };

                        var directory = new DirectoryInfo(folder);
                        var files = directory.GetFiles("*.*", SearchOption.AllDirectories).Where(s => ext.Contains(Path.GetExtension(s.Extension))).OrderByDescending(f => f.LastWriteTime);

                        if (files.Any())
                        {
                            taxeExcelFilePath = files.First().FullName;
                        }
                        else
                        {
                            throw new RateTigerProviderRateFileNotFoundException($"Unable to load rate file '{Settings.ExcelRateFilePath}'");
                        }
                    }

                    List<RateTigerHotelExcelRowDto> hotelContentFromExcel;
                    using (var streamData = File.OpenRead(taxeExcelFilePath))
                    {
                        hotelContentFromExcel = (await parser.GetHotelContentFromExcelAsync(streamData, Settings.InvalidTaxCodes)).ToList();
                    }

                    AddLogEntry($"Get Static Rates {Settings.ExcelRateFilePath} Finishing");


                    AddLogEntry($"MapHotelRooms Starting");
                    MapHotelRooms(root, hotelContentFromExcel);
                    AddLogEntry($"MapHotelRooms Finishing");


                    return await Task.FromResult(true);
                }).Wait();
                return true;
            }
            catch (AggregateException aggregate)
            {
                if (aggregate.InnerExceptions.Count > 0)
                    throw aggregate.InnerExceptions[0];
                else
                    throw;
            }

        }

        private async Task<IEnumerable<RateTigerHotelDto>> GetXmlFilesToBeImportedAsync()
        {
            if (string.IsNullOrEmpty(Settings.BaseUrl))
                return null;

            var url = $"{Settings.BaseUrl}content.xml";

            AddLogEntry($"Download {url} Starting");
            var content = await downloader.DownloadAsync(url);
            AddLogEntry($"Download {url} Finishing");

            AddLogEntry($"Parse Ota Content Starting");
            var result = await parser.GetOtaContentAsync(content);
            AddLogEntry($"Parse Ota Content Finishing");

            return await Task.FromResult(result);
        }

        private void MapHotelRooms(Root root, List<RateTigerHotelExcelRowDto> hotelsWithRateCode)
        {

            root.RoomConfigInfantsAndChildren.Add(RoomInfantsAndChildren.Id, RoomInfantsAndChildren);

            foreach (var hotelRow in hotelsWithRateCode)
            {
                for (var beds = 1; beds <= hotelRow.MaxAdultsOccupancy; beds++)
                {
                    var roomType = CreateRoomType(hotelRow, beds, 0, hotelRow.MaxAdultsOccupancy, hotelRow.MaxChildsOccupancy);
                    if (!root.RoomTypes.ContainsKey(roomType.Type_Id))
                        root.RoomTypes.Add(roomType.Type_Id, roomType);

                    if (hotelRow.MaxChildsOccupancy > 0)
                    {
                        for (var extraBeds = 1; extraBeds <= hotelRow.MaxChildsOccupancy; extraBeds++)
                        {
                            if (beds + extraBeds <= hotelRow.MaxOccupancy)
                            {
                                roomType = CreateRoomType(hotelRow, beds, extraBeds, hotelRow.MaxAdultsOccupancy, hotelRow.MaxChildsOccupancy);
                                if (!root.RoomTypes.ContainsKey(roomType.Type_Id))
                                    root.RoomTypes.Add(roomType.Type_Id, roomType);
                            }
                        }
                    }
                }
            }

            var countries = hotelsWithRateCode
                .Select(t => new
                {
                    Code = t.HotelInfo.CountryCode,
                    Name = t.HotelInfo.CountryName
                })
                .DisctinctBy(t => new { t.Code });

            foreach (var country in countries)
            {
                var countryHotels = hotelsWithRateCode
                    .Where(t => t.HotelInfo.CountryCode == country.Code)
                    .Select(t => t);

                var countryPlace = new Place()
                {
                    Id = country.Code,
                    Description = country.Name,
                    Places = GetCountryPlaces(countryHotels)
                };

                root.Places.Add(countryPlace);
            }
        }

        private List<Place> GetCountryPlaces(IEnumerable<RateTigerHotelExcelRowDto> countryHotels)
        {
            var places = new List<Place>();

            var cities = countryHotels
                .Select(t => new
                {
                    Code = t.HotelInfo.CityCode,
                    Name = t.HotelInfo.CityName
                })
                .DisctinctBy(t => new { t.Code });

            foreach (var city in cities)
            {
                var cityHotels = countryHotels
                    .Where(t => t.HotelInfo.CityCode == city.Code);

                var place = new Place()
                {
                    Id = city.Code,
                    Description = city.Name,
                    Hotels = GetCityHotels(cityHotels)
                };

                places.Add(place);
            }

            return places;
        }

        private Dictionary<string, Hotel> GetCityHotels(IEnumerable<RateTigerHotelExcelRowDto> cityHotels)
        {
            var hotels = new Dictionary<string, Hotel>();
            foreach (var hotelRow in cityHotels)
            {
                var hotelCode = hotelRow.HotelCode;
                if (!hotels.ContainsKey(hotelCode))
                {
                    var hotelRates = cityHotels.Where(t => t.HotelCode == hotelCode);
                    var hotel = new Hotel()
                    {
                        Id = hotelRow.HotelCode,
                        Name = hotelRow.HotelName,
                        Adress_Country = hotelRow.HotelInfo.CountryName,
                        Adress_City = hotelRow.HotelInfo.CityName,
                        Adress_Street1 = hotelRow.HotelAddress,
                        Classification = hotelRow.HotelClassification,
                        Email = hotelRow.HotelEmail,
                        Currency = hotelRow.Currency,
                        Rooms = GetHotelRooms(hotelRates)
                    };

                    hotels.Add(hotelCode, hotel);
                }
            }

            return hotels;
        }

        private Dictionary<string, Room> GetHotelRooms(IEnumerable<RateTigerHotelExcelRowDto> hotelRates)
        {
            var rooms = new Dictionary<string, Room>();

            var groupByRates = hotelRates.GroupBy(g => new
            {
                g.HotelCode,
                g.RateCode,
                g.RoomType,
                g.MaxOccupancy,
                g.MaxAdultsOccupancy,
                g.MaxChildsOccupancy
            });

            var invalidXmlRoomIds = new List<string>();
            foreach (var groupedRate in groupByRates)
            {
                var hotelRow = groupedRate.First();
                for (var beds = 1; beds <= hotelRow.MaxAdultsOccupancy; beds++)
                {
                    var roomTypeName = hotelRow.RoomTypeName;
                    var roomTypeId = xmlRoomIdGenerator.GenerateRoomTypeId(hotelRow.HotelCode, hotelRow.RoomType, beds, 0, hotelRow.MaxAdultsOccupancy, hotelRow.MaxChildsOccupancy);
                    var roomXmlId = xmlRoomIdGenerator.GetXmlRoomItem(groupedRate, roomTypeId);

                    if (_localConfig.MaxXmlRoomIdLength > 0 && roomXmlId.XmlRoomId.Length > _localConfig.MaxXmlRoomIdLength)
                    {
                        invalidXmlRoomIds.Add(roomXmlId.XmlRoomId);
                        continue;
                    }

                    var currency = hotelRow.Currency;
                    if (!rooms.ContainsKey(roomXmlId.XmlRoomId))
                    {
                        var room = CreateRoom(roomTypeName, roomTypeId, roomXmlId.XmlRoomId, currency, false, today, roomXmlId.NoRefundable);

                        var roomOccupancy = CreateRoomConfigurationOccupancy(beds, beds, 0, 0, beds, beds);
                        room.RoomConfigurationOccupancyId = roomOccupancy.Key;
                        room.RoomConfigurationInfantsAndChildrenId = RoomInfantsAndChildren.Id;
                        rooms.Add(roomXmlId.XmlRoomId, room);
                    }

                    if (hotelRow.MaxChildsOccupancy > 0)
                    {
                        for (var extraBeds = 1; extraBeds <= hotelRow.MaxChildsOccupancy; extraBeds++)
                        {
                            var totalPax = beds + extraBeds;
                            if (totalPax <= hotelRow.MaxOccupancy)
                            {
                                roomTypeId = xmlRoomIdGenerator.GenerateRoomTypeId(hotelRow.HotelCode, hotelRow.RoomType, beds, extraBeds, hotelRow.MaxAdultsOccupancy, hotelRow.MaxChildsOccupancy);
                                roomXmlId = xmlRoomIdGenerator.GetXmlRoomItem(groupedRate, roomTypeId);

                                var room = CreateRoom(roomTypeName, roomTypeId, roomXmlId.XmlRoomId, currency, true, today, roomXmlId.NoRefundable);
                                var roomOccupancy = CreateRoomConfigurationOccupancy(beds, beds, extraBeds, extraBeds, totalPax, totalPax);
                                room.RoomConfigurationInfantsAndChildrenId = RoomInfantsAndChildren.Id;
                                room.RoomConfigurationOccupancyId = roomOccupancy.Key;
                                rooms.Add(roomXmlId.XmlRoomId, room);
                            }
                        }
                    }
                }
            }

            if (invalidXmlRoomIds.Any())
            {
                var invalidXmlRoomStr = string.Join("], [", invalidXmlRoomIds.ToArray());
                AddLogEntry($"WARNING: {invalidXmlRoomIds.Count} rooms generated with XmlRoomId [{invalidXmlRoomStr}] exceed size of {_localConfig.MaxXmlRoomIdLength} chars.");
            }

            return rooms;
        }

        private KeyValuePair<int, RoomConfigurationOccupancies> CreateRoomConfigurationOccupancy(int minAdults, int maxAdults, int minChildren, int maxChildren, int minOccupancy, int maxOccupancy)
        {
            var roomOccupancy = root
                .RoomConfigOccupancies
                .Where(CheckRoomOccupancy(minAdults, maxAdults, minChildren, minOccupancy, maxOccupancy))
                .FirstOrDefault();

            if (roomOccupancy.Value == null)
            {
                var id = 1;

                if (root.RoomConfigOccupancies.Count > 0)
                    id = root.RoomConfigOccupancies.Max(t => t.Value.Id) + 1;

                roomOccupancy = new KeyValuePair<int, RoomConfigurationOccupancies>(id, new RoomConfigurationOccupancies()
                {
                    Id = id,
                    MinAdults = minAdults,
                    MaxAdults = maxAdults,
                    MinChildren = minChildren,
                    MaxChildren = maxChildren,
                    MinOccupancy = minOccupancy,
                    MaxOccupancy = maxOccupancy
                });

                root.RoomConfigOccupancies.Add(id, roomOccupancy.Value);
            }

            return roomOccupancy;
        }

        private static Func<KeyValuePair<int, RoomConfigurationOccupancies>, bool> CheckRoomOccupancy(int minAdults, int maxAdults, int minChildren, int minOccupancy, int maxOccupancy)
        {
            return t => t.Value.MaxAdults == maxAdults &&
            t.Value.MinAdults == minAdults &&
            t.Value.MinChildren == minChildren &&
            t.Value.MaxOccupancy == maxOccupancy &&
            t.Value.MinOccupancy == minOccupancy;
        }

        private Room CreateRoom(string roomTypeName, string roomTypeId, string roomXmlId, string currency, bool hasExtrabeds, DateTime today, bool isNoRefundable)
        {
            if (xmlRoomIds == null)
                xmlRoomIds = new ConcurrentBag<string>();

            xmlRoomIds.Add(roomXmlId);

            var room = new Room()
            {
                TypeId = roomTypeId,
                RoomId = roomXmlId,
                Description = roomTypeName,
                Currency = currency,
                NoRefundable = isNoRefundable
            };

            return room;
        }

        private RoomType CreateRoomType(RateTigerHotelExcelRowDto row, int beds, int extraBeds, int maxAdults, int maxChild)
        {
            var roomTypeId = xmlRoomIdGenerator.GenerateRoomTypeId(row.HotelCode, row.RoomType, beds, extraBeds, maxAdults, maxChild);
            var isNoRefundable = parser.GetNoRefundableValue(row.RateCode);
            return new RoomType()
            {
                Type_Id = roomTypeId,
                Beds = beds,
                ExtraBeds = extraBeds,
                Description = row.RoomTypeName,
                NonRefundable = isNoRefundable
            };
        }


        public bool Syncronize()
        {
            bool syncronizeStatus = true;
            try
            {
                if (Settings.Syncronize)
                {
                    AddLogEntry($"Start get rooms to be mapped");
                    var roomsToBeMapped = rateTigerProvider.GetRoomMap(Settings.RoomProviderIds, xmlRoomIds, Settings.XmlProviderId, _localConfig.MaxXmlRoomIdLength);
                    AddLogEntry($"End get rooms to be mapped");

                    var roomsToBeUpdated = roomsToBeMapped.Where(t => !t.ToBeDeleted).ToList();
                    var roomsToBeDeleted = roomsToBeMapped.Where(t => t.ToBeDeleted).ToList();

                    AddLogEntry($"Updating XmlRoomIds");
                    if (rateTigerProvider.UpdateRooms(roomsToBeUpdated))
                        AddLogEntry($"XmlRoomIds updated", roomsToBeUpdated);
                    else
                        throw new Exception("Update Rooms Failed");

                    AddLogEntry($"Updating Unmapped Rooms");
                    if (rateTigerProvider.DeleteRooms(roomsToBeDeleted))
                        AddLogEntry($"Unmapped Rooms Updated", roomsToBeDeleted);
                    else
                        throw new Exception("Delete Rooms Failed");

                    AddLogEntry($"Total: MappedRooms {roomsToBeUpdated.Count()} UnMappedRooms {roomsToBeDeleted.Count()}");
                }
            }
            catch (Exception ex)
            {
                AddLogEntry($"[ERROR] Update Rooms Exception {ex.Message}");
                syncronizeStatus = false;
            }

            return syncronizeStatus;
        }

        private void LogError(Exception ex, string from)
        {
            AddLogEntry($"{from} {ex.Message}");
        }

        private void AddLogEntry<T>(string msg, T objectToSerialize)
        {
            try
            {
                Task.Run(() =>
                {
                    var fileName = $@"{configuration.LogOutput}{msg}_{DateTime.Now.ToString("yyyyMMddHHmmss")}.json";
                    using (StreamWriter file = File.CreateText(fileName))
                    {
                        var serializer = new JsonSerializer();
                        serializer.Serialize(file, objectToSerialize);
                    }
                }).Wait();
            }
            catch { }
            finally
            {
                AddLogEntry(msg);
            }
        }
    }
}
