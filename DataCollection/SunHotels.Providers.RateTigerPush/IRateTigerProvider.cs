﻿using SunHotels.XML.Data;

namespace SunHotels.Providers
{
    public interface IRateTigerProvider
    {
        bool GetData(Root root);

        bool Syncronize();
    }
}
