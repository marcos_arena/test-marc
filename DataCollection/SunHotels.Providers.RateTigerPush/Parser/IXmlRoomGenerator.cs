﻿using System;
using System.Linq;
using SunHotels.Providers.Model;

namespace SunHotels.Providers.Parser
{
    public interface IXmlRoomGenerator
    {
        XmlRoomItem GetXmlRoomItem(IGrouping<object, RateTigerHotelExcelRowDto> groupedRate, string roomTypeId);
        string GenerateRoomTypeId(string hotelCode, string roomTypeCode, int beds, int extraBeds, int maxAdults, int maxChild);
    }

}