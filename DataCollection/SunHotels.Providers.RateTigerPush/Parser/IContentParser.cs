﻿using SunHotels.Providers.Model;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace SunHotels.Providers.Parser
{
    public interface IContentParser
    {
        Task<IEnumerable<string>> GetHotelXmlUrlFromOtaContentAsync(Stream content);
        Task<IEnumerable<RateTigerHotelDto>> GetOtaContentAsync(Stream content);
        Task<OtaHotelDescriptiveContent> GetHotelContentFromXMLAsync(Stream content);
        Task<IEnumerable<RateTigerHotelExcelRowDto>> GetHotelContentFromExcelAsync(Stream content, string[] invalidTaxeCodes);
        string GetTaxPrefix(TaxeUnit taxeUnit, TaxeCalculationRule taxeCalculationRule, bool taxeIncluded);
        bool GetNoRefundableValue(string rateCode);
        string GetShortRateValue(decimal rateValue);
        decimal GetRateAsDecimal(string rateValue); 
    }
}