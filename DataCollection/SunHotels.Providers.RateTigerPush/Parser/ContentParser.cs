﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.Data;
using SunHotels.Utils;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using System;
using System.Globalization;
using SunHotels.Providers.Model;

namespace SunHotels.Providers.Parser
{
    public class ContentParser : IContentParser
    {
        private CultureInfo ForcedCulture { get; set; }

        private static Dictionary<string, string> shortPrefix = new Dictionary<string, string>()
        {
            { "api", "a" },
            { "ap", "b" },
            { "ari", "c" },
            { "ar", "d" },
            { "ppi", "e" },
            { "pp", "f" },
            { "pri", "g" },
            { "pr", "h" }
        };

        private static Dictionary<TaxeUnit, string> taxTypeMap = new Dictionary<TaxeUnit, string>(){
            { TaxeUnit.Amount, "a" },
            { TaxeUnit.Percentage, "p" }
        };

        private static Dictionary<TaxeCalculationRule, string> taxApplyMap = new Dictionary<TaxeCalculationRule, string>(){
            { TaxeCalculationRule.PerNightPerPerson, "p" },
            { TaxeCalculationRule.PerRoomPerNight, "r" }
        };

        public enum RateTigerExcelCell
        {

            HotelCode,
            HotelName,
            City,
            Country,
            HotelAddress,
            PostalCode,
            HotelEmail,
            BrandName,
            Currency,
            RoomType,
            RoomTypeName,
            MaxOccupancy,
            MaxAdultsOccupancy,
            MaxChildsOccupancy,
            RateCode,
            RateName,
            TaxeCode,
            TaxeName,
            TaxeInformation,
            TaxeAmount,
            TaxeUnit,
            TaxeIncluded,
            TaxeCalculationRule,
            TaxePrePayable,
            TaxeVat 
        }

        private static readonly Dictionary<string, TaxeCodeType> TaxeCodeMap =
            new Dictionary<string, TaxeCodeType>()
            {
                {"TCITYC", TaxeCodeType.CityAndLocalTaxes },
                {"TCNTRR", TaxeCodeType.VATNationaTaxesOnAccommodation },
                {"TVATRR", TaxeCodeType.VATatReducedRate },
                {"TCITYP", TaxeCodeType.CityAndLocalTaxesInPct },
                {"DEPTAX", TaxeCodeType.CountyTax },
                {"TOTHER", TaxeCodeType.OtherTaxes },
                {"TVATR2", TaxeCodeType.VATatReducedRate2 },
                {"TSERVR", TaxeCodeType.ServiceChargeOnAccommodation },
                {"BRETAX", TaxeCodeType.CityTax },
                {"TDFONE", TaxeCodeType.TourismDirhamFee },
                {"TMFAAC", TaxeCodeType.MunicipalityFeeAccomodation },
                {"TMRFAA", TaxeCodeType.MunicipalityRoomFee },
                {"TDFTWO", TaxeCodeType.TourismDirhamFee },
                {"TDFTHR", TaxeCodeType.TourismDirhamFee }
            };


        private static readonly Dictionary<string, TaxeCalculationRule> TaxeRuleMap =
            new Dictionary<string, TaxeCalculationRule>()
            {
                 { "per room and per night",  TaxeCalculationRule.PerRoomPerNight  },
                 { "per night and per person" , TaxeCalculationRule.PerNightPerPerson }
            };

        private static readonly Dictionary<string, bool> BooleanMap =
            new Dictionary<string, bool>()
            {
                { "yes", true },
                { "no", false }
            };

        private static readonly TaxeUnit TaxUnitDefault = TaxeUnit.Amount;
        private static readonly Dictionary<string, TaxeUnit> TaxUnitMap =
            new Dictionary<string, TaxeUnit>()
            {
                { "%" , TaxeUnit.Percentage }
            };

        private Func<RateTigerSettings> Settings { get; set; }
        public ContentParser(Func<RateTigerSettings> settings)
        {
            ForcedCulture = CultureInfo.CreateSpecificCulture("en-GB");
            this.Settings = settings;
        }

        public Task<IEnumerable<string>> GetHotelXmlUrlFromOtaContentAsync(Stream content)
        {
            using (var streamContent = content)
            {
                var repositoryContentType = new OtaRepositoryContent();
                var serializer = new XmlSerializer(typeof(OtaRepositoryContent));
                var contentRepository = (OtaRepositoryContent)serializer.Deserialize(streamContent);
                var hotelFiles = contentRepository.hotels.Select(h => h.file);
                return Task.FromResult(hotelFiles);
            }
        }
        public Task<IEnumerable<RateTigerHotelDto>> GetOtaContentAsync(Stream content)
        {
            using (var streamContent = content)
            {
                var repositoryContentType = new OtaRepositoryContent();
                var serializer = new XmlSerializer(typeof(OtaRepositoryContent));
                var contentRepository = (OtaRepositoryContent)serializer.Deserialize(streamContent);

                var hotels = contentRepository.hotels.Select(h => new RateTigerHotelDto()
                {
                    CityName = h.cityName.ToUpper(),
                    CityCode = $"{h.cityName.ToUpper()}|{h.countryCode.ToUpper()}",
                    CountryName = h.countryName.ToUpper(),
                    CountryCode = h.countryCode.ToUpper(),
                    HotelName = h.hotelName.ToUpper(),
                    HotelCode = h.hotelCode.ToUpper()
                });

                return Task.FromResult(hotels);

            }
        }

        public Task<OtaHotelDescriptiveContent> GetHotelContentFromXMLAsync(Stream content)
        {
            using (var streamContent = content)
            {
                var serializer = new XmlSerializer(typeof(OtaHotelDescriptiveContent));
                var contentRepository = (OtaHotelDescriptiveContent)serializer.Deserialize(streamContent);
                return Task.FromResult(contentRepository);
            }
        }

        public Task<IEnumerable<RateTigerHotelExcelRowDto>> GetHotelContentFromExcelAsync(Stream content, string[] invalidTaxes)
        {
            var items = new List<RateTigerHotelExcelRowDto>() { };
            var data = ReadAsDataTable(content);
            foreach (DataRow row in data.Rows)
            {
                var item = GetHotelExcelRow(row, invalidTaxes);
                if (item != null && item.RoomBookable)
                    items.Add(item);
            }
            return Task.FromResult(items.AsEnumerable());
        }

        public static DataTable ReadAsDataTable(Stream stream)
        {
            DataTable dataTable = new DataTable();
            using (SpreadsheetDocument spreadSheetDocument = SpreadsheetDocument.Open(stream, false))
            {
                WorkbookPart workbookPart = spreadSheetDocument.WorkbookPart;
                IEnumerable<Sheet> sheets = spreadSheetDocument.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>();
                string relationshipId = sheets.First().Id.Value;
                WorksheetPart worksheetPart = (WorksheetPart)spreadSheetDocument.WorkbookPart.GetPartById(relationshipId);
                Worksheet workSheet = worksheetPart.Worksheet;
                SheetData sheetData = workSheet.GetFirstChild<SheetData>();
                IEnumerable<Row> rows = sheetData.Descendants<Row>();

                foreach (Cell cell in rows.ElementAt(0))
                {
                    dataTable.Columns.Add(GetCellValue(spreadSheetDocument, cell));
                }

                foreach (Row row in rows)
                {
                    DataRow dataRow = dataTable.NewRow();
                    for (int i = 0; i < row.Descendants<Cell>().Count(); i++)
                    {
                        dataRow[i] = GetCellValue(spreadSheetDocument, row.Descendants<Cell>().ElementAt(i));
                    }

                    dataTable.Rows.Add(dataRow);
                }

            }
            dataTable.Rows.RemoveAt(0);

            return dataTable;
        }

        private static string GetCellValue(SpreadsheetDocument document, Cell cell)
        { 
            SharedStringTablePart stringTablePart = document.WorkbookPart.SharedStringTablePart;

            if(cell.CellValue == null)
                return cell.InnerText;

            string value = cell.CellValue.InnerXml;
            if (cell.DataType != null)
            {
                if (cell.DataType.Value == CellValues.SharedString)
                {
                    return stringTablePart.SharedStringTable.ChildElements[int.Parse(value)].InnerText;
                }
            }

            return value;
        } 

        private RateTigerHotelExcelRowDto GetHotelExcelRow(DataRow row, string[] invalidTaxes)
        {
            try
            {
                var taxeCode = row[(int)RateTigerExcelCell.TaxeCode].ToString();
                if (invalidTaxes != null && invalidTaxes.Contains(taxeCode))
                    return default(RateTigerHotelExcelRowDto);

                var maxAdultsOccupancy = short.Parse(row[(int)RateTigerExcelCell.MaxAdultsOccupancy].ToString());
                var maxChildsOccupancy = short.Parse(row[(int)RateTigerExcelCell.MaxChildsOccupancy].ToString());
                var maxOccupancy = short.Parse(row[(int)RateTigerExcelCell.MaxOccupancy].ToString()); 
                var taxePrePayable = GetBooleanValue(row[(int)RateTigerExcelCell.TaxePrePayable].ToString());
                var taxVat = GetBooleanValue(row[(int)RateTigerExcelCell.TaxeVat].ToString());
                var taxeName = row[(int)RateTigerExcelCell.TaxeName].ToString();
                var taxeUnit = GetTaxUnit(row[(int)RateTigerExcelCell.TaxeUnit].ToString());
                var taxeCalculationRule = GetCalculationRule(row[(int)RateTigerExcelCell.TaxeCalculationRule].ToString());
                var brandName = row[(int)RateTigerExcelCell.BrandName].ToString(); 
                var currency = row[(int)RateTigerExcelCell.Currency].ToString();
                var hotelAddress = row[(int)RateTigerExcelCell.HotelAddress].ToString(); 
                var hotelCode = row[(int)RateTigerExcelCell.HotelCode].ToString();
                var hotelEmail = row[(int)RateTigerExcelCell.HotelEmail].ToString();
                var hotelName = row[(int)RateTigerExcelCell.HotelName].ToString();
                var rateCode = row[(int)RateTigerExcelCell.RateCode].ToString();
                var roomType = row[(int)RateTigerExcelCell.RoomType].ToString();
                var country = row[(int)RateTigerExcelCell.Country].ToString();
                var city = row[(int)RateTigerExcelCell.City].ToString();
                var roomTypeName = row[(int)RateTigerExcelCell.RoomTypeName].ToString();
                var taxeAmount = ConvertTaxAmout(row[(int)RateTigerExcelCell.TaxeAmount].ToString());
               
                var taxeIncluded = GetBooleanValue(row[(int)RateTigerExcelCell.TaxeIncluded].ToString());
                var taxeInformation = row[(int)RateTigerExcelCell.TaxeInformation].ToString();
                var rateName = row[(int)RateTigerExcelCell.RateName].ToString();

                return new RateTigerHotelExcelRowDto()
                {
                    MaxAdultsOccupancy = maxAdultsOccupancy,
                    MaxChildsOccupancy = maxChildsOccupancy,
                    MaxOccupancy = maxOccupancy,
                    RoomBookable = true,
                    TaxePrePayable = taxePrePayable,
                    TaxeCalculationRule = taxeCalculationRule,
                    TaxeUnit = taxeUnit,
                    BrandName = brandName,
                    CancelPolicy = string.Empty,
                    Currency = currency,
                    HotelAddress = hotelAddress,
                    HotelClassification = string.Empty,
                    HotelCode = hotelCode,
                    HotelEmail = hotelEmail,
                    HotelName = hotelName,
                    RateCode = rateCode,
                    RoomType = roomType,
                    RoomTypeName = roomTypeName,
                    TaxeAmount = taxeAmount,
                    TaxeCode = taxeCode,
                    TaxeIncluded = taxeIncluded,
                    TaxeInformation = taxeInformation,
                    TaxeName = taxeName,
                    TaxeVat = taxVat,
                    RateName = rateName,
                    HotelInfo = new RateTigerHotelDto()
                    {
                        HotelCode = hotelCode,
                        HotelName = hotelName,
                        CityCode = city?.TrimStart().TrimEnd(),
                        CityName = city?.TrimStart().TrimEnd(),
                        CountryCode = country?.TrimStart().TrimEnd(),
                        CountryName = country?.TrimStart().TrimEnd()
                    }
                };

            }
            catch (Exception ex)
            {

                throw ex;
            }
           
        }

        private string ConvertTaxAmout(string taxAmount)
        {
            decimal amount = 0;
            decimal.TryParse(taxAmount, NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture, out amount);
            return amount.ToString("#.##");
        }

        private TaxeUnit GetTaxUnit(string value)
        {
            var stringCellValue = CleanUpCellValue(value);
            return TaxUnitMap.GetValueOrDefault(stringCellValue, TaxUnitDefault);
        }

        private TaxeCalculationRule GetCalculationRule(string value)
        {
            var stringCellValue = CleanUpCellValue(value);
            return TaxeRuleMap.GetValueOrDefault(stringCellValue);
        }

        private bool GetBooleanValue(string value)
        {
            if (string.IsNullOrEmpty(value))
                return false;

            var stringCellValue = CleanUpCellValue(value);
            return BooleanMap.GetValueOrDefault(stringCellValue);
        }

        private string CleanUpCellValue(string value)
        {
            return value.ToLower().Trim();
        }

        public string GetTaxPrefix(TaxeUnit taxeUnit, TaxeCalculationRule taxeCalculationRule, bool taxeIncluded)
        {
            var tUnit = taxTypeMap[taxeUnit];
            var tApply = taxApplyMap[taxeCalculationRule];
            var includePrefix = taxeIncluded ? "i" : string.Empty;
            var prefix = $"{tUnit}{tApply}{includePrefix}";

            return shortPrefix.GetValueOrDefault(prefix);

        }

        public bool GetNoRefundableValue(string rateCode)
        {
            return (Settings?.Invoke().NonRefundableRatePlanCodes != null ? Settings.Invoke().NonRefundableRatePlanCodes.Contains(rateCode) : false);
        }

        public string GetShortRateValue(decimal rateValue)
        {
            var s = rateValue.ToString(ForcedCulture);
            if (s.EndsWith(".00") || s.EndsWith(".0"))
                return ((int)rateValue).ToString();
            else
                return s.Replace(",", ".").TrimStart('0');
        }

        public decimal GetRateAsDecimal(string rateValue)
        {
            decimal value = 0;
            decimal.TryParse(rateValue, NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture, out value);
            return value;
        }
         
    }
}
