﻿using SunHotels.Providers.Model;
using SunHotels.Utils;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SunHotels.Providers.Parser
{
    public class XmlRoomGenerator : IXmlRoomGenerator
    {
        private IContentParser parser;
        public XmlRoomGenerator(IContentParser parser)
        {
            this.parser = parser;
        }

        public XmlRoomItem GetXmlRoomItem(IGrouping<object, RateTigerHotelExcelRowDto> groupedRate, string roomTypeId)
        {
            var first = groupedRate.First();
            var rateCode = first.RateCode;
            var taxPrefix = CreateRoomTaxeSufix(groupedRate.Select(t => t));
            var xmlRoomId = $"{roomTypeId}.{rateCode}|{taxPrefix}";

            return new XmlRoomItem()
            {
                XmlRoomId = xmlRoomId,
                NoRefundable = parser.GetNoRefundableValue(rateCode)
            };
        }


        private string CreateRoomTaxeSufix(IEnumerable<RateTigerHotelExcelRowDto> roomEntries)
        {
            var validEntries = roomEntries;
            if (!validEntries.Any())
                return string.Empty;

            var taxePrefixs = GetTaxPrefixs(validEntries);
            var roomTaxeSufix = new List<string>();
            taxePrefixs.ForEach(t =>
            {
                var amount = parser.GetShortRateValue(t.Value.Sum());
                roomTaxeSufix.Add($"{amount}{t.Key}");
            });

            return string.Join("#", roomTaxeSufix);
        }

        private Dictionary<string, List<decimal>> GetTaxPrefixs(IEnumerable<RateTigerHotelExcelRowDto> validEntries)
        {
            var taxePrefixs = new Dictionary<string, List<decimal>>();
            validEntries.ForEach(t =>
            {
                var ratePrefix = parser.GetTaxPrefix(t.TaxeUnit, t.TaxeCalculationRule, t.TaxeIncluded);
                var rateValue = parser.GetRateAsDecimal(t.TaxeAmount);
                if (rateValue != 0)
                {
                    if (taxePrefixs.ContainsKey(ratePrefix))
                        taxePrefixs[ratePrefix].Add(rateValue);
                    else
                        taxePrefixs.Add(ratePrefix, new List<decimal>() { rateValue });
                }
            });
            return taxePrefixs;
        }

        public string GenerateRoomTypeId(string hotelCode, string roomTypeCode, int beds, int extraBeds, int maxAdults, int maxChild)
        {
            return $"{hotelCode}.{roomTypeCode}.{beds}.{extraBeds}.{maxAdults}.{maxChild}";
        }
    }
}
