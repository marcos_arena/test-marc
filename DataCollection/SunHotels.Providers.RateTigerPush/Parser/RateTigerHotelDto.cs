﻿using System;

namespace SunHotels.Providers.Parser
{
    public class RateTigerHotelDto : IEquatable<RateTigerHotelDto>
    {
        public string CityCode { get; set; }
        public string CityName { get; set; }
        public string CountryCode { get; set; }
        public string CountryName { get; set; }
        public string HotelCode { get; set; }
        public string HotelName { get; set; }

        public bool Equals(RateTigerHotelDto other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return string.Equals(HotelCode, other.HotelCode)
            && string.Equals(HotelName, other.HotelName)
            && string.Equals(CountryName, other.CountryName)
            && string.Equals(CountryCode, other.CountryCode)
            && string.Equals(CityName, other.CityName)
            && string.Equals(CityCode, other.CityCode);

        }
    }
}