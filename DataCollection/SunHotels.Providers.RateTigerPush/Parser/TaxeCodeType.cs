﻿namespace SunHotels.Providers.Parser
{
    public enum TaxeCodeType
    {
        None,
        CityAndLocalTaxes,
        VATNationaTaxesOnAccommodation,
        VATatReducedRate,
        CityAndLocalTaxesInPct,
        CountyTax,
        OtherTaxes,
        VATatReducedRate2,
        ServiceChargeOnAccommodation,
        CityTax,
        TourismDirhamFee,
        MunicipalityFeeAccomodation,
        MunicipalityRoomFee,
    }
}