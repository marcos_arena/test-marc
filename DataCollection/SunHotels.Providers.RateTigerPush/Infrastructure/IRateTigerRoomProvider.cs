﻿using System.Collections.Generic;

namespace SunHotels.Providers.Infrastructure
{
    public interface IRateTigerRoomProvider
    {
        List<RateTigerMappedRoom> GetRoomMap(int[] roomProviders, IEnumerable<string> xmlRoomIds, int xmlProviderId, int maxRoomXmlIdLenght);         
        bool UpdateRooms(List<RateTigerMappedRoom> roomsToBeMapped);
        bool DeleteRooms(List<RateTigerMappedRoom> roomsToBeDeleted);
    }
}