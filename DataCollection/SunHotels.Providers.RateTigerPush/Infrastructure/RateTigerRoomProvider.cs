﻿using SunHotels.Providers.Parser;
using SunHotels.Utils;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SunHotels.Providers.Infrastructure
{
    public class RateTigerRoomProvider : IRateTigerRoomProvider
    {
        private IRateTigerDal dal;
        private IContentParser parser;
        public RateTigerRoomProvider(IRateTigerDal dal, IContentParser parser)
        {
            this.dal = dal;
            this.parser = parser;
        }

        private enum RoomPartType
        {
            HotelCode = 0,
            RoomCode = 1,
            MaxAdults = 2,
            MaxChilds = 3,
            RateCode = 4,
            Beds = 5,
            ExtraBeds = 6
        }

        public List<RateTigerMappedRoom> GetRoomMap(int[] roomProviders, IEnumerable<string> xmlRoomIds, int xmlRoomProviderId, int maxRoomXmlIdLenght)
        {
            var toBeImported = new List<RateTigerMappedRoom>();
            var roomsWithRoomId = dal.GetRoomMappedRooms(roomProviders);

            var xmlRoomIdLookup = xmlRoomIds.ToDictionary(t => t.Split('|')[0], t => t);

            foreach (var room in roomsWithRoomId)
            {
                var roomXMLIdPart = room.RoomXmlID.Split('|');
                var isNewFormat = roomXMLIdPart.Length == 2;

                var code = roomXMLIdPart[0];
                if (!isNewFormat)
                {
                    code = string.Empty;
                    if (roomXMLIdPart.Length == 7)
                    {
                        var hotelCode = roomXMLIdPart[(int)RoomPartType.HotelCode];
                        var roomCode = roomXMLIdPart[(int)RoomPartType.RoomCode];
                        var maxAdults = roomXMLIdPart[(int)RoomPartType.MaxAdults];
                        var maxChilds = roomXMLIdPart[(int)RoomPartType.MaxChilds];
                        var rateCode = roomXMLIdPart[(int)RoomPartType.RateCode];
                        var beds = roomXMLIdPart[(int)RoomPartType.Beds];
                        var extraBeds = roomXMLIdPart[(int)RoomPartType.ExtraBeds];
                        code = $"{hotelCode}.{roomCode}.{beds}.{extraBeds}.{maxAdults}.{maxChilds}.{rateCode}";
                    }
                }

                string roomXmlIdWithRate;
                xmlRoomIdLookup.TryGetValue(code, out roomXmlIdWithRate);

                if (!string.IsNullOrEmpty(code) && !string.IsNullOrEmpty(roomXmlIdWithRate))
                {
                    var xmlRoomIdWithRateParts = code.Split('.');
                    var toBeDeleted = room.RoomXmlID.Length > maxRoomXmlIdLenght;
                    toBeImported.Add(new RateTigerMappedRoom()
                    {
                        RoomId = room.RoomId,
                        RoomXmlID = room.RoomXmlID,
                        RoomXmlIdWithRate = roomXmlIdWithRate,
                        IsNoRefundable = CheckNoRefundableFromXmlRoomId(xmlRoomIdWithRateParts),
                        XmlHotelId = GetXmlHotelIdFromXmlRoomId(xmlRoomIdWithRateParts),
                        XmlProviderId = xmlRoomProviderId,
                        XmlRoomTypeId = GetRoomTypeIdFromXmlRoomId(xmlRoomIdWithRateParts),
                        ToBeDeleted = toBeDeleted
                    });
                }
                else
                {
                    toBeImported.Add(new RateTigerMappedRoom()
                    {
                        RoomId = room.RoomId,
                        RoomXmlID = room.RoomXmlID,
                        XmlProviderId = xmlRoomProviderId,
                        ToBeDeleted = true
                    });
                }
            }

            return toBeImported;
        }


        public bool DeleteRooms(List<RateTigerMappedRoom> roomsToBeDeleted)
        {
            var success = true;

            try
            {
                dal.DeleteRooms(roomsToBeDeleted);
            }
            catch
            {
                success = false;
            }

            return success;
        }

        public bool UpdateRooms(List<RateTigerMappedRoom> roomsToBeMapped)
        {
            var success = true;

            try
            {
                dal.UpdateRooms(roomsToBeMapped);
            }
            catch
            {
                success = false;
            }

            return success;
        }

        private bool CheckNoRefundableFromXmlRoomId(string[] roomXmlIdWithRate)
        {
            var rateCode = roomXmlIdWithRate[roomXmlIdWithRate.Count() - 1];
            return parser.GetNoRefundableValue(rateCode);
        }

        private string GetXmlHotelIdFromXmlRoomId(string[] roomXmlIdWithRate)
        {
            var hotelCode = roomXmlIdWithRate[0];
            return hotelCode;
        }

        private string GetRoomTypeIdFromXmlRoomId(string[] roomXmlIdWithRate)
        {
            var roomTypeId = string.Join(".", roomXmlIdWithRate, 0, roomXmlIdWithRate.Length - 1);
            return roomTypeId;
        }
    }
}
