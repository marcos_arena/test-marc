﻿namespace SunHotels.Providers.Infrastructure
{    public class RateTigerMappedRoom
    {
        public int RoomId;
        public string RoomXmlID;
        public string RoomXmlIdWithRate;
        public string XmlHotelId;
        public string XmlRoomTypeId;
        public int XmlProviderId;
        public bool IsNoRefundable;

        public bool ToBeDeleted { get; internal set; }
    }
}