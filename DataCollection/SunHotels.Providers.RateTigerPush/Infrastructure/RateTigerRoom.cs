﻿namespace SunHotels.Providers.Infrastructure
{
    public class RateTigerRoomDataRecord
    {
        public int RoomId { get; }
        public string RoomXmlID { get; }  

        public RateTigerRoomDataRecord(int roomId, string roomXmlID)
        {
            RoomId = roomId;
            RoomXmlID = roomXmlID;
        }
         
    }
}