﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace SunHotels.Providers.Infrastructure
{
    public class RateTigerDal : IRateTigerDal
    {
        private const int MultipleQueryChunks = 1000;

        private readonly Func<SqlConnection> openConnection;
        private readonly Func<Exception, string, bool> errorException;
        public RateTigerDal(Func<SqlConnection> openConnection, Func<Exception, string, bool> errorException)
        {
            this.openConnection = openConnection;
            this.errorException = errorException;
        }


        public IEnumerable<RateTigerRoomDataRecord> GetRoomMappedRooms(int[] providerIds)
        {
            var rooms = new List<RateTigerRoomDataRecord>();

            var query = $@"
            SELECT
                id as Id,
                XMLRoomid as XmlRoomId
                FROM [sunhotels].[dbo].[rooms] R WITH(NOLOCK)
            WHERE NOT XMLRoomid IS NULL AND roomprovider_id IN({string.Join(",", providerIds)})";

            using (var connection = openConnection())
            {
                using (var command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandText = query;
                    connection.Open();

                    using (var dataTable = new DataTable())
                    {
                        using (var sqlDataAdapter = new SqlDataAdapter())
                        {
                            sqlDataAdapter.SelectCommand = command;
                            sqlDataAdapter.Fill(dataTable);
                        }

                        foreach (DataRow row in dataTable.Rows)
                            rooms.Add(CreateRateTigerRoom(row));
                    }

                    connection.Close();
                }
            }

            return rooms;
        }

        private static RateTigerRoomDataRecord CreateRateTigerRoom(DataRow row)
        {
            return new RateTigerRoomDataRecord((int)row["Id"], (string)row["XmlRoomId"]);

        }

        public void DeleteRooms(List<RateTigerMappedRoom> rooms)
        {
            using (var connection = new SqlConnection(Framework.Config.Config.Instance["ConnectionStrings.Main"]))
            {
                connection.Open();

                var roomIds = rooms.Select(t => t.RoomId).ToArray();
                var XmlProviderId = rooms.Select(t => t.XmlProviderId).FirstOrDefault();

                using (var transaction = connection.BeginTransaction())
                {
                    try
                    {
                        foreach (var roomIdSQL in MultipleQueryLists(roomIds, ","))
                        {
                            using (var command = new SqlCommand())
                            {
                                command.Connection = connection;
                                command.Transaction = transaction;
                                command.CommandText = $@"
                                UPDATE [sunhotels].[dbo].[rooms] 
                                SET deleted = 1,
                                XMLProvider = '{XmlProviderId}'
                                WHERE ID IN({roomIdSQL})";
                                command.ExecuteNonQuery();
                            }
                        }

                        transaction.Commit();

                    }
                    catch (Exception ex)
                    {
                        errorException(ex, "Error trying delete rooms.");
                        transaction.Rollback();
                        throw ex;
                    }
                    finally
                    {
                        if (connection.State == ConnectionState.Open)
                            connection.Close();
                    }
                }
            }
        }

        private static List<string> MultipleQueryLists<T>(IEnumerable<T> sourceList, string separator)
        {
            var tmpLists = new Dictionary<int, List<string>>();
            if (sourceList != null)
            {
                foreach (var element in sourceList)
                {
                    if (tmpLists.Count == 0)
                    {
                        tmpLists.Add(0, new List<string>());
                    }

                    if (tmpLists[tmpLists.Count - 1].Count > MultipleQueryChunks)
                    {
                        tmpLists.Add(tmpLists.Count, new List<string>());
                    }
                    tmpLists[tmpLists.Count - 1].Add(element.ToString());
                }
            }
            var retLists = new List<string>();
            foreach (var tmp in tmpLists.Values)
            {
                retLists.Add(string.Join(separator, tmp));
            }
            return retLists;
        }

        public void UpdateRooms(List<RateTigerMappedRoom> rooms)
        {
            var mappedParams = rooms.Select(CreateUpdateRoomQuery());

            using (var connection = openConnection())
            {
                connection.Open();

                using (var transaction = connection.BeginTransaction())
                {
                    try
                    {

                        foreach (var query in MultipleQueryLists(mappedParams, " "))
                        {
                            using (var command = new SqlCommand())
                            {
                                command.Connection = connection;
                                command.Transaction = transaction;
                                command.CommandText = query;
                                command.ExecuteNonQuery();
                            }
                        } 

                        transaction.Commit();

                    }
                    catch (Exception ex)
                    {
                        errorException(ex, "Error trying update rooms.");
                        transaction.Rollback();
                        throw;
                    }
                    finally
                    {
                        if (connection.State == ConnectionState.Open)
                            connection.Close();
                    }
                }
            }
        }

        private static Func<RateTigerMappedRoom, string> CreateUpdateRoomQuery()
        {
            return room =>
            {
                return $@" 
                    BEGIN
                    UPDATE [sunhotels].[dbo].[rooms]  SET 
                        XMLRoomid = '{room.RoomXmlIdWithRate}',                        
                        XMLRoom = 1,
                        deleted = 0, 
                        norefund = {(room.IsNoRefundable ? 1 : 0)},
                        XMLHotelId = '{room.XmlHotelId}',
                        XMLProvider = '{room.XmlProviderId}'
                    WHERE ID = {room.RoomId}; 
                    END 

                    IF (SELECT COUNT(1) FROM rooms_importTypeMapping WITH(NOLOCK)
                        WHERE sunhotelsRoomId = {room.RoomId}) > 0
                    BEGIN
	                    UPDATE rooms_importTypeMapping
	                    SET importRoomType = '{room.XmlRoomTypeId}'
	                    WHERE sunhotelsRoomId = {room.RoomId} 
                    END
                    ELSE
                    BEGIN
	                    INSERT INTO rooms_importTypeMapping(sunhotelsRoomId, importRoomType)
	                    VALUES({room.RoomId}, '{room.XmlRoomTypeId}')
                    END";

            };
        }
    }
}

