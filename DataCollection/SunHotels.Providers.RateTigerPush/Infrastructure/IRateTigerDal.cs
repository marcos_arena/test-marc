﻿using System.Collections.Generic;

namespace SunHotels.Providers.Infrastructure
{
    public interface IRateTigerDal
    {
        IEnumerable<RateTigerRoomDataRecord> GetRoomMappedRooms(int[] providerIds);
        void UpdateRooms(List<RateTigerMappedRoom> rooms);
        void DeleteRooms(List<RateTigerMappedRoom> rooms);
    }
}