using System;
using System.Collections.Generic;
using System.Text;

namespace SunHotels.Providers
{
    class BoardStructure
    {
        public class Board
        {
            private string type;
            private string bookableId;
            private double price;

            public string Type
            {
                get { return type; }
                set { type = value; }
            }

            public string BookableId
            {
                get { return bookableId; }
                set { bookableId = value; }
            }

            public double PriceAdult
            {
                get { return price; }
                set { price = value; }
            }

            public double PriceChild
            {
                get { return price; }
                set { price = value; }
            }

            public Board(string type, string bookableId, double priceAdult, double priceChild)
            {
                this.type = type;
                this.bookableId = bookableId;
                this.PriceAdult = priceAdult;
                this.PriceChild = priceChild;
            }
        }

        private Board includedBoard;
        private Dictionary<string, Board> additionalBoards;
        private bool hasDefault;

        public BoardStructure()
        {
            // set default
            includedBoard = new Board("none", string.Empty, 0, 0);
            additionalBoards = new Dictionary<string,Board>();
            hasDefault = false;
        }

        public void Create(string type, string bookableId, double priceAdult, double priceChild, string isDefault)
        {
            // Validate board

            // 
            //if ((isDefault == "true") && (price > 0))
            //{
                //Availability for this kind of default board is dropped later so for now we include this.
                //throw new ApplicationException("A default board may not have a price.");
            //}
            if ((isDefault != "true") && (priceAdult <= 0))
            {
               // throw new ApplicationException("An additinoal board must have a price grater than 0!");
            }
            else if ((this.hasDefault) && (isDefault == "true"))
            {
              //  throw new ApplicationException("Default board allready set");
            }
            else
            {
                if (isDefault == "true")
                {
                    includedBoard.Type = type;
                    includedBoard.BookableId = bookableId;
                    includedBoard.PriceAdult = priceAdult;
                    includedBoard.PriceChild = priceChild;
                    this.hasDefault = true;
                }
                else
                {
                    // add additional, if type allready exists use bord with lowest price.
                    if (additionalBoards.ContainsKey(type))
                    {
                        if (additionalBoards[type].PriceAdult > priceAdult)
                        {
                            additionalBoards[type].PriceAdult = priceAdult;
                            additionalBoards[type].BookableId = bookableId;
                        }
                    }
                    else
                    {
                        Board additionalBoard = new Board(type, bookableId, priceAdult, priceChild);
                        additionalBoards.Add(type, additionalBoard);
                    }
                }
            }
        }

        public Board GetIncludedBoard()
        {
            return includedBoard;
        }

        public Dictionary<string, Board>.ValueCollection GetAdditionalBoards()
        {
            return additionalBoards.Values;
        }
    }
}
