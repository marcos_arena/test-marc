using System;
using System.Collections.Generic;
using System.Text;

namespace SunHotels.Providers
{
    public class ExtrabedInfo
    {

        public ExtrabedInfo()
        {
            _ChildPrices = new Dictionary<int, string[]>();
            _AdultPrices = new Dictionary<int, string[]>();
            _RoomTypeExtraChildBeds = new Dictionary<string, int>();
            _RoomTypeExtraAdultBeds = new Dictionary<string, int>();
        }

        private int _MaxChilds  = 0;
        public int MaxChilds
        {
            get { return _MaxChilds; }
            set { _MaxChilds = value; }
        }

        private int _MaxAdults = 0;
        public int MaxAdults
        {
            get { return _MaxAdults; }
            set { _MaxAdults = value; }
        }

        private Dictionary<int, string[]> _ChildPrices;
        public Dictionary<int, string[]> ChildPrices
        {
            get { return _ChildPrices; }
        }

        private Dictionary<int, string[]> _AdultPrices;
        public Dictionary<int, string[]> AdultPrices
        {
            get { return _AdultPrices; }
        }

        private Dictionary<string, int> _RoomTypeExtraAdultBeds;
        public Dictionary<string, int> RoomTypeExtraAdultBeds
        {
            get { return _RoomTypeExtraAdultBeds; }
        }

        private Dictionary<string, int> _RoomTypeExtraChildBeds;
        public Dictionary<string, int> RoomTypeExtraChildBeds
        {
            get { return _RoomTypeExtraChildBeds; }
        }
    }
}
