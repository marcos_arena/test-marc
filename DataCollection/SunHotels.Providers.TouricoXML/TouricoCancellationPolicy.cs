using System;
using System.Collections.Generic;
using System.Text;

namespace SunHotels.Providers
{
    class TouricoCancellationPolicy
    {
        public class Policy
        {
            public string policyType;
            public double? policyAmount;
            public double policyFee;
        }

        public class PolicyPeriod : Policy
        {
            public DateTime startDate;
            public DateTime endDate;
        }

        List<Policy> _Policies;
        List<PolicyPeriod> _PolicyPeriods;

        public TouricoCancellationPolicy(bool createDefault)
        {
            _Policies = new List<Policy>();
            _PolicyPeriods = new List<PolicyPeriod>();

            if (createDefault)
            {
                // 'First you have a default rule of 48 hours prior to check in one night of stay for all out produc.' Ben David, Assaf
                AddPolicy("nights", 2, 1);
            }
        }

        public void AddPolicy(string policyType, double? policyAmount, double policyFee)
        {
            Policy policy = new Policy();
            policy.policyType = policyType;
            policy.policyAmount = policyAmount;
            policy.policyFee = policyFee;
            _Policies.Add(policy);
        }

        public void AddPolicyPeriod(string policyType, double? policyAmount, double policyFee, DateTime startDate, DateTime endDate)
        {
            PolicyPeriod policyPeriod = new PolicyPeriod();
            policyPeriod.policyType = policyType;
            policyPeriod.policyAmount = policyAmount;
            policyPeriod.policyFee = policyFee;
            policyPeriod.startDate = startDate;
            policyPeriod.endDate = endDate;
            _PolicyPeriods.Add(policyPeriod);
        }

        public bool Contains(Policy comparedPolicy)
        {
            foreach (Policy existingPolicy in _PolicyPeriods)
            {
                if (comparedPolicy.policyAmount == existingPolicy.policyAmount)
                {
                    return true;
                }
            }
            return false;
        }

        // Return all effective cancellation policies for specified day making sure that no duplicates of amount is returned.
        public List<Policy> GetCancellationPolicies(DateTime day)
        {
            List<Policy> returnList = new List<Policy>();

            foreach (PolicyPeriod policyPeriod in _PolicyPeriods)
            {
                if (day >= policyPeriod.startDate && day <= policyPeriod.endDate)
                {
                    bool policyExists = false;
                    foreach (Policy existingPolicy in returnList)
                    {
                        if (policyPeriod.policyAmount == existingPolicy.policyAmount)
                        {
                            if (policyPeriod.policyType == existingPolicy.policyType)
                            {
                                if (policyPeriod.policyFee > existingPolicy.policyFee)
                                {
                                    existingPolicy.policyFee = policyPeriod.policyFee;
                                }
                                policyExists = true;
                                continue;
                            }
                            else
                            {
                                policyExists = true;
                                continue;
                            }
                        }
                    }
                    if (!policyExists)
                    {
                        returnList.Add(policyPeriod);
                    }
                }
            }

            if (returnList.Count == 0)
            {
                foreach (Policy policy in _Policies)
                {
                    bool policyExists = false;
                    foreach (Policy existingPolicy in returnList)
                    {
                        if (policy.policyAmount == existingPolicy.policyAmount)
                        {
                            if (policy.policyType == existingPolicy.policyType)
                            {
                                if (policy.policyFee > existingPolicy.policyFee)
                                {
                                    existingPolicy.policyFee = policy.policyFee;
                                }
                                policyExists = true;
                                continue;
                            }
                            else
                            {
                                policyExists = true;
                                continue;
                            }
                        }
                    }
                    if (!policyExists)
                    {
                        returnList.Add(policy);
                    }
                }
            }

            return returnList;
        }

    }
}
