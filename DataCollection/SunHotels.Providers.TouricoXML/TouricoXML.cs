using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Text;
using System.Xml;
using System.Linq;

using SunHotels.Providers.com.touricoholidays.exportdataws;
using SunHotels.XML;
using SunHotels.XML.Data;
using System.Threading.Tasks;
using Sunhotels.Export.Utils;
using SunHotels.Providers.ProviderCommon.DataLoader;
using Sunhotels.Export;
using SunHotels.Providers.ProviderCommon.Mapping;



namespace SunHotels.Providers
{
    /// <summary>
    /// Fetch and parse information from Tourico FTP CSV files and SOAP WebService. Add this information to Sunhotels XML structure.
    /// </summary>
    public class TouricoXML : XMLProvider
    {
        // Create dictionaries for fast search
        Dictionary<string, double> allCurrencies = new Dictionary<string, double>(50);
        Dictionary<string, Place> allPlaces = new Dictionary<string, Place>(1000);
        Dictionary<string, Hotel> allHotels = new Dictionary<string, Hotel>(10000);
        Dictionary<string, DataRow> allRoomTypesStatic = new Dictionary<string, DataRow>(10000);
        Dictionary<string, RoomType> allRoomTypes = new Dictionary<string, RoomType>(10000);
        Dictionary<string, List<Feature>> allHotelFeatures = new Dictionary<string, List<Feature>>(10000);
        Dictionary<string, string> allBoards = new Dictionary<string, string>(100);
        Dictionary<string, TouricoCancellationPolicy> allHotelCancellationPolicies = new Dictionary<string, TouricoCancellationPolicy>(10000);
        Dictionary<string, TouricoCancellationPolicy> allRoomTypeCancellationPolicies = new Dictionary<string, TouricoCancellationPolicy>(100);
        Dictionary<string, CancellationPolicy> allCancellationPolicies = new Dictionary<string, CancellationPolicy>(10000);
        Dictionary<string, DataRow> allResortFees = new Dictionary<string, DataRow>(10000);
        Dictionary<string, List<DataRow>> allSeasons = new Dictionary<string, List<DataRow>>(10000);
        Dictionary<string, List<DataRow>> allImages = new Dictionary<string, List<DataRow>>(10000);
        Dictionary<string, List<DataRow>> allDescriptions = new Dictionary<string, List<DataRow>>(10000);
        //Dictionary<string, string> allRenovations = new Dictionary<string, string>(10000);
        List<string> blockHotels = new List<string>(50);

        //Keep track of logged errors to avoid massive log output (1 per day per room). 
        Dictionary<string, int?> loggedBoardErrors = new Dictionary<string, int?>();
        Dictionary<string, int?> loggedLimitedCheckinErrors = new Dictionary<string, int?>();

		IBasicLogger log = null;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="root"></param>
        protected override void getProviderDefinition(Root root)
        {
            root.ProviderDefinition.Name = configuration.ProviderName;
            root.ProviderDefinition.Currency = "EUR";
        }



        /// <summary>
        /// Entry point for class.
        /// </summary>
        /// <param name="root">Root for Sunhotels XML structure.</param>
        protected override void getData(Root root)
        {
            try
            {
                this.root = root;

                // ------------------------------------------------------------------------------------------------
                // Create log stream
                // ------------------------------------------------------------------------------------------------
                try
                {
                    Directory.CreateDirectory(configuration.LogOutput);
                    string filename = string.Format("Tourico {0}.log", DateTime.Now.ToString("yyyyMMdd HHmmss"));
                    log = BasicLogger.Create(configuration.LogOutput + filename);
                    
                }
                catch (Exception ex)
                {
                    throw new ApplicationException("Unable to create Tourico log file", ex);
                }

                // ------------------------------------------------------------------------------------------------
                // Fetch currency conversion table from database
                // ------------------------------------------------------------------------------------------------
                try
                {
                    SqlDataReader drCurrency = null;
                    SqlConnection cnCurrency = new SqlConnection(configuration.CurrencyConnectionString);
                    SqlCommand cmCurrency = new SqlCommand("SELECT currency, rate FROM exchange_rates_margin;", cnCurrency);

                    cnCurrency.Open();
                    drCurrency = cmCurrency.ExecuteReader();
                    while (drCurrency.Read())
                    {
                        allCurrencies.Add(drCurrency["currency"].ToString(), double.Parse(drCurrency["rate"].ToString()));
                    }
                }
                catch (Exception ex)
                {
                    throw new ApplicationException("Error while connecting to currency database.", ex);
                }

                blockHotels = configuration.BlockHotels.Split(',').ToList();
                
                // Get all static data from ftp
                GetStaticData();

                // Get all dynamic data from web services
                GetDynamicData();
            }
            catch (Exception ex)
            {
                log.WriteLine(string.Format("Unhandled exception [{0}] ", ex.ToString()));
                throw;
            }
            finally
            {
                if (log != null)
                {
                    log.Close();
                }
            }
        }

        /// <summary>
        /// Save all static CSV data in dictionaries to use later when mapping dynamic data.
        /// </summary>
        private void GetStaticData()
        {
            // Read XML document for hotel feature mapping.
            XmlDocument xmlFeatureMapping = new XmlDocument();
            xmlFeatureMapping.Load("Providers\\Tourico_FeatureMapping.xml");

            // Read XML document for board mapping.
            XmlDocument xmlBoardMapping = new XmlDocument();
            xmlBoardMapping.Load("Providers\\Tourico_BoardMapping.xml");

            // Read XML document for cancellation policies mapping.
            XmlDocument xmlCancellationPolicyMapping = new XmlDocument();
            xmlCancellationPolicyMapping.Load("Providers\\Tourico_CancellationPolicyMapping.xml");

            // Get all static data 
            StaticData csvData = new StaticData(configuration);
            List<DataTable> csvDataTables = csvData.GetStaticData();

            int processedFiles = 0;

            // Loop through all static data
			csvDataTables                			
				.All(dt => {
				string csvFileName = dt.TableName ;
                string csvType = csvFileName.Contains("Description") ? "Description" : csvFileName;
				switch (csvType)
				{
					case "PDS_ThfHotelInfo_Europe.csv":
						{
							processedFiles++;

							// Create XML structure Place and Hotel from imported tables
							foreach (DataRow dr in dt.Rows)
							{
								try
								{
									string sCountryCode = (dr["CountryCode"] != DBNull.Value) ? (string)dr["CountryCode"] : "";
									string sStateCode = (dr["StateCode"] != DBNull.Value) ? (string)dr["StateCode"] : "";
									string sState = (dr["State"] != DBNull.Value) ? (string)dr["State"] : "";
									string sCity = (dr["City"] != DBNull.Value) ? (string)dr["City"] : "";

									Place hotelPlace = GetPlace(sCountryCode.Trim(), sStateCode.Trim(), sState.Trim(), sCity.Trim());
									Hotel newHotel = CreateHotel(dr, hotelPlace);

									if (!hotelPlace.Hotels.ContainsKey(newHotel.Id))
									{
										// Save hotel information in hash table for fast access.
										allHotels.Add(newHotel.Id, newHotel);
									}
									else
									{
										log.WriteLine("Hotel Id: {0} already exist in the hotel collection", newHotel.Id);
									}
								}
								catch (Exception ex)
								{
									log.WriteLine(string.Format("Invalid data detected in {0}: {1}", csvFileName, ex.ToString()));
								}
							}
						}
						break;
					case "PDS_ThfHotelRoomTypes.csv":
						{
							processedFiles++;

							// Save static room type information in hash table for fast access
							foreach (DataRow dr in dt.Rows)
							{
								try
								{
									if (!allRoomTypesStatic.ContainsKey(string.Format("{0}", dr["HotelRoomTypeId"])))
									{
										allRoomTypesStatic.Add(string.Format("{0}", dr["HotelRoomTypeId"]), dr);
									}
									else
									{
										// Todo: This should never happen, still it does ! Why god why!
										log.WriteLine(string.Format("Data error: Duplicate room type detected: HotelId[{0}] RoomId[{1}]", dr["HotelId"], dr["RoomId"]));
									}
								}
								catch (Exception ex)
								{
									log.WriteLine(string.Format("Invalid data detected in {0}: {1}", csvFileName, ex.ToString()));
								}
							}
						}
						break;
					case "PDS_ThfHotelsTrans_bblist.csv":
						{
							processedFiles++;

							// Save static board information in hash table for fast access
							foreach (DataRow dr in dt.Rows)
							{
								try
								{
									XmlNode boardNode = xmlBoardMapping.SelectSingleNode(String.Format("//TouricoBoardType[@id=\"{0}\"]", dr["BoardId"].ToString()));
									if (boardNode != null)
									{
										allBoards.Add(dr["BoardId"].ToString(), boardNode.ParentNode.Attributes["name"].Value.ToString());
									}
								}
								catch (Exception ex)
								{
									log.WriteLine(string.Format("Invalid data detected in {0}: {1}", csvFileName, ex.ToString()));
								}
							}
						}
						break;
					case "PDS_ThfHotelAmenity.csv":
						{
							processedFiles++;

							// Save static hotel feature information in hash table for fast access
							foreach (DataRow dr in dt.Rows)
							{
								try
								{
									string hotelId = dr["HotelID"].ToString();

									string featureXpath = String.Format("/Amenities/Amenity[@AmenityId=\"{0}\"]/Feature", dr["AmenityId"].ToString());
									XmlNodeList featureNodeList = xmlFeatureMapping.SelectNodes(featureXpath);
									if (featureNodeList.Count > 0)
									{
										// Add new feature list if it does�t already exists
										if (!allHotelFeatures.ContainsKey(hotelId))
										{
											allHotelFeatures.Add(hotelId, new List<Feature>());
										}

										foreach (XmlNode featureNode in featureNodeList)
										{
											var id = featureNode.Attributes["id"].Value;
											if (!allHotelFeatures[hotelId].Any(f => f.Id == id))
												allHotelFeatures[hotelId].Add(new Feature() { Id = id, Name = featureNode.Attributes["name"].Value, Value = "true" });
										}
									}
								}
								catch (Exception ex)
								{
									log.WriteLine(string.Format("Invalid data detected in {0}: {1}", csvFileName, ex.ToString()));
								}
							}
						}
						break;

					case "PDS_ThfHotelCXL_ByPeriod.csv":
						{
							processedFiles++;

							// Save static hotel cancelation policy exceptions in hash table for fast access
							foreach (DataRow dr in dt.Rows)
							{
								try
								{
                                    string policyKey = dr["Hotel ID"].ToString();
									DateTime dateTo;
									if (DateTime.TryParse(dr["ToDate"].ToString(), out dateTo))
									{
										if (dateTo > DateTime.Today)
										{
											AddCancellationPolicy(dr, policyKey, allHotelCancellationPolicies, xmlCancellationPolicyMapping);
										}
									}
									else
									{
										AddCancellationPolicy(dr, policyKey, allHotelCancellationPolicies, xmlCancellationPolicyMapping);
									}
								}
								catch (Exception ex)
								{
									log.WriteLine(string.Format("Invalid data detected in {0}: {1}", csvFileName, ex.ToString()));
								}
							}
						}
						break;
					case "PDS_ThfHotelCXL_ByRoomType.csv":
						{
							processedFiles++;

							// Save overriding static room type cancellation policy exceptions in hash table for fast access
							foreach (DataRow dr in dt.Rows)
							{
								try
								{
									string policyKey = String.Format("{0}.{1}", dr["HotelId"].ToString(), dr["RoomType"].ToString());
									DateTime dateTo;
									if (DateTime.TryParse(dr["ToDate"].ToString(), out dateTo))
									{
										if (dateTo > DateTime.Today)
										{
											AddCancellationPolicy(dr, policyKey, allRoomTypeCancellationPolicies, xmlCancellationPolicyMapping);
										}
									}
									else
									{
										AddCancellationPolicy(dr, policyKey, allRoomTypeCancellationPolicies, xmlCancellationPolicyMapping);
									}
								}
								catch (Exception ex)
								{
									log.WriteLine(string.Format("Invalid data detected in {0}: {1}", csvFileName, ex.ToString()));
								}
							}
						}

						break;
					case "PDS_ThfHotelAddInfo_ResortFee.csv":
						{
							processedFiles++;

							// Save overriding static room type cancellation policy exceptions in hash table for fast access
							foreach (DataRow dr in dt.Rows)
							{
								try
								{
									string feeKey = String.Format("{0}", dr["HotelId"].ToString());
									// Add new fee if it does�t already exists
									if (!allResortFees.ContainsKey(feeKey))
									{
										allResortFees.Add(feeKey, dr);
									}
								}
								catch (Exception ex)
								{
									log.WriteLine(string.Format("Invalid data detected in {0}: {1}", csvFileName, ex.ToString()));
								}
							}
						}

						break;
					case "PDS_ThfHotelRoomTypeSeasons_DE.csv":
						{
							processedFiles++;

							// Save seasons in hash table for fast access
							foreach (DataRow dr in dt.Rows)
							{
								try
								{
									string seasonKey = String.Format("{0}.{1}", dr["HotelId"].ToString(), dr["roomId"].ToString());
									// Add new season if it does�t already exists
									if (!allSeasons.ContainsKey(seasonKey))
									{
										allSeasons.Add(seasonKey, new List<DataRow>(10));
									}
									// Add datarow with daterange to hotel.room node.
									allSeasons[seasonKey].Add(dr);
								}
								catch (Exception ex)
								{
									log.WriteLine(string.Format("Invalid data detected in {0}: {1}", csvFileName, ex.ToString()));
								}
							}
						}

						break;
					case "PDS_ThfHotelImages.csv":
						{
							processedFiles++;

							// Save images in hash table for fast access
							foreach (DataRow dr in dt.Rows)
							{
								try
								{
									string hotelKey = String.Format("{0}", dr["HotelId"].ToString());
									// Add new hotel key if it does�t already exists
									if (!allImages.ContainsKey(hotelKey))
									{
										allImages.Add(hotelKey, new List<DataRow>(10));
									}
									// Add datarow with image to hotel node.
									allImages[hotelKey].Add(dr);
								}
								catch (Exception ex)
								{
									log.WriteLine(string.Format("Invalid data detected in {0}: {1}", csvFileName, ex.ToString()));
								}
							}
						}
                        break;
                    case "Description":
                            var m = System.Text.RegularExpressions.Regex.Match(csvFileName, "^.*_([a-z]{2,2})_([A-Z]{2,2})\\.csv$");
                            var langCode = m.Groups[1].Value;
                            var countryCode = m.Groups[2].Value;
                            if (dt.Rows.Count != 0)
                                LoadTranslation(langCode, countryCode, dt);
                            processedFiles++;
                        break;
					//case "PDS2_HotelVoucherRemark_THF.csv":
					//		// Save static board information in hash table for fast access
					//		foreach (DataRow dr in dt.Rows)
					//		{
					//			try
					//			{
                                    
					//				if (dr != null)
					//				{
					//					allRenovations.Add(dr["HotelId"].ToString(), dr["VoucherRemark"].ToString());										
					//				}
					//			}
					//			catch (Exception ex)
					//			{
					//				log.WriteLine(string.Format("Invalid data detected in {0}: {1}", csvFileName, ex.ToString()));
					//			}
					//		}
					//		processedFiles++;
					//		break;
					default:
						throw new ApplicationException(String.Format("Unknown tablename for static data. Tablename[{0}]", csvFileName));
				}              
				return true;
			});

            if (processedFiles < csvDataTables.Count)
            {
                throw new ApplicationException(String.Format("At least one static data file missing. Processed tables[{0}]", processedFiles));
            }
        }
        private void LoadTranslation(String langCode, String countryCode, DataTable dt)
        {
            var shortDesc = String.Format("{0}_{1}_{2}", "ShortDescription", langCode, countryCode);
            var longDesc = String.Format("{0}_{1}_{2}", "LongDescription", langCode, countryCode);
			log.WriteLine("Starting load translations...");
            // Find hotel
            foreach (DataRow r in dt.Rows)
            {
                var id = r["HotelId"].ToString();
				//log.WriteLine(string.Format("Starting load translation to hotelId: {0}", id));
                if (!allHotels.ContainsKey(id))
                    continue;
                var hotel = allHotels[id];
                var desc = r[longDesc].ToString();
                if (!String.IsNullOrEmpty(desc) && !hotel.Translations.description.Any(d => d.lang == langCode && d.country == countryCode))
                    hotel.Translations.description.Add(new TranslationsDescription()
                    {
                        lang = langCode,
                        country = countryCode,
                        Value = desc
                    });
                var hdesc = r[shortDesc].ToString().Trim();
                if (!String.IsNullOrEmpty(hdesc) && !hotel.Translations.headline.Any(d => d.lang == langCode && d.country == countryCode))
                    hotel.Translations.headline.Add(new TranslationsHeadline()
                    {
                        lang = langCode,
                        country = countryCode,
                        Value = hdesc
                    });
				//log.WriteLine(string.Format("Translation loaded from hotelId: {0}", id));
            }
        }
        /// <summary>
        /// Help method used by both types of cancellation policies. Saves static 
        /// cancellation policy exceptions in hash table for fast access.
        /// </summary>
        /// <param name="policyDataRow">Policy data</param>
        /// <param name="policyKey">Param to use as dictionary key</param>
        /// <param name="policyDictionary">Dictionary to save policy in</param>
        /// <param name="policyMapping">XML document to use in Tourico to Sunhotels policy mapping</param>				
        private void AddCancellationPolicy(DataRow policyDataRow, string policyKey, Dictionary<string, TouricoCancellationPolicy> policyDictionary, XmlDocument policyMapping)
        {
            // Add new policy if it doesn't exists
            if (!policyDictionary.ContainsKey(policyKey))
            {
                policyDictionary.Add(policyKey, new TouricoCancellationPolicy(false));
            }

            // Get mapped cancellation policy type.
            string xpath = String.Format("//TouricoPenalty[@id=\"{0}\"]", policyDataRow["Fee Rule"].ToString());
            XmlNode policyNode = policyMapping.SelectSingleNode(xpath);

            if (policyNode == null)
            {
                throw new ApplicationException(string.Format("Error: Unkown tourico cancellation policy found. Hotel id:{0} Penalty basis:{1}", policyDataRow["Hotel ID"].ToString(), policyDataRow["Fee Rule"].ToString()));
            }

            string policyType = policyNode.ParentNode.Attributes["id"].Value;
            double? policyAmount = null;
            if (policyDataRow["Timing Rule"].ToString() != "Always")
            {
                policyAmount = double.Parse(policyDataRow["Timing Value / Days"].ToString());
            }
            double policyFee = double.Parse(policyDataRow["Fee Value"].ToString(), CultureInfo.InvariantCulture);

            //Check if the current row contains default data or a period.
            if (policyDataRow["Period"].ToString() == "Default" || policyDataRow["Period"].ToString() == "")
            {
                policyDictionary[policyKey].AddPolicy(policyType, policyAmount, policyFee);
            }
            else
            {
                try
                {
                    DateTime startDate = DateTime.Parse(policyDataRow["FromDate"].ToString(), CultureInfo.InvariantCulture);
                    DateTime endDate = DateTime.Parse(policyDataRow["ToDate"].ToString(), CultureInfo.InvariantCulture);

                    if (endDate >= DateTime.Today)
                    {
                        policyDictionary[policyKey].AddPolicyPeriod(policyType, policyAmount, policyFee, startDate, endDate);
                    }
                }
                catch (Exception e)
                {
                    throw new Exception("Error processing cancellation policy with key " + policyKey + ": " + e.Message + 
                                        " - FromDate Value: " + policyDataRow["FromDate"].ToString() + 
                                        " - ToDate Value: " + policyDataRow["ToDate"].ToString(), e);
                }
            }
        }



        /// <summary>
        /// Collect all dynamic data from web services (GetRates, GetAvailability)
        /// </summary>
        private void GetDynamicData()
        {
            bool useLocalCaceFiles = false;

            // Read XML document for place mapping.
            XmlDocument xmlRoomTypeMapping = new XmlDocument();
            xmlRoomTypeMapping.Load("Providers\\Tourico_RoomTypeMapping.xml");

            ExportDataService exportObject = new ExportDataService();
            LoginHeader exportLogin = new LoginHeader();
            exportObject.EnableDecompression = true;
            
            // ------------------------------------------------------------------------------------------------
            // Prepare Web Service for use.
            // ------------------------------------------------------------------------------------------------
            exportLogin.username = configuration.UserId;
            exportLogin.password = configuration.Password;
            exportObject.Url = configuration.WebServiceUrl;
            exportObject.LoginHeaderValue = exportLogin;

            DateTime startDate = DateTime.Today;
            DateTime endDate = DateTime.Today.AddDays(configuration.NrDaysInCache - 1);

            // Calculate timeout: 20 seconds base plus 1 second per day.
            exportObject.Timeout = 180000 + ((endDate - startDate).Days * 3000);
            
			Availability exportAvailability = null;
			Rates exportRates = null;
			Task[] ts = new Task[]{
				Task.Factory.StartNew(() => {
					// ------------------------------------------------------------------------------------------------
					// Request rates object from Web Service
					// ------------------------------------------------------------------------------------------------
					WriteDebug(String.Format("Fetching rates for period: {0} -> {1} Timeout [{2}]", startDate.ToShortDateString(), endDate.ToShortDateString(), (exportObject.Timeout / 1000).ToString()));
					var file = configuration.LocalTemp + @"\Rates (" + DateTime.Now.ToString("yyyyMMdd HHmmss") + ").xml";
					
					try
					{
						if (!useLocalCaceFiles)
						{
							exportRates = exportObject.GetRates(false, startDate, endDate, new string[] { });
							// Todo remove debug
							exportRates.WriteXml(file);
						}
						else
						{
							// Use an allready collected xml file to speed up testing
							exportRates = new Rates();
							exportRates.ReadXml(configuration.LocalTemp + @"\Rates.xml");
						}
					}
					catch (Exception ex)
					{
						throw new ApplicationException("Error calling GetRates through Web Service.", ex);
					}
				}),
				Task.Factory.StartNew(() => {
					// ------------------------------------------------------------------------------------------------
					// Request availability object from Web Service
					// ------------------------------------------------------------------------------------------------
					WriteDebug(String.Format("Fetching availability for period: {0} -> {1} Timeout [{2}]", startDate.ToShortDateString(), endDate.ToShortDateString(), (exportObject.Timeout / 1000).ToString()));
					var file = configuration.LocalTemp + @"\Avail (" + DateTime.Now.ToString("yyyyMMdd HHmmss") + ").xml";
					
					try
					{
						if (!useLocalCaceFiles)
						{
							exportAvailability = exportObject.GetAvailability(false, startDate, endDate, new string[] { });
							// Todo remove debug
							exportAvailability.WriteXml(file);
						}
						else
						{
							// Use an allready collected xml file to speed up testing
							exportAvailability = new Availability();
							exportAvailability.ReadXml(configuration.LocalTemp + @"\Avail.xml");
						}
					}
					catch (Exception ex)
					{
						throw new ApplicationException("Error calling GetAvailability through Web Service.", ex);
					}

				})
			};

			Task.WaitAll(ts);

            // ------------------------------------------------------------------------------------------------
            // Save availability information in hash table for fast access
            // ------------------------------------------------------------------------------------------------
            WriteDebug("Creating hash table for availability");
            Dictionary<string, string[]> allAvailabilityRoomTypeRows = new Dictionary<string, string[]>(10000);

			exportAvailability.Hotel
				.AsEnumerable()
				.All(dr =>
				{
					Availability.HotelRow ahr = (Availability.HotelRow)dr;
					foreach (Availability.RoomTypeRow artr in ahr.GetRoomTypeRows())
					{
						string roomType = string.Format("{0}.{1}", ahr.hotelId, artr.roomId);
						string[] ad = SplitPeriod(artr.availability);

						if (ad.Length != configuration.NrDaysInCache)
						{
							throw new ApplicationException(string.Format("Data error: Availability period differs from period specified by configuration file. Hotel_Id:{0}", artr.Hotel_Id));
						}

						if (!allAvailabilityRoomTypeRows.ContainsKey(roomType))
						{
							allAvailabilityRoomTypeRows.Add(roomType, ad);
						}
						else
						{
							for (int i = 0; i < ad.Length; i++)
							{
								if (ad[i] == "-" || allAvailabilityRoomTypeRows[roomType][i] == "-")
								{
									allAvailabilityRoomTypeRows[roomType][i] = "-";
								}
								else
								{
									if (Convert.ToInt32(ad[i]) < Convert.ToInt32(allAvailabilityRoomTypeRows[roomType][i]))
									{
										allAvailabilityRoomTypeRows[roomType][i] = ad[i];
									}
								}
							}
						}
					}
					return true;
				});


            // ------------------------------------------------------------------------------------------------
            // Process all Hotels in rates response.
            // ------------------------------------------------------------------------------------------------
            WriteDebug("Process all Hotels in rates response");
			exportRates.Hotel.AsEnumerable()
				.All(dr =>
				{
					Rates.HotelRow rhr = (Rates.HotelRow)dr;
					string ratesHotelRowHotelId = rhr.hotelId;

					if (blockHotels.Contains(ratesHotelRowHotelId))
					{
						log.WriteLine(string.Format("Hotel manually blocked in config file: hotelId[{0}]", ratesHotelRowHotelId));
						return true;
					}

					if (!allHotels.ContainsKey(ratesHotelRowHotelId))
					{
						log.WriteLine(string.Format("Data error: Hotel not found in CSV structure: hotelId[{0}]", ratesHotelRowHotelId));
						return true;
					}

					// DEBUG, find availability for one hotel only!
					//if (ratesHotelRowHotelId != "1259794")
					//{
					//    continue;
					//}

					// Check for resort fees and drop hotel if fee applies.
					if (allResortFees.ContainsKey(ratesHotelRowHotelId))
					{
						DataRow feeRow = allResortFees[ratesHotelRowHotelId];

						if (GetBoolValue(feeRow["IsMandatoryFee"].ToString()) == true)
						{
							log.WriteLine("Hotel dropped due to mandatory resort fees. Hotel: {0} Fee: {1}", ratesHotelRowHotelId, feeRow["VoucherRemark"].ToString());
							return true;
						}
					}

					string sHotelCurrency = rhr.currency;

					// Tourico has faulty currencies for some hotels in russia.
					if (sHotelCurrency.ToUpper() == "RUR")
					{
						sHotelCurrency = "RUB";
					}

					// RMB is same as CNY.
					if (sHotelCurrency.ToUpper() == "RMB")
					{
						sHotelCurrency = "CNY";
					}

					if (!allCurrencies.ContainsKey(sHotelCurrency))
					{
						log.WriteLine("Hotel dropped due to non existing currency. Hotel: {0} Currency: {1}", ratesHotelRowHotelId, sHotelCurrency);
						return true;
					}

					// Add exported feature info. to hotel root obj
					AddFeaturesAndImagesToHotel(ratesHotelRowHotelId);

					// Check for duplicate roomtypes.
					int[] iDuplicateRoomIds = (rhr.GetRoomTypeRows()
						.GroupBy(i => i.hotelRoomTypeId)
						.Where(g => g.Count() > 1)
						.Select(g => g.Key)).ToArray<int>();



					// ------------------------------------------------------------------------------------------------
					// Process all Room Types per Hotel in rates response.
					// ------------------------------------------------------------------------------------------------
					foreach (Rates.RoomTypeRow rrtr in rhr.GetRoomTypeRows())
					{
						if (iDuplicateRoomIds.Contains<int>(rrtr.hotelRoomTypeId))
						{
							log.WriteLine(string.Format("Data error: Duplicate room type detected in rates response: hotelId[{0}] hotelRoomTypeId[{1}]", rhr.hotelId, rrtr.hotelRoomTypeId));
							continue;
						}

						//if (rrtr.maxChilds == 0)
						//{
						//    log.WriteLine("Room dropped because it does not allow children. Hotel: {0} Room: {1}", ratesHotelRowHotelId, ratesRoomTypeRowRoomId);
						//    continue;
						//}

						string ratesRoomTypeRowRoomId = rrtr.roomId;
						string ratesRoomTypeRowHotelRoomTypeId = rrtr.hotelRoomTypeId.ToString();

						// Check hash table for availability and only add Room Type and Room information if availability data exists.
						if (allAvailabilityRoomTypeRows.ContainsKey(string.Format("{0}.{1}", ratesHotelRowHotelId, ratesRoomTypeRowRoomId)) &&
							(allRoomTypesStatic.ContainsKey(ratesRoomTypeRowHotelRoomTypeId)))
						{
							// ------------------------------------------------------------------------------------------------
							// Proces all Rooms per Room Type.
							// Add one Room Type per Room. This means that each Room Type only contains one room.
							// ------------------------------------------------------------------------------------------------                        

							Rates.RoomRow[] roomTypeRooms = rrtr.GetRoomRows();
							bool isRoomGroup = roomTypeRooms.Length > 1;
							RoomGroup roomGroup = null;

							if (isRoomGroup)
							{
								roomGroup = new RoomGroup();

								// set a unique room group id
								roomGroup.RoomGroupId = string.Format("{0}.{1}", ratesHotelRowHotelId, ratesRoomTypeRowHotelRoomTypeId);
							}

							// Map static roomtype data
							DataRow srtdr = allRoomTypesStatic[ratesRoomTypeRowHotelRoomTypeId];

							// Check child age and log. Age is checked later to see if child extrabed prices should be used.
							int iChildAge;
							try
							{
								iChildAge = int.Parse(srtdr["ChildAge"].ToString());
							}
							catch
							{
								continue;
							}
							//if (iChildAge < 11)
							//{
							//    log.WriteLine("Child age {0} is too low. Room: {1}.{2}", iChildAge, ratesHotelRowHotelId, ratesRoomTypeRowRoomId);
							//}

							// collect extrabed info for the room/s

							ExtrabedInfo extrabedInfo;
							try
							{
								extrabedInfo = CreateExtraBedInfo(rrtr, xmlRoomTypeMapping, ratesHotelRowHotelId);
							}
							catch
							{
								log.WriteLine("Error in extrabed structure. Dropping room: {0}.{1}", ratesHotelRowHotelId, ratesRoomTypeRowRoomId);
								continue;
							}

							foreach (Rates.RoomRow rrr in roomTypeRooms)
							{
								string ratesRoomRowType = rrr.type;

								//bool skipDouble = false;
								//if (rrr.type == "DoubleRoom")
								//{
								//    foreach (Rates.RoomRow rrr2 in roomTypeRooms)
								//    {
								//        if (rrr2.type == "TwinRoom")
								//        {
								//            skipDouble = true;
								//            continue;
								//        }
								//    }
								//}

								//if (skipDouble == true)
								//{
								//    continue;
								//}

								// Read fixed bed and description mapping file for Room Types                            
								XmlNode node = xmlRoomTypeMapping.SelectSingleNode(String.Format("/RoomTypes/RoomType[@id=\"{0}\"]", ratesRoomRowType));

								// Check if type has fixed beds & descriptiona and check if beds for type is higher than the maximum allowed adults.
								if (node != null)
								{
									if (int.Parse(node.Attributes["beds"].Value) > rrtr.maxAdults)
									{
										// Drop room if roomtype has more beds than allowed on the room.
										// Strangely enough Tourico does not filter this out.
										log.WriteLine("RoomType {0} allows {1} beds but room only allows {2}  beds: {3}.{4}", rrr.type, node.Attributes["beds"].Value, rrtr.maxAdults, ratesHotelRowHotelId, ratesRoomTypeRowRoomId);
										continue;
									}
								}

								RoomType newRoomType = new RoomType();
								DataRow roomTypeStaticRow = allRoomTypesStatic[ratesRoomTypeRowHotelRoomTypeId];

								// Check if type has fixed beds & description, if not use returned value (Unit).
								if (node == null)
								{
									newRoomType.Beds = rrtr.maxAdults;
									newRoomType.Description = string.Format("{0} {1}", roomTypeStaticRow["RtNameEn"], ratesRoomRowType);
								}
								else
								{
									newRoomType.Beds = int.Parse(node.Attributes["beds"].InnerText);
									newRoomType.Description = string.Format("{0} {1}",
																			roomTypeStaticRow["RtNameEn"],
																			node.Attributes["description"].Value);
									if (extrabedInfo != null && extrabedInfo.RoomTypeExtraChildBeds.ContainsKey(ratesRoomRowType) && iChildAge >= 11)
									{
										newRoomType.ExtraBeds = extrabedInfo.RoomTypeExtraChildBeds[ratesRoomRowType];
									}
								}

								// Set Room Type data
								newRoomType.Type_Id = string.Format("{0}.{1}.{2}.{3}", ratesRoomTypeRowRoomId, ratesRoomRowType, newRoomType.Beds, newRoomType.ExtraBeds);
								newRoomType.Bookable_Id = ratesRoomTypeRowHotelRoomTypeId.ToString();

								Room newRoom = new Room();

								newRoom.Description = newRoomType.Description;
								newRoom.TypeId = newRoomType.Type_Id;

								newRoom.RoomId = newRoomType.Bookable_Id;

								// Non refundable
								newRoomType.NonRefundable = newRoomType.Description.ToLower().IndexOf("non refund") != -1;

								// Create the discount period list used for creating the discount for each room in this room type
								List<DiscountPeriod> discountPeriods = CreateDiscountPeriods(rrtr);


								//foreach (Rates.RuleRow rur in rrtr.GetRuleRows())
								//{
								//Todo: Handle rules.

								// for example these rules probably needs to be handled
								// <Rule type="ArrWeekDays" startDate="6/17/2008" endDate="7/16/2008" value="1111110" />
								// <Rule type="MinMax" startDate="6/17/2008" endDate="7/16/2008" seqNum="0" minDays="2" maxDays="99" weekDays="0000011" />

								// WriteDebug(String.Format("Rules found: {0}", rur.type));
								//}

								// ------------------------------------------------------------------------------------------------
								// Process rates and availability strings for requested days.
								// ------------------------------------------------------------------------------------------------
								string[] availRates = SplitPeriod(rrr.price);

								if (availRates.Length != configuration.NrDaysInCache)
								{
									throw new ApplicationException(string.Format("Data error: Price period differs from period specified by configuration file. hotelRoomTypeId:{0}", rrtr.hotelRoomTypeId));
								}

								string[] availRooms = allAvailabilityRoomTypeRows[string.Format("{0}.{1}", ratesHotelRowHotelId, ratesRoomTypeRowRoomId)];

								//rules

								//string[] availExtrabedRates = GetExtraBedRates();

								// Create board structure for requested period
								BoardStructure[] boardStructures = new BoardStructure[configuration.NrDaysInCache];

								CreateBoardStructureForPeriod(boardStructures, rrtr, sHotelCurrency, newRoomType.Beds + newRoomType.ExtraBeds, ratesHotelRowHotelId);

								for (int iDay = 0; iDay < configuration.NrDaysInCache; iDay++)
								{
									DateTime dateDay = startDate.AddDays(iDay);
									AvailabilityPeriod availDay = new AvailabilityPeriod();

									try
									{
										availDay.DateFrom = dateDay;
										availDay.DateTo = dateDay;
										availDay.AvailabilityAvailableUnits = Int16.Parse(availRooms[iDay] == "" ? "0" : availRooms[iDay]);

										if (availDay.AvailabilityAvailableUnits == 0)
										{
											continue;
										}

										availDay.Pricing_BaseIncludedBoard = boardStructures[iDay].GetIncludedBoard().Type;
										if (boardStructures[iDay].GetIncludedBoard().PriceAdult > 0 && availRates[iDay] != "-")
										{
											double basePrice = double.Parse(FixPrice(sHotelCurrency, availRates[iDay], 0), CultureInfo.InvariantCulture);
											double boardPrice = boardStructures[iDay].GetIncludedBoard().PriceAdult;
											availDay.Pricing_BasePrice = (basePrice + (boardPrice * (newRoomType.Beds + newRoomType.ExtraBeds))).ToString(CultureInfo.InvariantCulture);
										}
										else
										{
											// Always pick highest price.
											availDay.Pricing_BasePrice = FixPrice(sHotelCurrency, availRates[iDay], -1);
										}

									}
									catch (Exception ex)
									{
										log.WriteLine(String.Format("Unable to evaluate price: HotelId[{0}] RoomId[{1}] Date[{2}] Message[{3}]", rhr.hotelId, rrtr.roomId, dateDay, ex.ToString()));
										continue;
									}

									bool skip = false;

									char[] cMinMaxDays = new char[7] { '0', '0', '0', '0', '0', '0', '0' };

									foreach (Rates.RuleRow rur in rrtr.GetRuleRows())
									{
										if (DateTime.Parse(rur.startDate) <= dateDay && dateDay <= DateTime.Parse(rur.endDate) && availDay.Pricing_BasePrice != String.Empty)
										{
											if (rur.type == "MinMax")
											{
												char[] cThisMinMaxDays = rur.weekDays.ToCharArray();

												string tempDiscountPrice = FixPrice(sHotelCurrency, availRates[iDay], Int32.Parse(rur.seqNum));
												if (tempDiscountPrice != String.Empty)
												{
													Decimal discountPrice = Convert.ToDecimal(tempDiscountPrice, CultureInfo.InvariantCulture);
													Decimal basePrice = Convert.ToDecimal(availDay.Pricing_BasePrice, CultureInfo.InvariantCulture);
													int discount;
													try
													{
														discount = (int)((1 - (discountPrice / basePrice)) * 100); //Calculate the discount in percent.
													}
													catch (Exception ex)
													{
														log.WriteLine(String.Format("Unable to evaluate discount: HotelId[{0}] HotelRoomTypeId[{1}] Date[{2}] Message[{3}]", rhr.hotelId, rrtr.hotelRoomTypeId, dateDay, ex.ToString()));
														discount = 0;
													}
													if (discount > 0)
													{
														// Check if discount is available on current weekday.
														if (cThisMinMaxDays[(int)dateDay.DayOfWeek] == '1')
														{
															string seasonKey = string.Format("{0}.{1}", ratesHotelRowHotelId, ratesRoomTypeRowRoomId);
															int seasonId = GetSeasonID(dateDay, seasonKey, DateTime.Parse(rur.startDate), rur.weekDays);
															availDay.Discounts.TryAdd(Discount.MinMaxPercent(Convert.ToInt32(rur.minDays), Convert.ToInt32(rur.maxDays), discount, seasonId));
														}
													}

													// Check if rule applies to current weekday.
													if (rur.minDays != null)
													{
														if (cThisMinMaxDays[(int)dateDay.DayOfWeek] == '1')
														{
															if (availDay.MinimumStay == 0 || availDay.MinimumStay > short.Parse(rur.minDays))
															{
																availDay.MinimumStay = short.Parse(rur.minDays);
															}
														}
													}


												}
											}
											else if (rur.type == "ArrWeekDays")
											{
												char[] cThisArrDays = rur.value.ToCharArray();
												if (cThisArrDays[(int)dateDay.DayOfWeek] == '0')
												{
													skip = true;
													string limitedCheckinErrorString = String.Format("Dropping limited checkin days: {0}.{1}", ratesHotelRowHotelId, ratesRoomTypeRowHotelRoomTypeId);
													if (!loggedLimitedCheckinErrors.ContainsKey(limitedCheckinErrorString))
													{
														log.WriteLine(String.Format("Dropping limited checkin days: {0}", limitedCheckinErrorString));
														loggedLimitedCheckinErrors.Add(limitedCheckinErrorString, null);
													}
												}
											}
										}
									}

									if (skip)
									{
										continue;
									}

									SunHotels.Providers.BoardStructure.Board includedBoard = boardStructures[iDay].GetIncludedBoard();

									availDay.Pricing_BookableBoardId = boardStructures[iDay].GetIncludedBoard().BookableId;

									if (extrabedInfo != null && newRoomType.ExtraBeds > 0)
									{
										HandleExtraBeds(availDay, extrabedInfo, sHotelCurrency, newRoomType.ExtraBeds, iDay, ratesRoomRowType, iChildAge);
									}

									if (discountPeriods.Count > 0)
									{
										foreach (DiscountPeriod dp in discountPeriods)
										{
											try
											{
												if (dp.InDiscountPeriod(dateDay) && availDay.Pricing_BasePrice != "")
												{
													string seasonKey = string.Format("{0}.{1}", ratesHotelRowHotelId, ratesRoomTypeRowRoomId);
													int seasonId = GetSeasonID(dateDay, seasonKey, dp.StartDate, "");

													if (dp.Type == DiscountPeriod.DiscountType.PayStay)
													{
														availDay.Discounts.TryAdd(Discount.PayStay(dp.Stay, dp.MaxFree / (dp.Stay - (int)dp.Value), dp.Stay - dp.Value, seasonId, checkinBased: true));
													}
													else if (dp.Type == DiscountPeriod.DiscountType.Amount)
													{
														//log.WriteLine(String.Format("Dropping discount type amount: hotelId[{0}] roomId[{1}] stay[{2}] value[{3}]", rhr.hotelId, rrtr.roomId, dp.Stay, dp.Value));
														availDay.Discounts.TryAdd(Discount.ValueAmount(dp.Stay, ToEuro(dp.Value, sHotelCurrency), seasonId, checkinBased: true));
													}
													else if (dp.Type == DiscountPeriod.DiscountType.Percentage)
													{
														availDay.Discounts.TryAdd(Discount.MinMaxPercent(dp.Stay, dp.MaxNights, dp.Value, seasonId, checkinBased: true));
													}
												}
											}
											catch (Exception ex)
											{
												log.WriteLine(String.Format("Dropping discount: hotelId[{0}] hotelRoomTypeId[{1}] type[{2}] message[{3}]", rhr.hotelId, rrtr.hotelRoomTypeId, dp.Type, ex.ToString()));
											}

										}
									}

									// Add additional boards
									foreach (BoardStructure.Board board in boardStructures[iDay].GetAdditionalBoards())
									{
										// Children are treated as adults, and are therefore not included in the xml structure.  
										AvailabilityPeriod.AdditionalBoard additionalBoard = new AvailabilityPeriod.AdditionalBoard(board.Type, board.PriceAdult.ToString(CultureInfo.InvariantCulture), board.PriceChild.ToString(CultureInfo.InvariantCulture), board.BookableId);
										availDay.AdditionalBoards.Add(additionalBoard);
									}

									// ------------------------------------------------------------------------------------------------
									// Check cancellation policies for period, room type cancellation policies overrides.
									// ------------------------------------------------------------------------------------------------
									string roomTypePolicyKey = string.Format("{0}.{1}", ratesHotelRowHotelId, srtdr["RtNameEn"].ToString());
									TouricoCancellationPolicy hotelCancellationPolicy;

									if (allRoomTypeCancellationPolicies.ContainsKey(roomTypePolicyKey))
									{
										//WriteDebug("Found overriding room type cancellation policy!");
										hotelCancellationPolicy = allRoomTypeCancellationPolicies[roomTypePolicyKey];

									}
									else if (allHotelCancellationPolicies.ContainsKey(ratesHotelRowHotelId))
									{
										//WriteDebug(String.Format("Found hotel cancellation policy!"));
										hotelCancellationPolicy = allHotelCancellationPolicies[ratesHotelRowHotelId];
									}
									else
									{
										//WriteDebug(String.Format("Add default cancellation policy!"));
										hotelCancellationPolicy = new TouricoCancellationPolicy(true);
									}

									List<TouricoCancellationPolicy.Policy> roomCancellationPolicies = hotelCancellationPolicy.GetCancellationPolicies(dateDay);
									foreach (TouricoCancellationPolicy.Policy roomCancellationPolicy in roomCancellationPolicies)
									{
										string cancellationPolicyId = CreateCancellationPolicy(roomCancellationPolicy.policyType, roomCancellationPolicy.policyAmount, roomCancellationPolicy.policyFee);
										availDay.CancellationPolicies.Add(cancellationPolicyId);
									}

									// Only add availability if price could be evaluated.
									if (!string.IsNullOrEmpty(availDay.Pricing_BasePrice))
									{
										newRoom.AvailabilityPeriods.Add(dateDay, availDay);
									}

								}

								// Protect shared collections
								lock (allHotels)
								{
									// Do not add a room type twice.
									if (!allRoomTypes.ContainsKey(newRoomType.Type_Id))
									{
										root.RoomTypes.Add(newRoomType.Type_Id, newRoomType);
										allRoomTypes.Add(newRoomType.Type_Id, newRoomType);
									}

                                    Hotel hotel;
                                    if (allHotels.TryGetValue(ratesHotelRowHotelId, out hotel))
                                    {
										if (newRoom.AvailabilityPeriods.Count > 0)
										{
											if (!isRoomGroup)
											{
                                                hotel.Rooms.Add(string.Format("{0}.{1}", newRoomType.Bookable_Id, newRoomType.Type_Id), newRoom);
											}
											else
											{
												roomGroup.Rooms.Add(string.Format("{0}.{1}", newRoomType.Bookable_Id, newRoomType.Type_Id), newRoom);
											}
										}
									}
									else
									{
										// Why are some hotels missing in CSV export (3506 for example)?
										// Why is there several availability rows for same roomId.HotelId?
										log.WriteLine(string.Format("Data error: Hotel removed from CSV structure: hotelId[{0}]", ratesHotelRowHotelId));
									}
								}
							}

                            lock (allHotels)
                            {
                                if (roomGroup != null)
                                {
                                    // It's a room group so add it to the room group collection
                                    Hotel hotel;
                                    if (allHotels.TryGetValue(ratesHotelRowHotelId, out hotel))
                                    {
                                        if (roomGroup.Rooms.Count > 0)
                                        {
                                            if (!hotel.RoomGroups.ContainsKey(roomGroup.RoomGroupId))
                                            {
                                                hotel.RoomGroups.Add(roomGroup.RoomGroupId, roomGroup);
                                            }
                                            else
                                            {
                                                log.WriteLine(string.Format("Data error: Duplicate room group detected: hotelId[{0}] RoomGroupId[{1}]", ratesHotelRowHotelId, roomGroup.RoomGroupId));
                                            }
                                        }
                                    }
                                    else
                                    {
                                        // Why are some hotels missing in CSV export (3506 for example)?
                                        // Why is there several availability rows for same roomId.HotelId?
                                        log.WriteLine(string.Format("Data error: Hotel removed from CSV structure: hotelId[{0}]", ratesHotelRowHotelId));
                                    }
                                }
                            }
                        }
						else
						{
							if (!allAvailabilityRoomTypeRows.ContainsKey(string.Format("{0}.{1}", ratesHotelRowHotelId, ratesRoomTypeRowRoomId)))
							{
								log.WriteLine(string.Format("Data error: Availability data missing: Hotel_Id[{0}] roomId[{1}]", ratesHotelRowHotelId, ratesRoomTypeRowRoomId));
							}

							if (!allRoomTypesStatic.ContainsKey(ratesRoomTypeRowHotelRoomTypeId))
							{
								log.WriteLine(string.Format("Data error: Data missing in file PDS_ThfHotelRoomTypes.csv: hotelRoomTypeId[{0}]", ratesRoomTypeRowHotelRoomTypeId));
							}
						}
					}
					// Protect shared collections
					lock (allHotels)
					{
                        Hotel hotel;
                        if (allHotels.TryGetValue(ratesHotelRowHotelId, out hotel))
                        {
							if (hotel.RoomGroups.Count > 0 || hotel.Rooms.Count > 0)
							{
								allPlaces[hotel.PlaceId].Hotels.Add(ratesHotelRowHotelId, hotel);
								Optimizer.OptimizeHotel(root, hotel);
							}
							else
							{
								log.WriteLine(string.Format("No availability on hotel: ratesHotelRowHotelId[{0}]", ratesHotelRowHotelId));
								allHotels.Remove(ratesHotelRowHotelId);
							}
						}
					}
					return true;
				});
        }

        private int GetSeasonID(DateTime dateDay, string seasonKey, DateTime startDate, string weekDays)
        {
            int? seasonId = null;

            // Check if any seasons are available for this hotelId.roomId.
            if (allSeasons.ContainsKey(seasonKey))
            {
                foreach (DataRow dr in allSeasons[seasonKey])
                {
                    // Find seasonId for discount.
                    if (dateDay >= DateTime.Parse(dr["StartDate"].ToString()) && dateDay <= DateTime.Parse(dr["EndDate"].ToString()))
                    {
                        return int.Parse(dr["seasonID"].ToString());
                    }
                }
            }

            // If no seasonId can be found, use absolute value of integer hash from startdate.
            if (seasonId == null)
            {
                seasonId = Math.Abs(startDate.GetHashCode() & weekDays.GetHashCode());
                if (seasonId == 0)
                    seasonId = 1;
            }

            return (int)seasonId;
        }

        private void HandleExtraBeds(AvailabilityPeriod availDay, ExtrabedInfo extrabedInfo, string currency, int numberOfExtraBeds, int day, string roomType, int childAge)
        {
            double tmpExtraBedPrice;

            if (extrabedInfo.RoomTypeExtraChildBeds.ContainsKey(roomType) && extrabedInfo.RoomTypeExtraChildBeds[roomType] >= numberOfExtraBeds)
            {
                string childExtrabedPrice = FixPrice(currency, extrabedInfo.ChildPrices[numberOfExtraBeds][day], 0);

                // we need to check if we can convert the value to double, otherwise the extrabed is not available that period
                if (double.TryParse(childExtrabedPrice, NumberStyles.Float, CultureInfo.InvariantCulture, out tmpExtraBedPrice))
                {
                    availDay.Pricing_ExtrabedChild = childExtrabedPrice;
                }
                else
                {
                    // if no child price is available there shall be no adult price set
                    return;
                }
            }

            if (extrabedInfo.RoomTypeExtraAdultBeds.ContainsKey(roomType) && extrabedInfo.RoomTypeExtraAdultBeds[roomType] >= numberOfExtraBeds
                && extrabedInfo.AdultPrices.ContainsKey(numberOfExtraBeds))
            {
                string adultExtraBedPrice = FixPrice(currency, extrabedInfo.AdultPrices[numberOfExtraBeds][day], 0);

                if (double.TryParse(adultExtraBedPrice, NumberStyles.Float, CultureInfo.InvariantCulture, out tmpExtraBedPrice))
                {
                    availDay.Pricing_ExtrabedAdult = adultExtraBedPrice;
                }
            }

            // If childage for hotel is set to less than 11 all children is calculated as adults.
            if (childAge < 11)
            {
                availDay.Pricing_ExtrabedChild = availDay.Pricing_ExtrabedAdult;
            }
        }

        /// <summary>
        /// Sets up an <c>ExtrabedInfo</c> object with info about all extrabed prices for the different room types in the <c>RoomTypeRow</c>
        /// </summary>
        /// <param name="rrtr">the <c>RoomTypeRow</c> to set up the extra bed info for</param>
        /// <param name="xmlRoomTypeMapping">the xml file with the number of ordinary beds for each room type</param>
        /// <returns>an <c>ExtrabedInfo</c> object with all info needed for creating extrabed prices for the rooms that will be created for the provided <c>RoomTypeRow</c>. Returns null if no extrabed info can be created.</returns>
        private ExtrabedInfo CreateExtraBedInfo(Rates.RoomTypeRow rrtr, XmlDocument xmlRoomTypeMapping, string hotelId)
        {
            ExtrabedInfo extrabedInfo = null;

            Rates.ExtraChildRow[] extraChildPriceRows = rrtr.GetExtraChildRows();
            Rates.ExtraAdultRow[] extraAdultPriceRows = rrtr.GetExtraAdultRows();

            int maxExtraChilds = rrtr.maxChilds;
            int totalNumberOfGuests;

            switch (rrtr.contructType)
            {
                case "Per Room Type ":
                    extrabedInfo = new ExtrabedInfo();
                    totalNumberOfGuests = rrtr.maxAdults;

                    if (rrtr.maxChilds > 0 && totalNumberOfGuests > 1)  // no reason to calculate prices etc. if no extra beds are available
                    {
                        foreach (Rates.RoomRow room in rrtr.GetRoomRows())
                        {
                            XmlNode node = xmlRoomTypeMapping.SelectSingleNode(String.Format("/RoomTypes/RoomType[@id=\"{0}\"]", room.type));

                            // max number of ordinary beds for this roomtype (= absolute number of max childs since there can be no more children than adult beds)
                            int tmpOrdinaryBeds = int.Parse(node.Attributes["beds"].Value);
                            int tmpAllowExtraBeds = int.Parse(node.Attributes["allowextrabeds"].Value);

                            int maxChildBedsForRoomType = 0;
                            if (tmpAllowExtraBeds == 1)
                            {
                                maxChildBedsForRoomType = GetMaxChildExtraBeds(ValidChildPriceRowCount(extraChildPriceRows), maxExtraChilds, totalNumberOfGuests, tmpOrdinaryBeds);
                            }

                            extrabedInfo.RoomTypeExtraChildBeds.Add(room.type, maxChildBedsForRoomType);
                        }

                        // setup price collections for child extrabeds
                        foreach (Rates.ExtraChildRow childPriceRow in extraChildPriceRows)
                        {
                            int childPriceRowNumber = Int32.Parse(childPriceRow.num);
                            extrabedInfo.ChildPrices.Add(childPriceRowNumber, GetFixedChildPriceRow(childPriceRowNumber, extraChildPriceRows));
                        }
                    }

                    // extrabedInfo.MaxChilds = maxExtraChilds;

                    // no adults are allowed in extra beds for this "contructType"
                    //extrabedInfo.MaxAdults = 0;

                    if (maxExtraChilds != extraChildPriceRows.Length)
                    {
                        log.WriteLine(string.Format("Data error: Max Child Extrabeds specified differs the number of extra child rows: hotelId[{0}] roomId[{1}]", hotelId, rrtr.roomId));
                    }
                    break;

                case "SGL,DBL + Guests":
                    if (rrtr.maxAdults > 2 && (extraAdultPriceRows.Length > 0 || extraChildPriceRows.Length > 0))
                    {   // only create extrabed info if there are available extra beds
                        extrabedInfo = new ExtrabedInfo();
                        totalNumberOfGuests = rrtr.maxAdults;

                        foreach (Rates.RoomRow room in rrtr.GetRoomRows())
                        {
                            XmlNode node = xmlRoomTypeMapping.SelectSingleNode(String.Format("/RoomTypes/RoomType[@id=\"{0}\"]", room.type));
                            int tmpOrdinaryBeds = int.Parse(node.Attributes["beds"].Value);

                            // calculate the number of extra child beds
                            int maxChildBedsForRoomType = 0;
                            if (room.type != "SingleRoom")
                            {
                                maxChildBedsForRoomType = GetMaxChildExtraBeds(ValidChildPriceRowCount(extraChildPriceRows), maxExtraChilds, totalNumberOfGuests, tmpOrdinaryBeds);
                            }
                            extrabedInfo.RoomTypeExtraChildBeds.Add(room.type, maxChildBedsForRoomType);

                            // calculate the number of extra adult beds
                            int maxAdultExtraBedsForRoomType = 0;
                            if (room.type != "SingleRoom")
                            {
                                maxAdultExtraBedsForRoomType = GetMaxAdultExtraBeds(ValidAdultPriceRowCount(extraAdultPriceRows), rrtr.maxAdults, tmpOrdinaryBeds, maxChildBedsForRoomType);
                            }

                            extrabedInfo.RoomTypeExtraAdultBeds.Add(room.type, maxAdultExtraBedsForRoomType);
                        }

                        // setup price collections for child extrabeds
                        foreach (Rates.ExtraChildRow childPriceRow in extraChildPriceRows)
                        {
                            int childPriceRowNumber = Int32.Parse(childPriceRow.num);
                            extrabedInfo.ChildPrices.Add(childPriceRowNumber, GetFixedChildPriceRow(childPriceRowNumber, extraChildPriceRows));
                        }

                        // setup price collections for adult extrabeds
                        foreach (Rates.ExtraAdultRow adultPriceRow in extraAdultPriceRows)
                        {
                            int adultPriceRowNumber = Int32.Parse(adultPriceRow.num);
                            extrabedInfo.AdultPrices.Add(adultPriceRowNumber, GetFixedAdultPriceRow(adultPriceRowNumber, extraAdultPriceRows));
                        }
                    }

                    break;
            }

            return extrabedInfo;
        }

        private int ValidAdultPriceRowCount(Rates.ExtraAdultRow[] PriceRows)
        {

            int validatedPriceRows = 0;

            foreach (Rates.ExtraAdultRow priceRow in PriceRows)
            {
                string sMarkup = priceRow.markUp.ToString();
                if (!sMarkup.Contains("-"))
                {
                    validatedPriceRows++;
                }
            }

            return validatedPriceRows;
        }

        private int ValidChildPriceRowCount(Rates.ExtraChildRow[] PriceRows)
        {

            int validatedPriceRows = 0;

            foreach (Rates.ExtraChildRow priceRow in PriceRows)
            {
                string sMarkup = priceRow.markUp.ToString();
                if (!sMarkup.Contains("-"))
                {
                    validatedPriceRows++;
                }
            }

            return validatedPriceRows;
        }
        /// <summary>
        /// Calculates the number of adult extra beds a room can have
        /// </summary>
        /// <param name="extraAdultPriceRows">the number of adult extrabed pricerows available</param>
        /// <param name="maxAdults">the specified maximum number of adults that are allowed in the room</param>
        /// <param name="nrOfOrdinaryBeds">the number of ordinary beds for the room</param>
        /// <param name="maxChildBedsForRoomType">the maximum number of child extra beds for the room (since we can not handle adult extra beds that no child can use)</param>
        /// <returns>the number of adult extra beds for the room</returns>
        private int GetMaxAdultExtraBeds(int extraAdultPriceRows, int maxAdults, int nrOfOrdinaryBeds, int maxChildBedsForRoomType)
        {
            int maxAdultExtraBedsForRoomType = maxAdults - nrOfOrdinaryBeds;

            if (nrOfOrdinaryBeds == 1)
            {
                // no extra adult beds for single rooms
                maxAdultExtraBedsForRoomType = 0;
            }

            if (maxAdultExtraBedsForRoomType > extraAdultPriceRows)
            {
                // there can't be more extra adults than there are price rows for
                maxAdultExtraBedsForRoomType = extraAdultPriceRows;
            }

            if (maxAdultExtraBedsForRoomType > maxChildBedsForRoomType)
            {
                // there can not be more adult extra beds than child extra beds
                maxAdultExtraBedsForRoomType = maxChildBedsForRoomType;
            }

            if (maxAdultExtraBedsForRoomType < 0)
            {
                maxAdultExtraBedsForRoomType = 0;
            }
            return maxAdultExtraBedsForRoomType;
        }


        /// <summary>
        /// Calculates the number of child extra beds a room can have
        /// </summary>
        /// <param name="extraChildPriceRows">the number of child extrabed pricerows available</param>
        /// <param name="maxExtraChilds">the specified maximum number of childs that are allowed in the room</param>
        /// <param name="totalNumberOfGuests">the maximum number of guests allowed in the room</param>
        /// <param name="nrOfOrdinaryBeds">the number of ordinary beds for the room</param>
        /// <returns>the number of child extra beds for the room</returns>
        private int GetMaxChildExtraBeds(int extraChildPriceRows, int maxExtraChilds, int totalNumberOfGuests, int nrOfOrdinaryBeds)
        {
            int maxChildBedsForRoomType = maxExtraChilds;

            if (totalNumberOfGuests - nrOfOrdinaryBeds < maxExtraChilds)
            {
                maxChildBedsForRoomType = totalNumberOfGuests - nrOfOrdinaryBeds;
            }

            if (extraChildPriceRows < maxChildBedsForRoomType)
            {
                maxChildBedsForRoomType = extraChildPriceRows;
            }

            if (maxChildBedsForRoomType < 0)
            {
                maxChildBedsForRoomType = 0;
            }

            return maxChildBedsForRoomType;
        }

        /// <summary>
        /// Creates a price array for a certain number of adult extrabeds.
        /// </summary>
        /// <param name="num">the number of adult extra beds to calculate price for</param>
        /// <param name="extraChildPriceRows">the <c>ExtraAdultRow</c> array to use when creating the prices</param>
        /// <returns>a string-array with extrabed prices</returns>				
        private string[] GetFixedAdultPriceRow(int num, Rates.ExtraAdultRow[] extraAdultPriceRows)
        {
            if (num == 1)
            {
                foreach (Rates.ExtraAdultRow row in extraAdultPriceRows)
                {
                    if (row.num == "1")
                    {
                        // only 1 extra bed so use the price set in the xml
                        return row.markUp.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                    }
                }
            }

            Dictionary<int, string[]> prices = new Dictionary<int, string[]>();
            List<string> newPrice = new List<string>();

            // get all prices that shall be compared
            //for (int i = 0; i < num; i++)
            foreach (Rates.ExtraAdultRow extraAdultRow in extraAdultPriceRows)
            {
                if (Int32.Parse(extraAdultRow.num) <= num)
                {
                    prices.Add(Int32.Parse(extraAdultRow.num), extraAdultRow.markUp.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries));
                }
            }

            double tmpPrice;
            double tmpComparePrice;
            string stmpPrice;

            // loop through each day-price and pick the most expensive!
            for (int i = 0; i < prices[1].Length; i++)
            {
                newPrice.Add(FixPrice("EUR", prices[1][i], 0));
                foreach (int nrOfExtraAdults in prices.Keys)
                {
                    if (double.TryParse(newPrice[i], NumberStyles.Float, CultureInfo.InvariantCulture, out tmpPrice))
                    {
                        stmpPrice = FixPrice("EUR", prices[nrOfExtraAdults][i], 0);
                        if (!double.TryParse(stmpPrice, NumberStyles.Float, CultureInfo.InvariantCulture, out tmpComparePrice))
                        {
                            newPrice[i] = stmpPrice;
                        }
                        else if (tmpComparePrice / nrOfExtraAdults > tmpPrice)
                        {
                            tmpComparePrice = tmpComparePrice / nrOfExtraAdults;
                            newPrice[i] = tmpComparePrice.ToString(CultureInfo.InvariantCulture);
                            tmpComparePrice = 0;
                        }
                    }
                }
            }

            return newPrice.ToArray();
        }

        /// <summary>
        /// Creates a price array for a certain number of child extrabeds.
        /// </summary>
        /// <param name="num">the number of child extra beds to calculate price for</param>
        /// <param name="extraChildPriceRows">the <c>ExtraChildRow</c> array to use when creating the prices</param>
        /// <returns>a string-array with extrabed prices</returns>
        private string[] GetFixedChildPriceRow(int num, Rates.ExtraChildRow[] extraChildPriceRows)
        {
            if (num == 1)
            {
                foreach (Rates.ExtraChildRow row in extraChildPriceRows)
                {
                    if (row.num == "1")
                    {
                        // only 1 extra bed so use the price set in the xml
                        return row.markUp.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                    }
                }
            }

            Dictionary<int, string[]> prices = new Dictionary<int, string[]>();
            List<string> newPrice = new List<string>();

            // get all prices that shall be compared
            //for (int i = 0; i < num; i++)
            foreach (Rates.ExtraChildRow extraChildRow in extraChildPriceRows)
            {
                if (Int32.Parse(extraChildRow.num) <= num)
                {
                    prices.Add(Int32.Parse(extraChildRow.num), extraChildRow.markUp.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries));
                }
            }

            double tmpPrice;
            double tmpComparePrice;
            string stmpPrice;

            // loop through each day-price and pick the most expensive!
            for (int i = 0; i < prices[1].Length; i++)
            {
                newPrice.Add(FixPrice("EUR", prices[1][i], 0));
                foreach (int nrOfExtraChilds in prices.Keys)
                {
                    if (double.TryParse(newPrice[i], NumberStyles.Float, CultureInfo.InvariantCulture, out tmpPrice))
                    {
                        stmpPrice = FixPrice("EUR", prices[nrOfExtraChilds][i], 0);
                        if (!double.TryParse(stmpPrice, NumberStyles.Float, CultureInfo.InvariantCulture, out tmpComparePrice))
                        {
                            newPrice[i] = stmpPrice;
                        }
                        else if (tmpComparePrice / nrOfExtraChilds > tmpPrice)
                        {
                            tmpComparePrice = tmpComparePrice / nrOfExtraChilds;
                            newPrice[i] = tmpComparePrice.ToString(CultureInfo.InvariantCulture);
                            tmpComparePrice = 0;
                        }
                    }
                }
            }

            return newPrice.ToArray();
        }

        /// <summary>
        /// Creates a list with discountperiods for a roomtype
        /// </summary>
        /// <param name="rrtr">the <c>RoomTypeRow</c> to create the discountperiods for</param>
        /// <returns>a list containing <c>discountperiod</c>s for the provided <c>RoomTypeRow</c></returns>
        private List<DiscountPeriod> CreateDiscountPeriods(Rates.RoomTypeRow rrtr)
        {
            List<DiscountPeriod> discounts = new List<DiscountPeriod>();

            foreach (Rates.DiscountRow rdr in rrtr.GetDiscountRows())
            {
                if (!rdr.IsvalueNull())
                {
                    try
                    {
                        discounts.Add(new DiscountPeriod(rdr));
                    }
                    catch (Exception ex)
                    {
                        log.WriteLine(String.Format("Discount error on hotelId: {0} roomId {1} : {2}", rrtr.Hotel_Id, rrtr.roomId, ex.ToString()));
                    }
                    //WriteDebug(String.Format("Discount found: {3} - {4} -- type {0}, pay {1}, stay {2} -- room_id {5}", rdr.type, rdr.pay, rdr.stay, rdr.startDate, rdr.endDate, ratesRoomTypeRowRoomId));
                }
            }


            return discounts;
        }

        /// <summary>
        /// Creates a cancellation policy and adds it to the root structure.
        /// </summary>
        /// <param name="type">Sunhotels penelty basis code.</param>
        /// <param name="amount">Cancellation deadline, number of days befor arrival.</param>
        /// <param name="fee">Penelty fee, either number of percent or number of nights.</param>
        /// <returns></returns>
        private string CreateCancellationPolicy(string type, double? amount, double fee)
        {

            string policyId = string.Format("{0}.{1}.{2}", type, amount, fee);

            if (!allCancellationPolicies.ContainsKey(policyId))
            {

                CancellationPolicy newPolicy = new CancellationPolicy();

                newPolicy.Deadline_Basis = "arrival";
                newPolicy.Deadline_Unit = "hours";
                newPolicy.Deadline_Value = amount == null ? null : (int?)amount * 24;
                newPolicy.Id = policyId;
                newPolicy.Penalty_Basis = type;
                newPolicy.Penalty_Value = (int)fee;

                root.CancellationsPolicies.Add(policyId, newPolicy);
                allCancellationPolicies.Add(policyId, newPolicy);

            }

            return policyId;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="boardPeriod"></param>
        /// <param name="rrtr"></param>
        /// <param name="sCurrency"></param>
        private void CreateBoardStructureForPeriod(BoardStructure[] boardPeriod, Rates.RoomTypeRow rrtr, string sCurrency, int beds, string ratesHotelRowHotelId)
        {

            Rates.BoardRow[] rbrs = rrtr.GetBoardRows();

            // Skip board included board if boardbase discount is present and there are more beds than free boards.
            bool skipBoard = false;
            foreach (Rates.DiscountRow rdr in rrtr.GetDiscountRows())
            {
                if (rdr.type == "BoardBase")
                {
                    if (beds > int.Parse(rdr.maxfree))
                    {
                        skipBoard = true;
                    }
                }
            }

            if (rbrs.Length > 0 && skipBoard == false)
            {
                foreach (Rates.BoardRow rbr in rbrs)
                {
                    string[] availBoardPriceAdult = SplitPeriod(rbr.price);
                    string[] availBoardPriceChild = SplitPeriod(rbr.price);

                    if (availBoardPriceAdult.Length != configuration.NrDaysInCache)
                    {
                        throw new ApplicationException(string.Format("Data error: Board period differs from period specified by configuration file. hotelRoomTypeId:{0}", rrtr.hotelRoomTypeId));
                    }

                    for (int iDay = 0; iDay < configuration.NrDaysInCache; iDay++)
                    {
                        // Check if board structure exists for period
                        if (boardPeriod[iDay] == null)
                        {
                            // Create default board
                            BoardStructure boardStructure = new BoardStructure();
                            boardPeriod[iDay] = boardStructure;
                        }

                        // Check if board is mapped to an Sunhotels board, if not the board is not bookalble
                        if (allBoards.ContainsKey(rbr.bbId))
                        {
                            // Check for "-" in board prices. If existant the board might not be available for all stay lengths.
                            if (!availBoardPriceAdult[iDay].Contains("-") && !availBoardPriceChild[iDay].Contains("-"))
                            {
                                // Note, if the price is set to the "-" symbol, this indicates that no board base is offered for that night in the hotel.
                                string sBoardPriceAdult = FixPrice(sCurrency, availBoardPriceAdult[iDay], 0);
                                string sBoardPriceChild = FixPrice(sCurrency, availBoardPriceChild[iDay], 0);
                                double boardPriceAdult;
                                double boardPriceChild;

                                if (double.TryParse(sBoardPriceAdult, NumberStyles.Float, CultureInfo.InvariantCulture, out boardPriceAdult) && double.TryParse(sBoardPriceChild, NumberStyles.Float, CultureInfo.InvariantCulture, out boardPriceChild))
                                {
                                    try
                                    {
                                        // Check if default is set to false but price is 0. Then the board is default.
                                        string includedBoard = rbr.isDefault;
                                        if (includedBoard == "false" && boardPriceAdult == 0 && boardPriceChild == 0)
                                        {
                                            includedBoard = "true";
                                        }

                                        // Create bookable bord.
                                        boardPeriod[iDay].Create(allBoards[rbr.bbId], rbr.bbId, boardPriceAdult, boardPriceChild, includedBoard);
                                    }
                                    catch (ApplicationException ape)
                                    {
                                        log.WriteLine(string.Format("Data error: {0}", ape.Message));
                                    }
                                }
                            }
                            //else
                            //{
                            //    string boardErrorString = string.Format("{0}.{1}.{2}", ratesHotelRowHotelId, rrtr.roomId, rbr.bbId);
                            //    if (!loggedBoardErrors.ContainsKey(boardErrorString))
                            //    {
                            //        log.WriteLine("Board price not available for all stay lengths for hotel.room.board: {0}", boardErrorString);
                            //        loggedBoardErrors.Add(boardErrorString, null);
                            //    }
                            //}
                        }
                        else
                        {
                            string boardErrorString = string.Format("{0}.{1}.{2}", ratesHotelRowHotelId, rrtr.roomId, rbr.bbId);
                            if (!loggedBoardErrors.ContainsKey(boardErrorString))
                            {
                                log.WriteLine("Non mappable hotel.room.board: {0}", boardErrorString);
                                loggedBoardErrors.Add(boardErrorString, null);
                            }
                        }
                    }
                }
            }
            else
            {
                for (int iDay = 0; iDay < configuration.NrDaysInCache; iDay++)
                {
                    // Create default board
                    BoardStructure boardStructure = new BoardStructure();
                    boardPeriod[iDay] = boardStructure;
                }
            }
        }

        /// <summary>
        /// Sets the feature param for specified Hotel object
        /// </summary>
        /// <param name="hotelId"></param>
        private void AddFeaturesAndImagesToHotel(string hotelId)
        {
            if (allHotels.ContainsKey(hotelId))
            {
				if(allHotelFeatures.ContainsKey(hotelId))
                    allHotels[hotelId].Features = allHotelFeatures[hotelId];
                
                if (allImages.ContainsKey(hotelId))
                {
                    foreach (DataRow imageRow in allImages[hotelId])
                    {
                        if (imageRow["IsThumbnail"].ToString() != "True" && imageRow["BigThumbnail"].ToString() != "True")
                        {
                            Hotel.Image image = new Hotel.Image()
                            {
                                ImageVariants = new List<Hotel.ImageVariant>() 
                                { 
                                    new Hotel.ImageVariant() 
                                    { 
                                        URL = imageRow["ImageURL"].ToString(),
										Width = 200,
										Height = 200
                                    }
                                }
                            };
                            allHotels[hotelId].Images.Add(image);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sCurrency"></param>
        /// <param name="sPrice"></param>
        /// <returns></returns>
        private string FixPrice(string sCurrency, string sPrice, int position)
        {
            string tmpPrice = sPrice;

            // Check and handle special price structures.
            if (tmpPrice.Contains("/") && position >= 0)
            {
                string[] sPrices = sPrice.Split('/');
                if (position <= sPrices.Length - 1)
                {
                    tmpPrice = sPrices[position];
                }
                else
                {
                    // Try to pick the highest price instead
                    position = -1;
                }
            }

            if (tmpPrice.Contains("/") && position < 0)
            {
                // Get the highest price to be sure...
                string[] sPrices = sPrice.Split('/');

                int maxPriceIndex = 0;
                double maxPrice = 0;
                double tempPrice;

                for (int i = 0; i < sPrices.Length; i++)
                {
                    if (double.TryParse(sPrices[i], NumberStyles.Float, CultureInfo.InvariantCulture, out tempPrice))
                    {
                        if (tempPrice > maxPrice)
                        {
                            maxPrice = tempPrice;
                            maxPriceIndex = i;
                        }
                    }
                    else
                    {
                        // if no valid price set this index as the max price 
                        maxPriceIndex = i;
                        break;
                    }
                }

                // TODO: Kolla med Dan hur '-' ska hanteras
                tmpPrice = sPrices[maxPriceIndex];

                // If there is a MinMax rule available for a particular day, there will be multiple prices for
                // that day, separated by a delimiter. In this case use lowest price (located at last position).
                //tmpPrice = sPrices[sPrices.Length - 1];
            }

            if (tmpPrice.Contains("-"))
            {
                // No price structure exists for this day.
                tmpPrice = String.Empty;
            }

            // Convert prices to "EUR".
            if (tmpPrice != String.Empty)
            {
                double dPrice = -1;

                // Check if price is parsable as double
                if (double.TryParse(tmpPrice, NumberStyles.Float, CultureInfo.InvariantCulture, out dPrice))
                {
                    if (sCurrency != "EUR")
                    {
                        dPrice = ToEuro(dPrice, sCurrency);
                    }

                    tmpPrice = dPrice.ToString(CultureInfo.InvariantCulture);
                }
                else
                {
                    // No valid or parsable price structure found.
                    // Todo is this a log or Exception case?
                    // log.WrriteLine("No valid or parsable price structure found.")
                    // throw new ApplicationException("No valid or parsable price structure found.");
                    tmpPrice = String.Empty;
                }
            }

            return tmpPrice;
        }

        /// <summary>
        /// Get previously created place or create new.
        /// </summary>
        /// <param name="hotelRow">The data row containing hotel information.</param>
        /// <param name="hotelPlace">The place under witch the hotel will be created.</param>
        /// <returns>The requested hotel.</returns>
        private Hotel CreateHotel(DataRow hotelRow, Place hotelPlace)
        {
            Hotel hNew = new Hotel();
            hNew.PlaceId = hotelPlace.Id;
            hNew.Id = hotelRow["HotelId"].ToString();
            hNew.Name = hotelRow["HotelName"].ToString();
            hNew.Adress_City = hotelRow["City"].ToString();
            hNew.Adress_Country = hotelRow["CountryCode"].ToString();
            hNew.Adress_State = hotelRow["State"].ToString();

            if (hNew.Id == "1032069")
            {
                hNew.Adress_Street1 = "";  // temp fix since the address contains contral characters that f..ks up the xml
            }
            else
            {
                hNew.Adress_Street1 = hotelRow["Address"].ToString();
            }

            hNew.Adress_Zipcode = hotelRow["Zip"].ToString();
            hNew.Classification = hotelRow["Stars"].ToString();
            hNew.Description = hotelRow["ShortDescription"].ToString();

            double dblDistance = 0;

            if (double.TryParse(hotelRow["RefPointDist"].ToString(), NumberStyles.Float, CultureInfo.InvariantCulture, out dblDistance))
            {
                if (hotelRow["DistUnit"].ToString() == "Mile")
                {
                    // Convert miles to km if distance to airport is specified in miles.
                    dblDistance = dblDistance * 1.609;
                }



                Hotel.Distance newDistance = new Hotel.Distance("airport", Decimal.Round((decimal)dblDistance, 1));
                hNew.Distances.Add(newDistance);
            }

            hNew.Fax = hotelRow["Fax"].ToString();
            hNew.Phone = hotelRow["Phone"].ToString();
            hNew.Position_Latitude = hotelRow["Latitude"].ToString();
            hNew.Position_Longitude = hotelRow["Longitude"].ToString();

            //hNew.Email = NO DATA IN CSV FILES
            //hNew.Adress_Street2 = NO DATA IN CSV FILES

            return hNew;
        }


        /// <summary>
        /// Get previously created place or create new.
        /// </summary>
        /// <param name="sCountryCode">The country code.</param>
        /// <param name="sStateCode">The state/zip code.</param>
        /// <param name="sState">The state name.</param>
        /// <param name="sCity">The city name.</param>
        /// <returns>The requested palce.</returns>
        private Place GetPlace(string sCountryCode, string sStateCode, string sState, string sCity)
        {

            // Check or create country level place
            string sCountryID = sCountryCode.ToLower();
            if (allPlaces.ContainsKey(sCountryID) != true)
            {
                Place newPlace = new Place();
                newPlace.Id = sCountryID;
                newPlace.Description = sCountryCode;
                allPlaces.Add(sCountryID, newPlace);

                root.Places.Add(newPlace);
            }

            // Check or create state level place if state is specified
            string sStateID = null;
            if (string.IsNullOrEmpty(sStateCode) != true)
            {
                sStateID = string.Format("{0}.{1}", sCountryCode.ToLower(), sStateCode.ToLower());
                if (allPlaces.ContainsKey(sStateID) != true)
                {
                    Place newPlace = new Place();
                    newPlace.Id = sStateID;
                    newPlace.Description = sState;
                    allPlaces.Add(sStateID, newPlace);

                    allPlaces[sCountryID].Places.Add(newPlace);
                }
            }

            // Check or create city level place skipping state level if not present
            string sCityID = null;
            if (string.IsNullOrEmpty(sStateCode) != true)
            {
                sCityID = string.Format("{0}.{1}.{2}", sCountryCode.ToLower(), sStateCode.ToLower(), sCity.ToLower());
                if (allPlaces.ContainsKey(sCityID) != true)
                {
                    Place newPlace = new Place();
                    newPlace.Id = sCityID;
                    newPlace.Description = sCity;
                    allPlaces.Add(sCityID, newPlace);
                    allPlaces[sStateID].Places.Add(newPlace);
                }
            }
            else
            {
                sCityID = string.Format("{0}.{1}", sCountryCode.ToLower(), sCity.ToLower());
                if (allPlaces.ContainsKey(sCityID) != true)
                {
                    Place newPlace = new Place();
                    newPlace.Id = sCityID;
                    newPlace.Description = sCity;
                    allPlaces.Add(sCityID, newPlace);
                    allPlaces[sCountryID].Places.Add(newPlace);
                }
            }

            return allPlaces[sCityID];

        }

        /// <summary>
        /// Convert local currency to EUR using hash table imported from Sunhotels currency database.
        /// </summary>
        /// <param name="dRate">Rate in local currency.</param>
        /// <param name="sCurrency">Local currency in international 3 letter style. USD, SEK, EUR etc.</param>
        /// <returns></returns>
        double ToEuro(double dRate, string sCurrency)
        {
            try
            {
                dRate = Math.Round(dRate / allCurrencies[sCurrency], 2);
            }
            catch
            {
                log.WriteLine(string.Format("Data error: Unsupported currency was used: {0}", sCurrency));
                dRate = 0;
            }

            return dRate;
        }

        /// <summary>
        /// Convert local currency to EUR using hash table imported from Sunhotels currency database.
        /// </summary>
        /// <param name="dRate">Rate in local currency.</param>
        /// <param name="sCurrency">Local currency in international 3 letter style. USD, SEK, EUR etc.</param>
        /// <returns></returns>
        decimal ToEuro(decimal dRate, string sCurrency)
        {
            try
            {
                // Tourico has faulty currencies for some hotels in russia.
                if (sCurrency.ToUpper() == "RUR")
                {
                    sCurrency = "RUB";
                }

                // RMB is same as CNY.
                if (sCurrency.ToUpper() == "RMB")
                {
                    sCurrency = "CNY";
                }

                dRate = Math.Round(dRate / Convert.ToDecimal(allCurrencies[sCurrency]), 2);
            }
            catch
            {
                log.WriteLine(string.Format("Data error: Unsupported currency was used: {0}", sCurrency));
                dRate = 0;
            }

            return dRate;
        }
        
        /// <summary>
        /// Splits availibility/price periods
        /// </summary>
        /// <param name="period">";" separated period string</param>
        /// <returns></returns>
        private string[] SplitPeriod(string period)
        {
            // An availibility/price appears for each day, separated by a ";" from the other days of the period.
            string[] periods = (period.TrimEnd(';')).Split(';');
            return periods;
        }

        void WriteDebug(string debugMessage)
        {
#if (DEBUG)
            Console.WriteLine("debug: " + debugMessage);
#endif
        }

        private bool GetBoolValue(string value)
        {
            return !(value == "0" || value.ToLower() == "false" || value.ToLower() == "no");
        }

    }
}
