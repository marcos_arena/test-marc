﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.IO;
using System.Linq;

namespace Empir.Data.CSV.FIX
{
	public class CSVData
	{
		private DataTable dataTable;
		private char separator_char;
		private String tableName;
		private String quoteString;
		private int? maxRowsInFile;
		private int rowCounter = 0;

		public CSVData()
		{
			tableName = "CSVData";
			SEPARATOR_CHAR = ';';
			quoteString = null;
		}

		public DataTable Data
		{
			get
			{
				return dataTable;
			}
			set
			{
				dataTable = value;
			}

		}

		public char SEPARATOR_CHAR
		{
			get
			{
				return separator_char;
			}
			set
			{
				separator_char = value;
			}

		}

		public string TableName
		{
			get
			{
				return tableName;
			}
			set
			{
				tableName = value;
			}

		}

		public String QuoteString
		{
			get { return quoteString; }
			set { quoteString = value; }
		}

		public int? MaxRowsInFile
		{
			get { return maxRowsInFile; }
			set { maxRowsInFile = value; }
		}

		public string StripQuotes(string sData)
		{
			if (quoteString != null)
			{
				if (sData.StartsWith(quoteString) && sData.EndsWith(quoteString))
				{
                    var length = sData.Length - (2 * quoteString.Length);
                    if (length >= 1)
                    {
                        sData = sData.Substring(quoteString.Length, length);
                    }
                    else
                    {
                        sData = string.Empty;
                    }
				}
			}

			return sData;
		}

		private string AddQuotes(string sData)
		{
			if (quoteString != null)
			{
				sData = string.Format("{0}{1}{0}", quoteString, sData);
			}

			return sData;
		}

		/// <summary>
		/// Reads the file that is given as a parameter to a <c>DataTable</c>.
		/// The table name can be set as a property on this object. The separator char can also be set as a property on this object.
		/// 
		/// Encoding of the file that is read must be windows-1252.
		/// </summary>
		public DataTable Read(string fileName)
		{
			return Read(fileName, System.Text.Encoding.GetEncoding(1252));
		}

		/// <summary>
		/// Reads the file that is given as a parameter to a <c>DataTable</c>.
		/// The table name can be set as a property on this object. The separator char can also be set as a property on this object.
		/// </summary>
		public DataTable Read(string fileName, Encoding encoding)
		{
			FileInfo fi = new FileInfo(fileName);
			tableName = fi.Name;

			dataTable = new DataTable(tableName);
			System.IO.StreamReader streamIn = new System.IO.StreamReader(fileName, encoding);

			string line = streamIn.ReadLine();
			string[] headers = line.Split(separator_char);

			// Skapa kolumnerna.
			foreach (string header in headers)
			{
				// Remove swedish char from column names.
				string headerText = header.Replace('Å', 'A').Replace('å', 'a').Replace('Ä', 'A').Replace('ä', 'a').Replace('Ö', 'O').Replace('ö', 'o');

				headerText = StripQuotes(headerText);
				headerText = headerText.Trim();
				try
				{
					dataTable.Columns.Add(headerText, System.Type.GetType("System.String"));
				}
				catch
				{
					throw new Exception(String.Format("A column with the name \"{0}\" was found multiple times in the file {1} .", header, tableName));
				}
			} 

			//Read the rest of the rows.
			while (!streamIn.EndOfStream)
			{
				line = streamIn.ReadLine();

				if (line.Replace(separator_char.ToString(), "").Trim().Length != 0)
				{

					string[] values = SplitLine(line).ToArray();
					DataRow row = dataTable.NewRow();

					for (int i = 0; i < values.Length; i++)
					{
						// Remove values in a position higher then the headers. Should this throw a exception? A setting for this maybe?
						if (i < dataTable.Columns.Count)
						{
							string value = values[i].Trim();
							// Skip empty lines.
							if (value.Length > 0)
							{
								value = StripQuotes(value);
								row[i] = value;
							}
						}
					}

					dataTable.Rows.Add(row);
				}
			}

			streamIn.Close();

			Data = dataTable;

			return dataTable;
		}

		public IEnumerable<string> SplitLine(String line)
		{
			int pos = 0,
				lastPos = 0;
			var useQuotes = !String.IsNullOrEmpty(quoteString);
			while (pos != -1)
			{
				pos = line.IndexOf(separator_char, pos);
				if (pos == -1)
				{
					yield return StripQuotes(line.Substring(lastPos));
					break;
				}
				if (useQuotes && line.Substring(lastPos, quoteString.Length).Equals(quoteString))
				{
					if (line.IndexOf(quoteString, lastPos + quoteString.Length) > pos)
					{
						pos++;
						continue;
					}
				}
				yield return StripQuotes(line.Substring(lastPos, pos - lastPos));
				lastPos = ++pos;
			}
		}

		/// <summary>
		/// Writes the <c>DataTable</c> on this object as a CSV file. The name of the CSV file is set as a parameter to this method.
		/// The separator char can be set as a property on this object.
		/// 
		/// The encoding will be windows-1252.
		/// </summary>
		public void Write(string fileName)
		{
			Write(fileName, System.Text.Encoding.GetEncoding(1252));
		}

		/// <summary>
		/// Writes the <c>DataTable</c> on this object as a CSV file. The name of the CSV file is set as a parameter to this method.
		/// The separator char can be set as a property on this object.
		/// The encoding on the out file will be whatever is set as the second parameter.
		/// /// </summary>
		public void Write(string fileName, Encoding encoding)
		{
			FileInfo fInfo = new FileInfo(fileName);
			if (!fInfo.Directory.Exists)
			{
				fInfo.Directory.Create();
			}

			int nrFiles;
			if (maxRowsInFile != null)
			{
				nrFiles = (dataTable.Rows.Count / (int)maxRowsInFile) + 1;
			}
			else
			{
				nrFiles = 1;
				maxRowsInFile = dataTable.Rows.Count;
			}

			rowCounter = 0;

			for (int fileCounter = 0; fileCounter < nrFiles; fileCounter++)
			{
				if (nrFiles != 1)
				{
					string firstFilenamePart = fInfo.Name.Replace(fInfo.Extension, "");
					fileName = String.Format("{0}\\{1}.{2}{3}", fInfo.Directory.FullName, firstFilenamePart, fileCounter, fInfo.Extension);
				}

				FileStream fs = new FileStream(fileName, FileMode.Create);
				Write(fs, encoding);
			}
		}


		/// <summary>
		/// Writes the <c>DataTable</c> on this object as a CSV stream.
		/// The separator char can be set as a property on this object.
		/// The encoding on the out file will be whatever is set as the second parameter.
		/// /// </summary>
		public void Write(Stream streamWrite, Encoding encoding)
		{
			if (maxRowsInFile == null)
			{
				maxRowsInFile = dataTable.Rows.Count;
			}

			System.IO.StreamWriter streamOut = new System.IO.StreamWriter(streamWrite, encoding);

			StringBuilder stringOut = new StringBuilder();

			// Add column names on the first row.
			foreach (DataColumn column in dataTable.Columns)
			{
				if (stringOut.Length != 0)
				{
					stringOut.Append(separator_char);
				}

				stringOut.Append(AddQuotes(column.ColumnName));
			}

			streamOut.WriteLine(stringOut);

			// Mata ut all data                
			for (int fileRowCounter = 0; fileRowCounter < maxRowsInFile && rowCounter < dataTable.Rows.Count; fileRowCounter++)
			{
				DataRow row = dataTable.Rows[rowCounter];
				stringOut = new StringBuilder();

				for (int i = 0; i < row.ItemArray.Length; i++)
				{
					object value = row[i];
					string stringValue = "";

					if (value != null && value != DBNull.Value)
					{
						stringValue = value.ToString().Replace(Environment.NewLine, " ").Replace(separator_char, ' ');
						stringValue = AddQuotes(stringValue);
					}

					if (i > 0)
					{
						stringOut.Append(separator_char);
					}

					stringOut.Append(stringValue);

				}

				streamOut.WriteLine(stringOut);
				rowCounter++;
			}

			streamOut.Close();

		}
	}
}


