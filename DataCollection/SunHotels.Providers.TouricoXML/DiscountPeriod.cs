using System;
using System.Collections.Generic;
using System.Text;
using SunHotels.Providers.com.touricoholidays.exportdataws;
using System.Globalization;

namespace SunHotels.Providers
{
    public class DiscountPeriod
    {
        public enum DiscountType { PayStay, Amount, Percentage, BoardBase }

        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int Stay { get; set; }
        public decimal Value { get; set; }
        public int MaxFree { get; set; }
        public int MaxNights { get; set; }
        public bool Cheapest { get; set; }
        public DiscountType Type { get; set; }

        public DiscountPeriod(Rates.DiscountRow rdr)
        {
            Type = (DiscountType)Enum.Parse(typeof(DiscountType), rdr.type, true);
            StartDate = DateTime.Parse(rdr.startDate);
            EndDate = DateTime.Parse(rdr.endDate);

            if (!rdr.IsstayNull())
            {
                int stay;
                if (Int32.TryParse(rdr.stay, out stay))
                {
                    Stay = stay;
                }
            }

            if (!rdr.IsvalueNull())
            {
                decimal value;
                if (decimal.TryParse(rdr.value, NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture, out value))
                {
                    Value = value;
                }
            }

            if (!rdr.IsmaxfreeNull())
            {
                int maxFree;
                if (Int32.TryParse(rdr.maxfree, out maxFree))
                {
                    MaxFree = maxFree;
                }
            }

            if (!rdr.IsmaxNightsNull())
            {
                MaxNights = rdr.maxNights;
            }

            if (!rdr.IscheapestNull())
            {
                bool cheapest;
                if (Boolean.TryParse(rdr.cheapest, out cheapest))
                {
                    Cheapest = cheapest;
                }
            }

            if (Type == DiscountType.PayStay && Stay <= Value)
            {
                // TODO: log this...
                throw new ApplicationException(string.Format("DiscountPeriod: MinStay ({0}) <= PayDays ({1})", Stay, Value));
            }
        }

        public bool InDiscountPeriod(DateTime date)
        {
            return StartDate <= date && date <= EndDate;
        }
    }
}
