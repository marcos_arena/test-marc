using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;
using System.Linq;
using Sunhotels.Export;
using SunHotels.XML;
using ICSharpCode.SharpZipLib.Zip;
using System.Net.Mail;
using Empir.Data.CSV.FIX;

namespace SunHotels.Providers
{
    /// <summary>
    /// Class containing all static data from Tourico as DataTables.
    /// </summary>
    class StaticData
    {
        List<DataTable> dataTableList;
        XmlDocument xmlExportConfig;
        string tmpWorkingDir;
        List<string> zipList;
        List<string> csvList;
        static List<string> allDownLoadedFiles;
        /// <summary>
        /// Class constructor.
        /// </summary>
        /// <param name="config">Loader configuration</param>
        public StaticData(Configuration config)
        {
            tmpWorkingDir = config.LocalTemp;
            Directory.CreateDirectory(tmpWorkingDir);
            dataTableList = new List<DataTable>();
            zipList = new List<string>();
            csvList = new List<string>();

            xmlExportConfig = new XmlDocument();
            xmlExportConfig.Load(config.ServersConfigFileFullPath);
        }

        /// <summary>
        /// Retrieves all static data specified by Tourico_ExportConfig.xml.
        /// </summary>
        /// <returns></returns>
        public List<DataTable> GetStaticData()
        {
            DownloadZipFiles();
            UnzipFiles();
            FillDataTables();
			CheckFileDate();

            return dataTableList;
        }

		private void CheckFileDate()
		{
			//TODO: Improve more patterns, for each provider, pending check..
			WriteDebug("Checking file dates...");
			Match date;
			string formatStringOne = "yyyyMMddHHmmss";
			string formatStringTwo = "yyyyMMdd";
			var yyyyMMddHHmmss = new Regex("(?:(?:(?:(?:(?:[13579][26]|[2468][048])00)|(?:[0-9]{2}(?:(?:[13579][26])|(?:[2468][048]|0[48]))))(?:(?:(?:09|04|06|11)(?:0[1-9]|1[0-9]|2[0-9]|30))|(?:(?:01|03|05|07|08|10|12)(?:0[1-9]|1[0-9]|2[0-9]|3[01]))|(?:02(?:0[1-9]|1[0-9]|2[0-9]))))|(?:[0-9]{4}(?:(?:(?:09|04|06|11)(?:0[1-9]|1[0-9]|2[0-9]|30))|(?:(?:01|03|05|07|08|10|12)(?:0[1-9]|1[0-9]|2[0-9]|3[01]))|(?:02(?:[01][0-9]|2[0-8])))))(?:0[0-9]|1[0-9]|2[0-3])(?:[0-5][0-9]){2}");
			var yyyymmdd = new Regex("(19|20)[0-9]{2}(0[1-9]|1[012])(0[1-9]|[12][0-9]|3[01])");
			DateTime dt;
			Dictionary<string, DateTime> dic = new Dictionary<string, DateTime>();
			int count = zipList.Where(f => yyyymmdd.IsMatch(f) || yyyyMMddHHmmss.IsMatch(f)).Count();
			foreach (var item in zipList.Where(f => yyyymmdd.IsMatch(f) || yyyyMMddHHmmss.IsMatch(f)))
			{

				if (yyyyMMddHHmmss.IsMatch(item))
				{
					date = yyyyMMddHHmmss.Match(item);
					dt = DateTime.ParseExact(date.Value, formatStringOne, null);
					if (dt < DateTime.Now.AddDays(-2))
					{
						dic.Add(item, dt);
					}
				}
				else if (yyyymmdd.IsMatch(item))
				{
					date = yyyymmdd.Match(item);
					dt = DateTime.ParseExact(date.Value, formatStringTwo, null);
					if (dt < DateTime.Now.AddDays(-2))
					{
						dic.Add(item, dt);
					}
				}
			}
			//TODO: If my dictionary contains any element sendMail warning with these elements

			//Remove descriptions and statif info from dictionary
			var filteredDic = dic.Where(d => !d.Key.ToLower().Contains("PDS_") && !d.Key.ToLower().Contains("THF_")).ToDictionary(x => x.Key, x => x.Value);
			
			if (filteredDic.Any())
			{
				SendEmail(filteredDic);
			}
		}

		private void SendEmail(Dictionary<string, DateTime> dic)
		{
			//TODO: Finish this function
			
			
			string providerName = XMLProvider.providerName;
			string HtmlBody = GetBodyAsHtml(dic, providerName);
			
			var client = new SmtpClient
			{
				Host = "smtp1.sunhotels.net",
				Port = 25,
				UseDefaultCredentials = false,
				DeliveryMethod = SmtpDeliveryMethod.Network
			};
			using (MailMessage message = new MailMessage("AlertsDG@sunhotels.net", "support@sunhotels.com")
			{
				Subject = "WARNING file too old",
				Body = HtmlBody,
				IsBodyHtml = true,
				BodyEncoding = System.Text.Encoding.UTF8,
			})
			{
				client.Send(message);
			};
		}

		private string GetBodyAsHtml(Dictionary<string, DateTime> dic, string providerName)
		{
			StringBuilder Body = new StringBuilder();
			Body.AppendLine("<html><body><table border='1' bordercolor='black'>");
			int numFiles = 0;
			foreach (var item in dic)
			{
				if (numFiles < 1)
				{
					Body.AppendLine("<tr>");
					Body.AppendLine("<td><b>FileName</b></td><b><td>ProviderName</b></td><td><b>DateCreated</b></td>");
					Body.AppendLine("</tr>");
				}
				Body.AppendLine("<tr>");
				Body.AppendLine("<td style='padding-right:10px'>" + item.Key + "</td><td style='padding-right:10px'>" + providerName + "</td><td style='padding-right:10px'>" + item.Value + "</td>");
				Body.AppendLine("</tr></br></br>");
				numFiles++;
			}
			Body.AppendLine(string.Format("</table><br/><b> We have {0} file/s pending to check.</b></br>", numFiles));
			Body.AppendLine("<body></html>");

			return Body.ToString();
		}

        /// <summary>
        ///  Downloades static data from Tourico FTP as unzipped CSV files. 
        /// </summary>
        private void DownloadZipFiles()
        {
            XmlNodeList serverNodes = xmlExportConfig.SelectNodes("//Server");
			Dictionary<string, Tuple<string, FTPCommunication>> files2download = new Dictionary<string, Tuple<string, FTPCommunication>>();

            foreach (XmlNode serverNode in serverNodes)
            {
                FTPCommunication ftpCom = new FTPCommunication(serverNode.Attributes["userId"].Value, serverNode.Attributes["userPassword"].Value);

                // Get folders
                XmlNodeList folderNodeList = serverNode.ChildNodes;
                foreach (XmlNode folderNode in folderNodeList)
                {
                    string sFileUrl = serverNode.Attributes["serverURL"].Value + folderNode.Attributes["folderId"].Value;

                    Dictionary<string, string> fileList = new Dictionary<string, string>(100);
                    ftpCom.GetDirectoryList(sFileUrl, fileList);

                    // Extract list of mathing files saved to TEMP folder
                    string sFileFilterRegex = string.Empty;
                    bool firstNode = true;
                    string sSuffix = string.Empty;

                    // Create RegExp to match all files we want to download.
                    XmlNodeList fileNodeList = folderNode.ChildNodes;
                    foreach (XmlNode fileNode in fileNodeList)
                    {
                        if (firstNode)
                        {                            
                                sFileFilterRegex += ("^((" + fileNode.Attributes["filePrefix"].Value + ")");
                                firstNode = false;    
                        }
                        else
                        {
                            sFileFilterRegex += ("|(" + fileNode.Attributes["filePrefix"].Value + ")");
                        }

                        sSuffix = fileNode.Attributes["fileSufix"].Value;
                    }

                    // Add suffix with escaped dots
                    sFileFilterRegex += (").*(" + sSuffix.Replace(".", @"\.") + ")");

                    // Match files using regexp
                    Regex regExFilter = new Regex(sFileFilterRegex);
                    foreach (string ftpFile in fileList.Keys)
                    {
                        if (regExFilter.IsMatch(fileList[ftpFile]))
                        {
                            files2download.Add(fileList[ftpFile], new Tuple<string, FTPCommunication>(ftpFile, ftpCom));
                        }
                    }
                }
            }
            allDownLoadedFiles = new List<string>();
			files2download
				.All(kv =>
				{
					var ftpFile = kv.Value;
					var file = kv.Key;
                    var fullFileName = tmpWorkingDir + file;
                    WriteDebug("Starting download of file: " + file);
                    ftpFile.Item2.DownloadFile(ftpFile.Item1, fullFileName);
                    if (ftpFile.Item1.EndsWith("zip"))
					{
						zipList.Add(fullFileName);
                        if (file.StartsWith("THF_Descriptions_"))
                        {
                            allDownLoadedFiles.Add(file.Substring(0, 22));
                        }                        
					}                   
					return true;
				});
        }

        /// <summary>
        /// Unzipes the CSV files and adds them to a list
        /// </summary>
        private void UnzipFiles()
        {
			zipList
				.All(sFile =>
				{
					WriteDebug("Unzipping file: " + sFile);
					csvList.AddRange(UnZip(sFile, tmpWorkingDir));
					WriteDebug("Unzip complete!");

					// Delete ZIP file.
					File.Delete(sFile);
                    return true;
				});
        }


        /// <summary>
        /// Check that all CSV files has ben downladed and unziped.
        /// If all CSV files exists, import the static info from CSV files to DataTables.
        /// </summary>
        private void FillDataTables()
        {
            XmlNodeList allFilesList = xmlExportConfig.SelectNodes("//File");
            string csvFilesMissing = string.Empty;
            bool firstFile = true;

            
            foreach (XmlNode fileNode in allFilesList)
            {
                if (fileNode.Attributes["csvFileName"] != null)
                {
                    string csvFileName = fileNode.Attributes["csvFileName"].Value;


                    if (!File.Exists(tmpWorkingDir + csvFileName))
                    {
                        if (firstFile)
                        {
                            csvFilesMissing = String.Concat(csvFilesMissing, csvFileName);
                            firstFile = false;
                        }
                        else
                        {
                            csvFilesMissing = String.Concat(csvFilesMissing, ", ", csvFileName);
                        }
                    }
                }
                else
                {
                    break;
                }
               
            }
            foreach (string fileName in allDownLoadedFiles)
            {

                string csvFileName = fileName + ".csv";
                if (!File.Exists(tmpWorkingDir + csvFileName))
                {
                    if (firstFile)
                    {
                        csvFilesMissing = String.Concat(csvFilesMissing, csvFileName);
                        firstFile = false;
                    }
                    else
                    {
                        csvFilesMissing = String.Concat(csvFilesMissing, ", ", csvFileName);
                    }
                }
                
            }

            if (csvFilesMissing != string.Empty)
            {
                throw new ApplicationException("CSV file missing, file/files does not exist: " + csvFilesMissing);
            }
            else
            {
                foreach (string sFile in csvList)
                {
                    string sImportFile = sFile;
                    
                    DataTable tableImport = CreateTableFromCSV(sImportFile);
                    dataTableList.Add(tableImport);
                }
            }
        }

        /// <summary>
        /// Unzip specified ZIP file content to specified target directory.
        /// </summary>
        /// <param name="sFile">The path to the zipfile.</param>
        /// <param name="sTargetDirectory">The directory used to save the extracted zipfile.</param>
        /// <returns>The list of unziped files.</returns>
        private List<string> UnZip(string sFile, string sTargetDirectory)
        {
            List<string> listFiles = new List<string>();

            using (ZipInputStream s = new ZipInputStream(File.OpenRead(sFile)))
            {
                try
                {
                    ZipEntry theEntry;
                    while ((theEntry = s.GetNextEntry()) != null)
                    {
                        string directoryName = sTargetDirectory;
                        string fileName = Path.GetFileName(theEntry.Name);

                        if (fileName != String.Empty)
                        {
                            using (FileStream streamWriter = File.Create(directoryName + theEntry.Name))
                            {
                                listFiles.Add(streamWriter.Name);

                                int size = 2048;
                                byte[] data = new byte[2048];
                                while (true)
                                {
                                    size = s.Read(data, 0, data.Length);
                                    if (size > 0)
                                    {
                                        streamWriter.Write(data, 0, size);
                                    }
                                    else
                                    {
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new ApplicationException(string.Format("Unable to unzip file {0} to directory {1}", sFile, sTargetDirectory), ex);
                }
            }
            return listFiles;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="csvFile"></param>
        /// <returns></returns>
        private DataTable CreateTableFromCSV(string csvFile)
        {
            try
            {
                string cleanString = File.ReadAllText(csvFile);

                //Clean all <LF> from file, then add <LF> after all <CR> to do correct line feeds. This will remove all invalid <LF> that does not have a <CR>.
                cleanString = cleanString.Replace(((char)0x0a).ToString(), string.Empty);
                cleanString = cleanString.Replace(((char)0x0d).ToString(), ((char)0x0d).ToString() + ((char)0x0a).ToString());


                //Clean file from Tourico invalid characters.
                cleanString = cleanString.Replace(((char)0x00).ToString(), string.Empty);
                cleanString = cleanString.Replace(((char)0x01).ToString(), string.Empty);
                cleanString = cleanString.Replace(((char)0x02).ToString(), string.Empty);
                cleanString = cleanString.Replace(((char)0x03).ToString(), string.Empty);
                cleanString = cleanString.Replace(((char)0x04).ToString(), string.Empty);
                cleanString = cleanString.Replace(((char)0x05).ToString(), string.Empty);
                cleanString = cleanString.Replace(((char)0x06).ToString(), string.Empty);
                cleanString = cleanString.Replace(((char)0x07).ToString(), string.Empty);
                cleanString = cleanString.Replace(((char)0x08).ToString(), string.Empty);
                // TAB cleanString = cleanString.Replace(((char)0x09).ToString(), string.Empty);
                cleanString = cleanString.Replace(((char)0x10).ToString(), string.Empty);
                // LF cleanString = cleanString.Replace(((char)0x11).ToString(), string.Empty);
                cleanString = cleanString.Replace(((char)0x12).ToString(), string.Empty);
                // CR cleanString = cleanString.Replace(((char)0x13).ToString(), string.Empty);
                cleanString = cleanString.Replace(((char)0x14).ToString(), string.Empty);
                cleanString = cleanString.Replace(((char)0x15).ToString(), string.Empty);
                cleanString = cleanString.Replace(((char)0x16).ToString(), string.Empty);
                cleanString = cleanString.Replace(((char)0x17).ToString(), string.Empty);
                cleanString = cleanString.Replace(((char)0x18).ToString(), string.Empty);
                cleanString = cleanString.Replace(((char)0x19).ToString(), string.Empty);
                cleanString = cleanString.Replace(((char)0x1A).ToString(), string.Empty);
                cleanString = cleanString.Replace(((char)0x1B).ToString(), string.Empty);
                cleanString = cleanString.Replace(((char)0x1C).ToString(), string.Empty);
                cleanString = cleanString.Replace(((char)0x1D).ToString(), string.Empty);
                cleanString = cleanString.Replace(((char)0x1E).ToString(), string.Empty);
                cleanString = cleanString.Replace(((char)0x1F).ToString(), string.Empty);

                //System.Diagnostics.Debug.WriteLine(cleanString.IndexOf((char)0x0a));
                File.WriteAllText(csvFile, cleanString);

                CSVData importCSV = new CSVData();
                importCSV.SEPARATOR_CHAR = '|';
                importCSV.QuoteString = "\"";
                return importCSV.Read( csvFile);
            }
            catch (Exception ex)
            {
                throw new ApplicationException(string.Format("Error while reading CSV file {0} to DataTable", csvFile), ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="debugMessage"></param>
        void WriteDebug(string debugMessage)
        {
#if (DEBUG)
            Console.WriteLine("debug: " + debugMessage);
#endif
        }

    }
}
