﻿using System;
using System.Globalization;
using System.Xml.Linq;
using System.Xml.Serialization;
using System.Linq;

namespace SunHotels.Providers
{
    public static class GeneralUtilities
    {
        #region Public Method

        /// <summary>
        /// Removes \n\nheadings\n from description
        /// </summary>
        /// <param name="str">string</param>
        /// <returns>Description</returns>
        public static string CleanHeadingsFromDescription(this string str)
        {
            var finalString = string.Empty;
            string[] splitFirst = str.Split(new[] { "\\n\\n" }, StringSplitOptions.None);
            if (splitFirst.Length <= 1)
                return str;
            foreach (var strVal in splitFirst)
            {
                string[] splitSecond = strVal.Split(new[] {"\\n"}, StringSplitOptions.None);
                int skipIndex = 0;
                if (splitSecond.Length > 1)
                    skipIndex = 1;
                string value = string.Join(" ", splitSecond.Skip(skipIndex));
                finalString = string.Join(" ", finalString, value.TrimStart());
            }
            return finalString.Trim();
        }

        /// <summary>
        /// Taking required lines from string.
        /// </summary>
        /// <param name="str">string</param>
        /// <param name="lineSeparator">lineSeparator</param>
        /// <param name="lineCount">lineCount</param>
        /// <returns>string</returns>
        public static string TakeLines(this string str, string lineSeparator, int lineCount)
        {
            if (string.IsNullOrEmpty(str))
                return string.Empty;
            var lines = str.Split(new[] { lineSeparator }, StringSplitOptions.RemoveEmptyEntries);
            return string.Join(lineSeparator, lines.Take(lineCount));
        }

        /// <summary>
        /// Round the decimal value and convert back to string
        /// </summary>
        /// <param name="str">string</param>
        /// <param name="decimals">rounding decimals</param>
        /// <returns></returns>
        public static string Round(this string str, int decimals)
        {
            return string.IsNullOrEmpty(str) ? string.Empty :
                Math.Round(decimal.Parse(str, CultureInfo.InvariantCulture),
                decimals).ToString(CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// Adding Dot and Space in required places.
        /// </summary>
        /// <param name="str">string</param>
        public static string AddDot(this string str)
        {
            string result = str;
            if (!string.IsNullOrEmpty(result))
                result = str.Trim().Right(1) == "." ? str.Trim() : str.Trim() + ".";
            return result;
        }

        /// <summary>
        /// De-Serialize
        /// </summary>
        /// <typeparam name="T">Expected type</typeparam>
        /// <param name="doc">XDocument</param>
        /// <returns>type of object</returns>
        public static T Deserialize<T>(XDocument doc)
        {
            var xmlSerializer = new XmlSerializer(typeof(T));
            if (doc.Root == null) return default(T);
            using (var reader = doc.Root.CreateReader())
                return (T)xmlSerializer.Deserialize(reader);
        }

        /// <summary>
        /// Taking string from right based on the length
        /// </summary>
        /// <param name="str"></param>
        /// <param name="length"></param>
        /// <returns></returns>
        public static string Right(this string str, int length)
        {
            str = str ?? string.Empty;
            return str.Length >= length ? str.Substring(str.Length - length, length) : str;
        }

        #endregion Public Method
    }
}
