using Empir.Data.CSV;
using SunHotels.Providers.ProviderCommon.DataLoader;
using SunHotels.XML;
using SunHotels.XML.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using SunHotels.XML.HelpClasses;
using FTPCommunication = SunHotels.XML.FTPCommunication;

namespace SunHotels.Providers
{
    public class JacTravelXMLLive : XMLProvider, IStaticDataLogger
    {
        #region Global Variable Declaration

        // Create dictionaries for fast search
        private readonly Dictionary<string, Place> _allPlaces = new Dictionary<string, Place>();
        private readonly Dictionary<string, Hotel> _allHotels = new Dictionary<string, Hotel>();
        private JacTravelXmlLiveSettings _jacTravelXmlLiveSettings = new JacTravelXmlLiveSettings();

        #endregion

        #region XMLProvider Implementation Methods

        /// <summary>
        /// getProviderDefinition
        /// </summary>
        /// <param name="root">Root</param>
        protected override void getProviderDefinition(Root root)
        {
            root.ProviderDefinition.Name = configuration.ProviderName;
            root.ProviderDefinition.Currency = configuration.Currency;
        }

        /// <summary>
        /// Update Configuration
        /// </summary>
        /// <param name="configuration">Configuration</param>
        protected override void UpdateConfiguration(Sunhotels.Export.Configuration configuration)
        {
            configuration.WriteHotelsWithoutRooms = true;
            configuration.WriteRoomsWithoutAvailability = true;
        }

        /// <summary>
        /// Entry point for class.
        /// </summary>
        /// <param name="root">Root for Sunhotels XML structure.</param>
        protected override void getData(Root root)
        {
            try
            {
                this.root = root;

                // ------------------------------------------------------------------------------------------------
                // Create log stream
                // ------------------------------------------------------------------------------------------------
                try
                {
                    Directory.CreateDirectory(configuration.LogOutput);
                }
                catch (Exception ex)
                {
                    throw new ApplicationException(string.Join(": ", configuration.ProviderName, "Unable to create log file."), ex);
                }
                AddToConsole(string.Join(": ", configuration.ProviderName, "Begin static data import."));
                Log(ILoggerStaticDataSeverity.Info, string.Join(": ", configuration.ProviderName, "Begin static data import."));

                // Get all static data. If files are valid and able to process then this method will return true.
                if (!GetStaticData()) return;
                AddToConsole(string.Join(": ", configuration.ProviderName, "Begin processing list of hotels and places from available static data."));
                Log(ILoggerStaticDataSeverity.Info, string.Join(": ", configuration.ProviderName, "Begin processing list of hotels and places from available static data."));
                foreach (var item in _allHotels)
                    _allPlaces[item.Value.PlaceId].Hotels.Add(item.Value.Id, item.Value);

                AddToConsole(string.Join(": ", configuration.ProviderName, "Finished processing list of hotels and places from available static data."));
                Log(ILoggerStaticDataSeverity.Info, string.Join(": ", configuration.ProviderName, "Finished processing list of hotels and places from available static data."));
            }
            catch (Exception ex)
            {
                Log(ILoggerStaticDataSeverity.Error, string.Join(string.Empty, configuration.ProviderName, ": Unhanded exception while getting data for static import: [", ex.ToString(), "]"));
                throw;
            }

        }
        /// <summary>
        /// Add message to Console
        /// </summary>
        /// <param name="message">message</param>
        private static void AddToConsole(string message)
        {
#if DEBUG
            Console.WriteLine(message);
#endif
        }

        /// <summary>
        /// Get Global Feature mapping for the provider
        /// </summary>
        /// <param name="root">Root</param>
        protected override void getFeatureTypes(Root root)
        {
            try
            {
                // Get SH features mapped with the provider
                DataMapper?.SunHotelsFeatures?.Join(DataMapper["Feature"], o => o.Key, i => i.Key, (o, i) => o)
                    .All(f =>
                    {
                        root.Features.Add(new Feature
                        {
                            Id = f.Key,
                            Value = f.Value,
                            Name = f.Value
                        });
                        return true;
                    });
            }
            catch (Exception ex)
            {
                Log(ILoggerStaticDataSeverity.Error, string.Join(": ", configuration.ProviderName, "Error occurred while getting Sunhotels feature types mapped for the provider. Error", ex.Message));
            }
        }

        /// <summary>
        /// Log function
        /// </summary>
        /// <param name="severity">ILoggerStaticDataSeverity</param>
        /// <param name="message">message</param>
        /// <param name="args">arguments</param>
        public void Log(ILoggerStaticDataSeverity severity, string message, params object[] args)
        {
            Log.MessageType s;
            switch (severity)
            {
                case ILoggerStaticDataSeverity.Error:
                    s = XML.HelpClasses.Log.MessageType.Error;
                    break;
                case ILoggerStaticDataSeverity.Warning:
                    s = XML.HelpClasses.Log.MessageType.Warning;
                    break;
                default:
                    s = XML.HelpClasses.Log.MessageType.Informational;
                    break;
            }
            LogEntry(s, message, args);
        }

        #endregion

        #region Void

        /// <summary>
        /// Get all static data from API and bind with Dictionary objects.
        /// </summary>              
        private bool GetStaticData()
        {
            try
            {
                _jacTravelXmlLiveSettings = GetJacTravelXmlLiveSettings();
                Log(ILoggerStaticDataSeverity.Info, string.Join(": ", configuration.ProviderName, "Downloading and processing files containing provider static information begins."));
                Directory.CreateDirectory(configuration.LocalTemp);
                var files = DownloadFilesFromServer();
                var isValid = files.Count > 0;
                if (isValid)
                {
                    ProcessFiles(files);
                    if (_jacTravelXmlLiveSettings.DeleteFileAfterProcessing)
                        DeleteFilesAfterProcessing(files);
                    Log(ILoggerStaticDataSeverity.Info, string.Join(string.Empty, configuration.ProviderName, ": Total no of hotels created is ", _allHotels.Count));
                    Log(ILoggerStaticDataSeverity.Info, string.Join(": ", configuration.ProviderName, "Downloading and processing files containing provider static information ends."));
                    return true;
                }
                Log(ILoggerStaticDataSeverity.Info, string.Join(": ", configuration.ProviderName, "Required files are not available to create the hotel and places."));
                return false;
            }
            catch (Exception ex)
            {
                Log(ILoggerStaticDataSeverity.Error, string.Join(": ", configuration.ProviderName, "Error occurred while downloading and processing files containing static data. Error", ex.Message));
                return false;
            }
        }

        /// <summary>
        /// Delete Directory After Processing
        /// </summary>
        /// <param name="files">Dictionary of Language, FileInformation</param>
        private void DeleteFilesAfterProcessing(Dictionary<Language, string> files)
        {
            try
            {
                var sw = Stopwatch.StartNew();
                Log(ILoggerStaticDataSeverity.Info, configuration.ProviderName, ": Begin deleting processed static data files.");
                foreach (var file in files)
                {
                    if (!string.IsNullOrEmpty(file.Value))
                        FileDelete(file.Value);
                }
                var ems = sw.ElapsedMilliseconds;
                Log(ILoggerStaticDataSeverity.Info, string.Join(": ", configuration.ProviderName, "Finished deleting processed static data files. Total time elapsed in ms", ems));
            }
            catch (Exception ex)
            {
                Log(ILoggerStaticDataSeverity.Error, string.Join(": ", configuration.ProviderName, "Error occurred while deleting processed static data files. Error", ex.Message));
            }
        }

        /// <summary>
        /// File Delete
        /// </summary>
        /// <param name="filePath">filePath</param>
        private void FileDelete(string filePath)
        {
            try
            {
                if (!File.Exists(filePath)) return;
                File.Delete(filePath);
                Log(ILoggerStaticDataSeverity.Info, string.Join(": ", configuration.ProviderName, "Deleted static data file successfully. File Path", filePath));
            }
            catch (Exception ex)
            {
                Log(ILoggerStaticDataSeverity.Error, string.Join(": ", configuration.ProviderName, "Error occurred while deleting static data file. File Path", filePath, ". Error", ex.Message));
            }
        }

        /// <summary>
        /// Add Hotel Translation
        /// </summary>
        /// <param name="filelist">file list</param>
        private void AddHotelTranslation(IEnumerable<KeyValuePair<Language, string>> filelist)
        {
            try
            {
                var sw = Stopwatch.StartNew();

                filelist.AsParallel().Select(file =>
                {
                    DataTable dt = CreateDataTableFromCsv(file.Value);
                    if (dt == null || dt.Rows.Count == 0)
                    {
                        Log(ILoggerStaticDataSeverity.Info, string.Join(": ", configuration.ProviderName, "Empty or null DataTable object returned while processing hotel translations."));
                        return true;
                    }

                    dt.Rows.AsEnumerable().AsParallel().Select(row =>
                    {
                        var propertyReferenceId = row["PropertyReferenceID"].ToString();
                        var description = row["Description"].ToString();
                        var country = row["Country"].ToString();

                        lock (_allHotels)
                        {
                            Hotel hotel;
                            _allHotels.TryGetValue(propertyReferenceId, out hotel);
                            if (hotel == null) return true;
                            //Clean the description
                            description = description.CleanHeadingsFromDescription();

                            if (string.IsNullOrEmpty(description)) return true;
                            hotel.Translations.description.Add(new TranslationsDescription
                            {
                                country = country,
                                lang = file.Key.ShCode,
                                Value = description
                            });

                            hotel.Translations.headline.Add(new TranslationsHeadline
                            {
                                country = country,
                                lang = file.Key.ShCode,
                                Value = description.TakeLines(_jacTravelXmlLiveSettings.HeadLineSeparator,
                                _jacTravelXmlLiveSettings.HeadLineCount).AddDot()
                            });

                        }
                        return true;
                    }).ToArray();

                    return true;
                }).ToArray();
                var ems = sw.ElapsedMilliseconds;
                Log(ILoggerStaticDataSeverity.Info, string.Join(": ", configuration.ProviderName, "Finished processing and adding Hotel Translations. Total time elapsed in ms", ems));
            }
            catch (Exception ex)
            {
                Log(ILoggerStaticDataSeverity.Error, string.Join(": ", configuration.ProviderName, "Error occurred while processing and adding Hotel Translations. Error", ex.Message));
            }
        }

        /// <summary>
        /// Process Hotel List
        /// </summary>
        /// <param name="language">Language</param>
        /// <param name="filePath">filePath</param>
        /// <param name="features">IReadOnlyDictionary of string, KeyValuePair of string, string</param>
        private void ProcessHotelList(string language, string filePath,
            IReadOnlyDictionary<string, KeyValuePair<string, string>> features)
        {
            try
            {
                var sw = Stopwatch.StartNew();
                DataTable dt = CreateDataTableFromCsv(filePath);
                if (dt == null || dt.Rows.Count == 0)
                {
                    Log(ILoggerStaticDataSeverity.Info, string.Join(": ", configuration.ProviderName, "Empty or null DataTable object returned while processing hotel lists."));
                    return;
                }

                dt.Rows.AsEnumerable().AsParallel().Select(row =>
                {
                    var hotelId = row["PropertyReferenceID"].ToString();
                    try
                    {
                        var newHotel = CreateHotel(row, features);
                        if (newHotel != null)
                        {
                            Hotel currentHotel;
                            if (!_allHotels.TryGetValue(newHotel.Id, out currentHotel))
                            {
                                lock (_allHotels)
                                {
                                    _allHotels.Add(newHotel.Id, newHotel);
                                }
                            }
                            else
                                Log(ILoggerStaticDataSeverity.Info, string.Join(": ", configuration.ProviderName, "The hotel with id", newHotel.Id, " already exists in the hotel list."));
                        }
                    }
                    catch (Exception ex)
                    {
                        Log(ILoggerStaticDataSeverity.Error, string.Join(string.Empty, configuration.ProviderName, ": Error occurred while processing Hotel object with id ", !string.IsNullOrEmpty(hotelId) ? hotelId : string.Empty, ".Error: ", ex.Message));
                    }
                    return true;
                }).ToArray();

                var ems = sw.ElapsedMilliseconds;
                Log(ILoggerStaticDataSeverity.Info, string.Join(": ", configuration.ProviderName, "Completed processing hotel lists. Total time elapsed in ms", ems));
            }
            catch (Exception ex)
            {
                Log(ILoggerStaticDataSeverity.Error, string.Join(string.Empty, configuration.ProviderName, ": Error occurred while processing hotel lists. Language: ", language, ". Error: ", ex.Message));
            }
        }

        /// <summary>
        /// Process Files
        /// </summary>
        /// <param name="files">Dictionary of Language, FileInformation</param>
        private void ProcessFiles(Dictionary<Language, string> files)
        {
            try
            {
                var sw = Stopwatch.StartNew();
                Log(ILoggerStaticDataSeverity.Info, string.Join(": ", configuration.ProviderName, "Begin processing of static data files."));
                var features = DataMapper?.SunHotelsFeatures?.Join(DataMapper["Feature"], o => o.Key, i => i.Key,
                            (o, i) => new { SHFeature = o, ProviderFeature = i })
                        .SelectMany(o => o.ProviderFeature.Value.Select(of => new { ProviderId = of, o.SHFeature }))
                        .ToDictionary(o => o.ProviderId, o => o.SHFeature);

                var primaryLanguageFilelist = files.First(c => c.Key.ShCode == configuration.Language);
                ProcessHotelList(primaryLanguageFilelist.Key.ShCode, primaryLanguageFilelist.Value, features);

                var filelist = files.Where(c => c.Key.ShCode != configuration.Language);
                AddHotelTranslation(filelist);

                var ems = sw.ElapsedMilliseconds;
                Log(ILoggerStaticDataSeverity.Info, string.Join(": ", configuration.ProviderName, "Processing static data files completed. Total time elapsed in ms", ems));
            }
            catch (Exception ex)
            {
                Log(ILoggerStaticDataSeverity.Error, string.Join(string.Empty, configuration.ProviderName, ": Error occurred while processing static data files. Number of files: ", files.Count, ". Error: ", ex.Message));
            }
        }

        /// <summary>
        /// Add Image into images list
        /// </summary>
        /// <param name="images">Image list</param>
        /// <param name="imageUrl">imageUrl</param>
        /// <param name="index">index</param>
        private void AddImage(ICollection<Hotel.Image> images, string imageUrl, int index)
        {
            try
            {
                if (string.IsNullOrEmpty(imageUrl))
                    return;
                images.Add(new Hotel.Image
                {
                    Id = index.ToString(),
                    ImageVariants = new List<Hotel.ImageVariant>
                    {
                        new Hotel.ImageVariant {

                            URL = imageUrl,
                            Height =_jacTravelXmlLiveSettings.ImageHeight,
                            Width =_jacTravelXmlLiveSettings.ImageWidth
                        }
                    }
                });
            }
            catch (Exception ex)
            {
                Log(ILoggerStaticDataSeverity.Error, string.Join(": ", configuration.ProviderName, "Error occurred while creating a new image. Error", ex.Message));
            }
        }

        #endregion

        #region Get Type

        /// <summary>
        /// Get Hotel Features
        /// </summary>
        /// <param name="hotelCode">hotelCode</param>
        /// <param name="hotelFeatures">hotelFeatures</param>
        /// <param name="features">features Dictionary key pair value</param>
        /// <returns></returns>
        private List<Feature> GetHotelFeatures(string hotelCode, string hotelFeatures,
            IReadOnlyDictionary<string, KeyValuePair<string, string>> features)
        {
            try
            {
                var featureCollection = new List<Feature>();
                if (features == null || string.IsNullOrEmpty(hotelFeatures))
                    return featureCollection;
                var featureList = hotelFeatures.Split(',');
                featureList.AsParallel().Select(feature =>
                {
                    KeyValuePair<string, string> shfeature;
                    if (!features.TryGetValue(feature, out shfeature)) return true;
                    lock (featureCollection)
                    {
                        if (featureCollection.All(f => f.Id != shfeature.Key))
                        {
                            featureCollection.Add(new Feature
                            {
                                Id = shfeature.Key,
                                Name = shfeature.Value,
                                Value = shfeature.Value
                            });
                        }
                    }
                    return true;
                }).ToList();
                return featureCollection.OrderBy(c => int.Parse(c.Id)).ToList();
            }
            catch (Exception ex)
            {
                Log(ILoggerStaticDataSeverity.Error, string.Join(string.Empty, configuration.ProviderName, ": Error occurred while listing hotel Features. Hotel: ", hotelCode, ". Error :", ex.Message));
                return null;
            }
        }

        /// <summary>
        /// Get JacTravelXMLLiveSettings
        /// </summary>
        /// <param name="fileName">fileName</param>
        /// <returns>JacTravelXMLLiveSettings</returns>
        private JacTravelXmlLiveSettings GetJacTravelXmlLiveSettings(string fileName= "JacTravelXMLLiveSettings")
        {
            try
            {
                return GeneralUtilities.Deserialize<JacTravelXmlLiveSettings>(XDocument.Load(configuration.ProviderConfigRoot + "\\" + fileName + ".xml"));
            }
            catch (Exception ex)
            {
                Log(ILoggerStaticDataSeverity.Error, string.Join(string.Empty, configuration.ProviderName, ": Error occurred while deserializing provider Live Settings xml file. File name: ", fileName, ". Error: ", ex.Message));
                return null;
            }
        }

        /// <summary>
        /// Get previously created place or create new one with three level mapping:  Country1, Region and Resort.
        /// </summary>
        /// <param name="country">Country1</param>
        /// <param name="region">Region</param>
        /// <param name="resort">Resort</param>
        /// <returns>Place</returns>
        private Place GetPlace(string country, string region, string resort)
        {
            try
            {
                if (string.IsNullOrEmpty(country) || string.IsNullOrEmpty(region) || string.IsNullOrEmpty(resort))
                    return null;

                country = country.ToUpper().Trim();
                region = region.ToUpper().Trim();
                resort = resort.ToUpper().Trim();
                lock (_allPlaces)
                {
                    // Check or create country1 level place 
                    Place matchingCountryPlace;
                    if (!_allPlaces.TryGetValue(country, out matchingCountryPlace))
                    {
                        var newPlace = new Place { Id = country, Description = country };
                        _allPlaces.Add(country, newPlace);
                        lock (root.Places)
                            root.Places.Add(newPlace);
                    }
                    // Check or create region level place if country1 is specified
                    Place matchingRegionPlace;
                    var regionPlace = new Place { Id = region, Description = region };
                    if (!_allPlaces.TryGetValue(region, out matchingRegionPlace))
                    {
                        _allPlaces.Add(region, regionPlace);
                        if (!_allPlaces[country].Places.Contains(regionPlace))
                            _allPlaces[country].Places.Add(regionPlace);
                    }

                    // Check or create resort level place if region is specified
                    Place matchingResortPlace;
                    if (_allPlaces.TryGetValue(resort, out matchingResortPlace)) return matchingResortPlace;

                    var resortplace = new Place { Id = resort, Description = resort };
                    _allPlaces.Add(resort, resortplace);

                    if (!_allPlaces[region].Places.Contains(resortplace))
                        _allPlaces[region].Places.Add(resortplace);
                }
                return _allPlaces[resort];
            }
            catch (Exception ex)
            {
                string errorMsg = string.Join(string.Empty, configuration.ProviderName, ": Error occurred while creating place with Country1: ", country, ", Region:", region, " Resort: ", resort, ". Error: ", ex.Message);
                Log(ILoggerStaticDataSeverity.Error, errorMsg);
                return null;
            }
        }

        /// <summary>
        /// Create Hotel
        /// </summary>
        /// <param name="dr">DataRow</param>
        /// <param name="features">features</param>
        /// <returns>Hotel</returns>
        private Hotel CreateHotel(DataRow dr, IReadOnlyDictionary<string, KeyValuePair<string, string>> features)
        {
            if (dr == null)
                return null;

            var propertyReferenceId = dr["PropertyReferenceID"].ToString();
            var country = dr["Country"].ToString();
            var townCity = dr["TownCity"].ToString();

            if (string.IsNullOrEmpty(propertyReferenceId) || string.IsNullOrEmpty(country) || string.IsNullOrEmpty(townCity))
                return null;

            var propertyName = dr["PropertyName"].ToString();
            var address1 = dr["Address1"].ToString();
            var address2 = dr["Address2"].ToString();
            var postcodeZip = dr["PostcodeZip"].ToString();
            var telephone = dr["Telephone"].ToString();
            var fax = dr["Fax"].ToString();
            var latitude = dr["Latitude"].ToString();
            var longitude = dr["Longitude"].ToString();
            var propertyType = dr["PropertyType"].ToString();
            var rating = dr["Rating"].ToString();
            var facilities = dr["Facilities"].ToString();
            var description = dr["Description"].ToString();         
            var country1 = dr["Country1"].ToString();
            var region = dr["Region"].ToString();
            var resort = dr["Resort"].ToString();

            //Clean description
            description = description.CleanHeadingsFromDescription();
            var headline = description.TakeLines(_jacTravelXmlLiveSettings.HeadLineSeparator,
                _jacTravelXmlLiveSettings.HeadLineCount).AddDot();

            try
            {
                var place = GetPlace(country1, region, resort);
                if (place == null) return null;
                return new Hotel
                {
                    Id = propertyReferenceId,
                    PlaceId = place.Id,
                    Name = propertyName,
                    AccomodationType = GetAccomodation(propertyType),
                    Headline = headline,
                    Description = description.Trim(),
                    Adress_Street1 = address1,
                    Adress_Street2 = address2,
                    Adress_Zipcode = postcodeZip,
                    Adress_City = townCity,
                    Adress_Country = country,
                    Position_Latitude = latitude.Round(_jacTravelXmlLiveSettings.GeoCodeDecimals),
                    Position_Longitude = longitude.Round(_jacTravelXmlLiveSettings.GeoCodeDecimals),
                    Phone = telephone,
                    Fax = fax,
                    Classification = rating,
                    BestBuy = false,
                    Features = GetHotelFeatures(propertyReferenceId, facilities, features),
                    AdultOnly = false,
                    BlockInfant = false,
                    Images = GetImages(dr),
                    Translations = new Translations
                    {
                        headline = new List<TranslationsHeadline>
                        {
                            new TranslationsHeadline { country=country, lang=configuration.Language, Value = headline }
                        },
                        description = new List<TranslationsDescription>
                        {
                            new TranslationsDescription { country=country, lang=configuration.Language, Value = description }
                        }
                    },
                    Identifiers = new[] { new Identifier { Type = _jacTravelXmlLiveSettings.IdentifierType, Value = configuration.ProviderName } }.ToList()
                };
            }
            catch (Exception ex)
            {
                Log(ILoggerStaticDataSeverity.Error, string.Join(string.Empty, configuration.ProviderName, ": Error occurred while creating Hotel object. Hotel Id: ", propertyReferenceId, ". Country: ", country, ". City: ", townCity, ". Error: ", ex.Message));
                return null;
            }
        }

        /// <summary>
        /// Creating Hotel.Image list
        /// </summary>
        /// <param name="dr">DataRow</param>
        /// <returns>List of Hotel.Image</returns>
        private List<Hotel.Image> GetImages(DataRow dr)
        {
            try
            {
                List<Hotel.Image> images = new List<Hotel.Image>();

                //10 images available for each property
                for (int i = 1; i < 11; i++)
                    AddImage(images, dr[string.Join(string.Empty, "Image", i, "URL")].ToString(), i);

                //Thumbnail image also added into image collection based on the AddThumbnail configuration value
                if (!_jacTravelXmlLiveSettings.AddThumbnail) return images;
                var thumbnailUrl = dr["ThumbnailURL"].ToString();
                AddImage(images, thumbnailUrl, 11);

                return images;
            }
            catch (Exception ex)
            {
                Log(ILoggerStaticDataSeverity.Error, string.Join(": ", configuration.ProviderName, "Error occurred while preparing image list. Error", ex.Message));
                return null;
            }
        }

        /// <summary>
        /// Get Accommodation type 
        /// </summary>
        /// <returns>AccomodationType</returns>
        private AccomodationType GetAccomodation(string propertyType)
        {
            try
            {
                switch (propertyType)
                {
                    case "Apartment":
                        return AccomodationType.Apartment;
                    default:
                        return AccomodationType.Hotel;
                }
            }
            catch (Exception ex)
            {
                Log(ILoggerStaticDataSeverity.Error, string.Join(": ", configuration.ProviderName, "Error occurred while getting accommodation type for the property. Error", ex.Message));
                return AccomodationType.Hotel;
            }
        }

        /// <summary>
        /// Download the static data files from ftp location
        /// </summary>
        /// <returns>Dictionary of Language, FileInformation</returns>
        private Dictionary<Language, string> DownloadFilesFromServer()
        {
            try
            {
                Dictionary<string, string> directoryList = new Dictionary<string, string>();

                Log(ILoggerStaticDataSeverity.Info, string.Join(": ", configuration.ProviderName, "Begin connecting to FTP server."));
                AddToConsole(string.Join(": ", configuration.ProviderName, "Begin connecting to FTP server."));

                var ftpCom = new FTPCommunication(configuration.UserId, configuration.Password);
                ftpCom.GetDirectoryList(configuration.WebServiceUrl, directoryList);

                Log(ILoggerStaticDataSeverity.Info, string.Join(": ", configuration.ProviderName, "FTP server connected successfully."));
                AddToConsole(string.Join(": ", configuration.ProviderName, "FTP server connected successfully."));
                Dictionary<Language, string> files = new Dictionary<Language, string>();

                //Connect to JacTravel FTP server and fetch the JacTravel files 
                Log(ILoggerStaticDataSeverity.Info, string.Join(": ", configuration.ProviderName, "Downloading files from the FTP server."));
                AddToConsole(string.Join(": ", configuration.ProviderName, "Downloading files from the FTP server."));

                var fileExtensions = _jacTravelXmlLiveSettings.FileExtensions.Split(',');

                directoryList
                    .Where(c => !string.IsNullOrEmpty(c.Value) &&
                    //Validate file extension
                    fileExtensions.All(f => c.Value.Contains(f)) &&
                    //Validate start with file name
                    c.Value.StartsWith(_jacTravelXmlLiveSettings.PropertyFilePrefix)
                    )
                    .Select(fileInDir =>
                  {
                      var filename = Path.GetFileNameWithoutExtension(fileInDir.Value);
                      //Extracting language name from file name
                      var languageName = filename.Right(_jacTravelXmlLiveSettings.LanguageCodeLength);

                      //Validating the language against the Sunhotels language list
                      if (_jacTravelXmlLiveSettings.LanguageList.Language
                            .All(lan => lan.JacTravelCode.Trim() != languageName.Trim().ToLower()))
                          return true;

                      var language = _jacTravelXmlLiveSettings.LanguageList.Language
                          .First(lan => lan.JacTravelCode.Trim() == languageName.Trim().ToLower());

                      string filepath = string.Join("/", ".", _jacTravelXmlLiveSettings.TempFolderName, configuration.ProviderName, fileInDir.Value);
                      ftpCom.DownloadFile(fileInDir.Key, filepath);
                      files.Add(language, filepath);

                      return true;
                  }).ToArray();

                Log(ILoggerStaticDataSeverity.Info, string.Join(": ", configuration.ProviderName, "Total number of static data files downloaded", files.Count));
                return files;
            }
            catch (Exception ex)
            {
                Log(ILoggerStaticDataSeverity.Error, string.Join(": ", configuration.ProviderName, "Error occurred while downloading static data files from FTP server. Error", ex.Message));
                return null;
            }
        }

        /// <summary>
        /// Create DataTable From Csv file
        /// </summary>
        /// <param name="csvFilePath">csvFile path</param>
        /// <returns></returns>
        private DataTable CreateDataTableFromCsv(string csvFilePath)
        {
            try
            {
                CSVData importCsv = new CSVData
                {
                    SEPARATOR_CHAR = '|'
                };
                return importCsv.Read(csvFilePath);
            }
            catch (Exception ex)
            {
                Log(ILoggerStaticDataSeverity.Error, string.Join(string.Empty, configuration.ProviderName, ": Error occurred while reading CSV file to DataTable at location: ", csvFilePath, ". Error: ", ex.Message));
                return null;
            }
        }

        #endregion
    }
}
