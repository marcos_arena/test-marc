﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace SunHotels.Providers
{
    [Serializable]
    [XmlRoot(ElementName = "Language")]
    public class Language
    {
        [XmlElement(ElementName = "ShCode")]
        public string ShCode { get; set; }
        [XmlElement(ElementName = "JacTravelCode")]
        public string JacTravelCode { get; set; }
    }
    [Serializable]
    [XmlRoot(ElementName = "LanguageList")]
    public class LanguageList
    {
        [XmlElement(ElementName = "Language")]
        public List<Language> Language { get; set; }
    }

    [Serializable]
    [XmlRoot(ElementName = "JacTravelXMLLiveSettings")]
    public class JacTravelXmlLiveSettings
    {
        public bool DeleteFileAfterProcessing { get; set; }
        public int ImageHeight { get; set; }
        public int ImageWidth { get; set; }
        [XmlElement(ElementName = "LanguageList")]
        public LanguageList LanguageList { get; set; }
        public string FileExtensions { get; set; }
        public string PropertyFilePrefix { get; set; }
        public int LanguageCodeLength { get; set; }
        public bool AddThumbnail { get; set; }
        public int GeoCodeDecimals { get; set; }
        public string HeadLineSeparator { get; set; }
        public int HeadLineCount { get; set; }
        public string SettingsFileName { get; set; }
        public string IdentifierType { get; set; }
        public string TempFolderName { get; set; }
    }
}
