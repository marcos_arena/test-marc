using System;
using System.Collections.Generic;
using System.Text;
using SunHotels.XML.Data;
using System.Xml;
using SunHotels.XML.HelpClasses;
using System.Globalization;
using SunHotels.XML;

namespace SunHotels.Providers.Data
{
    public class EurotoursHotel : SunHotels.XML.Data.Hotel
    {
        public SortedDictionary<DateTime, XmlDocument> Contracts = new SortedDictionary<DateTime, XmlDocument>();
        string xmlHeader = "request=<?xml version=\"1.0\" encoding=\"ISO-8859-1\" ?>";

        public XmlDocument GetCurrentContract(DateTime date)
        {
            XmlDocument currentContract = null;

            foreach (DateTime contractStartDate in Contracts.Keys)
            {
                if (contractStartDate <= date)
                {
                    currentContract = Contracts[contractStartDate];
                }
                else
                {
                    break;
                }
            }

            return currentContract;
        }


        public void LoadAllContracts(DateTime startDate, string url, string UserId, int NrDaysInCache, List<DateTime> seasonStartDates, ref int requestCounter)
        {
            XMLCommunication xmlCom = new XMLCommunication();
            xmlCom.RequestUrl = url + "/ContractWebRequest"; // "https://wwwet.eurotours.at/xml/ContractWebRequest";
            DateTime date = startDate;
            bool isFirstDate = true;

            foreach (var seasonStartDate in seasonStartDates)
            {
                if (seasonStartDate > date)
                {
                    date = seasonStartDate;
                }

                if (!isFirstDate && date == startDate)
                {
                    continue;
                }

                if (date > startDate.AddDays(NrDaysInCache))
                {
                    break;
                }

                string strReqContractRates = string.Format(@"<ETContractRatesRQ> 
                                                                        <Client>{0}</Client> 
                                                                        <LanguageCode>en</LanguageCode> 
                                                                        <ArrivalDate>{1}</ArrivalDate> 
                                                                        <ContractIdentification> 
                                                                        <ObjectCode>{2}</ObjectCode> 
                                                                        <ServiceType>{3}</ServiceType> 
                                                                        <ProductCode></ProductCode> 
                                                                        <SupplierCode></SupplierCode> 
                                                                        <IndividualOrGeneral>false</IndividualOrGeneral> 
                                                                        </ContractIdentification> 
                                                                        <RoomType></RoomType> 
                                                                     </ETContractRatesRQ> "
                                                    , UserId, date.ToString("dd.MM.yyyy"), this.Id, this.Tag);
                XmlDocument xmlContractRates = xmlCom.Run(xmlHeader + strReqContractRates);

                requestCounter++;

                this.Contracts.Add(date, xmlContractRates);

                if (isFirstDate)
                {
                    this.Extra = xmlContractRates;
                }

                isFirstDate = false;
            }
        }


        private DateTime? LastContractDate(XmlDocument contract)
        {
            DateTime? lastDate = null;
            XmlNodeList roomNodes = contract.SelectNodes("ETContractRatesRS/RatesList/Rate");
            foreach (XmlNode roomNode in roomNodes)
            {
                DateTime tempDate = DateTime.ParseExact(XmlHelper.GetString(roomNode, "ValidThru"), "dd.MM.yyyy", CultureInfo.InvariantCulture);
                if (lastDate == null)
                {
                    lastDate = tempDate;
                }

                if (lastDate.HasValue && tempDate > lastDate)
                {
                    lastDate = tempDate;
                }
            }

            return lastDate;
        }




        /*public void CreateDiscountRoomTypes(Root root)
        {
            //createDiscountRooms(root, this.rooms);

            foreach (RoomGroup rg in this.RoomGroups.Values)
            {
              //  createDiscountRooms(root, rg.Rooms);
            }
        }*/

        //private Dictionary<string, Room> createDiscountRooms(Root root, Dictionary<string, Room> rooms)
        //{
        //    Dictionary<string, Room> tempRoomList = new Dictionary<string, Room>();
        //    string roomTypeId = "";
        //    foreach (Room room in rooms.Values)
        //    {
        //        foreach (DateTime date in room.AvailabilityPeriods.Keys)
        //        {
        //            AvailabilityPeriod ap = room.AvailabilityPeriods[date];

        //            // Check if there is any durationreduction for the period
        //            for (DateTime dt = ap.DateFrom.Date; dt <= ap.DateTo.Date; dt = dt.AddDays(1))
        //            {
        //                List<XmlNode> durationReductionNodes = DurationReduction(dt.Date, this, room.RoomId, ap.Pricing_BookableBoardId);
        //                if (durationReductionNodes.Count > 0)
        //                {
        //                    foreach (XmlNode durationNode in durationReductionNodes)
        //                    {
        //                        int durationFrom = Int32.Parse(XmlHelper.GetString(durationNode, "DurationFrom"));
        //                        DateTime validFrom = DateTime.ParseExact(XmlHelper.GetString(durationNode, "ValidFrom"), "dd.MM.yyyy", CultureInfo.InvariantCulture);
        //                        DateTime validThru = DateTime.ParseExact(XmlHelper.GetString(durationNode, "ValidThru"), "dd.MM.yyyy", CultureInfo.InvariantCulture);


        //                        roomTypeId = room.TypeId + "_Discount" + durationFrom.ToString();

        //                        // Check if roomtype exists
        //                        RoomType rt = null;

        //                        if (!root.RoomTypes.ContainsKey(roomTypeId))
        //                        {
        //                            rt = new RoomType();
        //                            rt.MinimumStay = durationFrom;
        //                            rt.Beds = root.RoomTypes[room.TypeId].Beds;
        //                            rt.Bookable_Id = root.RoomTypes[room.TypeId].Bookable_Id;
        //                            rt.Description = root.RoomTypes[room.TypeId].Description;
        //                            rt.ExtraBeds = root.RoomTypes[room.TypeId].ExtraBeds;
        //                            rt.IsWeeklyStay = ap.IsWeeklyStay;
        //                            rt.Type_Id = roomTypeId;
        //                            root.RoomTypes.Add(roomTypeId, rt);
        //                        }
        //                        else
        //                        {
        //                            rt = root.RoomTypes[roomTypeId];
        //                        }

        //                        Room currentRoom = null;
        //                        if (!tempRoomList.ContainsKey(roomTypeId))
        //                        {
        //                            currentRoom = new Room();
        //                            currentRoom.RoomId = room.RoomId;
        //                            currentRoom.TypeId = roomTypeId;
        //                            currentRoom.Description = room.Description;
        //                            tempRoomList.Add(roomTypeId, currentRoom);
        //                        }
        //                        else
        //                        {
        //                            currentRoom = tempRoomList[roomTypeId];
        //                        }


        //                        // Check if AP is within current contract-span, otherwise shorten it to same lenght


        //                        AvailabilityPeriod currentAp = null;
        //                        if (currentRoom.AvailabilityPeriods.ContainsKey(date))
        //                        {
        //                            currentAp = currentRoom.AvailabilityPeriods[date];
        //                        }

        //                        if (ap.DateTo.Date <= validThru.Date)
        //                        {
        //                            if (currentAp == null)
        //                            {
        //                                currentAp = ap.Clone();
        //                                currentRoom.AvailabilityPeriods.Add(date, currentAp);
        //                            }

        //                            currentAp.DateTo = dt.Date;

        //                        }
        //                    }
        //                }
        //            }
        //        }
        //    }

        //    return tempRoomList;
        //}

        public List<XmlNode> DurationReduction(DateTime date, string RoomType, string MealType)
        {
            XmlDocument xmlDocDurationReduction = this.Extra as XmlDocument;
            XmlNodeList durationRelatedReductions = xmlDocDurationReduction.SelectNodes("ETContractRatesRS/DurationRelatedReductionsList/DurationRelatedReduction");
            List<XmlNode> nodes = new List<XmlNode>();
            foreach (XmlNode node in durationRelatedReductions)
            {
                DateTime validFrom = DateTime.ParseExact(XmlHelper.GetString(node, "ValidFrom"), "dd.MM.yyyy", CultureInfo.InvariantCulture);
                DateTime validThru = DateTime.ParseExact(XmlHelper.GetString(node, "ValidThru"), "dd.MM.yyyy", CultureInfo.InvariantCulture);
                string roomType = XmlHelper.GetString(node, "RoomType");
                string mealType = XmlHelper.GetString(node, "MealType");

                if (validFrom <= date && date <= validThru && (roomType == "" || roomType == RoomType) && (mealType == "" || mealType == MealType))
                {
                    nodes.Add(node.Clone());
                }
            }

            return nodes;
        }
    }
}
