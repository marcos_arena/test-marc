using System;
using System.Collections.Generic;
using System.Text;
using SunHotels.XML;
using SunHotels.XML.HelpClasses;
using SunHotels.XML.Data;
using System.Xml;
using System.Globalization;
using System.IO;
using System.Text.RegularExpressions;
using SunHotels.Providers.Data;
using SunHotels.Export;
using System.Linq;
using Sunhotels.Export;

namespace SunHotels.Providers
{


    public class Eurotours : XMLProvider
    {
        XMLCommunication xmlCom = new XMLCommunication();
        string xmlHeader = "request=<?xml version=\"1.0\" encoding=\"ISO-8859-1\" ?>";
        private bool DEBUG = false;
        Dictionary<string, object> missingRoomTypes = new Dictionary<string, object>();

        private Dictionary<string, XmlNode> _cachedRoomTypeNodes = new Dictionary<string, XmlNode>();
        private List<DateTime> _seasonStartDates = new List<DateTime>();
        private Dictionary<string, List<string>> _allHotels = new Dictionary<string, List<string>>();

        private int _requestCounter = 0;

        public Eurotours()
        {
        }

        public Eurotours(Configuration config)
            :
            base(config)
        {

        }


        int skippedCouseDeparturePossible = 0;
        List<string> addedLocations = new List<string>();


        protected override void getData(Root root)
        {
            Console.WriteLine("Start: " + DateTime.Now.ToString());
            Console.WriteLine("### Caching Areas #######################################");
            string filename = string.Format("{0}eurotours {1}.log", configuration.LogOutput, DateTime.Now.ToString("yyyyMMdd HHmmss"));
            StreamWriter log = new StreamWriter(filename);
            log.AutoFlush = true;
            try
            {
                CacheSeasonStartDates();

                CacheRoomTypes();

                /////////////////////////////
                // Areas
                /////////////////////////////

                xmlCom = new XMLCommunication();
                xmlCom.RequestUrl = configuration.WebServiceUrl + "/AreaWebRequest"; // "https://wwwet.eurotours.at/xml/AreaWebRequest";
                Console.WriteLine("Url called: " + xmlCom.RequestUrl);

                string strReqArea = string.Format(@"<ETAreaListRQ>
                                <Client>{0}</Client>
                                <LanguageCode>en</LanguageCode>
                             </ETAreaListRQ>", configuration.UserId);

                XmlDocument xmlDocAreas = xmlCom.Run(xmlHeader + strReqArea);

                _requestCounter++;

                if (xmlDocAreas.GetElementsByTagName("ETErrorRS").Count > 0)
                {
                    throw new Exception("XmlErrorResponse: " + xmlDocAreas.InnerXml);
                }

                XmlNodeList areas = xmlDocAreas.GetElementsByTagName("Area");
                foreach (XmlNode area in areas)
                {
                    Place pArea = new Place();
                    string placeId = area.SelectSingleNode("AreaCode").InnerText;

                    pArea.Id = placeId + " Area";
                    Console.WriteLine(string.Format("  Area: {0}", pArea.Id));

                    // 2017-07-17 - LocationDetail doesn't exist anymore??

                    //xmlCom = new XMLCommunication();
                    //xmlCom.RequestUrl = configuration.WebServiceUrl + "/LocationDetailWebRequest"; // "https://wwwet.eurotours.at/xml/LocationDetailWebRequest";
                    //string strReqLocationDetail = string.Format(@"<ETLocationDetailsRQ>
                    //                                    <Client>{0}</Client>
                    //                                    <LanguageCode>en</LanguageCode>
                    //                                    <LocationCode>{1}</LocationCode>
                    //                                    </ETLocationDetailsRQ>", configuration.UserId, placeId);



                    //XmlDocument xmlDocLocationDetail = xmlCom.Run(xmlHeader + strReqLocationDetail);

                    //_requestCounter++;

                    //XmlNodeList details = xmlDocLocationDetail.SelectNodes("TextList/Text");
                    pArea.Description = area.SelectSingleNode("AreaName").InnerText;

                    //foreach (XmlNode textNode in details)
                    //{
                    //    if (XmlHelper.GetString(textNode, "Topic") == "General information")
                    //    {
                    //        pArea.Description = XmlHelper.GetString(textNode, "FullText");
                    //    }
                    //}

                    root.Places.Add(pArea);

                    getLocations(pArea, log);
                }

                RemoveDuplicates(log);

                log.Close();

                string missingRoomTypesLog = string.Format("{0}eurotours missing rooms types.log", configuration.LogOutput);
                if (missingRoomTypes.Count > 0)
                {

                    using (var rtLog = new StreamWriter(missingRoomTypesLog))
                    {
                        foreach (var item in missingRoomTypes.Keys)
                        {
                            rtLog.WriteLine(item);
                        }
                    }
                }
                else
                {
                    File.Delete(missingRoomTypesLog);
                }

                Console.WriteLine("End: " + DateTime.Now.ToString());
                Console.WriteLine("Requests to EuroTours made: " + _requestCounter);
            }
            catch (Exception ex)
            {
                log.Write(ex);
                log.Dispose();
                throw;
            }
        }

        private void CacheSeasonStartDates()
        {
            xmlCom = new XMLCommunication();

            xmlCom.RequestUrl = configuration.WebServiceUrl + "/SeasonWebRequest"; // "https://wwwet.eurotours.at/xml/SeasonWebRequest";
            string strReqRoomType = string.Format(@"<ETSeasonRQ>
                                                    <Client>{0}</Client>
                                                    <LanguageCode>en</LanguageCode>
                                                    </ETSeasonRQ>", configuration.UserId);

            XmlDocument xmlDocSeasons = xmlCom.Run(xmlHeader + strReqRoomType);

            _requestCounter++;

            foreach (XmlNode seasonNode in xmlDocSeasons.GetElementsByTagName("Season"))
            {
                var seasonCode = XmlHelper.GetString(seasonNode, "SeasonCode");
                int tmpCode;

                if (Int32.TryParse(seasonCode, out tmpCode))
                {
                    var startDate = DateTime.ParseExact(XmlHelper.GetString(seasonNode, "SeasonFrom"), "dd.MM.yyyy", CultureInfo.InvariantCulture);
                    if (!_seasonStartDates.Contains(startDate))
                    {
                        _seasonStartDates.Add(startDate);
                    }
                }
            }

            _seasonStartDates = _seasonStartDates.OrderBy(startDate => startDate).ToList();
        }

        private void CacheRoomTypes()
        {
            xmlCom = new XMLCommunication();

            xmlCom.RequestUrl = configuration.WebServiceUrl + "/RoomTypeSpecWebRequest"; // "https://wwwet.eurotours.at/xml/RoomTypeSpecWebRequest";
            string strReqRoomType = string.Format(@"<ETRoomTypeSpecRQ>
                                                    <Client>{0}</Client>
                                                    <LanguageCode>en</LanguageCode>
                                                    <RoomType></RoomType>
                                                    </ETRoomTypeSpecRQ>", configuration.UserId);

            XmlDocument xmlDocRoomTypes = xmlCom.Run(xmlHeader + strReqRoomType);

            _requestCounter++;

            foreach (XmlNode roomTypeNode in xmlDocRoomTypes.GetElementsByTagName("RoomType"))
            {
                string roomTypeCode = roomTypeNode.SelectSingleNode("RoomTypeCode").InnerText;

                if (!_cachedRoomTypeNodes.ContainsKey(roomTypeCode))
                {
                    _cachedRoomTypeNodes.Add(roomTypeCode, roomTypeNode);
                }
            }
        }


        protected void getLocations(Place place, StreamWriter log)
        {
            /////////////////////////////
            // Locations
            /////////////////////////////

            string placeId = place.Id.Substring(0, place.Id.Length - 5);

            xmlCom = new XMLCommunication();
            xmlCom.RequestUrl = configuration.WebServiceUrl + "/LocationWebRequest"; //  "https://wwwet.eurotours.at/xml/LocationWebRequest";
            string strReqLocation = string.Format(@"<ETLocationListRQ>
                                       <Client>{0}</Client>
                                       <LanguageCode>en</LanguageCode>
                                       <AreaCode>{1}</AreaCode>
                                    </ETLocationListRQ>", configuration.UserId, placeId);

            XmlDocument xmlDocLocations = xmlCom.Run(xmlHeader + strReqLocation);

            _requestCounter++;

            XmlNodeList locations = xmlDocLocations.GetElementsByTagName("Location");
            foreach (XmlNode location in locations)
            {
                Place pLocation = new Place();


                pLocation.Id = location.SelectSingleNode("LocationCode").InnerText;
                pLocation.Description = location.SelectSingleNode("LocationName").InnerText;
                Console.WriteLine(string.Format("    Location: {0}", pLocation.Id));

                string place_location = string.Format("{0}.{1}", place, pLocation.Id);

                if (!addedLocations.Contains(place_location))
                {
                    addedLocations.Add(place_location);
                    place.Places.Add(pLocation);

                    // DEBUG-CODE

                    if (this.DEBUG && pLocation.Id != "Berlin") continue;

                    for (DateTime date = DateTime.Today; date <= DateTime.Today.AddDays(configuration.NrDaysInCache); date = date.AddDays(configuration.DaysToGetPerFetch))
                    {
                        getHotels(pLocation, date, log);
                    }

                    foreach (EurotoursHotel hotel in pLocation.Hotels.Values)
                    {
                        Optimizer.OptimizeHotel(root, hotel);
                    }
                }
            }
        }

        protected void getHotels(Place place, DateTime date, StreamWriter log)
        {
            ////////////////////////////
            // Hotels
            ////////////////////////////
            Console.WriteLine(string.Format("      Startdate: {0}", date.ToShortDateString()));
            int daysToFetch = configuration.DaysToGetPerFetch;
            if (DateTime.Today.AddDays(configuration.NrDaysInCache) < date.AddDays(daysToFetch))
            {
                daysToFetch = ((TimeSpan)(DateTime.Today.AddDays(configuration.NrDaysInCache) - date)).Days;
            }

            xmlCom = new XMLCommunication();
            xmlCom.RequestUrl = configuration.WebServiceUrl + "/SearchWebRequest"; // "https://wwwet.eurotours.at/xml/SearchWebRequest";
            string strReqHotel = string.Format(@"<ETSearchObjectRQ>
                                <Client>{0}</Client>
                                <LanguageCode>en</LanguageCode>
                                <LanguageSelect>true</LanguageSelect>
                                <ArrivalDate>{1}</ArrivalDate>
                                <Duration>{2}</Duration>
                                <Availability></Availability>
                                <ObjectCode></ObjectCode>
                                <LocationCode>{3}</LocationCode>
                                <AreaCode></AreaCode>
                                <ThemeCode></ThemeCode>
                                <Category></Category>
                                <ServiceType></ServiceType>
                                <ShowRate>True</ShowRate>
                                <ExpandSearch></ExpandSearch>
                                <Address>true</Address>
                                </ETSearchObjectRQ>", configuration.UserId, date.ToString("dd.MM.yyyy"), daysToFetch, place.Id);


            try
            {
                XmlDocument xmlDocHotels = xmlCom.Run(xmlHeader + strReqHotel);

                _requestCounter++;

                XmlNodeList hotels = xmlDocHotels.GetElementsByTagName("Object");
                foreach (XmlNode hotel in hotels)
                {
                    if (hotel.SelectSingleNode("Rooms/Room/DateList/Date[DeparturePossible='false' and MinimumStay/Days != '7']") != null)
                    {
                        // This hotel have a fixed departure day witch isent implemented for now. Lest skip this.
                        skippedCouseDeparturePossible++;
                        continue;
                    }

                    EurotoursHotel h = null;
                    try
                    {
                        string hotelId = XmlHelper.GetString(hotel, "ContractIdentification/ObjectCode");
                        string serviceType = XmlHelper.GetString(hotel, "ContractIdentification/ServiceType");
                        Console.WriteLine(string.Format("        Hotel: {0}", hotelId));

                        // DEBUG-CODE
                        if (this.DEBUG && hotelId != "GRANDCI BERLIN") continue;

                        if (XmlHelper.GetString(hotel, "ContractIdentification/ProductCode") != string.Empty ||
                            XmlHelper.GetString(hotel, "ContractIdentification/SupplierCode") != string.Empty)
                            continue;

                        if (place.Hotels.ContainsKey(hotelId) && place.Hotels[hotelId].Tag != serviceType)
                            continue;


                        if (!place.Hotels.ContainsKey(hotelId))
                        {
                            h = new EurotoursHotel();

                            h.Tag = serviceType;

                            h.Id = XmlHelper.GetString(hotel, "ContractIdentification/ObjectCode");
                            h.Name = XmlHelper.GetString(hotel, "ObjectName");
                            h.Description = string.Format("{1}{0}{0}{2}", Environment.NewLine, (serviceType == "APPART" ? "Import this hotel as an APARTMENT hotel!" : "Import this hotel as a NORMAL hotel!"), XmlHelper.GetString(hotel, "ShortDescription"));
                            h.PlaceId = place.Id;
                            h.Position_Latitude = XmlHelper.GetString(hotel, "MapY");
                            h.Position_Longitude = XmlHelper.GetString(hotel, "MapX");
                            h.Classification = XmlHelper.GetString(hotel, "Category");

                            h.Adress_Street1 = XmlHelper.GetString(hotel, "Address/Street");
                            h.Adress_Zipcode = XmlHelper.GetString(hotel, "Address/Zip");
                            h.Adress_City = XmlHelper.GetString(hotel, "Address/Location");
                            h.Phone = XmlHelper.GetString(hotel, "Address/Tel");
                            h.Adress_Country = XmlHelper.GetString(hotel, "Address/Nation");
                            h.Email = XmlHelper.GetString(hotel, "Address/Email");
                            h.LoadAllContracts(date, configuration.WebServiceUrl, configuration.UserId, configuration.NrDaysInCache, _seasonStartDates, ref _requestCounter);

                            ////////////////////////////
                            // Features
                            ////////////////////////////

                            if (XmlHelper.GetString(hotel, "HasRestaurant").ToLower() == "true")
                            {
                                h.Features.Add(new Feature() { Name = "restaurant", Value = "true" });
                            }

                            if (XmlHelper.GetString(hotel, "HasElevator").ToLower() == "true")
                            {
                                h.Features.Add(new Feature() { Name = "elevator", Value = "true" });
                            }

                            if (XmlHelper.GetString(hotel, "HasPool").ToLower() == "true")
                            {
                                h.Features.Add(new Feature() { Name = "pool", Value = "true" });
                            }


                            ////////////////////////////
                            // Distances
                            ////////////////////////////

                            if (XmlHelper.GetString(hotel, "LocatedInCenter") == "true")
                            {
                                h.Distances.Add(new Hotel.Distance("center", 0));
                            }

                            place.Hotels.Add(hotelId, h);

                            // add to list, to keep track of all hotels in the import
                            if (_allHotels.ContainsKey(hotelId))
                            {
                                _allHotels[hotelId].Add(string.Format("{0} ({1})", h.Name, h.PlaceId));
                            }
                            else
                            {
                                _allHotels.Add(hotelId, new List<string>() { string.Format("{0} ({1})", h.Name, h.PlaceId) });
                            }
                        }
                        else
                        {
                            h = place.Hotels[hotelId] as EurotoursHotel;
                        }


                        ////////////////////////////
                        // Rooms
                        ////////////////////////////

                        Dictionary<string, List<APLight>> poolRooms = new Dictionary<string, List<APLight>>();

                        XmlNodeList rooms = hotel.SelectNodes("Rooms/Room");

                        if (rooms.Count == 1 && XmlHelper.GetString(rooms[0], "RoomType") == "POOL")
                        {
                            // Do a extra request for the roomtypes. This is since POOL is a collection of roomtypes.
                            //                            xmlCom = new XMLCommunication();

                            //                            xmlCom.RequestUrl = "https://wwwet.eurotours.at/xml/ContractWebRequest";
                            //                            string strReqRoomType = string.Format(@"<ETContractRatesRQ> 
                            //                                                                        <Client>{0}</Client> 
                            //                                                                        <LanguageCode>en</LanguageCode> 
                            //                                                                        <ArrivalDate>{1}</ArrivalDate> 
                            //                                                                        <ContractIdentification> 
                            //                                                                        <ObjectCode>{2}</ObjectCode> 
                            //                                                                        <ServiceType>{3}</ServiceType> 
                            //                                                                        <ProductCode></ProductCode> 
                            //                                                                        <SupplierCode></SupplierCode> 
                            //                                                                        <IndividualOrGeneral>false</IndividualOrGeneral> 
                            //                                                                        </ContractIdentification> 
                            //                                                                        <RoomType></RoomType> 
                            //                                                                     </ETContractRatesRQ> "
                            //                                                                , configuration.UserId, date.ToString("dd.MM.yyyy"), h.Id, h.Tag);

                            //                            XmlDocument xmlDocContractPoolRooms = xmlCom.Run(xmlHeader + strReqRoomType);

                            //XmlDocument xmlDocContract = h.Extra as XmlDocument;



                            foreach (XmlDocument xmlDocContract in h.Contracts.Values)
                            {

                                XmlNodeList poolRoomNodes = xmlDocContract.SelectNodes("ETContractRatesRS/RatesList/Rate");
                                foreach (XmlNode poolRoomNode in poolRoomNodes)
                                {
                                    string rt = XmlHelper.GetString(poolRoomNode, "RoomType");
                                    if (!poolRooms.ContainsKey(rt))
                                    {
                                        poolRooms.Add(rt, new List<APLight>());
                                    }

                                    DateTime validFrom = DateTime.ParseExact(XmlHelper.GetString(poolRoomNode, "ValidFrom"), "dd.MM.yyyy", CultureInfo.InvariantCulture);
                                    DateTime validTo = DateTime.ParseExact(XmlHelper.GetString(poolRoomNode, "ValidThru"), "dd.MM.yyyy", CultureInfo.InvariantCulture);

                                    int priceMethod = Int32.Parse(XmlHelper.GetString(poolRoomNode, "PriceMethod"));
                                    Decimal baseRate = Decimal.Parse(XmlHelper.GetString(poolRoomNode, "BaseRate"), CultureInfo.InvariantCulture);

                                    poolRooms[rt].Add(new APLight(validFrom, validTo, priceMethod, baseRate));
                                }
                            }

                            RoomGroup roomGroup = null;

                            if (!h.RoomGroups.ContainsKey("POOL"))
                            {
                                roomGroup = new RoomGroup();
                                roomGroup.RoomGroupId = "POOL";
                                h.RoomGroups.Add("POOL", roomGroup);
                            }
                            else
                            {
                                roomGroup = h.RoomGroups["POOL"];
                            }


                            foreach (string roomKey in poolRooms.Keys)
                            {
                                XmlNode room = rooms[0];

                                room.SelectSingleNode("RoomType").InnerText = roomKey;

                                XmlNodeList DateList = room.SelectNodes("DateList/Date");
                                DateTime dateCount = date;

                                for (int listCount = 0; listCount < configuration.DaysToGetPerFetch; listCount++)
                                {
                                    XmlNode datenode = DateList[listCount];
                                    APLight currentAp = null;

                                    foreach (APLight ap in poolRooms[roomKey])
                                    {
                                        if (ap.ValidFrom.Date <= dateCount.Date && dateCount.Date <= ap.ValidTo)
                                        {
                                            currentAp = ap;
                                            break;
                                        }
                                    }

                                    if (currentAp != null)
                                    {
                                        datenode.SelectSingleNode("Price").InnerText = currentAp.BaseRate.ToString(CultureInfo.InvariantCulture);

                                    }
                                    else
                                    {
                                        datenode.SelectSingleNode("Availability").InnerText = "0";
                                    }

                                    dateCount = dateCount.AddDays(1);
                                }

                                getRoom(date, roomGroup, h, hotel, room, log);

                            }



                        }
                        else
                        {
                            // Normal rooms
                            foreach (XmlNode room in rooms)
                            {
                                getRoom(date, null, h, hotel, room, log);
                            }
                        }
                        //Console.WriteLine(h.Name + "(" + h.Id + ") ");
                    }
                    catch (Exception e)
                    {
                        log.WriteLine("{0}: Error - getHotels - Place{1}\t{2}\t{3}\n   EXCEPTION:{4}", DateTime.Now, place.Id, h != null ? h.Id : "", date.ToString("yyyy-MM-dd"), e.ToString());
                    }
                }
            }
            catch (Exception e)
            {
                log.WriteLine("{0}: Error - getHotel:Place - {1}\t{2}\n    EXCEPTION:{3}", DateTime.Now, place.Id, date.ToString("yyyy-MM-dd"), e.ToString());
                //throw new Exception("Error in getHotels", e);
            }
        }


        private XmlNode ChildReduction(DateTime date, EurotoursHotel h, string RoomType)
        {
            // Do a extra request for the roomtypes. This is since POOL is a collection of roomtypes.

            //XmlDocument xmlDocAgeReductions = h.Extra as XmlDocument;

            XmlDocument xmlDocAgeReductions = h.GetCurrentContract(date);

            if (xmlDocAgeReductions != null)
            {

                XmlNodeList ageRelatedReductionNodes = xmlDocAgeReductions.SelectNodes("ETContractRatesRS/AgeRelatedReductionsList/AgeRelatedReduction");
                int diff = 1000;
                XmlNode bestMatch = null;
                foreach (XmlNode ageRelatedReductionNode in ageRelatedReductionNodes)
                {
                    DateTime validFrom = DateTime.ParseExact(XmlHelper.GetString(ageRelatedReductionNode, "ValidFrom"), "dd.MM.yyyy", CultureInfo.InvariantCulture);
                    DateTime validThru = DateTime.ParseExact(XmlHelper.GetString(ageRelatedReductionNode, "ValidThru"), "dd.MM.yyyy", CultureInfo.InvariantCulture);
                    string roomType = XmlHelper.GetString(ageRelatedReductionNode, "RoomType");

                    if (validFrom <= date.Date && date.Date <= validThru && (roomType == "" || roomType == RoomType))
                    {
                        if (XmlHelper.GetString(ageRelatedReductionNode, "AgeThru") != "" && Int32.Parse(XmlHelper.GetString(ageRelatedReductionNode, "AgeThru")) >= 11 || (bestMatch != null && (Int32.Parse(XmlHelper.GetString(bestMatch, "AgeThru")) - 11) < diff))
                        {
                            bestMatch = ageRelatedReductionNode;
                            diff = Int32.Parse(XmlHelper.GetString(bestMatch, "AgeThru")) - 11;
                        }
                    }
                }
                return bestMatch;
            }

            return null;

        }

        protected bool getRoom(DateTime date, RoomGroup roomGroup, EurotoursHotel h, XmlNode hotel, XmlNode room, StreamWriter log)
        {
            Room r;
            string bookableId = XmlHelper.GetString(room, "RoomType");

            int priceMethod = Int32.Parse(XmlHelper.GetString(room, "PriceMethod"));

            RoomType roomType = null;
            if (!root.RoomTypes.ContainsKey(bookableId))
            {

                roomType = new RoomType();
                roomType.Bookable_Id = bookableId;
                roomType.Type_Id = bookableId;

                XmlNode roomTypeNode;

                if (!_cachedRoomTypeNodes.TryGetValue(roomType.Bookable_Id, out roomTypeNode))
                {
                    // could not find the room type so stop importing this room
                    return false;
                }

                int minimumOccupancy = Int32.Parse(XmlHelper.GetString(roomTypeNode, "MinimumOccupancy"));
                int maximumOccupancy = Int32.Parse(XmlHelper.GetString(roomTypeNode, "MaximumOccupancy"));

                if (maximumOccupancy < 1)
                {
                    return false;  // Not interessted in this room
                }

                int maximumAdults = Int32.Parse(XmlHelper.GetString(roomTypeNode, "MaximumAdults"));
                int maximumChild = Int32.Parse(XmlHelper.GetString(roomTypeNode, "MaximumAdults"));

                string bedding;

                if (priceMethod == 2 || priceMethod == 3)
                {
                    bedding = String.Format("{0}+0", maximumOccupancy);
                }
                else
                {
                    bedding = getRoomType(minimumOccupancy, maximumOccupancy, maximumAdults, maximumChild);

                    if (bedding == null)
                    {
                        Console.WriteLine(string.Format("          No SH bedding for roomType: {0}", roomType.Bookable_Id));
                        missingRoomTypes[String.Format("{0},{1},{2},{3}", minimumOccupancy, maximumOccupancy, maximumAdults, maximumChild)] = 1;
                        //log.WriteLine("No SH bedding for roomType: {0}", roomType.Bookable_Id));
                        return false;
                    }
                }

                roomType.Beds = Convert.ToInt32(bedding.Split('+')[0]);
                roomType.ExtraBeds = Convert.ToInt32(bedding.Split('+')[1]);

                //roomType.Beds = (minimumOccupancy <= maximumAdults ? minimumOccupancy : maximumAdults);
                //roomType.ExtraBeds = ((maximumOccupancy - roomType.Beds) <= maximumChild) ? maximumOccupancy - roomType.Beds : maximumChild;

                roomType.Description = XmlHelper.GetString(roomTypeNode, "RoomDescription");
                roomType.IsWeeklyStay = false;


                root.RoomTypes.Add(bookableId, roomType);
            }
            else
            {
                roomType = root.RoomTypes[bookableId];
            }



            //int priceMethod = Int32.Parse(XmlHelper.GetString(room, "PriceMethod"));

            XmlNodeList DateList = room.SelectNodes("DateList/Date");

            int availability = 0;
            string price = "0";

            DateTime dateCount = date;
            for (int listCount = 0; listCount < configuration.DaysToGetPerFetch; listCount++)
            {
                if (DateList[listCount] != null)
                {
                    XmlNode dateNode = DateList[listCount];

                    availability = Int32.Parse(XmlHelper.GetString(dateNode, "Availability"));
                    price = XmlHelper.GetString(dateNode, "Price");

                    int pos = price.LastIndexOfAny(new char[] { ',' });
                    if (pos >= (price.Length - 3))
                    {
                        price = price.Replace(".", "");
                        price = price.Replace(',', '.');
                    }

                    Decimal dPrice = Decimal.Parse(price, CultureInfo.InvariantCulture);

                    string hotelCurrency = XmlHelper.GetString(dateNode, "../../../../Currency");
                    if (hotelCurrency != root.ProviderDefinition.Currency)
                        dPrice = XmlHelper.ConvertCurrency(dPrice, hotelCurrency, root.ProviderDefinition.Currency);

                    Decimal dPriceExtrabed = 0;
                    Decimal dPriceExtrabedChild = 0;

                    switch (priceMethod)
                    {
                        case 0: // per person per night
                            dPrice = dPrice * roomType.Beds;
                            break;

                        case 2: // per unit per night
                            break;

                        case 3: // per unit per week
                            dPrice = dPrice / 7;
                            break;

                        case 4: // per person per week
                            dPrice = dPrice * roomType.Beds / 7;
                            break;

                        default:
                            log.WriteLine("Skipping unknown price method {0}", priceMethod);
                            return true;
                    }

                    dPriceExtrabed = dPriceExtrabedChild = dPrice / roomType.Beds;

                    XmlNode childReductionNode = ChildReduction(date, h, bookableId);


                    if (childReductionNode != null)
                    {
                        int reductionType = Int32.Parse(XmlHelper.GetString(childReductionNode, "ReductionMethod"));
                        Decimal reduction = Decimal.Parse(XmlHelper.GetString(childReductionNode, "Reduction"), CultureInfo.InvariantCulture);

                        switch (reductionType)
                        {
                            case 1: // Percent
                                dPriceExtrabedChild = dPriceExtrabed - (dPriceExtrabed * (reduction / 100 * -1));
                                break;

                            case 2: // Value
                                dPriceExtrabedChild = dPriceExtrabed - (reduction * -1);
                                break;
                        }
                    }




                    bool arrivalPossible = false;
                    if (XmlHelper.GetString(dateNode, "ArrivalPossible") != "")
                    {
                        arrivalPossible = Boolean.Parse(XmlHelper.GetString(dateNode, "ArrivalPossible"));
                    }


                    int minimumStay = 0;
                    if (XmlHelper.GetString(dateNode, "MinimumStay/Days") != "")
                    {
                        minimumStay = Int32.Parse(XmlHelper.GetString(dateNode, "MinimumStay/Days"));
                    }

                    bool isWeeklyStay = false;
                    if (XmlHelper.GetString(dateNode, "MinimumStay/IsMaximum") != "")
                    {
                        if (minimumStay == 7)
                        {
                            isWeeklyStay = true;
                        }
                    }

                    string currentRoomTypeId = bookableId;

                    if (roomGroup == null)
                    {
                        if (!h.Rooms.ContainsKey(currentRoomTypeId))
                        {
                            r = new Room();
                            r.RoomId = bookableId;
                            r.TypeId = currentRoomTypeId;
                            r.Description = XmlHelper.GetString(room, "RoomDescription");
                            h.Rooms.Add(currentRoomTypeId, r);
                        }
                        else
                        {
                            r = h.Rooms[currentRoomTypeId];
                        }
                    }
                    else
                    {
                        if (!roomGroup.Rooms.ContainsKey(currentRoomTypeId))
                        {
                            r = new Room();
                            r.RoomId = bookableId;
                            r.TypeId = currentRoomTypeId;
                            r.Description = XmlHelper.GetString(room, "RoomDescription");
                            roomGroup.Rooms.Add(currentRoomTypeId, r);
                        }
                        else
                        {
                            r = roomGroup.Rooms[currentRoomTypeId];
                        }
                    }


                    if (availability > 0)    //&& arrivalPossible && (!isWeeklyStay || (isWeeklyStay && minimumStay == 7)) 2008-11-23      // && !isMaximum && arrivalPossible) // Changed at 2007-09-28
                    {
                        if (!r.AvailabilityPeriods.ContainsKey(dateCount))
                        {
                            AvailabilityPeriod ap = new AvailabilityPeriod();
                            ap.AvailabilityAvailableUnits = (Int16)availability;
                            ap.DateFrom = dateCount;
                            ap.DateTo = dateCount;
                            ap.ReleaseDays = 7;

                            string tmpPrice = dPrice.ToString();
                            //ap.Pricing_BasePrice = dPrice.ToString().Replace(',', '.');
                            ap.Pricing_BasePrice = dPrice.ToString(CultureInfo.InvariantCulture);

                            ap.IsArrivalPossible = arrivalPossible;
                            ap.IsWeeklyStay = isWeeklyStay;
                            ap.MinimumStay = (Int16)minimumStay;

                            if (roomType.ExtraBeds > 0)
                            {
                                ap.Pricing_ExtrabedAdult = dPriceExtrabed.ToString(CultureInfo.InvariantCulture);
                                ap.Pricing_ExtrabedChild = dPriceExtrabedChild.ToString(CultureInfo.InvariantCulture);
                            }

                            ap.Pricing_BookableBoardId = XmlHelper.GetString(hotel, "MealType");



                            switch (ap.Pricing_BookableBoardId)
                            {

                                case "BB": // buffet breakfast
                                    ap.Pricing_BaseIncludedBoard = "breakfast";
                                    break;

                                case "VP": // fullboard
                                    ap.Pricing_BaseIncludedBoard = "full_board";
                                    break;

                                case "KF": // continental breakfast
                                    ap.Pricing_BaseIncludedBoard = "breakfast";
                                    break;

                                case "HP": // half board
                                    ap.Pricing_BaseIncludedBoard = "half_board";
                                    break;

                                case "AI": // all inclusive
                                    ap.Pricing_BaseIncludedBoard = "all_inclusive";
                                    break;

                                case "HB": // half board
                                    ap.Pricing_BaseIncludedBoard = "half_board";
                                    break;

                                case "FB": // buffet breakfast
                                    ap.Pricing_BaseIncludedBoard = "breakfast";
                                    break;

                                case "OV": // without catering
                                    ap.Pricing_BaseIncludedBoard = "none";
                                    break;

                                case "SN": // Snack
                                    ap.Pricing_BaseIncludedBoard = "none";
                                    break;

                                case "FS": // breakfast
                                    ap.Pricing_BaseIncludedBoard = "breakfast";
                                    break;

                                case "MI": // more included
                                    ap.Pricing_BaseIncludedBoard = "half_board";
                                    break;

                                default:
                                    log.WriteLine("Skipping unknown meal type {0} - Hotel {1} - Room {2} - Date {3}", ap.Pricing_BookableBoardId, h.Id, bookableId, ap.DateFrom);
                                    return true;
                            }

                            // Duration Discounts
                            List<XmlNode> reductionNodes = h.DurationReduction(ap.DateFrom, r.TypeId, ap.Pricing_BookableBoardId);
                            foreach (XmlNode reductionNode in reductionNodes)
                            {
                                int minimumDuration = Int32.Parse(XmlHelper.GetString(reductionNode, "DurationFrom"));
                                int durationControl = Int32.Parse(XmlHelper.GetString(reductionNode, "DurationControl"));
                                double reduction = Double.Parse(XmlHelper.GetString(reductionNode, "Reduction"), CultureInfo.InvariantCulture) * -1;

                                if (durationControl > 0)
                                {
                                }

                                int reductionMethod = Int32.Parse(XmlHelper.GetString(reductionNode, "ReductionMethod"));
                                switch (reductionMethod)
                                {
                                    case 0:    // Amount
                                        // I amount is negative here this is a price-upgrade
                                        if (reduction < 0)
                                        {
                                            ap.Pricing_BasePrice = (Double.Parse(ap.Pricing_BasePrice) + (reduction * -1)).ToString();
                                        }
                                        else
                                        {

                                            //ap.Discounts.Add(new Room.AvailabilityPeriod.Discount(Room.AvailabilityPeriod.DiscountType.Value, minimumDuration, Convert.ToDecimal(reduction), 0));
                                            ap.Discounts.TryAdd(Discount.ValueAmount(minimumDuration, Convert.ToDecimal(reduction)));
                                        }

                                        break;

                                    case 1:     // Percentage
                                        //ap.Discounts.Add(new Room.AvailabilityPeriod.Discount(Room.AvailabilityPeriod.DiscountType.Value, minimumDuration, 0, Convert.ToInt32(reduction)));
                                        ap.Discounts.TryAdd(Discount.ValuePercent(minimumDuration, Convert.ToInt32(reduction)));
                                        break;

                                    case 2:     // Days - (usual case)
                                        //ap.Discounts.Add(new Room.AvailabilityPeriod.Discount(Room.AvailabilityPeriod.DiscountType.PayStay, minimumDuration, Convert.ToDecimal(reduction),0)); 
                                        ap.Discounts.TryAdd(Discount.PayStay(minimumDuration, null, Convert.ToInt32(reduction)));
                                        break;
                                }
                            }

                            r.AvailabilityPeriods.Add(ap.DateFrom, ap);
                        }
                    }
                }
                else
                {
                    return false;
                }

                dateCount = dateCount.AddDays(1);
            }
            return true;
        }

        private string getRoomType(int minimumOccupancy, int maximumOccupancy, int maximumAdults, int maximumChild)
        {
            var xmlMappings = new XmlDocument();
            xmlMappings.Load(configuration.ExportConfigFileFullPath);

            XmlNodeList roomTypes = xmlMappings.SelectNodes("RoomMappings/RoomType");
            foreach (XmlNode roomType in roomTypes)
            {
                int miO = Int32.Parse(XmlHelper.GetString(roomType, "MinimumOccupancy"));
                int maO = Int32.Parse(XmlHelper.GetString(roomType, "MaximumOccupancy"));
                int maA = Int32.Parse(XmlHelper.GetString(roomType, "MaximumAdults"));
                int maC = Int32.Parse(XmlHelper.GetString(roomType, "MaximumChilds"));

                if (miO == minimumOccupancy &&
                   maO == maximumOccupancy &&
                   maA == maximumAdults &&
                   maC == maximumChild)
                {
                    return XmlHelper.GetString(roomType, "MapsTo");
                }

            }

            return null;
        }

        protected override void getProviderDefinition(Root root)
        {
            root.ProviderDefinition.Name = "Eurotours";
            root.ProviderDefinition.Currency = "EUR";
        }

        private void RemoveDuplicates(StreamWriter log)
        {
            // if any hotel occures twice, remove and add an error in the log
            if (_allHotels.Any(h => h.Value.Count > 1))
            {
                var places = root.Places.SelectMany(p => p.Places);
                foreach (var hotelObj in _allHotels.Where(h => h.Value.Count > 1))
                {
                    foreach (var pl in places.Where(p => p.Hotels.Any(h => h.Key == hotelObj.Key)))
                    {
                        // remove hotels 
                        pl.Hotels.Remove(hotelObj.Key);
                    }

                    log.WriteLine("{0}: Error - Duplicate hotel. ID: {1}. Name: {2}\n", DateTime.Now, hotelObj.Key, string.Join(", ", hotelObj.Value));
                }
            }
        }
    }

    public class APLight
    {
        private DateTime validFrom;

        public DateTime ValidFrom
        {
            get { return validFrom; }
            set { validFrom = value; }
        }

        private DateTime validTo;

        public DateTime ValidTo
        {
            get { return validTo; }
            set { validTo = value; }
        }

        private int priceMethod;

        public int PriceMethod
        {
            get { return priceMethod; }
            set { priceMethod = value; }
        }

        private decimal baseRate;

        public decimal BaseRate
        {
            get { return baseRate; }
            set { baseRate = value; }
        }



        public APLight(DateTime ValidFrom, DateTime ValidTo, int PriceMethod, Decimal BaseRate)
        {
            this.validFrom = ValidFrom;
            this.validTo = ValidTo;
            this.priceMethod = PriceMethod;
            this.baseRate = BaseRate;

        }

    }


}