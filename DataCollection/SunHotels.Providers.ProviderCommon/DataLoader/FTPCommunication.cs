using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Net;
using System.Text.RegularExpressions;
using System.Linq;

namespace SunHotels.Providers.ProviderCommon.DataLoader
{
    public class DirectoryEntry
    {
        public DateTime Date { get; set; }
        public long Size { get; set; }
        public string Name { get; set; }
    }

    public class FTPCommunication
    {
        const int MaxBufferSize = 1024;

        private readonly NetworkCredential listCredential;
        public int ConnectionLimit { get; set; }
        public int ConnectionTimeout { get; set; }

        public FTPCommunication(string strUser, string strPassword)
        {
            listCredential = new NetworkCredential(strUser, strPassword);
        }

        protected string FTPRequest(string url)
        {
            Exception _ex = null;
            for (int retry = 0; retry != 3; ++retry)
            {
                try
                {
                    StreamReader reader = null;
                    try
                    {
                        FtpWebRequest listRequest = (FtpWebRequest)WebRequest.Create(url);
                        listRequest.Credentials = listCredential;
                        listRequest.Method = WebRequestMethods.Ftp.ListDirectoryDetails;

                        FtpWebResponse listResponse = (FtpWebResponse)listRequest.GetResponse();

                        reader = new StreamReader(listResponse.GetResponseStream());

                        return reader.ReadToEnd();

                    }
                    catch (UriFormatException ex)
                    {
                        _ex = new ApplicationException("Invalid FTP URL.", ex);
                    }
                    catch (WebException ex)
                    {
                        _ex = new ApplicationException("FTP URL could not be reached.", ex);
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
                catch (Exception ex)
                {
                    _ex = ex;
                }
            }
            throw _ex;
        }

        /// <summary>
        /// Windows FTP
        /// Date                          Size     FileName
        /// 03-05-14  02:41PM             26437681 Cancellation Policy Variations.csv
        /// </summary>
        static readonly Regex WindowsDirecotyEntry = new Regex(@"^([0-9\-]+\s+[^\s]+)\s+([0-9]+)\s+(.+)$");
        /// <summary>
        /// Unix/linux FTP
        /// -rw-rw-r--   1 admin    jacobonline     8911 Apr  3 11:40 B2CBlockedHotels.txt
        /// </summary>
        static readonly Regex UnixDirecotyEntryCurrentYear = new Regex(@"[a-zA-Z 0-9]* +([0-9]*) +([a-zA-Z]* +[0-9]{1,2}) +([0-9]{2,2}\:[0-9]{2,2}) +(.*)$");

        /// <summary>
        /// Unix/linux FTP
        /// -rw-rw-r--   1 admin    jacobonline     8911 Apr  3 2013 B2CBlockedHotels.txt
        /// </summary>
        static readonly Regex UnixDirecotyEntryPastYear = new Regex(@"[a-zA-Z 0-9]* +([0-9]*) +([a-zA-Z]* +[0-9]{1,2}) +([0-9]{4,4}) +(.*)$");

        public DirectoryEntry ParseDirectoryEntry(string entry)
        {
            Match m = null;
            if ((m = WindowsDirecotyEntry.Match(entry)).Success)
            {
                DateTime dt;
                if (!DateTime.TryParse(m.Groups[1].Value, new System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None, out dt))
                    dt = DateTime.Now;
                return new DirectoryEntry()
                {
                    Date = dt,
                    Size = long.Parse(m.Groups[2].Value),
                    Name = m.Groups[3].Value
                };
            }
            if ((m = UnixDirecotyEntryCurrentYear.Match(entry)).Success)
            {
                DateTime dt;
                if (!DateTime.TryParse(String.Format("{0} {1}, {2}", m.Groups[2].Value, DateTime.Today.Year, m.Groups[3].Value), new System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None, out dt))
                    dt = DateTime.Now;
                if (dt > DateTime.Now) // C'mon
                    dt = dt.AddYears(-1);
                return new DirectoryEntry()
                {
                    Date = dt,
                    Size = long.Parse(m.Groups[1].Value),
                    Name = m.Groups[4].Value
                };
            }
            if ((m = UnixDirecotyEntryPastYear.Match(entry)).Success)
            {
                DateTime dt;
                if (!DateTime.TryParse(String.Format("{0} {1}", m.Groups[2].Value, m.Groups[3].Value), new System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None, out dt))
                    dt = DateTime.Now;
                return new DirectoryEntry()
                {
                    Date = dt,
                    Size = long.Parse(m.Groups[1].Value),
                    Name = m.Groups[4].Value
                };
            }

            return null;
        }

        public IEnumerable<DirectoryEntry> GetDirectoryEntries(string sourceURL, Dictionary<string, string> directoryList)
        {
            string directoryResponse = FTPRequest(sourceURL);
            return ListDirectory(directoryResponse);
        }
        public void GetDirectoryList(string sourceURL, Dictionary<string, string> directoryList)
        {
            GetDirectoryList(sourceURL)
            .All(sFilename =>
            {
                directoryList.Add(System.IO.Path.Combine(sourceURL, sFilename.Name), sFilename.Name);
                return true;
            });
        }
        public IEnumerable<DirectoryEntry> GetDirectoryList(string sourceURL)
        {

            string directoryResponse = FTPRequest(sourceURL);
            return ListDirectory(directoryResponse);
        }
        public IEnumerable<DirectoryEntry> ListDirectory(string entires)
        {
            return entires.Replace("\r\n", "\n").Split('\n')
                .Where(e => !String.IsNullOrEmpty(e))
                .Select(e =>
            {
                return ParseDirectoryEntry(e);
            })
            .Where(e => e != null);

        }

        public Stream DownloadFile(string strSource)
        {
            try
            {
                FtpWebRequest downloadRequest = (FtpWebRequest)WebRequest.Create(strSource);
                downloadRequest.Method = WebRequestMethods.Ftp.DownloadFile;
                downloadRequest.UseBinary = true;
                downloadRequest.KeepAlive = false;
                downloadRequest.Credentials = listCredential;

                if (ConnectionTimeout != 0)
                    downloadRequest.Timeout = ConnectionTimeout;

                if (ConnectionLimit > 0)
                    downloadRequest.ServicePoint.ConnectionLimit = ConnectionLimit;

                FtpWebResponse downloadResponse = (FtpWebResponse)downloadRequest.GetResponse();
                return downloadResponse.GetResponseStream();
            }
            catch (UriFormatException ex)
            {
                throw new ApplicationException("Invalid path to file.", ex);
            }
            catch (WebException ex)
            {
                throw new ApplicationException("FTP server could not be reached.", ex);
            }
        }

        public void DownloadFile(string strSource, string strDestination)
        {
            try
            {
                using (var responseStream = DownloadFile(strSource))
                {
                    using (var fileStream = File.Create(strDestination))
                    {
                        byte[] buffer = new byte[MaxBufferSize];
                        int bytesRead;
                        while (true)
                        {
                            bytesRead = responseStream.Read(buffer, 0, buffer.Length);
                            if (bytesRead == 0)
                                break;
                            fileStream.Write(buffer, 0, bytesRead);
                        }
                    }
                }
            }
            catch (IOException ex)
            {
                throw new ApplicationException("File could not be written to disk.", ex);
            }
        }
    }
}
