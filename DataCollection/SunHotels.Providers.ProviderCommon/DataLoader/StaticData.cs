using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Linq;
using SunHotels.Providers.ProviderCommon.DataLoader.Transformation;
using SunHotels.Framework.IO.Compression;
using System.Net.Mail;
using CsvHelper;

namespace SunHotels.Providers.ProviderCommon.DataLoader
{
    public enum ILoggerStaticDataSeverity
    {
        Error,
        Warning,
        Info
    }
    /// <summary>
    /// Avoid cross dependecy
    /// </summary>
    public interface IStaticDataLogger
    {
        void Log(ILoggerStaticDataSeverity severity, string message, params object[] args);
    }

    /// <summary>
    /// Class containing all static data from Tourico as DataTables.
    /// </summary>
    public class StaticData
    {

        IStaticDataLogger _logger = null;

        public int MaxDegreeOfParallelism { get; set; }
        public int MaxWebRequestConnections { get; set; }
        public int MaxWebRequestTimeout { get; set; }
        public bool SanitizeEnabled { get; set; }

        public class DownloadedFile
        {
            public ServersServerFolderFile FileInfo { get; set; }
            public string LocalPath { get; set; }
        }
        /// <summary>
        /// Remote directory listing custom filter handler
        /// </summary>
        /// <param name="remotePath"></param>
        /// <param name="direcotyListing"></param>
        /// <returns></returns>
        public delegate IEnumerable<DirectoryEntry> FilterRemoteFileListHandler(string remotePath, ServersServerFolderFile fileInfo, IEnumerable<DirectoryEntry> direcotyListing);
        /// <summary>
        /// Allows a client to filter the remote file lilsting before processing it
        /// </summary>
        public FilterRemoteFileListHandler RemoteDirectoryListingFilter { get; set; }

        List<DataTable> dataTableList;
        string tmpWorkingDir;
        Dictionary<string, ServersServerFolderFile> zipList;
        Dictionary<string, ServersServerFolderFile> fileList;
        public Servers Configuration { get; set; }
        public StaticData(string workingDir)
            : this(workingDir, null)
        {
        }
        public bool UsingLocal { get; set; }

        /// <summary>
        /// Class constructor.
        /// </summary>
        /// <param name="workingDir">Temporary working directory.</param>
        public StaticData(string workingDir, string xmlSettngsFile)
        {
            if (workingDir[workingDir.Length - 1] != '\\')
                workingDir += "\\";
            tmpWorkingDir = workingDir;
            dataTableList = new List<DataTable>();
            zipList = new Dictionary<string, ServersServerFolderFile>();
            fileList = new Dictionary<string, ServersServerFolderFile>();
            if (!string.IsNullOrEmpty(xmlSettngsFile))
                Configuration = Servers.FromFile(xmlSettngsFile);

            if (!Directory.Exists(workingDir))
                Directory.CreateDirectory(workingDir);

            UsingLocal = new System.IO.DirectoryInfo(workingDir).GetFiles().Any();
            SanitizeEnabled = true;
        }
        public StaticData(string workingDir, string xmlSettngsFile, IStaticDataLogger logger)
            : this(workingDir, xmlSettngsFile)
        {
            this._logger = logger;
        }

        static readonly string[] compressExt = new string[] { "zip", "7z", "rar" };
        public bool IsACompressedFile(string file)
        {

            return compressExt.Contains(System.IO.Path.GetExtension(file).Replace(".", ""));
        }

        /// <summary>
        /// 
        /// </summary>
        public int TotalFiles
        {
            get { return fileList.Count; }
        }

        /// <summary>
        /// Downloads the files from the server
        /// </summary>
        /// <returns></returns>
        public void DownloadFiles()
        {
            WriteOuput("Downloading...");
            DownloadZipFiles(UsingLocal);
            if (!UsingLocal)
            {
                WriteOuput("Unzipping...");
                UnzipFiles();
                WriteOuput("Applying transformations...");
                TransformFiles();

                if (SanitizeEnabled)
                {
                    WriteOuput("Sanitizing...");
                    SanitizeFiles();
                }

                WriteOuput("Check files date...");
                CheckFileDate();
            }
            else
            {
                WriteOuput("Local copy found (to prevent using local copy clear {0} directory...", tmpWorkingDir);
                WriteOuput("Check files date...");
                CheckFileDate();
            }
        }

        private void CheckFileDate()
        {
            Match date;
            string formatStringOne = "yyyyMMddHHmmss";
            string formatStringTwo = "yyyyMMdd";
            var yyyyMMddHHmmss = new Regex("(?:(?:(?:(?:(?:[13579][26]|[2468][048])00)|(?:[0-9]{2}(?:(?:[13579][26])|(?:[2468][048]|0[48]))))(?:(?:(?:09|04|06|11)(?:0[1-9]|1[0-9]|2[0-9]|30))|(?:(?:01|03|05|07|08|10|12)(?:0[1-9]|1[0-9]|2[0-9]|3[01]))|(?:02(?:0[1-9]|1[0-9]|2[0-9]))))|(?:[0-9]{4}(?:(?:(?:09|04|06|11)(?:0[1-9]|1[0-9]|2[0-9]|30))|(?:(?:01|03|05|07|08|10|12)(?:0[1-9]|1[0-9]|2[0-9]|3[01]))|(?:02(?:[01][0-9]|2[0-8])))))(?:0[0-9]|1[0-9]|2[0-3])(?:[0-5][0-9]){2}");
            var yyyymmdd = new Regex("(19|20)[0-9]{2}(0[1-9]|1[012])(0[1-9]|[12][0-9]|3[01])");
            DateTime dt;
            Dictionary<string, DateTime> dic = new Dictionary<string, DateTime>();
            int count = fileList.Where(f => yyyymmdd.IsMatch(f.Key) || yyyyMMddHHmmss.IsMatch(f.Key)).Count();
            foreach (var item in fileList.Where(f => yyyymmdd.IsMatch(f.Key) || yyyyMMddHHmmss.IsMatch(f.Key)))
            {
                if (yyyyMMddHHmmss.IsMatch(item.Key))
                {
                    date = yyyyMMddHHmmss.Match(item.Key);
                    dt = DateTime.ParseExact(date.Value, formatStringOne, null);
                    if (dt < DateTime.Now.AddDays(-2))
                    {
                        dic.Add(item.Key, dt);
                    }
                }
                else if (yyyymmdd.IsMatch(item.Key))
                {
                    date = yyyymmdd.Match(item.Key);
                    dt = DateTime.ParseExact(date.Value, formatStringTwo, null);
                    if (dt < DateTime.Now.AddDays(-2))
                    {
                        dic.Add(item.Key, dt);
                    }
                }
            }

            var filteredDic = dic.Where(d => !d.Key.ToLower().Contains("hotelinfo")
                                        && !d.Key.ToLower().Contains("roomtypes"))
                                        .ToDictionary(x => x.Key, x => x.Value);
            if (filteredDic.Any())
            {
                SendEmail(filteredDic);
            }
        }

        private void SendEmail(Dictionary<string, DateTime> dic)
        {

            string providerName = GetProviderName();
            string HtmlBody = GetBodyAsHTML(dic, providerName);

            var client = new SmtpClient
            {
                Host = "smtp1.sunhotels.net",
                Port = 25,
                UseDefaultCredentials = false,
                DeliveryMethod = SmtpDeliveryMethod.Network
            };
            using (MailMessage message = new MailMessage("AlertsDG@sunhotels.net", "support@sunhotels.com")
            {
                Subject = "WARNING file too old",
                Body = HtmlBody,
                IsBodyHtml = true,
                BodyEncoding = System.Text.Encoding.UTF8,
            })
            {
                client.Send(message);
            };

        }

        private string GetBodyAsHTML(Dictionary<string, DateTime> dic, string providerName)
        {
            StringBuilder Body = new StringBuilder();
            int numFiles = 0;
            Body.AppendLine("<html><body><table border='1' bordercolor='black'>");
            foreach (var item in dic)
            {
                if (numFiles < 1)
                {
                    Body.AppendLine("<tr>");
                    Body.AppendLine("<td><b>FileName</b></td><b><td>ProviderName</b></td><td><b>DateCreated</b></td>");
                    Body.AppendLine("</tr>");
                }
                Body.AppendLine("<tr>");
                Body.AppendLine("<td style='padding-right:10px'>" + item.Key + "</td><td style='padding-right:10px'>" + providerName + "</td><td style='padding-right:10px'>" + item.Value + "</td>");
                Body.AppendLine("</tr></br></br>");
                numFiles++;
            }
            Body.AppendLine(string.Format("</table><br/><b> We have {0} file/s pending to check.</b></br>", numFiles));
            Body.AppendLine("<body></html>");

            return Body.ToString();
        }

        private string GetProviderName()
        {
            try
            {
                if (this.RemoteDirectoryListingFilter != null)
                {
                    return this.RemoteDirectoryListingFilter.Target.GetType().ReflectedType.Name.ToString();
                }
                else if (_logger != null)
                {
                    return this._logger.GetType().UnderlyingSystemType.Name;
                }
                return "n/a";
            }
            catch (Exception)
            {
                return "n/a";
            }
        }

        /// <summary>
        /// Downloads the files and creates the data table fo the static data
        /// </summary>
        /// <returns></returns>
        public IEnumerable<DataTable> GetStaticData()
        {
            DownloadFiles();
            return FillDataTables();
        }
        /// <summary>
        /// Returns the list of downloaded files
        /// </summary>
        /// <returns></returns>
        public IEnumerable<DownloadedFile> GetDownloadedFiles()
        {
            return fileList.Where(f => Path.GetExtension(f.Key) != ".tsv")
            .Select(fl => new DownloadedFile()
            {
                FileInfo = fl.Value,
                LocalPath = fl.Key
            }).ToArray();
        }


        private void SanitizeFiles()
        {
            fileList
                .Where(f => f.Value.sanitize)
                .AsParallel()
                .All(f =>
                {
                    var srcFile = f.Key;
                    SanitizeFile(srcFile);
                    return true;
                });
        }

        static readonly int maxBufferSize = 100 * 1024 * 1024;
        public static void SanitizeFile(string srcFile)
        {
            var bufferSize = new System.IO.FileInfo(srcFile).Length;
            if (bufferSize > maxBufferSize)
                bufferSize = maxBufferSize;
            byte[] bufferIn = new byte[bufferSize];
            byte[] bufferOut = new byte[bufferSize];
            byte[] preBuffer = new byte[InvalidCombinationsReplace.Keys.OrderByDescending(k => k.Length).First().Length];
            int offset = 0, size = 0, sizeOut = 0, multimatch = 0, c = 0, i = 0;
            var csvFileOut = srcFile + ".sanitize";
            if (File.Exists(csvFileOut))
                File.Delete(csvFileOut);
            using (var fr = File.OpenRead(srcFile))
            {
                using (var fw = File.OpenWrite(csvFileOut))
                {
                    var flushOut = new Action(() =>
                    {
                        fw.Write(bufferOut, 0, i);
                        i = 0;
                    });
                    var writeOut = new Action<char>((cout) =>
                    {
                        bufferOut[i++] = (byte)cout;
                        sizeOut++;
                        if (i == bufferOut.Length)
                        {
                            flushOut();
                        }
                    });
                    var emptyPrebuffer = new Action(() =>
                    {
                        if (multimatch != 0)
                        {
                            // save prebuffer and clear
                            preBuffer.Take(multimatch).All(characterOut =>
                            {
                                writeOut((char)characterOut);
                                return true;
                            });
                            multimatch = 0;
                        }
                    });
                    while ((size = fr.Read(bufferIn, offset, bufferIn.Length)) > 0)
                    {
                        sizeOut = 0;
                        for (c = 0; c != size; ++c)
                        {
                            char charater = (char)bufferIn[c];
                            if (InvalidCharactersReplace.ContainsKey(charater))
                            {
                                emptyPrebuffer();
                                if (InvalidCharactersReplace[charater].HasValue)
                                {
                                    charater = InvalidCharactersReplace[charater].Value;
                                }
                                else
                                    continue;
                            }
                            else if (InvalidCombinationsReplace.Any(comb => comb.Key.Length > multimatch && comb.Key[multimatch] == charater))
                            {
                                var combination = InvalidCombinationsReplace.First(comb => comb.Key.Length > multimatch && comb.Key[multimatch] == charater);
                                if (combination.Key.Length == multimatch + 1)
                                {

                                    combination.Value.All(characterOut =>
                                    {
                                        writeOut(characterOut);
                                        return true;
                                    });
                                    multimatch = 0;
                                }
                                else
                                    preBuffer[multimatch++] = (byte)charater;
                                continue;
                            }
                            else if (multimatch != 0)
                            {
                                // save prebuffer and clear
                                emptyPrebuffer();
                            }
                            writeOut(charater);
                        }
                    }
                    emptyPrebuffer();
                    flushOut();
                    fw.Flush();
                    fw.Close();
                }
                fr.Close();
            }
            File.Copy(csvFileOut, srcFile, true);
            File.Delete(csvFileOut);
        }

        private void TransformFiles()
        {
            var basePath = System.IO.Directory.GetCurrentDirectory() + "\\";
            fileList.Select(f => new { file = f.Key, trans = f.Value?.Transformation })
                .Where(f => f.trans?.Count != 0)
                .AsParallel()
                .All(f =>
                {
                    string sImportFile = f.file;
                    foreach (var node in f.trans)
                    {
                        var type = Type.GetType(node.type);
                        var itrans = (IFileTransform)type.GetConstructor(new Type[0]).Invoke(new Object[0]);
                        try
                        {
                            var file = f.file;
                            if (!System.IO.Path.IsPathRooted(f.file))
                                file = basePath + f.file;
                            itrans.Transform(file, node.Parameters, tmpWorkingDir);
                        }
                        catch (Exception ex)
                        {
                            if (!node.silent)
                            {
                                throw ex;
                            }
                            else
                                WriteOuput("Error applying transformation {0} to file {1}: {2}", node.type, f.file, ex);
                        }
                    }
                    return true;
                });
        }

        /// <summary>
        ///  Downloades static data from Tourico FTP as unzipped CSV files. 
        /// </summary>
        private void DownloadZipFiles(bool fromLocal = false)
        {
            foreach (var serverNode in Configuration.Server)
            {
                FTPCommunication ftpCom = new FTPCommunication(serverNode.user, serverNode.password);
                ftpCom.ConnectionLimit = MaxWebRequestConnections;
                ftpCom.ConnectionTimeout = MaxWebRequestTimeout;
                // Get folders
                foreach (var folderNode in serverNode.Folder)
                {
                    string sFileUrl = serverNode.url + folderNode.path;
                    Dictionary<string, string> _fileList = new Dictionary<string, string>(100);
                    IEnumerable<DirectoryEntry> files = ftpCom.GetDirectoryList(sFileUrl);
                    // Remove olders
                    files = folderNode.File
                        // Files by "File" node
                        .Select(file => new { file, mfiles = files.Select(f => new { f, m = Regex.Match(f.Name, file.name) }).Where(f => f.m.Success) })
                        .SelectMany(file => file.mfiles.Select(f => new
                        {
                            file = f.f,
                            m = file.file.groupIndexSpecified ? f.m.Groups[file.file.groupIndex].Value : f.f.Name,
                            filterOlders = file.file.filterOldersSpecified && file.file.filterOlders
                        }))
                        // Group by: groupIndex or file name
                        .GroupBy(f => f.m)
                        // remove olders
                        .SelectMany(g => g.First().filterOlders ?
                            // Remove olders by ordering by date
                            g.OrderByDescending(f => f.file.Date).Take(1).Select(f => f.file) :
                            // Return all
                            g.Select(f => f.file)
                        ).ToArray();
                    // No custom filter, add all entries and match agains "File" settings
                    if (RemoteDirectoryListingFilter == null)
                    {
                        files.All(sFilename =>
                        {
                            _fileList.Add(System.IO.Path.Combine(sFileUrl, sFilename.Name), sFilename.Name);
                            return true;
                        });
                    }
                    // File mapping
                    Dictionary<String, ServersServerFolderFile> mapping = new Dictionary<string, ServersServerFolderFile>();
                    foreach (var fileNode in folderNode.File)
                    {
                        // Custom filter set, call with the File info
                        if (RemoteDirectoryListingFilter != null)
                        {
                            var filesMatch = files.Where(d => Regex.IsMatch(d.Name, fileNode.name));
                            if (filesMatch.Count() == 0)
                                throw new Exception("No files found on server that match " + fileNode.name);

                            RemoteDirectoryListingFilter(sFileUrl, fileNode, filesMatch)
                                .All(sFilename =>
                                {
                                    _fileList.Add(System.IO.Path.Combine(sFileUrl, sFilename.Name), sFilename.Name);
                                    return true;
                                });
                        }
                        mapping.Add(fileNode.name, fileNode);
                    }

                    if (!fromLocal)
                    {                        
                        _fileList.Keys
                            .AsParallel()
                            .WithDegreeOfParallelism(MaxDegreeOfParallelism == 0 ? 1 : MaxDegreeOfParallelism)
                            .All(ftpFile =>
                            {

                                var key = mapping.Where(d => Regex.IsMatch(_fileList[ftpFile], d.Key));
                                if (key.Any())
                                {
                                    var prefix = key.First().Value.type;
                                    var isCompressed = IsACompressedFile(ftpFile);
                                    var filename = prefix + _fileList[ftpFile];
                                    WriteOuput("{0}: {1}", prefix, _fileList[ftpFile]);
                                    ftpCom.DownloadFile(ftpFile, tmpWorkingDir + filename);

                                    if (isCompressed)
                                    {
                                        zipList.Add(tmpWorkingDir + filename, key.First().Value);
                                    }
                                    else
                                    {
                                        fileList.Add(tmpWorkingDir + filename, key.First().Value);
                                    }
                                }
                                return true;
                            });
                    }
                    else
                    {
                        var tmpFiles = FindTempFiles();
                        folderNode.File
                            .All(file =>
                            {
                                tmpFiles.Where(f => System.IO.Path.GetFileName(f).StartsWith(file.type.ToString()))
                                    .All(f =>
                                    {
                                        fileList.Add(f, file);
                                        return true;
                                    });
                                return true;
                            });
                    }
                    mapping
                        .Where(kv => !_fileList.Keys.Any(key => Regex.IsMatch(key, kv.Key)))
                        .All(err =>
                        {
                            WriteOuput("File not found {0}", err.Key);
                            return true;
                        });
                }
            }
        }

        List<string> FindTempFiles(string dir = null, List<string> files = null)
        {
            files = files ?? new List<string>();
            dir = dir ?? tmpWorkingDir;            
            files.AddRange(Directory.GetFiles(dir));
            foreach (string d in Directory.GetDirectories(dir))
            {
                FindTempFiles(d, files);
            }
            return files;
        }

        /// <summary>
        /// Unzipes the CSV files and adds them to a list
        /// </summary>
        private void UnzipFiles()
        {
            var unzipped = zipList.Keys.AsParallel()
            .All(sFile =>
            {
                ServersServerFolderFile serversServerFolderFile;
                if (zipList.TryGetValue(sFile, out serversServerFolderFile))
                {
                    UnZip(sFile, tmpWorkingDir, serversServerFolderFile).ForEach(f =>
                    {
                        var prefix = serversServerFolderFile.type;
                        var file = String.Format("{0}\\{1}{2}", Path.GetDirectoryName(f), prefix, Path.GetFileName(f));
                        File.Copy(f, file, true);
                        File.Delete(f);
                        lock (fileList)
                        {
                            if (!fileList.ContainsKey(file))
                                fileList.Add(file, serversServerFolderFile);
                        }
                    });
                    WriteOuput("Unzip complete!");
                    return true;
                }
                return false;
            });
            WriteOuput(string.Format("Unzipped all files: {0}", unzipped));
        }


        /// <summary>
        /// Check that all CSV files has ben downladed and unziped.
        /// If all CSV files exists, import the static info from CSV files to DataTables.
        /// </summary>
        private IEnumerable<DataTable> FillDataTables()
        {
            WriteOuput("FillDataTables started!");

            string csvFilesMissing = string.Empty;
            bool firstFile = true;

            var files = Directory.GetFiles(tmpWorkingDir);
            foreach (var fileNode in Configuration.Server.SelectMany(s => s.Folder.SelectMany(f => f.File)))
            {
                string csvFileName = fileNode.type + fileNode.name;

                if (!files.Any(f => Regex.IsMatch(Path.GetFileName(f), csvFileName)))
                {
                    if (firstFile)
                    {
                        csvFilesMissing = string.Concat(csvFilesMissing, csvFileName);
                        firstFile = false;
                    }
                    else
                    {
                        csvFilesMissing = string.Concat(csvFilesMissing, ", ", csvFileName);
                    }
                }
            }

            if (csvFilesMissing != string.Empty)
            {
                throw new ApplicationException("CSV file missing, file/files does not exist: " + csvFilesMissing);
            }

            var result = fileList
                .OrderBy(f => f.Value.order)
                .Select(f => CreateTableFromCSV(f.Key, f.Value));
            WriteOuput("FillDataTables finished!");

            return result;
        }

        /// <summary>
        /// Unzip specified ZIP file content to specified target directory.
        /// </summary>
        /// <param name="sFile">The path to the zipfile.</param>
        /// <param name="sTargetDirectory">The directory used to save the extracted zipfile.</param>
        /// <returns>The list of unziped files.</returns>
        private List<string> UnZip(string sFile, string sTargetDirectory, ServersServerFolderFile fileInfo)
        {
            List<string> listFiles = new List<string>();
            var basePath = System.IO.Path.GetDirectoryName(sFile);
            var zipfolder = (!string.IsNullOrEmpty(sTargetDirectory) ? sTargetDirectory : System.IO.Path.GetTempPath()) + "uncompress\\" + System.IO.Path.GetFileNameWithoutExtension(sFile);
            if (System.IO.Directory.Exists(zipfolder))
                System.IO.Directory.Delete(zipfolder, true);
            System.IO.Directory.CreateDirectory(zipfolder);
            // Possible multithread error with the lib
            lock (this)
            {
                new Compression().UnCompressFile(sFile, zipfolder);
            }
            var fileNames = fileInfo
                .Filter
                .Where(f => f.type == ServersServerFolderFileFilterType.filename)
                .Select(f => new System.Text.RegularExpressions.Regex(f.Value))
                .ToArray();
            var files = CopyDirectory(zipfolder, basePath, fileNames);

            System.IO.Directory.Delete(zipfolder, true);
            return files;
        }

        private List<string> CopyDirectory(string sourcePath, string destPath, IEnumerable<Regex> fileNameFilter)
        {
            if (!Directory.Exists(destPath))
            {
                Directory.CreateDirectory(destPath);
            }
            List<string> files = new List<string>();
            foreach (string file in Directory.GetFiles(sourcePath)
                .Where(f => fileNameFilter.Count() == 0 || fileNameFilter.Any(fn => fn.IsMatch(System.IO.Path.GetFileName(f)))))
            {
                string dest = Path.Combine(destPath, Path.GetFileName(file));
                if (!File.Exists(dest))
                {
                    files.Add(dest);
                }
                File.Copy(file, dest, true);
            }

            foreach (string folder in Directory.GetDirectories(sourcePath))
            {
                string dest = Path.Combine(destPath, Path.GetFileName(folder));
                files.AddRange(CopyDirectory(folder, dest, fileNameFilter));
            }
            return files;
        }

        static Dictionary<char[], char[]> InvalidCombinationsReplace = new Dictionary<char[], char[]>()
        {
            {new char[]{'\r','\r','\n'}, new char[]{'\r','\n'}},
            {new char[]{'\n','\r'}, new char[]{'\r','\n'}}
        };
        static Dictionary<Char, Char?> InvalidCharactersReplace = new Dictionary<char, char?>(){
                        {(char)0x00, null },
                        {(char)0x01, null },
                        {(char)0x02, null },
                        {(char)0x03, null },
                        {(char)0x04, null },
                        {(char)0x05, null },
                        {(char)0x06, null },
                        {(char)0x07, null },
                        {(char)0x08, null },
                        {(char)0x10, null },
                        {(char)0x12, null },
                        {(char)0x14, null },
                        {(char)0x15, null },
                        {(char)0x16, null },
                        {(char)0x17, null },
                        {(char)0x18, null },
                        {(char)0x19, null },
                        {(char)0x1A, null },
                        {(char)0x1B, null },
                        {(char)0x1C, null },
                        {(char)0x1D, null },
                        {(char)0x1E, null },
                        {(char)0x1F, null }
                    };

        public DataTable CreateTableFromCSV(string srcFile)
        {
            return CreateTableFromCSV(srcFile, fileList[srcFile]);
        }

        private DataTable CreateTableFromCSV(string srcFile, ServersServerFolderFile file)
        {
            try
            {
                var filters = GetFieldFilterFromServerFolderFile(file);                
                var csvOptions = GetServerFolderFileCSV(file);

                var fileInfo = new FileInfo(srcFile);
                var dataTable = new DataTable(fileInfo.Name);

                FillDataTableFromCSVSource(srcFile, dataTable, csvOptions);
                FilterDataTableColumns(filters, dataTable);
                return dataTable;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(string.Format("Error while reading CSV file {0} to DataTable", srcFile), ex);
            }
        }

        public void FillDataTableFromCSVSource(string srcFile, DataTable dt, ServersServerFolderFileCSV csvOptions)
        {
            
            using (StreamReader streamReader = File.OpenText(srcFile))
            {
                using (var csv = new CsvReader(streamReader))
                {
                    csv.Configuration.Delimiter = csvOptions.separator;
                    csv.Configuration.IgnoreBlankLines = true;
                    csv.Configuration.SkipEmptyRecords = true;

                    if (!csv.ReadHeader())
                        throw new ApplicationException(string.Format("Error while reading CSV file {0} to DataTable", srcFile), new Exception("ReadHeader"));

                    foreach (var headerField in csv.FieldHeaders)
                        dt.Columns.Add(headerField, Type.GetType("System.String"));

                    while (csv.Read())
                    {
                        dt.Rows.Add(csv.CurrentRecord);                     
                    }
                }
            }
        }

        private static void FilterDataTableColumns(List<FieldFilter> filters, DataTable dt)
        {
            if (filters.Any())
                for (var c = dt.Rows.Count - 1; c >= 0; --c)
                {
                    if (!filters.All(f =>
                    {
                        return f.Filter(dt.Rows[c][f.Field].ToString());
                    }))
                        dt.Rows.RemoveAt(c);
                }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="srcFile"></param>
        /// <returns></returns>
        //private DataTable CreateTableFromCSV(string srcFile, ServersServerFolderFile file)
        //{

        //List<FieldFilter> filters = GetFieldFilterFromServerFolderFile(file);
        //// Sanitize file
        //bool sanitize = file.sanitize;
        //try
        //{
        //    ServersServerFolderFileCSV csv = GetServerFolderFileCSV(file);
        //    CSVData importCSV = new CSVData();
        //    importCSV.SEPARATOR_CHAR = csv.separator[0];
        //    importCSV.QuoteString = csv.qualifier;
        //    var t = importCSV.Read(srcFile);
        //    // Filter
        //    if (filters.Any())
        //        for (var c = t.Rows.Count - 1; c >= 0; --c)
        //        {
        //            if (!filters.All(f =>
        //            {
        //                return f.Filter(t.Rows[c][f.Field].ToString());
        //            }))
        //                t.Rows.RemoveAt(c);
        //        }
        //    return t;

        //}
        //catch (Exception ex)
        //{
        //    throw new ApplicationException(string.Format("Error while reading CSV file {0} to DataTable", srcFile), ex);
        //}
        //}

        private static ServersServerFolderFileCSV GetServerFolderFileCSV(ServersServerFolderFile file)
        {
            var csv = file.CSV ?? new ServersServerFolderFileCSV();
            if (string.IsNullOrEmpty(csv.qualifier))
                csv.qualifier = "\"";
            if (string.IsNullOrEmpty(csv.separator))
                csv.separator = "|";
            return csv;
        }

        private static List<FieldFilter> GetFieldFilterFromServerFolderFile(ServersServerFolderFile file)
        {
            if (file == null || file.Filter == null || !file.Filter.Any())
                return new List<FieldFilter>();

            return file.Filter
                .Select(filter =>
                new FieldFilter()
                {
                    Field = filter.field,
                    Type = filter.type.ToString(),
                    Value = filter.Value
                }).ToList();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="debugMessage"></param>
        public void WriteOuput(string message, params object[] values)
        {
            if (_logger != null)
                _logger.Log(ILoggerStaticDataSeverity.Info, message, values);
            Console.WriteLine("debug: " + message, values);
        }

    }
}
