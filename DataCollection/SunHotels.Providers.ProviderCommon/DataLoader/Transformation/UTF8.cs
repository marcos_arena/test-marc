﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SunHotels.Providers.ProviderCommon.DataLoader.Transformation
{
	/// <summary>
	/// Littel-endian to Big-Endian
	/// </summary>
	public class UTF8 : IFileTransform
	{
		public UTF8()
		{ 
		}

		public void Transform(string path, string parameters, string tmpWorkingDir)
		{
            var transformDir = (!string.IsNullOrEmpty(tmpWorkingDir) ? tmpWorkingDir : System.IO.Path.GetTempPath()) + "transform";
            if (!System.IO.Directory.Exists(transformDir))
                System.IO.Directory.CreateDirectory(transformDir);

            var tmp = transformDir + "\\" + System.IO.Path.GetFileName(path);
			var buffer = new char[10 * 1024 * 1024];
			int size = 0;
			using (var sr = new System.IO.StreamReader(path))
			{
				using (var sw = new System.IO.StreamWriter(tmp, false, UTF8Encoding.UTF8))
				{
					while((size = sr.Read(buffer, 0, buffer.Length)) != 0){
						sw.Write(buffer, 0, size);
					}
					sw.Close();
				}
				sr.Close();
			}
			System.IO.File.Copy(tmp, path, true);
			System.IO.File.Delete(tmp);
		}
	}
}
