﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace SunHotels.Providers.ProviderCommon.DataLoader.Transformation
{
	/// <summary>
	/// Replace text
	/// 
	/// Parameers: 1 or more 'Replace' elements
	/// <Parameters><![CDATA[<Replace key=",&lt;&#x2F;" value="&lt;&#x2F;" type="plain"/>]]></Parameters>
	/// Supported types:
	///		* plain
	///		* regexp
	/// </summary>
	public class Replace : IFileTransform
	{
		public Replace()
		{ 
		}

		public void Transform(string path, string parameters, string tmpWorkingDir)
		{
			var replace = XElement.Parse("<Root>" + parameters + "</Root>")
				.Descendants("Replace")
				.ToDictionary(x => x.Attribute("key").Value,
							x => new
							{
								isRegExp = x.Attribute("type") != null && x.Attribute("type").Value == "regexp",
								value = x.Attribute("value").Value
							});
			var regexp = replace.Where(kv => kv.Value.isRegExp)
						.ToDictionary(kv => new System.Text.RegularExpressions.Regex(kv.Key), kv => kv.Value.value);
						
			var simpleReplace = replace.Where(kv => !kv.Value.isRegExp);

            var transformDir = (!string.IsNullOrEmpty(tmpWorkingDir) ? tmpWorkingDir : System.IO.Path.GetTempPath()) + "transform";
            if (!System.IO.Directory.Exists(transformDir))
                System.IO.Directory.CreateDirectory(transformDir);

            var tmp = transformDir + "\\" + System.IO.Path.GetFileName(path);
			using (var sr = new System.IO.StreamReader(path))
			{
				using (var sw = new System.IO.StreamWriter(tmp, false, sr.CurrentEncoding))
				{
					string line = null;
					while ((line = sr.ReadLine()) != null) {
						simpleReplace.All(simpler =>
						{
							line = line.Replace(simpler.Key, simpler.Value.value);
							return true;
						});
						regexp.All(regx =>
						{
							line = regx.Key.Replace(line, regx.Value);
							return true;
						});
						sw.WriteLine(line);
					}
					sw.Flush();
					sw.Close();
				}
				sr.Close();
			}
			System.IO.File.Copy(tmp, path, true);
			System.IO.File.Delete(tmp);
		}
	}
}
