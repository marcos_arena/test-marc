﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SunHotels.Providers.ProviderCommon.DataLoader.Transformation
{
	public interface IFileTransform
	{
		void Transform(string path, string parameters, string tmpWorkingDir);
	}
}
