﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace SunHotels.Providers.ProviderCommon.DataLoader.Transformation
{
	/// <summary>
	/// Littel-endian to Big-Endian
	/// </summary>
	public class Execute : IFileTransform
	{
		public Execute()
		{ 
		}
		public void Transform(string path, string parameters, string tmpWorkingDir)
		{
			// Start the child process.
			Process p = new Process();
			// Redirect the error stream of the child process.
			p.StartInfo.UseShellExecute = false;
			p.StartInfo.RedirectStandardError = true;
			p.StartInfo.FileName = parameters.Replace("$file", path);
			p.StartInfo.WorkingDirectory = System.IO.Path.GetDirectoryName(path);
			p.Start();
			// Do not wait for the child process to exit before
			// reading to the end of its redirected error stream.
			// p.WaitForExit();
			// Read the error stream first and then wait.
			string error = p.StandardError.ReadToEnd();
			p.WaitForExit();
			if(!String.IsNullOrEmpty(error))
				throw new Exception(String.Format("Error applying Execute transformation to {1}: {2}", path, error));
		}
	}
}
