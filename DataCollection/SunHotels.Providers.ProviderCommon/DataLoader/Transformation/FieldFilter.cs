﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SunHotels.Providers.ProviderCommon.DataLoader.Transformation
{
	public class FieldFilter
	{
		public string Field { get; set; }

		public string Type { get; set; }

		public string Value { get; set; }

		public bool Filter(String data) { 
			var value = Value.Split(',');
			switch(Type){
				case "equal":
					return value.Any(v => v.Equals(data));
				case "notequal":
					return !value.Any(v => v.Equals(data));
				case "contain":
					return value.Any(v => v.IndexOf(data) != -1);
				case "notcontain":
					return !value.Any(v => v.IndexOf(data) != -1);
				case "regex": 
					return System.Text.RegularExpressions.Regex.IsMatch(data, Value);
			}
			throw new Exception(String.Format("Filter type <{0}> not supported", Type));
		}
	}
}
