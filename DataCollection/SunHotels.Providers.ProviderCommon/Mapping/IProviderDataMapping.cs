﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;

namespace SunHotels.Providers.ProviderCommon.Mapping
{
	/// <summary>
	/// Provides data mapping between the provider and SunHotels
	/// </summary>
	public interface IProviderDataMapping
	{
		#region PROPERTIES
		int LanguageID { get; set; }
		string ProviderId { get; set; }
		#endregion PROPERTIES

		#region SUNHOTELS TABLES
		SingleMappingTable SunHotelsBoards { get; }
		SingleMappingTable SunHotelsFeatures { get; }
		#endregion SUNHOTELS TABLES

		#region Single VALUES
		MappingTable GetMappingFor(string mapping);
		SingleMappingTable GetSingleMappingFor(string mapping);
		void SetMappingFor(string mapping, MappingTable value);
		void SetMappingFor(string mapping, SingleMappingTable value);
		MappingTable this[string mapping] { get; set; }
		#endregion Single VALUES

		#region COMPLEX VALUES
		/// <summary>
		/// Returns a mapping table able to return dynamic objects to wrapp the values.
		/// </summary>
		/// <param name="mapping">Table mapping</param>
		/// <param name="getter">Internal value properties accesor</param>
		/// <returns></returns>
		DynSingleMappingTable GetDynSingleMappingFor(string mapping, ComplexTypeMapperHandler getter = null);
		/// <summary>
		/// Returns a mapping table able to return dynamic objects to wrapp the values.
		/// </summary>
		/// <param name="mapping"></param>
		/// <param name="getter"></param>
		/// <returns></returns>
		DynMappingTable GetDynMappingFor(string mapping, ComplexTypeMapperHandler getter = null);

		/// <summary>
		/// Registers a getter for a mapping so each time the mapping is called the getter will be used.
		/// </summary>
		/// <param name="mapping"></param>
		/// <param name="getter"></param>
		void RegisterDynMappingHandlerFor(string mapping, ComplexTypeMapperHandler getter);
		#endregion COMPLEX VALUES

	}
	/// <summary>
	/// Defines a propety accesor (getter) for thos complex values.
	/// This getter will be invoked each time a property is invoked over a Dynamic value.
	/// </summary>
	/// <param name="tableValue">Value as was stored in the mapping table</param>
	/// <param name="property">Property to read.</param>
	/// <returns></returns>
	public delegate object ComplexTypeMapperHandler(string tableValue, string property);

	/// <summary>
	/// Single mapping table 1:1 values
	/// </summary>
	public class SingleMappingTable : Dictionary<string, string>
	{
		public SingleMappingTable()
		{
		}
		public SingleMappingTable(Dictionary<string, string> dictionary)
		{
			dictionary.All(kv =>
			{
				Add(kv.Key, kv.Value);
				return true;
			});
		}
	}

	/// <summary>
	/// Multiple mapping table 1:N values
	/// </summary>
	public class MappingTable : Dictionary<string, string[]>
	{

		public MappingTable(Dictionary<string, string[]> dictionary)
		{
			dictionary.All(kv =>
			{
				Add(kv.Key, kv.Value);
				return true;
			});
		}
	}

	public class DynValue : DynamicObject
	{
		string _value;
		ComplexTypeMapperHandler _getter;
		public DynValue(string value, ComplexTypeMapperHandler getter)
		{
			_value = value;
			_getter = getter;
		}
		public override bool TryGetMember(GetMemberBinder binder, out object result)
		{
			result = _getter(_value, binder.Name);
			return true;
		}
	}

	/// <summary>
	/// Single dynamic mapping table 1:1
	/// </summary>
	public class DynSingleMappingTable 
	{

		ComplexTypeMapperHandler _getter;
		Dictionary<string, dynamic> _table;
		public DynSingleMappingTable(SingleMappingTable table, ComplexTypeMapperHandler getter)
		{
			_getter = getter;
			_table = table.ToDictionary(kv => kv.Key, kv => (dynamic) new DynValue(kv.Value, _getter));
		}

		public bool ContainsKey(string key)
		{
			return _table.ContainsKey(key);
		}

		public ICollection<string> Keys
		{
			get
			{
				return _table.Keys;
			}
		}

		public bool TryGetValue(string key, out dynamic value)
		{
			value = null;
			if (!ContainsKey(key))
				return false;
			value = this[key];
			return true;
		}

		public dynamic this[string key]
		{
			get
			{
				return _table[key];
			}
		}

		public bool Contains(KeyValuePair<string, dynamic> item)
		{
			return _table.ContainsKey(item.Key);
		}

		public int Count
		{
			get { return _table.Count; }
		}

		public bool IsReadOnly
		{
			get { return true; }
		}

		public IEnumerator<KeyValuePair<string, dynamic>> GetEnumerator()
		{
			return _table.GetEnumerator();
		}
	}

	/// <summary>
	/// Single dynamic mapping table 1:1
	/// </summary>
	public class DynMappingTable 
	{
		ComplexTypeMapperHandler _getter;
		Dictionary<string, dynamic[]> _table;
		public DynMappingTable(MappingTable table, ComplexTypeMapperHandler getter)
		{
			_getter = getter;
			_table = table.ToDictionary(kv => kv.Key, kv => kv.Value.Select(v => (dynamic)new DynValue(v, _getter)).ToArray());
		}

		public bool ContainsKey(string key)
		{
			return _table.ContainsKey(key);
		}

		public ICollection<string> Keys
		{
			get
			{
				return _table.Keys;
			}
		}

		public bool TryGetValue(string key, out dynamic[] value)
		{
			value = null;
			if (!ContainsKey(key))
				return false;
			value = this[key];
			return true;
		}

		public dynamic[] this[string key]
		{
			get
			{
				return _table[key];
			}
		}

		public bool Contains(KeyValuePair<string, dynamic[]> item)
		{
			return _table.ContainsKey(item.Key);
		}

		public int Count
		{
			get { return _table.Count; }
		}

		public bool IsReadOnly
		{
			get { return true; }
		}

		public IEnumerator<KeyValuePair<string, dynamic[]>> GetEnumerator()
		{
			return _table.GetEnumerator();
		}

	}
}
