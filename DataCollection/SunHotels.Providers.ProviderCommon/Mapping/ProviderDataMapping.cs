﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SunHotels.Providers.ProviderCommon.Mapping
{
	/// <summary>
	/// 
	/// </summary>
	public class ProviderDataMapping : SunHotels.Providers.ProviderCommon.Mapping.IProviderDataMapping
	{
		public string ProviderId { get; set; }
		public string CSVSeparator { get; set; }
		public string CSVQualifier { get; set; }
		public int LanguageID { get; set; }

		Dictionary<string, ComplexTypeMapperHandler> _getters = new Dictionary<string, ComplexTypeMapperHandler>();

		public SingleMappingTable SunHotelsFeatures
		{
			get
			{
				return new SingleMappingTable(GetDataContext().vFeatures.Where(v => v.LanguageID == LanguageID)
						.ToDictionary(v => v.FEATURE_ID.ToString(), v => v.text));
			}
		}
		public SingleMappingTable SunHotelsBoards
		{
			get
			{
				return new SingleMappingTable(GetDataContext().vBoards.Where(v => v.LanguageID == LanguageID)
						.ToDictionary(v => v.id.ToString(), v => v.text));
			}
		}


		

		public ProviderDataMapping():
			this(null){ 
			
		}

		public ProviderDataMapping(string providerId, 
						string csvseparator = ",", 
						string csvqualifier = "\"",
						int languageID = 1) {
			ProviderId = providerId;
			CSVSeparator = csvseparator;
			CSVQualifier = csvqualifier;
			LanguageID = languageID;
		}
		public MappingTable this[string mapping] {
			get {
				return GetMappingFor(mapping);
			}
			set {
				SetMappingFor(mapping, value);
			}
		}

		public DataMappingDataContext GetDataContext()
		{
			return new DataMappingDataContext(SunHotels.Framework.Config.Config.Instance["ConnectionStrings.Import103"]);
		}

		public void SetMappingFor(string mapping, MappingTable value)
		{
			DataMappingDataContext db = GetDataContext();
			var map = db.Mappings.First(m => m.Provider_id == ProviderId && m.Type == mapping);
			map.MappingEntries.Clear();
			value.All(v =>
			{
				if (v.Value.GroupBy(val => val).Select(g => g.Count() > 1).Any())
					throw new Exception(string.Format("Error duplicates on mapping value {0} for mapping table {1}", mapping, string.Join(",", v.Value)));
				map.MappingEntries.Add(new MappingEntry()
				{
					Mapping = map,
					Mapping_id = map.Id,
					CSV = string.Join(CSVSeparator, v.Value.Select(val => ToCSVValue(val)).ToArray()),
					Key = v.Key
				});
				return true;
			});
			db.SubmitChanges();
		}

		public void SetMappingFor(string mapping, SingleMappingTable value)
		{
			SetMappingFor(mapping, new MappingTable(value.ToDictionary(e => e.Key, e => new string[] { e.Value })));
		}

		public SingleMappingTable GetSingleMappingFor(string mapping)
		{
			DataMappingDataContext db = GetDataContext();
			var map = db.Mappings.First(m => m.Provider_id == ProviderId && m.Type == mapping);
			return new SingleMappingTable(map.MappingEntries
					.ToDictionary(me => me.Key, me => me.CSV));
		}

		public MappingTable GetMappingFor(string mapping)
		{
			DataMappingDataContext db = GetDataContext();
			var map = db.Mappings.First(m => m.Provider_id == ProviderId && m.Type == mapping);
			return new MappingTable(map.MappingEntries
					.ToDictionary(me => me.Key, me => FromCSVValue(me.CSV)));

		}

		public string ToCSVValue(string value) {
			if(value.IndexOf(CSVSeparator) == -1)
				return value;
			if(value.IndexOf(CSVQualifier) != -1){
				throw new Exception(string.Format("Can not save the value <{0}> with the current configuration. Change the CSVQualifier", value));
			}
			return CSVQualifier + value + CSVQualifier;
		}

		public string[] FromCSVValue(string value)
		{
			return SunHotels.Framework.IO.CSV.Utils.SplitLine(value, CSVSeparator, CSVQualifier).ToArray();
		}

		public DynSingleMappingTable GetDynSingleMappingFor(string mapping, ComplexTypeMapperHandler getter)
		{
			return new DynSingleMappingTable(GetSingleMappingFor(mapping), getter ?? _getters[mapping]);
		}

		public DynMappingTable GetDynMappingFor(string mapping, ComplexTypeMapperHandler getter)
		{
			return new DynMappingTable(GetMappingFor(mapping), getter ?? _getters[mapping]);
		}

		public void RegisterDynMappingHandlerFor(string mapping, ComplexTypeMapperHandler getter)
		{
			_getters.Add(mapping, getter);
		}
	}
}
