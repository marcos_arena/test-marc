﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Xml;

namespace System
{
	public static class Extensions
	{
		public static IEnumerable<XmlNode> AsEnumerable(this XmlNodeList nl) {
			foreach (XmlNode n in nl)
				yield return n;
		}
		public static IEnumerable<DataRow> AsEnumerable(this DataRowCollection col) {
			foreach (DataRow dr in col)
				yield return dr;
		}
	}
}
