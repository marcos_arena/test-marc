﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;


namespace SunHotels.Providers.ProviderCommon
{
	public partial class Servers
	{
		public static Servers FromFile(string file) {
			using (var fr = System.IO.File.OpenRead(file)) {
				return (Servers)new XmlSerializer(typeof(Servers)).Deserialize(fr);
			}
		}
	}
}
