﻿namespace SunHotels.Providers
{
    public class HanaTourXMLLiveSettings
    {
        public int GeoCodeDecimals { get; set; }
        public string IdentifierType { get; set; }
        public bool DeleteFileAfterProcessing { get; set; }
        public string FileExtensions { get; set; }
        public string HotelCodeHeading { get; set; }
    }
}



