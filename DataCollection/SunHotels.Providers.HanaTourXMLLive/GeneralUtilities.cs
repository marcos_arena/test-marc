﻿using System.Xml.Linq;
using System.Xml.Serialization;
using System.Globalization;
using System;

namespace SunHotels.Providers
{
    public static class GeneralUtilities
    {
        #region Public Method

        /// <summary>
        /// Trim Prefix if start with single quotation 
        /// </summary>
        /// <param name="obj">input object</param>
        /// <returns>string</returns>
        public static string TrimPrefix(this object obj)
        {
            if (Convert.IsDBNull(obj) || obj == null)
                return string.Empty;
            string str = obj.ToString().Trim();

            if (str == "-" || str == "'"
                           || str.Equals("'null", StringComparison.InvariantCultureIgnoreCase)
                           || str.Equals("'n/a", StringComparison.InvariantCultureIgnoreCase))
                return string.Empty;

            if (str.StartsWith("'"))
                str = str.Replace("'", string.Empty);
            if (str.EndsWith("null", StringComparison.InvariantCultureIgnoreCase))
                str = str.Remove(str.Length - 4, 4);
            return str;
        }

        /// <summary>
        /// Converts object to string and trim
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static string Trim(this object obj)
        {
            return Convert.IsDBNull(obj) || obj == null ? string.Empty : obj.ToString().Trim() ;
        }

        /// <summary>
        /// Round to the specified number of decimals
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="decimals"></param>
        /// <returns>string</returns>
        public static string RoundDecimal(this object obj, int decimals)
        {
            return Convert.IsDBNull(obj) || obj == null ? string.Empty : Math.Round(Convert.ToDecimal(obj, CultureInfo.InvariantCulture), decimals).ToString(CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// This method De-serializes string to object
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="doc">XDocument</param>
        /// <returns>object</returns>
        public static T Deserialize<T>(XDocument doc)
        {
            var xmlSerializer = new XmlSerializer(typeof(T));
            if (doc.Root == null) return default(T);
            using (var reader = doc.Root.CreateReader())
                return (T)xmlSerializer.Deserialize(reader);
        }

        #endregion Public Method
    }
}
