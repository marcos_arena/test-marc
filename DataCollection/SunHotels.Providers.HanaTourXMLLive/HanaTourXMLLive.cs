﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using SunHotels.XML;
using SunHotels.XML.Data;
using SunHotels.Providers.ProviderCommon.DataLoader;
using System.Xml.Linq;
using Hotel = SunHotels.XML.Data.Hotel;
using System.Threading.Tasks;
using System.Data;
using System.Data.OleDb;
using System.Globalization;

namespace SunHotels.Providers
{
    public class HanaTourXMLLive : XMLProvider, IStaticDataLogger
    {
        #region Global Variable Declaration

        // Create dictionaries for fast search
        private readonly Dictionary<string, Place> _allPlaces = new Dictionary<string, Place>();
        private readonly Dictionary<string, Hotel> _allHotels = new Dictionary<string, Hotel>();
        private HanaTourXMLLiveSettings _hanaTourXmlLiveSettings = new HanaTourXMLLiveSettings();

        #endregion Global Variable Declaration

        #region XMLProvider Implementation Methods

        /// <summary>
        /// getProviderDefinition
        /// </summary>
        /// <param name="root">Root</param>
        protected override void getProviderDefinition(Root root)
        {
            root.ProviderDefinition.Name = configuration.ProviderName;
            root.ProviderDefinition.Currency = configuration.Currency;
        }

        /// <summary>
        /// Update Configuration
        /// </summary>
        /// <param name="configuration">Configuration</param>
        protected override void UpdateConfiguration(Sunhotels.Export.Configuration configuration)
        {
            configuration.WriteHotelsWithoutRooms = true;
            configuration.WriteRoomsWithoutAvailability = true;
        }

        /// <summary>
        /// Entry point for class.
        /// </summary>
        /// <param name="root">Root for Sunhotels XML structure.</param>
        protected override void getData(Root root)
        {
            try
            {
                this.root = root;
                // ------------------------------------------------------------------------------------------------
                // Create log stream
                // ------------------------------------------------------------------------------------------------
                try
                {
                    Directory.CreateDirectory(configuration.LogOutput);
                }
                catch (Exception ex)
                {
                    throw new ApplicationException(string.Join(": ", configuration.ProviderName, "Unable to create log file."), ex);
                }
                AddToConsole(string.Join(": ", configuration.ProviderName, "Begin static data import."));
                Log(ILoggerStaticDataSeverity.Info, string.Join(": ", configuration.ProviderName, "Begin static data import."));

                // Get all static data. If files are valid and able to process then this method will return true.
                if (!GetStaticData()) return;
                AddToConsole(string.Join(": ", configuration.ProviderName, "Begin processing list of hotels and places from available static data."));
                Log(ILoggerStaticDataSeverity.Info, string.Join(": ", configuration.ProviderName, "Begin processing list of hotels and places from available static data."));

                foreach (var item in _allHotels)
                    _allPlaces[item.Value.PlaceId].Hotels.Add(item.Value.Id, item.Value);
                AddToConsole(string.Join(": ", configuration.ProviderName, "Finished processing list of hotels and places from available static data."));
                Log(ILoggerStaticDataSeverity.Info, string.Join(": ", configuration.ProviderName, "Finished processing list of hotels and places from available static data."));
            }
            catch (Exception ex)
            {
                Log(ILoggerStaticDataSeverity.Error, string.Join(string.Empty, configuration.ProviderName, "Unhanded exception while getting data for static import: [", ex.ToString(), "]"));
                throw;
            }
        }

        /// <summary>
        /// Get Global Feature mapping for the provider
        /// </summary>
        /// <param name="root">Root</param>
        protected override void getFeatureTypes(Root root)
        {
            //Feature is not available as part of the HanaTour API response.
        }

        /// <summary>
        /// Log function
        /// </summary>
        /// <param name="severity">ILoggerStaticDataSeverity</param>
        /// <param name="message">message</param>
        /// <param name="args">arguments</param>
        public void Log(ILoggerStaticDataSeverity severity, string message, params object[] args)
        {
            var s = XML.HelpClasses.Log.MessageType.Informational;
            switch (severity)
            {
                case ILoggerStaticDataSeverity.Error:
                    s = XML.HelpClasses.Log.MessageType.Error;
                    break;
                case ILoggerStaticDataSeverity.Warning:
                    s = XML.HelpClasses.Log.MessageType.Warning;
                    break;
            }

            LogEntry(s, message, args);
        }

        #endregion XMLProvider Implementation Methods

        #region Private 

        #region Boolean

        /// <summary>
        /// Get all static data from excel files in FTP and bind with dictionary objects.
        /// </summary>              
        private bool GetStaticData()
        {
            try
            {
                //Loading settings information
                _hanaTourXmlLiveSettings = GetHanaTourXmlLiveSettings();
                Log(ILoggerStaticDataSeverity.Info, string.Join(": ", configuration.ProviderName, "Downloading and processing files containing provider static information begins."));
                //Downloading data file
                var filepath = DownloadFilesFromServer();
                if (string.IsNullOrEmpty(filepath))
                {
                    Log(ILoggerStaticDataSeverity.Info, string.Join(": ", configuration.ProviderName, "Required files are not available to create the cache and places."));
                    return false;
                }

                var fullPath = Path.GetFullPath(filepath);
                Log(ILoggerStaticDataSeverity.Info, string.Join(": ", configuration.ProviderName, "Converting excel file to DataTable at location", fullPath));
                DataTable dt = ConvertExcelToDataTable(fullPath);
                if (dt?.Rows == null)
                {
                    Log(ILoggerStaticDataSeverity.Info, string.Join(": ", configuration.ProviderName, "No data available to process in the excel sheet."));
                    return false;
                }
                Log(ILoggerStaticDataSeverity.Info, string.Join(": ", configuration.ProviderName, "Total records found in Excel Sheet", dt.Rows.Count));

                ProcessDataTable(dt);

                //Deleting data file after processing based on the configuration value
                if (_hanaTourXmlLiveSettings.DeleteFileAfterProcessing)
                    FileDelete(filepath);
                Log(ILoggerStaticDataSeverity.Info, string.Join(": ", configuration.ProviderName, "Total hotels created", _allHotels.Count));
                Log(ILoggerStaticDataSeverity.Info, string.Join(": ", configuration.ProviderName, "Downloading and processing files containing provider static information ends."));
                return true;
            }
            catch (Exception ex)
            {
                Log(ILoggerStaticDataSeverity.Error, string.Join(string.Empty, configuration.ProviderName, "Error occurred while downloading and processing static data file. Error: ", ex.Message));
                return false;
            }
        }

        #endregion Boolean

        #region Void

        /// <summary>
        /// File Delete
        /// </summary>
        /// <param name="filePath">filePath</param>
        private void FileDelete(string filePath)
        {
            try
            {
                if (!File.Exists(filePath)) return;
                File.Delete(filePath);
                Log(ILoggerStaticDataSeverity.Info, string.Join(": ", configuration.ProviderName, "Deleted static data file successfully. File Path", filePath));
            }
            catch (Exception ex)
            {
                Log(ILoggerStaticDataSeverity.Error, string.Join(": ", configuration.ProviderName, "Error occurred while deleting static data file. File Path", filePath, ". Error", ex.Message));
            }
        }

        /// <summary>
        /// Add message to Console
        /// </summary>
        /// <param name="message">message</param>
        private static void AddToConsole(string message)
        {
#if DEBUG
            Console.WriteLine(message);
#endif
        }

        /// <summary>
        /// Process DataTable with hotel static import data records
        /// </summary>
        /// <param name="dt">DataTable</param>
        private void ProcessDataTable(DataTable dt)
        {
            try
            {
                Log(ILoggerStaticDataSeverity.Info, string.Join(": ", configuration.ProviderName, "Converting data in hotels and places begins."));
                int skippedRows = 0;
                Parallel.ForEach(dt.AsEnumerable(), dr =>
                {
                    if(dr == null)
                        return;

                    var hotelId = dr[_hanaTourXmlLiveSettings.HotelCodeHeading].ToString();
                    if (string.IsNullOrEmpty(hotelId))
                    {
                        skippedRows++;
                        return;
                    }
                    
                    var hotelName = dr["HotelName"].Trim();
                    var hpAddress = dr["HP address"].TrimPrefix();
                    var localName = dr["HotelNameLocal"].Trim();                    
                    var description = string.Join(" ", localName, hpAddress).TrimPrefix();                                     
                    var cityName = dr["CityName"].Trim();
                    var cityCode = dr["CityCode"].Trim();
                    var stateCode = dr["StateCode"].Trim();
                    var stateName = dr["StateName"].Trim();
                    var countryName = dr["CountryName"].Trim();
                    var countryCode = dr["CountryCode"].Trim();
                    var address = dr["Address"].TrimPrefix();
                    var longitude = dr["Longitude"].RoundDecimal(_hanaTourXmlLiveSettings.GeoCodeDecimals);
                    var latitude = dr["Latitude"].RoundDecimal(_hanaTourXmlLiveSettings.GeoCodeDecimals); 
                    var grade = Convert.ToString(dr["Grade"], CultureInfo.InvariantCulture)?.Trim();
                    var phone = dr["Phone"].TrimPrefix();
                    var fax = dr["Fax"].TrimPrefix();

                    //Create the place
                    var place = GetPlace(countryName, countryCode, stateCode, stateName, cityName, cityCode);
                    if (place == null)
                        return;
                    var hotel = new Hotel
                    {
                        Id = cityCode + "|" +  hotelId,
                        PlaceId = place.Id,
                        Name = hotelName,
                        AccomodationType = AccomodationType.Hotel,
                        Headline = string.Empty,
                        Description = description,
                        Adress_Street1 = address,
                        Adress_Zipcode = string.Empty,
                        Adress_City = cityName,
                        Adress_State = stateName,
                        Adress_Country = countryName,
                        Position_Latitude = latitude ,
                        Position_Longitude = longitude,
                        Phone = phone,
                        Fax = fax,
                        Email = string.Empty,
                        Classification = grade,
                        BestBuy = false,
                        AdultOnly = false,
                        BlockInfant = false,
                        Identifiers = new[] { new Identifier { Type = _hanaTourXmlLiveSettings.IdentifierType, Value = configuration.ProviderName } }.ToList()
                    };

                    Hotel currentHotel;
                    if (!_allHotels.TryGetValue(hotel.Id, out currentHotel))
                    {
                        lock (_allHotels)
                            _allHotels.Add(hotel.Id, hotel);
                    }
                    else
                        Log(ILoggerStaticDataSeverity.Info, string.Join(": ", configuration.ProviderName, "The hotel with id", hotel.Id, " already exists in the hotel list."));
                });

                if (skippedRows > 0)
                    Log(ILoggerStaticDataSeverity.Info, string.Join(": ", configuration.ProviderName, "Skipped Rows due to insufficient data ", skippedRows));
                Log(ILoggerStaticDataSeverity.Info, string.Join(": ", configuration.ProviderName, "Converting data in hotels and places is complete."));
            }
            catch (Exception ex)
            {
                Log(ILoggerStaticDataSeverity.Error, string.Join(string.Empty, configuration.ProviderName, ": Error occurred while converting data in hotels and places. Error : ", ex.Message));
            }

        }

        #endregion Void

        #region String

        /// <summary>
        /// Download Files From Server
        /// </summary>
        /// <returns>File path</returns>
        private string DownloadFilesFromServer()
        {
            var filepath = string.Empty;
            try
            {
                try
                {
                    Directory.CreateDirectory(configuration.LocalTemp);
                }
                catch (Exception ex)
                {
                    throw new ApplicationException(string.Join(": ", configuration.ProviderName, ": Unable to create Temp folder."), ex);
                }

                Dictionary<string, string> directoryList = new Dictionary<string, string>();
                Log(ILoggerStaticDataSeverity.Info, string.Join(": ", configuration.ProviderName, "Begin connecting to FTP server."));
                AddToConsole(string.Join(": ", configuration.ProviderName, "Begin connecting to FTP server."));

                var ftpCom = new XML.FTPCommunication(configuration.UserId, configuration.Password);
                ftpCom.GetDirectoryList(configuration.WebServiceUrl, directoryList);

                Log(ILoggerStaticDataSeverity.Info, string.Join(": ", configuration.ProviderName, "FTP server connected successfully."));
                AddToConsole(string.Join(": ", configuration.ProviderName, "FTP server connected successfully."));

                //Connect to FTP server and fetch the data files 
                Log(ILoggerStaticDataSeverity.Info, string.Join(": ", configuration.ProviderName, "Downloading files from the FTP server."));
                AddToConsole(string.Join(": ", configuration.ProviderName, "Downloading files from the FTP server."));

                var fileExtensions = _hanaTourXmlLiveSettings.FileExtensions.Split(',');

                //We are expecting only one file in location. We are processing the first static import data file.
                //Validate file extension
                var fileInfo = directoryList
                     .FirstOrDefault(c => !string.IsNullOrEmpty(c.Value) &&
                     fileExtensions.All(f => c.Value.Contains(f)));
                if ( string.IsNullOrEmpty(fileInfo.Value) || string.IsNullOrEmpty(fileInfo.Key))
                    return string.Empty;

                string filePathInfo = string.Join(string.Empty, configuration.LocalTemp, fileInfo.Value);
                ftpCom.DownloadFile(fileInfo.Key, filePathInfo);

                Log(ILoggerStaticDataSeverity.Info, string.Join(": ", configuration.ProviderName, "Completed download of static data file."));
                return filePathInfo;
            }
            catch (Exception ex)
            {
                Log(ILoggerStaticDataSeverity.Error, string.Join(string.Empty, configuration.ProviderName, ": Error occurred while downloading static data files from FTP server. Error : ", ex.Message));
            }
            return filepath;
        }

        #endregion String

        #region Object

        /// <summary>
        /// Convert Excel file data to DataTable
        /// </summary>
        /// <param name="filePath">Full file path</param>
        /// <returns>DataTable</returns>
        public DataTable ConvertExcelToDataTable(string filePath)
        {
            try
            {
                using (var connection = new OleDbConnection())
                {
                    var connectionString = $"Provider=Microsoft.ACE.OLEDB.12.0;Data Source={filePath};Extended Properties=\"Excel 12.0 Xml; HDR=YES\"";
                    connection.ConnectionString = connectionString;
                    connection.Open();
                    DataTable dt = connection.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);

                    if (dt == null || dt.Rows.Count == 0 || !dt.Columns.Contains("TABLE_NAME"))
                        return null;
                    string sheetName = dt.Rows[0]["TABLE_NAME"].ToString();
                    return ReadTables(connection, sheetName); 
                }
            }
            catch (Exception ex)
            {
                Log(ILoggerStaticDataSeverity.Error, string.Join(": ", configuration.ProviderName, "Error while processing File ", filePath, " Error", ex.Message));
                AddToConsole(string.Join(": ", configuration.ProviderName, "Error while processing File ", filePath, " Error", ex.Message));
            }
            return null;
        }

        /// <summary>
        /// Fill the excel rows into a data table
        /// </summary>
        /// <param name="connection">connection</param>
        /// <param name="sheetName">sheetName</param>
        /// <returns>DataTable</returns>
        private DataTable ReadTables(OleDbConnection connection, string sheetName)
        {
            try
            {
                string columns = string.Join(string.Empty,"[" , string.Join("],[", GetColumnNames()) , "]");
                using (var command = new OleDbCommand())
                {
                    command.Connection = connection;
                    command.CommandText = string.Join(string.Empty, "SELECT ",columns, " FROM [", sheetName , "]");

                    using (var excelDataTable = new DataTable { Locale = CultureInfo.InvariantCulture })
                    {
                        using (var dataAdapter = new OleDbDataAdapter())
                        {
                            dataAdapter.SelectCommand = command;
                            dataAdapter.Fill(excelDataTable);
                        }
                        return excelDataTable;
                    }
                }
            }
            catch (Exception ex)
            {
                Log(ILoggerStaticDataSeverity.Error, string.Join(string.Empty,
                    configuration.ProviderName, "Error occurred while reading excel file with sheet name: ",
                    sheetName, ". Error: ", ex.Message));
                return null;
            }
        }

        private IEnumerable<string> GetColumnNames()
        {
            return new List<string>
            {
                _hanaTourXmlLiveSettings.HotelCodeHeading,"HotelName","HotelNameLocal","CityCode","CityName","StateCode","StateName","CountryCode","CountryName",
                "Address","Phone","Fax","Grade","Latitude","Longitude","HP address"
            };
        }

        /// <summary>
        /// Get HanaTourXMLLiveSettings
        /// </summary>
        /// <param name="fileName">fileName</param>
        /// <returns>HanaTourXMLLiveSettings</returns>
        private HanaTourXMLLiveSettings GetHanaTourXmlLiveSettings(string fileName = "HanaTourXMLLiveSettings")
        {
            try
            {
                return GeneralUtilities.Deserialize<HanaTourXMLLiveSettings>(XDocument.Load(string.Join(string.Empty, configuration.ProviderConfigRoot, "\\", fileName, ".xml")));
            }
            catch (Exception ex)
            {
                Log(ILoggerStaticDataSeverity.Error, string.Join(string.Empty,
                    configuration.ProviderName, "Error occurred while deserializing provider Live Settings xml file. File name: ",
                    fileName, ". Error: ", ex.Message));
                return null;
            }
        }

        /// <summary>
        /// Get previously created place or create new.
        /// </summary>
        /// <param name="countryName">country Name</param>
        /// <param name="countryCode">country Code</param>
        /// <param name="stateName">state Name</param>
        /// <param name="cityName">city Name</param>
        /// <param name="cityCode">city Code</param>
        /// <param name="stateCode">state Code</param>
        /// <returns>Place</returns>
        private Place GetPlace(string countryName, string countryCode, string stateCode, string stateName, string cityName, string cityCode)
        {
            try
            {
                if (string.IsNullOrEmpty(countryCode) || string.IsNullOrEmpty(cityCode))
                    return null;

                countryName = string.IsNullOrEmpty(countryName) ? countryCode : countryName;

                countryCode = countryCode.ToUpper().Trim();
                stateCode = stateCode.ToUpper().Trim();
                stateName = stateName.ToUpper().Trim();
                cityCode = cityCode.ToUpper().Trim();
                lock (_allPlaces)
                {
                    // Check or create country level place 
                    Place matchingCountryPlace;
                    if (!_allPlaces.TryGetValue(countryCode, out matchingCountryPlace))
                    {
                        var newPlace = new Place { Id = countryCode, Description = countryName };
                        _allPlaces.Add(countryCode, newPlace);
                        lock (root.Places)
                            root.Places.Add(newPlace);
                    }

                    bool validState = false;
                    if (!string.IsNullOrEmpty(stateCode) && !string.IsNullOrEmpty(stateName))
                    {
                        validState = true;
                        // Check or create state level place if country is specified
                        Place matchingStatePlace;
                        var statePlace = new Place { Id = stateCode, Description = stateName };
                        if (!_allPlaces.TryGetValue(stateCode, out matchingStatePlace))
                        {
                            _allPlaces.Add(stateCode, statePlace);
                            if (!_allPlaces[countryCode].Places.Contains(statePlace))
                                _allPlaces[countryCode].Places.Add(statePlace);
                        }
                    }

                    // Check or create city level place if state is specified
                    Place matchingCityPlace;
                    if (_allPlaces.TryGetValue(cityCode, out matchingCityPlace)) return matchingCityPlace;

                    var cityPlace = new Place { Id = cityCode, Description = cityName };
                    _allPlaces.Add(cityCode, cityPlace);

                    var key = validState ? stateCode : countryCode;
                    if (!_allPlaces[key].Places.Contains(cityPlace))
                        _allPlaces[key].Places.Add(cityPlace);
                }
                return _allPlaces[cityCode];
            }
            catch (Exception ex)
            {
                string errorMsg = string.Join(string.Empty, configuration.ProviderName, ": Error occurred while creating place with Country: ", countryCode, ", State:", stateCode, " City: ", cityCode, ". Error: ", ex.Message);
                Log(ILoggerStaticDataSeverity.Error, errorMsg);
                return null;
            }
        }

        #endregion Object

        #endregion
    }
}
