﻿using SunHotels.Providers.Infrastructure;
using SunHotels.XML.Data;
using System;
using System.Collections.Generic;

namespace SunHotels.Providers.Domain
{
    public class PeakWorkDiscountCalculator
    {
        public enum DiscountCreationType
        {
            ValuePercent = 1,
            ValueAmount = 2,
            EarlyBookingPercent = 3,
            EarlyBookingAmount = 4,
            PayStayAmount = 5,
            MinMaxPercent = 6
        }

        public static Dictionary<DiscountCreationType, Func<DiscountDataRow, Discount>> CreateDiscountFunctions
            = new Dictionary<DiscountCreationType, Func<DiscountDataRow, Discount>>()
            {
                { DiscountCreationType.ValuePercent, CreateValuePercentDiscountFunction() },
                { DiscountCreationType.ValueAmount, CreateValueAmountDiscountFunction()},
                { DiscountCreationType.EarlyBookingPercent, CreateEarlyBookingPercentDiscountFunction() },
                { DiscountCreationType.EarlyBookingAmount, CreateEarlyBookingAmountDiscountFunction() },
                { DiscountCreationType.PayStayAmount, CreatePayStayAmountDiscountFunction() },
                { DiscountCreationType.MinMaxPercent, CreateMinMaxPercentDiscountFunction() }
            };

        private static Func<DiscountDataRow, Discount> CreateMinMaxPercentDiscountFunction()
        {
            return (discount) => { return Discount.MinMaxPercent(discount.discount_min_days, discount.discount_max_days, Convert.ToDecimal(discount.discount_value), discount.discount_period_id == 0 ? (int?)null : discount.discount_period_id, discount.accumulativeGroup, discount.checkinBased, discount.includeMeal, discount.includeExtraBed); };
        }

        private static Func<DiscountDataRow, Discount> CreatePayStayAmountDiscountFunction()
        {
            return (discount) =>
            {
                decimal value = Convert.ToDecimal(Math.Floor(discount.discount_value));
                if (value < 1) return null;
                return Discount.PayStay(discount.discount_min_days, discount.discount_max_days == 0 ? null : (int?)(discount.discount_max_days / discount.discount_min_days), Convert.ToDecimal(discount.discount_value), discount.discount_period_id == 0 ? (int?)null : discount.discount_period_id, discount.accumulativeGroup, discount.checkinBased, discount.includeMeal, discount.includeExtraBed);
            };
        }

        private static Func<DiscountDataRow, Discount> CreateEarlyBookingAmountDiscountFunction()
        {
            return (discount) => { return Discount.EarlyBookingAmount(discount.discount_min_days, Convert.ToDecimal(discount.discount_value), discount.discount_period_id == 0 ? (int?)null : discount.discount_period_id, discount.accumulativeGroup, discount.checkinBased, discount.includeMeal, discount.includeExtraBed); };
        }

        private static Func<DiscountDataRow, Discount> CreateEarlyBookingPercentDiscountFunction()
        {
            return (discount) => { return Discount.EarlyBookingProcent(discount.discount_min_days, Convert.ToDecimal(discount.discount_value), discount.discount_period_id == 0 ? (int?)null : discount.discount_period_id, discount.accumulativeGroup, discount.checkinBased, discount.includeMeal, discount.includeExtraBed); };
        }

        private static Func<DiscountDataRow, Discount> CreateValueAmountDiscountFunction()
        {
            return (discount) => { return Discount.ValueAmount(discount.discount_min_days, Convert.ToDecimal(discount.discount_value), discount.discount_period_id == 0 ? (int?)null : discount.discount_period_id, discount.accumulativeGroup, discount.checkinBased, discount.includeMeal, discount.includeExtraBed); };
        }

        private static Func<DiscountDataRow, Discount> CreateValuePercentDiscountFunction()
        {
            return (discount) => { return Discount.ValuePercent(discount.discount_min_days, Convert.ToDecimal(discount.discount_value), discount.discount_period_id == 0 ? (int?)null : discount.discount_period_id, discount.accumulativeGroup, discount.checkinBased, discount.includeMeal, discount.includeExtraBed); };
        }      
    }
}
