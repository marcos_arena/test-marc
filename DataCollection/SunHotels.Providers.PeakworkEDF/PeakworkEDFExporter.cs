﻿using Sunhotels.Export;
using SunHotels.Export;
using SunHotels.XML.Data;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Schema;

namespace SunHotels.Providers
{
    public class PeakworkEDFExporter : BaseFileExporter
    {
        private const string SeasonIdentifier = "WS";

        private Root _root;
        private readonly PeakworkEDFSettings _localConfig;
        private readonly PeakworkEDFOptimizer _optimizer;
        private DateTime? _startDate;
        private DateTime? _endDate;
        private string _seasonStartDate;
        private string _seasonEndDate;
        private readonly ConcurrentDictionary<string, Hotel> _hotelList = new ConcurrentDictionary<string, Hotel>();
        private readonly ConcurrentDictionary<string, string> _notValidHotels = new ConcurrentDictionary<string, string>();
        private XmlReaderSettings _hotelSettings;
        private XmlReaderSettings _hotelAllotmentSettings;
        private readonly ConcurrentDictionary<string, List<string>> _roomTypeMapping = new ConcurrentDictionary<string, List<string>>();
        private readonly ConcurrentDictionary<string, Place> _resortIdDestination = new ConcurrentDictionary<string, Place>();
        private string _languageCode;
        private bool _isDeltaFile;

        public PeakworkEDFExporter(Configuration configuration, PeakworkEDFSettings localConfig, PeakworkEDFOptimizer optimizer, bool isDeltaFile) : base(configuration)
        {
            _localConfig = localConfig;
            _optimizer = optimizer;
            _isDeltaFile = isDeltaFile;
        }

        public override void Write(ExportData data)
        {
            try
            {
                Console.WriteLine("Write Xml EDF Start: " + DateTime.Now);

                _root = data as Root;

                SetDates();
                _seasonStartDate = (DateTime.Today.Year - 1) + "-11-01";
                _seasonEndDate = (DateTime.Today.Year + 1) + "-10-31";
                _languageCode = _localConfig.AgentLanguageCode != "ge" ? "EN" : "DE";

                //Loading XSD from the assembly
                var myAssembly = Assembly.GetExecutingAssembly();

                _hotelSettings = new XmlReaderSettings();
                _hotelSettings.ValidationType = ValidationType.Schema;
                _hotelSettings.ValidationEventHandler += ValidationEventHandler;
                _hotelSettings.ValidationFlags = XmlSchemaValidationFlags.ReportValidationWarnings;
                XmlSchema hotelSchema;
                XmlSchema hotelCommonSchema;
                using (var schemaStream = myAssembly.GetManifestResourceStream(_localConfig.HotelXSD))
                {
                    hotelSchema = XmlSchema.Read(schemaStream, ValidationEventHandler);
                }
                using (var schemaStream = myAssembly.GetManifestResourceStream(_localConfig.HotelCommonsXSD))
                {
                    hotelCommonSchema = XmlSchema.Read(schemaStream, ValidationEventHandler);
                }
                var includeSchema = new XmlSchemaInclude();
                includeSchema.Schema = hotelCommonSchema;
                hotelSchema.Includes.Add(includeSchema);
                _hotelSettings.Schemas.Add(hotelSchema);
                _hotelSettings.Schemas.Compile();

                _hotelAllotmentSettings = new XmlReaderSettings();
                _hotelAllotmentSettings.ValidationType = ValidationType.Schema;
                _hotelAllotmentSettings.ValidationEventHandler += ValidationEventHandler;
                _hotelAllotmentSettings.ValidationFlags = XmlSchemaValidationFlags.ReportValidationWarnings;
                using (var schemaStream = myAssembly.GetManifestResourceStream(_localConfig.HotelAllotmentXSD))
                {
                    var schema = XmlSchema.Read(schemaStream, null);
                    _hotelAllotmentSettings.Schemas.Add(schema);
                }
                _hotelAllotmentSettings.Schemas.Compile();

                foreach (var mapping in _localConfig.Mapping)
                {
                    foreach (var shRoomTypeId in mapping.Value)
                    {
                        List<string> mappingCodes;
                        if (!_roomTypeMapping.TryGetValue(shRoomTypeId, out mappingCodes))
                        {
                            mappingCodes = new List<string>();
                            _roomTypeMapping.TryAdd(shRoomTypeId, mappingCodes);
                        }
                        mappingCodes.Add(mapping.Key);
                    }
                }

                _root.Places.SelectMany(country => country.Places).ToList().ForEach(destination =>
                {
                    destination.Places.ForEach(resort =>
                    {
                        _resortIdDestination.TryAdd(resort.Id, destination);
                    });
                });

                var handledHotels = new ConcurrentBag<string>();
                Parallel.ForEach(_root.AllPlaces(), new ParallelOptions() { MaxDegreeOfParallelism = configuration.MaxDegreeOfParallelism }, p =>
                {
                    if (p.Hotels == null || !p.Hotels.Any())
                        return;

                    Parallel.ForEach(p.Hotels.Values, new ParallelOptions() { MaxDegreeOfParallelism = configuration.MaxDegreeOfParallelism }, hotel =>
                    {
                        Thread.CurrentThread.CurrentCulture = Thread.CurrentThread.CurrentUICulture = CultureInfo.InvariantCulture;
                        _hotelList.TryAdd(hotel.Id, hotel);
                        if (!handledHotels.Contains(hotel.Id))
                        {
                            handledHotels.Add(hotel.Id);
                            if (configuration.WriteHotelsWithoutRooms || hotel.Rooms.Any() || hotel.RoomGroups.Any())
                            {
                                if (!_isDeltaFile)
                                {
                                    var hotelRoot = string.Format(configuration.CacheOutput, _localConfig.TourOperatorCode, hotel.Id, _localConfig.MarketLabel);
                                    CreateXmlFile(WriteCacheXml, hotelRoot, true);
                                }

                                var hotelAllotmentRoot = string.Format(configuration.AvailabilityCacheOutput, _localConfig.TourOperatorCode, hotel.Id, _localConfig.MarketLabel);
                                CreateXmlFile(WriteAvailabilityCacheXml, hotelAllotmentRoot, true);
                            }
                        }
                    });
                });

                Console.WriteLine("Write Xml EDF Stop: " + DateTime.Now);
                Console.WriteLine("Write ZIP Start: " + DateTime.Now);
                CompressEDF();
                Console.WriteLine("Write ZIP Stop: " + DateTime.Now);
            }
            catch (Exception e)
            {
                LogException(e);
                throw e;
            }
        }

        private void LogException(Exception e)
        {
            lock (_localConfig.LogFileName)
            {
                var errorList = new List<string>();
                Exception innerException = e.InnerException;
                var i = 0;
                while (innerException != null && i < 10)
                {
                    errorList.Add(innerException.Message + innerException.StackTrace);
                    innerException = innerException.InnerException;
                    i++;
                }
                errorList.Add(e.Message + e.StackTrace);
                if (!Directory.Exists(configuration.LogOutput))
                {
                    Directory.CreateDirectory(configuration.LogOutput);
                }
                File.AppendAllLines(configuration.LogOutput + _localConfig.LogFileName, errorList);
            }
        }

        public override void Validate()
        {
            //XML already validated 'WriteCacheXML' and 'WriteAvailabilityCacheXML'
            if (_notValidHotels.Count > 0)
            {
                Console.WriteLine("The following hotels can't be exported:");
                foreach (var key in _notValidHotels.Keys)
                {

                    Console.WriteLine(key + ": " + _notValidHotels[key]);
                }
                throw new ApplicationException("Hotels with errors");
            }
            else
            {
                Console.WriteLine("All hotels exported succesfully to EDF file format.");
            }
        }

        /// <summary>
        /// Sets the start date and the end date and limits the exported data
        /// </summary>
        private void SetDates()
        {
            if (_root.AffectedStartDateAdjustment.HasValue)
            {
                _startDate = DateTime.Now.Date.AddDays(_root.AffectedStartDateAdjustment.Value);
            }
            if (_root.AffectedEndDateAdjustment.HasValue)
            {
                _endDate = DateTime.Now.Date.AddDays(_root.AffectedEndDateAdjustment.Value);
            }
        }

        private void GetValidDates(AvailabilityPeriod ap, out DateTime stDate, out DateTime enDate)
        {
            if (_startDate.HasValue)
            {
                stDate = _startDate.Value >= ap.DateFrom ? _startDate.Value : ap.DateFrom;
            }
            else
            {
                stDate = ap.DateFrom;
            }

            if (_endDate.HasValue)
            {
                enDate = _endDate.Value <= ap.DateTo ? _endDate.Value : ap.DateTo;
            }
            else
            {
                enDate = ap.DateTo;
            }
        }

        /// <summary>
        /// Translate SunHotels Board code to EDF Board code
        /// AO - Accommodation only
        /// BB - Bed and Breakfast
        /// HB - Half Board
        /// FB - Full Board
        /// AI - All Inclusive
        /// </summary>
        /// <param name="sunHotelsBoard"></param>
        /// <returns></returns>
        private static string TranslateBoard(string sunHotelsBoard)
        {
            switch (sunHotelsBoard.ToLower())
            {
                case "breakfast": return "BB";
                case "half_board": return "HB";
                case "full_board": return "FB";
                case "all_inclusive": return "AI";
                default: return "AO";
            }
        }

        private void CompressEDF()
        {
            var zipName = _localConfig.ZipName;
            if (string.IsNullOrEmpty(zipName)) zipName = _localConfig.TourOperatorCode;

            var destinationFolder = configuration.CacheOutput.Substring(0, configuration.CacheOutput.IndexOf(configuration.Provider)) + configuration.Provider + @"\";

            if (!_localConfig.PreventZip)
            {
                var zipFileName = destinationFolder + zipName + ".zip";
                var zipFolder = destinationFolder + @"hotels\";

                if (!Directory.Exists(zipFolder)) return;

                if (File.Exists(zipFileName))
                {
                    File.Delete(zipFileName);
                }
                if (_localConfig.ZipFolderUnix)
                {
                    ZipFile.CreateFromDirectory(zipFolder, zipFileName, CompressionLevel.Optimal, true, new UnixEncoder());
                }
                else
                {
                    ZipFile.CreateFromDirectory(zipFolder, zipFileName, CompressionLevel.Optimal, true);
                }
                Directory.Delete(zipFolder, true);

            }

            var fileDone = File.Create($"{destinationFolder}{zipName}-{_localConfig.MarketLabel}-{configuration.UserId}.done");
            fileDone.WriteByte(Convert.ToByte(1));
            fileDone.Close();
        }

        private class UnixEncoder : UTF8Encoding
        {
            public UnixEncoder() : base(true)
            {

            }
            public override byte[] GetBytes(string s)
            {
                s = s.Replace("\\", "/");
                return base.GetBytes(s);
            }
        }

        private static string ApplyRoundPrice(string field)
        {
            var txt = String.Empty;
            if (!String.IsNullOrEmpty(field))
            {
                var amount = Decimal.Round(Decimal.Parse(field, NumberFormatInfo.InvariantInfo), 2, MidpointRounding.AwayFromZero);
                txt = amount.ToString(NumberFormatInfo.InvariantInfo);
            }
            return txt;
        }

        public static string GetXmlSafeOutput(string value)
        {
            if (string.IsNullOrEmpty(value)) return string.Empty;
            var xmlSafeOutput = System.Web.HttpUtility.HtmlDecode(value);
            return xmlSafeOutput.Trim();
        }

        #region CacheXML functions

        /// <summary>
        /// Gets the hotel id from the file name
        /// </summary>
        /// <param name="writer"></param>
        /// <returns></returns>
        private string WriterHotelId(XmlTextWriter writer)
        {
            var indexFileName = _localConfig.TourOperatorCode + "-";
            var file = writer.BaseStream as FileStream;
            var idHotelIni = file.Name.LastIndexOf(indexFileName) + indexFileName.Length;
            var idHotelEnd = file.Name.LastIndexOf(".");
            return file.Name.Substring(idHotelIni, idHotelEnd - idHotelIni);
        }

        private void CreateOccupancyFactor(XmlDocument doc, XmlElement nGuestCharges, RoomType roomType, RoomConfigurationExtrabedFactors roomExtrabedFactor, int maxChildAgeInclusive, string startDate, string endDate)
        {
            var personType = maxChildAgeInclusive < roomExtrabedFactor.FromAgeInclusive ? "A" : "C";

            var nGuestCharge = doc.CreateElement("GuestCharge");
            nGuestCharge.SetAttribute("Start", startDate);
            nGuestCharge.SetAttribute("End", endDate);
            nGuestCharge.SetAttribute("Exclusive", "false");
            if (personType.Equals("C"))
            {
                nGuestCharge.SetAttribute("MinAge", roomExtrabedFactor.FromAgeInclusive.ToString());
                nGuestCharge.SetAttribute("MaxAge", (roomExtrabedFactor.ToAgeExclusive - 1).ToString());
            }
            nGuestCharge.SetAttribute("MinCount", roomExtrabedFactor.FromQuantityInclusive.ToString());
            nGuestCharge.SetAttribute("MaxCount", roomExtrabedFactor.ToQuantityInclusive.ToString());
            //Adult 'A', Child 'C'
            nGuestCharge.SetAttribute("PersonType", personType);
            nGuestCharge.SetAttribute("Type", "ExtraDay");
            nGuestCharge.SetAttribute("Percent", decimal.Round((1 - roomExtrabedFactor.Value) * -100, 2, MidpointRounding.AwayFromZero).ToString(NumberFormatInfo.InvariantInfo));
            if (personType.Equals("C"))
            {
                //Child absolute count, adults relative count (depends on MinChargedPersons)
                var nGuestRestrictions = doc.CreateElement("GuestRestrictions");
                var nGuestRestriction = doc.CreateElement("GuestRestriction");
                nGuestRestriction.SetAttribute("MinPerson", roomType.Beds.ToString());
                nGuestRestriction.SetAttribute("PersonType", "B");
                nGuestRestrictions.AppendChild(nGuestRestriction);
                nGuestCharge.AppendChild(nGuestRestrictions);
            }
            nGuestCharges.AppendChild(nGuestCharge);
        }

        /// <summary>
        /// Creates all the xml nodes to write the price of the extra bed
        /// </summary>
        private static void CreatePriceExtraBed(XmlDocument doc, XmlElement nGuestCharges, RoomType roomType, AvailabilityPeriod ap, string personType, string startDate, string endDate)
        {
            var nGuestCharge = doc.CreateElement("GuestCharge");
            nGuestCharge.SetAttribute("Start", startDate);
            nGuestCharge.SetAttribute("End", endDate);
            nGuestCharge.SetAttribute("Amount", personType.Equals("C") ? ap.Pricing_ExtrabedChild : ap.Pricing_ExtrabedAdult);
            nGuestCharge.SetAttribute("Exclusive", "false");
            nGuestCharge.SetAttribute("MinCount", "1");
            nGuestCharge.SetAttribute("MaxCount", roomType.ExtraBeds.ToString());
            //Adult 'A', Child 'C'
            nGuestCharge.SetAttribute("PersonType", personType);
            nGuestCharge.SetAttribute("Type", "ExtraDay");
            if (personType.Equals("C"))
            {
                //Child absolute count, adults relative count (depends on MinChargedPersons)
                var nGuestRestrictions = doc.CreateElement("GuestRestrictions");
                var nGuestRestriction = doc.CreateElement("GuestRestriction");
                nGuestRestriction.SetAttribute("MinPerson", roomType.Beds.ToString());
                nGuestRestriction.SetAttribute("PersonType", "B");
                nGuestRestrictions.AppendChild(nGuestRestriction);
                nGuestCharge.AppendChild(nGuestRestrictions);
            }
            nGuestCharges.AppendChild(nGuestCharge);
        }

        /// <summary>
        /// Create xml nodes of the board charge
        /// </summary>
        private static void CreateBoardCharge(XmlDocument doc, XmlElement nBoardCharges, RoomType roomType, AvailabilityPeriod.AdditionalBoard ab, string personType, bool mealSupplementRestriction, string startDate, string endDate)
        {
            var nBoardCharge = doc.CreateElement("BoardCharge");
            nBoardCharge.SetAttribute("Start", startDate);
            nBoardCharge.SetAttribute("End", endDate);
            //Adult 'A', Child 'C'
            nBoardCharge.SetAttribute("Amount", ApplyRoundPrice(personType.Equals("C") ? ab.Child : ab.Adult));
            nBoardCharge.SetAttribute("Board", TranslateBoard(ab.Type));
            if (mealSupplementRestriction)
            {
                nBoardCharge.SetAttribute("MinCount", "1");
                nBoardCharge.SetAttribute("MaxCount", roomType.ExtraBeds.ToString());
                if (personType.Equals("C"))
                {
                    //Child absolute count, adults relative count (depends on MinChargedPersons)
                    var nGuestRestrictions = doc.CreateElement("GuestRestrictions");
                    var nGuestRestriction = doc.CreateElement("GuestRestriction");
                    nGuestRestriction.SetAttribute("MinPerson", roomType.Beds.ToString());
                    nGuestRestriction.SetAttribute("PersonType", "B");
                    nGuestRestrictions.AppendChild(nGuestRestriction);
                    nBoardCharge.AppendChild(nGuestRestrictions);
                }
            }
            nBoardCharge.SetAttribute("PersonType", personType);
            nBoardCharges.AppendChild(nBoardCharge);
        }

        /// <summary>
        /// Create xml nodes of the board charge
        /// </summary>
        private static void CreateBoardChargeZero(XmlDocument doc, XmlElement nBoardCharges, string boardType, string startDate, string endDate)
        {
            var nBoardCharge = doc.CreateElement("BoardCharge");
            nBoardCharge.SetAttribute("Start", startDate);
            nBoardCharge.SetAttribute("End", endDate);
            nBoardCharge.SetAttribute("Amount", "0.00");
            nBoardCharge.SetAttribute("Board", TranslateBoard(boardType));
            nBoardCharges.AppendChild(nBoardCharge);
        }

        /// <summary>
        /// Create xml nodes of the board extra
        /// </summary>
        private static void CreateBoardExtra(XmlDocument doc, XmlElement nExtras, RoomType roomType, AvailabilityPeriod.AdditionalBoard ab, int index, string startDate, string endDate)
        {
            var nBoard = doc.CreateElement("Extra");
            //'EC' - ExtraCharge - additional charge
            nBoard.SetAttribute("GlobalType", "EC");
            nBoard.SetAttribute("ApplianceCode", "ECB");
            nBoard.SetAttribute("Type", "ECB" + index.ToString());

            var nBoardRestrictions = doc.CreateElement("Restrictions");
            nBoard.AppendChild(nBoardRestrictions);
            var nBoardRestriction = doc.CreateElement("Restriction");
            nBoardRestrictions.AppendChild(nBoardRestriction);

            var nBoardDateRestrictions = doc.CreateElement("DateRestrictions");
            nBoardDateRestrictions.SetAttribute("Condition", "true");
            nBoardRestriction.AppendChild(nBoardDateRestrictions);
            var nBoardDateRestriction = doc.CreateElement("DateRestriction");
            nBoardDateRestriction.SetAttribute("Start", startDate);
            nBoardDateRestriction.SetAttribute("End", endDate);
            nBoardDateRestriction.SetAttribute("ApplyTo", "Stay");
            nBoardDateRestrictions.AppendChild(nBoardDateRestriction);

            var nBoardTypeRestrictions = doc.CreateElement("BoardRestrictions");
            nBoardTypeRestrictions.SetAttribute("Condition", "false");
            nBoardRestriction.AppendChild(nBoardTypeRestrictions);
            var nBoardTypeRestriction = doc.CreateElement("BoardRestriction");
            nBoardTypeRestriction.SetAttribute("Code", TranslateBoard(ab.Type));
            nBoardTypeRestrictions.AppendChild(nBoardTypeRestriction);

            var nBoardCharges = doc.CreateElement("Charges");
            nBoard.AppendChild(nBoardCharges);

            //MealSupplementRestriction
            var nBoardCharge = doc.CreateElement("Charge");
            nBoardCharges.AppendChild(nBoardCharge);
            var boardBasePrice = (Convert.ToDecimal(ab.Adult ?? (ab.Child ?? "0")) * roomType.Beds).ToString(CultureInfo.InvariantCulture);
            nBoardCharge.SetAttribute("Amount", ApplyRoundPrice(boardBasePrice));
            //'OS' - per object and stay, 'ON' - per object and night
            nBoardCharge.SetAttribute("Calculation", "ON");

            // type of Appliances
            var appliancesNode = doc.CreateElement("Appliances");
            var applianceNodeBase = doc.CreateElement("Appliance");
            applianceNodeBase.SetAttribute("Type", "Charge");
            applianceNodeBase.SetAttribute("Code", "ExtraDay");
            appliancesNode.AppendChild(applianceNodeBase);
            nBoardCharge.AppendChild(appliancesNode);

            nExtras.AppendChild(nBoard);
        }

        private List<Room> HotelRooms(Hotel hotel, Dictionary<string, string> roomGroups)
        {
            var allRooms = new List<Room>();
            foreach (var rg in hotel.RoomGroups.Values)
            {
                // Check that a room is avaliable, as roomgroups can't exist without a room
                if (!configuration.WriteRoomsWithoutAvailability && !rg.Rooms.Values.Any(r => r.AvailabilityPeriods.Count > 0))
                {
                    continue;
                }
                foreach (var r in rg.Rooms.Values)
                {
                    allRooms.Add(r);
                    roomGroups.Add(r.RoomId, rg.RoomGroupId);
                }
            }
            foreach (var r in hotel.Rooms.Values)
            {
                if (!configuration.WriteRoomsWithoutAvailability && r.AvailabilityPeriods.Count == 0)
                {
                    continue;
                }
                allRooms.Add(r);
            }
            return allRooms;
        }

        private void CreateBoardNode(XmlDocument doc, XmlElement nBoards, string shCode)
        {
            string description;
            switch (shCode.ToLower())
            {
                case "breakfast": description = "Bed and Breakfast"; break;
                case "half_board": description = "Half Board"; break;
                case "full_board": description = "Full Board"; break;
                case "all_inclusive": description = "All Inclusive"; break;
                default: description = "Accomodation Only"; break;
            }

            string intCode;
            switch (shCode.ToLower())
            {
                case "breakfast": intCode = "3"; break;
                case "half_board": intCode = "4"; break;
                case "full_board": intCode = "5"; break;
                case "all_inclusive": intCode = "6"; break;
                default: intCode = "1"; break;
            }

            var edfBoard = TranslateBoard(shCode);
            var nBoard = doc.CreateElement("Board");
            nBoard.SetAttribute("BoardKey", intCode);
            nBoard.SetAttribute("Code", edfBoard);
            nBoard.SetAttribute("GlobalType", edfBoard);
            var nDescriptions = doc.CreateElement("Descriptions");
            var nDescription = doc.CreateElement("Description");
            nDescription.SetAttribute("Language", "EN");
            nDescription.AppendChild(doc.CreateTextNode(description));
            nDescriptions.AppendChild(nDescription);
            nBoard.AppendChild(nDescriptions);
            nBoards.AppendChild(nBoard);
        }

        private void CreateDiscountNode(XmlDocument doc, XmlElement nExtras, Discount discount, Dictionary<int, IEnumerable<int>> compatibleGroups, int index, string startDate, string endDate)
        {
            if (discount.Amount <= 0 && discount.Percent <= 0)
            {
                return;
            }

            var nDiscount = doc.CreateElement("Extra");
            //'EB' - EarlyBird - Early Bird discount (Early Booking)
            //'ID' - Incentive - Duration - 7 = 6
            //'IC' - Incentive - Other
            switch (discount.Type)
            {
                case DiscountType.EarlyBooking:
                    nDiscount.SetAttribute("GlobalType", "EB");
                    nDiscount.SetAttribute("ApplianceCode", "EBO");
                    nDiscount.SetAttribute("Type", "EBO" + index.ToString());
                    break;

                case DiscountType.MinMax:
                    nDiscount.SetAttribute("GlobalType", "IC");
                    nDiscount.SetAttribute("ApplianceCode", "ICM");
                    nDiscount.SetAttribute("Type", "ICM" + index.ToString());
                    break;

                case DiscountType.PayStay:
                    nDiscount.SetAttribute("GlobalType", "ID");
                    nDiscount.SetAttribute("ApplianceCode", "IDU");
                    nDiscount.SetAttribute("Type", "IDU" + index.ToString());
                    break;

                case DiscountType.Value:
                    nDiscount.SetAttribute("GlobalType", "IC");
                    nDiscount.SetAttribute("ApplianceCode", "ICV");
                    nDiscount.SetAttribute("Type", "ICV" + index.ToString());
                    break;
            }
            nDiscount.SetAttribute("CombiGroupId", discount.AccumulativeGroup.ToString());
            nDiscount.SetAttribute("SortIndex", index.ToString());
            nExtras.AppendChild(nDiscount);

            var nDiscountRestrictions = doc.CreateElement("Restrictions");
            nDiscount.AppendChild(nDiscountRestrictions);
            var nDiscountRestriction = doc.CreateElement("Restriction");
            nDiscountRestrictions.AppendChild(nDiscountRestriction);

            var nDiscountDateRestrictions = doc.CreateElement("DateRestrictions");
            nDiscountDateRestrictions.SetAttribute("Condition", "true");
            nDiscountRestriction.AppendChild(nDiscountDateRestrictions);

            var nDiscountDateRestriction = doc.CreateElement("DateRestriction");
            nDiscountDateRestrictions.AppendChild(nDiscountDateRestriction);
            nDiscountDateRestriction.SetAttribute("Start", startDate);
            nDiscountDateRestriction.SetAttribute("End", endDate);

            if (discount.Type != DiscountType.MinMax && discount.MaxPeriods.HasValue)
            {
                nDiscountDateRestriction.SetAttribute("ExtraMaxApply", discount.MaxPeriods.Value.ToString());
            }

            if (discount.Type == DiscountType.EarlyBooking)
            {
                nDiscountDateRestriction.SetAttribute("ApplyTo", discount.CheckinBased ? "Arrival" : "Within");

                if (!discount.LastDate.HasValue)
                {
                    nDiscountDateRestriction.SetAttribute("MinBookingOffset", "P" + discount.MinDays + "D");
                }
                else
                {
                    nDiscountDateRestriction.SetAttribute("BookingEnd", discount.LastDate.Value.ToString("yyyy-MM-dd"));
                }
            }
            else
            {
                nDiscountDateRestriction.SetAttribute("ApplyTo", discount.CheckinBased ? "Arrival" : "Stay");

                var nDiscountStayRestrictions = doc.CreateElement("StayRestrictions");
                nDiscountStayRestrictions.SetAttribute("Condition", "false");
                var nDiscountStayRestriction = doc.CreateElement("StayRestriction");
                nDiscountStayRestriction.SetAttribute("Min", "P" + discount.MinDays + "D");
                if (discount.Type == DiscountType.MinMax && discount.MaxDays.HasValue)
                {
                    nDiscountStayRestriction.SetAttribute("Max", "P" + discount.MaxDays.Value + "D");
                }
                nDiscountStayRestrictions.AppendChild(nDiscountStayRestriction);
                nDiscountRestriction.AppendChild(nDiscountStayRestrictions);
            }

            var nCompatibleExtras = doc.CreateElement("CompatibleExtras");
            var nCompWith = doc.CreateElement("CompatibleWith");
            nCompWith.SetAttribute("GroupId", discount.AccumulativeGroup.ToString());
            nCompWith.SetAttribute("Invert", "true");
            nCompatibleExtras.AppendChild(nCompWith);
            foreach (var groupId in compatibleGroups.Keys)
            {
                nCompWith = doc.CreateElement("CompatibleWith");
                nCompWith.SetAttribute("GroupId", groupId.ToString());
                nCompatibleExtras.AppendChild(nCompWith);
            }
            nDiscount.AppendChild(nCompatibleExtras);

            var nDiscountReductions = doc.CreateElement("Reductions");
            var nDiscountReduction = doc.CreateElement("Reduction");
            XmlAttribute nDiscountAmount = null;
            XmlAttribute nDiscountPercent = null;
            if (discount.Amount > 0)
            {
                nDiscountAmount = doc.CreateAttribute(discount.Type == DiscountType.PayStay ? "DiscountedNights" : "Amount");
                nDiscountAmount.Value = discount.Amount.ToString(CultureInfo.InvariantCulture);
            }
            else
            {
                nDiscountPercent = doc.CreateAttribute("Percent");
                nDiscountPercent.Value = discount.Percent.ToString(CultureInfo.InvariantCulture);
            }
            //'OS' - per object and stay, 'ON' - per object and night
            nDiscountReduction.SetAttribute("Calculation", "ON");

            if (discount.Amount > 0)
            {
                nDiscountReduction.Attributes.Append(nDiscountAmount);
                if (discount.Type == DiscountType.PayStay)
                {
                    //'Cheap' - the lowest-cost day or days are reduced (example 7=6, 14=12)
                    nDiscountReduction.SetAttribute("Mode", "Cheap");
                    nDiscountReduction.SetAttribute("Nights", discount.MinDays.ToString());
                    nDiscountReduction.SetAttribute("Percent", "100");
                }
            }
            else
            {
                nDiscountReduction.Attributes.Append(nDiscountPercent);
            }

            // type of Appliances
            var appliancesNode = doc.CreateElement("Appliances");
            var applianceNodeBase = doc.CreateElement("Appliance");
            applianceNodeBase.SetAttribute("Type", "Charge");
            applianceNodeBase.SetAttribute("Code", "ExtraDay");
            appliancesNode.AppendChild(applianceNodeBase);

            if (discount.IncludeExtraBed)
            {
                var applianceNode = doc.CreateElement("Appliance");
                applianceNode.SetAttribute("Type", "Person");
                applianceNode.SetAttribute("Code", "B");
                appliancesNode.AppendChild(applianceNode);

                applianceNode = doc.CreateElement("Appliance");
                applianceNode.SetAttribute("Type", "Person");
                applianceNode.SetAttribute("Code", "N");
                appliancesNode.AppendChild(applianceNode);
            }

            if (discount.IncludeMeal)
            {
                var applianceNode = doc.CreateElement("Appliance");
                applianceNode.SetAttribute("Type", "Charge");
                applianceNode.SetAttribute("Code", "Board");
                appliancesNode.AppendChild(applianceNode);
            }

            foreach (var typeId in compatibleGroups.Where(kvp => kvp.Key > discount.AccumulativeGroup).SelectMany(kvp => kvp.Value).Distinct())
            {
                if (
                    discount.TypeId != typeId
                    //Check amount types and percentage types
                    && (discount.TypeId == 2 || discount.TypeId == 4 || discount.TypeId == 5)
                    && (typeId == 1 || typeId == 3 || typeId == 6)
                )
                {
                    string groupCode;
                    switch (typeId)
                    {
                        case 3:
                        case 4: groupCode = "EBO"; break; //DiscountType.EarlyBooking
                        case 6: groupCode = "ICM"; break; //DiscountType.MinMax
                        case 5: groupCode = "IDU"; break; //DiscountType.PayStay
                        default: groupCode = "ICV"; break; //DiscountType.Value
                    }
                    var applianceNode = doc.CreateElement("Appliance");
                    applianceNode.SetAttribute("Type", "Extra");
                    applianceNode.SetAttribute("Code", groupCode);
                    appliancesNode.AppendChild(applianceNode);
                }
            }

            nDiscountReduction.AppendChild(appliancesNode);
            nDiscountReductions.AppendChild(nDiscountReduction);
            nDiscount.AppendChild(nDiscountReductions);
        }

        public string CodeWithRate(string code, bool isNonRefundable)
        {
            return code + "." + (isNonRefundable ? "NRF" : "STD");
        }

        #endregion

        #region CacheXML

        private void WriteCacheXml(XmlTextWriter writer)
        {
            var idHotel = WriterHotelId(writer);
            Hotel currentHotel;
            _hotelList.TryGetValue(idHotel, out currentHotel);
            if (currentHotel == null)
            {
                return;
            }

            //EDF document
            var doc = new XmlDocument();
            var nHotelRoot = doc.CreateElement("HotelRoot");
            nHotelRoot.SetAttribute("xmlns", _localConfig.HotelRootNamespace);
            nHotelRoot.SetAttribute("SchemaVersion", _localConfig.EDFVersionHotel);
            doc.AppendChild(nHotelRoot);

            #region BasicData

            var nBasicData = doc.CreateElement("BasicData");
            nBasicData.SetAttribute("Code", currentHotel.Id);
            nBasicData.SetAttribute("TourOperatorCode", _localConfig.TourOperatorCode);
            nBasicData.SetAttribute("Usage", "HotelOnly");
            nBasicData.SetAttribute("Source", _localConfig.TourOperatorCode);
            nHotelRoot.AppendChild(nBasicData);

            //Hotel's name
            var nName = doc.CreateElement("Name");
#if DEBUG
            nName.AppendChild(doc.CreateTextNode("TEST - " + currentHotel.Name));
#else
            nName.AppendChild(doc.CreateTextNode(currentHotel.Name));
#endif
            nBasicData.AppendChild(nName);

            var nReferences = doc.CreateElement("References");
            var nMainHotel = doc.CreateElement("MainHotel");
            var nGiataCode = doc.CreateElement("GiataCode");
            nMainHotel.AppendChild(doc.CreateTextNode(currentHotel.Id));
            nGiataCode.AppendChild(doc.CreateTextNode(currentHotel.Identifiers[0].Value));
            nReferences.AppendChild(nMainHotel);
            nReferences.AppendChild(nGiataCode);
            nBasicData.AppendChild(nReferences);

            //Hotel's address
            if (
                !string.IsNullOrEmpty(currentHotel.Adress_Street1)
                || !string.IsNullOrEmpty(currentHotel.Adress_Street2)
                || !string.IsNullOrEmpty(currentHotel.Adress_Zipcode)
                || !string.IsNullOrEmpty(currentHotel.Adress_City)
                || !string.IsNullOrEmpty(currentHotel.Adress_Country)
                || !string.IsNullOrEmpty(currentHotel.Phone)
                || !string.IsNullOrEmpty(currentHotel.Fax)
            )
            {
                var nAddress = doc.CreateElement("Address");

                if (!string.IsNullOrEmpty(currentHotel.Adress_Street1) || !string.IsNullOrEmpty(currentHotel.Adress_Street2))
                {
                    var nStreet = doc.CreateElement("Street");
                    var street = currentHotel.Adress_Street1 + (!string.IsNullOrEmpty(currentHotel.Adress_Street2) ? " " + currentHotel.Adress_Street2 : "");
                    nStreet.AppendChild(doc.CreateTextNode(GetXmlSafeOutput(street)));
                    nAddress.AppendChild(nStreet);
                }

                if (!string.IsNullOrEmpty(currentHotel.Adress_Zipcode))
                {
                    var nZipCode = doc.CreateElement("ZipCode");
                    nZipCode.AppendChild(doc.CreateTextNode(currentHotel.Adress_Zipcode));
                    nAddress.AppendChild(nZipCode);
                }

                if (!string.IsNullOrEmpty(currentHotel.Adress_City))
                {
                    var nCity = doc.CreateElement("City");
                    nCity.AppendChild(doc.CreateTextNode(GetXmlSafeOutput(currentHotel.Adress_City)));
                    nAddress.AppendChild(nCity);
                }

                if (!string.IsNullOrEmpty(currentHotel.Adress_Country))
                {
                    var nCountry = doc.CreateElement("Country");
                    nCountry.AppendChild(doc.CreateTextNode(GetXmlSafeOutput(currentHotel.Adress_Country)));
                    nAddress.AppendChild(nCountry);
                }

                if (!string.IsNullOrEmpty(currentHotel.Phone))
                {
                    var nPhone = doc.CreateElement("Phone");
                    nPhone.AppendChild(doc.CreateTextNode(currentHotel.Phone));
                    nAddress.AppendChild(nPhone);
                }

                if (!string.IsNullOrEmpty(currentHotel.Fax))
                {
                    var nFax = doc.CreateElement("Fax");
                    nFax.AppendChild(doc.CreateTextNode(currentHotel.Fax));
                    nAddress.AppendChild(nFax);
                }

                nBasicData.AppendChild(nAddress);
            }

            //Hotel's location
            if (
                !string.IsNullOrEmpty(currentHotel.Adress_State)
                || !string.IsNullOrEmpty(currentHotel.Position_Latitude)
                || !string.IsNullOrEmpty(currentHotel.Position_Longitude)
            )
            {
                var nGeoInfos = doc.CreateElement("GeoInfos");

                if (!string.IsNullOrEmpty(currentHotel.Adress_State))
                {
                    var nLocation = doc.CreateElement("Location");
                    nLocation.SetAttribute("Name", GetXmlSafeOutput(currentHotel.Adress_State));
                    nLocation.SetAttribute("Type", "Region");
                    nGeoInfos.AppendChild(nLocation);
                }

                if (!string.IsNullOrEmpty(currentHotel.Position_Latitude) || !String.IsNullOrEmpty(currentHotel.Position_Longitude))
                {
                    var nGeoCode = doc.CreateElement("GeoCode");
                    if (!string.IsNullOrEmpty(currentHotel.Position_Latitude))
                    {
                        nGeoCode.SetAttribute("Latitude", currentHotel.Position_Latitude);
                    }
                    if (!string.IsNullOrEmpty(currentHotel.Position_Longitude))
                    {
                        nGeoCode.SetAttribute("Longitude", currentHotel.Position_Longitude);
                    }
                    nGeoInfos.AppendChild(nGeoCode);
                }

                nBasicData.AppendChild(nGeoInfos);
            }

            //Airports
            Place hotelDestination;
            if (_resortIdDestination.TryGetValue(currentHotel.PlaceId, out hotelDestination) && hotelDestination.Codes != null && hotelDestination.Codes.Any(c => c.Type == "IATA"))
            {
                var nArrivalAirports = doc.CreateElement("ArrivalAirports");
                foreach (var code in hotelDestination.Codes.Where(c => c.Type == "IATA"))
                {
                    var nAirport = doc.CreateElement("Airport");
                    nAirport.SetAttribute("IataCode", code.Value);
                    nArrivalAirports.AppendChild(nAirport);
                }
                nBasicData.AppendChild(nArrivalAirports);
            }

            //Hotel's attributes
            if (!String.IsNullOrEmpty(currentHotel.Classification))
            {
                var nAttributes = doc.CreateElement("Attributes");
                var nAttribute = doc.CreateElement("Attribute");
                nAttribute.SetAttribute("Name", "Category");
                if (currentHotel.Classification.IndexOf('+') > 0)
                {
                    nAttribute.SetAttribute("Value", currentHotel.Classification.Substring(0, 1) + ".5");
                }
                else
                {
                    nAttribute.SetAttribute("Value", currentHotel.Classification);
                }
                nAttributes.AppendChild(nAttribute);
                nBasicData.AppendChild(nAttributes);
            }

            #endregion

            #region SellingData

            var nSellingData = doc.CreateElement("SellingData");
            nSellingData.SetAttribute("Currency", _root.ProviderDefinition.Currency);
            nSellingData.SetAttribute("CompatibleExtrasMode", "Flexible");
            nHotelRoot.AppendChild(nSellingData);

            var nRounding = doc.CreateElement("Rounding");
            nRounding.SetAttribute("Mode", "Up");
            nRounding.SetAttribute("Scope", "Hotel");
            nSellingData.AppendChild(nRounding);

            var nSeason = doc.CreateElement("SeasonDefinitions");
            nSeason.SetAttribute("Start", _seasonStartDate);
            nSeason.SetAttribute("End", _seasonEndDate);
            nSeason.SetAttribute("Identifier", SeasonIdentifier);
            nSellingData.AppendChild(nSeason);

            //Relationship between roomID -> roomGroupID
            var roomGroups = new Dictionary<string, string>();

            //All rooms in one list
            var allRooms = HotelRooms(currentHotel, roomGroups);

            //Rooms node
            var nRooms = doc.CreateElement("Rooms");
            nSellingData.AppendChild(nRooms);

            //Each room
            foreach (var r in allRooms)
            {
                RoomType roomType;
                if (!_root.RoomTypes.TryGetValue(r.TypeId, out roomType))
                {
                    continue;
                }

                List<string> mappingCodes;
                if (!_roomTypeMapping.TryGetValue(roomType.Type_Id, out mappingCodes))
                {
                    continue;
                }

                string allotmentReferenceCode;
                if (!roomGroups.TryGetValue(r.RoomId, out allotmentReferenceCode))
                {
                    allotmentReferenceCode = r.RoomId;
                }

                var nRoom = doc.CreateElement("Room");
                nRoom.SetAttribute("AllotmentReferenceCode", string.Format("{0}.{1}", idHotel, allotmentReferenceCode));
                nRoom.SetAttribute("Code", CodeWithRate(roomType.Type_Id, roomType.NonRefundable));
                nRoom.SetAttribute("RoomKey", CodeWithRate(r.TypeId, roomType.NonRefundable));
                nRooms.AppendChild(nRoom);

                //Room's description
                if (!string.IsNullOrEmpty(r.Description) || !string.IsNullOrEmpty(roomType.Description))
                {
                    var nRoomDescriptions = doc.CreateElement("Descriptions");
                    var nRoomDescription = doc.CreateElement("Description");
                    if (!String.IsNullOrEmpty(roomType.Description) && !roomType.Description.Equals(r.Description))
                    {
                        var nRtDescription = doc.CreateElement("Description");
                        nRtDescription.SetAttribute("Language", _languageCode);
                        nRtDescription.AppendChild(doc.CreateTextNode(GetXmlSafeOutput(roomType.Description)));
                        nRoomDescriptions.AppendChild(nRtDescription);
                    }
                    nRoomDescription.SetAttribute("Language", _languageCode);
                    nRoomDescription.AppendChild(doc.CreateTextNode(GetXmlSafeOutput(r.Description)));
                    nRoomDescriptions.AppendChild(nRoomDescription);
                    nRoom.AppendChild(nRoomDescriptions);
                }

                #region Room's attributes (EDF asked to remove this information, except few things)

                if (roomType.NonRefundable)
                {
                    var nRoomAttributes = doc.CreateElement("Attributes");
                    nRoom.AppendChild(nRoomAttributes);

                    var nNonRefundable = doc.CreateElement("Attribute");
                    nNonRefundable.SetAttribute("Name", "Contract");
                    nNonRefundable.SetAttribute("Value", "NRF");
                    nRoomAttributes.AppendChild(nNonRefundable);
                }

                #endregion

                var availabilityPeriods = r.AvailabilityPeriods.Values;

                var roomExtraBedAdult = false;
                foreach (var ap in availabilityPeriods)
                {
                    decimal extraBedAdult;
                    if (!string.IsNullOrEmpty(ap.Pricing_ExtrabedAdult)
                        && decimal.TryParse(ap.Pricing_ExtrabedAdult, out extraBedAdult)
                        && extraBedAdult > decimal.Zero)
                    {
                        roomExtraBedAdult = true;
                        break;
                    }
                }

                var extraBedsAvailable = roomType.ExtraBeds > 0 && (roomExtraBedAdult || !currentHotel.AdultOnly)
                        ? roomType.ExtraBeds
                        : 0;

                //Room's occupancy, including extra beds
                var maxChildAgeInclusive = 11;
                var maxChildren = 0;
                var nOccupancies = doc.CreateElement("Occupancies");
                var nOccupancy = doc.CreateElement("Occupancy");
                var nOcInfant = doc.CreateElement("Infants");
                var nOcChild = doc.CreateElement("Children");
                if (r.RoomConfigurationOccupancyId > 0)
                {
                    var occ = _root.RoomConfigOccupancies[r.RoomConfigurationOccupancyId];
                    maxChildren = currentHotel.AdultOnly ? 0 : occ.MaxChildren;
                    nOccupancy.SetAttribute("Max", occ.MaxOccupancy.ToString());
                    nOccupancy.SetAttribute("MaxAdult", (roomExtraBedAdult ? occ.MaxAdults : roomType.Beds).ToString());
                    nOccupancy.SetAttribute("MaxChild", maxChildren.ToString());
                    nOccupancy.SetAttribute("Min", occ.MinOccupancy.ToString());
                    nOccupancy.SetAttribute("MinAdult", occ.MinAdults.ToString());
                    nOccupancy.SetAttribute("MinChargedPersons", roomType.Beds.ToString());
                    nOccupancy.SetAttribute("MinChild", (currentHotel.AdultOnly ? "0" : occ.MinChildren.ToString()));
                }
                else
                {
                    maxChildren = currentHotel.AdultOnly ? 0 : (roomType.Beds + extraBedsAvailable - 1);
                    nOccupancy.SetAttribute("Max", (roomType.Beds + extraBedsAvailable).ToString());
                    nOccupancy.SetAttribute("MaxAdult", (roomType.Beds + (roomExtraBedAdult ? extraBedsAvailable : 0)).ToString());
                    nOccupancy.SetAttribute("MaxChild", maxChildren.ToString());
                    nOccupancy.SetAttribute("Min", "1");
                    nOccupancy.SetAttribute("MinAdult", "1");
                    nOccupancy.SetAttribute("MinChargedPersons", roomType.Beds.ToString());
                    nOccupancy.SetAttribute("MinChild", "0");
                }
                if (r.RoomConfigurationInfantsAndChildrenId > 0)
                {
                    var iac = _root.RoomConfigInfantsAndChildren[r.RoomConfigurationInfantsAndChildrenId];
                    maxChildAgeInclusive = iac.MaxChildAgeInclusive;
                    if (!currentHotel.AdultOnly && !currentHotel.BlockInfant && iac.InfantCountAsChild)
                    {
                        nOcInfant.SetAttribute("ApplyToOccupancy", "Yes");
                    }
                }
                nOcInfant.SetAttribute("MaxCount", (currentHotel.AdultOnly || currentHotel.BlockInfant ? "0" : "1"));
                nOcChild.SetAttribute("MinAge", maxChildAgeInclusive < 2 ? maxChildAgeInclusive.ToString() : "2");
                nOcChild.SetAttribute("MaxAge", (maxChildren == 0 || maxChildAgeInclusive < 2 ? 1 : maxChildAgeInclusive).ToString());
                nOccupancy.AppendChild(nOcInfant);
                nOccupancy.AppendChild(nOcChild);
                nOccupancies.AppendChild(nOccupancy);
                nRoom.AppendChild(nOccupancies);

                if (r.RoomConfigurationOccupancyBlocks != null && r.RoomConfigurationOccupancyBlocks.Any())
                {
                    var nBlockedOccupancies = doc.CreateElement("BlockedOccupancies");
                    foreach (var id in r.RoomConfigurationOccupancyBlocks)
                    {
                        var occBlock = _root.RoomConfigOccupancyBlocks[id];
                        var nBlockedOccupancy = doc.CreateElement("BlockedOccupancy");
                        nBlockedOccupancy.SetAttribute("Adult", occBlock.NumberOfAdults.ToString());
                        nBlockedOccupancy.SetAttribute("Child", occBlock.NumberOfChildren.ToString());
                        nBlockedOccupancies.AppendChild(nBlockedOccupancy);
                    }
                    nOccupancy.AppendChild(nBlockedOccupancies);
                }

                //Boards
                var roomBoards = new List<string>();
                var nBoards = doc.CreateElement("Boards");
                nRoom.AppendChild(nBoards);

                List<AvailabilityPeriod> roomGroupedAdditionalBoards = null;
                var additionalBoardsAvailable = availabilityPeriods.Any(ap => ap.AdditionalBoards.Any());
                if (additionalBoardsAvailable)
                {
                    roomGroupedAdditionalBoards = _optimizer.GroupAdditionalBoards(availabilityPeriods, _startDate, _endDate);
                }

                #region Room's restriccions

                var nRoomRestrictions = doc.CreateElement("Restrictions");
                nRoom.AppendChild(nRoomRestrictions);

                if (roomType.IsWeeklyStay)
                {
                    //Mask seven letter pattern 'YNYYNNY'
                    var weeklyStayMask = "";
                    weeklyStayMask += (roomType.Starting_Days_Monday ? "Y" : "N");
                    weeklyStayMask += (roomType.Starting_Days_Tuesday ? "Y" : "N");
                    weeklyStayMask += (roomType.Starting_Days_Wednesday ? "Y" : "N");
                    weeklyStayMask += (roomType.Starting_Days_Thursday ? "Y" : "N");
                    weeklyStayMask += (roomType.Starting_Days_Friday ? "Y" : "N");
                    weeklyStayMask += (roomType.Starting_Days_Saturday ? "Y" : "N");
                    weeklyStayMask += (roomType.Starting_Days_Sunday ? "Y" : "N");

                    var nRoomRestriction = doc.CreateElement("Restriction");
                    var nWeekdayRestrictions = doc.CreateElement("WeekdayRestrictions");
                    var nWeekdayRestriction = doc.CreateElement("WeekdayRestriction");
                    nWeekdayRestriction.SetAttribute("ApplyTo", "Arrival");
                    nWeekdayRestriction.SetAttribute("Mask", weeklyStayMask);
                    nWeekdayRestrictions.AppendChild(nWeekdayRestriction);
                    nRoomRestriction.AppendChild(nWeekdayRestrictions);
                    nRoomRestrictions.AppendChild(nRoomRestriction);
                }

                var roomGroupedRestrictions = _optimizer.GroupRestrictions(availabilityPeriods, _startDate, _endDate);
                foreach (var ap in roomGroupedRestrictions)
                {
                    //Minimum stay restriction
                    var nRestriction = doc.CreateElement("Restriction");
                    nRoomRestrictions.AppendChild(nRestriction);

                    var nDateRestrictions = doc.CreateElement("DateRestrictions");
                    nDateRestrictions.SetAttribute("Condition", "true");
                    var nDateRestriction = doc.CreateElement("DateRestriction");
                    nDateRestriction.SetAttribute("Start", ap.DateFrom.ToString("yyyy-MM-dd"));
                    nDateRestriction.SetAttribute("End", ap.DateTo.ToString("yyyy-MM-dd"));
                    nDateRestriction.SetAttribute("ApplyTo", "Stay");
                    nDateRestrictions.AppendChild(nDateRestriction);
                    nRestriction.AppendChild(nDateRestrictions);

                    var nStayRestrictions = doc.CreateElement("StayRestrictions");
                    nStayRestrictions.SetAttribute("Condition", "false");
                    var nStayRestriction = doc.CreateElement("StayRestriction");
                    nStayRestriction.SetAttribute("Min", "P" + ap.MinimumStay + "D");
                    nStayRestrictions.AppendChild(nStayRestriction);
                    nRestriction.AppendChild(nStayRestrictions);
                }

                #endregion

                #region Room's extras

                var extraBoards = (r.MealSupplementRestriction && additionalBoardsAvailable);
                var extraDiscounts = availabilityPeriods.Any(ap => ap.Discounts.Any());
                if (extraBoards || extraDiscounts)
                {
                    var nRoomExtras = doc.CreateElement("Extras");
                    nRoom.AppendChild(nRoomExtras);

                    if (extraBoards)
                    {
                        var index = 0;
                        foreach (var ap in roomGroupedAdditionalBoards)
                        {
                            DateTime stDate;
                            DateTime enDate;
                            GetValidDates(ap, out stDate, out enDate);

                            var startDate = stDate.ToString("yyyy-MM-dd");
                            var endDate = enDate.ToString("yyyy-MM-dd");

                            foreach (var ab in ap.AdditionalBoards)
                            {
                                index++;
                                CreateBoardExtra(doc, nRoomExtras, roomType, ab, index, startDate, endDate);
                            }
                        }
                    }

                    if (extraDiscounts)
                    {
                        var index = 0;
                        var roomGroupedDiscounts = _optimizer.GroupDiscounts(availabilityPeriods, _startDate, _endDate);
                        var allDiscounts = roomGroupedDiscounts.SelectMany(ap => ap.Discounts);
                        foreach (var ap in roomGroupedDiscounts)
                        {
                            DateTime stDate;
                            DateTime enDate;
                            GetValidDates(ap, out stDate, out enDate);

                            var startDate = stDate.ToString("yyyy-MM-dd");
                            var endDate = enDate.ToString("yyyy-MM-dd");
                            var endDateEarlyBooking = enDate.AddDays(1).ToString("yyyy-MM-dd");

                            foreach (var discount in ap.Discounts)
                            {
                                var compatibleGroups = _optimizer.CompatibleGroups(allDiscounts, discount);
                                index++;
                                CreateDiscountNode(doc, nRoomExtras, discount, compatibleGroups, index, startDate, (discount.Type == DiscountType.EarlyBooking && !discount.CheckinBased ? endDateEarlyBooking : endDate));
                            }
                        }
                    }

                    if (!nRoomExtras.HasChildNodes)
                    {
                        nRoom.RemoveChild(nRoomExtras);
                    }
                }

                #endregion

                //Room's prices (Price per room)
                var nChargeBlocks = doc.CreateElement("ChargeBlocks");
                //Price per object/room 'O'
                nChargeBlocks.SetAttribute("CalculationMode", "O");
                nRoom.AppendChild(nChargeBlocks);

                #region GlobalTypes

                var globalTypes = doc.CreateElement("GlobalTypes");
                foreach (var mappingCode in mappingCodes)
                {
                    var globalType = doc.CreateElement("GlobalType");
                    globalType.SetAttribute("Code", mappingCode);
                    globalTypes.AppendChild(globalType);
                }
                nRoom.AppendChild(globalTypes);

                #endregion

                var nChargeBlock = doc.CreateElement("ChargeBlock");
                nChargeBlock.SetAttribute("Type", "Basic");
                nChargeBlocks.AppendChild(nChargeBlock);

                var nSections = doc.CreateElement("Sections");
                var nBoardCharges = doc.CreateElement("BoardCharges");
                var nGuestCharges = doc.CreateElement("GuestCharges");

                #region Base price

                var roomGroupedPrices = _optimizer.GroupPrices(availabilityPeriods, _startDate, _endDate);
                foreach (var ap in roomGroupedPrices)
                {
                    DateTime stDate;
                    DateTime enDate;
                    GetValidDates(ap, out stDate, out enDate);

                    var startDate = stDate.ToString("yyyy-MM-dd");
                    var endDate = enDate.ToString("yyyy-MM-dd");

                    if (!roomBoards.Contains(ap.Pricing_BaseIncludedBoard))
                    {
                        roomBoards.Add(ap.Pricing_BaseIncludedBoard);
                    }

                    var nSection = doc.CreateElement("Section");
                    nSection.SetAttribute("Start", startDate);
                    nSection.SetAttribute("End", endDate);
                    var nBaseCharges = doc.CreateElement("BaseCharges");
                    var nBaseCharge = doc.CreateElement("BaseCharge");
                    nBaseCharge.SetAttribute("Amount", ap.Pricing_BasePrice);
                    nBaseCharge.SetAttribute("Duration", "P1D");
                    nBaseCharge.SetAttribute("Type", "ExtraDay");
                    nBaseCharges.AppendChild(nBaseCharge);
                    nSection.AppendChild(nBaseCharges);
                    nSections.AppendChild(nSection);

                    CreateBoardChargeZero(doc, nBoardCharges, ap.Pricing_BaseIncludedBoard, startDate, endDate);
                }

                if (additionalBoardsAvailable)
                {
                    foreach (var ap in roomGroupedAdditionalBoards)
                    {
                        DateTime stDate;
                        DateTime enDate;
                        GetValidDates(ap, out stDate, out enDate);

                        var startDate = stDate.ToString("yyyy-MM-dd");
                        var endDate = enDate.ToString("yyyy-MM-dd");

                        foreach (var ab in ap.AdditionalBoards)
                        {
                            if (!roomBoards.Contains(ab.Type))
                            {
                                roomBoards.Add(ab.Type);
                            }

                            if (r.MealSupplementRestriction)
                            {
                                CreateBoardChargeZero(doc, nBoardCharges, ab.Type, startDate, endDate);
                            }
                            if (!r.MealSupplementRestriction || extraBedsAvailable > 0)
                            {
                                if (ab.Adult != null)
                                {
                                    CreateBoardCharge(doc, nBoardCharges, roomType, ab, "A", r.MealSupplementRestriction, startDate, endDate);
                                }
                                if (ab.Child != null && !currentHotel.AdultOnly)
                                {
                                    CreateBoardCharge(doc, nBoardCharges, roomType, ab, "C", r.MealSupplementRestriction, startDate, endDate);
                                }
                            }
                        }
                    }
                }

                //Extra beds, 'A'dult, 'C'hild
                if (extraBedsAvailable > 0)
                {
                    var roomGroupedExtrabeds = _optimizer.GroupExtrabeds(availabilityPeriods, _startDate, _endDate);
                    foreach (var ap in roomGroupedExtrabeds)
                    {
                        DateTime stDate;
                        DateTime enDate;
                        GetValidDates(ap, out stDate, out enDate);

                        var startDate = stDate.ToString("yyyy-MM-dd");
                        var endDate = enDate.ToString("yyyy-MM-dd");

                        if (roomExtraBedAdult && !String.IsNullOrEmpty(ap.Pricing_ExtrabedAdult))
                        {
                            CreatePriceExtraBed(doc, nGuestCharges, roomType, ap, "A", startDate, endDate);
                        }
                        if (!currentHotel.AdultOnly && !String.IsNullOrEmpty(ap.Pricing_ExtrabedChild))
                        {
                            CreatePriceExtraBed(doc, nGuestCharges, roomType, ap, "C", startDate, endDate);
                        }
                    }
                }

                if (r.RoomConfigurationExtrabedFactors != null && r.RoomConfigurationExtrabedFactors.Any())
                {
                    foreach (var id in r.RoomConfigurationExtrabedFactors)
                    {
                        var roomExtrabedFactor = _root.RoomConfigExtrabedFactors[id];
                        var startDate = (_startDate.HasValue ? _startDate.Value : availabilityPeriods.Min(ap => ap.DateFrom)).ToString("yyyy-MM-dd");
                        var endDate = (_endDate.HasValue ? _endDate.Value : availabilityPeriods.Max(ap => ap.DateTo)).ToString("yyyy-MM-dd");
                        CreateOccupancyFactor(doc, nGuestCharges, roomType, roomExtrabedFactor, maxChildAgeInclusive, startDate, endDate);
                    }
                }

                #endregion

                #region General nodes

                if (nSections.HasChildNodes)
                {
                    nChargeBlock.AppendChild(nSections);
                }
                if (nBoardCharges.HasChildNodes)
                {
                    nChargeBlock.AppendChild(nBoardCharges);
                }
                if (nGuestCharges.HasChildNodes)
                {
                    nChargeBlock.AppendChild(nGuestCharges);
                }

                #endregion

                foreach (var shCode in roomBoards)
                {
                    CreateBoardNode(doc, nBoards, shCode);
                }
            }

            #endregion

            //Validate hotel using the temporal document
            try
            {
                var xmlRaw = doc.OuterXml;
                using (var validatingReader = XmlReader.Create(new StringReader(xmlRaw), _hotelSettings))
                {
                    while (validatingReader.Read()) { /* just loop through document */ }
                }
                writer.WriteRaw(xmlRaw);
            }
            catch (Exception e)
            {
                if (typeof (ApplicationException) == e.GetType() || typeof (XmlSchemaValidationException) == e.GetType())
                {
                    _notValidHotels.TryAdd(currentHotel.Name + "(" + currentHotel.Id + ")", "(CacheXML) - " + e.Message);
                }
                else
                {
                    Console.WriteLine(e.StackTrace);
                    LogException(e);
                }
            }
        }
        #endregion

        #region AvailabilityCacheXML

        private void WriteAvailabilityCacheXml(XmlTextWriter writer)
        {
            var idHotel = WriterHotelId(writer);
            Hotel currentHotel;
            _hotelList.TryGetValue(idHotel, out currentHotel);
            if (currentHotel == null)
            {
                return;
            }
                        
            //EDF document
            var doc = new XmlDocument();
            var nHotelAllotmentRoot = doc.CreateElement("HotelAllotmentRoot");
            nHotelAllotmentRoot.SetAttribute("xmlns", _localConfig.HotelAllotmentRootNamespace);
            nHotelAllotmentRoot.SetAttribute("SchemaVersion", _localConfig.EDFVersionAllotment);
            nHotelAllotmentRoot.SetAttribute("Delta", _isDeltaFile.ToString().ToLower());
            doc.AppendChild(nHotelAllotmentRoot);

            var nBasicData = doc.CreateElement("BasicData");
            nBasicData.SetAttribute("Code", currentHotel.Id);
            nBasicData.SetAttribute("TourOperatorCode", _localConfig.TourOperatorCode);
            nBasicData.SetAttribute("Usage", "HotelOnly");
            nBasicData.SetAttribute("Source", _localConfig.TourOperatorCode);
            nHotelAllotmentRoot.AppendChild(nBasicData);

            var nSellingData = doc.CreateElement("SellingData");
            nSellingData.SetAttribute("SeasonIdentifier", SeasonIdentifier);
            nHotelAllotmentRoot.AppendChild(nSellingData);

            //Relationship between roomID -> roomGroupID
            var roomGroups = new Dictionary<string, string>();

            //All rooms in one list
            var allRooms = HotelRooms(currentHotel, roomGroups);

            var roomCodeUsed = new List<string>();

            //Each room
            foreach (var r in allRooms)
            {
                string allotmentReferenceCode;
                if (!roomGroups.TryGetValue(r.RoomId, out allotmentReferenceCode))
                {
                    allotmentReferenceCode = r.RoomId;
                }

                if (roomCodeUsed.Contains(allotmentReferenceCode))
                {
                    continue;
                }
                roomCodeUsed.Add(allotmentReferenceCode);

                var nAllotments = doc.CreateElement("Allotments");
                nAllotments.SetAttribute("RoomCode", String.Format("{0}.{1}", idHotel, allotmentReferenceCode));
                nSellingData.AppendChild(nAllotments);

                var nAllotment = doc.CreateElement("Allotment");
                nAllotment.SetAttribute("Start", DateTime.Today.ToString("yyyy-MM-dd"));
                nAllotment.SetAttribute("End", DateTime.Today.AddDays(configuration.NrDaysInCache).ToString("yyyy-MM-dd"));
                nAllotment.SetAttribute("Pattern", r.AllotmentPattern.Value);
                nAllotment.SetAttribute("PatternLength", r.AllotmentPattern.Key.ToString());

                nAllotments.AppendChild(nAllotment);
            }
                        

            //Validate hotel using the temporal document
            try
            {
                var xmlRaw = doc.OuterXml;
                using (var validatingReader = XmlReader.Create(new StringReader(xmlRaw), _hotelAllotmentSettings))
                {
                    while (validatingReader.Read()) { /* just loop through document */ }
                }

                //If no exception has been thrown then is a valid hotel
                writer.WriteRaw(xmlRaw);
                
            }
            catch (Exception e)
            {
                //Controlled exception 'ValidationEventHandler'
                if (typeof(ApplicationException) == e.GetType() || typeof(XmlSchemaValidationException) == e.GetType())
                {
                    _notValidHotels.TryAdd(currentHotel.Name + "(" + currentHotel.Id + ")", "(AvailabilityCacheXML) - " + e.Message);
                }
                else
                {
                    //Unknown exception
                    Console.WriteLine(e.StackTrace);
                    LogException(e);
                }
            }
        }
        #endregion
    }
}
