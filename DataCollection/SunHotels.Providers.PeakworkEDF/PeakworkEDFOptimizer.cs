﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunhotels.Export;
using SunHotels.XML.Data;
using SunHotels.XML.Optimize;
using System.Collections.Concurrent;
using System.Text;

namespace SunHotels.Providers
{
    public class PeakworkEDFOptimizer : ProviderOptimizer
    {
        public PeakworkEDFOptimizer(Configuration configuration) : base(configuration)
        {
        }

        public void OptimizeAvailabilityPeriods(Hotel hotel)
        {
            var listRooms = GetListHotelRooms(hotel);
            foreach (var room in listRooms.Values)
            {
                AvailabilityPeriod availabilityPeriod = null;
                var listDates = room.AvailabilityPeriods.Values.SelectMany(ap => Enumerable.Range(0, (ap.DateTo - ap.DateFrom).Days + 1).Select(d => ap.DateFrom.AddDays(d))).ToList();
                foreach (var date in listDates)
                {
                    var compilerDate = date;
                    var ap = room.AvailabilityPeriods.Values.FirstOrDefault(p => DateTime.Compare(p.DateFrom, compilerDate) <= 0 && DateTime.Compare(compilerDate, p.DateTo) <= 0);
                    if (ap == null)
                    {
                        continue;
                    }

                    if (DateTime.Compare(ap.DateTo.AddDays(ap.ReleaseDays * -1), DateTime.Today) < 0)
                    {
                        continue;
                    }

                    var dateWithReleaseApplied = ap.DateFrom.AddDays(ap.ReleaseDays * -1);
                    if (DateTime.Compare(dateWithReleaseApplied, DateTime.Today) < 0)
                    {
                        ap.DateFrom = ap.DateFrom.AddDays((DateTime.Today - dateWithReleaseApplied).Days);
                        if (DateTime.Compare(ap.DateTo, ap.DateFrom) < 0)
                        {
                            continue;
                        }
                    }

                    if (availabilityPeriod == null)
                    {
                        availabilityPeriod = ap;
                    }
                    else if (availabilityPeriod != ap)
                    {
                        //Basic check
                        var vOk = DateTime.Compare(availabilityPeriod.DateTo.AddDays(1), ap.DateFrom) == 0;
                        vOk = vOk && availabilityPeriod.AvailabilityAvailableUnits.Equals(ap.AvailabilityAvailableUnits);
                        vOk = vOk && availabilityPeriod.AvailabilityTotalAllotment.Equals(ap.AvailabilityTotalAllotment);
                        vOk = vOk && availabilityPeriod.IsArrivalPossible.Equals(ap.IsArrivalPossible);
                        vOk = vOk && availabilityPeriod.IsWeeklyStay.Equals(ap.IsWeeklyStay);
                        vOk = vOk && availabilityPeriod.MinimumStay.Equals(ap.MinimumStay);
                        vOk = vOk && availabilityPeriod.ReleaseDays.Equals(ap.ReleaseDays);
                        vOk = vOk && availabilityPeriod.BoardTypeId.Equals(ap.BoardTypeId);
                        vOk = vOk && String.IsNullOrEmpty(availabilityPeriod.Pricing_BaseIncludedBoard).Equals(String.IsNullOrEmpty(ap.Pricing_BaseIncludedBoard));
                        if (vOk && !String.IsNullOrEmpty(availabilityPeriod.Pricing_BaseIncludedBoard) && !String.IsNullOrEmpty(ap.Pricing_BaseIncludedBoard))
                        {
                            vOk = availabilityPeriod.Pricing_BaseIncludedBoard.Equals(ap.Pricing_BaseIncludedBoard);
                        }
                        vOk = vOk && availabilityPeriod.Pricing_BasePrice.Equals(ap.Pricing_BasePrice);
                        vOk = vOk && String.IsNullOrEmpty(availabilityPeriod.Pricing_BookableBoardId).Equals(String.IsNullOrEmpty(ap.Pricing_BookableBoardId));
                        if (vOk && !String.IsNullOrEmpty(availabilityPeriod.Pricing_BookableBoardId) && !String.IsNullOrEmpty(ap.Pricing_BookableBoardId))
                        {
                            vOk = availabilityPeriod.Pricing_BookableBoardId.Equals(ap.Pricing_BookableBoardId);
                        }
                        vOk = vOk && String.IsNullOrEmpty(availabilityPeriod.Pricing_ExtrabedAdult).Equals(String.IsNullOrEmpty(ap.Pricing_ExtrabedAdult));
                        if (vOk && !String.IsNullOrEmpty(availabilityPeriod.Pricing_ExtrabedAdult) && !String.IsNullOrEmpty(ap.Pricing_ExtrabedAdult))
                        {
                            vOk = availabilityPeriod.Pricing_ExtrabedAdult.Equals(ap.Pricing_ExtrabedAdult);
                        }
                        vOk = vOk && String.IsNullOrEmpty(availabilityPeriod.Pricing_ExtrabedChild).Equals(String.IsNullOrEmpty(ap.Pricing_ExtrabedChild));
                        if (vOk && !String.IsNullOrEmpty(availabilityPeriod.Pricing_ExtrabedChild) && !String.IsNullOrEmpty(ap.Pricing_ExtrabedChild))
                        {
                            vOk = availabilityPeriod.Pricing_ExtrabedChild.Equals(ap.Pricing_ExtrabedChild);
                        }

                        //Check lists
                        vOk = vOk && availabilityPeriod.AdditionalBoards.Count.Equals(ap.AdditionalBoards.Count);
                        vOk = vOk && availabilityPeriod.CancellationPolicies.Count.Equals(ap.CancellationPolicies.Count);
                        vOk = vOk && availabilityPeriod.Discounts.Count.Equals(ap.Discounts.Count);
                        if (vOk)
                        {
                            for (var i = 0; i < availabilityPeriod.AdditionalBoards.Count && vOk; i++)
                            {
                                vOk = availabilityPeriod.AdditionalBoards[i].Type.Equals(ap.AdditionalBoards[i].Type);
                                vOk = vOk && availabilityPeriod.AdditionalBoards[i].TypeId.Equals(ap.AdditionalBoards[i].TypeId);
                                vOk = vOk && availabilityPeriod.AdditionalBoards[i].BookableBoardId.Equals(ap.AdditionalBoards[i].BookableBoardId);
                                vOk = vOk && availabilityPeriod.AdditionalBoards[i].Adult.Equals(ap.AdditionalBoards[i].Adult);
                                vOk = vOk && availabilityPeriod.AdditionalBoards[i].Child.Equals(ap.AdditionalBoards[i].Child);
                            }
                            for (var i = 0; i < availabilityPeriod.CancellationPolicies.Count && vOk; i++)
                            {
                                vOk = availabilityPeriod.CancellationPolicies[i].Equals(ap.CancellationPolicies[i]);
                            }
                            var availabilityEnum = availabilityPeriod.Discounts.GetEnumerator();
                            var apEnum = ap.Discounts.GetEnumerator();
                            availabilityEnum.Reset();
                            apEnum.Reset();
                            for (var i = 0; i < availabilityPeriod.Discounts.Count && vOk; i++)
                            {
                                availabilityEnum.MoveNext();
                                apEnum.MoveNext();
                                var availabilityDiscount = availabilityEnum.Current;
                                var apDiscount = apEnum.Current;
                                vOk = availabilityDiscount.AccumulativeGroup.Equals(apDiscount.AccumulativeGroup);
                                vOk = vOk && availabilityDiscount.Type.Equals(apDiscount.Type);
                                vOk = vOk && availabilityDiscount.TypeId.Equals(apDiscount.TypeId);
                                vOk = vOk && availabilityDiscount.Amount.Equals(apDiscount.Amount);
                                vOk = vOk && availabilityDiscount.CheckinBased.Equals(apDiscount.CheckinBased);
                                vOk = vOk && availabilityDiscount.IncludeExtraBed.Equals(apDiscount.IncludeExtraBed);
                                vOk = vOk && availabilityDiscount.IncludeMeal.Equals(apDiscount.IncludeMeal);
                                vOk = vOk && availabilityDiscount.MaxDays.Equals(apDiscount.MaxDays);
                                vOk = vOk && availabilityDiscount.MaxPeriods.Equals(apDiscount.MaxPeriods);
                                if (vOk && availabilityDiscount.Type.Equals(DiscountType.EarlyBooking))
                                {
                                    vOk = availabilityDiscount.LastDate.HasValue.Equals(apDiscount.LastDate.HasValue);
                                    if (vOk && availabilityDiscount.LastDate.HasValue && apDiscount.LastDate.HasValue)
                                    {
                                        vOk = availabilityDiscount.LastDate.Value.Equals(apDiscount.LastDate.Value);
                                    }
                                    else
                                    {
                                        vOk = vOk && availabilityDiscount.MinDays.Equals(apDiscount.MinDays);
                                    }
                                }
                                else
                                {
                                    vOk = vOk && availabilityDiscount.MinDays.Equals(apDiscount.MinDays);
                                }
                                vOk = vOk && availabilityDiscount.Percent.Equals(apDiscount.Percent);
                                vOk = vOk && availabilityDiscount.Period_ID.Equals(apDiscount.Period_ID);
                            }
                        }

                        if (vOk)
                        {
                            availabilityPeriod.DateTo = date;
                            room.AvailabilityPeriods.Remove(date);
                        }
                        else
                        {
                            availabilityPeriod = ap;
                        }
                    }
                }

                //Check values and correct them to validate the XSD
                listDates = new List<DateTime>(room.AvailabilityPeriods.Keys);
                foreach (var date in listDates)
                {
                    var ap = room.AvailabilityPeriods[date];

                    if (ap.AvailabilityAvailableUnits == 0)
                    {
                        room.AvailabilityPeriods.Remove(date);
                    }
                }
            }
        }

        public void CancellationPolicyNRF(Root root, Hotel hotel, ConcurrentDictionary<string, bool> cancellationPoliciesNRF, bool removeNRF)
        {
            var listRooms = GetListHotelRooms(hotel);
            foreach (var room in listRooms.Values)
            {
                if (removeNRF)
                {
                    var periodsToRemove = room.AvailabilityPeriods
                            .Where(el => el.Value.CancellationPolicies.Any(cpId => cancellationPoliciesNRF[cpId]))
                            .Select(el => el.Key)
                            .ToList();
                    foreach (var key in periodsToRemove)
                    {
                        room.AvailabilityPeriods.Remove(key);
                    }
                }
                else if (room.AvailabilityPeriods.Values.SelectMany(ap => ap.CancellationPolicies).Any(cpId => cancellationPoliciesNRF[cpId]))
                {
                    //Mark room as NRF
                    RoomType roomType;
                    if (root.RoomTypes.TryGetValue(room.TypeId, out roomType))
                    {
                        roomType.NonRefundable = true;
                    }
                }
            }
        }

        public void RemoveRoomsWithoutAvailability(Hotel hotel)
        {
            var listRooms = GetListHotelRooms(hotel);

            //Check rooms
            var removeRoomList = new List<string>();
            foreach (var roomKvp in listRooms)
            {
                if (roomKvp.Value.AvailabilityPeriods == null || roomKvp.Value.AvailabilityPeriods.Count == 0)
                {
                    removeRoomList.Add(roomKvp.Key);
                }
            }
            RemoveRoomsFromHotel(hotel, removeRoomList);
        }

        private void RemoveRoomsFromHotel(Hotel hotel, List<string> removeRoomList)
        {
            foreach (var roomKey in removeRoomList)
            {
                if (hotel.Rooms.ContainsKey(roomKey))
                {
                    hotel.Rooms.Remove(roomKey);
                }
                else
                {
                    foreach (var rgKey in hotel.RoomGroups.Keys)
                    {
                        if (hotel.RoomGroups[rgKey].Rooms.ContainsKey(roomKey))
                        {
                            hotel.RoomGroups[rgKey].Rooms.Remove(roomKey);
                            break;
                        }
                    }
                }
            }
            //Check room groups
            var removeRoomGroups = new List<string>();
            foreach (var rgKey in hotel.RoomGroups.Keys)
            {
                if (hotel.RoomGroups[rgKey].Rooms.Count == 1)
                {
                    //Room group with just one room it's not correct, we move the room to the hotel object
                    foreach (var roomKey in hotel.RoomGroups[rgKey].Rooms.Keys)
                    {
                        hotel.Rooms.Add(roomKey, hotel.RoomGroups[rgKey].Rooms[roomKey]);
                    }
                    hotel.RoomGroups[rgKey].Rooms.Clear();
                }
                if (hotel.RoomGroups[rgKey].Rooms.Count == 0)
                {
                    removeRoomGroups.Add(rgKey);
                }
            }
            foreach (var rgKey in removeRoomGroups)
            {
                hotel.RoomGroups.Remove(rgKey);
            }
        }

        private Dictionary<string, Room> GetListHotelRooms(Hotel hotel)
        {
            var listRooms = new Dictionary<string, Room>();
            if (hotel.RoomGroups != null && hotel.RoomGroups.Count > 0)
            {
                foreach (var roomGroup in hotel.RoomGroups.Values)
                {
                    if (roomGroup.Rooms != null && roomGroup.Rooms.Count > 0)
                    {
                        foreach (var room in roomGroup.Rooms)
                        {
                            listRooms.Add(room.Key, room.Value);
                        }
                    }
                }
            }
            if (hotel.Rooms != null && hotel.Rooms.Count > 0)
            {
                foreach (var room in hotel.Rooms)
                {
                    listRooms.Add(room.Key, room.Value);
                }
            }
            return listRooms;
        }

        public void DiscountMinDaysToLastDate(Hotel hotel)
        {
            var listRooms = GetListHotelRooms(hotel);
            foreach (var room in listRooms.Values)
            {
                var listDiscounts = room.AvailabilityPeriods.Values
                                        .SelectMany(ap => ap.Discounts)
                                        .Where(di => di.Type == DiscountType.EarlyBooking)
                                        .OrderBy(di => di.MinDays)
                                        .GroupBy(di => di.Period_ID.GetValueOrDefault(0))
                                        .ToDictionary(el => el.Key, el => el.ToList());

                var minDaysComparison = new Dictionary<int, bool>();
                foreach (var date in room.AvailabilityPeriods.Keys)
                {
                    var discounts = room.AvailabilityPeriods[date].Discounts.Where(d => d.Type == DiscountType.EarlyBooking);
                    foreach (var disc in discounts)
                    {
                        var discKey = disc.Period_ID.GetValueOrDefault(0);
                        List<Discount> periodDiscounts;
                        if (discKey > 0 && listDiscounts.TryGetValue(discKey, out periodDiscounts))
                        {
                            bool sameMinDays;
                            if (!minDaysComparison.TryGetValue(discKey, out sameMinDays))
                            {
                                sameMinDays = periodDiscounts.All(d => d.MinDays == disc.MinDays);
                                minDaysComparison.Add(discKey, sameMinDays);
                            }
                            if (!sameMinDays)
                            {
                                disc.LastDate = date.AddDays(-disc.MinDays);
                            }
                        }
                    }
                }
            }
        }

        public List<AvailabilityPeriod> GroupRestrictions(IEnumerable<AvailabilityPeriod> availabilityPeriods, DateTime? startDate, DateTime? endDate)
        {
            var roomGroupedRestrictions = new List<AvailabilityPeriod>();
            AvailabilityPeriod lastPeriodGrouped = null;
            foreach (var ap in availabilityPeriods)
            {
                //Validating room dates against the exporting dates
                if ((startDate.HasValue && startDate.Value > ap.DateTo) || (endDate.HasValue && endDate.Value < ap.DateFrom))
                {
                    continue;
                }

                var groupPeriod = lastPeriodGrouped != null
                                    && lastPeriodGrouped.MinimumStay == ap.MinimumStay
                                    && lastPeriodGrouped.DateTo.AddDays(1).Equals(ap.DateFrom);

                if (!groupPeriod)
                {
                    lastPeriodGrouped = new AvailabilityPeriod();
                    lastPeriodGrouped.DateFrom = ap.DateFrom;
                    lastPeriodGrouped.DateTo = ap.DateTo;
                    lastPeriodGrouped.MinimumStay = ap.MinimumStay;
                    roomGroupedRestrictions.Add(lastPeriodGrouped);
                }
                else
                {
                    lastPeriodGrouped.DateTo = ap.DateTo;
                }
            }
            return roomGroupedRestrictions;
        }

        public List<AvailabilityPeriod> GroupAdditionalBoards(IEnumerable<AvailabilityPeriod> availabilityPeriods, DateTime? startDate, DateTime? endDate)
        {
            var roomGroupedBoards = new List<AvailabilityPeriod>();
            AvailabilityPeriod lastPeriodGrouped = null;
            foreach (var ap in availabilityPeriods)
            {
                //Validating room dates against the exporting dates
                if ((startDate.HasValue && startDate.Value > ap.DateTo) || (endDate.HasValue && endDate.Value < ap.DateFrom))
                {
                    continue;
                }

                var groupPeriod = lastPeriodGrouped != null
                                    && lastPeriodGrouped.AdditionalBoards != null && ap.AdditionalBoards != null
                                    && lastPeriodGrouped.AdditionalBoards.SequenceEqual(ap.AdditionalBoards)
                                    && lastPeriodGrouped.DateTo.AddDays(1).Equals(ap.DateFrom);

                if (!groupPeriod)
                {
                    lastPeriodGrouped = new AvailabilityPeriod();
                    lastPeriodGrouped.DateFrom = ap.DateFrom;
                    lastPeriodGrouped.DateTo = ap.DateTo;
                    lastPeriodGrouped.AdditionalBoards = ap.AdditionalBoards;
                    roomGroupedBoards.Add(lastPeriodGrouped);
                }
                else
                {
                    lastPeriodGrouped.DateTo = ap.DateTo;
                }
            }
            return roomGroupedBoards;
        }

        public List<AvailabilityPeriod> GroupDiscounts(IEnumerable<AvailabilityPeriod> availabilityPeriods, DateTime? startDate, DateTime? endDate)
        {
            var roomGroupedDiscounts = new List<AvailabilityPeriod>();

            var distinctDiscounts = availabilityPeriods.SelectMany(ap => ap.Discounts).Distinct();
            var listDiscounts = new List<Discount>();
            foreach (var d in distinctDiscounts)
            {
                if (!listDiscounts.Any(disc => (disc == d || disc.Equals(d))))
                {
                    listDiscounts.Add(d);
                }
            }

            listDiscounts = listDiscounts
                .OrderBy(d => d.AccumulativeGroup)
                .ThenBy(d =>
                {
                    switch (d.Type)
                    {
                        case DiscountType.EarlyBooking: return 0;
                        case DiscountType.PayStay: return 1;
                        case DiscountType.Value: return 2;
                        case DiscountType.MinMax: return 3;
                    }
                    return 4;
                })
                .ThenBy(d => d.CheckinBased ? 0 : 1)
                .ThenBy(d => d.LastDate.HasValue ? d.LastDate.Value.Ticks : Int64.MaxValue)
                .ThenByDescending(d => d.MinDays)
                .ThenByDescending(d => d.MaxDays.HasValue ? d.MaxDays.Value : 0)
                .ThenByDescending(d => d.Percent)
                .ThenByDescending(d => d.Amount)
                .ThenByDescending(d => (d.IncludeMeal ? 1 : 0) + (d.IncludeExtraBed ? 1 : 0))
                .ToList();

            var discountPerDay = new Dictionary<DateTime, Dictionary<int, List<int>>>();

            var periodsPerDiscount = listDiscounts
                    .Select(d => availabilityPeriods
                                    .Where(ap => ap.Discounts.Any(disc => (disc == d || disc.Equals(d))))
                                    .Select(ap =>
                                    {
                                        List<int> types;
                                        Dictionary<int, List<int>> accumulativeTypePerDay;
                                        if (!discountPerDay.TryGetValue(ap.DateFrom, out accumulativeTypePerDay))
                                        {
                                            accumulativeTypePerDay = new Dictionary<int, List<int>>();
                                            discountPerDay.Add(ap.DateFrom, accumulativeTypePerDay);
                                        }
                                        if (!accumulativeTypePerDay.TryGetValue(d.AccumulativeGroup, out types))
                                        {
                                            types = new List<int>();
                                            accumulativeTypePerDay.Add(d.AccumulativeGroup, types);
                                        }
                                        if (!types.Contains(d.TypeId))
                                        {
                                            types.Add(d.TypeId);
                                        }

                                        var canAddPeriod = true;
                                        if (accumulativeTypePerDay.Count > 1)
                                        {
                                            var typesInPreviousGroups = accumulativeTypePerDay
                                                    .Where(item => item.Key < d.AccumulativeGroup)
                                                    .SelectMany(item => item.Value);
                                            if (typesInPreviousGroups.Contains(d.TypeId))
                                            {
                                                canAddPeriod = false;
                                            }
                                        }

                                        AvailabilityPeriod discountPeriod = null;
                                        if (canAddPeriod)
                                        {
                                            discountPeriod = new AvailabilityPeriod
                                            {
                                                DateFrom = ap.DateFrom,
                                                DateTo = ap.DateTo
                                            };
                                            discountPeriod.Discounts.TryAdd(d);
                                        }
                                        return discountPeriod;
                                    })
                                    .Where(ap => ap != null)
                    );

            foreach (var listPeriods in periodsPerDiscount)
            {
                AvailabilityPeriod lastPeriodGrouped = null;
                foreach (var ap in listPeriods)
                {
                    //Validating room dates against the exporting dates
                    if ((startDate.HasValue && startDate.Value > ap.DateTo) || (endDate.HasValue && endDate.Value < ap.DateFrom))
                    {
                        continue;
                    }

                    var groupPeriod = lastPeriodGrouped != null
                                        && lastPeriodGrouped.Discounts != null && ap.Discounts != null
                                        && lastPeriodGrouped.Discounts.SequenceEqual(ap.Discounts)
                                        && lastPeriodGrouped.DateTo.AddDays(1).Equals(ap.DateFrom);

                    if (!groupPeriod)
                    {
                        lastPeriodGrouped = new AvailabilityPeriod();
                        lastPeriodGrouped.DateFrom = ap.DateFrom;
                        lastPeriodGrouped.DateTo = ap.DateTo;
                        lastPeriodGrouped.Discounts = ap.Discounts;
                        roomGroupedDiscounts.Add(lastPeriodGrouped);
                    }
                    else
                    {
                        lastPeriodGrouped.DateTo = ap.DateTo;
                    }
                }
            }

            //Filter periods by discount.MinDays (except EB) to avoid generating xml not bookable with discount
            return roomGroupedDiscounts
                    .Where(ap => ap.Discounts.Any(d => d.Type == DiscountType.EarlyBooking || (ap.DateTo - ap.DateFrom).TotalDays + 1 >= d.MinDays))
                    .ToList();
        }

        public Dictionary<int, IEnumerable<int>> CompatibleGroups(IEnumerable<Discount> allDiscounts, Discount currentDiscount)
        {
            return allDiscounts
                        .Where(d => d.AccumulativeGroup != currentDiscount.AccumulativeGroup)
                        .GroupBy(d => d.AccumulativeGroup)
                        .ToDictionary(d => d.Key, d => d.Select(disc => disc.TypeId));
        }

        public List<AvailabilityPeriod> GroupExtrabeds(IEnumerable<AvailabilityPeriod> availabilityPeriods, DateTime? startDate, DateTime? endDate)
        {
            var roomGroupedExtrabeds = new List<AvailabilityPeriod>();
            AvailabilityPeriod lastPeriodGrouped = null;
            foreach (var ap in availabilityPeriods)
            {
                //Validating room dates against the exporting dates
                if ((startDate.HasValue && startDate.Value > ap.DateTo) || (endDate.HasValue && endDate.Value < ap.DateFrom))
                {
                    continue;
                }

                var groupPeriod = lastPeriodGrouped != null
                                    && lastPeriodGrouped.Pricing_ExtrabedAdult == ap.Pricing_ExtrabedAdult
                                    && lastPeriodGrouped.Pricing_ExtrabedChild == ap.Pricing_ExtrabedChild
                                    && lastPeriodGrouped.DateTo.AddDays(1).Equals(ap.DateFrom);

                if (!groupPeriod)
                {
                    lastPeriodGrouped = new AvailabilityPeriod();
                    lastPeriodGrouped.DateFrom = ap.DateFrom;
                    lastPeriodGrouped.DateTo = ap.DateTo;
                    lastPeriodGrouped.Pricing_ExtrabedAdult = ap.Pricing_ExtrabedAdult;
                    lastPeriodGrouped.Pricing_ExtrabedChild = ap.Pricing_ExtrabedChild;
                    roomGroupedExtrabeds.Add(lastPeriodGrouped);
                }
                else
                {
                    lastPeriodGrouped.DateTo = ap.DateTo;
                }
            }
            return roomGroupedExtrabeds;
        }

        public List<AvailabilityPeriod> GroupPrices(IEnumerable<AvailabilityPeriod> availabilityPeriods, DateTime? startDate, DateTime? endDate)
        {
            var roomGroupedPrices = new List<AvailabilityPeriod>();
            AvailabilityPeriod lastPeriodGrouped = null;
            foreach (var ap in availabilityPeriods)
            {
                //Validating room dates against the exporting dates
                if ((startDate.HasValue && startDate.Value > ap.DateTo) || (endDate.HasValue && endDate.Value < ap.DateFrom))
                {
                    continue;
                }

                var groupPeriod = lastPeriodGrouped != null
                                    && lastPeriodGrouped.Pricing_BaseIncludedBoard == ap.Pricing_BaseIncludedBoard
                                    && lastPeriodGrouped.Pricing_BasePrice == ap.Pricing_BasePrice
                                    && lastPeriodGrouped.DateTo.AddDays(1).Equals(ap.DateFrom);

                if (!groupPeriod)
                {
                    lastPeriodGrouped = new AvailabilityPeriod();
                    lastPeriodGrouped.DateFrom = ap.DateFrom;
                    lastPeriodGrouped.DateTo = ap.DateTo;
                    lastPeriodGrouped.Pricing_BaseIncludedBoard = ap.Pricing_BaseIncludedBoard;
                    lastPeriodGrouped.Pricing_BasePrice = ap.Pricing_BasePrice;
                    roomGroupedPrices.Add(lastPeriodGrouped);
                }
                else
                {
                    lastPeriodGrouped.DateTo = ap.DateTo;
                }
            }
            return roomGroupedPrices;
        }

        public List<AvailabilityPeriod> GroupAvailability(IEnumerable<AvailabilityPeriod> availabilityPeriods, DateTime? startDate, DateTime? endDate)
        {
            var roomGroupedAvailability = new List<AvailabilityPeriod>();
            AvailabilityPeriod lastPeriodGrouped = null;
            foreach (var ap in availabilityPeriods)
            {
                //Validating room dates against the exporting dates
                if ((startDate.HasValue && startDate.Value > ap.DateTo) || (endDate.HasValue && endDate.Value < ap.DateFrom))
                {
                    continue;
                }

                var groupPeriod = lastPeriodGrouped != null
                                    && lastPeriodGrouped.AvailabilityAvailableUnits == ap.AvailabilityAvailableUnits
                                    && lastPeriodGrouped.DateTo.AddDays(1).Equals(ap.DateFrom);

                if (!groupPeriod)
                {
                    lastPeriodGrouped = new AvailabilityPeriod();
                    lastPeriodGrouped.DateFrom = ap.DateFrom;
                    lastPeriodGrouped.DateTo = ap.DateTo;
                    lastPeriodGrouped.AvailabilityAvailableUnits = ap.AvailabilityAvailableUnits;
                    roomGroupedAvailability.Add(lastPeriodGrouped);
                }
                else
                {
                    lastPeriodGrouped.DateTo = ap.DateTo;
                }
            }
            return roomGroupedAvailability;
        }

        public void FilterDeltaRooms(Hotel hotel, Dictionary<string, string> roomsPattern, ConcurrentDictionary<string, string> availabilityPattern)
        {
            List<string> roomsToRemove = new List<string>();
            foreach (var roomPattern in roomsPattern)
            {
                var hotelRoomKey = string.Join("@", hotel.Id, roomPattern.Key);
                string storedPattern;
                if (availabilityPattern != null && availabilityPattern.TryGetValue(hotelRoomKey, out storedPattern) && storedPattern != roomPattern.Value)
                {
                    // Update if the value has changed
                    availabilityPattern[hotelRoomKey] = roomPattern.Value;
                }
                else
                {
                    roomsToRemove.Add(roomPattern.Key);
                }
            }
            RemoveRoomsFromHotel(hotel, roomsToRemove);
        }


        public Dictionary<string, string> GetRoomsPattern(Hotel hotel, DateTime currentDate, DateTime endDate)
        {
            var roomsPattern = new Dictionary<string, string>();
            var rooms = GetListHotelRooms(hotel);
            DateTime tmpDate;
            foreach(var room in rooms)
            {
                int patternLength = room.Value.AvailabilityPeriods.Values.Max(ap => ap.AvailabilityAvailableUnits).ToString().Length;
                var sb = new StringBuilder();
                int daysWithNoAvailability;
                tmpDate = currentDate;
                foreach (var ap in room.Value.AvailabilityPeriods.Values)
                {
                    daysWithNoAvailability = (int)Math.Round((ap.DateFrom - tmpDate).TotalDays, MidpointRounding.AwayFromZero);
                    if (daysWithNoAvailability > 0)
                    {
                        sb.Append('0', daysWithNoAvailability * patternLength);
                        tmpDate = ap.DateFrom;
                    }
                    while (tmpDate <= ap.DateTo)
                    {
                        sb.Append(ap.AvailabilityAvailableUnits.ToString().PadLeft(patternLength, '0'));
                        tmpDate = tmpDate.AddDays(1);
                    }
                }

                daysWithNoAvailability = (int)Math.Round((endDate - tmpDate).TotalDays, MidpointRounding.AwayFromZero);
                if (daysWithNoAvailability > 0)
                {
                    sb.Append('0', daysWithNoAvailability * patternLength);
                }
                room.Value.AllotmentPattern = new KeyValuePair<int, string>(patternLength, sb.ToString());
                roomsPattern.Add(room.Key, sb.ToString());
            };

            return roomsPattern;
        }
    }
}
