﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace SunHotels.Providers
{
    public class PeakworkEDFSettings
    {
        public bool HideManualRooms { get; set; }
        public bool HideXMLRooms { get; set; }
        public List<int> BlockedHotels { get; set; }
        public bool ShowXmlOnlyIfMissingManual { get; set; }
        public string EDFVersionHotel { get; set; }
        public string EDFVersionAllotment { get; set; }
        public string HotelRootNamespace { get; set; }
        public string HotelAllotmentRootNamespace { get; set; }
        public string TourOperatorCode { get; set; }
        public string ZipName { get; set; }
        public string HotelXSD { get; set; }
        public string HotelCommonsXSD { get; set; }
        public string HotelAllotmentXSD { get; set; }

        [XmlIgnore]
        public Dictionary<string, string[]> Mapping { get; set; }
        public bool RemoveNRF { get; set; }
        public string AgentLanguageCode { get; set; }
        public bool ExcludeSharedRooms { get; set; }
        public bool ExcludeSharedFacilities { get; set; }

        [XmlIgnore]
        public string LogFileName { get; set; }
        public string MarketLabel { get; set; }
        public string AccountLabel { get; set; }
        public bool PreventZip { get; set; }
        public bool DebugMode { get; set; } 

        public bool PreventAvailabilityPatternFile { get; set; }
        public bool ZipFolderUnix { get; set; } = false;
    }
}
