﻿namespace SunHotels.Providers
{
    public partial class PeakworkEDFDataSet
    {


        partial class cancellation_policyDataTable
        {
        }
    }
}

namespace SunHotels.Providers.PeakworkEDFDataSetTableAdapters
{
    public partial class availabilityTableAdapter
    {
        public int CommandTimeout
        {
            set
            {
                this.InitCommandCollection();
                foreach (System.Data.IDbCommand command in this.CommandCollection)
                {
                    command.CommandTimeout = value;
                }
            }
            get
            {
                return this.CommandCollection[0].CommandTimeout;
            }
        }
    }

    public partial class agentTableAdapter
    {
        public int CommandTimeout
        {
            set
            {
                this.InitCommandCollection();
                foreach (System.Data.IDbCommand command in this.CommandCollection)
                {
                    command.CommandTimeout = value;
                }
            }
            get
            {
                return this.CommandCollection[0].CommandTimeout;
            }
        }
    }
    
    public partial class roomGroupTableAdapter
    {
        public int CommandTimeout
        {
            set
            {
                this.InitCommandCollection();
                foreach (System.Data.IDbCommand command in this.CommandCollection)
                {
                    command.CommandTimeout = value;
                }
            }
            get
            {
                return this.CommandCollection[0].CommandTimeout;
            }
        }
    }

    public partial class hotelTableAdapter
    {
        public int CommandTimeout
        {
            set
            {
                this.InitCommandCollection();
                foreach (System.Data.IDbCommand command in this.CommandCollection)
                {
                    command.CommandTimeout = value;
                }
            }
            get
            {
                return this.CommandCollection[0].CommandTimeout;
            }
        }
    }

    public partial class discountTableAdapter
    {
        public int CommandTimeout
        {
            set
            {
                this.InitCommandCollection();
                foreach (System.Data.IDbCommand command in this.CommandCollection)
                {
                    command.CommandTimeout = value;
                }
            }
            get
            {
                return this.CommandCollection[0].CommandTimeout;
            }
        }
    }

    public partial class spWSDL_GetEffectiveCancellationPoliciesTableAdapter
    {
        public int CommandTimeout
        {
            set
            {
                this.InitCommandCollection();
                foreach (System.Data.IDbCommand command in this.CommandCollection)
                {
                    command.CommandTimeout = value;
                }
            }
            get
            {
                return this.CommandCollection[0].CommandTimeout;
            }
        }
    }

    public partial class hotelStandardTableAdapter
    {
        public string GetCommandText(int num)
        {
            return this.CommandCollection[num].CommandText;
        }
        public void SetCommandText(int num, string SQL)
        {
            this.CommandCollection[num].CommandText = SQL;
        }

        public void SetCommandTimeout(int num, int timeout)
        {
            this.CommandCollection[num].CommandTimeout = timeout;
        }
    }

	public partial class roomsStaticTableAdapter
    {
        public string GetCommandText(int num)
        {
            return this.CommandCollection[num].CommandText;
        }
        public void SetCommandText(int num, string SQL)
        {
            this.CommandCollection[num].CommandText = SQL;
        }

        public void SetCommandTimeout(int num, int timeout)
        {
            this.CommandCollection[num].CommandTimeout = timeout;
        }
    }

    public partial class HotelMealLabelsTableAdapter
    {
        public string GetCommandText(int num)
        {
            return this.CommandCollection[num].CommandText;
        }
        public void SetCommandText(int num, string SQL)
        {
            this.CommandCollection[num].CommandText = SQL;
        }

        public void SetCommandTimeout(int num, int timeout)
        {
            this.CommandCollection[num].CommandTimeout = timeout;
        }
    }

    public partial class cancellation_policyTableAdapter
    {
        public string GetCommandText(int num)
        {
            return this.CommandCollection[num].CommandText;
        }
        public void SetCommandText(int num, string SQL)
        {
            this.CommandCollection[num].CommandText = SQL;
        }

        public void SetCommandTimeout(int num, int timeout)
        {
            this.CommandCollection[num].CommandTimeout = timeout;
        }
    }

    public partial class hotel_cancellation_policyTableAdapter
    {
        public string GetCommandText(int num)
        {
            return this.CommandCollection[num].CommandText;
        }
        public void SetCommandText(int num, string SQL)
        {
            this.CommandCollection[num].CommandText = SQL;
        }

        public void SetCommandTimeout(int num, int timeout)
        {
            this.CommandCollection[num].CommandTimeout = timeout;
        }
    }

    public partial class roomGroupTableAdapter
    {
        public string GetCommandText(int num)
        {
            return this.CommandCollection[num].CommandText;
        }
        public void SetCommandText(int num, string SQL)
        {
            this.CommandCollection[num].CommandText = SQL;
        }

        public void SetCommandTimeout(int num, int timeout)
        {
            this.CommandCollection[num].CommandTimeout = timeout;
        }
    }

    public partial class languageTableAdapter
    {
        public string GetCommandText(int num)
        {
            return this.CommandCollection[num].CommandText;
        }
        public void SetCommandText(int num, string SQL)
        {
            this.CommandCollection[num].CommandText = SQL;
        }

        public void SetCommandTimeout(int num, int timeout)
        {
            this.CommandCollection[num].CommandTimeout = timeout;
        }
    }

    public partial class roomsWeeklyTableAdapter
    {
        public string GetCommandText(int num)
        {
            return this.CommandCollection[num].CommandText;
        }
        public void SetCommandText(int num, string SQL)
        {
            this.CommandCollection[num].CommandText = SQL;
        }

        public void SetCommandTimeout(int num, int timeout)
        {
            this.CommandCollection[num].CommandTimeout = timeout;
        }
    }

    public partial class DestinationsTableAdapter
    {
        public string GetCommandText(int num)
        {
            return this.CommandCollection[num].CommandText;
        }
        public void SetCommandText(int num, string SQL)
        {
            this.CommandCollection[num].CommandText = SQL;
        }

        public void SetCommandTimeout(int num, int timeout)
        {
            this.CommandCollection[num].CommandTimeout = timeout;
        }
    }

    public partial class ResortsTableAdapter
    {
        public string GetCommandText(int num)
        {
            return this.CommandCollection[num].CommandText;
        }
        public void SetCommandText(int num, string SQL)
        {
            this.CommandCollection[num].CommandText = SQL;
        }

        public void SetCommandTimeout(int num, int timeout)
        {
            this.CommandCollection[num].CommandTimeout = timeout;
        }
    }
}