﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SunHotels.XML.Data;

namespace SunHotels.Providers
{
    /// <summary>
    /// Contains a method for comparing two cancellation policies.
    /// </summary>
    public class CancellationPolicyComparer : IComparer<CancellationPolicy>
    {
        private bool sortByTypeId;

        public CancellationPolicyComparer(bool sortByTypeId)
        {
            this.sortByTypeId = sortByTypeId;
        }

        #region IComparer<CancellationPolicy> Members

        private int TypeWeight(string penaltyBasis)
        {
            switch (penaltyBasis)
            {
                case "full_stay": return 3;
                case "nights": return 2;
                case "fixed_amount": return 1;
            }
            return 0;
        }

        /// <summary>
        /// Compares two objects of the type "CancellationPolicy".
        /// </summary>
        /// <param name="x">The first cancellation policy.</param>
        /// <param name="y">The second cancellation policy.</param>
        /// <returns>A negative number if x comes before y in a relative order.</returns>
        public int Compare(CancellationPolicy x, CancellationPolicy y)
        {
            var weightX = TypeWeight(x.Penalty_Basis);
            var weightY = TypeWeight(y.Penalty_Basis);

            // Sort by type ID ascending
            if (weightX > weightY && sortByTypeId)
            {
                return 1;
            }
            else if (weightX < weightY && sortByTypeId)
            {
                return -1;
            }
            else
            {
                // Sort by deadline descending (a deadline of null comes first, NRF)
                if (x.Deadline_Value < y.Deadline_Value || (!y.Deadline_Value.HasValue && x.Deadline_Value.HasValue))
                {
                    return 1;
                }
                else if (x.Deadline_Value > y.Deadline_Value || (!x.Deadline_Value.HasValue && y.Deadline_Value.HasValue))
                {
                    return -1;
                }
                else if (!sortByTypeId)
                {
                    if (weightX > weightY)
                    {
                        return -1;
                    }
                    else if (weightX < weightY)
                    {
                        return 1;
                    }
                    else if (x.Penalty_Value < y.Penalty_Value)
                    {
                        return 1;
                    }
                    else if (x.Penalty_Value > y.Penalty_Value)
                    {
                        return -1;
                    }
                    else
                    {
                        return 0;
                    }
                }
                else
                {
                    return 0;
                }
            }
        }

        #endregion
    }
}
