﻿ 
 	cls

	$configRoot =  (get-item "C:\VC\spider v103\PeakworkExportThomasCookMarket\").Directory.FullName;

	$processPath = "$($configRoot)";
	$marketsXml = "$($configRoot)MarketAccountMapping.xml"; 
	$cachePath = "$($configRoot)Cache\PeakworkEDF\"
	$Logfile = "$($cachePath)$(gc env:computername).log"

	$zipFolder = "$($cachePath)SunHotels"  
	$zipFile = "$($zipFolder).zip"

	function LogWrite
	{
	   Param ([string]$logstring)
	   Add-content $Logfile -value $logstring
	}

	function ZipFiles( $zipfilename, $sourcedir )
	{ 
		Add-Type -Assembly System.IO.Compression.FileSystem
		$compressionLevel = [System.IO.Compression.CompressionLevel]::Faster

		LogWrite "Compression start"
		[System.IO.Compression.ZipFile]::CreateFromDirectory($sourcedir,$zipfilename,$compressionLevel,$false)
		LogWrite "Compression finshed"

	} 

	cd $processPath 
 
	If(Test-Path "$($cachePath)"){
		Remove-Item -Path "$($cachePath)" -Recurse
	}
	
	New-Item -Force -ItemType directory -Path "$($cachePath)"	 

	$taskProcess = new-object system.collections.arraylist
	[xml]$xml = Get-Content  "$($marketsXml)"
	foreach($market in $xml.Markets.Market){
		foreach($account in $market.Accounts.Account | Where-Object  {$_.IsPrimaryAccount -eq "true"}){		

			$configurationXml = "$($configRoot)PeakworkEDF_$($market.Label).xml" 
			if($account.ConfigXml){
				$configurationXml = "$($configRoot)$($account.ConfigXml)" 
			}
			LogWrite "Start File $configurationXml process"			 
			Start-Process  "$($processPath)SunHotels.Loader2.exe" -ArgumentList $configurationXml -PassThru -Wait -ErrorAction Stop
			LogWrite "File $($configurationXml) processed" 
		}
	}
	 
	LogWrite "Moving generated folders to $($zipFolder)"
 
	New-Item -Force -Path $cachePath -Name "SunHotels" -ItemType Directory 
	 
	Get-ChildItem  -Path "$($cachePath)" -exclude "SunHotels" | Where-Object {$_.Attributes -match 'Directory'} | Move-Item -Force -destination $zipFolder
	
	Get-ChildItem $cachePath -include *.csv -recurse | foreach ($_) {Remove-Item $_.fullname }

	Copy-Item $marketsXml $zipFolder

	If(Test-Path $zipFolder){

		If (Test-Path $zipFile){
			Remove-Item "$($zipFile)" -recurse  
		}	
		
		ZipFiles "$($zipFile)" "$($zipFolder)" 		
		Remove-Item "$($cachePath)*" -exclude SunHotels.zip , *.log -Force -Recurse
		New-Item -Path $cachePath -Name "SUNHOTELS.done" -ItemType File
	} 

	cd $PSScriptRoot

	exit
