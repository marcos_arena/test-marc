﻿using SunHotels.Providers.PeakworkEDFDataSetTableAdapters;
using SunHotels.XML.Data;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace SunHotels.Providers
{
    /// <summary>
    /// This class handles CancellationPolicyRules in two sp calls and 
    /// puts the results in roomBasedCancellationRules and hotelBasedCancellationRules
    /// </summary>
    class CancellationRulesManager
    {
        #region Private fields
        private ConcurrentDictionary<string, Dictionary<DateTime, List<CancellationPolicy>>> roomAndDateBasedCancellationRules;
        private ConcurrentDictionary<string, List<CancellationPolicy>> roomBasedCancellationRules;
        private ConcurrentDictionary<string, List<CancellationPolicyPeriod>> hotelBasedCancellationRules;
        private Dictionary<string, CancellationPolicy> allCancellationPolicies;
        private ConcurrentDictionary<string, bool> AllCancellationPoliciesNRF;
        private int MaxDegreeOfParallelism = 1;

        #endregion

        /// <summary>
        /// Default Constructor
        /// </summary>
        public CancellationRulesManager(int maxDegreeOfParallelism)
        {
            MaxDegreeOfParallelism = maxDegreeOfParallelism > 0 ? maxDegreeOfParallelism : 1;
        }

        #region Methods

        /// <summary>
        /// Load the data in to the dictionaries
        /// </summary>
        /// <param name="allCancellationPolicies"></param>
        public void LoadCancelationRules(Dictionary<string, CancellationPolicy> allCancellationPolicies)
        {
            this.allCancellationPolicies = allCancellationPolicies;
            roomAndDateBasedCancellationRules = new ConcurrentDictionary<string, Dictionary<DateTime, List<CancellationPolicy>>>();
            roomBasedCancellationRules = new ConcurrentDictionary<string, List<CancellationPolicy>>();
            hotelBasedCancellationRules = new ConcurrentDictionary<string, List<CancellationPolicyPeriod>>();
        }

        /// <summary>
        /// Load the data in to the dictionaries
        /// </summary>
        /// <param name="hotelList"></param>
        /// <param name="roomList"></param>
        /// <param name="GENERAL_TIMEOUT"></param>
        public void LoadCancelationRulesHotelRoom(Dictionary<string, List<string>> hotelRooms, int GeneralTimeout)
        {
            // Get Rooms data
            Console.WriteLine("*** Get Room data ***");

            var allRooms = hotelRooms.Values.SelectMany(list => list);
            Parallel.ForEach(PeakworkEDF.MultipleQueryLists(allRooms), new ParallelOptions { MaxDegreeOfParallelism = MaxDegreeOfParallelism }, listRooms =>
            {
                Thread.CurrentThread.CurrentCulture = Thread.CurrentThread.CurrentUICulture = CultureInfo.InvariantCulture;

                cancellation_policyTableAdapter ta = new cancellation_policyTableAdapter();
                ta.SetCommandTimeout(0, GeneralTimeout);
                var command = string.Format(ta.GetCommandText(0), "WHERE", "cp.room_id IN (" + listRooms + ")", "AND", "ID IN (" + listRooms + ")");
                ta.SetCommandText(0, command);

                PeakworkEDFDataSet.cancellation_policyDataTable roomData = ta.GetData();
                foreach (var cprRow in roomData)
                {

                    string roomId = cprRow.room_id.ToString();
                    DateTime? date = cprRow.IsdatumNull() ? (DateTime?)null : cprRow.datum;

                    // get the active policy
                    CancellationPolicy cprActive = CreateCancellationPolicyPeriod(cprRow.cancellation_type_id, cprRow.cancellation_deadline, cprRow.cancellation_penalty, 0);

                    // check to see if a new roomID, if not, create a new List
                    Dictionary<DateTime, List<CancellationPolicy>> roomAndDateBasedCancellationRule;
                    if (!roomAndDateBasedCancellationRules.TryGetValue(roomId, out roomAndDateBasedCancellationRule))
                    {
                        roomAndDateBasedCancellationRule = new Dictionary<DateTime, List<CancellationPolicy>>();
                        roomAndDateBasedCancellationRules.TryAdd(roomId, roomAndDateBasedCancellationRule);
                    }

                    if (date.HasValue)
                    {
                        List<CancellationPolicy> cancellationPolicyPerDateList;
                        if (!roomAndDateBasedCancellationRule.TryGetValue(date.Value, out cancellationPolicyPerDateList))
                        {
                            cancellationPolicyPerDateList = new List<CancellationPolicy>();
                            roomAndDateBasedCancellationRule.Add(date.Value, cancellationPolicyPerDateList);
                        }

                        cancellationPolicyPerDateList.Add(cprActive);
                    }
                    else
                    {
                        roomBasedCancellationRules.TryAdd(roomId, new List<CancellationPolicy>() { cprActive });
                    }
                }
            });

            // Get Hotel data
            Console.WriteLine("*** Get Hotel data ***");

            foreach (var listHotels in PeakworkEDF.MultipleQueryLists(hotelRooms.Keys))
            {
                hotel_cancellation_policyTableAdapter hca = new hotel_cancellation_policyTableAdapter();
                hca.SetCommandTimeout(0, GeneralTimeout);
                var com = string.Format(hca.GetCommandText(0), "where", "cpgh.hotel_id IN (" + listHotels + ")");
                hca.SetCommandText(0, com);

                PeakworkEDFDataSet.hotel_cancellation_policyDataTable hotelData = hca.GetData();

                foreach (var cphRow in hotelData)
                {
                    string hotelId = cphRow.hotel_id.ToString();

                    List<CancellationPolicyPeriod> hotelPolicies;
                    if (!hotelBasedCancellationRules.TryGetValue(hotelId, out hotelPolicies))
                    {
                        hotelPolicies = new List<CancellationPolicyPeriod>();
                        hotelBasedCancellationRules.TryAdd(hotelId, hotelPolicies);
                    }

                    hotelPolicies.AddRange(CreatePeriods(cphRow));
                }
            }

        }

        /// <summary>
        /// Get a IEnumerable of CancellationRules
        /// Do the validation and return the correct Rules for this hotel, room and date
        /// </summary>
        /// <param name="hotelId"></param>
        /// <param name="roomId"></param>
        /// <param name="currentDate"></param>
        /// <returns></returns>
        public IEnumerable<string> GetCancellationRules(string hotelId, string roomId, DateTime currentDate)
        {
            List<CancellationPolicy> policies = new List<CancellationPolicy>();
            if (roomBasedCancellationRules.ContainsKey(roomId))
            {
                List<CancellationPolicy> cancelPerRoom;
                roomBasedCancellationRules.TryGetValue(roomId, out cancelPerRoom);
                policies.AddRange(cancelPerRoom);
            }
            else
            {
                if (roomAndDateBasedCancellationRules.ContainsKey(roomId))
                {
                    Dictionary<DateTime, List<CancellationPolicy>> cancelPerDay;
                    roomAndDateBasedCancellationRules.TryGetValue(roomId, out cancelPerDay);
                    if (cancelPerDay.ContainsKey(currentDate))
                    {
                        policies.AddRange(cancelPerDay[currentDate]);
                    }
                }

                if (hotelBasedCancellationRules.ContainsKey(hotelId))
                {
                    List<CancellationPolicyPeriod> cancelPerHotel;
                    hotelBasedCancellationRules.TryGetValue(hotelId, out cancelPerHotel);
                    var filteredPeriods = cancelPerHotel.Where(r => (r.StartDate == null) || (currentDate >= r.StartDate && currentDate <= r.EndDate));
                    policies.AddRange(filteredPeriods.Select(p => p.CancellationPolicy));
                }
            }

            if (policies.Count > 1)
            {
                //Sort per type ascending and later per deadline descending
                policies.Sort(new CancellationPolicyComparer(true));
                decimal? lastPenalty = null;
                string lastTypeId = null;
                foreach (CancellationPolicy cancellationPolicy in policies.ToArray())
                {
                    if (lastTypeId == null)
                    {
                        lastTypeId = cancellationPolicy.Penalty_Basis;
                    }
                    // New type (reset penalty)
                    else if (cancellationPolicy.Penalty_Basis != lastTypeId)
                    {
                        lastPenalty = null;
                        lastTypeId = cancellationPolicy.Penalty_Basis;
                    }

                    if (!lastPenalty.HasValue || cancellationPolicy.Penalty_Value > lastPenalty.Value)
                    {
                        lastPenalty = cancellationPolicy.Penalty_Value;
                    }
                    else
                    {
                        policies.Remove(cancellationPolicy);
                    }
                }
            }

            if (policies.Count > 1)
            {
                //Sort per deadline descending and later per penalty value descending
                policies.Sort(new CancellationPolicyComparer(false));
                var noRefund = policies.FirstOrDefault(IsCancellationPolicyNRF);
                if (noRefund != null)
                {
                    policies.RemoveAll(c => c != noRefund);
                }
                else
                {
                    // Look for the first "full_stay" cancellation policy and remove all policies below it.
                    var indexToRemoveBelow = policies.FindIndex(c => c.Penalty_Basis == "full_stay");
                    if (indexToRemoveBelow != -1 && indexToRemoveBelow != policies.Count - 1)
                    {
                        policies.RemoveAll(c => policies.IndexOf(c) > indexToRemoveBelow);
                    }
                }
            }

            //Return the correct values
            return policies.Select(p => p.Id);
        }

        /// <summary>
        /// Get the policies per hotel
        /// </summary>
        /// <param name="h"></param>
        public void GetPolicies(Hotel h)
        {
            var rooms = (from roomGroup in h.RoomGroups.Values
                         from room in roomGroup.Rooms.Values
                         select room).Union(
                                  from room in h.Rooms.Values
                                  select room
                                  );
            foreach (var room in rooms)
            {
                var removePeriods = new List<DateTime>();
                foreach (var period in room.AvailabilityPeriods.Values)
                {
                    var policies = GetCancellationRules(h.Id, room.RoomId, period.DateFrom);
                    if (policies.Count() == 0)
                    {
                        // Don't display periods without cancellation rules.
                        removePeriods.Add(period.DateFrom);
                    }
                    else
                    {
                        period.CancellationPolicies.AddRange(policies);
                    }
                }
                foreach (var removePeriod in removePeriods)
                {
                    room.AvailabilityPeriods.Remove(removePeriod);
                }
            }
        }

        public ConcurrentDictionary<string, bool> CancellationPoliciesNRF(bool create)
        {
            if (create)
            {
                AllCancellationPoliciesNRF = new ConcurrentDictionary<string, bool>();
                foreach (var cp in allCancellationPolicies)
                {
                    AllCancellationPoliciesNRF.TryAdd(cp.Key, IsCancellationPolicyNRF(cp.Value));
                }
            }
            return AllCancellationPoliciesNRF;
        }

        public bool IsCancellationPolicyNRF(CancellationPolicy cancellationPolicy)
        {
            return cancellationPolicy.Deadline_Value.GetValueOrDefault(-1) == -1;
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Create cancellation policy
        /// </summary>
        /// <param name="typeId"></param>
        /// <param name="deadline"></param>
        /// <param name="penalty"></param>
        /// <param name="unit"></param>
        /// <returns></returns>
        private CancellationPolicy CreateCancellationPolicyPeriod(int typeId, int? deadline, decimal penalty, int unit)
        {
            string policyId = String.Format("{0}.{1}.{2}", typeId, deadline, (int)penalty);
            CancellationPolicy policy;
            lock (allCancellationPolicies)
            {
                if (!allCancellationPolicies.TryGetValue(policyId, out policy))
                {
                    policy = new CancellationPolicy() { Id = policyId, Deadline_Basis = "arrival", Deadline_Value = !deadline.HasValue || deadline == -1 ? null : deadline, Deadline_Unit = "hours", Penalty_Value = Convert.ToInt32(penalty) };
                    switch (typeId)
                    {
                        case 1:
                            policy.Penalty_Basis = "nights";
                            break;
                        case 2:
                            policy.Penalty_Basis = "full_stay";
                            break;
                        case 3:
                            policy.Penalty_Basis = "fixed_amount";
                            policy.Penalty_Value = (int)Math.Ceiling(PeakworkEDF.Agent.CalculatePrice(policy.Penalty_Value));
                            break;
                        default: throw new NotImplementedException(String.Format("Cancelation policy type with id {0} is not implemented.", policyId));
                    }

                    allCancellationPolicies.Add(policy.Id, policy);
                }
            }
            return policy;
        }

        /// <summary>
        /// CreatePeriods with regards of dates and a new year
        /// </summary>
        /// <param name="cphRow"></param>
        /// <returns></returns>
        private IEnumerable<CancellationPolicyPeriod> CreatePeriods(SunHotels.Providers.PeakworkEDFDataSet.hotel_cancellation_policyRow cphRow)
        {
            CancellationPolicy policy = CreateCancellationPolicyPeriod(cphRow.cancellation_type_id, cphRow.cancellation_deadline, cphRow.cancellation_penalty, 0);

            string hotelId = cphRow.hotel_id.ToString();


            if (cphRow.Isstart_dateNull())
            {
                // This is a valid for the whole year
                return new CancellationPolicyPeriod[]
                {
                    new CancellationPolicyPeriod() {
                        StartDate = null,
                        EndDate = null,
                        CancellationPolicy = policy
                    }
                };
            }
            else
            {
                // This is a reoccurring date, we have to fake some years to the date

                DateTime startDate = cphRow.start_date;
                DateTime endDate = cphRow.end_date;

                CancellationPolicyPeriod[] createdPeriods;
                int thisYear = DateTime.Today.Year;
                int nextYear = thisYear + 1;

                if ((CreateDate(thisYear, endDate) - CreateDate(nextYear, startDate)).Days == -1) // 1900-12-31 - 1901-01-01 = -1
                {
                    // This is valid all the year around, lets set it to a long period.
                    createdPeriods = new CancellationPolicyPeriod[1];
                    createdPeriods[0] = new CancellationPolicyPeriod()
                    {
                        StartDate = CreateDate(thisYear, startDate),
                        EndDate = CreateDate(nextYear, endDate),
                        CancellationPolicy = policy
                    };

                }
                else if (endDate < startDate)
                {
                    // This span over the new year
                    createdPeriods = new CancellationPolicyPeriod[3];

                    createdPeriods[0] = new CancellationPolicyPeriod()
                    {
                        StartDate = CreateDate(thisYear - 1, startDate),
                        EndDate = CreateDate(thisYear, endDate),
                        CancellationPolicy = policy
                    };

                    createdPeriods[1] = new CancellationPolicyPeriod()
                    {
                        StartDate = CreateDate(thisYear, startDate),
                        EndDate = CreateDate(nextYear, endDate),
                        CancellationPolicy = policy
                    };

                    createdPeriods[2] = new CancellationPolicyPeriod()
                    {
                        StartDate = CreateDate(nextYear, startDate),
                        EndDate = CreateDate(nextYear + 1, endDate),
                        CancellationPolicy = policy
                    };
                }
                else
                {
                    createdPeriods = new CancellationPolicyPeriod[2];
                    createdPeriods[0] = new CancellationPolicyPeriod()
                    {
                        StartDate = CreateDate(thisYear, startDate),
                        EndDate = CreateDate(thisYear, endDate),
                        CancellationPolicy = policy
                    };

                    createdPeriods[1] = new CancellationPolicyPeriod()
                    {
                        StartDate = CreateDate(nextYear, startDate),
                        EndDate = CreateDate(nextYear, endDate),
                        CancellationPolicy = policy
                    };
                }

                return createdPeriods;
            }
        }

        /// <summary>
        /// Create date time
        /// </summary>
        /// <param name="year"></param>
        /// <param name="originalDate"></param>
        /// <returns></returns>
        private static DateTime CreateDate(int year, DateTime originalDate)
        {
            return new DateTime(year, originalDate.Month, originalDate.Day);
        }

        #endregion

        /// <summary>
        /// Private property that contains strat, enddate and CancellationPolicy
        /// </summary>
        private struct CancellationPolicyPeriod
        {
            public DateTime? StartDate { get; set; }
            public DateTime? EndDate { get; set; }
            public CancellationPolicy CancellationPolicy { get; set; }
        }

    }
}
