﻿using Sunhotels.Export;
using SunHotels.Export;
using SunHotels.Providers.Infrastructure;
using SunHotels.Providers.PeakworkEDFDataSetTableAdapters;
using SunHotels.XML;
using SunHotels.XML.Data;
using SunHotels.XML.Optimize;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Serialization;
using static SunHotels.Providers.Domain.PeakWorkDiscountCalculator;

namespace SunHotels.Providers
{
    public class PeakworkEDF : XMLProvider
    {
        private const int GeneralTimeout = 45 * 60;
        private const int QueryElements = 999;
        private const char csvDelimiter = ',';
        private HotelDal hotelDal { get; set; }

        public PeakworkEDF() : base()
        {
            hotelDal = new HotelDal(Properties.Settings.Default.sunhotelsConnectionString, GeneralTimeout);
        }

        public PeakworkEDF(Configuration config) :
            base(config)
        {
            hotelDal = new HotelDal(Properties.Settings.Default.sunhotelsConnectionString, GeneralTimeout);
        }

        private bool IsDeltaFile
        {
            get
            {
                return configuration.IsDeltaFile;
            }
        }
        private string _availabilityPatternOutput
        {
            get
            {
                return string.Format(configuration.AvailabilityPatternOutput, _localConfig.MarketLabel);
            }
        }

        private PeakworkEDFSettings _localConfig;
        protected PeakworkEDFSettings Settings
        {
            get
            {
                if (_localConfig == null)
                {
                    var providerConfigFullPath = configuration.ExportConfigFileFullPath;
                    if (File.Exists(providerConfigFullPath))
                    {
                        var serializer = new XmlSerializer(typeof(PeakworkEDFSettings));
                        var s = new FileStream(providerConfigFullPath, FileMode.Open, FileAccess.Read);
                        _localConfig = (PeakworkEDFSettings)serializer.Deserialize(s);
                    }
                    else
                    {
                        _localConfig = new PeakworkEDFSettings();
                    }
                }
                return _localConfig;
            }
        }

        private BaseOptimizer _optimizer;
        protected override BaseOptimizer Optimizer
        {
            get { return _optimizer ?? (_optimizer = new PeakworkEDFOptimizer(configuration)); }
        }

        private BaseFileExporter _exporter;
        protected override BaseFileExporter Exporter
        {
            get { return _exporter ?? (_exporter = new PeakworkEDFExporter(configuration, Settings, (PeakworkEDFOptimizer)Optimizer, IsDeltaFile)); }
        }

        public static PeakworkEDFDataSet.agentRow Agent { get; private set; }
        private CancellationRulesManager _cancellationRulesManager;
        private readonly Dictionary<int, Hotel> _hotelSearch = new Dictionary<int, Hotel>();
        private List<string> _roomTypesList;
        private ConcurrentDictionary<string, string> _availabilityPattern = new ConcurrentDictionary<string, string>();

        protected override void getProviderDefinition(Root dataRoot)
        {
            dataRoot.ProviderDefinition.Name = configuration.Provider;
            dataRoot.ProviderDefinition.Currency = configuration.Currency;
        }


        protected override void getData(Root dataRoot)
        {
            var getDataLogInfo = new StringBuilder();
            var dStart = DateTime.Now;

            try
            {

                Thread.CurrentThread.CurrentCulture = Thread.CurrentThread.CurrentUICulture = CultureInfo.InvariantCulture;
                Settings.LogFileName = $"{dStart.ToString("yyyy-MM-dd_HH-mm-ss")}_{configuration.UserId}.log";

                GetAgentInformation();

                Settings.AgentLanguageCode = Agent.cc != "ge" ? "en" : Agent.cc;

                WriteLog("Start", getDataLogInfo);

                WriteLog("Fetching countries, destinations and resorts...", getDataLogInfo);
                GetPlaces(dataRoot);
                WriteLog("Fetching countries, destinations and resorts done.", getDataLogInfo);

                Settings.Mapping = new Dictionary<string, string[]>();
                var dataMapper = DataMapper["RoomType"];
                if (dataMapper != null)
                {
                    foreach (var mappedRoomType in dataMapper)
                    {
                        int roomtypeId;
                        Settings.Mapping.Add(mappedRoomType.Key, mappedRoomType.Value.Where(v => !string.IsNullOrEmpty(v) && int.TryParse(v, out roomtypeId)).Distinct().ToArray());
                    }
                }
                _roomTypesList = Settings.Mapping.SelectMany(m => m.Value != null ? m.Value : new string[0]).ToList();

                ILookup<int, PeakworkEDFDataSet.roomGroupRow> roomGroups = null;
                if (!string.IsNullOrEmpty(configuration.RoomGroupsOutput))
                {
                    WriteLog("Fetching Room Groups...", getDataLogInfo);
                    roomGroups = new roomGroupTableAdapter { CommandTimeout = GeneralTimeout }.GetData().ToLookup(r => r.hotelId, r => r);
                    WriteLog("Fetching Room Groups done.", getDataLogInfo);
                }

                WriteLog("Fetching Hotels...", getDataLogInfo);

                var numHotels = GetHotels(dataRoot);
                WriteLog("Fetching Hotels done.", getDataLogInfo);

                RetrieveAvailabilityPattern();

                var sw = new System.Diagnostics.Stopwatch();
                sw.Start();

                WriteLog("Fetching Hotel data.", getDataLogInfo);

                WriteLog("Start ProcessPlaceHotelsInParallel", getDataLogInfo);

                var isPushOut = Agent.isPushOut;
                var agentId = Agent.id;
                var hideXMLRooms = Settings.HideXMLRooms;
                var hideManualRooms = Settings.HideManualRooms;
                var showXmlOnlyIfMissingManual = Settings.ShowXmlOnlyIfMissingManual;
                ProcessPlaceHotelsInParallel(dataRoot, roomGroups, hideXMLRooms, hideManualRooms, showXmlOnlyIfMissingManual, isPushOut, agentId);
                WriteLog("End ProcessPlaceHotelsInParallel", getDataLogInfo);

                WriteLog("Start CreateAvailabilityPatternFile", getDataLogInfo);
                CreateAvailabilityPatternFile(_availabilityPatternOutput);
                WriteLog("End CreateAvailabilityPatternFile Finished", getDataLogInfo);

                sw.Stop();

                long elapsedAverageTime = 1;
                if (numHotels > 0)
                    elapsedAverageTime = sw.ElapsedMilliseconds / numHotels;

                WriteLog($"Fetching Hotel data done.\nAverage time/hotel (ms): {elapsedAverageTime}", getDataLogInfo);
                WriteLog($"Start : {dStart}", getDataLogInfo);
                WriteLog($"End   : {DateTime.Now}", getDataLogInfo);
            }
            catch (Exception e)
            {
                var errorList = new List<string>();
                Exception innerException = e.InnerException;
                var i = 0;
                while (innerException != null && i < 10)
                {
                    errorList.Add(innerException.Message + innerException.StackTrace);
                    innerException = innerException.InnerException;
                    i++;
                }
                errorList.Add(e.Message + e.StackTrace);
                if (!Directory.Exists(configuration.LogOutput))
                {
                    Directory.CreateDirectory(configuration.LogOutput);
                }
                File.AppendAllLines(configuration.LogOutput + Settings.LogFileName, errorList);
                throw e;
            }
            finally
            {
                File.AppendAllText($"{configuration.LogOutput}{Settings.LogFileName}", getDataLogInfo.ToString());
            }
        }

        private void GetAgentInformation()
        {
            var agentTable = new agentTableAdapter { CommandTimeout = GeneralTimeout }.GetData(int.Parse(configuration.UserId));

            if (agentTable.Rows.Count != 1)
                throw new Exception(string.Format("Agent with id {0} not found.", configuration.UserId));
            Agent = agentTable.First();
        }

        public void ProcessPlaceHotelsInParallel(
            Root dataRoot,
            ILookup<int, PeakworkEDFDataSet.roomGroupRow> roomGroups,
            bool hideXMLRooms,
            bool hideManualRooms,
            bool showXmlOnlyIfMissingManual,
            bool isPushOut,
            int agentId)
        {
            var options = new ParallelOptions { MaxDegreeOfParallelism = configuration.MaxDegreeOfParallelism };
            var discountCacheOutputEnabled = !string.IsNullOrEmpty(configuration.DiscountCacheOutput);
            var cancellationPolicyCacheOutputEnabled = !string.IsNullOrEmpty(configuration.CancellationPolicyCacheOutput);
            var roomGroupsOutputEnabled = !string.IsNullOrEmpty(configuration.RoomGroupsOutput);

            var placesWithHotels = dataRoot.AllPlaces().Where(p => p.Hotels != null && p.Hotels.Any());

            var currentDate = DateTime.Today;
            var endDate = currentDate.AddDays(configuration.NrDaysInCache);

            Parallel.ForEach(placesWithHotels, options, p =>
            {
                Parallel.ForEach(p.Hotels.Values, options, hotel =>
                {
                    Thread.CurrentThread.CurrentCulture = Thread.CurrentThread.CurrentUICulture = CultureInfo.InvariantCulture;
                    var op = ((PeakworkEDFOptimizer)Optimizer);

                    if (hotel.Rooms != null && hotel.Rooms.Count > 0)
                    {

                        GetAvailability(hotel, endDate, hideXMLRooms, hideManualRooms, showXmlOnlyIfMissingManual, isPushOut, agentId);

                        if (discountCacheOutputEnabled)
                            GetDiscounts(hotel);

                        if (roomGroupsOutputEnabled)
                            GetRoomGroups(hotel, roomGroups);
                        // Get the policy per hotel - Invoke after availability
                        if (cancellationPolicyCacheOutputEnabled)
                            _cancellationRulesManager.GetPolicies(hotel);

                        // Check the availability per hotel 
                        op.DiscountMinDaysToLastDate(hotel);
                        op.OptimizeAvailabilityPeriods(hotel);
                        if (cancellationPolicyCacheOutputEnabled)
                        {
                            var cancellationPoliciesNRF = _cancellationRulesManager.CancellationPoliciesNRF(false);
                            op.CancellationPolicyNRF(dataRoot, hotel, cancellationPoliciesNRF, _localConfig.RemoveNRF);
                        }

                        op.RemoveRoomsWithoutAvailability(hotel);

                        var roomsPattern = op.GetRoomsPattern(hotel, currentDate, endDate);
                        if (IsDeltaFile)
                        {
                            // Check and filter rooms that have not changed since last export
                            op.FilterDeltaRooms(hotel, roomsPattern, _availabilityPattern);
                        }
                        else
                        {
                            foreach (var roomPattern in roomsPattern)
                            {
                                _availabilityPattern.TryAdd(string.Join("@", hotel.Id, roomPattern.Key), roomPattern.Value);
                            }
                        }

                    }
                });

                var hRemove = GetHotelsWithoutRoom(p);
                foreach (var key in hRemove)
                {
                    p.Hotels.Remove(key);
                }

            });
        }

        private static string[] GetHotelsWithoutRoom(Place p)
        {
            return p.Hotels
                .Where(h =>
                    (h.Value.Rooms == null || h.Value.Rooms.Count == 0) &&
                    (h.Value.RoomGroups == null || h.Value.RoomGroups.Count == 0 || !h.Value.RoomGroups.Any(rg => rg.Value.Rooms != null && rg.Value.Rooms.Count > 0)))
                .Select(h => h.Key)
                .ToArray();
        }

        private static void GetRoomGroups(Hotel hotel, ILookup<int, PeakworkEDFDataSet.roomGroupRow> roomGroups)
        {
            var hotelId = int.Parse(hotel.Id);

            foreach (var roomGroup in roomGroups[hotelId])
            {
                string roomId = roomGroup.roomId.ToString();
                Room room = hotel.Rooms.Values.FirstOrDefault(r => r.RoomId == roomId);

                if (room == null)
                {
                    continue;
                }

                var edfRoomId = room.TypeId;
                var groupId = roomGroup.groupId.ToString();

                RoomGroup hotelRoomGroup;
                if (hotel.RoomGroups.TryGetValue(groupId, out hotelRoomGroup))
                {
                    hotelRoomGroup.Rooms.Add(edfRoomId, room);
                }
                else
                {
                    hotelRoomGroup = new RoomGroup
                    {
                        RoomGroupId = groupId,
                        Rooms = new Dictionary<string, Room> { { edfRoomId, room } }
                    };

                    hotel.RoomGroups.Add(groupId, hotelRoomGroup);
                }

                hotel.Rooms.Remove(edfRoomId);
            }
        }

        private int GetHotels(Root dataRoot)
        {
            var specificDestinations = configuration.SpecificDestinations;
            var userId = int.Parse(configuration.UserId);
            var userAgentPush = Agent.isPushOut;

            var sw = new System.Diagnostics.Stopwatch();
            sw.Start();

            WriteToConsole("GetHotels ... ", sw);

            var bHotels = GetBlockedHotelsFromConfiguration();
            Settings.BlockedHotels.AddRange(bHotels);

            IEnumerable<HotelDataRow> hotelTableRows = null;
            Dictionary<int, string> giataCodes = null;
            HashSet<int> hotelsSuspended = null;

            var tasks = new List<Task>()
            {
                { Task.Run(async()=> { hotelTableRows = await hotelDal.GetExportHotelAsync(specificDestinations, userId, userAgentPush); }) },
                { Task.Run(()=> { giataCodes = GiataHotelCodes(); }) },
                { Task.Run(()=> { hotelsSuspended = HotelsSuspended();  }) }
            };

            WriteToConsole("Start [GetHotelTableEntries , GiataHotelCodes, HotelsSuspended]", sw);
            Task.WaitAll(tasks.ToArray());
            WriteToConsole("Finished [GetHotelTableEntries , GiataHotelCodes, HotelsSuspended]", sw);

            var resorts = dataRoot.Places.SelectMany(c => c.Places).SelectMany(d => d.Places).ToDictionary(res => res.Id, res => res);

            WriteToConsole("Start processing HotelTable Items", sw);
            foreach (var hRow in hotelTableRows)
            {
                var hotelId = hRow.id;
                if (hotelsSuspended.Contains(hotelId))
                {
                    continue;
                }

                var placeId = hRow.resort.ToString();
                if (Settings.BlockedHotels.Contains(hotelId))
                    continue;

                Place currentPlace;
                if (!resorts.TryGetValue(placeId, out currentPlace))
                {
                    continue;
                }

                string giataCode;
                if (!giataCodes.TryGetValue(hotelId, out giataCode))
                {
                    continue;
                }

                if (!hRow.ShowinEN)
                    continue;

                var idAsString = hotelId.ToString(CultureInfo.InvariantCulture);
                var hotel = new Hotel { Id = idAsString, Name = hRow.hotelName, PlaceId = placeId };
                currentPlace.Hotels.Add(idAsString, hotel);
                hotel.Identifiers.Add(new Identifier { Type = "GIATA", Value = giataCode });
                _hotelSearch.Add(hotelId, hotel);

            }
            sw.Stop();
            WriteToConsole("Finished processing HotelTable Items", sw);


            var hotelsFilteredByRoomProvider = HotelsFilteredByRoomProvider(root.AllHotels());
            var hotelList = hotelsFilteredByRoomProvider.Keys;
            var resortsWithHotels = resorts.Values.Where(r => r.Hotels.Count > 0);
            foreach (var resort in resortsWithHotels)
            {
                resort.Hotels.Keys
                    .Where(hotelId => !hotelList.Contains(hotelId))
                    .ToList()
                    .ForEach(hotelId =>
                    {
                        resort.Hotels.Remove(hotelId);
                        _hotelSearch.Remove(int.Parse(hotelId));
                    });
            }

            Console.WriteLine("------ List Hotel ------");

            Console.WriteLine("INI -> StandardData -> {0}", DateTime.Now);
            GetStandardData(hotelsFilteredByRoomProvider);
            Console.WriteLine("END -> StandardData -> {0}", DateTime.Now);

            Console.WriteLine("INI -> Rooms -> {0}", DateTime.Now);
            GetRooms(hotelsFilteredByRoomProvider);
            Console.WriteLine("END -> Rooms -> {0}", DateTime.Now);

            if (!string.IsNullOrEmpty(configuration.CancellationPolicyCacheOutput))
            {
                Console.WriteLine("INI -> CancellationPolicies -> {0}", DateTime.Now);
                _cancellationRulesManager = new CancellationRulesManager(configuration.MaxDegreeOfParallelism);
                _cancellationRulesManager.LoadCancelationRules(dataRoot.CancellationsPolicies);
                _cancellationRulesManager.LoadCancelationRulesHotelRoom(hotelsFilteredByRoomProvider, GeneralTimeout);
                _cancellationRulesManager.CancellationPoliciesNRF(true);
                Console.WriteLine("END -> CancellationPolicies -> {0}", DateTime.Now);
            }

            Console.WriteLine("Hotels Filtered (" + hotelTableRows.Count() + "-" + hotelList.Count + "): " + (hotelTableRows.Count() - hotelList.Count));

            return hotelList.Count;
        }

        private static void WriteLog(string text, StringBuilder sb)
        {
            var msg = $"{DateTime.Now.ToShortDateString()}: {text}";
            sb.Append(msg);
            Console.WriteLine(msg);
        }
        private static void WriteToConsole(string msg, System.Diagnostics.Stopwatch sw)
        {
            Console.WriteLine($"{msg} , [{sw.ElapsedMilliseconds / 1000} sec]");
            sw.Reset();
            sw.Start();
        }

        private List<int> GetBlockedHotelsFromConfiguration()
        {
            var blockedHotels = configuration.BlockHotels;
            if (string.IsNullOrEmpty(blockedHotels))
                return new List<int>();

            return new List<int>(blockedHotels.Split(',').Select(int.Parse).ToList());
        }

        private void GetStandardData(Dictionary<string, List<string>> hotelRooms)
        {
            foreach (var hotelList in MultipleQueryLists(hotelRooms.Keys))
            {
                var hotelStandard = new hotelStandardTableAdapter();
                hotelStandard.SetCommandTimeout(0, GeneralTimeout);
                var command = hotelStandard.GetCommandText(0).Replace("'replaceToken' = 'replaceToken2'", "h.id IN (" + hotelList + ")");
                hotelStandard.SetCommandText(0, command);

                var hStatic = hotelStandard.GetData();

                foreach (var hRow in hStatic)
                {
                    Hotel hotel;
                    if (!_hotelSearch.TryGetValue(hRow.id, out hotel))
                    {
                        continue;
                    }

                    if (hRow.BlockInfant)
                    {
                        hotel.BlockInfant = hRow.BlockInfant;
                    }
                    if (hRow.AdultOnly)
                    {
                        hotel.AdultOnly = hRow.AdultOnly;
                    }

                    hotel.Adress_Street1 = hRow.addr_1.Trim();
                    hotel.Adress_Street2 = hRow.addr_2.Trim();
                    hotel.Adress_City = hRow.addr_city.Trim();
                    hotel.Adress_State = hRow.addr_state.Trim();
                    hotel.Adress_Country = hRow.addr_country.Trim();
                    hotel.Adress_Zipcode = hRow.addr_zip.Trim();
                    hotel.Classification = hRow.sunClass.Trim();
                    if (!String.IsNullOrEmpty(hRow.classPlus))
                    {
                        hotel.Classification += hRow.classPlus;
                    }
                    hotel.Fax = hRow.fax.Trim();
                    hotel.Phone = hRow.telephone.Trim();

                    try
                    {
                        if (!hRow.IsGLatNull() && !hRow.IsGLngNull())
                        {
                            hotel.Position_Latitude = hRow.GLat.ToString();
                            hotel.Position_Longitude = hRow.GLng.ToString();
                        }
                    }
                    catch
                    {
                        Console.WriteLine("Longitude or latidute DBNull on hotel with id: {0}", hotel.Id);
                    }
                }
            }
        }

        private void GetRooms(Dictionary<string, List<string>> hotelRooms)
        {

            var query = @"SELECT
                                ic.RoomId,
                                ic.RoomConfigurationInfantsAndChildrenId,
                                oc.RoomConfigurationOccupancyId,
                                ef.RoomConfigurationExtrabedFactorId,
                                ob.RoomConfigurationOccupancyBlockId
                            FROM dbo.Rooms_RoomConfigurationInfantsAndChildren ic WITH (NOLOCK)
	                            INNER JOIN Rooms_RoomConfigurationOccupancies oc WITH (NOLOCK) ON oc.RoomId = ic.RoomId
	                            LEFT JOIN Rooms_RoomConfigurationExtrabedFactors ef WITH (NOLOCK) ON ef.RoomId = ic.RoomId
	                            LEFT JOIN Rooms_RoomConfigurationOccupancyBlocks ob WITH (NOLOCK) ON ob.RoomId = ic.RoomId
                            WHERE ic.RoomId IN (@RoomId)";

            Parallel.ForEach(MultipleQueryLists(hotelRooms.Values.SelectMany(lr => lr)), new ParallelOptions { MaxDegreeOfParallelism = configuration.MaxDegreeOfParallelism }, roomIdList =>
            {
                using (var roomAdapter = new roomsStaticTableAdapter())
                {
                    roomAdapter.SetCommandTimeout(0, GeneralTimeout);
                    var commandRoomStatic = roomAdapter.GetCommandText(0).Replace("'replaceToken' = 'replaceToken3'", "r.ID IN (" + roomIdList + ")");
                    roomAdapter.SetCommandText(0, commandRoomStatic);

                    using (var roomDataTable = roomAdapter.GetData(_localConfig.AgentLanguageCode))
                    {
                        using (var roomWeeklyAdapter = new roomsWeeklyTableAdapter())
                        {
                            roomWeeklyAdapter.SetCommandTimeout(0, GeneralTimeout);
                            var com = roomWeeklyAdapter.GetCommandText(0).Replace("'replaceToken' = 'replaceToken15'", "r.ID IN (" + roomIdList + ")");
                            roomWeeklyAdapter.SetCommandText(0, com);

                            using (var roomWeeklyDataTable = roomWeeklyAdapter.GetData())
                            {
                                var roomWeelky = new Dictionary<int, PeakworkEDFDataSet.roomsWeeklyRow>();
                                foreach (var rw in roomWeeklyDataTable)
                                {
                                    if (!roomWeelky.ContainsKey(rw.roomid))
                                    {
                                        roomWeelky[rw.roomid] = rw;
                                    }
                                }

                                // Read room configurations
                                var roomConfigInfantsAndChildren = new Dictionary<int, int>();
                                var roomConfigOccupancy = new Dictionary<int, int>();
                                var roomConfigExtrabedFactors = new Dictionary<int, List<int>>();
                                var roomConfigOccupancyBlocks = new Dictionary<int, List<int>>();
                                using (var connection = new SqlConnection(Properties.Settings.Default.sunhotelsConnectionString))
                                {
                                    using (var command = new SqlCommand())
                                    {
                                        command.Connection = connection;
                                        command.CommandTimeout = GeneralTimeout;
                                        command.CommandText = query.Replace("@RoomId", roomIdList);
                                        connection.Open();
                                        using (var rdr = command.ExecuteReader(CommandBehavior.SequentialAccess))
                                        {
                                            while (rdr.Read())
                                            {
                                                var roomId = (int)rdr["RoomId"];
                                                var roomConfigInfantsAndChildrenId = (int)rdr["RoomConfigurationInfantsAndChildrenId"];
                                                var roomConfigOccupancyId = (int)rdr["RoomConfigurationOccupancyId"];
                                                var roomConfigExtrabedFactorId = rdr.Field<int?>("RoomConfigurationExtrabedFactorId");
                                                var roomConfigOccupancyBlockId = rdr.Field<int?>("RoomConfigurationOccupancyBlockId");

                                                if (!roomConfigInfantsAndChildren.ContainsKey(roomId))
                                                {
                                                    roomConfigInfantsAndChildren.Add(roomId, roomConfigInfantsAndChildrenId);
                                                }
                                                if (!roomConfigOccupancy.ContainsKey(roomId))
                                                {
                                                    roomConfigOccupancy.Add(roomId, roomConfigOccupancyId);
                                                }
                                                if (roomConfigExtrabedFactorId.HasValue)
                                                {
                                                    List<int> roomExtrabedFactors;
                                                    if (!roomConfigExtrabedFactors.TryGetValue(roomId, out roomExtrabedFactors))
                                                    {
                                                        roomExtrabedFactors = new List<int> { roomConfigExtrabedFactorId.Value };
                                                        roomConfigExtrabedFactors.Add(roomId, roomExtrabedFactors);
                                                    }
                                                    else if (!roomExtrabedFactors.Contains(roomConfigExtrabedFactorId.Value))
                                                    {
                                                        roomExtrabedFactors.Add(roomConfigExtrabedFactorId.Value);
                                                    }
                                                }
                                                if (roomConfigOccupancyBlockId.HasValue)
                                                {
                                                    List<int> roomOccupancyBlocks;
                                                    if (!roomConfigOccupancyBlocks.TryGetValue(roomId, out roomOccupancyBlocks))
                                                    {
                                                        roomOccupancyBlocks = new List<int> { roomConfigOccupancyBlockId.Value };
                                                        roomConfigOccupancyBlocks.Add(roomId, roomOccupancyBlocks);
                                                    }
                                                    else if (!roomOccupancyBlocks.Contains(roomConfigOccupancyBlockId.Value))
                                                    {
                                                        roomOccupancyBlocks.Add(roomConfigOccupancyBlockId.Value);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }

                                foreach (var roomRow in roomDataTable)
                                {
                                    Hotel hotel;
                                    if (!_hotelSearch.TryGetValue(roomRow.hotelId, out hotel))
                                    {
                                        continue;
                                    }

                                    var room = new Room();
                                    room.RoomId = roomRow.ID.ToString();
                                    room.Description = roomRow.RoomTypeName.Trim();
                                    room.MealSupplementRestriction = roomRow.MealSupplementRestriction;

                                    if (roomRow.bestbuy == 1)
                                    {
                                        room.BestBuy = true;
                                    }

                                    int configId;
                                    List<int> configIds;
                                    room.RoomConfigurationInfantsAndChildrenId = roomConfigInfantsAndChildren.TryGetValue(roomRow.ID, out configId) ? configId : 0;
                                    room.RoomConfigurationOccupancyId = roomConfigOccupancy.TryGetValue(roomRow.ID, out configId) ? configId : 0;
                                    room.RoomConfigurationExtrabedFactors = roomConfigExtrabedFactors.TryGetValue(roomRow.ID, out configIds) ? configIds : new List<int>();
                                    room.RoomConfigurationOccupancyBlocks = roomConfigOccupancyBlocks.TryGetValue(roomRow.ID, out configIds) ? configIds : new List<int>();

                                    var edfRoomId = String.Format("{0}.{1}", roomRow.roomtypes_id, room.RoomId);

                                    RoomType rType;
                                    lock (root)
                                    {
                                        if (!root.RoomTypes.TryGetValue(edfRoomId, out rType))
                                        {
                                            rType = new RoomType();
                                            root.RoomTypes.Add(edfRoomId, rType);
                                        }
                                    }
                                    rType.Beds = roomRow.beds;
                                    rType.ExtraBeds = roomRow.extraBeds;
                                    rType.Type_Id = roomRow.roomtypes_id.ToString();
                                    rType.Description = room.Description;
                                    rType.SharedRoom = roomRow.SharedRoom;
                                    rType.SharedFacilities = roomRow.SharedFacilities;
                                    rType.NonRefundable = (roomRow.norefund == 1);
                                    room.TypeId = edfRoomId;

                                    PeakworkEDFDataSet.roomsWeeklyRow roomWeekly;
                                    if (roomRow.weekly == 1 && roomWeelky.TryGetValue(roomRow.ID, out roomWeekly))
                                    {
                                        rType.IsWeeklyStay = true;
                                        rType.Starting_Days_Monday = roomWeekly.d1;
                                        rType.Starting_Days_Tuesday = roomWeekly.d2;
                                        rType.Starting_Days_Wednesday = roomWeekly.d3;
                                        rType.Starting_Days_Thursday = roomWeekly.d4;
                                        rType.Starting_Days_Friday = roomWeekly.d5;
                                        rType.Starting_Days_Saturday = roomWeekly.d6;
                                        rType.Starting_Days_Sunday = roomWeekly.d7;
                                    }

                                    lock (hotel)
                                    {
                                        hotel.Rooms.Add(edfRoomId, room);
                                    }
                                }
                            }
                        }
                    }
                }
            });

            GetRoomsRootConfiguration();
        }

        private void GetRoomsRootConfiguration()
        {
            var rooms = _hotelSearch.Values.SelectMany(h =>
                (h.Rooms != null ? h.Rooms.Values.ToList() : new List<Room>())
                .Concat(h.RoomGroups != null ? h.RoomGroups.Values.SelectMany(rg => rg.Rooms != null ? rg.Rooms.Values.ToList() : new List<Room>()) : new List<Room>())
            );
            var roomConfigInfantsAndChildrenList = string.Join(",", rooms.Select(r => r.RoomConfigurationInfantsAndChildrenId).Where(id => id > 0).Distinct());
            var roomConfigurationOccupancyList = string.Join(",", rooms.Select(r => r.RoomConfigurationOccupancyId).Where(id => id > 0).Distinct());
            var roomConfigurationExtrabedFactorsList = string.Join(",", rooms.SelectMany(r => r.RoomConfigurationExtrabedFactors).Where(id => id > 0).Distinct());
            var roomConfigurationOccupancyBlocksList = string.Join(",", rooms.SelectMany(r => r.RoomConfigurationOccupancyBlocks).Where(id => id > 0).Distinct());

            if (!string.IsNullOrEmpty(roomConfigInfantsAndChildrenList))
            {
                using (var connection = new SqlConnection(Properties.Settings.Default.sunhotelsConnectionString))
                {
                    using (var command = new SqlCommand())
                    {
                        command.Connection = connection;
                        command.CommandTimeout = GeneralTimeout;
                        command.CommandText = @"SELECT * FROM RoomConfigurationInfantsAndChildren WITH (NOLOCK) WHERE Id IN (" + roomConfigInfantsAndChildrenList + @")";
                        connection.Open();
                        using (var rdr = command.ExecuteReader(CommandBehavior.SequentialAccess))
                        {
                            while (rdr.Read())
                            {
                                var row = rdr;
                                var id = (int)row["Id"];
                                var infantCountAsChild = (bool)row["InfantCountAsChild"];
                                var maxChildAge = (int)row["MaxChildAge"];

                                if (!root.RoomConfigInfantsAndChildren.ContainsKey(id))
                                {
                                    root.RoomConfigInfantsAndChildren.Add(id, new RoomConfigurationInfantsAndChildren
                                    {
                                        Id = id,
                                        InfantCountAsChild = infantCountAsChild,
                                        MaxChildAgeInclusive = maxChildAge
                                    });
                                }
                            }
                        }
                    }
                }
            }

            if (!string.IsNullOrEmpty(roomConfigurationOccupancyList))
            {
                using (var connection = new SqlConnection(Properties.Settings.Default.sunhotelsConnectionString))
                {
                    using (var command = new SqlCommand())
                    {
                        command.Connection = connection;
                        command.CommandTimeout = GeneralTimeout;
                        command.CommandText = @"SELECT * FROM RoomConfigurationOccupancies WITH (NOLOCK) WHERE Id IN (" + roomConfigurationOccupancyList + @")";
                        connection.Open();
                        using (var rdr = command.ExecuteReader(CommandBehavior.SequentialAccess))
                        {
                            while (rdr.Read())
                            {
                                var row = rdr;
                                var id = (int)row["Id"];
                                var minOccupancy = (int)row["MinOccupancy"];
                                var maxOccupancy = (int)row["MaxOccupancy"];
                                var minAdults = (int)row["MinAdults"];
                                var maxAdults = (int)row["MaxAdults"];
                                var minChildren = (int)row["MinChildren"];
                                var maxChildren = (int)row["MaxChildren"];

                                if (!root.RoomConfigOccupancies.ContainsKey(id))
                                {
                                    root.RoomConfigOccupancies.Add(id, new RoomConfigurationOccupancies
                                    {
                                        Id = id,
                                        MinOccupancy = minOccupancy,
                                        MaxOccupancy = maxOccupancy,
                                        MinAdults = minAdults,
                                        MaxAdults = maxAdults,
                                        MinChildren = minChildren,
                                        MaxChildren = maxChildren
                                    });
                                }
                            }
                        }
                    }
                }
            }

            if (!string.IsNullOrEmpty(roomConfigurationExtrabedFactorsList))
            {
                using (var connection = new SqlConnection(Properties.Settings.Default.sunhotelsConnectionString))
                {
                    using (var command = new SqlCommand())
                    {
                        command.Connection = connection;
                        command.CommandTimeout = GeneralTimeout;
                        command.CommandText = @"SELECT * FROM RoomConfigurationExtrabedFactors WITH (NOLOCK) WHERE Id IN (" + roomConfigurationExtrabedFactorsList + @")";
                        connection.Open();
                        using (var rdr = command.ExecuteReader(CommandBehavior.SequentialAccess))
                        {
                            while (rdr.Read())
                            {
                                var row = rdr;
                                var id = (int)row["Id"];
                                var fromAgeInclusive = (int)row["FromAgeInclusive"];
                                var toAgeExclusive = (int)row["ToAgeExclusive"];
                                var fromQuantityInclusive = (int)row["FromQuantityInclusive"];
                                var toQuantityInclusive = (int)row["ToQuantityInclusive"];
                                var value = (decimal)row["Value"];

                                if (!root.RoomConfigExtrabedFactors.ContainsKey(id))
                                {
                                    root.RoomConfigExtrabedFactors.Add(id, new RoomConfigurationExtrabedFactors
                                    {
                                        Id = id,
                                        FromAgeInclusive = fromAgeInclusive,
                                        ToAgeExclusive = toAgeExclusive,
                                        FromQuantityInclusive = fromQuantityInclusive,
                                        ToQuantityInclusive = toQuantityInclusive,
                                        Value = value
                                    });
                                }
                            }
                        }
                    }
                }
            }

            if (!string.IsNullOrEmpty(roomConfigurationOccupancyBlocksList))
            {
                using (var connection = new SqlConnection(Properties.Settings.Default.sunhotelsConnectionString))
                {
                    using (var command = new SqlCommand())
                    {
                        command.Connection = connection;
                        command.CommandTimeout = GeneralTimeout;
                        command.CommandText = @"SELECT * FROM RoomConfigurationOccupancyBlocks WITH (NOLOCK) WHERE Id IN (" + roomConfigurationOccupancyBlocksList + @")";
                        connection.Open();
                        using (var rdr = command.ExecuteReader(CommandBehavior.SequentialAccess))
                        {
                            while (rdr.Read())
                            {
                                var row = rdr;
                                var id = (int)row["Id"];
                                var numberOfAdults = (int)row["NumberOfAdults"];
                                var numberOfChildren = (int)row["NumberOfChildren"];

                                if (!root.RoomConfigOccupancyBlocks.ContainsKey(id))
                                {
                                    root.RoomConfigOccupancyBlocks.Add(id, new RoomConfigurationOccupancyBlocks
                                    {
                                        Id = id,
                                        NumberOfAdults = numberOfAdults,
                                        NumberOfChildren = numberOfChildren
                                    });
                                }
                            }
                        }
                    }
                }
            }
        }



        private void GetAvailability(Hotel h, DateTime date, bool hideXMLRooms, bool hideManualRooms, bool showXmlOnlyIfMissingManual, bool isPushOut, int agentId)
        {
            var hotelId = int.Parse(h.Id);

            IEnumerable<CalendarDataRow> availability = null;

            Task.Run(async () =>
            {
                availability = await hotelDal.GetAvailabilityAsync(hotelId, agentId, hideManualRooms, hideManualRooms, showXmlOnlyIfMissingManual, isPushOut);
            }).Wait();

            if (availability == null) return;

            var pushOutDiscounts = new Dictionary<int, Dictionary<DateTime, PushOutDiscount>>();
            if (isPushOut && availability.Any(r => r.IncludeEarlyBookingInPrice))
            {
                pushOutDiscounts = GetPushOutDiscounts(hotelId, date);
            }

            foreach (var row in availability)
            {
                var edfRoomId = $"{row.roomtypes_id}.{row.Id}";

                Room room;
                if (!h.Rooms.TryGetValue(edfRoomId, out room))
                    continue;

                if (row.datum > date)
                    continue;

                PushOutDiscount pushOutDiscount = null;

                Dictionary<DateTime, PushOutDiscount> roomPushOutDiscounts;
                if (pushOutDiscounts != null && pushOutDiscounts.TryGetValue(row.Id, out roomPushOutDiscounts))
                {
                    roomPushOutDiscounts.TryGetValue(row.datum, out pushOutDiscount);
                }

                var basePrice = Math.Round(GetPriceWithDiscount(Agent.CalculatePrice(row.price), pushOutDiscount, PriceType.Base), 2);
                var period = new AvailabilityPeriod
                {
                    DateFrom = row.datum,
                    DateTo = row.datum,
                    AvailabilityAvailableUnits = (short)row.units,
                    MinimumStay = (short)row.MinDays,
                    ReleaseDays = (short)row.releaseTime,
                    Pricing_BasePrice = basePrice.ToString("f2"),
                    Pricing_BaseIncludedBoard = GetBoardTypes(row.meals),
                    Pricing_BookableBoardId = row.meals
                };

                if (configuration.MaxUnitsImported.HasValue)
                {
                    period.AvailabilityAvailableUnits = period.AvailabilityAvailableUnits > configuration.MaxUnitsImported.Value
                                                            ? configuration.MaxUnitsImported.Value
                                                            : period.AvailabilityAvailableUnits;
                }
                if (configuration.ReleaseDaysAdded.HasValue)
                {
                    period.ReleaseDays += configuration.ReleaseDaysAdded.Value;
                }

                if (row.extrabedPriceAdult > 0)
                    period.Pricing_ExtrabedAdult = Math.Round(GetPriceWithDiscount(Agent.CalculatePrice(row.extrabedPriceAdult), pushOutDiscount, PriceType.ExtraBed), 2).ToString("f2");
                if (row.extraBeds > 0)
                    period.Pricing_ExtrabedChild = Math.Round(GetPriceWithDiscount(Agent.CalculatePrice(row.extrabedPrice), pushOutDiscount, PriceType.ExtraBed), 2).ToString("f2");

                period.AdditionalBoards = GetAdditionalBoards(row, pushOutDiscount);

                AvailabilityPeriod existingAvailabilityPeriod;
                if (room.AvailabilityPeriods.TryGetValue(period.DateFrom, out existingAvailabilityPeriod))
                {
                    if (basePrice > decimal.Parse(existingAvailabilityPeriod.Pricing_BasePrice, NumberFormatInfo.InvariantInfo))
                    {
                        room.AvailabilityPeriods.Remove(period.DateFrom);
                        room.AvailabilityPeriods.Add(period.DateFrom, period);
                    }
                }
                else
                {
                    room.AvailabilityPeriods.Add(period.DateFrom, period);
                }
            }
        }

        private enum PriceType { Base, ExtraBed, Meal };
        private static Dictionary<PriceType, Func<decimal, PushOutDiscount, decimal>> getPriceByDiscount =
            new Dictionary<PriceType, Func<decimal, PushOutDiscount, decimal>>()
            {
                {   PriceType.Base, (price, pushOutDiscount) => { return price * pushOutDiscount.DiscountMultiplier; }  },
                {   PriceType.ExtraBed, (price, pushOutDiscount) => {  return price * (pushOutDiscount.IncludeExtraBed ? pushOutDiscount.DiscountMultiplier : 1); } },
                {   PriceType.Meal, (price, pushOutDiscount)=> { return price * (pushOutDiscount.IncludeMeals ? pushOutDiscount.DiscountMultiplier : 1); }  }
            };

        private static decimal GetPriceWithDiscount(decimal price, PushOutDiscount pushOutDiscount, PriceType type)
        {
            if (pushOutDiscount == null)
                return price;

            return getPriceByDiscount[type](price, pushOutDiscount);
        }

        private static Dictionary<int, Dictionary<DateTime, PushOutDiscount>> GetPushOutDiscounts(int hotelId, DateTime endDate)
        {
            var pushOutDiscounts = new Dictionary<int, Dictionary<DateTime, PushOutDiscount>>();

            if (Agent.isPushOut)
            {
                using (var discounts = new PushOutEarlyBookingDiscountsTableAdapter().GetData(Agent.id, hotelId, DateTime.Today, endDate, RoomId: null))
                {
                    foreach (var discountRow in discounts)
                    {
                        Dictionary<DateTime, PushOutDiscount> roomDiscount;

                        if (!pushOutDiscounts.TryGetValue(discountRow.room_id, out roomDiscount))
                        {
                            roomDiscount = new Dictionary<DateTime, PushOutDiscount>();
                            pushOutDiscounts.Add(discountRow.room_id, roomDiscount);
                        }

                        var currentRowDiscount = new PushOutDiscount
                        {
                            DiscountPercentage = (decimal)discountRow.discountPercentage,
                            IncludeExtraBed = discountRow.includeExtraBed,
                            IncludeMeals = discountRow.includeMeal
                        };

                        PushOutDiscount pushOutDiscount;

                        if (!roomDiscount.TryGetValue(discountRow.date, out pushOutDiscount))
                        {
                            roomDiscount.Add(discountRow.date, currentRowDiscount);
                        }
                        else if (currentRowDiscount.IsBetter(pushOutDiscount))
                        {
                            roomDiscount[discountRow.date] = currentRowDiscount;
                        }
                    }
                }
            }

            return pushOutDiscounts;
        }

        private void GetDiscounts(Hotel h)
        {
            if (!h.Rooms.Any()) return;

            var roomIds = h.Rooms.Values.Select(_ => int.Parse(_.RoomId)).ToArray();
            IEnumerable<DiscountDataRow> discounts = null;

            Task.Run(async () => { discounts = await hotelDal.GetDiscounts(roomIds); }).Wait();

            var discountLookup = discounts.ToLookup(r => r.room_id, r => r);
            foreach (var room in h.Rooms.Values)
            {
                foreach (var discountRow in discountLookup[int.Parse(room.RoomId)])
                {
                    AvailabilityPeriod ap;
                    if (room.AvailabilityPeriods.TryGetValue(discountRow.datum, out ap))
                        ap.Discounts.TryAdd(CreateDiscount(discountRow));
                }
            }
        }


        private static Discount CreateDiscount(DiscountDataRow discount)
        {
            try
            {
                return CreateDiscountFunctions[(DiscountCreationType)discount.discount_type_id](discount);
            }
            catch
            {
                throw new NotImplementedException($"Discount type with id {discount.discount_type_id} is not implemented.");
            }
        }

        private List<AvailabilityPeriod.AdditionalBoard> GetAdditionalBoards(CalendarDataRow row, PushOutDiscount pushOutDiscount)
        {
            var boardList = new List<AvailabilityPeriod.AdditionalBoard>();

            if (row.Brf > 0) boardList.Add(new AvailabilityPeriod.AdditionalBoard(GetBoardTypes("Breakfast"), Math.Round(GetPriceWithDiscount(Agent.CalculatePrice(row.Brf), pushOutDiscount, PriceType.Meal), 2).ToString("f2"), GetPriceWithDiscount(Agent.CalculatePrice(row.ChildBrf), pushOutDiscount, PriceType.Meal).ToString(), "Breakfast"));
            if (row.HB > 0) boardList.Add(new AvailabilityPeriod.AdditionalBoard(GetBoardTypes("Half Board"), Math.Round(GetPriceWithDiscount(Agent.CalculatePrice(row.HB), pushOutDiscount, PriceType.Meal), 2).ToString("f2"), GetPriceWithDiscount(Agent.CalculatePrice(row.ChildHB), pushOutDiscount, PriceType.Meal).ToString(), "Half Board"));
            if (row.FB > 0) boardList.Add(new AvailabilityPeriod.AdditionalBoard(GetBoardTypes("Full Board"), Math.Round(GetPriceWithDiscount(Agent.CalculatePrice(row.FB), pushOutDiscount, PriceType.Meal), 2).ToString("f2"), GetPriceWithDiscount(Agent.CalculatePrice(row.ChildFB), pushOutDiscount, PriceType.Meal).ToString(), "Full Board"));
            if (row.AllBoards > 0) boardList.Add(new AvailabilityPeriod.AdditionalBoard(GetBoardTypes("All"), Math.Round(GetPriceWithDiscount(Agent.CalculatePrice(row.AllBoards), pushOutDiscount, PriceType.Meal), 2).ToString("f2"), GetPriceWithDiscount(Agent.CalculatePrice(row.ChildAllBoards), pushOutDiscount, PriceType.Meal).ToString(), "All"));

            return boardList;
        }

        private static string GetBoardTypes(string bookableName)
        {
            switch (bookableName)
            {
                case "Breakfast": return "breakfast";
                case "Half Board": return "half_board";
                case "Full Board": return "full_board";
                case "All": return "all_inclusive";
                default: return "none";
            }
        }

        /// <summary>
        /// Get countries, destinations and resorts
        /// </summary>
        private void GetPlaces(Root dataRoot)
        {
            GetCountry(dataRoot);
            GetDestinations(dataRoot);
            GetResorts(dataRoot);
        }

        /// <summary>
        /// Gets all countries from the database
        /// </summary>
        private void GetCountry(Root dataRoot)
        {
            using (var countryAdapter = new countriesTableAdapter())
            {
                using (var countryDataTable = countryAdapter.GetData(_localConfig.AgentLanguageCode))
                {
                    foreach (var cRow in countryDataTable)
                    {
                        var country = new Place();
                        country.Id = cRow.id.ToString();
                        country.Description = cRow.CountryName.Trim();
                        country.Codes.Add(new PlaceCodes { Type = "ISO-COUNTRY", Value = cRow.countrycode });
                        dataRoot.Places.Add(country);
                    }
                }
            }
        }

        /// <summary>
        /// Gets all destinations from the database
        /// </summary>
        private void GetDestinations(Root dataRoot)
        {
            var destinationsLookup = dataRoot.Places.ToDictionary(t => t.Id, t => t);
            foreach (var countryList in MultipleQueryLists(dataRoot.Places.Select(c => c.Id)))
            {
                using (var destinationAdapter = new DestinationsTableAdapter())
                {
                    destinationAdapter.SetCommandTimeout(0, GeneralTimeout);
                    var command = destinationAdapter.GetCommandText(0).Replace("'replaceToken' = 'replaceToken16'", "countries.id IN (" + countryList + ")");
                    destinationAdapter.SetCommandText(0, command);

                    using (var destinationDataTable = destinationAdapter.GetData(_localConfig.AgentLanguageCode))
                    {
                        foreach (var dRow in destinationDataTable)
                        {
                            Place countryPlace;
                            if (destinationsLookup.TryGetValue(dRow.countryId.ToString(), out countryPlace))
                            {
                                var dest = new Place();
                                dest.Id = dRow.ID.ToString();
                                dest.Description = dRow.DestinationName.Trim();
                                var destCode = !string.IsNullOrEmpty(dRow.destination_code) ? dRow.destination_code.Trim() : string.Empty;
                                var destCode2 = !string.IsNullOrEmpty(dRow.destination_code2) ? dRow.destination_code2.Trim() : string.Empty;
                                var destCode3 = !string.IsNullOrEmpty(dRow.destination_code3) ? dRow.destination_code3.Trim() : string.Empty;
                                var destCode4 = !string.IsNullOrEmpty(dRow.destination_code4) ? dRow.destination_code4.Trim() : string.Empty;

                                if (!string.IsNullOrEmpty(destCode))
                                {
                                    dest.Codes.Add(new PlaceCodes { Type = "IATA", Value = destCode });
                                }
                                if (!string.IsNullOrEmpty(destCode2))
                                {
                                    dest.Codes.Add(new PlaceCodes { Type = "IATA", Value = destCode2 });
                                }
                                if (!string.IsNullOrEmpty(destCode3))
                                {
                                    dest.Codes.Add(new PlaceCodes { Type = "IATA", Value = destCode3 });
                                }
                                if (!string.IsNullOrEmpty(destCode4))
                                {
                                    dest.Codes.Add(new PlaceCodes { Type = "IATA", Value = destCode4 });
                                }

                                countryPlace.Places.Add(dest);
                            }
                        }
                    }

                }

            }
        }

        /// <summary>
        /// Gets all resorts from the database
        /// </summary>
        private void GetResorts(Root dataRoot)
        {
            var destinations = dataRoot.Places.SelectMany(c => c.Places).ToDictionary(t => t.Id, t => t);

            foreach (var destinationList in MultipleQueryLists(destinations.Keys.Select(key => key)))
            {
                using (var resortAdapter = new ResortsTableAdapter())
                {
                    resortAdapter.SetCommandTimeout(0, GeneralTimeout);
                    var command = resortAdapter.GetCommandText(0).Replace("'replaceToken' = 'replaceToken17'", "Resorts.destId IN (" + destinationList + ")");
                    resortAdapter.SetCommandText(0, command);

                    using (var resortDataTable = resortAdapter.GetData(_localConfig.AgentLanguageCode))
                    {
                        foreach (var resRow in resortDataTable)
                        {
                            Place destinationPlace;
                            if (destinations.TryGetValue(resRow.destId.ToString(), out destinationPlace))
                            {
                                var reso = new Place();
                                reso.Id = resRow.ID.ToString();
                                reso.Description = resRow.ResortName.Trim();
                                //Copy object "Codes" from the destination in order to send the list of airports available
                                reso.Codes.AddRange(destinationPlace.Codes);
                                destinationPlace.Places.Add(reso);
                            }
                        }
                    }
                }


            }
        }

        /// <summary>
        /// Splits the source list (individual elements) into several string lists (joined), each list with maximum QUERY_ELEMENTS, joined by ','
        /// </summary>
        /// <param name="sourceList"></param>
        /// <returns></returns>
        public static List<string> MultipleQueryLists(IEnumerable<string> sourceList)
        {
            var tmpLists = new Dictionary<int, List<string>>();
            if (sourceList != null)
            {
                foreach (var element in sourceList)
                {
                    if (tmpLists.Count == 0)
                    {
                        tmpLists.Add(0, new List<string>());
                    }
                    //Limit queries by a certain number of elements
                    if (tmpLists[tmpLists.Count - 1].Count > QueryElements)
                    {
                        tmpLists.Add(tmpLists.Count, new List<string>());
                    }
                    tmpLists[tmpLists.Count - 1].Add(element);
                }
            }
            var retLists = new List<string>();
            foreach (var tmp in tmpLists.Values)
            {
                retLists.Add(String.Join(",", tmp));
            }
            return retLists;
        }

        public static Dictionary<int, string> GiataHotelCodes()
        {
            var giataCodes = new Dictionary<int, string>();
            using (var connection = new SqlConnection(Properties.Settings.Default.sunhotelsConnectionString))
            {
                using (var command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandTimeout = GeneralTimeout;
                    command.CommandText = "SELECT hotelId, value FROM dbo.hotelCode WHERE hotelCodeTypeId = 2"; //Type 2 = GIATA
                    using (var dataTable = new DataTable())
                    {
                        using (var sqlDataAdapter = new SqlDataAdapter())
                        {
                            sqlDataAdapter.SelectCommand = command;
                            sqlDataAdapter.Fill(dataTable);
                        }

                        if (dataTable.Rows.Count > 0)
                        {
                            for (var i = 0; i < dataTable.Rows.Count; i++)
                            {
                                var row = dataTable.Rows[i];
                                var hotelId = (int)row["hotelId"];
                                var value = (string)row["value"];

                                if (!giataCodes.ContainsKey(hotelId))
                                {
                                    giataCodes.Add(hotelId, value);
                                }
                            }
                        }
                    }
                }
            }
            return giataCodes;
        }

        public static HashSet<int> HotelsSuspended()
        {
            var hotelsSuspended = new HashSet<int>();
            using (var connection = new SqlConnection(Properties.Settings.Default.sunhotelsConnectionString))
            {
                using (var command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandTimeout = GeneralTimeout;
                    command.CommandText = "SELECT id FROM dbo.vHotelStatus WHERE Suspended > 0";
                    using (var dataTable = new DataTable())
                    {
                        using (var sqlDataAdapter = new SqlDataAdapter())
                        {
                            sqlDataAdapter.SelectCommand = command;
                            sqlDataAdapter.Fill(dataTable);
                        }

                        if (dataTable.Rows.Count > 0)
                        {
                            for (var i = 0; i < dataTable.Rows.Count; i++)
                            {
                                var row = dataTable.Rows[i];
                                var hotelId = (int)row["id"];

                                if (!hotelsSuspended.Contains(hotelId))
                                {
                                    hotelsSuspended.Add(hotelId);
                                }
                            }
                        }
                    }
                }
            }
            return hotelsSuspended;
        }

        private List<int> RoomProviderMarketBlocks(int agentCountryId)
        {
            var roomProviderMarketBlocks = new List<int>();
            using (var connection = new SqlConnection(Properties.Settings.Default.sunhotelsConnectionString))
            {
                using (var command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandTimeout = GeneralTimeout;
                    command.CommandText = "EXEC spWSDL_GetRoomProviderMarketBlocks";
                    connection.Open();
                    using (var rdr = command.ExecuteReader(CommandBehavior.SequentialAccess))
                    {
                        while (rdr.Read())
                        {
                            var row = rdr;
                            var roomProviderId = (int)row["roomProviderId"];
                            var countryId = (int)row["countryId"];

                            if (agentCountryId <= 0 || agentCountryId == countryId)
                            {
                                if (!roomProviderMarketBlocks.Contains(roomProviderId))
                                    roomProviderMarketBlocks.Add(roomProviderId);
                            }
                        }
                    }
                }
            }
            return roomProviderMarketBlocks;
        }

        private List<int> AgentRoomProviderBlocks(int agentId)
        {
            var agentRoomProviderBlocks = new List<int>();
            using (var connection = new SqlConnection(Properties.Settings.Default.sunhotelsConnectionString))
            {
                using (var command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandTimeout = GeneralTimeout;
                    command.CommandText = "SELECT roomproviderid FROM agentroomprovider_block WITH(NOLOCK) WHERE agentid = " + agentId.ToString();
                    connection.Open();
                    using (var rdr = command.ExecuteReader(CommandBehavior.SequentialAccess))
                    {
                        while (rdr.Read())
                        {
                            var row = rdr;
                            var roomProviderId = (int)row["roomproviderid"];
                            if (!agentRoomProviderBlocks.Contains(roomProviderId))
                            {
                                agentRoomProviderBlocks.Add(roomProviderId);
                            }
                        }
                    }
                }
            }
            return agentRoomProviderBlocks;
        }

        private List<int> B2CRoomProviderBlocks()
        {
            var b2CRoomProviderBlocks = new List<int>();
            using (var connection = new SqlConnection(Properties.Settings.Default.sunhotelsConnectionString))
            {
                using (var command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandTimeout = GeneralTimeout;
                    command.CommandText = "SELECT id FROM roomprovider WITH(NOLOCK) where blockB2CAgents = 1";
                    connection.Open();
                    using (var rdr = command.ExecuteReader(CommandBehavior.SequentialAccess))
                    {
                        while (rdr.Read())
                        {
                            var row = rdr;
                            var roomProviderId = (int)row["id"];
                            if (!b2CRoomProviderBlocks.Contains(roomProviderId))
                            {
                                b2CRoomProviderBlocks.Add(roomProviderId);
                            }
                        }
                    }
                }
            }
            return b2CRoomProviderBlocks;
        }

        private Dictionary<string, List<string>> GetHotelRooms(IEnumerable<Hotel> allHotels, Dictionary<int, int> roomProviderBlocks)
        {
            var hotelRooms = new Dictionary<string, List<string>>();
            var query = GenerateHotelRoomsQuery(allHotels, roomProviderBlocks);

            var sw = new System.Diagnostics.Stopwatch();
            sw.Start();

            using (var connection = new SqlConnection(Properties.Settings.Default.sunhotelsConnectionString))
            {
                using (var command = new SqlCommand())
                {
                    connection.Open();
                    command.Connection = connection;
                    command.CommandTimeout = GeneralTimeout;
                    command.CommandText = query;

                    WriteToConsole("GetHotelRooms execute query ", sw);
                    using (var rdr = command.ExecuteReader(CommandBehavior.SequentialAccess))
                    {
                        WriteToConsole("GetHotelRooms execute query finished ", sw);
                        WriteToConsole("Start processing HotelRooms ", sw);
                        while (rdr.Read())
                        {
                            var row = rdr;
                            var roomId = row["ID"].ToString();
                            var hotelId = row["hotelId"].ToString();

                            List<string> roomList;
                            if (!hotelRooms.TryGetValue(hotelId, out roomList))
                            {
                                roomList = new List<string>();
                                hotelRooms.Add(hotelId, roomList);
                            }

                            if (!roomList.Contains(roomId))
                            {
                                roomList.Add(roomId);
                            }
                        }
                        WriteToConsole("Finished processing HotelRooms ", sw);
                    }

                    if (connection.State == ConnectionState.Open)
                        connection.Close();
                }
            }

            return hotelRooms;
        }

        private string GenerateHotelRoomsQuery(IEnumerable<Hotel> allHotels, Dictionary<int, int> roomProviderBlocks)
        {
            var query = new StringBuilder();

            query.AppendLine(" DECLARE @MinDate AS DATE = (GETDATE() - 1); ");
            query.AppendLine(" CREATE TABLE #TempHotels (hotelId Int);  ");
            query.AppendLine(" CREATE UNIQUE CLUSTERED INDEX #IX_TempHotels on #TempHotels (hotelId); ");
            query.AppendLine(" ");
            query.AppendLine(" CREATE TABLE #TempRoomType (roomtypes_id Int); ");
            query.AppendLine(" CREATE UNIQUE CLUSTERED INDEX #IX_TempRoomType on #TempRoomType (roomtypes_id) ");
            query.AppendLine(" ");
            query.AppendLine(" CREATE TABLE #TempBlockedProviders(roomprovider_id Int); ");
            query.AppendLine(" CREATE UNIQUE CLUSTERED INDEX #IX_TempBlockedProviders on #TempBlockedProviders (roomprovider_id) ");
            query.AppendLine(" ");
            query.AppendLine(" CREATE TABLE #TempHotelRooms(ID Int, hotelId INT); ");
            query.AppendLine(" CREATE UNIQUE CLUSTERED INDEX #IX_TempHotelRooms on #TempHotelRooms (ID, hotelId) ");
            query.AppendLine(" ");

            foreach (var hotelList in MultipleQueryLists(allHotels.Select(h => h.Id)))
            {
                query.AppendLine(" INSERT INTO #TempHotels(HotelId)  ");
                query.AppendLine($" VALUES ({string.Join("),(", hotelList.Split(','))}) ");
            }
            query.AppendLine(" ");
            foreach (var roomProviderBlock in MultipleQueryLists(roomProviderBlocks.Values.Distinct().Select(t => t.ToString())))
            {
                query.AppendLine(" INSERT INTO #TempBlockedProviders(roomprovider_id)  ");
                query.AppendLine($" VALUES ({string.Join("),(", roomProviderBlock.Split(','))}) ");
            }
            query.AppendLine(" ");
            foreach (var roomTypeIds in MultipleQueryLists(_roomTypesList.Distinct()))
            {
                query.AppendLine(" INSERT INTO #TempRoomType(roomtypes_id)  ");
                query.AppendLine($" VALUES ({string.Join("),(", roomTypeIds.Split(','))}) ");
            }
            query.AppendLine(" ");
            query.Append(" INSERT INTO #TempHotelRooms ");
            query.AppendLine("SELECT r.ID, r.hotelId  FROM #TempHotels h ");
            query.AppendLine("INNER JOIN Rooms r WITH(NOLOCK) ON r.hotelId = h.hotelId ");
            query.AppendLine("  AND r.isLiveRoom = 0 ");
            query.AppendLine("  AND ISNULL(r.SuspendRoom, 0) = 0 ");
            query.AppendLine("  AND ISNULL(r.deleted, 0) = 0 ");
            query.AppendLine("  AND ISNULL(r.roomprovider_id, 0) > 0 ");
            if (_localConfig.RemoveNRF)
                query.AppendLine(" AND ISNULL(r.norefund, 0) = 0 ");

            query.AppendLine(@" INNER JOIN roomtypes rt WITH (NOLOCK) ON rt.id = r.roomtypes_id ");
            if (_localConfig.ExcludeSharedRooms)
                query.AppendLine(" AND ISNULL(rt.sharedRoom, 0) = 0 ");
            if (_localConfig.ExcludeSharedFacilities)
                query.AppendLine(" AND ISNULL(rt.SharedFacilities, 0) = 0 ");
            query.AppendLine("INNER JOIN #TempRoomType TRT ON TRT.roomtypes_id = rt.id ");

            query.AppendLine(" WHERE NOT EXISTS(SELECT TOP 1 PB.roomprovider_id FROM #TempBlockedProviders PB WHERE PB.roomprovider_id = r.roomprovider_id)");

            query.AppendLine(" ");
            query.AppendLine("  SELECT DISTINCT TR.ID, TR.hotelId FROM #TempHotelRooms TR ");
            query.AppendLine(@" WHERE EXISTS(
                                                SELECT TOP 1 c.datum
                                                FROM calendar c WITH(NOLOCK)
                                                WHERE
                                                    TR.ID = c.roomId
                                                    AND c.datum > @MinDate
				                                    AND (c.units - c.bookedUnits) > 0 ) ");
            query.AppendLine(" ");
            query.AppendLine(" DROP TABLE #TempHotelRooms");
            query.AppendLine(" ");
            query.AppendLine(" DROP TABLE #TempRoomType");
            query.AppendLine(" ");
            query.AppendLine(" DROP TABLE #TempHotels");
            query.AppendLine(" ");
            query.AppendLine(" DROP TABLE #TempBlockedProviders ");

            return query.ToString();
        }

        private Dictionary<string, List<string>> HotelsFilteredByRoomProvider(IEnumerable<Hotel> allHotels)
        {
            var agentRoomProviderBlocks = AgentRoomProviderBlocks(Agent.id);
            var roomProviderMarketBlocks = RoomProviderMarketBlocks(Agent.countryId);
            var b2CRoomProviderBlocks = Agent.B2C
                    ? B2CRoomProviderBlocks()
                    : new List<int>();
            var roomProviderBlocks = agentRoomProviderBlocks
                    .Concat(roomProviderMarketBlocks)
                    .Concat(b2CRoomProviderBlocks)
                    .Distinct()
                    .ToDictionary(t => t, t => t);
            return GetHotelRooms(allHotels, roomProviderBlocks);
        }

        private void RetrieveAvailabilityPattern()
        {
            if (IsDeltaFile)
            {
                string path = _availabilityPatternOutput;
                if (File.Exists(path))
                {
                    using (StreamReader sr = new StreamReader(path))
                    {
                        while (sr.Peek() >= 0)
                        {
                            var line = sr.ReadLine().Split(csvDelimiter);
                            var hotelRoomKey = line[0];
                            var pattern = line[1];
                            if (!_availabilityPattern.ContainsKey(hotelRoomKey))
                            {
                                _availabilityPattern.TryAdd(hotelRoomKey, pattern);
                            }
                        }
                    }
                }
            }
        }

        private void CreateAvailabilityPatternFile(string location)
        {
            var folder = Path.GetDirectoryName(location);
            if (!Directory.Exists(folder))
                Directory.CreateDirectory(folder);

            if (!File.Exists(location))
            {
                File.Create(location).Close();
            }

            using (TextWriter writer = File.CreateText(location))
            {
                foreach (var index in _availabilityPattern)
                {
                    writer.WriteLine(string.Join(csvDelimiter.ToString(), index.Key, index.Value));

                }
            }
        }

    }

    public static class ExtensionMethods
    {
        public static decimal CalculatePrice(this PeakworkEDFDataSet.agentRow agentRow, decimal originalPrice)
        {
            return originalPrice * (Convert.ToDecimal(agentRow.primaryCurrFactor) / 100);
        }

        public static T Field<T>(this SqlDataReader sqlDataReader, string columnName, T defaultValue = default(T))
        {
            var value = sqlDataReader[columnName];
            // Check for DBNull
            if (DBNull.Value == value)
            {
                return defaultValue;
            }

            // Convert to T (nullable types supported)
            Type t = typeof(T);
            Type u = Nullable.GetUnderlyingType(t);

            if (u != null)
            {
                return (value == null) ? default(T) : (T)Convert.ChangeType(value, u);
            }
            else
            {
                return (T)Convert.ChangeType(value, t);
            }

        }
    }
}
