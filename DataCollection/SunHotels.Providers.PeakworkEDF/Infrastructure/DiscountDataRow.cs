﻿using System;

namespace SunHotels.Providers.Infrastructure
{
    public class DiscountDataRow
    {
        public int room_id { get; set; }
        public DateTime datum { get; set; }
        public int discount_type_id { get; set; }
        public int discount_min_days { get; set; }
        public int discount_max_days { get; set; }
        public float discount_value { get; set; }
        public int discount_period_id { get; set; }
        public int accumulativeGroup { get; set; }
        public bool checkinBased { get; set; }
        public bool includeMeal { get; set; }
        public bool includeExtraBed { get; set; }
    }
}
