﻿using System;

namespace SunHotels.Providers.Infrastructure
{
    public class CalendarDataRow
    {
        public int roomtypes_id { get; set; }
        public int beds { get; set; }
        public int extraBeds { get; set; }
        public decimal price { get; set; }
        public decimal extrabedPrice { get; set; }
        public decimal extrabedPriceAdult { get; set; }
        public string meals { get; set; }
        public decimal Brf { get; set; }
        public decimal ChildBrf { get; set; }
        public decimal HB { get; set; }
        public decimal ChildHB { get; set; }
        public decimal FB { get; set; }
        public decimal ChildFB { get; set; }
        public decimal AllBoards { get; set; }
        public decimal ChildAllBoards { get; set; }
        public int units { get; set; }
        public int MinDays { get; set; }
        public int releaseTime { get; set; }
        public DateTime datum { get; set; }
        public bool IsRegular { get; set; }
        public bool IsPayAtHotel { get; set; }
        public int Id { get; set; }
        public bool IncludeEarlyBookingInPrice { get; set; }

    }
}
