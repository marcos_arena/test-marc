﻿namespace SunHotels.Providers.Infrastructure
{
    public class HotelDataRow
    {
        public int id { get; set; }
        public string hotelName { get; set; }
        public int resort { get; set; }
        public bool ShowinSV { get; set; }
        public bool ShowinNO { get; set; }
        public bool ShowinEN { get; set; }
        public bool ShowinES { get; set; }
        public bool ShowinGE { get; set; }
        public bool ShowinDK { get; set; }
        public bool ShowinFR { get; set; }
        public bool ShowinFI { get; set; }
        public bool ShowinPL { get; set; }
        public bool ShowinRU { get; set; }
        public bool ShowinIT { get; set; }
        public bool ShowinZH { get; set; }
        public bool ShowinSVxml { get; set; }
        public bool ShowinNOxml { get; set; }
        public bool ShowinENxml { get; set; }
        public bool ShowinESxml { get; set; }
        public bool ShowinGExml { get; set; }
        public bool ShowinDKxml { get; set; }
        public bool ShowinFRxml { get; set; }
        public bool ShowinFIxml { get; set; }
        public bool ShowinPLxml { get; set; }
        public bool ShowinRUxml { get; set; }
        public bool ShowinITxml { get; set; }
        public bool ShowinZHxml { get; set; }

    }
}
