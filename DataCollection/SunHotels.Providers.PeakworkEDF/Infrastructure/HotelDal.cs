﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace SunHotels.Providers.Infrastructure
{
    public class HotelDal
    {

        private string ConnectionString;
        private int TimeOut;
        public HotelDal(string ConnectionString, int timeOut)
        {
            this.ConnectionString = ConnectionString;
            TimeOut = timeOut;
        }

        public async Task<IEnumerable<HotelDataRow>> GetExportHotelAsync(string iataNumbers, int agentId, bool isAgentPush)
        {
            var result = new List<HotelDataRow>();
            using (var conn = new SqlConnection(ConnectionString))
            {
                await conn.OpenAsync();
                SqlCommand cmd = new SqlCommand("[dbo].[spExportHotels]", conn);
                cmd.CommandTimeout = TimeOut;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter() { ParameterName = "IATANumbers", Value = iataNumbers, DbType = System.Data.DbType.String });
                cmd.Parameters.Add(new SqlParameter() { ParameterName = "agentId", Value = agentId, DbType = System.Data.DbType.Int32 });
                cmd.Parameters.Add(new SqlParameter() { ParameterName = "isAgentPush", Value = isAgentPush, DbType = System.Data.DbType.Boolean });

                using (var rdr = await cmd.ExecuteReaderAsync())
                {
                    while (await rdr.ReadAsync())
                    {
                        result.Add(ToHotelDataRow(rdr));
                    }
                }

                conn.Close();
                return result;
            }
        }

        private static HotelDataRow ToHotelDataRow(SqlDataReader rdr)
        {
            return new HotelDataRow()
            {
                id = Convert.ToInt32(rdr["id"]),
                hotelName = rdr["hotelName"].ToString(),
                resort = Convert.ToInt32(rdr["resort"]),
                ShowinSV = Convert.ToBoolean(rdr["ShowinSV"]),
                ShowinNO = Convert.ToBoolean(rdr["ShowinNO"]),
                ShowinEN = Convert.ToBoolean(rdr["ShowinEN"]),
                ShowinES = Convert.ToBoolean(rdr["ShowinSV"]),
                ShowinGE = Convert.ToBoolean(rdr["ShowinSV"]),
                ShowinDK = Convert.ToBoolean(rdr["ShowinSV"]),
                ShowinFR = Convert.ToBoolean(rdr["ShowinSV"]),
                ShowinFI = Convert.ToBoolean(rdr["ShowinSV"]),
                ShowinPL = Convert.ToBoolean(rdr["ShowinSV"]),
                ShowinRU = Convert.ToBoolean(rdr["ShowinSV"]),
                ShowinIT = Convert.ToBoolean(rdr["ShowinSV"]),
                ShowinZH = Convert.ToBoolean(rdr["ShowinSV"]),
                ShowinSVxml = Convert.ToBoolean(rdr["ShowinSV"]),
                ShowinNOxml = Convert.ToBoolean(rdr["ShowinSV"]),
                ShowinENxml = Convert.ToBoolean(rdr["ShowinSV"]),
                ShowinESxml = Convert.ToBoolean(rdr["ShowinSV"]),
                ShowinGExml = Convert.ToBoolean(rdr["ShowinSV"]),
                ShowinDKxml = Convert.ToBoolean(rdr["ShowinSV"]),
                ShowinFRxml = Convert.ToBoolean(rdr["ShowinSV"]),
                ShowinFIxml = Convert.ToBoolean(rdr["ShowinSV"]),
                ShowinPLxml = Convert.ToBoolean(rdr["ShowinSV"]),
                ShowinRUxml = Convert.ToBoolean(rdr["ShowinSV"]),
                ShowinITxml = Convert.ToBoolean(rdr["ShowinSV"]),
                ShowinZHxml = Convert.ToBoolean(rdr["ShowinSV"]),
            };
        }

        public async Task<IEnumerable<CalendarDataRow>> GetAvailabilityAsync(int hotelId, int agentId, bool hideXml, bool hideManual, bool showXmlOnlyIfMissingManual, bool isPushOut)
        {
            var result = new List<CalendarDataRow>();
            using (var conn = new SqlConnection(ConnectionString))
            {
                await conn.OpenAsync();
                SqlCommand cmd = new SqlCommand("[dbo].[spExportAvailability]", conn);
                cmd.CommandTimeout = TimeOut;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter() { ParameterName = "HotelId", Value = hotelId, DbType = System.Data.DbType.Int32 });
                cmd.Parameters.Add(new SqlParameter() { ParameterName = "AgentId", Value = agentId, DbType = System.Data.DbType.Int32 });
                cmd.Parameters.Add(new SqlParameter() { ParameterName = "hideXML", Value = hideXml, DbType = System.Data.DbType.Boolean });
                cmd.Parameters.Add(new SqlParameter() { ParameterName = "hideManual", Value = hideManual, DbType = System.Data.DbType.Boolean });
                cmd.Parameters.Add(new SqlParameter() { ParameterName = "showXmlOnlyIfMissingManual", Value = showXmlOnlyIfMissingManual, DbType = System.Data.DbType.Boolean });
                cmd.Parameters.Add(new SqlParameter() { ParameterName = "isPushOut", Value = isPushOut, DbType = System.Data.DbType.Boolean });

                using (var rdr = await cmd.ExecuteReaderAsync())
                {
                    while (await rdr.ReadAsync())
                        result.Add(ToRoomCalendarDataRow(rdr));
                }

                conn.Close();
                return result;
            }
        }

        private CalendarDataRow ToRoomCalendarDataRow(SqlDataReader rdr)
        {            
            return new CalendarDataRow()
            {
                roomtypes_id = Convert.ToInt32(rdr["roomtypes_id"]),
                beds = Convert.ToInt32(rdr["beds"]),
                extraBeds = Convert.ToInt32(rdr["extraBeds"]),
                price = Convert.ToDecimal(rdr["price"]),
                extrabedPrice = Convert.ToDecimal(rdr["extrabedPrice"]),
                extrabedPriceAdult = Convert.ToDecimal(rdr["extrabedPriceAdult"]),
                meals = rdr["meals"].ToString(),
                Brf = Convert.ToDecimal(rdr["Brf"]),
                ChildBrf = Convert.ToDecimal(rdr["ChildBrf"]),
                HB = Convert.ToDecimal(rdr["HB"]),
                ChildHB = Convert.ToDecimal(rdr["ChildHB"]),
                FB = Convert.ToDecimal(rdr["FB"]),
                ChildFB = Convert.ToDecimal(rdr["ChildFB"]),
                AllBoards = Convert.ToDecimal(rdr["AllBoards"]),
                ChildAllBoards = Convert.ToDecimal(rdr["ChildAllBoards"]),
                units = Convert.ToInt32(rdr["units"]),
                MinDays = Convert.ToInt32(rdr["MinDays"]),
                releaseTime = Convert.ToInt32(rdr["releaseTime"]),
                datum = Convert.ToDateTime(rdr["datum"]),
                IsRegular = Convert.ToBoolean(rdr["IsRegular"]),
                IsPayAtHotel = Convert.ToBoolean(rdr["IsPayAtHotel"]),
                Id = Convert.ToInt32(rdr["Id"]),
                IncludeEarlyBookingInPrice = Convert.ToBoolean(rdr["IncludeEarlyBookingDiscountInPrice"])
            };
        }

        public async Task<IEnumerable<DiscountDataRow>> GetDiscounts(int[] roomIds)
        {
            var result = new List<DiscountDataRow>();
            using (var conn = new SqlConnection(ConnectionString))
            {
                await conn.OpenAsync();
                SqlCommand cmd = new SqlCommand("[dbo].[ExportDiscounts]", conn);
                cmd.CommandTimeout = TimeOut;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter() { ParameterName = "roomTable", Value = CreateDataTable(roomIds) });

                using (var rdr = await cmd.ExecuteReaderAsync())
                {
                    while (await rdr.ReadAsync())
                        result.Add(ToDiscountRow(rdr));
                }

                conn.Close();
                return result;
            }
        }

        private DiscountDataRow ToDiscountRow(SqlDataReader rdr)
        {
            return new DiscountDataRow()
            {
                accumulativeGroup = Convert.ToInt32( rdr["accumulativeGroup"]),
                checkinBased = Convert.ToBoolean(rdr["checkinBased"]),
                datum = Convert.ToDateTime(rdr["datum"]),
                discount_max_days = Convert.ToInt32(rdr["discount_max_days"]),
                discount_min_days = Convert.ToInt32(rdr["discount_min_days"]),
                discount_period_id = Convert.ToInt32(rdr["discount_period_id"]),
                discount_type_id = Convert.ToInt32(rdr["discount_type_id"]),
                discount_value = Convert.ToSingle(rdr["discount_value"]),
                includeExtraBed = Convert.ToBoolean(rdr["includeExtraBed"]),
                includeMeal = Convert.ToBoolean(rdr["includeMeal"]),
                room_id = Convert.ToInt32(rdr["room_id"]),
            };
        }

        private static DataTable CreateDataTable(IEnumerable<int> ids)
        {
            DataTable table = new DataTable();
            table.Columns.Add("ID", typeof(long));
            foreach (long id in ids)
            {
                table.Rows.Add(id);
            }
            return table;
        }
    }
}