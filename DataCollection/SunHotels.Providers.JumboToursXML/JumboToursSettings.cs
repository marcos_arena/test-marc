using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace SunHotels.Providers
{
	[XmlRoot("JumboToursSettings")]
	public class JumboToursSettings
    {
		public Supplement Supplement;
        public string[] NonRefundableText { get; set; }
    }

	public class Supplement
	{		
		public decimal MaxPercent;
	}
}
