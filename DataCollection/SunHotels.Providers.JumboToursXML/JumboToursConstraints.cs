using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using SunHotels.XML.Data;
using System.Globalization;
using System.Linq;
using System.Xml.Linq;

namespace SunHotels.Providers
{
    public class Constraint
    {
        protected static IFormatProvider culture = new CultureInfo("en-GB", true);
        DateTime formalizationFrom;
        public DateTime FormalizationFrom
        {
            get { return formalizationFrom; }
        }

        DateTime formalizationTo;
        public DateTime FormalizationTo
        {
            get { return formalizationTo; }
        }

        protected DateTime stayFrom;
        public DateTime StayFrom
        {
            get { return stayFrom; }
        }

        protected DateTime stayTo;
        public DateTime StayTo
        {
            get { return stayTo; }
        }

        string weekdays;
        public string Weekdays
        {
            get { return weekdays; }
        }

        protected string[] roomTypes;
        public IEnumerable<string> RoomTypes
        {
            get { return roomTypes; }
        }

        protected string[] boardTypes;
        public IEnumerable<string> BoardTypes
        {
            get { return boardTypes; }
        }

        public Constraint(XElement constraintNode, JumboToursContract contract)
        {
            JumboToursXML.VerifyAttributeExists(
                constraintNode,
                "formalizationFrom",
                String.Format("contract id {0}", constraintNode.Ancestors("contract").First().Attribute("id").Value));

            this.formalizationFrom = DateTime.Parse(constraintNode.Attribute("formalizationFrom").Value, culture);

            JumboToursXML.VerifyAttributeExists(
                constraintNode,
                "formalizationTo",
                String.Format("contract id {0}", constraintNode.Ancestors("contract").First().Attribute("id").Value));

            this.formalizationTo = DateTime.Parse(constraintNode.Attribute("formalizationTo").Value, culture);

            JumboToursXML.VerifyAttributeExists(
                constraintNode,
                "weekdays",
                String.Format("contract id {0}", constraintNode.Ancestors("contract").First().Attribute("id").Value));

            this.weekdays = constraintNode.Attribute("weekdays").Value;

			if (constraintNode.Attribute("roomtypes") != null 
				&& !string.IsNullOrEmpty(constraintNode.Attribute("roomtypes").Value) )
			{
				roomTypes = constraintNode.Attribute("roomtypes").Value.Split(new char[] { ',' });
			}
			else {
				roomTypes = contract.RoomTypes.Keys.ToArray();
			}

			if (constraintNode.Attribute("boardtypes") != null &&
				!string.IsNullOrEmpty(constraintNode.Attribute("boardtypes").Value))
			{
				boardTypes = constraintNode.Attribute("boardtypes").Value.Split(new char[] { ',' });
			}
			else {
				boardTypes = contract.Boardtypes.Split(new char[] { ',' });
			}
        }

        //Checks if a date is within a formalization period of the constraint
        public bool DateWithinFormalization(DateTime date)
        {
            if (date.CompareTo(formalizationFrom) >= 0 && date.CompareTo(formalizationTo) <= 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public char MatchWeekDay(DayOfWeek day)
        {
            int weekday = 0;
            switch (day)
            {
                case DayOfWeek.Sunday: weekday = 0; break;
                case DayOfWeek.Monday: weekday = 1; break;
                case DayOfWeek.Tuesday: weekday = 2; break;
                case DayOfWeek.Wednesday: weekday = 3; break;
                case DayOfWeek.Thursday: weekday = 4; break;
                case DayOfWeek.Friday: weekday = 5; break;
                case DayOfWeek.Saturday: weekday = 6; break;
            }

            return this.weekdays[weekday];
        }

        /// <summary>
        /// Checks if a date is within a staying period of the constraint 
        /// </summary>
        /// <param name="date">The datetime to check against</param>
        /// <returns>True or false</returns>
        public bool Match(DateTime date, string roomType)
        {
            if (date.CompareTo(stayFrom) >= 0 && date.CompareTo(stayTo) <= 0 && (roomTypes == null || roomTypes.Any( rt => rt == roomType)))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }

    public class SalesClosed : Constraint
    {
        public SalesClosed(XElement salesClosedNode, JumboToursContract contract)
			: base(salesClosedNode, contract)
        {
            this.stayFrom = DateTime.Parse(salesClosedNode.Attribute("stayFrom").Value, culture);
            this.stayTo = DateTime.Parse(salesClosedNode.Attribute("stayTo").Value, culture);
			//var roomType = (string)salesClosedNode.Attribute("roomtypes") ?? "";
			//this.roomTypes = new string[] { roomType };
			//var boardType = (string)salesClosedNode.Attribute("boardtypes") ?? "";
			//this.boardTypes = new string[] { boardType };			
        }


		internal IEnumerable<salesClosedFilter> UnGroup()
		{
			//(string.IsNullOrEmpty(sc.Weekdays) || sc.Weekdays[(int)date.DayOfWeek] == 'X')
			for (var s = this.stayFrom; s <= this.stayTo; s = s.AddDays(1))
			{
				if (string.IsNullOrEmpty(this.Weekdays) || this.Weekdays[(int)s.DayOfWeek] == 'X')
				{
					foreach (var b in this.boardTypes)
						foreach (var r in this.roomTypes)
						{
							yield return new salesClosedFilter() { start = s, end = s, boards = new string[] { b }, roomtypes = new string[] { r } };
						}
				}						
			}
		}
	}

	public class salesClosedFilter 
	{
		
		
		public DateTime start;				
		public DateTime end;
		public IEnumerable<string> boards;

		public IEnumerable<string> roomtypes;	
		
	}

    public class MinStay : Constraint
    {
        string result;
        public string Result
        {
            get { return result; }
            set { result = value; }
        }

        Int16 nights;
        public Int16 Nights
        {
            get { return nights; }
            set { nights = value; }
        }

        private int percent;
        public int Percent
        {
            get { return percent; }
            set { percent = value; }
        }

        public MinStay(XElement minStayNode, JumboToursContract contract)
			: base(minStayNode, contract)
        {
            this.stayFrom = DateTime.Parse(minStayNode.Attribute("stayFrom").Value, culture);
            this.stayTo = DateTime.Parse(minStayNode.Attribute("stayTo").Value, culture);
            this.result = minStayNode.Attribute("result").Value;
            this.nights = Int16.Parse(minStayNode.Attribute("nights").Value);
            if (this.nights > 100)
            {
                throw new MinstayMaxLimitExceededException();
            }

            if (this.result == "SUPPLEMENT")
            {
                decimal dPercent = JumboToursXML.DecimalParse(minStayNode.Attribute("percent").Value, culture);
                this.percent = (int)dPercent;
            }
        }
    }

    public class CheckinDays : Constraint
    {
        string monthdays;

        public CheckinDays(XElement checkinDaysNode, JumboToursContract contract)
            : base(checkinDaysNode, contract)
        {
            this.stayFrom = DateTime.Parse(checkinDaysNode.Attribute("stayFrom").Value, culture);
            this.stayTo = DateTime.Parse(checkinDaysNode.Attribute("stayTo").Value, culture);
            this.monthdays = checkinDaysNode.Attribute("monthdays").Value;
        }
    }

    public class CheckInRelease : Constraint
    {
        Int16 release;

        public Int16 Release
        {
            get { return release; }
            set { release = value; }
        }

		public CheckInRelease(XElement checkInReleaseNode, JumboToursContract contract)
            : base(checkInReleaseNode, contract)
        {
            this.stayFrom = DateTime.Parse(checkInReleaseNode.Attribute("stayFrom").Value, culture);
            this.stayTo = DateTime.Parse(checkInReleaseNode.Attribute("stayTo").Value, culture);
            this.release = Math.Abs(Int16.Parse(checkInReleaseNode.Attribute("release").Value, culture));
        }
    }


    public class StayRelease : Constraint
    {
        Int16 release;

        public Int16 Release
        {
            get { return release; }
            set { release = value; }
        }

		public StayRelease(XElement stayReleaseNode, JumboToursContract contract)
            : base(stayReleaseNode, contract)
        {
            this.stayFrom = DateTime.Parse(stayReleaseNode.Attribute("stayFrom").Value, culture);
            this.stayTo = DateTime.Parse(stayReleaseNode.Attribute("stayTo").Value, culture);
            this.release = Math.Abs(Int16.Parse(stayReleaseNode.Attribute("release").Value, culture));
        }
    }

    public class CompulsoryStay : Constraint
    {
        Int16 nights;

        public Int16 Nights
        {
            get { return nights; }
            set { nights = value; }
        }

		public CompulsoryStay(XElement compulsoryStayNode, JumboToursContract contract)
            : base(compulsoryStayNode, contract)
        {
            this.stayFrom = DateTime.Parse(compulsoryStayNode.Attribute("stayFrom").Value, culture);
            this.stayTo = DateTime.Parse(compulsoryStayNode.Attribute("stayTo").Value, culture);
            this.nights = Math.Abs(Int16.Parse(compulsoryStayNode.Attribute("nights").Value, culture));
        }

    }


    public class CancellationTerms : Constraint
    {
        Int16 release;
        public Int16 Release
        {
            get { return release; }
            set { release = value; }
        }

        int percent;
        public int Percent
        {
            get { return percent; }
            set { percent = value; }
        }

        int amount;
        public int Amount
        {
            get { return amount; }
            set { amount = value; }
        }

        int days;
        public int Days
        {
            get { return days; }
            set { days = value; }
        }

        string curency;
        public string Curency
        {
            get { return curency; }
            set { curency = value; }
        }

        string over;
        public string Over
        {
            get { return over; }
            set { over = value; }
        }

        public bool WeekDayBased { get; set; }
        public bool[] WeekDays { get; set; }

		public CancellationTerms(XElement cancellationTermsNode, JumboToursContract contract)
            : base(cancellationTermsNode, contract)
        {
            int tempRelease = (int)Math.Ceiling((JumboToursXML.DecimalParse(cancellationTermsNode.Attribute("release").Value, culture)));
            if (tempRelease > Int16.MaxValue) return;
            this.release = (Int16)tempRelease;
            this.percent = (int)Math.Ceiling(JumboToursXML.DecimalParse(cancellationTermsNode.Attribute("percent").Value, culture));
            this.amount = (int)Math.Ceiling(JumboToursXML.DecimalParse(cancellationTermsNode.Attribute("amount").Value, culture));
            this.days = (int)Math.Ceiling((JumboToursXML.DecimalParse(cancellationTermsNode.Attribute("penaltyDays").Value, culture)));
            this.curency = cancellationTermsNode.Attribute("currency").Value;
            this.over = cancellationTermsNode.Attribute("over").Value;
            this.stayFrom = DateTime.Parse(cancellationTermsNode.Attribute("stayFrom").Value, culture);
            this.stayTo = DateTime.Parse(cancellationTermsNode.Attribute("stayTo").Value, culture);

            if (!DateWithinFormalization(DateTime.Today))
            {
                this.over = null; //Cant use this cancellation term, formalization expired
            }

            this.WeekDayBased = false;
            if (cancellationTermsNode.Attribute("weekdays") != null)
            {
                string weekDaysTemp = cancellationTermsNode.Attribute("weekdays").Value;

                if (weekDaysTemp == "XXXXXXX")
                {
                    this.WeekDayBased = false;
                }
                else
                {
                    this.WeekDays = new bool[weekDaysTemp.Length];
                    int dayCount = 0;
                    foreach (char day in weekDaysTemp)
                    {
                        if (day == 'X')
                        {
                            this.WeekDays[dayCount++] = true;
                        }
                        else if (day == '.')
                        {
                            this.WeekDayBased = true;
                        }
                    }
                }

            }
        }
    }
}
