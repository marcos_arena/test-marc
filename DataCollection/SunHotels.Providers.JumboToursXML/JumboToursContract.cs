using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using SunHotels.XML.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Xml.Linq;

namespace SunHotels.Providers
{
	public class JumboToursContract
	{

		public static IFormatProvider culture = new CultureInfo("en-GB", true);
		public static IFormatProvider usCulture = new CultureInfo("en-US", true);


		string contractId;
		public string ContractId
		{
			get { return contractId; }
			set { contractId = value; }
		}

		//DateTime validFrom;
		//DateTime validTo;

		bool validContract = true;
		public bool ValidContract
		{
			get { return validContract; }
			set { validContract = value; }
		}

		bool maxInfantsControl;
		public bool MaxInfantsControl
		{
			get { return maxInfantsControl; }
			set { maxInfantsControl = value; }
		}

		string contractTitle;
		public string ContractTitle
		{
			get { return contractTitle; }
			set { contractTitle = value; }
		}

		string hotelId;
		public string HotelId
		{
			get { return hotelId; }
			set { hotelId = value; }
		}

		string hotelName;
		public string HotelName
		{
			get { return hotelName; }
			set { hotelName = value; }
		}

		Dictionary<string, ContractRoomType> roomTypes = new Dictionary<string, ContractRoomType>();
		public Dictionary<string, ContractRoomType> RoomTypes
		{
			get { return roomTypes; }
			set { roomTypes = value; }
		}

		string roomTypesString;
		public string RoomTypesString
		{
			get { return roomTypesString; }
			set { roomTypesString = value; }
		}

		Dictionary<string, List<JumboToursErrata>> erratas = new Dictionary<string, List<JumboToursErrata>>();
		public Dictionary<string, List<JumboToursErrata>> Erratas
		{
			get { return erratas; }
			set { erratas = value; }
		}

        Dictionary<PaxTypeEnum, PaxType> paxTypes = new Dictionary<PaxTypeEnum, PaxType>();
        public Dictionary<PaxTypeEnum, PaxType> PaxTypes
		{
			get { return paxTypes; }
			set { paxTypes = value; }
		}

		string boardtypes;
		public string Boardtypes
		{
			get { return boardtypes; }
			set { boardtypes = value; }
		}

		List<Constraint> constraints = new List<Constraint>();
		public List<Constraint> Constraints
		{
			get { return constraints; }
			set { constraints = value; }
		}

		List<MinStay> minstay = new List<MinStay>();
		public List<MinStay> Minstay
		{
			get { return minstay; }
			set { minstay = value; }
		}

		public List<JumboToursSupplement> Supplements { get; private set; }

		Dictionary<string, List<ContractHotelRoomGroup>> contractHotelRoomGroups = new Dictionary<string, List<ContractHotelRoomGroup>>();
		public Dictionary<string, List<ContractHotelRoomGroup>> ContractHotelRoomGroups
		{
			get { return contractHotelRoomGroups; }
			set { contractHotelRoomGroups = value; }
		}

		List<SalesClosed> salesClosedDates = new List<SalesClosed>();
		public List<SalesClosed> SalesClosedDates
		{
			get { return salesClosedDates; }
			set { salesClosedDates = value; }
		}

		List<CheckinDays> checkInDays = new List<CheckinDays>();
		public List<CheckinDays> CheckInDays
		{
			get { return checkInDays; }
			set { checkInDays = value; }
		}

		List<CheckInRelease> checkInReleases = new List<CheckInRelease>();
		public List<CheckInRelease> CheckInReleases
		{
			get { return checkInReleases; }
			set { checkInReleases = value; }
		}

		List<StayRelease> stayReleases = new List<StayRelease>();
		public List<StayRelease> StayReleases
		{
			get { return stayReleases; }
			set { stayReleases = value; }
		}

		List<CompulsoryStay> compulsoryStays = new List<CompulsoryStay>();
		public List<CompulsoryStay> CompulsoryStays
		{
			get { return compulsoryStays; }
			set { compulsoryStays = value; }
		}

		List<CancellationTerms> cancelTerms = new List<CancellationTerms>();
		public List<CancellationTerms> CancelTerms
		{
			get { return cancelTerms; }
			set { cancelTerms = value; }
		}

        Dictionary<string, List<JumboToursOffer>> offers = new Dictionary<string, List<JumboToursOffer>>();
		public Dictionary<string, List<JumboToursOffer>> Offers
		{
			get { return offers; }
			set { offers = value; }
		}

		List<StockNode> stock = new List<StockNode>();
		public List<StockNode> Stock
		{
			get { return stock; }
			set { stock = value; }
		}
		/// <summary>
		/// Only opened stock
		/// </summary>
		public List<StockNodeFilter> OpenedStock { get; set; }

        List<ContractPrice> prices = new List<ContractPrice>();
        public List<ContractPrice> Prices
        {
            get { return prices; }
            set { prices = value; }
        }


        Dictionary<string, Dictionary<DateTime, Dictionary<string, ContractPrice>>> adultPrices = new Dictionary<string, Dictionary<DateTime, Dictionary<string, ContractPrice>>>();
        //public Dictionary<string, Dictionary<DateTime, Dictionary<string, ContractPrice>>> AdultPrices
        //{
        //    get { return adultPrices; }
        //    set { adultPrices = value; }
        //}

        Dictionary<string, Dictionary<DateTime, Dictionary<string, ContractPrice>>> childrenPrices = new Dictionary<string, Dictionary<DateTime, Dictionary<string, ContractPrice>>>();
        //public Dictionary<string, Dictionary<DateTime, Dictionary<string, ContractPrice>>> ChildrenPrices
        //{
        //    get { return childrenPrices; }
        //    set { childrenPrices = value; }
        //}

		List<string> suplements = new List<string>();
		public List<string> Suplements
		{
			get { return suplements; }
			set { suplements = value; }
		}

		Dictionary<string, Dictionary<DateTime, decimal>> childDiscounts = new Dictionary<string, Dictionary<DateTime, decimal>>();
		public Dictionary<string, Dictionary<DateTime, decimal>> ChildDiscounts
		{
			get { return childDiscounts; }
			set { childDiscounts = value; }
		}

		/// <summary>
		/// Block list used to minimize logging
		/// </summary>
		private List<string> logBlock = new List<string>();
		private List<string> logFreeRoom = new List<string>();

        public JumboToursContract(XElement xmlContractNode, string hotelId, StreamWriter log)
		{
            //ValidContract = xmlContractNode.SelectNodes("suplements/ExtendedGenericSupplement").Count == 0;

			Supplements = new List<JumboToursSupplement>();

			this.contractId = xmlContractNode.Attribute("id").Value;
			this.contractTitle = xmlContractNode.Attribute("title").Value;

            if (xmlContractNode.Element("maxinfantsControl").Value == "true")
			{
				this.maxInfantsControl = true;
			}
			else
			{
				this.maxInfantsControl = false;
				//log.WriteLine(String.Format("Contract {0} doesnt allow children att all?", contractId));
			}


            IEnumerable<XElement> hotelContract = xmlContractNode.Elements("hotel").Where(h => h.Attribute("id").Value ==  hotelId).AsEnumerable();
            if (hotelContract.Count() > 1) throw new Exception("Same hotel id appears twice within the same contract");

            XElement hotelNode = hotelContract.First();
			this.hotelId = hotelNode.Attribute("id").Value;

			this.hotelName = hotelNode.Attribute("name").Value;


			StringBuilder bTypeBuilder = new StringBuilder();

            IEnumerable<XElement> priceBoardTypeNodeList = xmlContractNode.Elements("boardtypes").Elements("boardtype");
			int rtCount = 1;
			foreach (XElement priceBoardTypeNode in priceBoardTypeNodeList)
			{
				if (rtCount < priceBoardTypeNodeList.Count())
				{
					bTypeBuilder.AppendFormat("{0},", priceBoardTypeNode.Attribute("code").Value);
				}
				else
				{
					bTypeBuilder.Append(priceBoardTypeNode.Attribute("code").Value);
				}
				rtCount++;
			}
			this.boardtypes = bTypeBuilder.ToString();


            IEnumerable<XElement> roomTypeList = xmlContractNode.Elements("roomtypes").Elements("roomtype");

			foreach (XElement rType in roomTypeList)
			{
				ContractRoomType crType = new ContractRoomType(rType);

				//If this room has requirement of minchildren see if any rooms has same space for adults without requirement for children
				if (Int32.Parse(rType.Attribute("minChildren").Value) > 0)
				{
					string rTypeCode = rType.Attribute("code").Value;
					int requiredAdultPaxes = Int32.Parse(rType.Attribute("maxpaxes").Value) - Int32.Parse(rType.Attribute("minChildren").Value);
					int roomsFitForAdultsWithoutChildren = 0;
					foreach (XElement temprType in roomTypeList)
					{
						if (temprType.Attribute("code").Value != rTypeCode)
						{
							if ((Int32.Parse(temprType.Attribute("maxpaxes").Value) - Int32.Parse(temprType.Attribute("minChildren").Value)) == requiredAdultPaxes)
							{
								//Add the room only if it isnt a minchildren room itself
								if (Int32.Parse(temprType.Attribute("minChildren").Value) == 0)
								{
									if (crType.CompatibleRoomTypes != "")
									{
										crType.CompatibleRoomTypes += ",";
									}

									crType.CompatibleRoomTypes += temprType.Attribute("code").Value;
									roomsFitForAdultsWithoutChildren++;
								}
							}
						}
					}
					//Room that fits the same number of adults as the room that has minchildrenreq exists, add it.
					if (roomsFitForAdultsWithoutChildren > 0)
					{
						this.roomTypes.Add(rType.Attribute("code").Value, crType);
					}
					else
					{
						JumboLogger.AddDiscarded(contractId, contractId);
					}
				}
				//Room doesnt have minchildren req add it.
				else
				{
					this.roomTypes.Add(rType.Attribute("code").Value, crType);
				}
			}


			int cpCount = 1;
			StringBuilder rTypeBuilder = new StringBuilder();
			foreach (string priceRoomTypeNode in this.roomTypes.Keys)
			{
				if (cpCount < this.roomTypes.Count)
				{
					rTypeBuilder.AppendFormat("{0},", priceRoomTypeNode);
				}
				else
				{
					rTypeBuilder.Append(priceRoomTypeNode);
				}
				cpCount++;
			}
			this.roomTypesString = rTypeBuilder.ToString();



			
            IEnumerable<XElement> stockNodeList = xmlContractNode.Elements("stocks").Elements("stock");
			
			if (stockNodeList.Count() == 0)
			{
				//No stock no availability this hotel can be discarded this time
				return;
			}

            foreach (XElement stockNode in stockNodeList)
			{
				StockNode theStock = new StockNode(stockNode, this);
								
				/* DUPLICATED: ???
				 * if (stockNode.Attribute("roomtypes") != null)
				{
					theStock.RoomTypes = stockNode.Attribute("roomtypes").Value;
				}*/

				this.stock.Add(theStock);
				string sRoomTypes = string.Empty;

				int cRoomTypeCounter = 0;

				sRoomTypes = ContractId + ".";

				foreach (ContractRoomType rType in roomTypes.Values)
				{
					sRoomTypes += rType.Code;
					cRoomTypeCounter++;
					if (cRoomTypeCounter < roomTypes.Count)
					{
						sRoomTypes += ',';
					}
				}

				ContractHotelRoomGroup rGroup = new ContractHotelRoomGroup(contractId, hotelId, sRoomTypes);

				foreach (ContractRoomType rType in roomTypes.Values)
				{
					if (rGroup.RoomTypes.Contains(rType.Code))
					{
						rType.RoomGroup = rGroup.RoomTypes;
					}
				}

				string contractHotelKey = hotelId;
				if (!contractHotelRoomGroups.ContainsKey(contractHotelKey))
				{
					contractHotelRoomGroups = new Dictionary<string, List<ContractHotelRoomGroup>>();
				}

				if (!contractHotelRoomGroups.ContainsKey(contractHotelKey))
				{
					contractHotelRoomGroups[contractHotelKey] = new List<ContractHotelRoomGroup>();
				}


				if (!ContractHotelRoomGroups[contractHotelKey].Contains(rGroup))
				{
					contractHotelRoomGroups[contractHotelKey].Add(rGroup);
				}

			}

            IEnumerable<XElement> erratasList = xmlContractNode.Elements("hotel").Elements("erratas").Elements("errata");
			if (erratasList.Count() > 0)
			{
				string contractHotelKey = String.Format("{0}.{1}", contractId, hotelId);

				List<JumboToursErrata> tempErrataList = new List<JumboToursErrata>();

				foreach (XElement errata in erratasList)
				{
					JumboToursErrata JumboErrata = new JumboToursErrata(errata);
					tempErrataList.Add(JumboErrata);
				}

				if (!this.erratas.ContainsKey(contractHotelKey))
					this.erratas.Add(contractHotelKey, tempErrataList);
			}

            IEnumerable<XElement> roomTypesList = xmlContractNode.Elements("roomtypes").Elements("roomtype");
            foreach (XElement rType in roomTypesList)
			{
				ContractRoomType roomType = new ContractRoomType(rType);

				if (!this.roomTypes.ContainsKey(roomType.Code))
				{
					this.roomTypes.Add(roomType.Code, roomType);
				}
			}

            IEnumerable<XElement> paxTypesList = xmlContractNode.Elements("paxtypes").Elements("paxtype");
			foreach (XElement paxType in paxTypesList)
			{
				PaxType pax = new PaxType(paxType);
				this.paxTypes.Add(pax.Code, pax);
			}

            IEnumerable<XElement> constraintList = xmlContractNode.Elements("constraints");

			foreach (XElement constraint in constraintList)
			{
                foreach (XElement minstayNode in constraint.Elements("minstay").Elements("constraint"))
				{
					MinStay mStay = new MinStay(minstayNode, this);
					if (mStay.DateWithinFormalization(DateTime.Today))
					{
						if (mStay != null)
						{
							this.constraints.Add(mStay);
							this.minstay.Add(mStay);
						}
					}
				}

                foreach (XElement sClosedNode in constraint.Elements("salesclosed").Elements("constraint"))
				{
					SalesClosed sClosed = new SalesClosed(sClosedNode, this);
					if (sClosed.DateWithinFormalization(DateTime.Today))
					{
						this.constraints.Add(sClosed);
						this.salesClosedDates.Add(sClosed);
					}
				}


				foreach (XElement checkinDaysNode in constraint.Elements("checkindays").Elements("constraint"))
				{
					CheckinDays cInDays = new CheckinDays(checkinDaysNode, this);
					if (cInDays.DateWithinFormalization(DateTime.Today))
					{
						this.constraints.Add(cInDays);
						this.checkInDays.Add(cInDays);
					}
				}


				foreach (XElement checkInReleaseNode in constraint.Elements("checkinrelease").Elements("constraint"))
				{
					CheckInRelease checkInRelease = new CheckInRelease(checkInReleaseNode, this);
					if (checkInRelease.DateWithinFormalization(DateTime.Today))
					{
						this.constraints.Add(checkInRelease);
						this.checkInReleases.Add(checkInRelease);
					}
				}



				foreach (XElement stayReleaseNode in constraint.Elements("stayrelease").Elements("constraint"))
				{
					StayRelease stayRelease = new StayRelease(stayReleaseNode, this);
					if (stayRelease.DateWithinFormalization(DateTime.Today))
					{
						this.constraints.Add(stayRelease);
						this.stayReleases.Add(stayRelease);
					}
				}

				foreach (XElement compulsoryStayNode in constraint.Elements("compulsorystay").Elements("constraint"))
				{
					CompulsoryStay compulsoryStay = new CompulsoryStay(compulsoryStayNode, this);
					if (compulsoryStay.DateWithinFormalization(DateTime.Today))
					{
						this.constraints.Add(compulsoryStay);
						this.compulsoryStays.Add(compulsoryStay);
					}
				}

				foreach (XElement cancellationTermsNode in constraint.Elements("cancellationterms").Elements("constraint"))
				{
					CancellationTerms cancellationTerms = new CancellationTerms(cancellationTermsNode, this);
					if (cancellationTerms.DateWithinFormalization(DateTime.Today))
					{
						this.constraints.Add(cancellationTerms);
						this.cancelTerms.Add(cancellationTerms);
					}
				}
			}

            if (hotelId == "21652")
            {

            }
            
			foreach (XElement supplement in xmlContractNode.Elements("suplements").Elements("GenericSupplement"))
			{
				this.Supplements.AddRange(JumboToursSupplement.Parse(supplement, roomTypes, boardtypes, paxTypes, log));
			}
            
            foreach (XElement supplement in xmlContractNode.Elements("suplements").Elements("ExtendedGenericSupplement"))
            {
                this.Supplements.AddRange(JumboToursSupplement.Parse(supplement, roomTypes, boardtypes, paxTypes, log));
            }

			foreach (XElement contractPriceNode in xmlContractNode.Elements("prices").Elements("price"))
			{
				Dictionary<string, ContractRoomType> contractPriceRoomTypes = new Dictionary<string, ContractRoomType>();
                if (contractPriceNode.Attribute("roomtypes") != null && contractPriceNode.Attribute("roomtypes").Value != String.Empty)
				{
					foreach (string tempRoomtype in contractPriceNode.Attribute("roomtypes").Value.Split(','))
					{
						if (this.roomTypes.ContainsKey(tempRoomtype))
						{
							contractPriceRoomTypes.Add(tempRoomtype, this.roomTypes[tempRoomtype]);
						}
					}
				}
				else
				{
					contractPriceRoomTypes = this.roomTypes;
				}

				foreach (ContractRoomType roomtype in contractPriceRoomTypes.Values)
				{
					string contractPriceBoardtypes;
                    if (contractPriceNode.Attribute("boardtypes") != null && contractPriceNode.Attribute("boardtypes").Value != String.Empty)
					{
						contractPriceBoardtypes = contractPriceNode.Attribute("boardtypes").Value;
					}
					else
					{
						contractPriceBoardtypes = this.boardtypes;
					}

					foreach (string boardtype in contractPriceBoardtypes.Split(','))
					{
                        //string contractPriceRoomtypes;
                        //if (contractPriceNode.Attribute("roomtypes") != null)
                        //{
                        //    contractPriceRoomtypes = contractPriceNode.Attribute("roomtypes").Value;
                        //}
                        //else
                        //{
                        //    contractPriceRoomtypes = this.roomTypesString;

                        //}

                        //List<string> pricesRoomTypeList = new List<string>(contractPriceRoomtypes.Split(','));

                        //foreach (string roomType in pricesRoomTypeList)
                        //{
							//If the roomtype is declared in the contract use it
							if (this.roomTypes.ContainsKey(roomtype.Code))
							{
								decimal amount = JumboToursXML.DecimalParse(contractPriceNode.Attribute("amount").Value, usCulture);//We need the decimals in us format
								if (amount > 0)
								{
                                    if (this.HotelId == "54626")
                                    {

                                    }
									ContractPrice contPrice = new ContractPrice(contractPriceNode, roomtype, boardtype);

                                    this.prices.Add(contPrice);

									if (contPrice.Paxtypes.Contains(PaxTypeEnum.Adult))
									{

                                        if (!this.adultPrices.ContainsKey(roomtype.Code))
										{
                                            this.adultPrices.Add(roomtype.Code, new Dictionary<DateTime, Dictionary<string, ContractPrice>>());
										}

                                        if (!this.adultPrices[roomtype.Code].ContainsKey(contPrice.StayFrom))
										{
                                            this.adultPrices[roomtype.Code][contPrice.StayFrom] = new Dictionary<string, ContractPrice>();
										}

                                        if (!this.adultPrices[roomtype.Code][contPrice.StayFrom].ContainsKey(boardtype))
										{
                                            this.adultPrices[roomtype.Code][contPrice.StayFrom].Add(boardtype, contPrice);
										}

                                        if (this.adultPrices[roomtype.Code][contPrice.StayFrom][boardtype].WeekdaysString == contPrice.WeekdaysString)
										{
                                            this.adultPrices[roomtype.Code][contPrice.StayFrom][boardtype].PricePerAdult = contPrice.PricePerAdult;
										}
									}


									if (contPrice.Paxtypes.Contains(PaxTypeEnum.Child) && roomtype.MinChildren > 0 && this.paxTypes[PaxTypeEnum.Child].ToAge >= 11)
									{

                                        if (!this.childrenPrices.ContainsKey(roomtype.Code))
										{
                                            this.childrenPrices.Add(roomtype.Code, new Dictionary<DateTime, Dictionary<string, ContractPrice>>());
										}

                                        if (!this.childrenPrices[roomtype.Code].ContainsKey(contPrice.StayFrom))
										{
                                            this.childrenPrices[roomtype.Code][contPrice.StayFrom] = new Dictionary<string, ContractPrice>();

										}
                                        if (!this.childrenPrices[roomtype.Code][contPrice.StayFrom].ContainsKey(boardtype))
										{
                                            this.childrenPrices[roomtype.Code][contPrice.StayFrom].Add(boardtype, contPrice);
										}

                                        else if (this.childrenPrices[roomtype.Code][contPrice.StayFrom][boardtype].PricePerChild > contPrice.PricePerChild)
										{

                                            if (this.childrenPrices[roomtype.Code][contPrice.StayFrom][boardtype].WeekdaysString == contPrice.WeekdaysString)
											{
                                                this.childrenPrices[roomtype.Code][contPrice.StayFrom][boardtype].PricePerChild = contPrice.PricePerChild;
											}
										}
									}
								}
								else
								{
									string listKey = String.Format("{0}.{1}", contractId, roomtype);
									if (!logFreeRoom.Contains(listKey))
									{
										log.WriteLine(String.Format("Dropping price, 0(zero) value found. Contract [{0}] Room [{0}]", contractId, roomtype));
										logFreeRoom.Add(listKey);
									}
								}
							}
							else
							{
								string listKey = String.Format("{0}.{1}", contractId, roomtype);
								if (!logBlock.Contains(listKey))
								{
									log.WriteLine(String.Format("Skipping room {0} since it isnt declared in the contract {1}", roomtype, contractId));
									logBlock.Add(listKey);
								}
							}
						//}
					}
				}								
			}


			//Get Only Opened stock for this contract
			OpenedStock = GetOpenedStock().ToList();
			IEnumerable<XElement> offerNodeList = xmlContractNode.Elements("offers").Elements();
            string[] supportedOffers = { "EarlyBookingOffer", "ThreeForTwoOffer", "LongStayOffer", "ChildrenOffer", "NPaxOffer"}; 
			foreach (XElement offerNode in offerNodeList)
			{
                //if (!supportedOffers.Contains(offerNode.Name.ToString())) continue;
                
				JumboToursOffer offer = new JumboToursOffer(offerNode);
				if (offer.DateWithinFormalization(DateTime.Today))
				{
                    AddOffer(offer);
					//this.offers.Add(offer);
				}
			}
		}

        private void AddOffer(JumboToursOffer offer)
        {
            string[] roomTypes = null;
            if (!String.IsNullOrEmpty(offer.RoomTypes))
            {
                roomTypes = offer.RoomTypes.Split(',');
            }
            else
            {
                roomTypes = this.RoomTypes.Keys.ToArray();
            }

            foreach (string roomType in roomTypes)
            {
                if(!this.Offers.ContainsKey(roomType))
                {
                    this.Offers.Add(roomType, new List<JumboToursOffer>());
                }
                this.Offers[roomType].Add(offer.Clone() as JumboToursOffer);
            }
        }

        public void getDiscounts(AvailabilityPeriod currentPeriod, string roomTypeKey, string currentContractId, StreamWriter log, Dictionary<string, Room> hotelRooms, string cheapestMeal, JumboToursContractPriceManager currentPrices)
		{
            if (this.offers.ContainsKey(roomTypeKey))
            {
                foreach (JumboToursOffer theOffer in this.offers[roomTypeKey])
                {
                    if (theOffer.Id == 658829415)
                    {

                    }

                    if (!theOffer.MatchDate(currentPeriod.DateFrom) || !theOffer.Active || !(theOffer.WeekDayBased == false || theOffer.WeekDays[Convert.ToInt32(currentPeriod.DateFrom.DayOfWeek)]))
                        continue;

                    //If the roomtype is relevant to the discount but isn't matching don't add discount
                    if (theOffer.RoomTypeMatches(roomTypeKey, theOffer.RoomTypes))
                    {
                        //If the boardtype is relevant to the discount but isn't matching dont add the discount
                        ContractRoomType currentRoomType = this.RoomTypes[roomTypeKey];

                        if (theOffer.BoardTypeMatches(cheapestMeal, theOffer.BoardTypes))
                        {
                            addDiscountsToPeriod(theOffer.GetDiscounts(currentPrices, currentRoomType), currentPeriod);
                        }
                        else
                        {
                            string[] offerBoardTypesParts = theOffer.BoardTypes.Split(',');
                            List<string> boardList = new List<string>(offerBoardTypesParts);

                            if (boardList.Count == 1)
                            {
                                if (currentPeriod.Pricing_BaseIncludedBoard == getMealDesc(boardList[0]))
                                {
                                    addDiscountsToPeriod(theOffer.GetDiscounts(currentPrices, currentRoomType), currentPeriod);

                                    //clear additional board the discount only applies to the base board
                                    currentPeriod.AdditionalBoards.Clear();

                                }
                            }
                            else if (boardList.Count > 1)
                            {
                                //If the baseprice isn't included in the discount throw it away
                                if (currentPeriod.Pricing_BaseIncludedBoard == getMealDesc(boardList[0]))
                                {
                                    //Remove all mealtypes that the discount doesnt apply to
                                    for (int additionalBoardCount = 0; additionalBoardCount < currentPeriod.AdditionalBoards.Count; additionalBoardCount++)
                                    {
                                        if (!boardList.Contains(currentPeriod.AdditionalBoards[additionalBoardCount].BookableBoardId))
                                        {
                                            currentPeriod.AdditionalBoards.Remove(currentPeriod.AdditionalBoards[additionalBoardCount]);
                                        }
                                    }

                                    addDiscountsToPeriod(theOffer.GetDiscounts(currentPrices, currentRoomType), currentPeriod);
                                }
                            }
                            else
                            {
                                Console.WriteLine(string.Format("Discarding offer:{0} in {1} on {2} since its based on boardtype", theOffer.Name, contractId, currentPeriod.DateFrom.ToShortDateString()));
                                JumboLogger.AddDiscarded(contractId, string.Format("Discarding {0} in {1} since its based on boardtype:{2}", theOffer.Name, contractId, theOffer.BoardTypes));
                            }
                            
                        }
                    }
                }
            }
		}

		/// <summary>
		/// Adds discounts to a room-availability period
		/// </summary>
		/// <param name="theOffer"></param>
		/// <param name="currentPeriod"></param>
		public void addDiscountsToPeriod(Discount discount, AvailabilityPeriod currentPeriod)
		{
            currentPeriod.Discounts.TryAdd(discount);
		}

		/// <summary>
		/// Gets all discount that apply to children
		/// but since child discounts dont exist in the sunhotels system,
		/// they are applied as lower prices per extra childbed instead
		/// </summary>
		/// <param name="currentPeriod"></param>
		/// <param name="roomTypeKey"></param>
		/// <param name="currentContractId"></param>
		/// <param name="log"></param>
		/// <param name="hotelRooms"></param>
		/// <returns></returns>
		public Dictionary<string, Dictionary<DateTime, decimal>> getChildDiscounts(AvailabilityPeriod currentPeriod, string roomTypeKey, string currentContractId, StreamWriter log, Dictionary<string, Room> hotelRooms, string cheapestMeal, JumboToursContractPriceManager currentPrices)
		{
            if (this.offers.ContainsKey(roomTypeKey))
            {
                foreach (JumboToursOffer theOffer in this.offers[roomTypeKey])
                {
                    //If the roomtype is relevant to the discount but isn't matching don't add discount
                    if (theOffer.RoomTypeMatches(roomTypeKey, theOffer.RoomTypes))
                    {
                        //If the boardtype is relevant to the discount but isn't matching dont add the discount
                        if (theOffer.BoardTypeMatches(cheapestMeal, theOffer.BoardTypes))
                        {
                            if (theOffer.MatchDate(currentPeriod.DateFrom) && theOffer.Active)
                            {
                                int? childDiscountPercent = theOffer.GetChildDiscount(currentPrices);
                                if (childDiscountPercent.HasValue)
                                {
                                    if (!ChildDiscounts.ContainsKey(roomTypeKey))
                                    {
                                        ChildDiscounts[roomTypeKey] = new Dictionary<DateTime, decimal>();
                                    }

                                    ChildDiscounts[roomTypeKey][currentPeriod.DateFrom] = childDiscountPercent.Value;
                                }
                            }
                        }
                    }
                }
            }
			return childDiscounts;
		}		

		public IEnumerable<StockNodeFilter> GetOpenedStock()
		{
			//initialize salesclosed
			IEnumerable<SalesClosed> closed = salesClosedDates;
			foreach (var s in Stock)
			{
				//searching in sales closed which sales closed affects to stock period, and ungroup this sales closed (board, day, roomtype)
				IEnumerable<salesClosedFilter> closeList = closed.Where(c => TimePeriodOverlap(s.startDate, s.endDate, c.StayFrom, c.StayTo))
					.SelectMany(c => c.UnGroup())
					.ToArray();
				var stockFiltered = new StockNodeFilter();
				if (closeList.Any())
				{
					//if we have any close for this stock period, first ungroup (board, day, roomtype) and returning just opened stocks
					foreach (var ns in s.UnGroup().Where(us => !closeList.Any(cl => cl.start == us.start && cl.boards.FirstOrDefault() == us.boards.FirstOrDefault() && cl.roomtypes.FirstOrDefault() == us.roomtypes.FirstOrDefault())))
					{
						ns.WeekDays = s.WeekDays;
						yield return ns;
					}
						
				}
				else
				{
					stockFiltered.boards = s.BoardTypes;
					stockFiltered.roomtypes = s.RoomTypes;
					stockFiltered.start = s.startDate;
					stockFiltered.end = s.endDate;
					stockFiltered.AvailableUnits = s.AvailableUnits;
					stockFiltered.WeekDays= s.WeekDays;
					yield return stockFiltered;
				}
			}
		}

		public bool TimePeriodOverlap(DateTime StockStart, DateTime StockEnd, DateTime CloseStart, DateTime CloseEnd)
		{


			return (
						(CloseEnd >= StockStart && CloseEnd <= StockEnd)

						||

						(CloseStart >= StockStart && CloseStart <= StockEnd)

						||

						(CloseStart < StockStart && CloseEnd > StockEnd)
					);		
		}

		/// <summary>
		/// Get all minstay constraints that instead of dissallowing booking, enforce extra payment for stay bellow mindays
		/// </summary>
		/// <param name="period"></param>
		/// <returns>List<Minstay></returns>
		public List<MinStay> getMinStaySupplement(DateTime period)
		{
			List<MinStay> minStaysSup = new List<MinStay>();
			foreach (MinStay minstay in this.Minstay)
			{
				if (minstay.Result == "SUPPLEMENT")
				{
					if (minstay.StayFrom.CompareTo(period) <= 0 && minstay.StayTo.CompareTo(period) >= 0)
					{
						if (!minStaysSup.Contains(minstay))
						{
							minStaysSup.Add(minstay);
						}
					}
				}
			}

			//Sort the minstays before returning it
			minStaysSup.Sort(CompareMinStay);

			return minStaysSup;
		}


		/// <summary>
		/// Get all minstay constraints that instead of dissallowing booking, enforce extra payment for stay bellow mindays
		/// </summary>
		/// <param name="period"></param>
		/// <returns>List<Minstay></returns>
		public IEnumerable<JumboToursSupplement> getSupplements(DateTime period)
		{
			List<JumboToursSupplement> supplements = new List<JumboToursSupplement>();
			foreach (JumboToursSupplement supplement in this.Supplements)
			{
				//if (minstay.Result == "SUPPLEMENT")
				//{
				//    if (minstay.StayFrom.CompareTo(period) <= 0 && minstay.StayTo.CompareTo(period) >= 0)
				//    {
				//        if (!minStaysSup.Contains(minstay))
				//        {
				//            minStaysSup.Add(minstay);
				//        }
				//    }
				//}
			}

			//Sort the minstays before returning it
			//minStaysSup.Sort(CompareMinStay);

			return supplements;
		}

		public MinStay getMinStayReject(DateTime period)
		{
			MinStay minStaysRej = null;
			foreach (MinStay minstay in this.Minstay)
			{

				if (minstay.Result == "REJECT")
				{
					if (minstay.StayFrom.CompareTo(period) <= 0 && minstay.StayTo.CompareTo(period) >= 0)
					{
						if (minStaysRej == null)
						{
							minStaysRej = minstay;
						}
						else
						{
							if (minstay.Nights > minStaysRej.Nights)
							{
								minStaysRej = minstay;
							}
						}
					}
				}
			}

			return minStaysRej;
		}

		/// <summary>
		/// Compares 2 minsstayvalues
		/// used to deicide which minstay to use when rooms just charge extra
		/// for the room if you stay less than the specified minstay value
		/// </summary>
		/// <param name="x"></param>
		/// <param name="y"></param>
		/// <returns></returns>
		private static int CompareMinStay(MinStay x, MinStay y)
		{
			return y.Nights.CompareTo(x.Nights);
		}

		/// <summary>
		/// Converts the JumboTours meals to correct SunHotels meals
		/// </summary>
		/// <param name="meal">The jumbotours definition of a certain meal</param>
		/// <returns>The sunhotels translation of that certain meal</returns>
		public string getMealDesc(string meal)
		{
			string mealDesc = "";
			switch (meal)
			{
				case "BB": mealDesc = "breakfast"; break;//Bed&Breakfast
				case "HB": mealDesc = "half_board"; break;//Haflboard
				case "FB": mealDesc = "full_board"; break;//Fullboard
				case "AI": mealDesc = "all_inclusive"; break;//All Inclusive
				case "SC": mealDesc = "none"; break;//Self Catering
				case "RO": mealDesc = "none"; break;//Room only
			}
			return mealDesc;

		}




        //public decimal getChildrenPrice(string roomtype, DateTime date, string mealtype)
        //{
        //    decimal childrenPrice = -1;

        //    if (childrenPrices.ContainsKey(roomtype))
        //    {
        //        foreach (Dictionary<string, ContractPrice> childrenPriceDictionary in childrenPrices[roomtype].Values)
        //        {
        //            if (childrenPriceDictionary.ContainsKey(mealtype))
        //            {
        //                if ((date.CompareTo(childrenPriceDictionary[mealtype].StayFrom) >= 0) && (date.CompareTo(childrenPriceDictionary[mealtype].StayTo) <= 0) && childrenPriceDictionary[mealtype].MatchWeekDay(date.DayOfWeek))
        //                {
        //                    childrenPrice = childrenPriceDictionary[mealtype].PricePerChild;

        //                }
        //            }
        //        }
        //    }

        //    return childrenPrice;
        //}

        //public decimal getAdultPrice(string roomtype, DateTime date, string mealtype)
        //{
        //    decimal adultPrice = -1;

        //    if (adultPrices.ContainsKey(roomtype))
        //    {
        //        foreach (Dictionary<string, ContractPrice> adultPricesDictionary in adultPrices[roomtype].Values)
        //        {
        //            if (adultPricesDictionary.ContainsKey(mealtype))
        //            {
        //                if ((date.CompareTo(adultPricesDictionary[mealtype].StayFrom) >= 0) && (date.CompareTo(adultPricesDictionary[mealtype].StayTo) <= 0) && adultPricesDictionary[mealtype].MatchWeekDay(date.DayOfWeek))
        //                {
        //                    adultPrice = adultPricesDictionary[mealtype].PricePerAdult;
        //                }
        //            }
        //        }
        //    }
        //    return adultPrice;
        //}

        //public PriceType getPaxType(string roomtype, DateTime date, string mealtype)
        //{
        //    PriceType paxType = PriceType.Undefined;

        //    if (adultPrices.ContainsKey(roomtype))
        //    {
        //        foreach (Dictionary<string, ContractPrice> adultPricesDictionary in adultPrices[roomtype].Values)
        //        {
        //            if (adultPricesDictionary.ContainsKey(mealtype))
        //            {
        //                if ((date.CompareTo(adultPricesDictionary[mealtype].StayFrom) >= 0) && (date.CompareTo(adultPricesDictionary[mealtype].StayTo) <= 0) && adultPricesDictionary[mealtype].MatchWeekDay(date.DayOfWeek))
        //                {
        //                    paxType = adultPricesDictionary[mealtype].Type;
        //                }
        //            }
        //        }
        //    }
        //    return paxType;
        //}


	}
}



