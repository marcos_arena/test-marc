using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace SunHotels.Providers
{

	class JumboLogger
	{
		static Dictionary<string, List<string>> discardedOffers = new Dictionary<string, List<string>>();

		public static void AddDiscarded(string contract, string reason)
		{
			if (!discardedOffers.ContainsKey(contract))
			{
				discardedOffers.Add(contract, new List<string>());
			}

			if (!discardedOffers[contract].Contains(reason))
			{

				discardedOffers[contract].Add(reason);
			}

		}

		public static void PrintLog(string logfile, StreamWriter log)
		{
			foreach (string key in discardedOffers.Keys)
			{
				log.WriteLine("Discarded Offer:" + key);
				foreach (string reason in discardedOffers[key])
				{
					if (!reason.Equals(key))
					{
						log.WriteLine(reason);
					}
				}
			}
		}
	}
}
