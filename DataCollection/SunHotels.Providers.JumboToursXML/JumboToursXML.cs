using System;
using System.Collections.Generic;
using System.Text;
using SunHotels.XML;
using SunHotels.XML.HelpClasses;
using SunHotels.XML.Data;
using System.Xml;
using System.IO;
using System.Globalization;
using System.Data;
using ICSharpCode.SharpZipLib.Zip;
using ICSharpCode.SharpZipLib.Zip.Compression.Streams;
using System.Text.RegularExpressions;
using System.Linq;
using System.Xml.Serialization;
using System.Xml.Linq;
using Sunhotels.Export;
using SunHotels.Providers.ProviderCommon.DataLoader;

namespace SunHotels.Providers
{
    public class JumboToursXML : XMLProvider
    {
        /*<?xml version="1.0" encoding="UTF-8"?>
         * <xts-tcache>
         *   <contract>
         *       <hotel>
         *           <erratas>
         *               <errata>
         *                   <translation></translation>
         *               </errata>
         *           </erratas>
         *       </hotel>
         *       <roomtypes>
         *          <roomtype/>
         *       </roomtypes>
         *       <paxtypes>
         *          <paxtype/>
         *       </paxtypes>
         *       <boardtypes>
         *          <boardtype/>
         *       </boardtypes>
         *       <constraints>
         *          <minstay/>
         *          <checkindays/>
         *          <checkinrelease>
         *             <constraint/>
         *          </checkinrelease>
         *          <cancellationterms>
         *              <constraint/>
         *          </cancellationterms>
         *       </constrains>
         *       <stocks>
         *          <stock/>
         *       </stocks>
         *       <prices>
         *          <price/>
         *       </prices>
         *       <suplements/>
         *       <offers/>
         *   </contract> 
         * </xts-tcache>
         * 
         */

        bool serverDown = false;

        XMLCommunication xmlCom = new XMLCommunication();
        XDocument xmlDoc;
        XDocument xmlDoc2;

        IFormatProvider culture = new CultureInfo("en-GB", true);
        IFormatProvider usCulture = new CultureInfo("en-US", true);

        Dictionary<string, Hotel> hotels = new Dictionary<string, Hotel>(3000);
        Dictionary<string, Dictionary<DateTime, Dictionary<string, JumboToursContractPriceManager>>> CurrentHotel;

        Dictionary<string, List<CancellationTerms>> cancelTermsDictionary = new Dictionary<string, List<CancellationTerms>>();

        JumboToursParser contractParser = new JumboToursParser();

        Dictionary<string, string> roomDescriptions = new Dictionary<string, string>();
        StreamWriter log;

        Dictionary<string, List<Discount>> roomsWithMinStaySupplement = new Dictionary<string, List<Discount>>();

        string currentContractId = "";
        int beds = 0;
        int extraBeds = 0;

        //Set this to true in the final release to include cancellationPolicies
        bool cancelPoliciesIncluded = true;

        JumboToursSettings settings = null;

        /// <summary>
        /// Block list used to minimize logging
        /// </summary>
        List<string> logBlock = new List<string>();

        protected override void getData(Root root)
        {
            LoadSettings();

            DateTime programStartTime = DateTime.Now;

            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("Program started:" + programStartTime);
            Console.WriteLine("-------------------------------------------------------");

			var data = new StaticData(configuration.LocalTemp, configuration.ServersConfigFileFullPath);
			// Files older than 2 days
			int olderCounter = 0;
			data.RemoteDirectoryListingFilter = (remotePath, fileInfo, list) =>
			{
				
				switch(fileInfo.type){
					case ProviderCommon.ServersServerFolderFileType.HotelAvailability:
                        // Check date
                        list = list.Select(e => {
                            var m = System.Text.RegularExpressions.Regex.Match(e.Name, ".*([0-9]{8})_?([0-9]+).*");
                            return new
                            {
                                e,
                                FileDate = m.Groups[1].Value,
                                Version = int.Parse(m.Groups[2].Value)
                            };
                        })
                        .OrderBy(f => f.FileDate).ThenBy(f => f.Version)
                        .Select(f => f.e)
                        .ToArray();
                        if ((DateTime.Today - list.Last().Date).TotalDays > 1)
                            olderCounter++;
                        // Last one
                        return list.Skip(list.Count() - 1);

                    case ProviderCommon.ServersServerFolderFileType.HotelInfo:
						// Check date
						list = list.Select(e =>{
							var m = System.Text.RegularExpressions.Regex.Match(e.Name, ".*([0-9]{8})_v?([0-9]+).*");
								return new {
									e,
									FileDate = m.Groups[1].Value,
									Version = int.Parse(m.Groups[2].Value)
								};
						})
						.OrderBy(f => f.FileDate).ThenBy(f => f.Version)
						.Select(f => f.e)
						.ToArray();
						if ((DateTime.Today - list.Last().Date).TotalDays > 1)
							olderCounter++;
						// Last one
						return list.Skip(list.Count() - 1);
				}
				return list;
			};
			data.DownloadFiles();

			if (olderCounter == 2)
			{
				throw new Exception("Both hotel and cachefiles older than 2 days ");
			}

			var files = data.GetDownloadedFiles();
            // Get room descriptions from file.
            if (!files.Any())
            {
                throw new Exception("No files downloaded");
            }
            var roomTypeFiles = files.Where(f => f.FileInfo.type == ProviderCommon.ServersServerFolderFileType.RoomType);
            if(!roomTypeFiles.Any())
            {
                throw new Exception("No RoomType files found");
            }
            getRoomsDesc(roomTypeFiles.First().LocalPath);
			 

            if (!Directory.Exists("./Log/Jumbotours"))
            {
                Directory.CreateDirectory("./Log/Jumbotours");
            }

            string filename = string.Format("/Log/Jumbotours/JumboTours {0}.log", DateTime.Now.ToString("yyyyMMdd HHmmss"));
            log = new StreamWriter(Directory.GetCurrentDirectory() + filename);

            try
            {
              
                xmlDoc = XDocument.Load(files.First(f => f.FileInfo.type == ProviderCommon.ServersServerFolderFileType.HotelAvailability).LocalPath);
                contractParser.ParseContract(xmlDoc, log);

                xmlDoc2 = XDocument.Load(files.First(f => f.FileInfo.type == ProviderCommon.ServersServerFolderFileType.HotelInfo).LocalPath);
                contractParser.parseEstablishment(xmlDoc2);

                TimeSpan timeToParseAllContracts = DateTime.Now - programStartTime;
                Console.WriteLine("Time elapsed to parse contracts:" + timeToParseAllContracts);

                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine(string.Format("{0} - Starting to get places", DateTime.Now));

                getPlaces();

                TimeSpan totalRunTime = DateTime.Now - programStartTime;
                Console.WriteLine("Total Runtime: " + totalRunTime);
                Optimizer.OptimizeAvailabilityPeriods(root);

                log.AutoFlush = true;
                JumboLogger.PrintLog(string.Format("JumboToursDiscarded {0}.log", DateTime.Now.ToString("yyyyMMdd HHmmss")), log);
            }
            catch (Exception e)
            {
                log.WriteLine(String.Format("FATAL ERROR in getData, process aborted: {1}{0}", e.ToString(), Environment.NewLine));
                log.Flush();
                throw;
            }

			
        }

        private void LoadSettings()
        {
            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(JumboToursSettings));
                FileStream file = new FileStream(configuration.ExportConfigFileFullPath, FileMode.Open, FileAccess.Read);

                settings = (JumboToursSettings)serializer.Deserialize(file);
                file.Close();
            }
            catch (Exception ex)
            {
                throw new Exception("Error loading JumboToursSettings", ex);
            }
        }

        /// <summary>
        /// Gets all Countries,states,cities,and hotels
        /// </summary>
        private void getPlaces()
        {
            DateTime placesNow = DateTime.Now;

            // THESE FUCKING FOREACH LOOPS MAKES ME SICK!

            foreach (JumboToursEstablishment place in contractParser.EstablishmentList)
            {
                Place p = new Place();
                p.Id = place.CountryCode;
                p.Description = place.CountryName;

                if (!root.Places.Contains(p))
                {
                    root.Places.Add(p);

                    foreach (JumboToursEstablishment state in contractParser.EstablishmentList)
                    {
                        Place s = new Place();
                        if (state.CountryCode == p.Id)
                        {
                            s.Id = string.Format("{0}.{1}", p.Id, state.StateCode);
                            s.Description = state.StateName;

                            if (!p.Places.Contains(s))
                            {
                                p.Places.Add(s);
                                foreach (JumboToursEstablishment city in contractParser.EstablishmentList)
                                {
                                    Place c = new Place();
                                    string[] jointId = s.Id.Split('.');

                                    if (city.StateCode == jointId[1])
                                    {
                                        c.Id = string.Format("{0}.{1}.{2}", city.CountryCode, city.StateCode, city.CityCode);
                                        c.Description = UTF8Decode(city.CityName);
                                        if (!s.Places.Contains(c))
                                        {
                                            s.Places.Add(c);
                                            string[] jointCityId = c.Id.Split('.');

                                            foreach (JumboToursEstablishment hotel in contractParser.EstablishmentList)
                                            {
                                                Hotel h = new Hotel();

                                                //if (hotel.Id != "4731")
                                                //{
                                                //    continue;
                                                //}

                                                if (hotel.CityCode == city.CityCode)
                                                {
                                                    h.Id = UTF8Decode(hotel.Id);

                                                    //If the hotel isn't in the hotel dictionary 
                                                    //it wont have availability, therefore no need to import it
                                                    if (!contractParser.Hotels.ContainsKey(h.Id)) continue;

                                                    StringBuilder erratasToAppend = new StringBuilder();
                                                    List<JumboToursErrata> jTErrataList;
                                                    foreach (JumboToursContract con in contractParser.Hotels[h.Id])
                                                    {
                                                        currentContractId = con.ContractId;

                                                        if (con.Erratas.ContainsKey(string.Format("{0}.{1}", currentContractId, h.Id)))
                                                        {
                                                            jTErrataList = con.Erratas[string.Format("{0}.{1}", currentContractId, h.Id)];

                                                            foreach (JumboToursErrata jTErrata in jTErrataList)
                                                            {
                                                                if (jTErrata.To.CompareTo(DateTime.Today) < 0) continue; //don't include expired erratas
                                                                if (!erratasToAppend.ToString().Contains(string.Format("Errata:{0}-{1}: {2}", jTErrata.From.ToShortDateString(), jTErrata.To.ToShortDateString(), jTErrata.TranslationEnglish)))
                                                                {
                                                                    erratasToAppend.AppendLine(string.Format("Errata:{0}-{1}: {2}", jTErrata.From.ToShortDateString(), jTErrata.To.ToShortDateString(), jTErrata.TranslationEnglish));
                                                                }
                                                            }
                                                        }
                                                    }

                                                    string hotelDesc = hotel.LongDescription;

                                                    //removes the characters
                                                    string regPattern = "(\r)|(\t)|(\n)(\f)(  )*";
                                                    hotelDesc = Regex.Replace(hotelDesc, regPattern, "");
                                                    hotelDesc = hotelDesc.Trim();

                                                    string finalHotelDesc = string.Format("{0}{1}", erratasToAppend, hotelDesc);
                                                    if (finalHotelDesc.Length >= 900)
                                                    {
                                                        finalHotelDesc = finalHotelDesc.Remove(899, finalHotelDesc.Length - 900);
                                                    }

                                                    string headline = hotel.ShortDescription;
                                                    headline = headline.Trim();

                                                    if (headline.Length > 50)
                                                    {
                                                        headline = headline.Remove(49, headline.Length - 50);
                                                    }

                                                    h.Headline = headline;
                                                    h.Description = finalHotelDesc;

                                                    h.Name = hotel.Name;
                                                    h.Adress_Country = p.Description;
                                                    h.Adress_City = c.Description;
                                                    h.Adress_State = s.Description;
                                                    h.Adress_Street1 = hotel.Address;
                                                    h.Adress_Zipcode = hotel.ZipCode;
                                                    h.Classification = hotel.CategoryCode;
                                                    h.Email = hotel.Email;
                                                    h.Fax = hotel.Fax;
                                                    h.Phone = hotel.Telephone;
                                                    h.PlaceId = c.Id;
                                                    h.Position_Longitude = hotel.Longitude;
                                                    h.Position_Latitude = hotel.Latitude;

                                                    if (hotel.HotelDistances != null)
                                                    {
                                                        foreach (Hotel.Distance hDist in hotel.HotelDistances)
                                                        {
                                                            h.Distances.Add(hDist);
                                                        }
                                                    }
                                                    foreach (string feat in hotel.Features)
                                                    {
                                                        h.Features.Add(new Feature() { Name = feat, Value = "true" });
                                                    }

                                                    if (contractParser.Hotels.ContainsKey(h.Id) && !c.Hotels.ContainsKey(h.Id))
                                                    {
                                                        c.Hotels.Add(h.Id, h);
                                                        hotels.Add(h.Id, h);
                                                    }

                                                }
                                            }//End foreach city
                                        }
                                    }
                                }
                            }
                        }
                    }//End foreach state
                }
            }//End foreach place

            TimeSpan placesTimeSpan = DateTime.Now - placesNow;
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Out.WriteLine("Time elapsed to fetch all places and hotels: " + placesTimeSpan.Seconds);
            int ContractCounter = 0;
            int HotelCounter = 0;

            Console.WriteLine("Processing contracts this may take a couple of minutes");


            //for every hotel there is one or more contracts that need to be parsed in order to retrieve all the information needed
			foreach (Hotel hotel in hotels.Values)
            {
                try
                {              
                    HotelCounter++;
                    Console.WriteLine(String.Format("Hotel {0}/{1}", HotelCounter, hotels.Count));

                    CurrentHotel = new Dictionary<string, Dictionary<DateTime, Dictionary<string, JumboToursContractPriceManager>>>(20);
                    foreach (JumboToursContract c in contractParser.Hotels[hotel.Id])
                    {
                        getContract(hotel, c);
                        ContractCounter++;

                        //Console.Out.WriteLine(string.Format("{0} {1}", ContractCounter, h.Name));
                    }

                    Optimizer.OptimizeHotel(root, hotel);
                }
                catch (Exception e)
                {
                    Console.WriteLine("Error processing hotel " + hotel.Id + ": " + e.ToString());
                    log.WriteLine("Error processing hotel " + hotel.Id + ": " + e.ToString());
                }
            }
            Console.Out.WriteLine(ContractCounter + " Contracts total");
            Console.Out.WriteLine("Writing cachefiles.");
        }

        /// <summary>
        /// Gets all contracts for the specified hotel
        /// </summary>
        /// <param name="h"></param>
        /// <param name="contractCounter"></param>
        private void getContract(Hotel h, JumboToursContract contract)
        {
            currentContractId = contract.ContractId;

            foreach (CancellationTerms cancelTerm in contract.CancelTerms)
            {
                if (cancelTerm != null)
                {
                    if (!cancelTermsDictionary.ContainsKey(currentContractId))
                    {
                        cancelTermsDictionary.Add(currentContractId, new List<CancellationTerms>());
                    }

                    if (!cancelTermsDictionary[currentContractId].Contains(cancelTerm))
                    {
                        cancelTermsDictionary[currentContractId].Add(cancelTerm);
                    }
                }
            }

            DateTime now = DateTime.Now;

            getContractRooms(h, contract);

            //TimeSpan tmpTime = DateTime.Now - now;
            //if (tmpTime.Seconds > 0)
            //{
            //    Console.ForegroundColor = ConsoleColor.Red;
            //    Console.Out.Write(string.Format("time: {0} sec ", tmpTime.Seconds));
            //}
            //else
            //{
            //    Console.ForegroundColor = ConsoleColor.White;
            //    Console.Out.Write(string.Format("time: {0} msec ", tmpTime.Milliseconds));
            //}
        }

        /// <summary>
        /// Compares if the dates are on the closedDates list and adds them if they aren't
        /// </summary>
        /// <param name="dateFrom"></param>
        /// <param name="dateTo"></param>
        /// <param name="salesClosedDates"></param>
        /// <param name="roomTypeId"></param>
        /// <param name="room"></param>
        /// <param name="h"></param>
        /// <param name="Hotel"></param>
        /// <param name="singleBoardType"></param>
        /// <param name="contract"></param>
        private void getAvailableDates(DateTime dateFrom, DateTime dateTo, string roomTypeId, ContractPrice contractPrice, JumboToursContract contract)
        {
			
            for (DateTime currentDate = dateFrom; currentDate <= dateTo; currentDate = currentDate.AddDays(1))
            {
				// No opened price
				if (!contractPrice.MatchWeekDay(currentDate.DayOfWeek) || 
						// No stock!
						!contract.OpenedStock.Any(os => os.MatchDate(currentDate)))
                {
                    continue;
                }

                TimeSpan ts = new TimeSpan(configuration.NrDaysInCache, 0, 0, 0);

                if (-DateTime.Today.Subtract(currentDate) > ts) break;
                if (!CurrentHotel[roomTypeId].ContainsKey(currentDate))
                {
                    CurrentHotel[roomTypeId][currentDate] = new Dictionary<string, JumboToursContractPriceManager>();
                }

                //CurrentHotel[roomTypeId][currentDate][contractPrice.Boardtypes] = contractPrice.Amount + contractPrice.Feebooking;
                if (!CurrentHotel[roomTypeId][currentDate].ContainsKey(contractPrice.Boardtypes))
                {
                    CurrentHotel[roomTypeId][currentDate][contractPrice.Boardtypes] = new JumboToursContractPriceManager();
                }
                CurrentHotel[roomTypeId][currentDate][contractPrice.Boardtypes].ContractPrices.Add(contractPrice.Clone());

            } // End foreach dateTime 
        }

        /// <summary>
        /// Gets all rooms for this contract
        /// </summary>
        /// <param name="h"></param>
        /// <param name="contract"></param>
        /// <param name="Hotel"></param>
        /// <param name="singleBoardType"></param>
        /// <param name="singleRoomType"></param>
        private void getContractRooms(Hotel h, JumboToursContract contract)
        {
            if (h.Id == "54626")
            {
            }

            foreach (ContractPrice contractPrice in contract.Prices)
            {
                string roomTypeId = contractPrice.Roomtypes;
                DateTime dateFrom;

                if (roomTypeId != null && contract.RoomTypes.ContainsKey(roomTypeId))
                {
                    if (!CurrentHotel.ContainsKey(roomTypeId))
                    {
                        CurrentHotel[roomTypeId] = new Dictionary<DateTime, Dictionary<string, JumboToursContractPriceManager>>();
                    }
                    dateFrom = contractPrice.StayFrom;

                    if (dateFrom < DateTime.Today)
                    {
                        dateFrom = DateTime.Today;
                    }
                    DateTime dateTo = contractPrice.StayTo;

                    if (h.Id == "45135" && roomTypeId == "JS2")
                    {

                    }

                    getAvailableDates(dateFrom, dateTo, roomTypeId, contractPrice, contract);
                }


            }

            foreach (string roomType in CurrentHotel.Keys)
            {
                if (!contract.RoomTypes.ContainsKey(roomType)) continue;
                
                AddRoomsToHotel(h, contract, roomType);// , contractPrice.StayFrom, contractPrice.Amount, contractPrice);
            }

        }

        /// <summary>
        /// Iterates trough all rooms for this hotel and adds them.
        /// </summary>
        /// <param name="h"></param>
        /// <param name="contract"></param>
        /// <param name="Hotel"></param>
        /// <param name="weekdays"></param>
        private void AddRoomsToHotel(Hotel h, JumboToursContract contract, string roomTypeKey)
        {
            int minAdults = 0;
            int minChildren = 0;

            List<ContractHotelRoomGroup> cHGroupList = contract.ContractHotelRoomGroups[h.Id];

            //if the hotel doesnt have a roomgroup make one else add roomtypes to it
            if (!h.RoomGroups.ContainsKey(h.Id))
            {
                RoomGroup rGroup = new RoomGroup();

                rGroup.RoomGroupId = h.Id;
                h.RoomGroups.Add(h.Id, rGroup);
            }
            
            String roomId = String.Format("{0}.{1}", h.Id, roomTypeKey);

            string minchildrenRoomTypes = "";

            foreach (RoomGroup rGroup in h.RoomGroups.Values)
            {
                Room r;

                if (rGroup.Rooms.ContainsKey(roomId))
                {
                    r = rGroup.Rooms[roomId];
                }
                else
                {
                    r = new Room();
                }

                minAdults = contract.RoomTypes[roomTypeKey].MinAdults;
                minChildren = contract.RoomTypes[roomTypeKey].MinChildren;

                if (minchildrenRoomTypes.Length > 0)
                {
                    if (minchildrenRoomTypes != "")
                    {
                        minchildrenRoomTypes += ",";
                    }
                    minchildrenRoomTypes += roomTypeKey;
                }

                r.RoomId = roomId;
                r.TypeId = r.RoomId;

                string roomDesc = roomTypeKey;

                if (!root.RoomTypes.ContainsKey(r.RoomId))
                {
                    //We cant handle minchildren rooms so toss em all away
                    //if (contract.RoomTypes[roomTypeInKey].MinChildren > 0) continue;

                    RoomType rType = new RoomType();

                    rType.Type_Id = r.RoomId;
                    //type='ROOM' means that price is per room so it doesnt matter how many people stay in it
                    if (minChildren == 0)
                    {
                        rType.Beds = contract.RoomTypes[roomTypeKey].MaxPaxes;
                        rType.ExtraBeds = 0;
                    }
                    else if (minChildren > 0)
                    {
                        rType.Beds = contract.RoomTypes[roomTypeKey].MaxPaxes - minChildren;
                        rType.ExtraBeds = minChildren;
                    }

                    if (roomDescriptions.ContainsKey(roomTypeKey))
                    {
                        rType.Description = roomDescriptions[roomTypeKey];
                    }
                    if (!string.IsNullOrEmpty(rType.Description) && settings.NonRefundableText.Any(nrf => rType.Description.Contains(nrf)))
                    {
                        rType.NonRefundable = true;
                    }

                    root.RoomTypes.Add(rType.Type_Id, rType);
                }

                //Most time intensive part of the application
                foreach (DateTime currentDate in CurrentHotel[roomTypeKey].Keys)
                {
                    //therisamatchingroom should mean that there is a matching 2+0 room, for the 2+1 minchildren 1 room
                    bool thereIsAMatchingRoom = false;
                    if (minChildren > 0 && contract.PaxTypes[PaxTypeEnum.Child].ToAge >= 11)
                    {
                        foreach (string compRoomType in CurrentHotel.Keys)
                        {
                            if (contract.RoomTypes.ContainsKey(roomTypeKey))
                            {
                                if (contract.RoomTypes[roomTypeKey].CompatibleRoomTypes != null)
                                {
                                    if (contract.RoomTypes[roomTypeKey].CompatibleRoomTypes.Contains(compRoomType))
                                    {
                                        foreach (DateTime compDate in CurrentHotel[compRoomType].Keys)
                                        {
                                            if (thereIsAMatchingRoom) break;
                                            if (compDate.CompareTo(currentDate) == 0)
                                            {
                                                thereIsAMatchingRoom = true;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    //continue if there are no compatible rooms for a room with minchildren
                    if (minChildren > 0 && thereIsAMatchingRoom == false) continue;


                    if (h.RoomGroups[h.Id].Rooms.ContainsKey(roomId) && h.RoomGroups[h.Id].Rooms[roomId].AvailabilityPeriods.ContainsKey(currentDate)) continue;

                    AvailabilityPeriod currentPeriod = new AvailabilityPeriod();
                    currentPeriod.DateFrom = currentDate;
                    currentPeriod.DateTo = currentDate;

                    string[] rTypeParts = r.TypeId.Split('.');
                    //Gets the availablility for that room on that date
					foreach (StockNodeFilter stockNode in contract.OpenedStock)
                    {
                        //If there is a stock date that matches the date sent in and that particular date isnt dissalowed and contracts arent closed for sale 
						if ((stockNode.MatchDate(currentDate)) && stockNode.MatchWeekDay(currentDate.DayOfWeek))
                        {
							if ((stockNode.roomtypes == null) || stockNode.roomtypes.Contains(rTypeParts[1]))
							{
								currentPeriod.AvailabilityAvailableUnits = (int)stockNode.AvailableUnits;
								break;	
							}																			
                        }
                    }

                    MinStay minStayNode = contract.getMinStayReject(currentDate);
                    if (minStayNode != null)
                    {
                        currentPeriod.MinimumStay = (int)minStayNode.Nights;
                    }



                    CheckInRelease checkInRelease = null;
                    StayRelease stayRelease = null;
                    //Gets releasedays for that room on that date

                    foreach (CheckInRelease checkinReleaseNode in contract.CheckInReleases)
                    {
                        //If there is a checkinRelease date that matches the date sent in and that particular date  
                        if (checkinReleaseNode.Match(currentDate, roomTypeKey))
                        {
                            checkInRelease = checkinReleaseNode;

                        }

                    }


                    foreach (StayRelease stayReleaseNode in contract.StayReleases)
                    {
                        if (stayReleaseNode.Match(currentDate, roomTypeKey))
                        {
                            stayRelease = stayReleaseNode;

                        }
                    }

                    if (checkInRelease != null && stayRelease == null)
                    {
                        currentPeriod.ReleaseDays = (int)checkInRelease.Release;
                    }
                    else if (checkInRelease == null && stayRelease != null)
                    {
                        currentPeriod.ReleaseDays = (int)stayRelease.Release;
                    }
                    else if (checkInRelease != null && stayRelease != null)
                    {
                        if (checkInRelease.Release >= stayRelease.Release)
                        {
                            currentPeriod.ReleaseDays = (int)checkInRelease.Release;
                        }
                        else
                        {
                            currentPeriod.ReleaseDays = (int)stayRelease.Release;
                        }
                    }

                    CompulsoryStay compulsoryStay = null;
                    foreach (CompulsoryStay compulsoryStayNode in contract.CompulsoryStays)
                    {
                        if (compulsoryStayNode.Match(currentDate, roomTypeKey))
                        {
                            compulsoryStay = compulsoryStayNode;
                        }
                    }


                    foreach (CheckinDays checkInDay in contract.CheckInDays)
                    {
                        if (checkInDay.Match(currentDate, roomTypeKey))
                        {
                            if (checkInDay.Weekdays != String.Empty && checkInDay.Weekdays.Contains("."))
                            {
                                currentPeriod.IsWeeklyStay = true;
                                if (checkInDay.MatchWeekDay(currentDate.DayOfWeek) == 'X')
                                {
                                    currentPeriod.IsArrivalPossible = true;
                                }

                            }
                        }
                    }

                    if (compulsoryStay != null)
                    {
                        currentPeriod.MinimumStay = compulsoryStay.Nights;
                    }

                    //This condition removes all dates where releasedays is 0
                    //if (currentPeriod.ReleaseDays == 0)
                    //{
                    //    //Go on to the next date
                    //    continue;
                    //}

                    if (cancelPoliciesIncluded)
                    {
                        if (cancelTermsDictionary.ContainsKey(currentContractId))
                        {
                            foreach (CancellationTerms cTerm in cancelTermsDictionary[currentContractId])
                            {
                                if (cTerm.Over != null)
                                {
                                    if (!cTerm.Match(currentPeriod.DateFrom, roomTypeKey) || !(cTerm.WeekDayBased == false || cTerm.WeekDays[Convert.ToInt32(currentPeriod.DateFrom.DayOfWeek)]))
                                        continue;
                                    //currentPeriod.ReleaseDays =(int)cTerm.Release;

                                    CancellationPolicy canPol = new CancellationPolicy();
                                    if (cTerm.Percent > 0)
                                    {
                                        canPol.Id = string.Format("{0}.{1}.1", cTerm.Percent, cTerm.Release);
                                        canPol.Penalty_Value = cTerm.Percent;
                                        canPol.Deadline_Basis = "arrival";
                                        canPol.Deadline_Unit = "hours";
                                        canPol.Deadline_Value = cTerm.Release * 24;
                                        canPol.Penalty_Basis = "full_stay";
                                    }
                                    else if (cTerm.Amount > 0)
                                    {                                        
                                        canPol.Id = string.Format("{0}.{1}.2", cTerm.Amount, cTerm.Release);
                                        canPol.Penalty_Value = cTerm.Amount;
                                        canPol.Deadline_Basis = "arrival";
                                        canPol.Deadline_Unit = "hours";
                                        canPol.Deadline_Value = cTerm.Release * 24;
                                        canPol.Penalty_Basis = "fixed_amount";
                                    }
                                    else if (cTerm.Days > 0)
                                    {
                                        canPol.Id = string.Format("{0}.{1}.3", cTerm.Days, cTerm.Release);
                                        canPol.Penalty_Value = cTerm.Days;
                                        canPol.Deadline_Basis = "arrival";
                                        canPol.Deadline_Unit = "hours";
                                        canPol.Deadline_Value = cTerm.Release * 24;
                                        canPol.Penalty_Basis = "nights";
                                    }
                                    else
                                    {
                                        throw new Exception(String.Format("Cancellationterms for hotel {0} did not set amount, percent or days.", h.Id));
                                    }


                                    if (cTerm.Over != "BOOKING")
                                    {
                                        canPol.Penalty_Value = cTerm.Percent * this.beds + extraBeds;
                                    }

                                    if (!currentPeriod.CancellationPolicies.Contains(canPol.Id))
                                    {
                                        currentPeriod.CancellationPolicies.Add(canPol.Id);
                                    }

                                    if (!root.CancellationsPolicies.ContainsKey(canPol.Id))
                                    {
                                        root.CancellationsPolicies.Add(canPol.Id, canPol);
                                    }
                                }
                            }
                        }
                    }

                    // If unhandled supplements are encountered, return false.
                    if (getPrice(h, roomTypeKey, currentDate, minAdults, minChildren, currentPeriod, contract))
                    {
                        //If the period-date is new add it, otherwise modify the period
                        if (!r.AvailabilityPeriods.ContainsKey(currentDate))
                        {
                            r.AvailabilityPeriods.Add(currentDate, currentPeriod);
                        }
                        else
                        {
                            if (currentPeriod.Discounts != null)
                            {
                                int tempunits = r.AvailabilityPeriods[currentDate].AvailabilityAvailableUnits;
                                r.AvailabilityPeriods[currentDate] = currentPeriod;
                                r.AvailabilityPeriods[currentDate].AvailabilityAvailableUnits = tempunits;
                            }
                            else
                            {
                                int tempunits = r.AvailabilityPeriods[currentDate].AvailabilityAvailableUnits;
                                currentPeriod.Discounts = r.AvailabilityPeriods[currentDate].Discounts;
                                r.AvailabilityPeriods[currentDate] = currentPeriod;
                                r.AvailabilityPeriods[currentDate].AvailabilityAvailableUnits = tempunits;
                            }
                        }

                        if (!h.RoomGroups[h.Id].Rooms.ContainsKey(r.RoomId))
                        {
                            h.RoomGroups[h.Id].Rooms.Add(r.RoomId, r);
                        }
                    }
                    else
                    {
                        string listKey = roomId;
                        if (!logBlock.Contains(listKey))
                        {
                            log.WriteLine(string.Format("Availability Hotel:{0} RoomType:{1} dropped due to unhandled supplements.", h.Id, roomTypeKey));
                            logBlock.Add(listKey);
                        }
                    }
                }
            }

        }

        /// <summary>
        ///  /// Gets the price for the rooms according to their mealtypes
        /// </summary>
        /// <param name="h"></param>
        /// <param name="roomTypeKey"></param>
        /// <param name="currentDate"></param>
        /// <param name="minAdults"></param>
        /// <param name="minChildren"></param>
        /// <param name="currentPeriod"></param>
        /// <param name="contract"></param>
        /// <param name="room"></param>
        private bool getPrice(Hotel h, string roomTypeKey, DateTime currentDate, int minAdults, int minChildren, AvailabilityPeriod currentPeriod, JumboToursContract contract)
        {
            decimal cheapestRoomPrice = 0;
            string cheapestMeal = "";
            bool hasMinStayDiscount = false;
            decimal worstDiscountPercent = 0;
            //decimal priceExtraBedChild = 0;

            Dictionary<string, Dictionary<DateTime, decimal>> childDiscounts = new Dictionary<string, Dictionary<DateTime, decimal>>();

            // Create array with supplements for current criteria.
            JumboToursSupplement[] supplements = contract.Supplements.Where(s => currentDate >= s.StartDate
                && currentDate <= s.EndDate
                && (s.RoomType == null || roomTypeKey == s.RoomType.Code)).ToArray();

            if (roomsWithMinStaySupplement.ContainsKey(roomTypeKey))
            {
                hasMinStayDiscount = true;
            }

            List<MinStay> constraintList = contract.getMinStaySupplement(currentDate);

            //  Get the list of supplements that are sorted by minstay nights
            List<Discount> minStayDiscounts = new List<Discount>();

            for (int supplementCounter = constraintList.Count - 1; supplementCounter >= 0; supplementCounter--)
            {
                MinStay currentSupplement = constraintList[supplementCounter];

                if (currentSupplement.Nights == 0)
                {
                    throw new Exception("minstay 0!!!");
                }

                int discountPercent = currentSupplement.Percent;
                int nextDiscountPercent = 0;

                if (worstDiscountPercent == 0)
                {
                    worstDiscountPercent = discountPercent;
                }

                // Do we have another supplement that have will make another discount
                if (supplementCounter != 0)
                {
                    nextDiscountPercent = constraintList[supplementCounter - 1].Percent;
                }

                // The worst discount - the next discount / the worst discount increse
                decimal minStayDiscountPercent = (((decimal)worstDiscountPercent - (decimal)nextDiscountPercent) / (100 + (decimal)worstDiscountPercent)) * 100;

                //No point in doing anything if the discount is 0.
                if ((int)minStayDiscountPercent > 0)
                {
                    // Add it as a discount, dont forget to raise the price to get this correct.
                    currentPeriod.Discounts.TryAdd(Discount.ValuePercent(currentSupplement.Nights, (int)minStayDiscountPercent));
                    hasMinStayDiscount = true;
                }
            }

            if (CurrentHotel[roomTypeKey][currentDate].Keys.Count > 1)
            {
                string[] keyArr = new string[CurrentHotel[roomTypeKey][currentDate].Keys.Count];
                int mealCounter = 0;
                foreach (string meal in CurrentHotel[roomTypeKey][currentDate].Keys)
                {
                    keyArr[mealCounter] = meal;
                    mealCounter++;
                }

                //Sort the keys
                for (int i = (CurrentHotel[roomTypeKey][currentDate].Keys.Count - 1); i >= 0; i--)
                {
                    for (int j = 1; j <= i; j++)
                    {
                        if (keyArr[j - 1].CompareTo(keyArr[j]) > 0)
                        {
                            string temp = keyArr[j - 1];
                            keyArr[j - 1] = keyArr[j];
                            keyArr[j] = temp;
                        }
                    }
                }


                Dictionary<string, JumboToursContractPriceManager> currentHotelCopy = new Dictionary<string, JumboToursContractPriceManager>(CurrentHotel[roomTypeKey][currentDate].Count());

                foreach (string meal in CurrentHotel[roomTypeKey][currentDate].Keys)
                {
                    currentHotelCopy.Add(meal, CurrentHotel[roomTypeKey][currentDate][meal]);
                }
                CurrentHotel[roomTypeKey][currentDate].Clear();

                foreach (string meal in keyArr)
                {
                    CurrentHotel[roomTypeKey][currentDate].Add(meal, currentHotelCopy[meal]);
                }
            }
			
            foreach (string meal in CurrentHotel[roomTypeKey][currentDate].Keys.ToArray())
            {			
				if (!contract.OpenedStock.Any(os => os.MatchDate(currentDate) && os.boards.Contains(meal) && os.roomtypes.Contains(roomTypeKey)))
				{
					continue;
				}
                decimal supplementCost = 0;
                decimal supplementRate = 1;

                if (h.Id == "1386" && roomTypeKey == "QB2" && currentDate == DateTime.Parse("2011-08-12"))
                {

                }

                foreach (JumboToursSupplement supplement in supplements.Where(s => s.BoardType == null || s.BoardType == meal))
                {
                    decimal amount = XmlHelper.ConvertCurrency(supplement.Amount, supplement.Currency, "EUR");

                    if (supplement.Order == 0 && settings.Supplement.MaxPercent > 0)
                    {
                        if (supplement.Percent <= settings.Supplement.MaxPercent)
                        {
                            supplementRate += (supplement.Percent / 100);
                        }
                        else
                        {
                            // Return false if period contains unhadled supplements.
                            return false;
                        }
                    }
                    else if (supplement.Order == 1)
                    {
                        int maxAdults = contract.RoomTypes[roomTypeKey].MaxPaxes - contract.RoomTypes[roomTypeKey].MinChildren;
                        int maxChildren = contract.RoomTypes[roomTypeKey].MaxPaxes - contract.RoomTypes[roomTypeKey].MinAdults;

                        if (supplement.PaxType.Code == PaxTypeEnum.All)
                        {
                            supplementCost += amount * (decimal)(maxAdults + maxChildren);
                            supplementCost += supplement.GetPaxbasedSupplement(maxAdults, CurrentHotel[roomTypeKey][currentDate][meal].PricePerAdult);
                            supplementCost += supplement.GetPaxbasedSupplement(maxChildren, CurrentHotel[roomTypeKey][currentDate][meal].PricePerChild);
                        }
                        else if (supplement.PaxType.Code == PaxTypeEnum.Adult)
                        {
                            supplementCost += amount * (decimal)maxAdults;
                            supplementCost += supplement.GetPaxbasedSupplement(maxAdults, CurrentHotel[roomTypeKey][currentDate][meal].PricePerAdult);
                        }
                        else if (supplement.PaxType.Code == PaxTypeEnum.Child)
                        {
                            supplementCost += amount * (decimal)maxChildren;
                            supplementCost += supplement.GetPaxbasedSupplement(maxChildren, CurrentHotel[roomTypeKey][currentDate][meal].PricePerChild);
                        } 
                    }
                    else if (supplement.Order == 2)
                    {
                        supplementCost += (decimal)XmlHelper.ConvertCurrency(amount, supplement.Currency, "EUR");
                    }
                    else
                    {
                        // Return false if period contains unhadled supplements.
                        return false;
                    }
                }



                if (supplementCost < 0)
                {
                    supplementCost = 0;
                }
                if (supplementRate < 0) supplementRate = 0;

                CurrentHotel[roomTypeKey][currentDate][meal].SupplementCost = ((CurrentHotel[roomTypeKey][currentDate][meal].Amount + (decimal)supplementCost) * (decimal)supplementRate) - CurrentHotel[roomTypeKey][currentDate][meal].Amount;

                decimal mealPrice = (decimal)CurrentHotel[roomTypeKey][currentDate][meal].TotalPrice;

                if (h.Id == "4377" )// && roomTypeKey == "QB2")
            {

            }

                //If the meal is the first meal or the cheapest meal, set it to cheapest meal
                if (mealPrice < cheapestRoomPrice || (mealPrice == cheapestRoomPrice && mealValue(meal) > mealValue(cheapestMeal)) || (cheapestMeal == "" && cheapestRoomPrice == 0))
                {
                    cheapestRoomPrice = mealPrice;
                    cheapestMeal = meal;
                }
            }

            //If the room has minstaydiscount modify the price
            if (hasMinStayDiscount)
            {
                cheapestRoomPrice = cheapestRoomPrice * (1 + (worstDiscountPercent / 100));
            }

            beds = contract.RoomTypes[roomTypeKey].MinPaxes;
            extraBeds = contract.RoomTypes[roomTypeKey].MaxPaxes - beds;

            foreach (string meal in CurrentHotel[roomTypeKey][currentDate].Keys)
            {
				if (!contract.OpenedStock.Any(os => os.MatchDate(currentDate) && os.boards.Contains(meal)))
				{
					continue;
				}

                if (meal != cheapestMeal)
                {
                    decimal mealPrice = 0;

                    mealPrice = CurrentHotel[roomTypeKey][currentDate][meal].TotalPrice;

                    //If the room has minstaydiscount modify the meal price
                    if (hasMinStayDiscount)
                    {
                        mealPrice = mealPrice * (1 + (worstDiscountPercent / 100));
                    }

                    //Spread the added meal cost evenly on all beds
                    decimal priceDiff = mealPrice - cheapestRoomPrice;
                    priceDiff = Math.Round(priceDiff, 2);

                    decimal finalAddedMealPrice = Math.Round(priceDiff / (beds + extraBeds), 2);

                    if (getMealDesc(meal) != "none")
                    {
                        // Removed additional boards since they give us the wrong price at booking time.
                        //currentPeriod.AdditionalBoards.Add(new AvailabilityPeriod.AdditionalBoard(getMealDesc(meal), finalAddedMealPrice.ToString(culture), finalAddedMealPrice.ToString(culture), meal));
                    }
                }
            }          
			if(string.IsNullOrEmpty(cheapestMeal)) return false;
            decimal totalPrice = CurrentHotel[roomTypeKey][currentDate][cheapestMeal].TotalPrice;
            
            if (hasMinStayDiscount)
            {
                totalPrice = totalPrice * (1 + (worstDiscountPercent / 100));
            }

            childDiscounts = contract.getChildDiscounts(currentPeriod, roomTypeKey, currentContractId, log, h.RoomGroups[h.Id].Rooms, cheapestMeal, CurrentHotel[roomTypeKey][currentPeriod.DateFrom][cheapestMeal]); 
            if (childDiscounts.ContainsKey(roomTypeKey) && childDiscounts[roomTypeKey].ContainsKey(currentDate) && supplements.Count() == 0)
            {
                decimal childDiscount = (CurrentHotel[roomTypeKey][currentDate][cheapestMeal].PricePerChild * ((decimal)childDiscounts[roomTypeKey][currentDate] / 100));
                totalPrice -= childDiscount;

                if (totalPrice < 0)
                {

                }
            }            

            currentPeriod.Pricing_BasePrice = totalPrice.ToString(culture);//Needed to make the decimal in correct usFormat
            currentPeriod.Pricing_BookableBoardId = cheapestMeal.ToString(culture);
            currentPeriod.Pricing_BaseIncludedBoard = getMealDesc(cheapestMeal.ToString(culture));

            currentPeriod.AdditionalBoards.RemoveRedundantMeals(currentPeriod.Pricing_BaseIncludedBoard);

            // Discounts will not apply to the supplement part of the price.
            // If a supplement is applicable for this date on any meals we need to skip adding discounts.
            // TODO: compatibleOffer can be used to determine if discounts can be combined with supplements (this does not seem to be used).
//            if (supplements.Count() == 0)
            {
                contract.getDiscounts(currentPeriod, roomTypeKey, currentContractId, log, h.RoomGroups[h.Id].Rooms, cheapestMeal, CurrentHotel[roomTypeKey][currentPeriod.DateFrom][cheapestMeal]);
            }
            return true;
        }

        private int mealValue(string meal)
        {
            switch (meal)
            {
                case "RO":
                    return 1;
                case "SC":
                    return 2;
                case "BB":
                    return 3;
                case "HB":
                    return 4;
                case "FB":
                    return 5;
                case "AI":
                    return 6;
                default:
                    return 0;
            }
        }

        protected override void getProviderDefinition(Root root)
        {
            root.ProviderDefinition.Name = configuration.ProviderName;
            root.ProviderDefinition.Currency = "EUR";
        }

        /// <summary>
        /// Converts the JumboTours meals to correct SunHotels meals
        /// </summary>
        /// <param name="meal">The JumboTours meal name</param>
        /// <returns>The sunhotels translation of the JumboTours meal</returns>
        public string getMealDesc(string meal)
        {
            string mealDesc = "";
            switch (meal)
            {
                case "BB": mealDesc = "breakfast"; break;//Bed&Breakfast
                case "HB": mealDesc = "half_board"; break;//Haflboard
                case "FB": mealDesc = "full_board"; break;//Fullboard
                case "AI": mealDesc = "all_inclusive"; break;//All Inclusive
                case "SAI": mealDesc = "none"; break;//Soft All Inclusive
                case "SC": mealDesc = "none"; break;//Self Catering
                case "RO": mealDesc = "none"; break;//Room only

                default: throw new Exception(String.Format("No meal declared? ({0})", meal));
            }

            return mealDesc;
        }

        /// <summary>
        /// Decodes a string using System.Text.UTF8Encoding.
        /// </summary>
        /// <param name="s"></param>
        /// <returns>A decoded string</returns>
        public static string UTF8Decode(string s)
        {
            if (!String.IsNullOrEmpty(s))
            {
                byte[] buf = System.Text.Encoding.GetEncoding("iso-8859-1").GetBytes(s);
                return System.Text.Encoding.UTF8.GetString(buf);
            }
            else
            {
                return String.Empty;
            }
        }

        /// <summary>
        /// Unzip specified ZIP file content to specified target directory.
        /// </summary>
        /// <param name="sFile">The path to the zipfile.</param>
        /// <param name="sTargetDirectory">The directory used to save the extracted zipfile.</param>
        /// <returns>The list of unziped files.</returns>
        private List<string> UnZip(string sFile, string sTargetDirectory)
        {
            List<string> listFiles = new List<string>();

            using (ZipInputStream s = new ZipInputStream(File.OpenRead(sFile)))
            {
                try
                {
                    ZipEntry theEntry;
                    while ((theEntry = s.GetNextEntry()) != null)
                    {
                        string directoryName = sTargetDirectory;
                        string fileName = Path.GetFileName(theEntry.Name);

                        if (fileName != String.Empty)
                        {
                            using (FileStream streamWriter = File.Create(directoryName + theEntry.Name))
                            {
                                listFiles.Add(streamWriter.Name);

                                int size = 2048;
                                byte[] data = new byte[2048];
                                while (true)
                                {
                                    size = s.Read(data, 0, data.Length);
                                    if (size > 0)
                                    {
                                        streamWriter.Write(data, 0, size);
                                    }
                                    else
                                    {
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new ApplicationException(string.Format("Unable to unzip file {0} to directory {1}", sFile, sTargetDirectory), ex);
                }
            }
            return listFiles;
        }


		public void getRoomsDesc(string file)
        {
            var xmlDocRoomsDesc = XDocument.Load(file);

            var typeList = xmlDocRoomsDesc.Descendants(XName.Get("roomTypes"));
            foreach (var roomTypeNode in typeList)
            {
                if (roomTypeNode.Element("code") != null
                    && roomTypeNode.Element("name") != null)
                {
                    var code = roomTypeNode.Element("code").Value;
                    var name = roomTypeNode.Element("name").Value;
                    if (!roomDescriptions.ContainsKey(code))
                    {
                        roomDescriptions.Add(code, name);
                    }
                }
            }
        }

        public static void VerifyAttributeExists(XElement constraintNode, string attributeName, string errorMessageAddon)
        {
            if (constraintNode.Attribute(attributeName) == null)
            {
                throw new MissingElementAttributeException(String.Format("Attribute name:{0}, {1}", attributeName,
                                                                         errorMessageAddon));
            }
        }

        public static decimal DecimalParse(string s, IFormatProvider provider, bool throwException = true)
        {
            if (string.IsNullOrEmpty(s)) return 0m;

            decimal output = 0;
            if (!Decimal.TryParse(s, NumberStyles.Any, provider, out output) && throwException)
                throw new Exception("Cannot convert to decimal: " + s);
            return output;
        }
    }

    /// <summary>
    /// The exception that is thrown when a minstay value is set above 100.
    /// </summary>
    public class MinstayMaxLimitExceededException : ApplicationException
    {
        /// <summary>
        /// Initializes a new, empty instance of the MinstayMaxLimitExceededException class.
        /// </summary>
        public MinstayMaxLimitExceededException() : base() { }

    }

    /// <summary>
    /// Exception to use when an element is missing an attribute. 
    /// </summary>
    public class MissingElementAttributeException : ApplicationException
    {
        public MissingElementAttributeException() : base() { }
        public MissingElementAttributeException(string message) : base(message) { }
    }
}
