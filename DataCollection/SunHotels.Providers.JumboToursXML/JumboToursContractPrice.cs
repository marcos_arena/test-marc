using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using SunHotels.XML;
using SunHotels.XML.Data;
using System.Globalization;
using SunHotels.XML.HelpClasses;
using System.Xml.Linq;
using System.Linq;

namespace SunHotels.Providers
{
    public class ContractPrice
    {
        //<price stayFrom="29/04/2008" stayTo="09/06/2008" weekdays="XXXXXXX" nights="1" amount="33.92" feepax="0.0" feebooking="0.00" currency="EUR" type="ROOM" offer="false" roomtypes="DAC,TAC,DBL" />
        protected static IFormatProvider culture = new CultureInfo("en-GB", true);
        protected static IFormatProvider usCulture = new CultureInfo("en-US", true);
        DateTime stayFrom;
        public DateTime StayFrom
        {
            get { return stayFrom; }
            set { stayFrom = value; }
        }

        DateTime stayTo;
        public DateTime StayTo
        {
            get { return stayTo; }
            set { stayTo = value; }
        }

        bool[] weekdays;
        public bool[] Weekdays
        {
            get { return weekdays; }
            set { weekdays = value; }
        }

        string weekdaysString;
        public string WeekdaysString
        {
            get { return weekdaysString; }
            set { weekdaysString = value; }
        }

        int nights;
        public int Nights
        {
            get { return nights; }
            set { nights = value; }
        }

        decimal amount;
        public decimal Amount
        {
            get { return amount; }
            set { amount = value; }
        }
     
        decimal feepax;
        public decimal Feepax
        {
            get { return feepax; }
            set { feepax = value; }
        }

        decimal feebooking;
        public decimal Feebooking
        {
            get { return feebooking; }
            set { feebooking = value; }
        }

        public decimal TotalPrice
        {
            get { return Amount + Feebooking; }
        }

        string currency;
        public string Currency
        {
            get { return currency; }
            set { currency = value; }
        }

        PriceType type;
        public PriceType Type
        {
            get { return type; }
            set { type = value; }
        }

        bool offer;
        public bool Offer
        {
            get { return offer; }
            set { offer = value; }
        }

        string roomtypes;
        public string Roomtypes
        {
            get { return roomtypes; }
            set { roomtypes = value; }
        }

        string boardtypes;
        public string Boardtypes
        {
            get { return boardtypes; }
            set { boardtypes = value; }
        }

        List<PaxTypeEnum> paxtypes;
        public List<PaxTypeEnum> Paxtypes
        {
            get { return paxtypes; }
            set { paxtypes = value; }
        }

        decimal pricePerAdult;
        public decimal PricePerAdult
        {
            get { return pricePerAdult; }
            set { pricePerAdult = value; }
        }

        decimal pricePerChild;
        public decimal PricePerChild
        {
            get { return pricePerChild; }
            set { pricePerChild = value; }
        }


        public ContractPrice(XElement contractPriceNode, ContractRoomType roomtype, string boardtype)
        {
            this.stayFrom = DateTime.Parse(contractPriceNode.Attribute("stayFrom").Value, culture);
            this.stayTo = DateTime.Parse(contractPriceNode.Attribute("stayTo").Value, culture);

            string weekDaysTemp = contractPriceNode.Attribute("weekdays").Value;
            weekdaysString = weekDaysTemp;

            this.weekdays = new bool[weekDaysTemp.Length];
            int dayCount = 0;

            foreach (char day in weekDaysTemp)
            {
                if (day == 'X')
                {
                    this.weekdays[dayCount] = true;
                }
                dayCount++;
            }

            this.nights = int.Parse(contractPriceNode.Attribute("nights").Value);
            if (this.nights == 0) throw new Exception("0 nights req?");
            this.amount = JumboToursXML.DecimalParse(contractPriceNode.Attribute("amount").Value, usCulture);//We need the decimals in us format

            if (this.amount == 0) throw new Exception("Doesnt a totally free room sound a bit too good to be true?");

            //If a room has more than 1 declared as nights that means thats the price for that amount of days
            if (this.nights > 1)
            {
                this.amount = this.amount / this.nights;
            }

            this.feepax = JumboToursXML.DecimalParse(contractPriceNode.Attribute("feepax").Value, usCulture);
            this.feebooking = JumboToursXML.DecimalParse(contractPriceNode.Attribute("feebooking").Value, usCulture);
            this.currency = contractPriceNode.Attribute("currency").Value;


            if (this.currency != "EUR")
            {
                //Lets convert currencyvalues to euros to get correct end result
                this.amount = (decimal)XmlHelper.ConvertCurrency((decimal)this.amount, this.currency, "EUR");
                this.feepax = (decimal)XmlHelper.ConvertCurrency((decimal)this.feepax, this.currency, "EUR");
                this.feebooking = (decimal)XmlHelper.ConvertCurrency((decimal)this.feebooking, this.currency, "EUR");
                this.currency = "EUR";
            }

            //If paxtypes aren't declared all paxtypes are applicable to the price
            if (contractPriceNode.Attribute("paxtypes") == null)
            {
                this.paxtypes = contractPriceNode.Parent.Parent.Element("paxtypes").Elements("paxtype").Select(pt => PaxType.Parse(pt.Attribute("code").Value)).ToList();
            }
            else
            {
                this.paxtypes = contractPriceNode.Attribute("paxtypes").Value.Split(',').Select(p => PaxType.Parse(p)).ToList();
            }

            this.type = (PriceType)Enum.Parse(typeof(PriceType), contractPriceNode.Attribute("type").Value);

            int adults = (roomtype.MaxPaxes - roomtype.MinChildren);
            int children = roomtype.MinChildren;

            if (roomtype.MinChildren > 0 && this.type == PriceType.ROOM)
            {
                this.pricePerChild = (this.amount/roomtype.MaxPaxes) + this.feepax;
                this.PricePerAdult = (this.amount / roomtype.MaxPaxes) + this.feepax;
            }
            if (this.type == PriceType.PAX && children > 0)
            {

                decimal totalAdultPrice = 0;
                decimal totalChildPrice = 0;
                if (this.paxtypes.Contains(PaxTypeEnum.Child) || this.paxtypes == null)
                {
                    this.pricePerChild = this.amount + this.feepax;
                    totalChildPrice = this.pricePerChild * children;
                }
                if (this.paxtypes.Contains(PaxTypeEnum.Adult) || this.paxtypes == null)
                {
                    this.pricePerAdult = this.amount + this.feepax;
                    totalAdultPrice = this.pricePerAdult * adults;
                    if (this.pricePerAdult == 0) throw new Exception("Doesnt a totally free room sound a bit too good to be true?");
                }

                this.amount = totalAdultPrice + totalChildPrice; 
            }
            else if(this.type == PriceType.PAX)
            {
                //this.type = "ROOM";  // Allways use room in sunhotels sytem.
                this.pricePerAdult = this.amount + this.feepax;
                this.amount = (this.amount + this.feepax) * roomtype.MaxPaxes;
                if (this.pricePerAdult == 0) throw new Exception("Doesnt a totally free room sound a bit too good to be true?");
            }
            
            if (this.amount == 0) throw new Exception("Doesnt a totally free room sound a bit too good to be true?");
            
            if (contractPriceNode.Attribute("offer").Value == "true")
            {
                this.offer = true;
            }
            else
            {
                this.offer = false;
            }

            this.roomtypes = roomtype.Code;
            this.boardtypes = boardtype;
        }

        //Checks if a date is within a staying period of the price
        public bool MatchDate(DateTime date)
        {
            if (date.CompareTo(stayFrom) >= 0 && date.CompareTo(stayTo) <= 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool MatchWeekDay(DayOfWeek day)
        {
            int weekday = 0;
            switch (day)
            {
                case DayOfWeek.Sunday: weekday = 0; break;
                case DayOfWeek.Monday: weekday = 1; break;
                case DayOfWeek.Tuesday: weekday = 2; break;
                case DayOfWeek.Wednesday: weekday = 3; break;
                case DayOfWeek.Thursday: weekday = 4; break;
                case DayOfWeek.Friday: weekday = 5; break;
                case DayOfWeek.Saturday: weekday = 6; break;
            }

            return this.weekdays[weekday];
        }

        public ContractPrice()
        {
        }

        internal ContractPrice Clone()
        {
            return (ContractPrice)this.MemberwiseClone();
        }
    }

    public enum PriceType { Undefined, PAX, ROOM }
}
