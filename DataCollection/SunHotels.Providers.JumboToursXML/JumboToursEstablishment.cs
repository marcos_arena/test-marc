using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using SunHotels.XML.Data;
using System.Xml.Linq;

namespace SunHotels.Providers
{
    class JumboToursEstablishment
    {
        string id;
        public string Id
        {
            get { return id; }
            set { id = value; }
        }
        
        string name;
        public string Name
        {
            get { return name; }
            set { name = value; }
        }
        
        string categoryCode;
        public string CategoryCode
        {
            get { return categoryCode; }
            set { categoryCode = value; }
        }
        
        string categoryName;
        public string CategoryName
        {
            get { return categoryName; }
            set { categoryName = value; }
        }
        
        string latitude;
        public string Latitude
        {
            get { return latitude; }
            set { latitude = value; }
        }
        
        string longitude;
        public string Longitude
        {
            get { return longitude; }
            set { longitude = value; }
        }
        
        string shortDescription;
        public string ShortDescription
        {
            get { return shortDescription; }
            set { shortDescription = value; }
        }
        
        string longDescription;
        public string LongDescription
        {
            get { return longDescription; }
            set { longDescription = value; }
        }
        
        string typeCode;
        public string TypeCode
        {
            get { return typeCode; }
            set { typeCode = value; }
        }
        
        string typeName;
        public string TypeName
        {
            get { return typeName; }
            set { typeName = value; }
        }
        
        string address;
        public string Address
        {
            get { return address; }
            set { address = value; }
        }
        
        string airports;
        public string Airports
        {
            get { return airports; }
            set { airports = value; }
        }
        
        string cityCode;
        public string CityCode
        {
            get { return cityCode; }
            set { cityCode = value; }
        }
        
        string cityName;
        public string CityName
        {
            get { return cityName; }
            set { cityName = value; }
        }
        
        string countryCode;
        public string CountryCode
        {
            get { return countryCode; }
            set { countryCode = value; }
        }
        
        string countryName;
        public string CountryName
        {
            get { return countryName; }
            set { countryName = value; }
        }
        
        string email;
        public string Email
        {
            get { return email; }
            set { email = value; }
        }
        
        string fax;
        public string Fax
        {
            get { return fax; }
            set { fax = value; }
        }
        
        string stateCode;
        public string StateCode
        {
            get { return stateCode; }
            set { stateCode = value; }
        }
        
        string stateName;
        public string StateName
        {
            get { return stateName; }
            set { stateName = value; }
        }
        
        string telephone;
        public string Telephone
        {
            get { return telephone; }
            set { telephone = value; }
        }
        
        string zipCode;
        public string ZipCode
        {
            get { return zipCode; }
            set { zipCode = value; }
        }
        
        string zoneName;
        public string ZoneName
        {
            get { return zoneName; }
            set { zoneName = value; }
        }
        
        string image_1;
        public string Image_1
        {
            get { return image_1; }
            set { image_1 = value; }
        }
        
        string image_2;
        public string Image_2
        {
            get { return image_2; }
            set { image_2 = value; }
        }
        
        string image_3;
        public string Image_3
        {
            get { return image_3; }
            set { image_3 = value; }
        }
        
        List<string> features;
        public List<string> Features
        {
            get { return features; }
            set { features = value; }
        }

        List<Hotel.Distance> hotelDistances = new List<Hotel.Distance>();
        public List<Hotel.Distance> HotelDistances
        {
            get { return hotelDistances; }
            set { hotelDistances = value; }
        }

        public JumboToursEstablishment(XElement xmlEstablishment)
        {

            this.id = xmlEstablishment.Element("id").Value;
            this.name = xmlEstablishment.Element("name").Value;
            this.categoryCode = xmlEstablishment.Element("categoryCode").Value;
            this.categoryName = xmlEstablishment.Element("categoryName").Value;
            this.latitude = xmlEstablishment.Element("latitude").Value;
            this.longitude = xmlEstablishment.Element("longitude").Value;
            this.shortDescription = xmlEstablishment.Element("shortDescription").Value;
            this.longDescription = xmlEstablishment.Element("longDescription").Value;
            this.typeCode = xmlEstablishment.Element("typeCode").Value;
            this.typeName = xmlEstablishment.Element("typeName").Value;
             //<Address>Theese are parts of the adress node 
            XElement xAdress = xmlEstablishment.Element("Address");
            this.address = xAdress.Element("address").Value;
            this.airports = xAdress.Element("airports").Value;
            this.cityCode = xAdress.Element("cityCode").Value;
            this.cityName = xAdress.Element("cityName").Value;
            this.countryCode = xAdress.Element("countryCode").Value;
            this.countryName = xAdress.Element("countryName").Value;
            this.email = xAdress.Element("email").Value;
            this.fax = xAdress.Element("fax").Value;
            this.stateCode = xAdress.Element("stateCode").Value;
            this.stateName = xAdress.Element("stateName").Value;
            this.telephone = xAdress.Element("telephone").Value;
            this.zipCode = xAdress.Element("zipCode").Value;
            this.zoneName = xAdress.Element("zoneName").Value;
            //</Address>
            this.features = new List<string>();

            if (xmlEstablishment.Element("Image_1") != null)
            {
                this.image_1 = xmlEstablishment.Element("Image_1").Element("uri").Value;
            }
            if (xmlEstablishment.Element("Image_2") != null)
            {
                this.image_2 = xmlEstablishment.Element("Image_2").Element("uri").Value;
            }
            if (xmlEstablishment.Element("Image_3") != null)
            {
                this.image_3 = xmlEstablishment.Element("Image_3").Element("uri").Value;
            }
   
            //Distances are measured in kilometres
            if (xmlEstablishment.Element("Property_Distance_to_town_centre") != null)
            {
                string distanceToTownCentre = xmlEstablishment.Element("Property_Distance_to_town_centre").Element("name").Value;
                ConvertToDistance("center", distanceToTownCentre);
            }

            if (xmlEstablishment.Element("Property_Distance_to_the_airport") != null)
            {
                string distanceToAirport = xmlEstablishment.Element("Property_Distance_to_the_airport").Element("name").Value;
                ConvertToDistance("airport", distanceToAirport);
            }

            if (xmlEstablishment.Element("Property_Distance_to_the_beach") != null)
            {
                string distanceToBeach = xmlEstablishment.Element("Property_Distance_to_the_beach").Element("name").Value;
                ConvertToDistance("beach", distanceToBeach);
            }

            AddFeature(xmlEstablishment, "Property_Air_conditioning", "airconditioning");
            AddFeature(xmlEstablishment, "Property_Restaurant", "restaurant");
            AddFeature(xmlEstablishment, "Property_Disco", "bar");
            AddFeature(xmlEstablishment, "Property_In_room_telephone", "telephone");

            bool pool = AddFeature(xmlEstablishment, "Property_Swimming_pool", "pool");
            if (!pool) AddFeature(xmlEstablishment, "Property_Indoor_pool", "pool");

            bool safetyBox = AddFeature(xmlEstablishment, "Property_Safety_box_at_reception", "safe");
            if (!safetyBox) AddFeature(xmlEstablishment, "Property_Safety_box_at_reception", "safe");

            AddFeature(xmlEstablishment, "Property_Terrace", "balcony");

            bool hasTV = AddFeature(xmlEstablishment, "Property_TV", "tv");
            if (!hasTV) hasTV = AddFeature(xmlEstablishment, "Property_Satellite_TV", "tv");
            if (!hasTV) hasTV = AddFeature(xmlEstablishment, "Property_TV__pay_per_view", "tv");         
        }

        private void ConvertToDistance(string distanceType, string distance)
        {
            if (!String.IsNullOrEmpty(distance))
            {
                decimal distanceValue;
                if (distance.ToLower().Contains("km"))
                {
                    distance = distance.Trim('k', 'm', 's');
                    if (decimal.TryParse(distance, out distanceValue) != false)
                    {
                        if (distanceValue < 0) return;
                        HotelDistances.Add(new Hotel.Distance(distanceType, distanceValue));
                    }
                }
                else if (distance.ToLower().Contains("m"))
                {
                    distance = distance.Trim('m', 'e', 't', 'r', 'o', 's');
                    if (decimal.TryParse(distance, out distanceValue) != false)
                    {
                        if (distanceValue < 0) return;
                        distanceValue = distanceValue / 1000;
                        HotelDistances.Add(new Hotel.Distance(distanceType, distanceValue));
                    }
                }
            }
        }

        private bool AddFeature(XElement xmlEstablishment, string propName, string featureName)
        {
            XElement xElem = xmlEstablishment.Element(propName);
            if (xElem != null)
            {
                if (xElem.Element("name").Value == "YES")
                {
                    Features.Add(featureName);
                    return true;
                }
            }
            return false;
        }

    }
}
