using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using SunHotels.XML.Data;
using System.Globalization;
using System.Xml.Linq;
using System.Linq;

namespace SunHotels.Providers
{
	public class StockNodeFilter
	{


		public DateTime start;
		public DateTime end;
		public IEnumerable<string> boards;

		public IEnumerable<string> roomtypes;
		private Int16 availableUnits;
		public Int16 AvailableUnits
		{
			get { return availableUnits; }
			set { availableUnits = value; }
		}

		private bool[] weekDays;
		public bool[] WeekDays
		{
			get { return weekDays; }
			set { weekDays = value; }

		}
		public bool MatchDate(DateTime date)
		{
			if (date.CompareTo(start) >= 0 && date.CompareTo(end) <= 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		public bool MatchWeekDay(DayOfWeek day)
		{
			int weekday = 0;
			switch (day)
			{
				case DayOfWeek.Sunday: weekday = 0; break;
				case DayOfWeek.Monday: weekday = 1; break;
				case DayOfWeek.Tuesday: weekday = 2; break;
				case DayOfWeek.Wednesday: weekday = 3; break;
				case DayOfWeek.Thursday: weekday = 4; break;
				case DayOfWeek.Friday: weekday = 5; break;
				case DayOfWeek.Saturday: weekday = 6; break;
			}
			
			if (this.WeekDays == null)
				this.WeekDays = new bool[] { true, true, true, true, true, true, true };
			return this.WeekDays[weekday];
		}
	}
	public class StockNode
	{
		public DateTime startDate;
		public DateTime endDate;

		private static IFormatProvider culture = new CultureInfo("en-GB", true);

		private Int16 availableUnits;
		public Int16 AvailableUnits
		{
			get { return availableUnits; }
			set { availableUnits = value; }
		}

		private bool[] weekDays;
		public bool[] WeekDays
		{
			get { return weekDays; }
			set { weekDays = value; }

		}

		private string[] roomTypes;
		public string[] RoomTypes
		{
			get { return roomTypes; }
			set { roomTypes = value; }
		}
		private string[] boardTypes;
		public string[] BoardTypes
		{
			get { return boardTypes; }
			set { boardTypes = value; }
		}

		public StockNode(string startDate, string endDate, string availableUnits, string weekDays)
		{
			this.startDate = DateTime.Parse(startDate, culture);
			this.endDate = DateTime.Parse(endDate, culture);
			this.availableUnits = Int16.Parse(availableUnits);


			this.weekDays = new bool[weekDays.Length];
			int dayCount = 0;

			foreach (char day in weekDays)
			{
				if (day == 'X')
				{
					this.weekDays[dayCount] = true;
				}
				dayCount++;
			}
		}

		public StockNode(XElement xmlStockNode, JumboToursContract contract)
		{
			this.startDate = DateTime.Parse(xmlStockNode.Attribute("stayFrom").Value, culture);
			this.endDate = DateTime.Parse(xmlStockNode.Attribute("stayTo").Value, culture);
			this.availableUnits = Int16.Parse(xmlStockNode.Attribute("available").Value);

			if (xmlStockNode.Attribute("roomtypes") != null && 
				!string.IsNullOrEmpty(xmlStockNode.Attribute("roomtypes").Value))
			{
				this.roomTypes = xmlStockNode.Attribute("roomtypes").Value.Split(new char[] { ',' });
			}
			else
			{
				this.roomTypes = contract.RoomTypes.Keys.ToArray();
			}
			if (xmlStockNode.Attribute("boardtypes") != null &&
				!string.IsNullOrEmpty(xmlStockNode.Attribute("boardtypes").Value))
			{
				this.boardTypes = xmlStockNode.Attribute("boardtypes").Value.Split(new char[] { ',' });
			}
			else
			{
				this.boardTypes = contract.Boardtypes.Split(new char[] { ',' }); 
			}

			string weekDaysTemp = xmlStockNode.Attribute("weekdays").Value;
			this.weekDays = new bool[weekDaysTemp.Length];
			int dayCount = 0;
			foreach (char day in weekDaysTemp)
			{
				if (day == 'X')
				{
					this.weekDays[dayCount] = true;
				}
				dayCount++;
			}
		}
		internal IEnumerable<StockNodeFilter> UnGroup()
		{
			/*
			 this.weekDays = new bool[weekDays.Length];
			int dayCount = 0;

			foreach (char day in weekDays)
			{
				if (day == 'X')
				{
					this.weekDays[dayCount] = true;
				}
				dayCount++;
			}
			 */
			for (var s = this.startDate; s <= this.endDate; s = s.AddDays(1))
			{
				if (this.weekDays[(int)s.DayOfWeek])
				{
					foreach (var b in this.boardTypes)
						foreach (var r in this.roomTypes)
						{
							yield return new StockNodeFilter() { start = s, end = s, boards = new string[] { b }, roomtypes = new string[] { r }, AvailableUnits = this.AvailableUnits, WeekDays = this.weekDays };
						}
				}
				
			}
		}
		/// <summary>
		/// Checks if the specified date is between the startdate and the enddate for that stocknode
		/// </summary>
		/// <param name="date"></param>
		/// <returns></returns>
		public bool MatchDate(DateTime date)
		{
			if (date.CompareTo(startDate) >= 0 && date.CompareTo(endDate) <= 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}		
	}
}
