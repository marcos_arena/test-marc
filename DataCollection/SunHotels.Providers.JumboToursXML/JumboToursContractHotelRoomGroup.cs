using System;
using System.Collections.Generic;
using System.Text;

namespace SunHotels.Providers
{
    public class ContractHotelRoomGroup
    {
        string contractId;
        public string ContractId
        {
            get { return contractId; }
            set { contractId = value; }
        }

        string hotelId;
        public string HotelId
        {
            get { return hotelId; }
            set { hotelId = value; }
        }

        string roomTypes;
        public string RoomTypes
        {
            get { return roomTypes; }
            set { roomTypes = value; }
        }

        public ContractHotelRoomGroup(string sContractId, string sHotelId, string sRoomTypes)
        {
            this.contractId = sContractId;
            this.hotelId = sHotelId;
            this.roomTypes = sRoomTypes;
        }
    }
}
