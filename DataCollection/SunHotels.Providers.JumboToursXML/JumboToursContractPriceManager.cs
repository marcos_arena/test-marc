﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SunHotels.Providers
{
    public class JumboToursContractPriceManager
    {
        public List<ContractPrice> ContractPrices { get; set; }
        public decimal TotalPrice { get { return ContractPrices.Sum(cp => cp.TotalPrice) + SupplementCost; } }
        public decimal Amount { get { return ContractPrices.Sum(cp => cp.Amount); } }
        public decimal PricePerAdult { get { return ContractPrices.Sum(cp => cp.PricePerAdult); } }
        public decimal PricePerChild { get { return ContractPrices.Sum(cp => cp.PricePerChild); } }
        public decimal SupplementCost { get; set; }

        public bool AdultPriceIsPaxBased { get { return ContractPrices.Any(cp => cp.Type == PriceType.PAX && cp.Paxtypes.Any(pt => pt == PaxTypeEnum.All || pt == PaxTypeEnum.Adult)); } }
        public bool ChildPriceIsPaxBased { get { return ContractPrices.Any(cp => cp.Type == PriceType.PAX && cp.Paxtypes.Any(pt => pt == PaxTypeEnum.All || pt == PaxTypeEnum.Child)); } }


        public JumboToursContractPriceManager()
        {
            ContractPrices = new List<ContractPrice>();
        }
    }
}
