﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using SunHotels.XML.Data;
using System.Globalization;
using System.Linq;
using SunHotels.XML.HelpClasses;
using System.IO;
using System.Xml.Linq;

namespace SunHotels.Providers
{
	/// <summary>
	/// Representation of supplements
	/// </summary>
	public class JumboToursSupplement
	{
		public int ID { get; set; }
		public DateTime StartDate { get; set; }
		public DateTime EndDate { get; set; }
		public int Order { get; set; }
        public string Currency { get; set; }
		public decimal Percent { get; set; }
		public decimal Amount { get; set; }
		public ContractRoomType RoomType { get; set; }
		public string BoardType { get; set; }
		public PaxType PaxType { get; set; }
        public decimal FirstPaxDiscount { get; set; }
        public decimal SecondPaxDiscount { get; set; }
        public decimal ThirdPaxDiscount { get; set; }
        public decimal FourthAndMorePaxDiscount { get; set; }
 
		public JumboToursSupplement()
		{
		}


        public static IEnumerable<JumboToursSupplement> Parse(XElement xSupplement, Dictionary<string, ContractRoomType> roomTypes, string boardTypes, Dictionary<PaxTypeEnum, PaxType> paxTypes, StreamWriter log)
        {
            /// <summary>
            /// Block list used to minimize logging
            /// </summary>
            List<string> logBlock = new List<string>();

            IFormatProvider culture = new CultureInfo("en-GB", true);
            List<JumboToursSupplement> supplements = new List<JumboToursSupplement>();

            bool active = bool.Parse(xSupplement.Attribute("active").Value);
            DateTime formalizationFrom = DateTime.Parse(xSupplement.Attribute("formalizationFrom").Value, culture);
            DateTime formalizationTo = DateTime.Parse(xSupplement.Attribute("formalizationTo").Value, culture).AddDays(1);
            int release = xSupplement.Attribute("release") == null ? 0 : Int32.Parse(xSupplement.Attribute("release").Value);
            if (DateTime.Now < formalizationFrom || DateTime.Now >= formalizationTo || active == false)
            {
                return supplements;
            }


            int id = Convert.ToInt32(xSupplement.Attribute("id").Value, culture);
            DateTime stayFrom = DateTime.Parse(xSupplement.Attribute("stayFrom").Value, culture);
            DateTime stayTo = DateTime.Parse(xSupplement.Attribute("stayTo").Value, culture);

            //  order = supplement type. Values:
            //  0: percent supplement. apply percent attribute value.
            //  1: apply fee attribute value per day and pax
            //  2: apply fee attribute value per day and room
            //  3: apply amount attribute value per stay and pax
            //  4: apply amount attribute value per stay and room

            int order = Convert.ToInt32(xSupplement.Attribute("order").Value);

            // If type is per stay but stay is only one day/night it can be regarded as per day.
            if (order == 3 || order == 4)
            {
                order = order - 2;
            }

            if (order > 2)
            {
                log.WriteLine(String.Format("Supplement type ({0}) specified in Supplement ({1}) can not be handled.", order, id));
                // Store unhandled supplments as order = -1 to be able to drop availability for affected dates.
                order = -1;
            }

            decimal percent = 0;
            if (xSupplement.Attribute("percent") != null)
            {
                percent = Convert.ToDecimal(xSupplement.Attribute("percent").Value, culture);
            }

            decimal amount = 0;
            if (xSupplement.Attribute("amount") != null)
            {
                amount = Convert.ToDecimal(xSupplement.Attribute("amount").Value, culture);
            }

            string currency = "EUR";
            if (xSupplement.Attribute("currency") != null)
            {
                currency = xSupplement.Attribute("currency").Value;
            }

            string monthDaysNode = GetAttribute(xSupplement, "monthDays");
            if (String.IsNullOrEmpty(monthDaysNode))
                monthDaysNode = GetAttribute(xSupplement,"formalizationMonthDays");

            bool[] monthDays = null;
            if (!String.IsNullOrEmpty(monthDaysNode))
            {
                monthDays = ParseMonthDays(monthDaysNode);
            }

            //Dictionary<int, bool> monthDays = null;
            //if(xSupplement.Attribute("monthDays").Value != String.Empty)
            //{
            //    xSupplement.Attribute("monthDays").Value.Split(',').ToDictionary(day => Convert.ToInt32(day), day => true);
            //}

            char[] weekdays = null;
            if (xSupplement.Attribute("stayWeekDays") != null)
            {
                weekdays = xSupplement.Attribute("stayWeekDays").Value.ToCharArray();
            }
            

            // If this supplement does not have one or more roomtypes specified it applies to all roomtypes for the contract (null).
            string[] roomtypes = new string[] { "" };
            if (xSupplement.Attribute("roomtypes") != null)
            {
                roomtypes = xSupplement.Attribute("roomtypes").Value.Split(',');

                // If this supplement has the same amount of roomtypes specified it applies to all roomtypes for the contract (null).
                if (roomtypes.Count() == roomTypes.Select(rt => rt.Key).Count())
                {
                    roomtypes = new string[] { "" };
                }
            }

            // If this supplement does not have one or more boardtypes specified it applies to all boardtypes for the contract (null).
            string[] boardtypes = new string[] { "" };
            if (xSupplement.Attribute("boardtypes") != null)
            {
                boardtypes = xSupplement.Attribute("boardtypes").Value.Split(',');

                // If this supplement has the same amount of boardtypes specified it applies to all boardtypes for the contract (null).
                if (boardtypes.Count() == boardTypes.Split(',').Count())
                {
                    boardtypes = new string[] { "" };
                }
            }

            // If this supplement does not have one or more paxtypes specified it applies to all paxtypes for the contract (null).
            PaxTypeEnum[] paxtypes = xSupplement.Elements("paxtype").Select(pt => PaxType.Parse(pt.Attribute("code").Value)).ToArray();

            decimal firstPaxDiscount  = JumboToursXML.DecimalParse(GetAttribute(xSupplement, "firstPaxDiscount"), CultureInfo.InvariantCulture, false);
            decimal secondPaxDiscount = JumboToursXML.DecimalParse(GetAttribute(xSupplement, "secondPaxDiscount"), CultureInfo.InvariantCulture, false);
            decimal thirdPaxDiscount = JumboToursXML.DecimalParse(GetAttribute(xSupplement, "thirdPaxDiscount"), CultureInfo.InvariantCulture, false);
            decimal fourthAndMorePaxDiscount = JumboToursXML.DecimalParse(GetAttribute(xSupplement, "fourthAndMorePaxDiscount"), CultureInfo.InvariantCulture, false);
            
            for (DateTime date = stayFrom; date <= stayTo; date = date.AddDays(1))
            {
                if (release > 1 && DateTime.Now < date)
                {

                }

                if (
                        (weekdays == null || weekdays[Convert.ToInt32(date.DayOfWeek)] == 'X') && 
                        (monthDays == null || monthDays[date.Day]) &&
                        DateTime.Today.AddDays(release-1) <= date &&
                        DateTime.Today <= date
                    )
                {
                    foreach (string roomtype in roomtypes)
                    {
                        ContractRoomType currentRoomType = null;
                        if (roomtype != "")
                        {
                            if (roomTypes.ContainsKey(roomtype))
                            {
                                currentRoomType = roomTypes[roomtype];
                            }
                            else
                            {
                                string listKey = String.Format("{0}.{1}", id, roomtype);
                                if (!logBlock.Contains(listKey))
                                {
                                    log.WriteLine(String.Format("RoomType ({0}) specified in Supplement ({1}) does not exist.", roomtype, id));
                                    logBlock.Add(listKey);
                                }
                                continue;
                            }
                        }

                        foreach (string boardtype in boardtypes)
                        {
                            string currentBoardType = null;
                            if (boardtype != "")
                            {
                                currentBoardType = boardtype;
                            }

                            if ((paxtypes.Contains(PaxTypeEnum.Adult) && paxtypes.Contains(PaxTypeEnum.Child)) || paxtypes.Count() == 0)
                            {
                                paxtypes = new PaxTypeEnum[] { PaxTypeEnum.All };
                            }

                            foreach (PaxTypeEnum paxtype in paxtypes)
                            {
                                PaxType currentPaxType = null;
                                if (paxtype == PaxTypeEnum.All)
                                {
                                    currentPaxType = new PaxType() { Code = paxtype };
                                }
                                else
                                {
                                    currentPaxType = paxTypes[paxtype];
                                }

                                // Find out if there is a supplement that we can append this data to.
                                IEnumerable<JumboToursSupplement> matchingSupplements = supplements.Where(s => s.Order == order
                                    && s.Percent == percent
                                    && s.Amount == amount
                                    && s.RoomType == currentRoomType
                                    && s.BoardType == currentBoardType
                                    && s.PaxType == currentPaxType
                                    && s.EndDate == date.AddDays(-1)
                                    && s.FirstPaxDiscount == firstPaxDiscount
                                    && s.SecondPaxDiscount == secondPaxDiscount
                                    && s.ThirdPaxDiscount == thirdPaxDiscount
                                    && s.FourthAndMorePaxDiscount == fourthAndMorePaxDiscount
                                    );
                                if (matchingSupplements.Count() > 0)
                                {
                                    matchingSupplements.First().EndDate = date;
                                }
                                else
                                {
                                    JumboToursSupplement newSupplement = new JumboToursSupplement()
                                    {
                                        ID = id,
                                        StartDate = date,
                                        EndDate = date,
                                        Order = order,
                                        Currency = currency,
                                        Percent = percent,
                                        Amount = amount, // Currency is always EUR regardless of what is in the cache.
                                        RoomType = currentRoomType,
                                        BoardType = currentBoardType,
                                        PaxType = currentPaxType,
                                        FirstPaxDiscount = firstPaxDiscount,
                                        SecondPaxDiscount = secondPaxDiscount,
                                        ThirdPaxDiscount = thirdPaxDiscount,
                                        FourthAndMorePaxDiscount = fourthAndMorePaxDiscount
                                    };
                                    supplements.Add(newSupplement);
                                }
                            }
                        }
                    }
                }
            }
            return supplements;
        }

        private static bool[] ParseMonthDays(string monthDaysString)
        {
            bool[] monthDays = new bool[32];
            
            IEnumerable<int> values = monthDaysString.Split(',').Select(v => Int32.Parse(v));                
            foreach (int day in values)
            {
                monthDays[day] = true;
            }

            return monthDays;
        }

        private static string GetAttribute(XElement elem, string attribName)
        {
            XAttribute attr = elem.Attribute(attribName);
            if (attr != null)
                return attr.Value;

            return String.Empty;
        }

        public decimal GetPaxbasedSupplement(int nrPax, decimal price)
        {
            if ((FirstPaxDiscount == 0 && SecondPaxDiscount == 0 && ThirdPaxDiscount == 0 && FourthAndMorePaxDiscount == 0) || nrPax == 0)
            {
                return 0;
            }

            decimal accumulatedSupplement = 0;
            for(int visitor = 1; visitor <= nrPax; visitor++)
            {
                switch (visitor)
                {
                    case 1: accumulatedSupplement += FirstPaxDiscount; break;
                    case 2: accumulatedSupplement += SecondPaxDiscount; break;
                    case 3: accumulatedSupplement += ThirdPaxDiscount; break;
                    // default is >= 4
                    default: accumulatedSupplement += FourthAndMorePaxDiscount; break;
                }
            }

            if (this.Order == 0)
            {
                // Percent based.

                return ((accumulatedSupplement / nrPax) / 100) * nrPax * price;
            }
            else
            {
                // Value based.
                return (decimal)XmlHelper.ConvertCurrency(accumulatedSupplement, this.Currency, "EUR"); ;
            }
        }
	}
}







