using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using SunHotels.XML.Data;
using System.Globalization;
using System.Xml.Linq;
using System.Linq;


namespace SunHotels.Providers
{

    /// <summary>
    /// Information about the hotel, things like additional fees are mentioned here if any
    /// </summary>
    public class JumboToursErrata
    {
        private static IFormatProvider culture = new CultureInfo("en-GB", true);

        string id;
        public string Id
        {
            get { return id; }
            set { id = value; }
        }

        DateTime from;
        public DateTime From
        {
            get { return from; }
            set { from = value; }
        }

        DateTime to;
        public DateTime To
        {
            get { return to; }
            set { to = value; }
        }

        string translationEnglish;
        public string TranslationEnglish
        {
            get { return translationEnglish; }
            set { translationEnglish = value; }
        }

        public JumboToursErrata(XElement xmlErrata)
        {
            this.id = xmlErrata.Attribute("id").Value;
            this.from = DateTime.Parse(xmlErrata.Attribute("from").Value, culture);
            this.to = DateTime.Parse(xmlErrata.Attribute("to").Value, culture);

            if (xmlErrata.Elements("translation").Count() > 0)
            {
                IEnumerable<XElement> trans = xmlErrata.Elements("translation").Where(t => t.Attribute("lang").Value == "en");
                //If the english text isn't available take the first text in any language
                if (trans.Count() > 0)
                {
                    this.translationEnglish = trans.First().Value;
                }
                else
                {
                    this.translationEnglish = xmlErrata.Elements("translation").First().Value;
                }
            }
            else
            {
                this.translationEnglish = "";
            }
        }

    }
}
