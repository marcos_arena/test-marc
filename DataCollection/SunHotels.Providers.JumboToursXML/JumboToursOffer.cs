using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using SunHotels.XML.Data;
using System.Globalization;
using System.IO;
using System.Xml.Linq;
using System.Linq;
using SunHotels.XML.HelpClasses;

namespace SunHotels.Providers
{
    public class JumboToursOffer : ICloneable
	{
		private static IFormatProvider culture = new CultureInfo("en-GB", true);
		private static IFormatProvider usCulture = new CultureInfo("en-US", true);

        public int Id { get; set; }
        public string Name { get; set; }
        //public Discount Discount { get; set; }
        public bool Active { get; set; }
        public string Currency { get; set; }
        public string RoomTypes { get; set; }
        public string BoardTypes { get; set; }
        private Int16 mindays;
        public Int16 Mindays { get { return mindays; } }
        private Int16 forDays;
        public int PercentDiscount { get; set; }
        public decimal AmountDiscount { get; set; }
        //public decimal ChildDiscountPercent { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime StartDateFormalization { get; set; }
        public DateTime EndDateFormalization { get; set; }
        public bool[] WeekDays { get; set; }
        public bool[] MonthDays { get; set; }
        public bool Accumulable { get; set; }
        public bool WeekDayBased { get; set; }
        public int Order { get; set; }
        private string PaxType { get; set; }
        //public bool IsPaxDiscount { get { return (FirstPaxDiscount != 0 || SecondPaxDiscount != 0 || ThirdPaxDiscount != 0 || FourthAndMorePaxDiscount != 0) }}
        private decimal firstPaxDiscount;
        public decimal FirstPaxDiscount { get { return firstPaxDiscount; } }
        private decimal secondPaxDiscount;
        public decimal SecondPaxDiscount { get { return secondPaxDiscount; } }
        private decimal thirdPaxDiscount;
        public decimal ThirdPaxDiscount { get { return thirdPaxDiscount; } }
        private decimal fourthAndMorePaxDiscount;
        public decimal FourthAndMorePaxDiscount { get { return fourthAndMorePaxDiscount; } }
        public bool IsPaxDiscount { get { return FirstPaxDiscount != 0 || SecondPaxDiscount != 0 || ThirdPaxDiscount != 0 || FourthAndMorePaxDiscount != 0; } }
        private int? maxDays;
        private int maxAge;
        private int percent;
        private decimal amount;
        public int MaxAge { get { return maxAge; } }


		public JumboToursOffer(XElement xmlOfferNode)
		{
			this.Id = Int32.Parse(xmlOfferNode.Attribute("id").Value);

            if (this.Id == 763315347)
            {

            }

			this.StartDate = DateTime.Parse(xmlOfferNode.Attribute("stayFrom").Value, culture);
			this.EndDate = DateTime.Parse(xmlOfferNode.Attribute("stayTo").Value, culture);

            int release = 0;
            Int32.TryParse(GetAttribute(xmlOfferNode, "release"), NumberStyles.Number, CultureInfo.InvariantCulture, out release);
            if (DateTime.Today.AddDays(release) > StartDate) StartDate = DateTime.Today.AddDays(release);

            if (this.StartDate > this.EndDate) return;//Release have expired
			if (this.EndDate.CompareTo(DateTime.Today) < 0) return;//Offer expired
			if (this.StartDate.CompareTo(DateTime.Today) < 0) this.StartDate = DateTime.Today;//starting date is below todays date but the offer hasnt expired

			this.StartDateFormalization = DateTime.Parse(xmlOfferNode.Attribute("formalizationFrom").Value, culture);
			this.EndDateFormalization = DateTime.Parse(xmlOfferNode.Attribute("formalizationTo").Value, culture);



            string monthDaysString = GetAttribute(xmlOfferNode, "monthDays");
            if (String.IsNullOrEmpty(monthDaysString))
                monthDaysString = GetAttribute(xmlOfferNode, "formalizationMonthDays");

            ParseMonthDays(monthDaysString);

            this.Order = Convert.ToInt32(xmlOfferNode.Attribute("order").Value);

            XElement paxType = xmlOfferNode.Element("paxtype");
            if (paxType != null) PaxType = paxType.Attribute("code").Value;

            Int32.TryParse(GetAttribute(xmlOfferNode, "toAge"), NumberStyles.Number, CultureInfo.InvariantCulture, out maxAge);

			if (!DateWithinFormalization(DateTime.Today))
			{
				return; //Cant use this offer, formalization expired
			}

            string checkinWeekDays = GetAttribute(xmlOfferNode, "checkinWeekDays");
            if (!(checkinWeekDays == String.Empty || checkinWeekDays == "XXXXXXX"))
            {
                return;
            }

			this.Name = xmlOfferNode.Name.ToString();
			if (xmlOfferNode.Attribute("roomtypes") != null)
			{
				this.RoomTypes = xmlOfferNode.Attribute("roomtypes").Value;
			}
			if (xmlOfferNode.Attribute("boardtypes") != null)
			{
				this.BoardTypes = xmlOfferNode.Attribute("boardtypes").Value;
			}

            Currency = "EUR";
            if (xmlOfferNode.Attribute("currency") != null)
            {
                Currency = xmlOfferNode.Attribute("currency").Value;
            }

            firstPaxDiscount = JumboToursXML.DecimalParse(GetAttribute(xmlOfferNode, "firstPaxDiscount"), CultureInfo.InvariantCulture);
            secondPaxDiscount = JumboToursXML.DecimalParse(GetAttribute(xmlOfferNode, "secondPaxDiscount"), CultureInfo.InvariantCulture);
            thirdPaxDiscount = JumboToursXML.DecimalParse(GetAttribute(xmlOfferNode, "thirdPaxDiscount"), CultureInfo.InvariantCulture);
            fourthAndMorePaxDiscount = JumboToursXML.DecimalParse(GetAttribute(xmlOfferNode, "fourthAndMorePaxDiscount"), CultureInfo.InvariantCulture);

            this.mindays = 1;
            Int16.TryParse(GetAttribute(xmlOfferNode, "minStay"), NumberStyles.Number, CultureInfo.InvariantCulture, out mindays);
            if (mindays == 0) mindays = 1;

            if (Order == -1) // ThreeForTwoOffer
			{
                this.mindays = Int16.Parse(xmlOfferNode.Attribute("three").Value);
				this.forDays = Int16.Parse(xmlOfferNode.Attribute("two").Value);
            }

            this.maxDays = xmlOfferNode.Attribute("maxStay") == null || xmlOfferNode.Attribute("maxStay").Value == "0" ? null : (int?)Int32.Parse(xmlOfferNode.Attribute("maxStay").Value);
            decimal perc = JumboToursXML.DecimalParse(xmlOfferNode.Attribute("percent").Value, culture, false);
            
            this.percent = perc > 0 ? (int)Math.Floor(perc) : 0;
            this.amount = xmlOfferNode.Attribute("amount") != null ? Convert.ToDecimal(xmlOfferNode.Attribute("amount").Value, culture) : 0;

            //GetDiscounts();

			if (xmlOfferNode.Attribute("active").Value == "true")
			{
				this.Active = true;
			}


			this.WeekDayBased = false;
			if (xmlOfferNode.Attribute("weekdays") != null)
			{
				string weekDaysTemp = xmlOfferNode.Attribute("weekdays").Value;

                if (weekDaysTemp == "XXXXXXX")
                {
                    this.WeekDays = new bool[] { true, true, true, true, true, true, true };
                }
                else
                {
                    this.WeekDays = new bool[weekDaysTemp.Length];
                    int dayCount = 0;
                    foreach (char day in weekDaysTemp)
                    {
                        if (day == 'X')
                        {
                            this.WeekDays[dayCount++] = true;
                        }
                        else if (day == '.')
                        {
                            this.WeekDayBased = true;
                        }
                    }
                }

			}

			if (xmlOfferNode.Attribute("monthdays") != null)
			{
				string monthDaysTemp = xmlOfferNode.Attribute("monthdays").Value;
				int dayCount = 1;
				foreach (char day in monthDaysTemp)
				{
					if (day == dayCount)
					{
						this.MonthDays[dayCount++] = true;
					}
				}
			}

            if (this.Mindays == 0)
            {

            }
		}

        private void ParseMonthDays(string monthDaysString)
        {
            IEnumerable<int> values = monthDaysString.Split(',').Select(v => Int32.Parse(v));
            this.MonthDays = new bool[32];
            foreach (int day in values)
            {
                this.MonthDays[day] = true;
            }
        }

		public bool MatchDate(DateTime date)
		{
			if (date.CompareTo(StartDate) >= 0 && date.CompareTo(EndDate) <= 0)
			{
				return true;
			}
			else
			{
				return false;
			}

		}

		//If the sent date is within the formalization dates it means it's valid to book
		public bool DateWithinFormalization(DateTime date)
		{
			//check if the currentdate is bigger than the starting day and if the tommorowdate is less than the ending date
			if ((date.CompareTo(StartDateFormalization) >= 0) && (date.AddDays(1).CompareTo(EndDateFormalization) <= 0))
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		//In the beggining we couldnt support board based board types but now we do it 
		public bool BoardTypeMatches(string boardType, string offerBoardTypes)
		{
			if (offerBoardTypes == null)
			{
				return true;
			}
			if (boardType != null && offerBoardTypes.Contains(boardType))
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		//Return true if the roomtypes atribute contains the roomtype
		//or if the roomtypes attribute is null
		public bool RoomTypeMatches(string roomType, string offerRoomTypes)
		{
			if (offerRoomTypes == null)
			{
				return true;
			}
			else if (offerRoomTypes.Contains(roomType))
			{
				return true;
			}
			else
			{
				return false;
			}
		}

        private static string GetAttribute(XElement elem, string attribName)
        {
            XAttribute attr = elem.Attribute(attribName);
            if (attr != null)
                return attr.Value;

            return String.Empty;
        }

        public decimal GetPaxbasedOffer(int nrPax, decimal price)
        {
            if (!this.IsPaxDiscount || nrPax == 0)
            {
                return 0;
            }

            decimal accumulatedSupplement = 0;
            for (int visitor = 1; visitor <= nrPax; visitor++)
            {
                switch (visitor)
                {
                    case 1: accumulatedSupplement += FirstPaxDiscount; break;
                    case 2: accumulatedSupplement += SecondPaxDiscount; break;
                    case 3: accumulatedSupplement += ThirdPaxDiscount; break;
                    // default is >= 4
                    default: accumulatedSupplement += FourthAndMorePaxDiscount; break;
                }
            }

            if (this.Order == 0)
            {
                // Percent based.

                return ((accumulatedSupplement / nrPax) / 100) * nrPax * price;
            }
            else
            {
                // Value based.
                return (decimal)XmlHelper.ConvertCurrency(accumulatedSupplement, this.Currency, "EUR"); ;
            }
        }

        public int? GetChildDiscount(JumboToursContractPriceManager prices)
        {
            string paxType = PaxType;

            if (paxType == "AD" && MaxAge != 0 && MaxAge < 20 && !prices.AdultPriceIsPaxBased)
            {
                paxType = "NI";
            }

            if (paxType == "NI")
            {
                //children discounts aren't discounts in the sunhotels system, instead they manifest as lower childprice
                if (this.Mindays == 1 && this.percent > 0 
                    && !(this.IsPaxDiscount && prices.AdultPriceIsPaxBased) // child discount is applied as a discount if its a pax discount and the prices are in pax.
                   )
                {
                    return this.percent;
                }
            }
            return null;
        }

        public Discount GetDiscounts(JumboToursContractPriceManager prices, ContractRoomType roomType)
        {
            Discount discount = null;

            string paxType = PaxType;

            if (paxType == "AD" && MaxAge != 0 && MaxAge < 20 && !prices.AdultPriceIsPaxBased)
            {
                paxType = "NI";
            }

            if (Order == -1) // ThreeForTwoOffer
            {
                int discountDays = this.mindays - forDays;

                if (paxType == null || paxType == "AD")
                {
                    discount = Discount.PayStay(this.Mindays, null, discountDays, Id);
                }
            }
            else if (this.IsPaxDiscount && paxType != "BB")
            {
                decimal adultPrice = prices.PricePerAdult;
                decimal childPrice = prices.PricePerChild;
                decimal totalPrice = prices.TotalPrice;

                int minAdults = roomType.MinAdults;
                int minChildren = roomType.MinChildren;

                decimal totalDiscount = 0;

                if (Order == 1 || Order == 0)
                {
                    if (String.IsNullOrEmpty(PaxType) && prices.AdultPriceIsPaxBased && prices.ChildPriceIsPaxBased)
                    {
                        totalDiscount += GetPaxbasedOffer(minAdults, adultPrice);
                        totalDiscount += GetPaxbasedOffer(minChildren, childPrice);
                    }
                    else if (paxType == "AD" && minAdults > 0 && adultPrice > 0 && prices.AdultPriceIsPaxBased)
                    {
                        totalDiscount += GetPaxbasedOffer(minAdults, adultPrice);
                    }
                    else if (paxType == "NI" && minChildren > 0 && childPrice > 0 && (MaxAge == 0 || MaxAge >= 12) && prices.ChildPriceIsPaxBased)
                    {
                        totalDiscount += GetPaxbasedOffer(minChildren, childPrice);
                    }

                    int totalDiscountPercent = (int)Decimal.Floor((totalDiscount / totalPrice) * 100);

                    if (totalDiscountPercent > 0)
                    {
                        discount = Discount.ValuePercent(Mindays, totalDiscountPercent, Id);                        
                    }
                }
            }
            else
            {
                if (paxType == null || paxType == "AD")
                {
                    if (this.percent > 0)
                    {
                        discount = maxDays.HasValue
                            ? Discount.MinMaxPercent(Mindays, maxDays.Value, percent, Id)
                            : Discount.ValuePercent(Mindays, this.percent, Id);
                    }
                    else if (amount > 0)
                    {
                        if (Order == 1)
                        {
                            discount = Discount.ValueAmount(Mindays, amount, Id);
                        }
                    }
                }
                else if (paxType == "NI")
                {
                    //children discounts aren't discounts in the sunhotels system, instead they manifest as lower childprice
                    // Is handled by GetChildDiscount(..).
                }
                else
                {
                    throw new Exception(String.Format("New paxtype discovered {0} on offer {1}", PaxType, this.Id));
                }
            }

            if (discount != null)
            {
                if (discount.Percent >= 100)
                {
                    Console.WriteLine("100% discount in " + this.Name);
                }
            }

            return discount;
        }

        public object Clone()
        {
            return this.MemberwiseClone();
        }
	}
}
