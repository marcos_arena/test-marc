using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using SunHotels.XML.Data;
using System.Globalization;
using System.Xml.Linq;

namespace SunHotels.Providers
{
    /// <summary>
    /// Representation of the roomocupant like aduult,child etc.
    /// </summary>
    public class PaxType
    {
        PaxTypeEnum code;
        public PaxTypeEnum Code
        {
            get { return code; }
            set { code = value; }
        }

        int fromAge;
        public int FromAge
        {
            get { return fromAge; }
            set { fromAge = value; }
        }
        
        int toAge;
        public int ToAge
        {
            get { return toAge; }
            set { toAge = value; }
        }

        public PaxType(XElement paxType)
        {
            this.code = Parse(paxType.Attribute("code").Value);
            this.fromAge = Int32.Parse(paxType.Attribute("fromAge").Value);
            this.toAge = Int32.Parse(paxType.Attribute("toAge").Value);
        }

        public PaxType()
        {
        }

        public static PaxTypeEnum Parse(string value)
        {
            switch (value)
            {
                case "AD": return PaxTypeEnum.Adult;
                case "CH": return PaxTypeEnum.Child;
                case "NI": return PaxTypeEnum.Child;
                case "BB": return PaxTypeEnum.Baby;
                default: throw new Exception("Unknown pax type.");
            }
        }
    }

    public enum PaxTypeEnum
    {
        Adult,
        Child,
        Baby,
        All
    }
}
