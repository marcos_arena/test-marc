using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using SunHotels.XML.Data;
using System.Globalization;
using System.IO;
using System.Xml.Linq;
using System.Linq;

namespace SunHotels.Providers
{
    class JumboToursParser
    {
        Dictionary<string, List<JumboToursContract>> hotels = new Dictionary<string, List<JumboToursContract>>();
        public Dictionary<string, List<JumboToursContract>> Hotels
        {
            get { return hotels; }
            set { hotels = value; }
        }

        List<JumboToursEstablishment> establishmentList = new List<JumboToursEstablishment>();
        internal List<JumboToursEstablishment> EstablishmentList
        {
            get { return establishmentList; }
            set { establishmentList = value; }
        }

        /// <summary>
        /// Parses all the contract nodes into objects
        /// </summary>
        /// <param name="inputDocument"></param>
        /// <param name="log"></param>
        public void ParseContract(XDocument inputDocument, StreamWriter log)
        {
            int contractCount = 0;
			foreach (XElement xmlContractHotelNode in inputDocument.Descendants("hotel"))
            {
                XElement xmlContractNode = xmlContractHotelNode.Parent;
                string hotelId = xmlContractHotelNode.Attribute("id").Value;

                //if (hotelId != "6348") continue;
                try
                {
                    if (xmlContractNode.Elements().Count() > 0)
                    {
                        JumboToursContract contract = new JumboToursContract(xmlContractNode, hotelId, log);

                        if (contract.ValidContract && contract.Stock.Count() > 0 && contract.Prices.Count() > 0)
                        {
                            if (!hotels.ContainsKey(contract.HotelId))
                            {
                                contractCount++;
                                hotels[contract.HotelId] = new List<JumboToursContract>();
                            }
                            hotels[contract.HotelId].Add(contract);
                        }
                        else
                        {

                        }
                    }
                    else
                    {

                    }
                }
                catch (MinstayMaxLimitExceededException)
                {
                    log.WriteLine(
                        String.Format("Dropping minstay constraint(s), minstay value over 100 found in hotel [{0}]",
                                      hotelId));

                    continue;
                }
                catch (MissingElementAttributeException e)
                {
                    log.WriteLine(String.Format("Error, missing attribute ({0})", e.Message));
                    continue;
                }
                catch (Exception e)
                {
                    log.WriteLine(String.Format("ERROR found while parsing hotel {0} - ({1})", hotelId, e.ToString()));
                    continue;
                }
            }			
            return;
        }

        /// <summary>
        /// Parses all the establishmentnodes into objects
        /// </summary>
        /// <param name="inputDocument"></param>
        public void parseEstablishment(XDocument inputDocument)
        {
            foreach (XElement xmlEstablishmentNode in inputDocument.Element("hotels").Elements("Establishment"))
            {
                JumboToursEstablishment establishment = new JumboToursEstablishment(xmlEstablishmentNode);
                establishmentList.Add(establishment);
            }
        }

    }
}



