using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using SunHotels.XML.Data;
using System.Globalization;
using System.Xml.Linq;

namespace SunHotels.Providers
{
    public class ContractRoomType
    {
        string code;
        public string Code
        {
            get { return code; }
            set { code = value; }
        }

        int minPaxes;
        public int MinPaxes
        {
            get { return minPaxes; }
            set { minPaxes = value; }
        }

        int maxPaxes;
        public int MaxPaxes
        {
            get { return maxPaxes; }
            set { maxPaxes = value; }
        }

        int minAdults;
        public int MinAdults
        {
            get { return minAdults; }
            set { minAdults = value; }
        }

        int minChildren;
        public int MinChildren
        {
            get { return minChildren; }
            set { minChildren = value; }
        }

        string weekdays;
        public string Weekdays
        {
            get { return weekdays; }
            set { weekdays = value; }
        }

        string roomGroup;
        public string RoomGroup
        {
            get { return roomGroup; }
            set { roomGroup = value; }
        }

        string compatibleRoomTypes;
        public string CompatibleRoomTypes
        {
            get { return compatibleRoomTypes; }
            set { compatibleRoomTypes = value; }
        }


        public ContractRoomType(XElement roomType)
        {
            this.code = roomType.Attribute("code").Value;
            this.minPaxes = Int32.Parse(roomType.Attribute("minpaxes").Value);
            this.maxPaxes = Int32.Parse(roomType.Attribute("maxpaxes").Value);
            this.minAdults = Int32.Parse(roomType.Attribute("minadults").Value);
            this.minChildren = Int32.Parse(roomType.Attribute("minChildren").Value);

            //In order to coop with rooms that enforce children and set minpaxes to children + adults 
            //Our system cant support that so such room have to be minpaxes=adults and minchildren as extra beds 
            if (this.minChildren > 0)
            {
                this.minPaxes = this.MinAdults;
                this.maxPaxes = this.MinChildren + this.MinAdults;
            }
        }


        public ContractRoomType(ContractRoomType cRoomType,string newCode)
        {
            this.code = newCode;
            this.minPaxes = cRoomType.minPaxes;
            this.maxPaxes = cRoomType.maxPaxes;
            this.minAdults = cRoomType.minAdults;
            this.minChildren = cRoomType.minChildren;  
        }

        //Asigns the room to a roomgroup
        public void SetRoomGroupForContractRoomType(string roomgroupName)
        {
            this.roomGroup = roomgroupName;
        }
    }
}
