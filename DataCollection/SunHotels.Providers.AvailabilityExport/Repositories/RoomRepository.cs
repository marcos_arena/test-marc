﻿using SunHotels.SearchCache.Search.InvertedIndex;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SunHotels.Providers.Repositories
{
    public class RoomRepository
    {
        public HashSet<int> weeklyRooms;
        public Dictionary<int, DateRangeCollection> calendarBlockedRooms;

        public static RoomRepository LoadRoomRepository()
        {
            var roomRep = new RoomRepository();

            try
            {
                Console.WriteLine("Getting weekly rooms");
                roomRep.BuildWeeklyRooms();
                Console.WriteLine("Getting calendar blocks");
                roomRep.BuildCalendarBlockedRooms();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error loading room repository: " + ex.ToString());
                throw;
            }

            return roomRep;
        }

        private void BuildWeeklyRooms()
        {
            weeklyRooms = new HashSet<int>();

            using (var connection = new SqlConnection(Properties.Settings.Default.sunhotelsConnectionString))
            {
                using (var command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandText = @"SELECT id FROM rooms r WHERE ISNULL(weekly, 0) = 1 AND isLiveRoom = 0 AND ISNULL(r.SuspendRoom, 0) = 0";
                    connection.Open();
                    using (var rdr = command.ExecuteReader(CommandBehavior.SequentialAccess))
                    {
                        while (rdr.Read())
                        {
                            weeklyRooms.Add(rdr.GetInt32(0));
                        }
                    }
                }
            }
        }

        private void BuildCalendarBlockedRooms()
        {
            calendarBlockedRooms = new Dictionary<int, DateRangeCollection>();
            using (var connection = new SqlConnection(Properties.Settings.Default.sunhotelsConnectionString))
            {
                using (var command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandText = @"SELECT room_id, datum FROM dbo.calendar_block";
                    connection.Open();
                    using (var rdr = command.ExecuteReader(CommandBehavior.SequentialAccess))
                    {
                        while (rdr.Read())
                        {
                            DateRangeCollection dates;
                            var roomId = rdr.GetInt32(0);
                            var day = rdr.GetDateTime(1);

                            if (!calendarBlockedRooms.TryGetValue(roomId, out dates))
                                dates = new DateRangeCollection();

                            dates.Add(day);
                            calendarBlockedRooms[roomId] = dates;
                        }
                    }
                }
            }
        }
    }
}
