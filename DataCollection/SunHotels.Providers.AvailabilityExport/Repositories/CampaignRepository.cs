﻿using SunHotels.SearchCache.Search.InvertedIndex.BinaryDatabaseSerialization;
using SunHotels.XML.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SunHotels.Providers.Repositories
{
    public class CampaignRepository
    {
        public List<Campaign> campaignCache;

        public class CampaignResult
        {
            public double percent = 0;
            public double value = 0;
        }

        public static CampaignRepository LoadCampaignRepository(int agentId)
        {
            var campRep = new CampaignRepository();

            try
            {
                Console.WriteLine("Getting Campaigns");
                campRep.LoadFromDatabase(agentId);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error loading campaign repository: " + ex.ToString());
                throw;
            }

            return campRep;
        }

        private void LoadFromDatabase(int agentId)
        {
            campaignCache = new List<Campaign>();

            using (var connection = new SqlConnection(Properties.Settings.Default.sunhotelsConnectionString))
            {
                using (var command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandText = @"
                SELECT c.value,
                       c.unitId,
                       c.roomProviderId,
                       c.checkinStartDate,
                       c.checkinEndDate,
                       c.hotelCountryId,
                       c.hotelDestinationId,
                       c.hotelResortId,
                       c.hotelId,
                       c.agentId,
                       c.agentCountryId,
                       c.brandId
                FROM dbo.Campaigns c
                WHERE c.bitDeleted = 0
                 AND ISNULL(c.bookingEndDate, GETDATE()) >= GETDATE()
	             AND c.bookingStartDate <= GETDATE()
                 AND (c.agentId IS NULL OR c.agentId = " + agentId.ToString() + ")";
                    connection.Open();
                    using (var rdr = command.ExecuteReader(CommandBehavior.SequentialAccess))
                    {
                        while (rdr.Read())
                        {
                            var c = new Campaign();
                            c.Value = rdr.GetDouble(0);
                            c.UnitId = rdr.GetInt32(1);
                            c.RoomProviderId = rdr.IsDBNull(2) ? (int?)null : rdr.GetInt32(2);
                            c.CheckinStartDate = rdr.IsDBNull(3) ? DateTime.MinValue : rdr.GetDateTime(3);
                            c.CheckinEndDate = rdr.IsDBNull(4) ? DateTime.MaxValue : rdr.GetDateTime(4);
                            c.HotelCountryId = rdr.IsDBNull(5) ? (int?)null : rdr.GetInt32(5);
                            c.HotelDestinationId = rdr.IsDBNull(6) ? (int?)null : rdr.GetInt32(6);
                            c.HotelResortId = rdr.IsDBNull(7) ? (int?)null : rdr.GetInt32(7);
                            c.HotelId = rdr.IsDBNull(8) ? (int?)null : rdr.GetInt32(8);
                            c.AgentId = rdr.IsDBNull(9) ? (int?)null : rdr.GetInt32(9);
                            c.AgentCountryId = rdr.IsDBNull(10) ? (int?)null : rdr.GetInt32(10);
                            c.BrandId = rdr.IsDBNull(11) ? (int?)null : rdr.GetInt32(11);

                            campaignCache.Add(c);
                        }
                    }
                }
            }
        }

        public CampaignResult GetCampaignsByAgentRoomProviderAndHotel(AvailabilityExportDataSet.agentRow agent, int roomProviderId, CacheHotelObject hotel, DateTime date)
        {
            CampaignResult campaignResult = new CampaignResult();

            if (campaignCache != null && campaignCache.Count > 0)
            {
                List<Campaign> campaigns = campaignCache.Where(c => (c.RoomProviderId == null || c.RoomProviderId == roomProviderId) &&
                                                                    (c.BrandId == null || c.BrandId == agent.SH_address) &&
                                                                    (c.AgentCountryId == null || c.AgentCountryId == agent.countryId) &&
                                                                    (c.HotelId == null || c.HotelId == hotel.HotelId) &&
                                                                    (c.HotelCountryId == null || c.HotelCountryId == hotel.countryId) &&
                                                                    (c.HotelDestinationId == null || c.HotelDestinationId == hotel.destinationId) &&
                                                                    (c.HotelResortId == null || c.HotelResortId == hotel.resortId) &&
                                                                    (agent.isFileExportAgent ? (c.AgentId == agent.id) : (c.AgentId == null || c.AgentId == agent.id)) &&
                                                                    (date >= c.CheckinStartDate && date <= c.CheckinEndDate)).ToList();

                if (campaigns != null && campaigns.Count > 0)
                {
                    double maxValuePos = 0, maxPercentPos = 0, maxValueNeg = 0, maxPercentNeg = 0;

                    foreach (Campaign c in campaigns)
                    {
                        if (c.Value > 0)
                        {
                            if (c.UnitId == 1 && c.Value >= maxPercentPos) { maxPercentPos = c.Value; }
                            if (c.UnitId == 2 && c.Value >= maxValuePos) { maxValuePos = c.Value; }
                        }
                        else if (c.Value < 0)
                        {
                            if (c.UnitId == 1 && c.Value <= maxPercentNeg) { maxPercentNeg = c.Value; }
                            if (c.UnitId == 2 && c.Value <= maxValueNeg) { maxValueNeg = c.Value; }
                        }
                    }

                    campaignResult.percent = maxPercentPos + maxPercentNeg;
                    campaignResult.value = maxValuePos + maxValueNeg;
                }
            }

            return campaignResult;   
        }
    }
}
