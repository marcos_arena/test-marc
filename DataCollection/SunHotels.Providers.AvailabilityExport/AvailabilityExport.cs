﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using SunHotels.Providers.AvailabilityExportDataSetTableAdapters;
using SunHotels.XML;
using SunHotels.XML.Data;
using System.Threading;
using System.Threading.Tasks;
using System.Globalization;
using System.IO;
using System.Xml.Serialization;
using System.Collections.Concurrent;
using SunHotels.XML.Infrastructure;
using SunHotels.Providers.Repositories;
using SunHotels.SearchCache.Search.InvertedIndex;
using SunHotels.SearchCache.Search.InvertedIndex.BinaryDatabaseSerialization;
using SunHotels.XML.HelpClasses;

namespace SunHotels.Providers
{
    public class AvailabilityExport : XMLProvider
    {
        private const int GeneralTimeout = 45 * 60;
        private const int QueryElements = 1000;

        AvailabilityExportSettings _settings;
        public static AvailabilityExportDataSet.agentRow Agent { get; private set; }
        private readonly NumberFormatInfo _invariantNumberFormat = CultureInfo.InvariantCulture.NumberFormat;
        private CancellationRulesManager cancellationRulesManager;

        private Dictionary<string, List<string>> hotelRoomList;

        private CampaignRepository campaignRepository;
        private RoomRepository roomRepository;

        protected static FasstIndexes _cache;

        private IRoomConfigurationProvider roomConfigurationProvider;
        private Dictionary<int, XMLProviderDefinition> xmlProviders;

        private static DateTime today = DateTime.Today;
        private static DateTime yesterday = today.AddDays(-1);

        private LogHierarchy loggerIncluded;
        private int hotelsIncluded = 0;
        private int roomsIncluded = 0;
        private LogHierarchy loggerExcluded;
        private int hotelsExcluded = 0;
        private int roomsExcluded = 0;

        protected override void getProviderDefinition(Root root)
        {
            root.ProviderDefinition.Name = configuration.ProviderName;
            root.ProviderDefinition.Currency = "EUR";
        }

        protected override void getData(Root root)
        {
            var availabilityConfigFullPath = Path.Combine(configuration.ProviderConfigRoot, "AvailabilityExportSettings.xml");
            var dStart = DateTime.Now;

            if (File.Exists(availabilityConfigFullPath))
            {
                var serializer = new XmlSerializer(typeof(AvailabilityExportSettings));
                Stream s = new FileStream(availabilityConfigFullPath, FileMode.Open, FileAccess.Read);
                _settings = (AvailabilityExportSettings)serializer.Deserialize(s);
            }
            else
            {
                _settings = new AvailabilityExportSettings();
            }

            if (_settings.LogToFile)
            {
                // Redirect console to text log file
                string logPath = string.IsNullOrEmpty(configuration.LogOutput) ? "Log" : configuration.LogOutput;
                loggerIncluded = new LogHierarchy("IncludedAvailability", logPath, configuration.UserId);
                loggerExcluded = new LogHierarchy("ExcludedAvailability", logPath, configuration.UserId);
                   
                if (!Directory.Exists(logPath)) Directory.CreateDirectory(logPath);
                FileStream filestream = new FileStream(Path.Combine(logPath, DateTime.Now.ToString("yyyyMMddHHmmss") + ".txt"), FileMode.Append);
                var streamwriter = new StreamWriter(filestream);
                streamwriter.AutoFlush = true;
                Console.SetOut(streamwriter);
                Console.SetError(streamwriter);
            }

            Console.WriteLine("Start : {0}", dStart);
            Thread.CurrentThread.CurrentCulture = Thread.CurrentThread.CurrentUICulture = CultureInfo.InvariantCulture;
            var l = new agentTableAdapter();
            AvailabilityExportDataSet.agentDataTable t = new AvailabilityExportDataSet.agentDataTable();
            try
            {
                t = l.GetData(int.Parse(configuration.UserId));
            }
            catch (Exception e)
            {
                Console.WriteLine("Error retrieving agent data: " + e.ToString());
                var errors = t.GetErrors();
                if (errors.Any())
                    Console.WriteLine(errors[0].RowError);
            }

            if (t.Rows.Count != 1)
            {
                throw new Exception(string.Format("Agent with id {0} not found [{1} rows].", configuration.UserId, t.Rows.Count));
            }

            Agent = t[0];

            if (_settings.UseSearchCacheFiles)
            {
                _cache = FasstIndexes.LoadUnicornCache(_settings);
                campaignRepository = CampaignRepository.LoadCampaignRepository(int.Parse(configuration.UserId));
                roomRepository = RoomRepository.LoadRoomRepository();

                if (Agent.enforce_provider_min_release)
                    GetXMLProviders();
            }
            
            Console.WriteLine("Fetching countries, destinations and resorts...");
            GetPlaces(root);
            Console.WriteLine("Fetching countries, destinations and resorts done.");

            ConcurrentDictionary<int, List<AvailabilityExportDataSet.roomGroupRow>> roomGroups = null;

            if (UseRoomGroups())
            {
                Console.WriteLine("Fetching Room Groups...");
                roomGroups = new ConcurrentDictionary<int, List<AvailabilityExportDataSet.roomGroupRow>>();
                new roomGroupTableAdapter().GetData().ToList().ForEach(r =>
                {
                    List<AvailabilityExportDataSet.roomGroupRow> rowList;
                    if (!roomGroups.TryGetValue(r.hotelId, out rowList))
                    {
                        rowList = new List<AvailabilityExportDataSet.roomGroupRow>();
                        roomGroups.TryAdd(r.hotelId, rowList);
                    }
                    rowList.Add(r);
                });
            }

            Console.WriteLine("Fetching Hotels...");

            // Get the the data from the CancellationRulesManager
            if (configuration.Optimizer != Sunhotels.Export.ConfigurationOptimizer.splitted || !string.IsNullOrEmpty(configuration.CancellationPolicyCacheOutput))
            {
                cancellationRulesManager = new CancellationRulesManager(configuration.MaxDegreeOfParallelism);
                cancellationRulesManager.LoadCancelationRules(root.CancellationsPolicies);
            }

            hotelRoomList = GetHotels(root);

            var numHotels = hotelRoomList.Keys.Count;
            Console.WriteLine("Fetching Hotels done.");

            Console.WriteLine("Fetching Hotel data.");
            var sw = new System.Diagnostics.Stopwatch();
            sw.Start();
            var count = 0;

            roomConfigurationProvider = new RoomConfigurationProvider(new RoomConfigurationDal(Properties.Settings.Default.sunhotelsConnectionString));
            
            Parallel.ForEach(root.AllPlaces(), new ParallelOptions { MaxDegreeOfParallelism = configuration.MaxDegreeOfParallelism }, p =>
            {
                if (p.Hotels != null && p.Hotels.Count > 0)
                {
                    lock (root)
                    {
                        Console.WriteLine("    Resort: {0}", p.Id);
                    }

                    Parallel.ForEach(p.Hotels.Values, new ParallelOptions { MaxDegreeOfParallelism = configuration.MaxDegreeOfParallelism }, hotel =>
                    {
                        Thread.CurrentThread.CurrentCulture = Thread.CurrentThread.CurrentUICulture = CultureInfo.InvariantCulture;

                        lock (root)
                        {
                            ++count;
                            Console.WriteLine("Hotel: {0} / {1} - {2} ({3})\t", count, numHotels, hotel.Name, hotel.Id);
                        }

                        if (configuration.Optimizer != Sunhotels.Export.ConfigurationOptimizer.splitted || !string.IsNullOrEmpty(configuration.AvailabilityCacheOutput))
                        {
                            GetAvailability(hotel);
                        }

                        if (configuration.Optimizer != Sunhotels.Export.ConfigurationOptimizer.splitted || !string.IsNullOrEmpty(configuration.DiscountCacheOutput))
                        {
                            GetDiscounts(hotel);
                        }

                        if (configuration.Optimizer == Sunhotels.Export.ConfigurationOptimizer.splitted && !string.IsNullOrEmpty(configuration.RoomNotesOutput))
                        {
                            GetRoomNotes(hotel);
                        }
                        if (configuration.Optimizer == Sunhotels.Export.ConfigurationOptimizer.splitted && !string.IsNullOrEmpty(configuration.HotelNotesOutput))
                        {
                            GetHotelNotes(hotel);
                        }

                        if (UseRoomGroups())
                        {
                            GetRoomGroups(hotel, roomGroups);
                        }

                        // Get the policy per hotel
                        if (configuration.Optimizer != Sunhotels.Export.ConfigurationOptimizer.splitted || !string.IsNullOrEmpty(configuration.CancellationPolicyCacheOutput))
                        {
                            cancellationRulesManager.GetPolicies(hotel);
                        }

                        // Check the availability per hotel 
                        lock (root)
                        {
                            Optimizer.OptimizeHotel(root, hotel);
                        }

                        if (hotel.Rooms?.Count > 0)
                        {
                            DetailedLogIncluded("Hotel/rooms included", $"{hotel.Id}/{hotel.Rooms.Count}");
                            DetailedLogIncludedStatistics(1, hotel.Rooms.Count);
                        }
                    });

                    var hRemove = p.Hotels.Where(
                            h =>
                                (h.Value.Rooms == null || h.Value.Rooms.Count == 0) &&
                                (h.Value.RoomGroups == null || h.Value.RoomGroups.Count == 0 || !h.Value.RoomGroups.Any(rg => rg.Value.Rooms != null && rg.Value.Rooms.Count > 0))
                        ).Select(h => h.Key).ToArray();
                    foreach (var key in hRemove)
                    {
                        p.Hotels.Remove(key);
                    }
                }
            });

            if (configuration.Version >= 170)
            {
                var allRooms = root.AllHotels().SelectMany(hotel => hotel.Rooms.Values);
                Console.WriteLine("Start load room configuration!");
                Task.Run(async () => { await roomConfigurationProvider.FillRoomConfigurationAsync(allRooms, root, configuration.Language); }).Wait();
                roomConfigurationProvider.FillRootRoomConfiguration(root);
                Console.WriteLine("Finish load room configuration!");
            }

            sw.Stop();
            long elapsedAverageTime = 1;
            if (numHotels > 0)
                elapsedAverageTime = sw.ElapsedMilliseconds / numHotels;
            Console.WriteLine("Fetching Hotel data done.\nAverage time/hotel (ms): {0}", elapsedAverageTime);

            DetailedLogFlush();

            Console.WriteLine("Start : {0}", dStart);
            Console.WriteLine("End   : {0}", DateTime.Now);
        }

        private bool UseRoomGroups()
        {
            return (configuration.Optimizer == Sunhotels.Export.ConfigurationOptimizer.splitted) && (!string.IsNullOrEmpty(configuration.RoomGroupsOutput));
        }

        private void GetRoomGroups(Hotel hotel, ConcurrentDictionary<int, List<AvailabilityExportDataSet.roomGroupRow>> roomGroups)
        {
            var hotelId = int.Parse(hotel.Id);

            List<AvailabilityExportDataSet.roomGroupRow> rGroups;
            if (!roomGroups.TryGetValue(hotelId, out rGroups))
            {
                return;
            }

            foreach (var roomGroup in rGroups)
            {
                var roomId = roomGroup.roomId.ToString(_invariantNumberFormat);
                Room room;

                if (!hotel.Rooms.TryGetValue(roomId, out room))
                {
                    continue;
                }

                var groupId = roomGroup.groupId.ToString(_invariantNumberFormat);

                RoomGroup hotelRoomGroup;
                if (hotel.RoomGroups.TryGetValue(groupId, out hotelRoomGroup))
                {
                    hotelRoomGroup.Rooms.Add(roomId, room);
                }
                else
                {
                    hotelRoomGroup = new RoomGroup
                    {
                        RoomGroupId = groupId,
                        Rooms = new Dictionary<string, Room> { { roomId, room } }
                    };

                    hotel.RoomGroups.Add(groupId, hotelRoomGroup);
                }
            }
        }

        private Dictionary<string, List<string>> GetHotels(Root root)
        {
            var t = new hotelTableAdapter()
                .GetData(configuration.SpecificDestinations, int.Parse(configuration.UserId), Agent.isPushOut);

            var hotelsSuspended = HotelsSuspended();
            var hotelsBlockedByAgentAndBrand = AgentAndBrandHotelBlocks(Agent.id, Agent.SH_address);

            var resorts = root.Places.SelectMany(c => c.Places).SelectMany(d => d.Places).ToList();

            foreach (var hRow in t)
            {
                var hotelId = hRow.id;
                if (hotelsSuspended.Contains(hotelId))
                {
                    DetailedLogExcluded("Hotels filtered out because are suspended", hotelId.ToString());
                    DetailedLogExcludedStatistics(1, 0);
                    continue;
                }

                if (hotelsBlockedByAgentAndBrand.Contains(hotelId))
                {
                    DetailedLogExcluded("Hotels filtered out because are blocked by agent or brand", hotelId.ToString());
                    DetailedLogExcludedStatistics(1, 0);
                    continue;
                }

                if (_settings.BlockedHotels.Contains(hotelId))
                {
                    DetailedLogExcluded("Hotels filtered out because are in blockedHotels list", hotelId.ToString());
                    DetailedLogExcludedStatistics(1, 0);
                    continue;
                }

                var placeId = hRow.resort.ToString();
                var currentPlace = resorts.FirstOrDefault(r => r.Id == placeId);
                if (currentPlace == null)
                {
                    DetailedLogExcluded("Hotels filtered out because Resort not found in Places", hotelId.ToString());
                    DetailedLogExcludedStatistics(1, 0);
                    continue;
                }

                if (hRow.ShowinEN <= 0)
                {
                    DetailedLogExcluded("Hotels filtered out because ShowinEN is false", hotelId.ToString());
                    DetailedLogExcludedStatistics(1, 0);
                    continue;
                }
                //if (Agent.cc == "dk" && (hRow.ShowinDKxml <= 0 || hRow.ShowinDK <= 0)) continue;
                //if (Agent.cc == "en" && (hRow.ShowinENxml <= 0 || hRow.ShowinEN <= 0)) continue;
                //if (Agent.cc == "fi" && (hRow.ShowinFIxml <= 0 || hRow.ShowinFI <= 0)) continue;
                //if (Agent.cc == "fr" && (hRow.ShowinFRxml <= 0 || hRow.ShowinFR <= 0)) continue;
                //if (Agent.cc == "ge" && (hRow.ShowinGExml <= 0 || hRow.ShowinGE <= 0)) continue;
                //if (Agent.cc == "no" && (hRow.ShowinNOxml <= 0 || hRow.ShowinNO <= 0)) continue;
                //if (Agent.cc == "pl" && (hRow.ShowinPLxml <= 0 || hRow.ShowinPL <= 0)) continue;
                //if (Agent.cc == "ru" && (hRow.ShowinRUxml <= 0 || hRow.ShowinRU <= 0)) continue;
                //if (Agent.cc == "es" && (hRow.ShowinESxml <= 0 || hRow.ShowinES <= 0)) continue;
                //if (Agent.cc == "sv" && (hRow.ShowinSVxml <= 0 || hRow.ShowinSV <= 0)) continue;
                //if (Agent.cc == "it" && (hRow.ShowinITxml <= 0 || hRow.ShowinIT <= 0)) continue;
                //if (Agent.cc == "zh" && (hRow.ShowinZHxml <= 0 || hRow.ShowinZH <= 0)) continue;

                var idAsString = hotelId.ToString(CultureInfo.InvariantCulture);
                var hotel = new Hotel { Id = idAsString, Name = hRow.hotelName, PlaceId = placeId };
                currentPlace.Hotels.Add(idAsString, hotel);
            }

            var hotelsFilteredByRoomProvider = HotelsFilteredByRoomProvider(root.AllHotels());
            var hotelList = hotelsFilteredByRoomProvider.Keys;
            var resortsWithHotels = resorts.Where(r => r.Hotels.Count > 0);
            foreach (var resort in resortsWithHotels)
            {
                resort.Hotels.Keys
                    .Where(hotelId => !hotelList.Contains(hotelId))
                    .ToList()
                    .ForEach(hotelId => resort.Hotels.Remove(hotelId));
            }

            if (configuration.Optimizer != Sunhotels.Export.ConfigurationOptimizer.splitted || !string.IsNullOrEmpty(this.configuration.CancellationPolicyCacheOutput))
            {
                cancellationRulesManager.LoadCancelationRulesHotelRoom(hotelsFilteredByRoomProvider, GeneralTimeout);
            }

            return hotelsFilteredByRoomProvider;
        }

        private void GetAvailability(Hotel h)
        {
            var hotelId = int.Parse(h.Id);
            var ta = new availabilityTableAdapter { CommandTimeout = GeneralTimeout };
            AvailabilityExportDataSet.availabilityDataTable availability = new AvailabilityExportDataSet.availabilityDataTable();

            if (_settings.UseSearchCacheFiles && _cache != null)
            {
                var roomIds = _cache.hotelIdRoomIds.Get(hotelId).ToList();

                roomIds.Where(roomId => !hotelRoomList[h.Id].Contains(roomId.ToString()))
                    .ToList()
                    .ForEach(roomId => roomIds.Remove(roomId));

                foreach (var roomId in roomIds)
                {
                    if (roomRepository.weeklyRooms.Contains(roomId))
                    {
                        DetailedLogExcluded("Rooms filtered out because are a WeeklyRoom", "Hotel:" + hotelId, "Room:" + roomId);
                        DetailedLogExcludedStatistics(0, 1);
                        continue;
                    }
                    var availInfo = _cache.roomCalendarAvailabilityInfo.GetFirst(roomId);     
                    var prices = _cache.roomPrices.Get(roomId);
                    var roomStaticInfo = _cache.roomStaticInfo.GetFirst(roomId);
                    var hotelInfo = _cache.hotelInfo.GetFirst(hotelId);                  

                    if (availInfo == null || prices == null || roomStaticInfo.roomtypes_id == 0 || hotelInfo == null) continue;

                    if (_settings.RemoveNRF && roomStaticInfo.options.HasFlag(CacheRoomFlags.norefund))
                    {
                        DetailedLogExcluded("Rooms filtered out because are NonRefundable", "Hotel:" + hotelId, "Room:" + roomId);
                        DetailedLogExcludedStatistics(0, 1);
                        continue;
                    }

                    var roomUnitsCollection = (availInfo.Units.ToDictionary(u => u.UnitsLeft, u => u.ApplicableDates));

                    RemoveInvalidDays(roomUnitsCollection);                    

                    RemoveDaysBlocked(roomId, roomUnitsCollection, roomRepository);

                    if (roomUnitsCollection.Count == 0)
                    {
                        DetailedLogExcluded("Rooms filtered out because roomUnits == 0", "Hotel:" + hotelId, "Room:" + roomId);
                        DetailedLogExcludedStatistics(0, 1);
                        continue;
                    }

                    int roomProviderId = Convert.ToInt32(roomStaticInfo.roomprovider_id);

                    XMLProviderDefinition xmlProvider = null;
                    DateTime? minReleaseDate = null;

                    if (xmlProviders != null && xmlProviders.TryGetValue(roomStaticInfo.XMLProvider, out xmlProvider))
                        minReleaseDate = today.AddDays(xmlProvider.MinRelease);

                    foreach (var roomUnitsItem in roomUnitsCollection)
                    {
                        DateRangeCollection dates = roomUnitsItem.Value;
                        var units = roomUnitsItem.Key;

                        foreach (var date in dates)
                        {
                            int priceId = prices.Where(p => p.dates.Contains(date)).Select(p => p.Id).FirstOrDefault();
                            var priceObject = _cache.priceLookup[priceId];
                            var realUnits = units;

                            if (minReleaseDate.HasValue && date <= minReleaseDate)
                                realUnits -= xmlProvider.MinUnits;

                            CampaignRepository.CampaignResult campaign = campaignRepository.GetCampaignsByAgentRoomProviderAndHotel(Agent, roomProviderId, hotelInfo, date);

                            double childBreakfast = priceObject.ChildBreakfast / CacheCalendarPriceObject.PriceFactor;
                            double childHalfBoard = priceObject.ChildHalfBoard / CacheCalendarPriceObject.PriceFactor;
                            double childFullBoard = priceObject.ChildFullBoard / CacheCalendarPriceObject.PriceFactor;
                            double childAllBoards = priceObject.ChildAllBoards / CacheCalendarPriceObject.PriceFactor;

                            AvailabilityExportDataSet.availabilityRow availabilityRow = (AvailabilityExportDataSet.availabilityRow)availability.NewRow();

                            availabilityRow["ID"] = roomId;
                            availabilityRow["RoomTypes_ID"] = roomStaticInfo.roomtypes_id;
                            availabilityRow["Datum"] = date;
                            availabilityRow["Beds"] = roomStaticInfo.beds;
                            availabilityRow["ExtraBeds"] = roomStaticInfo.extraBeds;
                            availabilityRow["Units"] = realUnits;
                            availabilityRow["MinDays"] = availInfo.MinMaxDays.Where(m => m.ApplicableDates.Contains(date)).Select(m => m.MinDays).FirstOrDefault();
                            availabilityRow["ReleaseTime"] = priceObject.releaseUniversalHours / 24;
                            availabilityRow["Price"] = Convert.ToDecimal(priceObject.d_price + (campaign.percent / 100.0 * priceObject.d_price) + campaign.value);
                            availabilityRow["Meals"] = GetMealType(priceObject.MealOrder);
                            availabilityRow["ExtraBedPrice"] = Convert.ToDecimal(priceObject.d_extrabedprice + (campaign.percent / 100.0 * priceObject.d_extrabedprice));
                            availabilityRow["ExtraBedPriceAdult"] = Convert.ToDecimal(priceObject.d_extrabedpriceadult + (campaign.percent / 100.0 * priceObject.d_extrabedpriceadult));
                            availabilityRow["Brf"] = Convert.ToDecimal(priceObject.d_Breakfast + (campaign.percent / 100.0 * priceObject.d_Breakfast));
                            availabilityRow["ChildBrf"] = Convert.ToDecimal(childBreakfast + (campaign.percent / 100.0 * childBreakfast));
                            availabilityRow["HB"] = Convert.ToDecimal(priceObject.d_HalfBoard + (campaign.percent / 100.0 * priceObject.d_HalfBoard));
                            availabilityRow["ChildHB"] = Convert.ToDecimal(childHalfBoard + (campaign.percent / 100.0 * childHalfBoard));
                            availabilityRow["FB"] = Convert.ToDecimal(priceObject.d_FullBoard + (campaign.percent / 100.0 * priceObject.d_FullBoard));
                            availabilityRow["ChildFB"] = Convert.ToDecimal(childFullBoard + (campaign.percent / 100.0 * childFullBoard));
                            availabilityRow["AllBoards"] = Convert.ToDecimal(priceObject.d_AllBoards + (campaign.percent / 100.0 * priceObject.d_AllBoards));
                            availabilityRow["ChildAllBoards"] = Convert.ToDecimal(childAllBoards + (campaign.percent / 100.0 * childAllBoards));

                            availability.Rows.Add(availabilityRow);
                        }    
                    }
                }
            }
            else
            {
                try
                {
                    ta.Connection.Open();
                    var cmd = new SqlCommand("SET ARITHABORT ON", ta.Connection);
                    cmd.ExecuteNonQuery();
                    availability = ta.GetData(hotelId,
                                              Agent.id,
                                              _settings.HideXMLRooms,
                                              _settings.HideManualRooms,
                                              _settings.ShowXmlOnlyIfMissingManual,
                                              true,
                                              false,
                                              Agent.isPushOut);
                }
                finally
                {
                    if (ta.Connection.State == ConnectionState.Open)
                    {
                        ta.Connection.Close();
                    }
                }
            }

            var endDate = DateTime.Today.AddDays(configuration.NrDaysInCache);

            var pushOutDiscounts = new Dictionary<int, Dictionary<DateTime, PushOutDiscount>>();
            Dictionary<DateTime, PushOutDiscount> roomPushOutDiscounts;

            if (Agent.isPushOut)
            {
                pushOutDiscounts = GetPushOutDiscounts(hotelId, endDate);
            }

            foreach (var row in availability)
            {
                if (row.datum > endDate) continue;
                
                var roomTypeId = String.Format("{0}.{1}.{2}", row.roomtypes_id, row.beds, row.extraBeds);
                // Add room type if its not found
                lock (root.RoomTypes)
                    if (!root.RoomTypes.ContainsKey(roomTypeId))
                    {
                        root.RoomTypes.Add(roomTypeId, new RoomType { Type_Id = roomTypeId, Beds = row.beds, ExtraBeds = row.extraBeds });
                    }

                Room room;
                // Add the room if its not found
                lock (h.Rooms)
                    if (!h.Rooms.ContainsKey(row.ID.ToString(_invariantNumberFormat)))
                    {
                        room = new Room { RoomId = row.ID.ToString(_invariantNumberFormat), TypeId = roomTypeId };
                        h.Rooms.Add(room.RoomId, room);
                    }
                    else
                    {
                        room = h.Rooms[row.ID.ToString(_invariantNumberFormat)];
                    }

                PushOutDiscount pushOutDiscount = null;

                if (pushOutDiscounts != null && pushOutDiscounts.TryGetValue(row.ID, out roomPushOutDiscounts))
                {
                    roomPushOutDiscounts.TryGetValue(row.datum, out pushOutDiscount);
                }

                var period = new AvailabilityPeriod
                {
                    DateFrom = row.datum,
                    DateTo = row.datum,
                    AvailabilityAvailableUnits = (short)row.units,
                    MinimumStay = (short)row.MinDays,
                    ReleaseDays = (short)row.releaseTime,
                    Pricing_BasePrice = Math.Round(GetPriceWithDiscount(Agent.CalculatePrice(row.price), pushOutDiscount, PriceType.Base), 2).ToString("f2"),
                    Pricing_BaseIncludedBoard = GetBoardTypes(row.meals),
                    Pricing_BookableBoardId = row.meals
                };

                if (configuration.MaxUnitsImported.HasValue)
                {
                    period.AvailabilityAvailableUnits = period.AvailabilityAvailableUnits > configuration.MaxUnitsImported.Value
                                                            ? configuration.MaxUnitsImported.Value
                                                            : period.AvailabilityAvailableUnits;
                }
                if (configuration.ReleaseDaysAdded.HasValue)
                {
                    period.ReleaseDays += configuration.ReleaseDaysAdded.Value;
                }

                if (row.extrabedPriceAdult > 0)
                    period.Pricing_ExtrabedAdult = Math.Round(GetPriceWithDiscount(Agent.CalculatePrice(row.extrabedPriceAdult), pushOutDiscount, PriceType.ExtraBed), 2).ToString("f2");
                if (row.extraBeds > 0)
                    period.Pricing_ExtrabedChild = Math.Round(GetPriceWithDiscount(Agent.CalculatePrice(row.extrabedPrice), pushOutDiscount, PriceType.ExtraBed), 2).ToString("f2");

                period.AdditionalBoards = GetAdditionalBoards(row, pushOutDiscount);
                room.AvailabilityPeriods.Add(period.DateFrom, period);
            }
        }

        private enum PriceType { Base, ExtraBed, Meal };

        private decimal GetPriceWithDiscount(decimal price, PushOutDiscount pushOutDiscount, PriceType type)
        {
            if (pushOutDiscount == null)
            {
                return price;
            }

            switch (type)
            {
                case PriceType.Base:
                    return price * pushOutDiscount.DiscountMultiplier;
                case PriceType.ExtraBed:
                    return price * (pushOutDiscount.IncludeExtraBed ? pushOutDiscount.DiscountMultiplier : 1);
                case PriceType.Meal:
                    return price * (pushOutDiscount.IncludeMeals ? pushOutDiscount.DiscountMultiplier : 1);
            }

            return price;
        }

        private static Dictionary<int, Dictionary<DateTime, PushOutDiscount>> GetPushOutDiscounts(int hotelId, DateTime endDate)
        {
            var pushOutDiscounts = new Dictionary<int, Dictionary<DateTime, PushOutDiscount>>();

            var discounts = new PushOutEarlyBookingDiscountsTableAdapter().GetData(Agent.id, hotelId, DateTime.Today, endDate, RoomId: null);

            foreach (var discountRow in discounts)
            {
                Dictionary<DateTime, PushOutDiscount> roomDiscount;

                if (!pushOutDiscounts.TryGetValue(discountRow.room_id, out roomDiscount))
                {
                    roomDiscount = new Dictionary<DateTime, PushOutDiscount>();
                    pushOutDiscounts.Add(discountRow.room_id, roomDiscount);
                }

                var currentRowDiscount = new PushOutDiscount
                {
                    DiscountPercentage = (decimal)discountRow.discountPercentage,
                    IncludeExtraBed = discountRow.includeExtraBed,
                    IncludeMeals = discountRow.includeMeal
                };
                PushOutDiscount pushOutDiscount;

                if (!roomDiscount.TryGetValue(discountRow.date, out pushOutDiscount))
                {
                    roomDiscount.Add(discountRow.date, currentRowDiscount);
                }
                else if (currentRowDiscount.IsBetter(pushOutDiscount))
                {
                    roomDiscount[discountRow.date] = currentRowDiscount;
                }
            }

            return pushOutDiscounts;
        }

        private void GetDiscounts(Hotel h)
        {
            if (h.Rooms.Count <= 0)
            {
                return;
            }

            var roomIdTable = new DataTable();
            roomIdTable.Columns.Add("Id", typeof(int));

            foreach (var roomIdString in h.Rooms.Keys)
            {
                roomIdTable.Rows.Add(Int32.Parse(roomIdString));
            }

            var discountTableAdapter = new discountTableAdapter { CommandTimeout = GeneralTimeout };
            var discountLookup = discountTableAdapter.GetData(roomIdTable).ToLookup(r => r.room_id, r => r);

            foreach (var room in h.Rooms.Values)
            {
                foreach (var discountRow in discountLookup[Int32.Parse(room.RoomId)])
                {
                    AvailabilityPeriod ap;
                    if (room.AvailabilityPeriods.TryGetValue(discountRow.datum, out ap))
                    {
                        ap.Discounts.TryAdd(CreateDiscount(discountRow));
                    }
                }
            }
        }

        private Discount CreateDiscount(AvailabilityExportDataSet.discountRow discount)
        {
            switch (discount.discount_type_id)
            {
                case 1: // value_percent
                    return Discount.ValuePercent(discount.discount_min_days, Convert.ToDecimal(discount.discount_value), discount.discount_period_id == 0 ? (int?)null : discount.discount_period_id, discount.accumulativeGroup, discount.checkinBased, discount.includeMeal, discount.includeExtraBed);
                case 2: // value_amount
                    return Discount.ValueAmount(discount.discount_min_days, Convert.ToDecimal(discount.discount_value), discount.discount_period_id == 0 ? (int?)null : discount.discount_period_id, discount.accumulativeGroup, discount.checkinBased, discount.includeMeal, discount.includeExtraBed);
                case 3: // early_booking_percent
                    return Discount.EarlyBookingProcent(discount.discount_min_days, Convert.ToDecimal(discount.discount_value), discount.discount_period_id == 0 ? (int?)null : discount.discount_period_id, discount.accumulativeGroup, discount.checkinBased, discount.includeMeal, discount.includeExtraBed);
                case 4: // early_booking_amount
                    return Discount.EarlyBookingAmount(discount.discount_min_days, Convert.ToDecimal(discount.discount_value), discount.discount_period_id == 0 ? (int?)null : discount.discount_period_id, discount.accumulativeGroup, discount.checkinBased, discount.includeMeal, discount.includeExtraBed);
                case 5: // pay_stay_amount
                    decimal value = Convert.ToDecimal(Math.Floor(discount.discount_value));
                    if (value < 1) return null;
                    return Discount.PayStay(discount.discount_min_days, discount.discount_max_days == 0 ? null : (int?)(discount.discount_max_days / discount.discount_min_days), Convert.ToDecimal(discount.discount_value), discount.discount_period_id == 0 ? (int?)null : discount.discount_period_id, discount.accumulativeGroup, discount.checkinBased, discount.includeMeal, discount.includeExtraBed);
                case 6: // min_max_percent
                    return Discount.MinMaxPercent(discount.discount_min_days, discount.discount_max_days, Convert.ToDecimal(discount.discount_value), discount.discount_period_id == 0 ? (int?)null : discount.discount_period_id, discount.accumulativeGroup, discount.checkinBased, discount.includeMeal, discount.includeExtraBed);
                default:
                    throw new NotImplementedException(String.Format("Discount type with id {0} is not implemented.", discount.discount_type_id));
            }
        }

        private List<AvailabilityPeriod.AdditionalBoard> GetAdditionalBoards(AvailabilityExportDataSet.availabilityRow row, PushOutDiscount pushOutDiscount)
        {
            var boardList = new List<AvailabilityPeriod.AdditionalBoard>();

            if (row.Brf > 0) boardList.Add(new AvailabilityPeriod.AdditionalBoard(GetBoardTypes("Breakfast"), (Math.Ceiling(GetPriceWithDiscount(Agent.CalculatePrice(row.Brf), pushOutDiscount, PriceType.Meal) * 100) / 100).ToString(_invariantNumberFormat), (Math.Ceiling(GetPriceWithDiscount(Agent.CalculatePrice(row.ChildBrf), pushOutDiscount, PriceType.Meal) * 100) / 100).ToString(_invariantNumberFormat), "Breakfast"));
            if (row.HB > 0) boardList.Add(new AvailabilityPeriod.AdditionalBoard(GetBoardTypes("Half Board"), (Math.Ceiling(GetPriceWithDiscount(Agent.CalculatePrice(row.HB), pushOutDiscount, PriceType.Meal) * 100) / 100).ToString(_invariantNumberFormat), (Math.Ceiling(GetPriceWithDiscount(Agent.CalculatePrice(row.ChildHB), pushOutDiscount, PriceType.Meal) * 100) / 100).ToString(_invariantNumberFormat), "Half Board"));
            if (row.FB > 0) boardList.Add(new AvailabilityPeriod.AdditionalBoard(GetBoardTypes("Full Board"), (Math.Ceiling(GetPriceWithDiscount(Agent.CalculatePrice(row.FB), pushOutDiscount, PriceType.Meal) * 100) / 100).ToString(_invariantNumberFormat), (Math.Ceiling(GetPriceWithDiscount(Agent.CalculatePrice(row.ChildFB), pushOutDiscount, PriceType.Meal) * 100) / 100).ToString(_invariantNumberFormat), "Full Board"));
            if (row.AllBoards > 0) boardList.Add(new AvailabilityPeriod.AdditionalBoard(GetBoardTypes("All"), (Math.Ceiling(GetPriceWithDiscount(Agent.CalculatePrice(row.AllBoards), pushOutDiscount, PriceType.Meal) * 100) / 100).ToString(_invariantNumberFormat), (Math.Ceiling(GetPriceWithDiscount(Agent.CalculatePrice(row.ChildAllBoards), pushOutDiscount, PriceType.Meal) * 100) / 100).ToString(_invariantNumberFormat), "All"));

            return boardList;
        }

        private string GetBoardTypes(string bookableName)
        {
            switch (bookableName)
            {
                case "Breakfast": return "breakfast";
                case "Half Board": return "half_board";
                case "Full Board": return "full_board";
                case "All": return "all_inclusive";
                default: return "none";
            }
        }

        private string GetMealType(Meals meal)
        {
            switch (meal)
            {
                case Meals.BREAKFAST:
                    return "Breakfast";
                case Meals.HALF_BOARD:
                    return "Half Board";
                case Meals.FULL_BOARD:
                    return "Full Board";
                case Meals.ALL_INCLUSIVE:
                    return "All";
                default:
                    return "none";
            }
        }

        private void GetRoomNotes(Hotel hotel)
        {
            var from = DateTime.Today;
            var to = from.AddDays(configuration.NrDaysInCache);
            var today = DateTime.Today;

            var groupedNotes = new RoomNoteTableAdapter().GetData(int.Parse(hotel.Id), to, from).GroupBy(n => n.RoomId);

            foreach (var roomNotes in groupedNotes)
            {
                var notes = roomNotes.SelectMany(note => Note.Create(note.Id, note.StartDate, note.EndDate, configuration.NrDaysInCache, today)).ToList();
                var roomId = roomNotes.Key.ToString(_invariantNumberFormat);

                lock (hotel.Rooms)
                    if (hotel.Rooms.ContainsKey(roomId))
                    {
                        hotel.Rooms[roomId].Notes = notes;
                    }
            }
        }

        private void GetHotelNotes(Hotel hotel)
        {
            var from = DateTime.Today;
            var to = from.AddDays(configuration.NrDaysInCache);
            var today = DateTime.Today;

            var hotelNotes = new HotelNoteTableAdapter().GetData(int.Parse(hotel.Id), from, to);

            foreach (var note in hotelNotes)
            {
                var notes = Note.Create(note.Id, note.StartDate, note.EndDate, configuration.NrDaysInCache, today);

                if (hotel.Notes == null)
                    hotel.Notes = notes;
                else
                    hotel.Notes.AddRange(notes);
            }
        }

        /// <summary>
        /// Get countries, destinations and resorts
        /// </summary>
        private void GetPlaces(Root root)
        {
            GetCountry(root);
            GetDestinations(root);
            GetResorts(root);
        }

        /// <summary>
        /// Gets all countries from the database
        /// </summary>
        private void GetCountry(Root root)
        {
            using (var connection = new SqlConnection(Properties.Settings.Default.sunhotelsConnectionString))
            {
                using (var command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandText = @"
                        SELECT
                            id,
                            ISNULL(NULLIF(dbo.GetLangCountry(id, @lang), ''), dbo.GetLangCountry(id, N'en')) AS CountryName,
                            countrycode
                        FROM countries
                        WHERE (suspend IS NULL) OR (suspend = 0)";
                    command.Parameters.Add(new SqlParameter("lang", Agent.cc));
                    connection.Open();
                    using (var rdr = command.ExecuteReader(CommandBehavior.SequentialAccess))
                    {
                        while (rdr.Read())
                        {
                            var cRow = rdr;
                            var country = new Place();
                            country.Id = cRow["id"].ToString();
                            country.Description = ((string)cRow["CountryName"]).Trim();
                            country.Codes.Add(new PlaceCodes { Type = "ISO-COUNTRY", Value = (string)cRow["countrycode"] });
                            root.Places.Add(country);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Gets all destinations from the database
        /// </summary>
        private void GetDestinations(Root root)
        {
            foreach (var countryList in MultipleQueryLists(root.Places.Select(c => c.Id)))
            {
                using (var connection = new SqlConnection(Properties.Settings.Default.sunhotelsConnectionString))
                {
                    using (var command = new SqlCommand())
                    {
                        command.Connection = connection;
                        command.CommandText = @"
                            SELECT
                                Destinations.ID,
                                ISNULL(NULLIF(dbo.GetLangDestinationByDestinationId(Destinations.ID, @lang), ''), dbo.GetLangDestinationByDestinationId(Destinations.ID, N'en')) AS DestinationName,
                                countries.id as countryId
                            FROM Destinations
                                INNER JOIN countries ON Destinations.country = countries.id
                            WHERE
                                (ISNULL(Destinations.suspend, 0) = 0) AND(ISNULL(countries.suspend, 0) = 0)
                                AND (countries.id IN (" + countryList + "))";
                        command.Parameters.Add(new SqlParameter("lang", Agent.cc));
                        connection.Open();
                        using (var rdr = command.ExecuteReader(CommandBehavior.SequentialAccess))
                        {
                            while (rdr.Read())
                            {
                                var dRow = rdr;
                                var destId = dRow["ID"].ToString();
                                var destName = ((string)dRow["DestinationName"]).Trim();
                                var countryId = dRow["countryId"].ToString();
                                var countryPlace = root.Places.FirstOrDefault(c => c.Id == countryId);
                                if (countryPlace != null)
                                {
                                    var dest = new Place();
                                    dest.Id = destId;
                                    dest.Description = destName;
                                    countryPlace.Places.Add(dest);
                                }
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Gets all resorts from the database
        /// </summary>
        private void GetResorts(Root root)
        {
            var destinations = root.Places.SelectMany(c => c.Places).ToList();
            foreach (var destinationList in MultipleQueryLists(destinations.Select(d => d.Id.ToString())))
            {
                using (var connection = new SqlConnection(Properties.Settings.Default.sunhotelsConnectionString))
                {
                    using (var command = new SqlCommand())
                    {
                        command.Connection = connection;
                        command.CommandText = @"
                        SELECT
                            Resorts.ID,
                            ISNULL(NULLIF (dbo.GetLangResortByResortId(Resorts.ID, @lang), ''), dbo.GetLangResortByResortId(Resorts.ID, N'en')) AS ResortName,
                            Resorts.destId
                        FROM Resorts
                            INNER JOIN Destinations ON Destinations.ID = Resorts.destId
                        WHERE
                            (ISNULL(Resorts.suspend, 0) = 0)
                            AND (Resorts.destId IN (" + destinationList + "))";
                        command.Parameters.Add(new SqlParameter("lang", Agent.cc));
                        connection.Open();
                        using (var rdr = command.ExecuteReader(CommandBehavior.SequentialAccess))
                        {
                            while (rdr.Read())
                            {
                                var resRow = rdr;
                                var resortId = resRow["ID"].ToString();
                                var resortName = ((string)resRow["ResortName"]).Trim();
                                var destId = resRow["destId"].ToString();
                                var destinationPlace = destinations.FirstOrDefault(d => d.Id == destId);
                                if (destinationPlace != null)
                                {
                                    var reso = new Place();
                                    reso.Id = resortId;
                                    reso.Description = resortName;
                                    destinationPlace.Places.Add(reso);
                                }
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Splits the source list (individual elements) into several string lists (joined), each list with maximum QUERY_ELEMENTS, joined by ','
        /// </summary>
        /// <param name="sourceList"></param>
        /// <returns></returns>
        public static List<string> MultipleQueryLists(IEnumerable<string> sourceList)
        {
            var tmpLists = new Dictionary<int, List<string>>();
            if (sourceList != null)
            {
                foreach (var element in sourceList)
                {
                    if (tmpLists.Count == 0)
                    {
                        tmpLists.Add(0, new List<string>());
                    }
                    //Limit queries by a certain number of elements
                    if (tmpLists[tmpLists.Count - 1].Count > QueryElements)
                    {
                        tmpLists.Add(tmpLists.Count, new List<string>());
                    }
                    tmpLists[tmpLists.Count - 1].Add(element);
                }
            }
            var retLists = new List<string>();
            foreach (var tmp in tmpLists.Values)
            {
                retLists.Add(String.Join(",", tmp));
            }
            return retLists;
        }

        private List<int> HotelsSuspended()
        {
            var hotelsSuspended = new List<int>();
            using (var connection = new SqlConnection(Properties.Settings.Default.sunhotelsConnectionString))
            {
                using (var command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandText = "SELECT id FROM dbo.vHotelStatus WHERE Suspended > 0";
                    connection.Open();
                    using (var rdr = command.ExecuteReader(CommandBehavior.SequentialAccess))
                    {
                        while (rdr.Read())
                        {
                            var row = rdr;
                            var hotelId = (int)row["id"];
                            if (!hotelsSuspended.Contains(hotelId))
                            {
                                hotelsSuspended.Add(hotelId);
                            }
                        }
                    }
                }
            }
            return hotelsSuspended;
        }

        private List<int> AgentAndBrandHotelBlocks(int agentId, int brandId)
        {
            var blockedHotels = new List<int>();
            using (var connection = new SqlConnection(Properties.Settings.Default.sunhotelsConnectionString))
            {
                using (var command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandText = "[dbo].[spWSDL_GetAllHotelBlocks]";
                    command.CommandType = CommandType.StoredProcedure;
                    connection.Open();
                    using (var rdr = command.ExecuteReader(CommandBehavior.SequentialAccess))
                    {
                        while (rdr.Read())
                        {
                            int? agent = rdr.IsDBNull(0) ? (int?)null : rdr.GetInt32(0);
                            int? brand = rdr.IsDBNull(1) ? (int?)null : rdr.GetInt32(1);
                            int hotel = rdr.GetInt32(2);

                            if (agent.HasValue && agent.Value == agentId)
                            {
                                if (!blockedHotels.Contains(hotel))
                                    blockedHotels.Add(hotel);
                            }
                            else if (brand.HasValue && brand.Value == brandId)
                            {
                                if (!blockedHotels.Contains(hotel))
                                    blockedHotels.Add(hotel);
                            }
                        }
                    }
                }
            }
            return blockedHotels;
        }

        private List<int> RoomProviderMarketBlocks(int agentCountryId)
        {
            var roomProviderMarketBlocks = new List<int>();
            using (var connection = new SqlConnection(Properties.Settings.Default.sunhotelsConnectionString))
            {
                using (var command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandText = "EXEC spWSDL_GetRoomProviderMarketBlocks";
                    connection.Open();
                    using (var rdr = command.ExecuteReader(CommandBehavior.SequentialAccess))
                    {
                        while (rdr.Read())
                        {
                            var row = rdr;
                            var roomProviderId = (int)row["roomProviderId"];
                            var countryId = (int)row["countryId"];

                            if (agentCountryId <= 0 || agentCountryId == countryId)
                            {
                                if (!roomProviderMarketBlocks.Contains(roomProviderId))
                                {
                                    roomProviderMarketBlocks.Add(roomProviderId);
                                }
                            }
                        }
                    }
                }
            }
            return roomProviderMarketBlocks;
        }

        private List<int> AgentRoomProviderBlocks(int agentId)
        {
            var agentRoomProviderBlocks = new List<int>();
            using (var connection = new SqlConnection(Properties.Settings.Default.sunhotelsConnectionString))
            {
                using (var command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandText = "SELECT roomproviderid FROM agentroomprovider_block WHERE agentid = " + agentId.ToString();
                    connection.Open();
                    using (var rdr = command.ExecuteReader(CommandBehavior.SequentialAccess))
                    {
                        while (rdr.Read())
                        {
                            var row = rdr;
                            var roomProviderId = (int)row["roomproviderid"];
                            if (!agentRoomProviderBlocks.Contains(roomProviderId))
                            {
                                agentRoomProviderBlocks.Add(roomProviderId);
                            }
                        }
                    }
                }
            }
            return agentRoomProviderBlocks;
        }

        private List<int> B2CRoomProviderBlocks()
        {
            var b2CRoomProviderBlocks = new List<int>();
            using (var connection = new SqlConnection(Properties.Settings.Default.sunhotelsConnectionString))
            {
                using (var command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandText = "SELECT id FROM roomprovider where blockB2CAgents = 1";
                    connection.Open();
                    using (var rdr = command.ExecuteReader(CommandBehavior.SequentialAccess))
                    {
                        while (rdr.Read())
                        {
                            var row = rdr;
                            var roomProviderId = (int)row["id"];
                            if (!b2CRoomProviderBlocks.Contains(roomProviderId))
                            {
                                b2CRoomProviderBlocks.Add(roomProviderId);
                            }
                        }
                    }
                }
            }
            return b2CRoomProviderBlocks;
        }

        private Dictionary<string, List<int>> RoomProviderPayableOnSpotBlocks()
        {
            var roomProvidersPayableOnSpot = new Dictionary<string, List<int>>();

            var sql = $@"
                    IF OBJECT_ID('[tempdb].[dbo].[#TMP]') IS NOT NULL BEGIN
	                    DROP TABLE [#TMP]					
                    END
                    CREATE TABLE [#TMP](hotelId INT , roomprovider_id int)
                    INSERT INTO [#TMP]  
                    EXEC spWSDL_GetRoomProviderPayableOnSpot 

                    SELECT DISTINCT hotelId, roomprovider_id
                    FROM [#TMP] 
                    
                    DROP TABLE [#TMP]";

            using (var connection = new SqlConnection(Properties.Settings.Default.sunhotelsConnectionString))
            {
                using (var command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandText = sql;
                    connection.Open();
                    using (var rdr = command.ExecuteReader(CommandBehavior.SequentialAccess))
                    {
                        while (rdr.Read())
                        {
                            List<int> roomProviders;
                            var hotelId = rdr.GetInt32(0).ToString();
                            if (!roomProvidersPayableOnSpot.TryGetValue(hotelId, out roomProviders))
                            {
                                roomProviders = new List<int>();
                            }
                            roomProviders.Add(rdr.GetInt32(1));
                            roomProvidersPayableOnSpot[hotelId] = roomProviders;
                        }
                    }
                }
            }
            return roomProvidersPayableOnSpot;
        }

        private Dictionary<string, List<string>> GetHotelRooms(IEnumerable<Hotel> allHotels, List<int> roomProviderBlocks, Dictionary<string, List<int>> hotelRoomProviderBlocks)
        {
            var hotelRooms = new Dictionary<string, List<string>>();
            foreach (var hotelList in MultipleQueryLists(allHotels.Select(h => h.Id)))
            {
                using (var connection = new SqlConnection(Properties.Settings.Default.sunhotelsConnectionString))
                {
                    using (var command = new SqlCommand())
                    {
                        command.Connection = connection;
                        command.CommandTimeout = 5 * 60;//seconds avoid time outs when database is quite busy
                        command.CommandText =
                            @"SELECT r.ID, r.hotelId, r.roomprovider_id
                            FROM rooms r WITH (NOLOCK)
                            WHERE
                                r.isLiveRoom = 0
                                AND ISNULL(r.weekly, 0) = 0
                                AND ISNULL(r.SuspendRoom, 0) = 0
                                AND ISNULL(r.deleted, 0) = 0
                                AND ISNULL(r.roomprovider_id, 0) > 0
                                AND r.hotelId IN (" + hotelList + @")
                                AND EXISTS(
                                        SELECT TOP 1 c.datum
                                        FROM calendar c WITH(NOLOCK)
                                        WHERE
                                            r.ID = c.roomId
                                            AND c.datum > (GETDATE() - 1)
				                            AND (c.units - c.bookedUnits) > 0
                                )";
                        connection.Open();
                        using (var rdr = command.ExecuteReader(CommandBehavior.SequentialAccess))
                        {
                            while (rdr.Read())
                            {
                                var row = rdr;
                                var roomId = row["ID"].ToString();
                                var hotelId = row["hotelId"].ToString();
                                var roomproviderId = (int)row["roomprovider_id"];

                                var allRoomProvidersBlocked = roomProviderBlocks;
                                List<int> roomProviders;
                                if (hotelRoomProviderBlocks.TryGetValue(hotelId, out roomProviders))
                                {
                                    allRoomProvidersBlocked = allRoomProvidersBlocked
                                        .Concat(roomProviders)
                                        .Distinct()
                                        .ToList();
                                }

                                if (!allRoomProvidersBlocked.Contains(roomproviderId))
                                {
                                    List<string> roomList;
                                    if (!hotelRooms.TryGetValue(hotelId, out roomList))
                                    {
                                        roomList = new List<string>();
                                        hotelRooms.Add(hotelId, roomList);
                                    }
                                    if (!roomList.Contains(roomId))
                                    {
                                        roomList.Add(roomId);
                                    }
                                }
                                else
                                {
                                    DetailedLogExcluded("Rooms filtered out because roomprovider is blocked", "RoomProviderId:" + roomproviderId, "Hotel:" + hotelId, "Room:" + roomId);
                                    DetailedLogExcludedStatistics(0, 1);
                                }
                            }
                        }
                    }
                }
            }
            return hotelRooms;
        }

        private Dictionary<string, List<string>> HotelsFilteredByRoomProvider(IEnumerable<Hotel> allHotels)
        {
            var agentRoomProviderBlocks = AgentRoomProviderBlocks(Agent.id);
            var roomProviderMarketBlocks = RoomProviderMarketBlocks(Agent.countryId);
            var b2CRoomProviderBlocks = Agent.B2C
                    ? B2CRoomProviderBlocks()
                    : new List<int>();
            var roomProviderPayableOnSpotBlocks = Agent.blockPayableOnSpot
                    ? RoomProviderPayableOnSpotBlocks()
                    : new Dictionary<string, List<int>>();
            var roomProviderBlocks = agentRoomProviderBlocks
                    .Concat(roomProviderMarketBlocks)
                    .Concat(b2CRoomProviderBlocks)
                    .Distinct()
                    .ToList();
            return GetHotelRooms(allHotels, roomProviderBlocks, roomProviderPayableOnSpotBlocks);
        }

        private void GetXMLProviders()
        {
            xmlProviders = new Dictionary<int, XMLProviderDefinition>();
            using (var connection = new SqlConnection(Properties.Settings.Default.sunhotelsConnectionString))
            {
                using (var command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandText = @"SELECT providerid, min_release, min_units FROM dbo.xmlProvider WHERE NOT suspend = 1 AND isLiveProvider = 0 AND min_release != 0";
                    connection.Open();
                    using (var rdr = command.ExecuteReader(CommandBehavior.SequentialAccess))
                    {
                        while (rdr.Read())
                        {
                            var xmlProviderId = rdr.GetInt32(0);
                            xmlProviders[xmlProviderId] = new XMLProviderDefinition()
                            {
                                MinRelease = (byte)rdr.GetInt32(1),
                                MinUnits = (byte)rdr.GetInt32(2)
                            };
                        }
                    }
                }
            }
        }

        private void RemoveInvalidDays(Dictionary<byte, DateRangeCollection> roomUnitsCollection)
        {
            var roomUnitsToRemove = new List<byte>();

            foreach (var units in roomUnitsCollection)
            {
                if (units.Value.First() < yesterday)
                {
                    units.Value.RemoveRange(units.Value.First(), today, true);
                }
                else
                {
                    units.Value.RemoveRange(yesterday, today, true);
                }
                if (units.Value.RangeCount == 0)
                    roomUnitsToRemove.Add(units.Key);
            }

            roomUnitsToRemove.ForEach(u => roomUnitsCollection.Remove(u));
        }

        private void RemoveDaysBlocked(int roomId, Dictionary<byte, DateRangeCollection> roomUnitsCollection, RoomRepository roomRepository)
        {
            var roomUnitsToRemove = new List<byte>();

            DateRangeCollection daysBlocked;
            if (roomRepository.calendarBlockedRooms.TryGetValue(roomId, out daysBlocked))
            {
                foreach (var roomUnitsItem in roomUnitsCollection)
                {
                    roomUnitsItem.Value.RemoveRange(daysBlocked);
                    if (roomUnitsItem.Value.RangeCount == 0)
                        roomUnitsToRemove.Add(roomUnitsItem.Key);
                }
                roomUnitsToRemove.ForEach(u => roomUnitsCollection.Remove(u));
            }
        }

        #region Detailed Log Methods

        private void DetailedLogIncluded(params string[] msg)
        {
            if (_settings.LogToFile)
            {
                lock (loggerIncluded)
                {
                    loggerIncluded.Add(msg);
                }
            }
        }

        private void DetailedLogIncludedStatistics(int numHotels, int numRooms)
        {
            if (_settings.LogToFile)
            {
                hotelsIncluded += numHotels;
                roomsIncluded += numRooms;
            }
        }

        private void DetailedLogExcluded(params string[] msg)
        {
            if (_settings.LogToFile)
            {
                lock (loggerExcluded)
                {
                    loggerExcluded.Add(msg);
                }
            }
        }

        private void DetailedLogExcludedStatistics(int numHotels, int numRooms)
        {
            if (_settings.LogToFile)
            {
                hotelsExcluded += numHotels;
                roomsExcluded += numRooms;
            }
        }

        private void DetailedLogFlush()
        {
            if (_settings.LogToFile)
            {
                loggerIncluded.AddStatistics($"Total included hotels: {hotelsIncluded}");
                loggerIncluded.AddStatistics($"Total included rooms: {roomsIncluded}");
                loggerIncluded.SerializeToFile();
                loggerExcluded.AddStatistics($"Total excluded hotels: {hotelsExcluded}");
                loggerExcluded.AddStatistics($"Total excluded rooms: {roomsExcluded}");
                loggerExcluded.SerializeToFile();
            }
        }

        #endregion
    }

    public static class ExtensionMethods
    {
        public static decimal CalculatePrice(this AvailabilityExportDataSet.agentRow agentRow, decimal originalPrice)
        {
            return originalPrice * (Convert.ToDecimal(agentRow.primaryCurrFactor) / 100);
        }
    }
}
