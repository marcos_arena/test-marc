﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SunHotels.XML.Data;

namespace SunHotels.Providers
{
    /// <summary>
    /// Contains a method for comparing two cancellation policies.
    /// </summary>
    public class CancellationPolicyComparer : IComparer<CancellationPolicy>
    {
        private bool sortByTypeId;

        public CancellationPolicyComparer(bool sortByTypeId)
        {
            this.sortByTypeId = sortByTypeId;
        }

        #region IComparer<CancellationPolicy> Members

        /// <summary>
        /// Compares two objects of the type "CancellationPolicy".
        /// </summary>
        /// <param name="x">The first cancellation policy.</param>
        /// <param name="y">The second cancellation policy.</param>
        /// <returns>A negative number if x comes before y in a relative order.</returns>
        public int Compare(CancellationPolicy x, CancellationPolicy y)
        {            
            // Sort by type ID ascending
            if (String.Compare(x.Penalty_Basis, y.Penalty_Basis) > 0 && sortByTypeId)
            {
                return 1;
            }
            else if (String.Compare(x.Penalty_Basis, y.Penalty_Basis) < 0 && sortByTypeId)
            {
                return -1;
            }
            else
            {
                // Sort by deadline descending (a deadline of null comes first)
                if (x.Deadline_Value < y.Deadline_Value || (!y.Deadline_Value.HasValue && x.Deadline_Value.HasValue))
                {
                    return 1;
                }
                else if (x.Deadline_Value > y.Deadline_Value || (!x.Deadline_Value.HasValue && y.Deadline_Value.HasValue))
                {
                    return -1;
                }
                else
                {
                    if (x.Penalty_Value < y.Penalty_Value || ((y.Penalty_Basis == "full_stay" && y.Penalty_Value == 100) && (x.Penalty_Basis != "full_stay" && x.Penalty_Value != 100)))
                    {
                        return 1;
                    }
                    else if (x.Penalty_Value > y.Penalty_Value || ((x.Penalty_Basis == "full_stay" && x.Penalty_Value == 100) && (y.Penalty_Basis != "full_stay" && y.Penalty_Value != 100)))
                    {
                        return -1;
                    }
                    else
                    {
                        return 0;
                    }
                }
            }
        }

        #endregion
    }
}
