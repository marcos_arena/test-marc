﻿namespace SunHotels.Providers
{
    public partial class AvailabilityExportDataSet
    {
        partial class agentDataTable
        {
        }

        partial class cancellation_policyDataTable
        {
        }
    }
}

namespace SunHotels.Providers.AvailabilityExportDataSetTableAdapters
{
    partial class DestinationNotesTableAdapter
    {
    }

    public partial class cancellation_policyTableAdapter
    {
        public string GetCommandText(int num)
        {
            return this.CommandCollection[num].CommandText;
        }
        public void SetCommandText(int num, string SQL)
        {
            this.CommandCollection[num].CommandText = SQL;
        }

        public void SetCommandTimeout(int num, int timeout)
        {
            this.CommandCollection[num].CommandTimeout = timeout;
        }
    }

    public partial class hotel_cancellation_policyTableAdapter
    {
        public string GetCommandText(int num)
        {
            return this.CommandCollection[num].CommandText;
        }
        public void SetCommandText(int num, string SQL)
        {
            this.CommandCollection[num].CommandText = SQL;
        }

        public void SetCommandTimeout(int num, int timeout)
        {
            this.CommandCollection[num].CommandTimeout = timeout;
        }
    }

    public partial class availabilityTableAdapter
    {
        public int CommandTimeout
        {
            set
            {
                this.InitCommandCollection();
                foreach (System.Data.IDbCommand command in this.CommandCollection)
                {
                    command.CommandTimeout = value;
                }
            }
            get
            {
                return this.CommandCollection[0].CommandTimeout;
            }
        }
    }

    public partial class discountTableAdapter
    {
        public int CommandTimeout
        {
            set
            {
                this.InitCommandCollection();
                foreach (System.Data.IDbCommand command in this.CommandCollection)
                {
                    command.CommandTimeout = value;
                }
            }
            get
            {
                return this.CommandCollection[0].CommandTimeout;
            }
        }
    }

    public partial class spWSDL_GetEffectiveCancellationPoliciesTableAdapter
    {
        public int CommandTimeout
        {
            set
            {
                this.InitCommandCollection();
                foreach (System.Data.IDbCommand command in this.CommandCollection)
                {
                    command.CommandTimeout = value;
                }
            }
            get
            {
                return this.CommandCollection[0].CommandTimeout;
            }
        }
    }
}
