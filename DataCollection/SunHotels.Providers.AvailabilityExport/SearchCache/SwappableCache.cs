﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SunHotels.Providers.Repositories
{
    public class SwappableCache<TKey, TValue>
    {
        private IDictionary<TKey, TValue[]> backing;

        public SwappableCache()
        {
            backing = new Dictionary<TKey, TValue[]>();
            //cache = new MemoryCache("_swapCache");
        }

        public void Add(TKey key, TValue value)
        {
            TValue[] array;
            lock (backing)
            {
                backing[key] = !backing.TryGetValue(key, out array)
                    ? new[] { value }
                    : array.Concat(new[] { value }).OrderBy(v => v).Distinct().ToArray();
            }
        }

        public void ReplaceAll(IDictionary<TKey, IEnumerable<TValue>> newSource)
        {
            var tmp = new Dictionary<TKey, TValue[]>(newSource.ToDictionary(f => f.Key, f => f.Value.ToArray()));
            backing = tmp;
        }

        public void ReplaceAll(IDictionary<TKey, TValue[]> newSource)
        {
            var tmp = new Dictionary<TKey, TValue[]>(newSource.ToDictionary(f => f.Key, f => f.Value));
            backing = tmp;
        }

        public void ReplaceAll(IDictionary<TKey, TValue> newSource)
        {
            var tmp = new Dictionary<TKey, TValue[]>(newSource.ToDictionary(f => f.Key, f => new[] { f.Value }));
            backing = tmp;
        }


        public void ReplaceAllSingleKey(TKey key, Func<TValue[]> factory)
        {
            var newValue = factory();
            lock (backing)
                backing[key] = newValue;
        }

        public void ReplaceAllSingleKey(TKey key, TValue[] values)
        {
            lock (backing)
                backing[key] = values;
        }

        /// <summary>
        /// For room blocks/removal of date, etc..
        /// </summary>
        /// <param name="key"></param>
        /// <param name="values"></param>
        public void RemoveSubValues(TKey key, TValue[] values)
        {

            TValue[] array;
            if (!backing.TryGetValue(key, out array))
                return;

            array = array.Except(values).ToArray();

            lock (backing)
            {
                backing[key] = array;
            }
        }


        public TValue[] Get(TKey key)
        {
            TValue[] array;
            return backing.TryGetValue(key, out array) ? array : new TValue[0];
        }

        public TValue GetFirst(TKey key)
        {
            TValue[] array;
            return backing.TryGetValue(key, out array) ? array.FirstOrDefault() : default(TValue);
        }


        public TValue GetFirstOrDefault(TKey key, TValue defaultValue)
        {
            TValue[] array;
            return backing.TryGetValue(key, out array) ? array.FirstOrDefault() : defaultValue;
        }


        public IDictionary<TKey, TValue[]> GetAll()
        {
            return backing;
            //return new ReadOnlyDictionary<TKey, TValue[]>(backing);
        }


        #region Non-Generic interface

        public TValue[] Get(object key)
        {
            return Get((TKey)key);
        }

        public void ReplaceAll(object key, Func<TValue[]> factory)
        {
            ReplaceAllSingleKey((TKey)key, factory);
        }

        public void RemoveSingle<TV>(object key, TV value) where TV : TValue, IComparable
        {
            TValue[] array;
            var k = (TKey)key;
            if (!backing.TryGetValue(k, out array))
                return;

            int index = Array.BinarySearch(array, value);
            // If value is not found and value is less than one or more elements in array, a negative number which is the bitwise complement of the index of the first element that is larger than value.
            if (index < 0)
                return;

            var array2 = new TValue[array.Length - 1];
            // index is 0 based, and we want to exclude the last value, so this is correct.

            Array.Copy(array, 0, array2, 0, index);
            Array.Copy(array, index + 1, array2, index, array.Length - index - 1);

            lock (backing)
            {
                backing[k] = array2;
            }
        }
        public void RemoveSingle(object key, TValue value)
        {
            TValue[] array;
            var k = (TKey)key;
            if (!backing.TryGetValue(k, out array))
                return;

            int index = Array.IndexOf(array, value);
            if (index == -1)
                return;

            var array2 = new TValue[array.Length - 1];
            // index is 0 based, and we want to exclude the last value, so this is correct.

            Array.Copy(array, 0, array2, 0, index);
            Array.Copy(array, index + 1, array2, index, array.Length - index - 1);

            lock (backing)
            {
                backing[k] = array2;
            }
        }

        internal TValue[][] GetMany(object[] Objkeys)
        {
            var output = new ConcurrentBag<TValue[]>();
            Objkeys
                .OfType<TKey>()
                .AsParallel()
                .ForAll(key =>
                {
                    TValue[] val;
                    if (backing.TryGetValue(key, out val))
                        output.Add(val);
                });

            return output.ToArray();
        }


        private Lazy<ObjectPoolSized<TValue[][]>> lTempPool =
            new Lazy<ObjectPoolSized<TValue[][]>>(() =>
            {
                var poolConfig = new ObjectPoolSizeConfiguration(
                        new Dictionary<int, int> {
                        { 128, 100},
                        { 1024, 100},
                        { 16384, 100},
                        { 131072, 10},
                        { 1048576, 1},
                        });
                return ObjectPoolSized<TValue[][]>.CreatePool((size => new TValue[size][]), "SwappableCache", poolConfig, true);
            });


        private ObjectPoolSized<TValue[][]> tempPool => lTempPool.Value;

        internal TValue[][] GetMany(int[] Objkeys)
        {
            var temp = tempPool.Get(Objkeys.Length);
            try
            {
                int Matches = 0;
                foreach (var key in Objkeys.OfType<TKey>())
                {
                    TValue[] val;
                    if (backing.TryGetValue(key, out val))
                        temp[Matches++] = val;
                }

                TValue[][] output = new TValue[Matches][];
                //temp.XCopyTo(output, 0, 0, Matches);
                Array.Copy(temp, output, Matches);
                return output;
            }
            finally
            {
                tempPool.Put(temp);
            }
        }

        internal IEnumerable<TValue[]> YieldMany(IEnumerable<int> Objkeys)
        {
            foreach (var key in Objkeys.OfType<TKey>())
            {
                TValue[] val;
                if (backing.TryGetValue(key, out val))
                    yield return val;
            }
        }

        internal IEnumerable<T> YieldMany<T>(IEnumerable<int> Objkeys)
        {
            foreach (var key in Objkeys.OfType<TKey>())
            {
                TValue[] val;
                if (backing.TryGetValue(key, out val))
                    yield return (T)Convert.ChangeType(val[0], typeof(T));
            }
        }

        #endregion
    }
}
