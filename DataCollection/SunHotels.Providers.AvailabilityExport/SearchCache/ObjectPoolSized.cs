﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SunHotels.Providers.Repositories
{
    public class ObjectPoolSizeConfiguration
    {
        public IDictionary<int, int> Size_Count;
        public ObjectPoolSizeConfiguration(IDictionary<int, int> values)
        {
            Size_Count = values;
        }
    }
    public class ObjectPoolSized<T>
    {
        private List<Tuple<int, ObjectPool<T>>> _pools;
        private bool dynamicPools;
        //private Func<int, T> sizeFactory;

        private ObjectPoolSized()
        {

        }

        public static ObjectPoolSized<T> CreatePool(Func<int, T> sizeFactory, string CategoryName,
             ObjectPoolSizeConfiguration Config = null, bool PreAllocate = false)
        {
            if (Config == null)
            {
                Config =
                    new ObjectPoolSizeConfiguration(new Dictionary<int, int>
                    {
                        {128, 100},
                        {1024, 100},
                        {16384, 100},
                        {131072, 10},
                        {1572864, 1}
                    });
            }

            ObjectPoolSized<T> obj = new ObjectPoolSized<T>
            {
                dynamicPools = true,
                _pools = new List<Tuple<int, ObjectPool<T>>>(Config.Size_Count.Count)
            };

            foreach (var pair in Config.Size_Count)
            {
                string PoolName = string.Join(".", CategoryName, "Pool", $"{pair.Key} x ({pair.Value})");
                obj._pools.Add(
                    Tuple.Create(pair.Key,
                        new ObjectPool<T>(() => sizeFactory(pair.Key), PoolName, pair.Value, PreAllocate)));
            }

            if (PreAllocate)
            {
                System.Runtime.GCSettings.LargeObjectHeapCompactionMode = System.Runtime.GCLargeObjectHeapCompactionMode.CompactOnce;
                GC.Collect(2, GCCollectionMode.Forced, false, true);
            }

            return obj;
        }

        public T Get(int size)
        {
            var poolIndex = FindDynamicPool(size);
            if (poolIndex == -1)
                throw new ArgumentOutOfRangeException(nameof(size));

            return _pools[poolIndex].Item2.GetObject();
        }

        public void Put(T obj, int size = 0)
        {
            size = size > 0 ? size : (obj as IList)?.Count ?? size;

            var poolIndex = FindDynamicPool(size);
            if (poolIndex == -1)
                throw new ArgumentOutOfRangeException(nameof(size));

            _pools[poolIndex].Item2.PutObject(obj);
        }

        private int FindDynamicPool(int size)
        {
            if (!dynamicPools)
                return -1;

            for (int i = 0; i < _pools.Count; i++)
            {
                if (_pools[i].Item1 < size)
                    continue;

                return i;
            }
            return -1;
        }
    }
}
