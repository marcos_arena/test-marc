﻿using Sunhotels.Export;
using SunHotels.SearchCache.Search.InvertedIndex;
using SunHotels.SearchCache.Search.InvertedIndex.BinaryDatabaseSerialization;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SunHotels.Providers.Repositories
{
    public class FasstIndexes
    {
        public static FasstIndexes LoadUnicornCache(AvailabilityExportSettings config)
        {
            try
            {
                Console.WriteLine("Reading Unicorn Cache");

                var copySearchCachePath = config.OriginSearchCacheFilesPath;
                var searchCachePath = config.SearchCacheFilesPath;

                if (!string.IsNullOrEmpty(copySearchCachePath))
                {
                    Console.WriteLine("   Start copy files");
                    DirectoryCopy(copySearchCachePath, searchCachePath);
                    Console.WriteLine("   End copy files");
                }
                HotLoadAllExport(searchCachePath);
                return Instance;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error loading Unicorn Cache", ex);
                return null;
            }
        }

        private static void DirectoryCopy(string sourceDirName, string destDirName)
        {
            DirectoryInfo dir = new DirectoryInfo(sourceDirName);
            int searchCacheRetryCount = 3;
            int searchCacheSecondsBetweenRetries = 5;

            if (!dir.Exists)
            {
                throw new DirectoryNotFoundException(
                    "Source directory does not exist or could not be found: "
                    + sourceDirName);
            }

            foreach (FileInfo file in dir.GetFiles())
            {
                if (file.Extension == ".bin")
                {
                    CopyIfNewerThan(file, destDirName, 5 * 60, searchCacheRetryCount, searchCacheSecondsBetweenRetries);
                }
            }
        }

        private static void CopyIfNewerThan(FileInfo sourceFile, string destinyPath, double copySecondsOlder, int numRetries, int secondsBetweenRetries)
        {
            if (!sourceFile.Exists) return;
            if (!Directory.Exists(destinyPath)) Directory.CreateDirectory(destinyPath);

            var destinyFile = System.IO.Path.Combine(destinyPath, sourceFile.Name);
            var destinyFileInfo = new FileInfo(destinyFile);
            if (destinyFileInfo.Exists && destinyFileInfo.CreationTime > sourceFile.CreationTime)
            {
                Console.WriteLine($"{destinyFile} newer, not copied");
                return;
            }
            Exception exeption = null;
            numRetries = (numRetries < 1) ? 1 : numRetries;
            for (int i = 0; i < numRetries; i++)
            {
                try
                {

                    sourceFile.CopyTo(destinyFile, true);
                    Console.WriteLine($"{sourceFile.Name} copied, numretries:{i}");
                    return;
                }
                catch (Exception ex)
                {
                    exeption = ex;
                    System.Threading.Thread.Sleep(secondsBetweenRetries * 1000);
                }
            }
            throw exeption;
        }

        #region Singleton
        static FasstIndexes()
        {

        }
        //        private static Lazy<FasstIndexes> instance = new Lazy<FasstIndexes>(() => new CacheLoader().LoadAllDataFiles(), System.Threading.LazyThreadSafetyMode.ExecutionAndPublication);
        private static Lazy<FasstIndexes> instance = new Lazy<FasstIndexes>(() => new FasstIndexes(false), LazyThreadSafetyMode.ExecutionAndPublication);

        public static FasstIndexes Instance => instance.Value;

        public static void SetInstance(FasstIndexes index)
        {
            instance = new Lazy<FasstIndexes>(() => index);
        }

        #endregion

        public SwappableCache<DateTime, int> dateRoomIds;
        public SwappableCache<int, int> xmlProviderRoomIds;
        public SwappableCache<int, int> hotelIdRoomIds;
        public SwappableCache<int, RoomWeeklyStay> roomWeeklyStay;
        public SwappableCache<int, CacheHotelObject> hotelInfo;
        public SwappableCache<int, CacheRoomObject> roomStaticInfo;
        public SwappableCache<int, RoomDiscountsCacheObject> roomDiscount;
        public CacheCalendarPriceObject[] priceLookup;
        public SwappableCache<int, IdRange<int>> roomPrices;
        public SwappableCache<int, RoomCalendarAvailabilityInfo> roomCalendarAvailabilityInfo;
        public CacheCancellationPolicyData[] cancellationPolicyLookup;
        public SwappableCache<int, IdRange<int>> roomCancellationPolicies;

        public SwappableCache<int, int> maxOccupancies;
        public SwappableCache<int, int> maxAdults;
        public SwappableCache<int, int> maxChildren;
        public SwappableCache<int, int> maxChildrenAges;
        public RoomConfigurationExtrabedFactor[] extrabedFactorsLookup;
        public SwappableCache<int, int> roomIdExtrabedFactor;
        public SwappableCache<int, RoomConfigurationOccupancyBlock> roomIdOccupancyBlocks;
        /// <summary>
        /// This field is calculated based on roomPrices and calendar, it is not serialized from files.
        /// </summary>
        public SwappableCache<int, DateRangeCollection> roomHasExtrabedPriceAdult;

        public Dictionary<RoomDistribution, int[]> RoomDistributionCache;

        public static object SyncronizationRoot = new object();

        //Export
        public Dictionary<int, RoomConfigurationOccupancy> roomConfiguration;

        public FasstIndexes(bool Load)
        {

            dateRoomIds = new SwappableCache<DateTime, int>();
            xmlProviderRoomIds = new SwappableCache<int, int>();
            hotelIdRoomIds = new SwappableCache<int, int>();
            roomWeeklyStay = new SwappableCache<int, RoomWeeklyStay>();
            hotelInfo = new SwappableCache<int, CacheHotelObject>();
            roomStaticInfo = new SwappableCache<int, CacheRoomObject>();
            roomDiscount = new SwappableCache<int, RoomDiscountsCacheObject>();
            priceLookup = new CacheCalendarPriceObject[0];
            roomPrices = new SwappableCache<int, IdRange<int>>();
            roomCalendarAvailabilityInfo = new SwappableCache<int, RoomCalendarAvailabilityInfo>();
            cancellationPolicyLookup = new CacheCancellationPolicyData[0];
            roomCancellationPolicies = new SwappableCache<int, IdRange<int>>();

            maxOccupancies = new SwappableCache<int, int>();
            maxAdults = new SwappableCache<int, int>();
            maxChildren = new SwappableCache<int, int>();
            maxChildrenAges = new SwappableCache<int, int>();
            extrabedFactorsLookup = new RoomConfigurationExtrabedFactor[0];
            roomIdExtrabedFactor = new SwappableCache<int, int>();
            roomIdOccupancyBlocks = new SwappableCache<int, RoomConfigurationOccupancyBlock>();
            roomHasExtrabedPriceAdult = new SwappableCache<int, DateRangeCollection>();
            RoomDistributionCache = new Dictionary<RoomDistribution, int[]>();
        }

        public static void HotLoadAllExport(string filesPath)
        {

            var loader = new CacheLoader();
            var IX = loader.LoadAllDataFilesExport(filesPath);
            lock (SyncronizationRoot)
                instance = new Lazy<FasstIndexes>(() => IX);

        }

        public void RecalculateRoomHasExtrabedPriceAdult()
        {
            lock (SyncronizationRoot)
            {
                var tmp = new Dictionary<int, DateRangeCollection>();
                foreach (var room in roomStaticInfo.GetAll())
                {
                    var dates = new DateRangeCollection();
                    var p = roomPrices.Get(room.Key);
                    // for some strange reason while doing a hotload, the price may disappear.... awesome.

                    // race condition updating the price, if this breaks we have a big problem.
                    foreach (var range in p.Where(price => priceLookup[price.Id].extrabedpriceadult > 0).Select(f => f.dates))
                    {
                        dates.AddRange(range.BaseDate, range.Last(), true, true);
                    }
                    if (dates.RangeCount > 0)
                        tmp[room.Key] = dates;
                }
                roomHasExtrabedPriceAdult.ReplaceAll(tmp);
            }
        }

        public void Load(IndexType x, IDictionary<int, int[]> source)
        {
            ////tracer.Write($"Loading full source of type {x}");
            // ReSharper disable once SwitchStatementMissingSomeCases
            switch (x)
            {
                case IndexType.XmlProviderRoomId: xmlProviderRoomIds.ReplaceAll(source); break;
                case IndexType.HotelIdRoomId: hotelIdRoomIds.ReplaceAll(source); break;
                case IndexType.RoomExtrabedFactors: roomIdExtrabedFactor.ReplaceAll(source); break;
            }
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct BedConfiguration
        {
            public byte beds;
            public byte extraBeds;

            public BedConfiguration(int beds, int extraBeds)
            {
                this.beds = (byte)beds;
                this.extraBeds = (byte)extraBeds;
            }
            public override int GetHashCode()
            {
                return beds + (extraBeds << 8);
            }
            public override bool Equals(object obj)
            {
                if (obj is BedConfiguration)
                {
                    var cfg = (BedConfiguration)obj;
                    return beds == cfg.beds && extraBeds == cfg.extraBeds;
                }
                return base.Equals(obj);
            }
        }


        private Lazy<Dictionary<BedConfiguration, Tuple<int, int, int>>> occupancyCache =
            new Lazy<Dictionary<BedConfiguration, Tuple<int, int, int>>>(() => GenerateBedExtraBedMap());

        static Dictionary<BedConfiguration, Tuple<int, int, int>> GenerateBedExtraBedMap()
        {
            var retval = new Dictionary<BedConfiguration, Tuple<int, int, int>>();

            const int minAdults = 1;
            const int minChildren = 0;

            for (int beds = 1; beds < 19; beds++)
                for (int extraBeds = 0; extraBeds < (19 - beds); extraBeds++)
                {
                    var bb = new BedConfiguration(beds, extraBeds);

                    var maxOcc = beds + extraBeds;
                    var maxAdults = maxOcc - minChildren;
                    var maxChildren = maxOcc - minAdults;
                    retval[bb] = Tuple.Create(maxOcc, maxAdults, maxChildren);
                }

            return retval;
        }

        private Lazy<Dictionary<BedConfiguration, RoomConfigurationOccupancy>> roomConfigOccupancyCache =

            new Lazy<Dictionary<BedConfiguration, RoomConfigurationOccupancy>>(() => GenerateRoomConfigBedExtraBedMap());

        static Dictionary<BedConfiguration, RoomConfigurationOccupancy> GenerateRoomConfigBedExtraBedMap()
        {
            var retval = new Dictionary<BedConfiguration, RoomConfigurationOccupancy>();

            const int minAdults = 1;
            const int minChildren = 0;

            for (int beds = 1; beds < 19; beds++)
                for (int extraBeds = 0; extraBeds < (19 - beds); extraBeds++)
                {
                    var bb = new BedConfiguration(beds, extraBeds);

                    var maxOcc = beds + extraBeds;
                    var maxAdults = maxOcc - minChildren;
                    var maxChildren = maxOcc - minAdults;
                    retval[bb] = new RoomConfigurationOccupancy() { maxAdults = maxAdults, minAdults = 1, maxChildren = maxChildren, minChildren = 0, maxOccupancy = maxOcc, minOccupancy = 1 };
                }

            return retval;

        }
        public void Load(IndexType x, IDictionary<RoomConfigurationInfantsAndChildren, int[]> source)
        {
            //tracer.Write($"Loading full source of type {x}");

            List<int> alist;

            // ReSharper disable once SwitchStatementMissingSomeCases
            switch (x)
            {
                case IndexType.RoomInfantsAndChildren:
                    var infAndCh = new Dictionary<int, List<int>>();

                    foreach (KeyValuePair<RoomConfigurationInfantsAndChildren, int[]> entry in source)
                    {
                        var infAndChild = entry.Key;
                        for (int i = 0; i <= infAndChild.maxChildAge; i++)
                        {
                            if (!infAndCh.TryGetValue(i, out alist))
                                infAndCh[i] = alist = new List<int>();

                            alist.AddRange(entry.Value);
                        }
                        if (roomStaticInfo == null)
                            continue;

                        foreach (int roomId in entry.Value)
                        {
                            var room = roomStaticInfo.GetFirst(roomId);
                            //if (room.maxChildAge < infAndChild.maxChildAge)
                            //{

                            roomStaticInfo.ReplaceAllSingleKey(roomId, (() =>
                            {
                                room.maxChildAge = (byte)infAndChild.maxChildAge;
                                return new CacheRoomObject[] { room };
                            }));
                            //}
                        }
                    }

                    foreach (KeyValuePair<int, List<int>> entry in infAndCh)
                        maxChildrenAges.ReplaceAll(entry.Key, () => entry.Value.ToArray());

                    break;
            }
        }

        public void Load(IndexType x, Tuple<RoomConfigurationExtrabedFactor[], Dictionary<int, int[]>> tuple)
        {
            if (x != IndexType.RoomExtrabedFactors || tuple?.Item1.Length < 1)
                return;

            extrabedFactorsLookup = tuple.Item1;
            roomIdExtrabedFactor.ReplaceAll(tuple.Item2);
        }

        public void Load(IndexType x, Tuple<RoomConfigurationOccupancyBlock[], Dictionary<int, int[]>> tuple)
        {
            if (x != IndexType.RoomOccupancyBlocks || tuple?.Item1.Length < 1)
                return;

            var occupancyBlocks = new Dictionary<int, RoomConfigurationOccupancyBlock[]>();
            var tmp = new List<RoomConfigurationOccupancyBlock>();
            foreach (var room in tuple.Item2)
            {
                tmp.Clear();
                foreach (var occBlockIdx in room.Value)
                {
                    tmp.Add(tuple.Item1[occBlockIdx]);
                }
                occupancyBlocks[room.Key] = tmp.ToArray();
            }

            roomIdOccupancyBlocks.ReplaceAll(occupancyBlocks);
        }


        public void Load(IndexType x, IDictionary<DateTime, int[]> source)
        {
            if (x != IndexType.CalendarDateRoom || source?.Count < 1)
                return;

            //tracer.Write($"Loading full source of type {x}");
            dateRoomIds.ReplaceAll(source);
        }

        public void Load(IndexType x, Dictionary<int, IdRange<int>[]> item2)
        {
            if (item2?.Count == 0)
                return;

            switch (x)
            {
                case IndexType.RoomPrices:
                    roomPrices.ReplaceAll(item2);
                    break;

                case IndexType.RoomCancellationPolicy:
                    roomCancellationPolicies.ReplaceAll(item2);
                    break;
            }
        }

        public void Load(IndexType x, CacheCalendarPriceObject[] item1)
        {
            if (x != IndexType.PriceLookup || item1?.Length < 1)
                return;

            priceLookup = item1;
        }

        public void Load(IndexType x, IDictionary<int, RoomWeeklyStay> wk)
        {

            if (x != IndexType.WeeklyStay || wk == null)
                return;
            //tracer.Write($"Loading full source of type {x}");
            roomWeeklyStay.ReplaceAll(wk);
        }

        public void Load(IndexType x, IDictionary<int, CacheHotelObject> loadOne)
        {
            if (x != IndexType.HotelStaticInfo || loadOne == null)
                return;
            //tracer.Write($"Loading full source of type {x}");
            hotelInfo.ReplaceAll(loadOne);
        }

        public void Load(IndexType x, IDictionary<int, CacheRoomObject> loadOne)
        {
            if (x != IndexType.RoomStaticInfo || loadOne == null)
                return;
            //tracer.Write($"Loading full source of type {x}");
            roomStaticInfo.ReplaceAll(loadOne);
        }

        public void Load(IndexType x, IDictionary<int, RoomDiscountsCacheObject> loadOne)
        {
            if (x != IndexType.RoomDiscount || loadOne == null)
                return;
            //tracer.Write($"Loading full source of type {x}");
            roomDiscount.ReplaceAll(loadOne);
        }

        public void Load(IndexType x, Dictionary<int, RoomCalendarAvailabilityInfo> loadOne)
        {
            if (x != IndexType.RoomCalendarAvailabilityInfo || loadOne == null)
                return;
            //tracer.Write($"Loading full source of type {x}");
            roomCalendarAvailabilityInfo.ReplaceAll(loadOne);
        }

        public void Load(IndexType x, CacheCancellationPolicyData[] item1)
        {
            if (x != IndexType.RoomCancellationPolicyLookup || item1?.Length < 1)
                return;

            cancellationPolicyLookup = item1;
        }

        public static RoomDistribution getRoomDistributionCacheKey(int adults, int children, int childAge1, int childAge2, int childAge3)
        {
            return new RoomDistribution()
            {
                adults = adults,
                children = children,
                childAge1 = childAge1,
                childAge2 = childAge2,
                childAge3 = childAge3
            };
        }

        public Dictionary<CacheCalendarPriceObject, DateRangeCollection> getCalendarPrices(int roomId, DateTime From, DateTime To)
        {
            int matched = 0;
            DateTime[] matchedDates;
            dynamic retval = new ExpandoObject();
            var g = new Dictionary<CacheCalendarPriceObject, DateRangeCollection>();
            foreach (var p in roomPrices.Get(roomId))
            {
                var dr = new DateRangeCollection();
                matched += p.dates.IntersectedRangeCountAndOutput(From, To, out matchedDates);
                foreach (var d in matchedDates)
                    dr.Add(d);
                if (dr.RangeCount > 0)
                    g[priceLookup[p.Id]] = dr;
            }
            return g;
        }

        public void LoadExportRoomConfiguration(IndexType x, IDictionary<RoomConfigurationOccupancy, int[]> source)
        {
            var roomConfig = new Dictionary<int, RoomConfigurationOccupancy>();
            foreach (var kvp in source)
                foreach (var roomId in kvp.Value)
                    roomConfig.Add(roomId, kvp.Key);

            //AddDefaultRoomConfigurations(roomConfig);
            roomConfiguration = roomConfig;
        }
    }

    public struct RoomDistribution
    {
        internal int data;

        const int adultsMask = 0x000F;
        const int childrenMask = 0x000F;
        const int childAge1Mask = 0x001F;
        const int childAge2Mask = 0x001F;
        const int childAge3Mask = 0x001F;

        const int childrenShift = 4;
        const int childAge1Shift = childrenShift + 4;
        const int childAge2Shift = childAge1Shift + 5;
        const int childAge3Shift = childAge2Shift + 5;

        public int adults
        {
            get { return data & adultsMask; }
            set { data = (data & ~adultsMask) | (value & adultsMask); }
        }

        public int children
        {
            get { return (data >> childrenShift) & childrenMask; }
            set { data = (data & ~(childrenMask << childrenShift)) | (value & childrenMask) << childrenShift; }
        }

        public int childAge1
        {
            get { return (data >> childAge1Shift) & childAge1Mask; }
            set { data = (data & ~(childAge1Mask << childAge1Shift)) | (value & childAge1Mask) << childAge1Shift; }
        }

        public int childAge2
        {
            get { return (data >> childAge2Shift) & childAge2Mask; }
            set { data = (data & ~(childAge2Mask << childAge2Shift)) | (value & childAge2Mask) << childAge2Shift; }
        }

        public int childAge3
        {
            get { return (data >> childAge3Shift) & childAge3Mask; }
            set { data = (data & ~(childAge3Mask << childAge3Shift)) | (value & childAge3Mask) << childAge3Shift; }
        }

    }
}
