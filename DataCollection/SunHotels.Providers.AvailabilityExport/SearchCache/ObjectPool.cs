﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SunHotels.Providers.Repositories
{
    public class ObjectPool<T>
    {
        private ConcurrentQueue<T> _objects;
        private Func<T> _objectGenerator;
        readonly private int _maxPoolSize;
        readonly private string _poolName;


        //private int objectPoolCount;
        public ObjectPool(Func<T> objectGenerator, string PoolName, int maxPoolSize, bool PreAllocate = false)
        {
            if (objectGenerator == null) throw new ArgumentNullException(nameof(objectGenerator));
            _objects = new ConcurrentQueue<T>();
            _objectGenerator = objectGenerator;
            _maxPoolSize = maxPoolSize;
            _poolName = PoolName;



            if (PreAllocate)
                this.PreAllocate(maxPoolSize);
        }

        internal void PreAllocate(int elements)
        {
            for (int i = 0; i < elements; i++)
            {
                PutObject(_objectGenerator());
            }
        }

        public T GetObject()
        {
            T item;
            if (_objects.TryDequeue(out item))
            {

                return item;
            }



            return _objectGenerator();
        }

        public void PutObject(T item)
        {
            if (_objects.Count >= _maxPoolSize)
            {


                return;
            }
            _objects.Enqueue(item);


            item = default(T);
            return;
        }
    }
}
