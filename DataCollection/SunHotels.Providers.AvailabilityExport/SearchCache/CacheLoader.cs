﻿using SunHotels.SearchCache.Search.InvertedIndex;
using SunHotels.SearchCache.Search.InvertedIndex.BinaryDatabaseSerialization;
using System;
using System.Collections.Generic;
using System.Linq;
using Files = SunHotels.SearchCache.Search.InvertedIndex.CacheLoadConfiguration.FileNameCollection;
using System.Text;
using System.Threading.Tasks;
using SunHotels.SearchCache.Common;

namespace SunHotels.Providers.Repositories
{
    public class CacheLoader
    {
        public FasstIndexes LoadAllDataFilesExport(string filesPath)
        {
            CacheLoadConfiguration config = new CacheLoadConfiguration(true, string.Empty);
            return LoadGeneratedFilesExport(config, filesPath);
        }
        public Dictionary<int, T> LoadOne<T>(InvertedIndexBinaryStructureHelper file, bool ConfigEnabled, string DataName, string FileName) where T : IBinaryStructureWriteable<T>, new()
        {
            if (!ConfigEnabled)
                return new Dictionary<int, T>();

            var searchCacheRetryCount = 3;
            var searchCacheSecondsBetweenRetries = 5;

            var dic = CallRetrying(() => file.Get<T>(FileName), searchCacheRetryCount, searchCacheSecondsBetweenRetries);
            return dic;
        }
        public static T CallRetrying<T>(Func<T> method, int numRetries, int secondsBetweenFailingCall)
        {
            Exception exeption = null;
            numRetries = (numRetries < 1) ? 1 : numRetries;
            for (int i = 0; i < numRetries; i++)
            {
                try
                {
                    return method();
                }
                catch (Exception ex)
                {
                    exeption = ex;
                    System.Threading.Thread.Sleep(secondsBetweenFailingCall * 1000);
                }
            }
            throw exeption;
        }
        public FasstIndexes LoadGeneratedFilesExport(CacheLoadConfiguration config, string filesPath, SBLogger logger = null)
        {
            string uncompressedFilename = "temp.bin";
            var file = new InvertedIndexBinaryStructureHelper(uncompressedFilename, logger);

            var retval = new FasstIndexes(false);

            string dir = filesPath;

            retval = LoadGeneratedFiles(config, file, retval, dir, logger);
            LoadRoomConfigurationOccupancyFileExport(config, file, retval, dir);
            return retval;
        }
        public FasstIndexes LoadGeneratedFiles(CacheLoadConfiguration config, InvertedIndexBinaryStructureHelper file, FasstIndexes retval, string dir, SBLogger logger = null)
        {
            var searchCacheRetryCount = 3;
            var searchCacheSecondsBetweenRetries = 5;

            if (config.WeeklyStays)
            {
                var wk = CallRetrying(() => file.GetWeeklyStays(dir + Files.WeeklyStays).ToDictionary(g => g.Key, g => g.Value), searchCacheRetryCount, searchCacheSecondsBetweenRetries);
                retval.Load(IndexType.WeeklyStay, wk);
            }

            if (config.ProviderRooms)
            {
                var pidRooms = CallRetrying(() => file.GetData(dir + Files.ProviderRooms).ToDictionary(g => g.Key.KeyValue, g => g.Value), searchCacheRetryCount, searchCacheSecondsBetweenRetries);
                retval.Load(IndexType.XmlProviderRoomId, pidRooms);
            }

            if (config.HotelToRoomId)
            {
                var pidRooms = CallRetrying(() => file.GetData(dir + Files.HotelToRoomId).ToDictionary(g => g.Key.KeyValue, g => g.Value), searchCacheRetryCount, searchCacheSecondsBetweenRetries);
                retval.Load(IndexType.HotelIdRoomId, pidRooms);
            }

            if (config.CalendarDateRoom)
            {
                var pidRooms = CallRetrying(() => file.GetData(dir + Files.CalendarDateRoom).ToDictionary(g => g.Key.KeyValueAsDate, g => g.Value), searchCacheRetryCount, searchCacheSecondsBetweenRetries);
                retval.Load(IndexType.CalendarDateRoom, pidRooms);
            }

            retval.Load(IndexType.HotelStaticInfo,
                LoadOne<CacheHotelObject>(file, config.Hotels,
                nameof(Files.Hotels),
                dir + Files.Hotels));

            retval.Load(IndexType.RoomStaticInfo,
                LoadOne<CacheRoomObject>(file, config.Rooms,
                nameof(Files.Rooms),
                dir + Files.Rooms));

            retval.Load(IndexType.RoomDiscount,
                LoadOne<RoomDiscountsCacheObject>(file, config.Discounts,
                nameof(Files.Discounts),
                dir + Files.Discounts));

            retval.Load(IndexType.RoomCalendarAvailabilityInfo,
                LoadOne<RoomCalendarAvailabilityInfo>(file, config.RoomCalendarMinDays,
                nameof(Files.RoomCalendarMinDays),
                dir + Files.RoomCalendarMinDays));

            if (config.CalendarPrices)
            {
                var output = CallRetrying(() => file.GetTree<CacheCalendarPriceObject>(dir + Files.CalendarPrices), searchCacheRetryCount, searchCacheSecondsBetweenRetries);
                retval.Load(IndexType.PriceLookup, output.Item1); // var priceLookup = output.Item1;
                retval.Load(IndexType.RoomPrices, output.Item2); //var roomPrices = output.Item2;
            }

            if (config.RoomConfigurationInfantsAndChildren)
            {
                var pidRooms = CallRetrying(() => file.GetDictionary<RoomConfigurationInfantsAndChildren>(dir + Files.RoomConfigurationInfantsAndChildren).ToDictionary(g => g.Key, g => g.Value), searchCacheRetryCount, searchCacheSecondsBetweenRetries);
                retval.Load(IndexType.RoomInfantsAndChildren, pidRooms);
            }

            if (config.RoomConfigurationExtrabedFactors)
            {
                var extrabedFactorsAndRooms = CallRetrying(() => file.GetTreeIntArray<RoomConfigurationExtrabedFactor>(dir + Files.RoomConfigurationExtrabedFactors), searchCacheRetryCount, searchCacheSecondsBetweenRetries);
                retval.Load(IndexType.RoomExtrabedFactors, extrabedFactorsAndRooms);
            }

            if (config.RoomConfigurationOccupancyBlocks)
            {
                var occupancyBlocksAndRooms = CallRetrying(() => file.GetTreeIntArray<RoomConfigurationOccupancyBlock>(dir + Files.RoomConfigurationOccupancyBlocks), searchCacheRetryCount, searchCacheSecondsBetweenRetries);
                retval.Load(IndexType.RoomOccupancyBlocks, occupancyBlocksAndRooms);
            }

            if (config.RoomCancellationPolicies)
            {
                var output = CallRetrying(() => file.GetTree<CacheCancellationPolicyData>(dir + Files.RoomCancellationPolicies), searchCacheRetryCount, searchCacheSecondsBetweenRetries);
                retval.Load(IndexType.RoomCancellationPolicyLookup, output.Item1);
                retval.Load(IndexType.RoomCancellationPolicy, output.Item2);
            }

            retval.RecalculateRoomHasExtrabedPriceAdult();

            return retval;
        }

        private void LoadRoomConfigurationOccupancyFileExport(CacheLoadConfiguration config, InvertedIndexBinaryStructureHelper file, FasstIndexes retval, string dir)
        {
            if (config.RoomConfigurationOccupancies)
            {
                var searchCacheRetryCount = 3;
                var searchCacheSecondsBetweenRetries = 5;

                var pidRooms = CallRetrying(() => file.GetDictionary<RoomConfigurationOccupancy>(dir + Files.RoomConfigurationOccupancies).ToDictionary(g => g.Key, g => g.Value), searchCacheRetryCount, searchCacheSecondsBetweenRetries);
                retval.LoadExportRoomConfiguration(IndexType.RoomOccupancy, pidRooms);
            }
        }
    }
}
