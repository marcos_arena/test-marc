﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SunHotels.Providers.Repositories
{
    public enum IndexType
    {
        /// <summary>
        /// RoomIds avail per Date
        /// </summary>
        CalendarDateRoom = 1,
        /// <summary>
        /// Room Ids available per XmlProvider
        /// </summary>
        XmlProviderRoomId = 2,
        /// <summary>
        /// Room Ids available per Hotel
        /// </summary>
        HotelIdRoomId = 3,

        /// <summary>
        /// Static cache of Weeklystay information
        /// </summary>
        WeeklyStay = 4,

        /// <summary>
        /// Information like resort/dstination at the hotel level
        /// </summary>
        HotelStaticInfo = 5,
        RoomStaticInfo = 6,
        RoomDiscount = 7,
        PriceLookup = 8,
        RoomPrices = 9,
        RoomCalendarAvailabilityInfo = 10,
        RoomCancellationPolicyLookup = 11,
        RoomCancellationPolicy = 12,

        RoomOccupancy = 13,
        RoomInfantsAndChildren = 14,
        RoomExtrabedFactors = 15,
        RoomOccupancyBlocks = 16,

        //MaxOccupancy,
        //MaxAdults,
        //MaxChildren,
        //MaxChildrenAge
    }
}
