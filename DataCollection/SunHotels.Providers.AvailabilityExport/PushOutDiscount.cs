﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SunHotels.Providers
{
    public class PushOutDiscount
    {
        public decimal DiscountPercentage { get; set; }
        public bool IncludeMeals { get; set; }
        public bool IncludeExtraBed { get; set; }
        public decimal DiscountMultiplier 
        { 
            get { return (100 - DiscountPercentage) / 100; }
        }

        public bool IsBetter(PushOutDiscount discountToCompare)
        {
            if (this.DiscountPercentage > discountToCompare.DiscountPercentage)
            {
                return true;
            }

            if (this.DiscountPercentage == discountToCompare.DiscountPercentage)
            {
                if (this.IncludeMeals && !discountToCompare.IncludeMeals)
                {
                    return true;
                }

                return this.IncludeMeals == discountToCompare.IncludeMeals && this.IncludeExtraBed && !discountToCompare.IncludeExtraBed;
            }

            return false;
        }
    }
}
