﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SunHotels.Providers
{
    public class AvailabilityExportSettings
    {
        public bool HideManualRooms { get; set; }
        public bool HideXMLRooms { get; set; }
        public bool LogToFile { get; set; }
        public List<int> BlockedHotels { get; set; }
        public bool ShowXmlOnlyIfMissingManual { get; set; }
        public bool RemoveNRF { get; set; }
        public bool UseSearchCacheFiles { get; set; }
        public string OriginSearchCacheFilesPath { get; set; }
        public string SearchCacheFilesPath { get; set; }
    }
}
