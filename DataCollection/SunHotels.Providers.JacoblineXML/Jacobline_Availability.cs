﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using SunHotels.XML;
using SunHotels.XML.HelpClasses;
using System.Xml;
using SunHotels.XML.Data;
using SunHotels.Providers.ProviderCommon.Mapping;


namespace SunHotels.Providers
{
	partial class Jacobline
	{
		#region inner class AvailabilityHotel & AvailabilityRoom & Availability

		public class AvailabilityHotel
		{
			public int service_id { get; set; }
			public string currencyCode { get; set; }
			public Dictionary<int, Jacobline.AvailabilityRoom> Rooms { get; set; }

			public Dictionary<DateTime, Jacobline.Availability> Availabilities(int roomId)
			{
				Dictionary<DateTime, Jacobline.Availability> availabilities = new Dictionary<DateTime, Jacobline.Availability>();
				Jacobline.AvailabilityRoom tmpRoom = null;

				if (Rooms.TryGetValue(roomId, out tmpRoom))
				{
					availabilities = tmpRoom.Availability;
				}
				return availabilities;
			}
		}

		public class AvailabilityRoom
		{
			public int option_id { get; set; }
			public Dictionary<DateTime, Jacobline.Availability> Availability { get; set; }
		}

		public class Availability
		{
			// hotel level
			public int service_id { get; set; }
			public string currency { get; set; }
			public int totalmaxroom { get; set; }
			public int availableUnits { get; set; }
			public int cancellationpolicy_id { get; set; }

			// room level
			public int option_id { get; set; }
			public string roomname { get; set; }
			public int occupancy_id { get; set; }
			public int beds { get; set; }
			public int extrabeds { get; set; }
			

			// date level	
			public DateTime date { get; set; }
			public decimal price { get; set; }
			public string mealplan { get; set; }
			public int mealplan_id { get; set; }
			public int childpolicy_id { get; set; }
			public int minimumstayrule_id { get; set; }
			public bool norefund { get; set; }
			
		}


		#endregion

		public Dictionary<int, Jacobline.AvailabilityHotel> GetAvailability(Logger log, string path, string fileName, Dictionary<int, string> hotelCurrencies, IProviderDataMapping mapper)
		{

			ParseStatus status = new ParseStatus();
			ParseStatus detailstatus = new ParseStatus();

			log.RecordNewLine(true);
			log.RecordMessage("Start Processing Availability Cache...", Log.MessageType.Informational);
			Console.WriteLine("Start Processing Availability Cache...");

			// Read board mapping.
			var boardTable = mapper.GetDynMappingFor("Board");
			var boards = boardTable.Keys.SelectMany(key => boardTable[key].Select(v => new { key, v = v }))
						.ToDictionary(v => v.v.Description, v => new { meal_name = v.key, meal_id = v.v.Id });

			// Read room type mapping.
			var roomMapping = mapper.GetDynSingleMappingFor("RoomType");

			// Read currency mapping.
			var currencies = mapper.GetSingleMappingFor("Currency");

			DirectoryInfo di = new DirectoryInfo(path);
			var fiList = di.GetFiles().Where(f => f.Name.IndexOf(fileName) != -1 && f.Extension == ".txt").FirstOrDefault();

			if (fiList == null)
			{
				log.RecordMessage("The file Availability was not found!", Log.MessageType.Error);
				throw new FileNotFoundException();
			}

			// 6212,2014-03-31 00:00:00.000,2,2,2,2,0,0,0,0,Sterling,2,|,759,#,Single Room,#,1,#,168.75,#,Bed & Full Scottish Breakfast,#,883,#,,;,#,760,#,Twin Room,#,2,#,87.01,#,Bed & Full Scottish Breakfast,#,883,#,,;,#,758,#,Double Room,#,3,#,87.01,#,Bed & Full Scottish Breakfast,#,883,#,,;,|
			var csvreader = new SunHotels.Framework.IO.CSV.CSVStream(fiList.FullName, new Framework.IO.CSV.CSVStream.CSVStreamSettings()
			{
				Separator = "|",
				QuoteString = null
			});

			Dictionary<int, AvailabilityHotel> availabilityHotels = new Dictionary<int, AvailabilityHotel>();
			AvailabilityHotel availabilityHotel = null;

			List<int> blNonStaticHotel = new List<int>();
			List<int> blDuplicatedRooms = new List<int>();
			List<string> blUnsuportedRoomType = new List<string>();
			List<string> blUnsuportedCurrencies = new List<string>();
			csvreader.ReadLines()
				.All(line =>
			{
				string[] row = line.First().Split(',');

				string[] detail = line.Skip(1).First().Split(new string[] { ",;,#," }, StringSplitOptions.None);

				int nServiceID = status.ParseInt(row[0]);

				// Dont add availibility for hotels that isn't derived from the static information
				if (hotelCurrencies.ContainsKey(nServiceID))
				{

					DateTime dtDate = status.ParseDateTime(row[1]);
					int nTotalMaxRoom = status.ParseInt(row[2]);
					int nTotalSingle = status.ParseInt(row[3]);
					int nTotalDouble = status.ParseInt(row[4]);
					int nTotalTwin = status.ParseInt(row[5]);
					int nTotalTriple = status.ParseInt(row[6]);
					int nTotalQuad = status.ParseInt(row[7]);
					int nTotalFam1 = status.ParseInt(row[8]);
					int nTotalFam2 = status.ParseInt(row[9]);
					string sCurrency = status.ParseString(row[10]).Replace(" ", "");
					int nCancellationPolicyID = status.ParseInt(row[11]);
					bool norefund = false;

					// Block availability if "non refoundable" cancellation policy found
					if (nCancellationPolicyID == 30)
					{
						norefund = true;
					}


					if (status.Current)
					{
						// If not already in hotel list, add it
						if (!availabilityHotels.ContainsKey(nServiceID))
						{
							string currency = String.Empty;
							hotelCurrencies.TryGetValue(nServiceID, out currency);

							availabilityHotels.Add(nServiceID, new AvailabilityHotel()
							{
								service_id = nServiceID,
								currencyCode = currency,
								Rooms = new Dictionary<int, AvailabilityRoom>()
							});
						}

						if (availabilityHotel == null || availabilityHotel.service_id != nServiceID)
							availabilityHotels.TryGetValue(nServiceID, out availabilityHotel);
						AvailabilityRoom availabilityRoom = null;


						for (int x = 0; x < detail.Length; x++)
						{
							detail[x] = detail[x].Replace(",;,|", "");
							if (detail[x].StartsWith(","))
								detail[x] = detail[x].Substring(1);
							string[] detailitem = detail[x].Split(new string[] { ",#," }, StringSplitOptions.None);
							
							#region  WORKAROUND: CC-1034 bad split
							var lastValue = detailitem[detailitem.Length - 1];
							if (lastValue.EndsWith(",;,"))
								detailitem[detailitem.Length - 1] = detailitem[detailitem.Length - 1].Substring(0, lastValue.Length - 3);
							#endregion WORKAROUND: CC-1034 bad split

							if (detailitem.Length == 7)
							{
								int nOptionID = detailstatus.ParseInt(detailitem[0]);
								string sRoomName = detailitem[1];
								int nOccupancyID = detailstatus.ParseInt(detailitem[2]);
								decimal fPrice = decimal.Parse(detailitem[3], System.Globalization.CultureInfo.InvariantCulture);
								string sMealPlan = detailitem[4];
								int nChildPolicyID = detailstatus.ParseInt(detailitem[5]);
								int nMinimumStayRuleID; int.TryParse(detailitem[6], out nMinimumStayRuleID);

								if (detailstatus.Current)
								{
									var bKey = sMealPlan.Replace("&", "and");
									if (!boards.ContainsKey(bKey))
									{
										log.RecordMessage("PARSING AVAILABILITY DETAIL : Unknown board. " + detail[x], Log.MessageType.Warning);
										continue;
									}
									var boardNode = boards[bKey];
									var rKey = nOccupancyID.ToString();
									if (!roomMapping.ContainsKey(rKey))
									{
										log.RecordMessage("PARSING AVAILABILITY DETAIL: Unknown roomtype. " + detail[x], Log.MessageType.Warning);
										continue;
									}
									var roomTypeNode = roomMapping[rKey];

									if (!availabilityHotel.Rooms.ContainsKey(nOptionID))
									{
										availabilityHotel.Rooms.Add(nOptionID, new AvailabilityRoom()
											{
												option_id = nOptionID,
												Availability = new Dictionary<DateTime, Availability>()
											});
									}

									availabilityRoom = availabilityHotel.Rooms[nOptionID];


									if (availabilityRoom.Availability.ContainsKey(dtDate))
									{
										if (!blDuplicatedRooms.Contains(nServiceID))
										{
											blDuplicatedRooms.Add(nServiceID);
										}
										continue;
									}


									int availableUnits = 0;

									switch (nOccupancyID)
									{
										case 1:
											availableUnits = nTotalSingle;
											break;
										case 2:
											availableUnits = nTotalTwin;
											break;
										case 3:
											availableUnits = nTotalDouble;
											break;
										case 4:
											availableUnits = nTotalTriple;
											break;
										case 5:
											availableUnits = nTotalQuad;
											break;
										case 6:
											string unsuportedRoom = String.Format("{0}.{1}.{2}", nServiceID, nOptionID, sRoomName);
											if (!blUnsuportedRoomType.Contains(unsuportedRoom))
											{
												blUnsuportedRoomType.Add(unsuportedRoom);
											}
											break;
										case 7:
											availableUnits = nTotalFam1;
											break;
										case 8:
											availableUnits = nTotalFam2;
											break;
										default:
											log.RecordMessage(String.Format("DROPPING AVAILABILITY: Unknown roomtype found. ServiceID [{0}] OptionID [{1}] Date [{2}]", nServiceID, nOptionID, dtDate), Log.MessageType.Warning);
											break;
									}

									
									if (!currencies.Any(kv => string.Compare(kv.Value, sCurrency, true) == 0))
									{
										string unsuportedCurrency = String.Format("{0}.{1}", nServiceID, sCurrency);

										if (!blUnsuportedCurrencies.Contains(unsuportedCurrency))
										{
											blUnsuportedCurrencies.Add(unsuportedCurrency);
										}

										continue;
									}
									string currencyCode = currencies.First(kv => string.Compare(kv.Value, sCurrency, true) == 0).Key;

									if (availableUnits > 0)
									{
										availabilityRoom.Availability.Add(dtDate, new Availability()
										{
											service_id = nServiceID,
											currency = currencyCode, //ParseCurrency(log, xmlCurrencyMapping, sCurrency),
											totalmaxroom = nTotalMaxRoom,
											availableUnits = availableUnits,
											cancellationpolicy_id = nCancellationPolicyID,
											option_id = nOptionID,
											roomname = sRoomName,
											occupancy_id = nOccupancyID,
											date = dtDate,
											// OLDCODE: price = fPrice * int.Parse(roomTypeNode.Attributes["beds"].Value),
											price = fPrice * roomTypeNode.Beds,
											beds = roomTypeNode.Beds,
											extrabeds = roomTypeNode.Extrabeds,
											mealplan = boardNode.meal_name,
											mealplan_id = boardNode.meal_id,
											childpolicy_id = nChildPolicyID,
											minimumstayrule_id = nMinimumStayRuleID,
											norefund = norefund
										});
									}
								}
								else
								{
									detailstatus.Clear();
									log.RecordMessage("PARSING AVAILABILITY DETAIL: " + detail[x], Log.MessageType.Error);
								}
							}
							else
							{
								log.RecordMessage(String.Format("PARSING AVAILABILITY DETAIL: Wrong number of detail items. ServiceID [{0}]", nServiceID), Log.MessageType.Error);
							}
						}
					}
					else
					{
						status.Clear();
						log.RecordMessage("PARSING AVAILABILITY: " + String.Join("|", line.ToArray()), Log.MessageType.Error);
					}
				}
				else
				{
					if (!blNonStaticHotel.Contains(nServiceID))
					{
						blNonStaticHotel.Add(nServiceID);
					}
				}
				return true;
			});


			// log duplicated rooms
			if (blDuplicatedRooms.Count > 0)
			{
				foreach (int blDuplicatedRoom in blDuplicatedRooms)
				{
					log.RecordMessage("PARSING AVAILABILITY DETAIL: Duplicate date. ServiceID [" + blDuplicatedRoom + "]", Log.MessageType.Error);
				}
			}


			// log blocked hotels
			if (blNonStaticHotel.Count > 0)
			{
				StringBuilder sbBlockedHotels = new StringBuilder();
				foreach (int blockedHotel in blNonStaticHotel)
				{
					sbBlockedHotels.AppendFormat("{0}{1}", sbBlockedHotels.Length > 0 ? "." : String.Empty, blockedHotel);
				}
				log.RecordMessage("DROPPING AVAILABILITY: Hotel not derived from the static information. ServiceID [" + sbBlockedHotels.ToString() + "]", Log.MessageType.Warning);
			}

			// log unsuported roomtypes
			if (blUnsuportedRoomType.Count > 0)
			{
				foreach (string unsuportedRoom in blUnsuportedRoomType)
				{
					string[] keySplit = unsuportedRoom.Split('.');
					log.RecordMessage(String.Format("DROPPING AVAILABILITY: Unsuported roomtype found. ServiceID [{0}] OptionID [{1}] Occupancy [{2}]", keySplit[0], keySplit[1], keySplit[2]), Log.MessageType.Warning);
				}
			}

			// log unsuported currencies
			if (blUnsuportedCurrencies.Count > 0)
			{
				foreach (string unsuportedCurency in blUnsuportedCurrencies)
				{
					string[] keySplit = unsuportedCurency.Split('.');
					log.RecordMessage(String.Format("DROPPING AVAILABILITY: Unsuported currency found. ServiceID [{0}] Currency [{1}]", keySplit[0], keySplit[1]), Log.MessageType.Error);
				}
			}

			log.RecordMessage("Done Processing Availability Cache...", Log.MessageType.Informational);

			return availabilityHotels;
		}
	}
}
