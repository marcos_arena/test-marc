﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using SunHotels.XML.HelpClasses;

namespace SunHotels.Providers
{
    partial class Jacobline
	{

		#region inner class MinimumStay

		public class MinimumStay
		{
			public int MinStayRuleID { get; set; }
			public string MinimumStayRuleName { get; set; }
			public string MinimumStayRuleDescription { get; set; }
			public int MinimumNights { get; set; }

		}  

		#endregion

        public List<Jacobline.MinimumStay> GetMinimumStay(Logger log, string path, string fileName)
        {
            ParseStatus status = new ParseStatus();
			log.RecordNewLine(true);
			log.RecordMessage("Start Processing Minimum Stay Cache.", Log.MessageType.Informational);
			Console.WriteLine("Start Processing Minimum Stay Cache.");
			

			DirectoryInfo di = new DirectoryInfo(path);
			var fiList = di.GetFiles().Where(f => f.Name.IndexOf(fileName) != -1 && f.Extension == ".txt").FirstOrDefault();

			if (fiList == null)
			{
				log.RecordMessage("The file MinimumStay was not found!", Log.MessageType.Error);
				throw new FileNotFoundException();
			}

			FileStream fs = new FileStream(fiList.FullName, FileMode.Open, FileAccess.Read);
            StreamReader sr = new StreamReader(fs);

            //TODO: add functionallity to get newest file from ftp;
            List<MinimumStay> minimumstay = new List<MinimumStay>();

            do
            {
                string s = sr.ReadLine();
                if (string.IsNullOrEmpty(s)) s = sr.ReadLine();

                string[] row = s.Split(',');

                int nMinStayRuleID = status.ParseInt(row[0]);
                string sMinimumStayRuleName = status.ParseString(row[1]);
                string sMinimumStayRuleDescription = status.ParseString(row[2]);
                int nMinimumNights = status.ParseInt(row[3]);

                if (status.Current)
                {
                    minimumstay.Add(new MinimumStay()
                    {
                        MinStayRuleID = nMinStayRuleID,
                        MinimumStayRuleName = sMinimumStayRuleName,
                        MinimumStayRuleDescription = sMinimumStayRuleDescription,
                        MinimumNights = nMinimumNights
                    });
                }
                else
                {
                    status.Clear();
                    log.RecordMessage(s, Log.MessageType.Warning);
                }

            } while (!sr.EndOfStream);



			
			log.RecordMessage("Done Processing Minimum Stay Cache.", Log.MessageType.Informational);

            return minimumstay;
        }

          
    }
}
