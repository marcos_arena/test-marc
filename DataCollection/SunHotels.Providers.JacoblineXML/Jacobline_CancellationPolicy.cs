﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using SunHotels.XML.HelpClasses;


namespace SunHotels.Providers
{
    partial class Jacobline
    {
        #region inner class CancellationPolicy & CancellationPolicyDetail

        public class CancellationPolicy
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public string Description { get; set; }
            public string ChargeType { get; set; }
            public List<CancellationPolicyDetail> CancellationPolicyDetails { get; set; }

        }

        public class CancellationPolicyDetail
        {
            public int NumberOfDays { get; set; }
            public decimal Amount { get; set; }
        }

        #endregion


        private string ParseInvalidCancellationLine(string line, StreamReader sr)
        {
            line = line.Replace(System.Environment.NewLine, " ");

            var index = line.IndexOf('|');
            if (index == -1)
            {
                var next = sr.ReadLine();
                return ParseInvalidCancellationLine($"{line}{next}", sr);
            }
            return line;
        }

        public List<CancellationPolicy> GetCancellationPolicy(Logger log, string path, string fileName)
        {
            ParseStatus status = new ParseStatus();
            ParseStatus detailstatus = new ParseStatus();

            log.RecordNewLine(true);
            log.RecordMessage("Start Processing Cancellation Policy Cache...", Log.MessageType.Informational);
            Console.WriteLine("Start Processing Cancellation Policy Cache...");


            DirectoryInfo di = new DirectoryInfo(path);
            var fiList = di.GetFiles().Where(f => f.Name.IndexOf(fileName) != -1 && f.Extension == ".txt").FirstOrDefault();

            List<CancellationPolicy> cancellationpolicies = new List<CancellationPolicy>();

            if (fiList != null)
            {
                if (File.Exists(fiList.FullName))
                {
                    using (FileStream fs = new FileStream(fiList.FullName, FileMode.Open, FileAccess.Read))
                    {
                        using (StreamReader sr = new StreamReader(fs))
                        {
                            do
                            {
                                string s = sr.ReadLine();
                                if (s.Length == 0) s = sr.ReadLine();

                                int firstPipe = s.IndexOf('|');
                                int lastPipe = s.LastIndexOf('|');

                                if (firstPipe == -1)
                                {
                                    s = ParseInvalidCancellationLine(s, sr);
                                    firstPipe = s.IndexOf('|');
                                    lastPipe = s.LastIndexOf('|');
                                }

                                string[] header = s.Substring(0, firstPipe).Trim(',').Split(',');
                                string[] detail = s.Substring(firstPipe, lastPipe - firstPipe).Trim('|').Split('|');
                                string chargeType = s.Substring(lastPipe + 1).Trim(',');

                                int id = status.ParseInt(header[0]);
                                string name = status.ParseString(header[1]);
                                string description = status.ParseString(header[2]);

                                List<CancellationPolicyDetail> cpd = new List<CancellationPolicyDetail>();

                                int x = 0;
                                while (x < detail.Length)
                                {
                                    detail[x] = detail[x].TrimStart(',').Replace(",;,", "");
                                    string[] detailitem = detail[x].Split(new string[] { ",#," }, StringSplitOptions.None);
                                    int days = 0;
                                    decimal amount = 0;

                                    try
                                    {
                                        days = detailstatus.ParseInt(detailitem[0]);
                                        amount = detailstatus.ParseDecimal(detailitem[1]);
                                    }
                                    catch
                                    {
                                        int sts = detailstatus.ParseInt(null);
                                    }

                                    if (detailstatus.Current)
                                    {
                                        cpd.Add(new CancellationPolicyDetail()
                                        {
                                            NumberOfDays = days,
                                            Amount = amount
                                        });
                                    }
                                    else
                                    {
                                        detailstatus.Clear();
                                        status.Current = false;
                                        log.RecordMessage("ERROR PARSING CANCELLATIONPOLICY DETAIL: " + detail[x], Log.MessageType.Warning);
                                    }
                                    x++;
                                }

                                // Check parse status
                                if (status.Current && cpd.Count > 0)
                                {
                                    cancellationpolicies.Add(new CancellationPolicy()
                                    {
                                        Id = id,
                                        Name = name,
                                        Description = description,
                                        CancellationPolicyDetails = cpd,
                                        ChargeType = chargeType
                                    });
                                }
                                else
                                {
                                    status.Clear();
                                    log.RecordMessage("ERROR PARSING CANCELLATIONPOLICY: " + s, Log.MessageType.Warning);
                                }

                            } while (!sr.EndOfStream);

                        }
                    }

                }
                else
                {
                    log.RecordMessage("The file CancellationPolicies was not found!", Log.MessageType.Error);
                    throw new FileNotFoundException();
                }
            }
            else
            {
                log.RecordMessage("Wrong number of files found", Log.MessageType.Error);
                throw new FileNotFoundException();
            }

            log.RecordMessage("Done Processing Cancellation Policy Cache...", Log.MessageType.Informational);
            return cancellationpolicies;
        }
    }
}
