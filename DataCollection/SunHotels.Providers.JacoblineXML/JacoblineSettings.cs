using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace SunHotels.Providers
{
    [XmlRoot("JacoblineSettings")]
    public class JacoblineSettings
    {
        public StaticData StaticData;
        public CachFiles CachFiles;
        public DebugData DebugData;
    }

    public class StaticData
    {
        public string CacheDirectory;
        public string HotelDirectory;
    }

    public class CachFiles
    {
        public string AvailabilityFile;
        public string CancellationPolicyFile;
        public string ChildPolicyFile;
        public string MealPlanFile;
        public string MinStayFile;
        public string SpecialOffersFile;
    }

    public class DebugData
    {
        //[XmlArray("TeamMembers")]
        public HotelItem[] HotelItems;       
    }

    public class HotelItem
    {
        public string HotelId;
    }
}
