﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SunHotels.XML;
using SunHotels.XML.HelpClasses;
using SunHotels.XML.Data;
using System.IO;
using System.Xml;
using System.Xml.Linq;

using ICSharpCode.SharpZipLib.Zip;
using System.Text.RegularExpressions;
using System.Xml.Serialization;
using SunHotels.Providers.ProviderCommon.Mapping;
using System.Dynamic;
using SunHotels.Providers.ProviderCommon.DataLoader;
using Sunhotels.Export;


namespace SunHotels.Providers
{
	public class JacoblineXML : XMLProvider, SunHotels.Providers.ProviderCommon.DataLoader.IStaticDataLogger
	{
		private Logger log = null;
		private JacoblineSettings settings = null;

		Dictionary<string, Hotel> Hotels = new Dictionary<string, Hotel>(3000);
		Dictionary<string, Place> Places = new Dictionary<string, Place>(1000);

		List<string> lowBasePriceBlockList = new List<string>();
		List<string> currencyMissMatchBlockList = new List<string>();


		public override void InitializeProvider(Configuration config)
		{
			base.InitializeProvider(config);
			DataMapper.RegisterDynMappingHandlerFor("RoomType", (value, type) =>
			{
				var values = value.Split(';');
				switch (type)
				{
					case "Description":
						return values[0];
					case "Price":
						return decimal.Parse(values[1]);
					case "Beds":
						return int.Parse(values[1]);
					case "Extrabeds":
						return int.Parse(values[2]);
				}
				throw new Exception(string.Format("Property not valid {0}", type));
			});
			DataMapper.RegisterDynMappingHandlerFor("Board", (value, type) =>
			{
				var values = value.Split(';');
				switch (type)
				{
					case "Id":
						return int.Parse(values[0]);
					case "Description":
						return values[1];
				}
				throw new Exception(string.Format("Property not valid {0}", type));
			});
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="root"></param>
		protected override void getProviderDefinition(Root root)
		{
			log = base.Logger;

			root.ProviderDefinition.Name = "JacoblineXML";
			root.ProviderDefinition.Currency = "EUR";

		}

		protected override void getFeatureTypes(Root root)
		{
			// Get SH features mapped with the provider
			DataMapper.SunHotelsFeatures.Join(DataMapper["Feature"], o => o.Key, i => i.Key, (o, i) => o)
			.All(f =>
			{
				root.Features.Add(new Feature()
				{
					Id = f.Key,
					Value = f.Value,
					Name = f.Value
				});
				return true;
			});
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="root"></param>
		protected override void getData(Root root)
		{
			LogEntry(XML.HelpClasses.Log.MessageType.Informational, "START getData()");

			LogEntry(XML.HelpClasses.Log.MessageType.Informational, "----------------------------------------");
			LogEntry(XML.HelpClasses.Log.MessageType.Informational, "       Jacobline File Import.");
			LogEntry(XML.HelpClasses.Log.MessageType.Informational, "----------------------------------------");
			LogEntry(XML.HelpClasses.Log.MessageType.Informational, "Start Processing Cache Files.");

            try
            {
                // Lodad Jacobline settings
                LoadSettings();

                var staticDataPath = Path.Combine(configuration.LocalTemp, settings.StaticData.HotelDirectory);
                var cachePath = Path.Combine(configuration.LocalTemp, settings.StaticData.CacheDirectory);

                var fullStaticData = configuration.Build.type == ConfigurationBuildType.full
                    || configuration.Build.type == ConfigurationBuildType.@static;
                var availabilityData = configuration.Build.type == ConfigurationBuildType.full
                    || configuration.Build.type == ConfigurationBuildType.availability;

                var sdata = new ProviderCommon.DataLoader.StaticData(staticDataPath, configuration.ServersConfigFileFullPath, this);
                // Remove availability server to avoid download data
                if (!availabilityData)
                    sdata.Configuration.Server = sdata.Configuration.Server.Where(s => s.id != "availabilityData").ToList();
                // Filter servers/files to download
                sdata.DownloadFiles();

                Jacobline jl = new Jacobline(sdata, DataMapper.GetMappingFor("Language")
                                                        .SelectMany(kv => kv.Value.Select(value => new { SHID = kv.Key, ProvId = value }))
                                                        .ToDictionary(m => m.ProvId, m => m.SHID));

                Jacobline.StaticData staticData = null;



                // --------------------
                // Hotels & Rooms
                // --------------------
                staticData = jl.GetStaticData(log, staticDataPath, settings, fullStaticData);

                List<Jacobline.CancellationPolicy> policies = null;
                List<Jacobline.ChildPolicy> childPolicy = null;
                List<Jacobline.MealPlan> mealPlan = null;
                List<Jacobline.MinimumStay> minStay = null;
                List<Jacobline.SpecialOffer> spo = null;
                Dictionary<int, Jacobline.AvailabilityHotel> availabilityHotels = null;
                if (availabilityData)
                {
                    // --------------------
                    // Cancellation Policies
                    // --------------------
                    policies = jl.GetCancellationPolicy(log, cachePath, settings.CachFiles.CancellationPolicyFile);

                    // --------------------
                    // Clild Policies
                    // --------------------
                    childPolicy = jl.GetChildPolicies(log, cachePath, settings.CachFiles.ChildPolicyFile);

                    // --------------------
                    // Meal Plans
                    // --------------------
                    mealPlan = jl.GetMealPlans(log, cachePath, settings.CachFiles.MealPlanFile);

                    // --------------------
                    // Minimum Stay
                    // --------------------
                    minStay = jl.GetMinimumStay(log, cachePath, settings.CachFiles.MinStayFile);

                    // --------------------
                    // Special Offers
                    // --------------------
                    spo = jl.GetSpecialOffers(log, cachePath, settings.CachFiles.SpecialOffersFile);

                    // --------------------
                    // Availability
                    // --------------------
                    Dictionary<int, string> hotelCurrencies = staticData == null ? null : staticData.hotelCurrencies;
                    availabilityHotels = jl.GetAvailability(log, cachePath, settings.CachFiles.AvailabilityFile, hotelCurrencies, DataMapper);
                }

                // Read room type mapping.
                var roomMapping = DataMapper.GetDynSingleMappingFor("RoomType");

                // --------------------
                // Parse: Hotels 
                // --------------------
                string strCountryId = String.Empty, strResortId = String.Empty, strCityId = String.Empty;
                Place placeCountry = null, placeResort = null, placeCity = null;
                Hotel hotel = null;
                MappingTable fatures = DataMapper["Feature"];

                var staticHotels = staticData.hotels.Values;
                foreach (Jacobline.StaticHotel staticHotel in staticHotels)
                {
                    placeCountry = placeResort = placeCity = null;
                    // Create country place
                    strCountryId = (staticHotel.AddressCountry.GetHashCode()).ToString();
                    placeCountry = root.Places.Find(delegate (Place place) { return place.Id.Equals(strCountryId); });
                    if (placeCountry == null)
                    {
                        placeCountry = new Place() { Id = strCountryId, Description = staticHotel.AddressCountry };
                        root.Places.Add(placeCountry);
                    }

                    // Create city place
                    strCityId = (String.Format("{0}.{1}", staticHotel.AddressCountry, staticHotel.AddressCity).GetHashCode()).ToString();
                    placeCity = placeCountry.Places.Find(delegate (Place place) { return place.Id.Equals(strCityId); });
                    if (placeCity == null)
                    {
                        placeCity = new Place() { Id = strCityId, Description = staticHotel.AddressCity };
                        placeCountry.Places.Add(placeCity);
                    }

                    // parse static hotel/room data
                    hotel = Parse(staticHotel, staticData.roomTypes, roomMapping, fatures);
                    hotel.PlaceId = placeCity.Id;


                    if (availabilityHotels != null && availabilityHotels.ContainsKey(staticHotel.ServiceId))
                    {

                        Jacobline.SpecialOffer hotelSpecialOffer = spo.Find(delegate (Jacobline.SpecialOffer so) { return so.ServiceID.ToString().Equals(hotel.Id); });

                        //// parse avalibility for roomgroups
                        //foreach (RoomGroup roomGroup in hotel.RoomGroups.Values)
                        //{
                        //    foreach (Room room in roomGroup.Rooms.Values)
                        //    {
                        //        Jacobline.AvailabilityHotel tmpHotel = null;
                        //        if (availabilityHotels.TryGetValue(int.Parse(hotel.Id), out tmpHotel))
                        //        {
                        //            foreach (Jacobline.Availability availability in tmpHotel.Availabilities(int.Parse(room.RoomId)).Values)
                        //            {
                        //                AvailabilityPeriod period = Parse(hotel.Id, room.RoomId, availability, tmpHotel.currencyCode, minStay, hotelSpecialOffer, childPolicy);

                        //                if (period != null)
                        //                {
                        //                    room.AvailabilityPeriods.Add(availability.date, period);
                        //                }
                        //            }
                        //        }
                        //    }
                        //}

                        // parse avalibility for rooms
                        foreach (Room room in hotel.Rooms.Values)
                        {
                            Jacobline.AvailabilityHotel tmpHotel = null;
                            if (availabilityHotels.TryGetValue(int.Parse(hotel.Id), out tmpHotel))
                            {
                                foreach (Jacobline.Availability availability in tmpHotel.Availabilities(int.Parse(room.RoomId)).Values)
                                {
                                    AvailabilityPeriod period = Parse(hotel.Id, room.RoomId, availability, minStay, hotelSpecialOffer, childPolicy);
                                    if (period != null)
                                    {
                                        room.AvailabilityPeriods.Add(availability.date, period);
                                    }
                                }
                            }
                        }
                    }
                    placeCity.Hotels.Add(hotel.Id, hotel);
                }

                // log hotels with low base price
                if (lowBasePriceBlockList != null && lowBasePriceBlockList.Count > 0)
                {
                    foreach (string blockedBoard in lowBasePriceBlockList)
                    {
                        LogEntry(XML.HelpClasses.Log.MessageType.Warning, "LOW BASEPRICE: Hotel.Room [{0}] ", blockedBoard);
                    }
                }

                // log hotels with currency missmatch
                if (currencyMissMatchBlockList != null && currencyMissMatchBlockList.Count > 0)
                {
                    foreach (string missMatch in currencyMissMatchBlockList)
                    {
                        string[] keySplit = missMatch.Split('.');
                        LogEntry(XML.HelpClasses.Log.MessageType.Error, "CURRENCY MISSING FOR HOTEL: Hotel {0} Room {1}", keySplit[0], keySplit[1]);
                    }
                }

                root.Places = root.Places.OrderBy(x => x.Description).ToList();

                // --------------------
                // Parse: RoomTypes
                // --------------------
                foreach (Jacobline.StaticRoomType staticRoomType in staticData.roomTypes.Values)
                {
                    root.RoomTypes.Add(staticRoomType.Id, new RoomType() { Type_Id = staticRoomType.Id, Description = staticRoomType.Description, Beds = staticRoomType.Beds, ExtraBeds = staticRoomType.ExtraBeds, NonRefundable = staticRoomType.NonRefundable });
                }

                // --------------------
                // Parse: Cancellation Policies 
                // --------------------
                if (policies != null)
                    foreach (Jacobline.CancellationPolicy policy in policies)
                    {
                        if (!root.CancellationsPolicies.ContainsKey(policy.Id.ToString()))
                        {
                            root.CancellationsPolicies.Add(policy.Id.ToString(), Parse(policy));
                        }
                        else
                        {
                            LogEntry(XML.HelpClasses.Log.MessageType.Warning, "Duplicate cancellation policy detected: {0}", policy.Id.ToString());
                        }
                    }

                // This is the key to optimize data
                Optimizer.OptimizeAvailabilityPeriods(root);

                LogEntry(XML.HelpClasses.Log.MessageType.Informational, "END getData()");
            }
            catch (Exception e)
            {
                LogEntry(XML.HelpClasses.Log.MessageType.Error, "Error on getData: " + e.ToString());
                throw;
            }
			
		}
		private void LoadSettings()
		{
			try
			{
				var serializer = new XmlSerializer(typeof(JacoblineSettings));
			    var file = new FileStream(Path.Combine(configuration.ProviderConfigRoot, "JacoblineSettings.xml"),
			                              FileMode.Open,
			                              FileAccess.Read);
				settings = (JacoblineSettings)serializer.Deserialize(file);
				file.Close();
			}
			catch (Exception ex)
			{
				throw new Exception("Error loading JacoblineSettings", ex);
			}
		}


		#region Parse methods

		public SunHotels.XML.Data.Hotel Parse(Jacobline.StaticHotel staticHotel, Dictionary<string, Jacobline.StaticRoomType> staticRoomtypes, DynSingleMappingTable roomType, MappingTable features)
		{
			var dst = root.Distance.FirstOrDefault(d => string.Compare(d.Name, "Airport", true) == 0);
			var norefundhotel = staticHotel.LongName.ToUpper().IndexOf("NON REFUNDABLE") != -1;
			SunHotels.XML.Data.Hotel hotel = new SunHotels.XML.Data.Hotel()
			{
				Id = staticHotel.ServiceId.ToString(),
				AccomodationType = null,
				Adress_City = staticHotel.AddressCity,
				Adress_Country = staticHotel.AddressCountry,
				Adress_State = String.Empty,
				Adress_Street1 = staticHotel.AddressLine1,
				Adress_Street2 = String.Format("{0} {1} {2}", staticHotel.AddressLine2, staticHotel.AddressLine3, staticHotel.AddressLine4),
				Adress_Zipcode = staticHotel.AddressPostCode,
				Classification = staticHotel.StarRating, // TODO: needs to be mapped
				Description = staticHotel.Description,
				Distances = new Hotel.Distance[]{
					string.IsNullOrEmpty(staticHotel.Distance) ?
					null:
					new Hotel.Distance(){
						Id = dst.Id,
						Name = dst.Name,
						ReferencePoint = staticHotel.NearestAirport,
						Value = staticHotel.Distance
					}
				}.Where(d => d != null).ToList(),
				Email = staticHotel.AddressEmailAddress,
				Extra = null,
				Fax = staticHotel.AddressFaxnumber,
				Headline = String.Empty,
				Images = staticHotel.Images,
				Name = staticHotel.LongName,
				Phone = staticHotel.AddressTelephoneNumber,
				PlaceId = String.Empty,
				Position_Latitude = staticHotel.Latitude,
				Position_Longitude = staticHotel.Longitude,
				RoomGroups = ParseRoomGroups(staticHotel.Rooms, staticRoomtypes, roomType, norefundhotel),
				Rooms = Parse(staticHotel.Rooms, staticRoomtypes, roomType, norefundhotel),
				Tag = String.Empty,
				Translations = staticHotel.Translations,
				// Amenities
				Features = (staticHotel.Facilities ?? string.Empty).Split(',')
									.SelectMany(f => features.Where(kv => kv.Value.Any(v => v == f))
									.Select(kv => kv.Key))
									.Select(f => root.Features.FirstOrDefault(rf => rf.Id == f))
									.Distinct()
									.ToList(),
				// Provider
				Identifiers = new Identifier[]{
					new Identifier()
				 {
					 Type = "provider",
					 Value = configuration.ProviderName
				 }}.ToList()
			};
			
			return hotel;
		}

		private Dictionary<string, SunHotels.XML.Data.RoomGroup> ParseRoomGroups(Dictionary<int, Jacobline.StaticRoom> staticRooms, Dictionary<string, Jacobline.StaticRoomType> staticRoomtypes, DynSingleMappingTable roomType, bool nonrefundable)
		{
			Dictionary<string, SunHotels.XML.Data.RoomGroup> roomGroups = new Dictionary<string, SunHotels.XML.Data.RoomGroup>();

			foreach (Jacobline.StaticRoom staticRoom in staticRooms.Values)
			{
				if (staticRoom.OccupancyTypeIds.Count > 1)
				{
					SunHotels.XML.Data.RoomGroup roomGroup = null;

					Dictionary<string, SunHotels.XML.Data.Room> rooms = new Dictionary<string, SunHotels.XML.Data.Room>();
					SunHotels.XML.Data.Room room = null;

					foreach (int typeId in staticRoom.OccupancyTypeIds)
					{
						var staticTypeId = (nonrefundable ? "NRF.": string.Empty) + typeId;
						Jacobline.StaticRoomType staticRoomType = null;

						string strOptionId = String.Format("{0}.{1}", staticTypeId, staticRoom.OptionName);

						if (!staticRoomtypes.ContainsKey(strOptionId))
						{
							var roomTypeNode = roomType[typeId.ToString()];
							staticRoomType = new Jacobline.StaticRoomType()
							{
								Id = String.Format("{0}.{1}.{2}", staticTypeId, roomTypeNode.Description, staticRoom.OptionName.GetHashCode()),
								Beds = roomTypeNode.Beds,
								ExtraBeds = roomTypeNode.Extrabeds,
								Description = staticRoom.OptionName,
								NonRefundable = nonrefundable
							};
							staticRoomtypes.Add(strOptionId, staticRoomType);
						}
						else
						{
							staticRoomType = staticRoomtypes[strOptionId];
						}

						room = new SunHotels.XML.Data.Room()
						{
							RoomId = staticRoom.OptionId.ToString(),
							AvailabilityPeriods = new SortedDictionary<DateTime, XML.Data.AvailabilityPeriod>(),
							Description = staticRoom.OptionName,
							TypeId = staticRoomType.Id
						};

						rooms.Add(staticTypeId, room);
					}

					roomGroup = new SunHotels.XML.Data.RoomGroup() { RoomGroupId = staticRoom.OptionId.ToString(), Rooms = rooms };
					roomGroups.Add(staticRoom.OptionId.ToString(), roomGroup);
				}
			}

			return roomGroups;
		}

		private Dictionary<string, SunHotels.XML.Data.Room> Parse(Dictionary<int, Jacobline.StaticRoom> staticRooms, Dictionary<string, Jacobline.StaticRoomType> staticRoomtypes, DynSingleMappingTable roomType, bool nonrefundable)
		{
			Dictionary<string, SunHotels.XML.Data.Room> rooms = new Dictionary<string, SunHotels.XML.Data.Room>();
			SunHotels.XML.Data.Room room = null;

			foreach (Jacobline.StaticRoom staticRoom in staticRooms.Values)
			{
				StringBuilder sbTypeIds = new StringBuilder();
				foreach (int typeId in staticRoom.OccupancyTypeIds)
				{
					sbTypeIds.AppendFormat("{0}{1}", sbTypeIds.Length > 0 ? "." : String.Empty, typeId);
				}

				if (staticRoom.OccupancyTypeIds.Count == 1)
				{
					Jacobline.StaticRoomType staticRoomType = null;
					var typeId = staticRoom.OccupancyTypeIds[0].ToString();
					var staticTypeId = (nonrefundable ? "NRF." : string.Empty) + typeId;
					string strOptionId = String.Format("{0}.{1}", staticTypeId, staticRoom.OptionName);

					if (!staticRoomtypes.ContainsKey(strOptionId))
					{
						var roomTypeNode = roomType[typeId];
						staticRoomType = new Jacobline.StaticRoomType()
						{
							Id = String.Format("{0}.{1}.{2}", staticTypeId, roomTypeNode.Description, staticRoom.OptionName.GetHashCode()),
							Beds = roomTypeNode.Beds,
							ExtraBeds = roomTypeNode.Extrabeds,
							Description = staticRoom.OptionName,
							NonRefundable = nonrefundable
						};
						staticRoomtypes.Add(strOptionId, staticRoomType);
					}
					else
					{
						staticRoomType = staticRoomtypes[strOptionId];
					}

					room = new SunHotels.XML.Data.Room()
					{
						RoomId = staticRoom.OptionId.ToString(),
						AvailabilityPeriods = new SortedDictionary<DateTime, XML.Data.AvailabilityPeriod>(),
						Description = staticRoom.OptionName,
						TypeId = staticRoomType.Id
					};

					rooms.Add(room.RoomId, room);
				}
			}

			return rooms;
		}

		private decimal IncludeChildPriceInBasePrice(List<Jacobline.ChildPolicy> childPolicies, Jacobline.Availability availability)
		{
			decimal pricing_ExtrabedChild = 0;

			// find child policy
			Jacobline.ChildPolicy childPolicy = childPolicies.Find(delegate(Jacobline.ChildPolicy c) { return c.ChildPolicyID == availability.childpolicy_id; });

			if (childPolicy != null)
			{
				if (!childPolicy.MaxUsage.HasValue)
				{
					pricing_ExtrabedChild = (availability.price / availability.beds) * childPolicy.PriceRate;
				}
				else
				{
					int nrFullPrice = ((int)childPolicy.MaxUsage > availability.extrabeds) ? 0 : (availability.extrabeds - (int)childPolicy.MaxUsage);
					int nrReducedPrice = availability.extrabeds - nrFullPrice;

					pricing_ExtrabedChild = (nrFullPrice * (availability.price / availability.beds)) + (nrReducedPrice * ((availability.price / availability.beds) * childPolicy.PriceRate));
					pricing_ExtrabedChild = (pricing_ExtrabedChild > 0 ? pricing_ExtrabedChild : 1);
				}
			}
			else
			{
				// if child policy is blocked, child price counts as full price
				pricing_ExtrabedChild = (availability.price / availability.beds) * availability.extrabeds;
			}

			return availability.price + (pricing_ExtrabedChild * availability.extrabeds);
		}

		public XML.Data.AvailabilityPeriod Parse(string hotelId, string roomId, Jacobline.Availability availability, List<Jacobline.MinimumStay> staticMinStay, Jacobline.SpecialOffer specialOffer, List<Jacobline.ChildPolicy> childPolicies)
		{
			XML.Data.AvailabilityPeriod period = new AvailabilityPeriod();

			if (String.IsNullOrEmpty(availability.currency))
			{
				string currencyKey = String.Format("{0}.{1}", hotelId, roomId);
				if (!currencyMissMatchBlockList.Contains(currencyKey))
				{
					currencyMissMatchBlockList.Add(currencyKey);
				}
				return null;
			}

			if (availability.currency != "EUR")
			{
                //Lets convert currencyvalues to euros to get correct end result
                try
                {
                    availability.price = XmlHelper.ConvertCurrency(availability.price, availability.currency, "EUR");
                }
                catch (Exception e)
                {
                    LogEntry(XML.HelpClasses.Log.MessageType.Warning, "Error converting price to EUR, currency {0} for hotel {1} room {2}: {3}", 
                        availability.currency, hotelId, roomId, e.ToString());
                    throw; // log and abort
                }
				
			}

			// find right minStay value
			Jacobline.MinimumStay minStay = staticMinStay.Find(delegate(Jacobline.MinimumStay ms) { return ms.MinStayRuleID.Equals(availability.minimumstayrule_id); });

			period.AdditionalBoards = new List<AvailabilityPeriod.AdditionalBoard>();
			period.AvailabilityAvailableUnits = (short)availability.availableUnits;
			period.AvailabilityTotalAllotment = (short)availability.availableUnits; // TODO check this!
			period.CancellationPolicies = new List<string>();
			period.DateFrom = availability.date;
			period.DateTemp = new DateTime();
			period.DateTo = availability.date;
			period.Extra = null;
			period.IsArrivalPossible = false;
			period.IsWeeklyStay = false;
			period.MinimumStay = (short)(minStay != null ? minStay.MinimumNights : 0);
			period.Pricing_BaseIncludedBoard = availability.mealplan;
			period.NoRefund = availability.norefund;

			decimal tmpBasePrice = 0;
			if (availability.extrabeds > 0)
			{
				tmpBasePrice = IncludeChildPriceInBasePrice(childPolicies, availability);
				period.Pricing_ExtrabedChild = "0";
			}
			else
			{
				tmpBasePrice = availability.price;
			}

			if (tmpBasePrice < 1)
			{
				string lowBasePriceKey = String.Format("{0}.{1}", hotelId, roomId);
				if (!lowBasePriceBlockList.Contains(lowBasePriceKey))
				{
					lowBasePriceBlockList.Add(lowBasePriceKey);
				}
				return null;
			}

			period.Pricing_BasePrice = tmpBasePrice.ToString(System.Globalization.CultureInfo.InvariantCulture);
			period.Pricing_BookableBoardId = availability.mealplan_id.ToString();
			period.ReleaseDays = 0;

			// set cancellation policies
			period.CancellationPolicies.Add(availability.cancellationpolicy_id.ToString());


			// check discount
			if (specialOffer != null)
			{
				bool validForCurrentRoomType = specialOffer.RoomTypes.Exists(delegate(string roomTypeName) { return roomTypeName.Equals(availability.roomname); });
				bool validForAnyStayDay = specialOffer.StayDays.ToUpper().Equals("ANY");
				bool validForMinimumOnePax = (specialOffer.MinimumPax != null ? (int)specialOffer.MinimumPax : 99) == 1;
				bool validForCurrentDate = (period.DateFrom.Date >= specialOffer.RuleValidFrom.Date && period.DateTo.Date <= specialOffer.RuleValidTo.Date);
				bool validForCurrentDay = (specialOffer.ArrivalDays.ToUpper().Equals("ANY", StringComparison.InvariantCultureIgnoreCase)
					|| specialOffer.ArrivalDays.ToUpper().Contains((period.DateFrom.DayOfWeek.ToString().ToUpper()).Substring(0, 3)));

				// (dropp spo if room not included, stay days specified, max pax != 1, criteria != 1 (per day/night) or arrivaldays don't contain current date) 
				if (validForCurrentRoomType
					&& validForAnyStayDay
					&& validForMinimumOnePax
					&& specialOffer.Criteria == 1
					&& validForCurrentDate
					&& validForCurrentDay)
				{
					int minDays = specialOffer.MinimumStayNights != null ? (int)specialOffer.MinimumStayNights : 25;

					switch (specialOffer.Offer)
					{
						case 1: // Pay stay 
							if (specialOffer.CalculationUnits < minDays)
							{
								period.Discounts.TryAdd(Discount.PayStay(minDays, specialOffer.MaximumLimit, specialOffer.CalculationUnits, null));
							}
							break;

						case 2: // Percentage reduction
							if (specialOffer.Amount > 0 && specialOffer.OfferNight.Equals("-1") && specialOffer.BookingNight.Equals("-1"))
							{
								period.Discounts.TryAdd(Discount.ValuePercent(minDays, specialOffer.Amount));
							}
							break;

						case 3: // Amount reduction
							if (specialOffer.Amount > 0 && specialOffer.OfferNight.Equals("-1") && specialOffer.BookingNight.Equals("-1"))
							{
								period.Discounts.TryAdd(Discount.ValueAmount(minDays, specialOffer.Amount));
							}
							break;

						//case 5: // Percentage supplement
						//    availabilityPeriod.Pricing_BasePrice = (availability.price * specialOffer.Amount).ToString(System.Globalization.CultureInfo.InvariantCulture);
						//    break;

						//case 6: // Amount supplement
						//    availabilityPeriod.Pricing_BasePrice = (availability.price + specialOffer.Amount).ToString(System.Globalization.CultureInfo.InvariantCulture);
						//    break;
					}
				}
			}

			if (String.IsNullOrEmpty(period.Pricing_BasePrice))
			{
				period = null;
			}

			return period;
		}

		public SunHotels.XML.Data.CancellationPolicy Parse(Jacobline.CancellationPolicy penalty)
		{
			string penaltyBasis = "";
			int penaltyValue = (int)penalty.CancellationPolicyDetails[0].Amount;

			// This specifies the policy type applied. There a currently 4 policy types which can be applied to cancellation policy:
			// 1.	Number of nights
			// 2.	Fixed fee
			// 3.	Percentage of full cost
			// 4.	Percentage of first night

			if (penalty.ChargeType == "Number of nights")
			{
				penaltyBasis = "nights";
			}
			else if (penalty.ChargeType == "Percentage of first night")
			{
				penaltyBasis = "nights";
				penaltyValue = (int)Math.Ceiling(penalty.CancellationPolicyDetails[0].Amount * (decimal)0.01);
			}
			else if (penalty.ChargeType == "Percentage of full cost")
			{
				penaltyBasis = "full_stay";
			}
			else if (penalty.ChargeType == "Fixed fee")
			{
				penaltyBasis = "fixed_amount";
			}
			else
			{
				LogEntry(XML.HelpClasses.Log.MessageType.Informational, "DROPPING CANCELLATIONPOLICY: {0}", penalty.Id);
			}

			SunHotels.XML.Data.CancellationPolicy policy = new SunHotels.XML.Data.CancellationPolicy()
			{
				Id = penalty.Id.ToString(),
				Deadline_Basis = "arrival",
				Deadline_Unit = "hours",
				Deadline_Value = penalty.CancellationPolicyDetails[0].NumberOfDays * 24,
				Penalty_Basis = penaltyBasis,
				Penalty_Value = penaltyValue
			};

			return policy;
		}

		#endregion

		public void Log(ILoggerStaticDataSeverity severity, string message, params object[] args)
		{
			XML.HelpClasses.Log.MessageType s = XML.HelpClasses.Log.MessageType.Informational;
			if (severity == ILoggerStaticDataSeverity.Error)
				s = XML.HelpClasses.Log.MessageType.Error;
			if (severity == ILoggerStaticDataSeverity.Warning)
				s = XML.HelpClasses.Log.MessageType.Warning;
			base.LogEntry(s, message, args);
		}
	}
}
