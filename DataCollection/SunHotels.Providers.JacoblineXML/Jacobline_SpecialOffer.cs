﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SunHotels.XML.HelpClasses;
using System.IO;
using System.Text.RegularExpressions;
using SunHotels.XML.Data;

namespace SunHotels.Providers
{

    partial class Jacobline
    {
        #region inner class SpecialOffer

        public class SpecialOffer
        {
            public int ServiceID { get; set; }
            public string ServiceName { get; set; }
            public DateTime RuleValidFrom { get; set; }
            public DateTime RuleValidTo { get; set; }
            public List<string> RoomTypes { get; set; }
            public string Specials { get; set; }
            public string SpecialsDescription { get; set; }
            public int? MinimumStayNights { get; set; }
            public int? MinimumPax { get; set; }
            public int CalculationUnits { get; set; }
            public int Criteria { get; set; }
            public int Offer { get; set; }
            public decimal Amount { get; set; }
            public int MaximumLimit { get; set; }
            public string OfferNight { get; set; }
            public string BookingNight { get; set; }
            public string ArrivalDays { get; set; }
            public string StayDays { get; set; }
        }

        #endregion

        public List<Jacobline.SpecialOffer> GetSpecialOffers(Logger log, string path, string fileName)
        {
            ParseStatus status = new ParseStatus();
            log.RecordNewLine(true);
            log.RecordMessage("Start Processing Special Offers Cache.", Log.MessageType.Informational);
            Console.WriteLine("Start Processing Special Offers Cache.");
            

            DirectoryInfo di = new DirectoryInfo(path);
			var fiList = di.GetFiles().Where(f => f.Name.IndexOf(fileName) != -1 && f.Extension == ".txt").FirstOrDefault();

            if (fiList == null)
            {
                log.RecordMessage("The file SpecialOffers was not found!", Log.MessageType.Error);
                throw new FileNotFoundException();
            }

            FileStream fs = new FileStream(fiList.FullName, FileMode.Open, FileAccess.Read);
            StreamReader sr = new StreamReader(fs);

            List<SpecialOffer> specialOffers = new List<SpecialOffer>();
            List<string> specialOfferIds = new List<string>();


            do
            {
                string s = sr.ReadLine();
                if (string.IsNullOrEmpty(s)) s = sr.ReadLine();

                char[] trimChar = { '#', ',' };
                Regex reg = new Regex("(#.*?#)|([^\\,]+)", RegexOptions.CultureInvariant);

                MatchCollection mc = reg.Matches(s);

                List<string> row = new List<string>();
                foreach (Match m in mc)
                {
                    row.Add(m.ToString());
                }

                int nServiceID = status.ParseInt(row[0]);

                string sServiceName = status.ParseString(row[1]);
                DateTime dtRuleValidFrom = status.ParseDateTime(row[2]);
                DateTime dtRuleValidTo = status.ParseDateTime(row[3]);
                List<string> lRoomTypes = status.ParseString(row[4].Trim(trimChar)).Split(',').ToList();
                string sSpecials = status.ParseString(row[5].Trim(trimChar));
                string sSpecialsDescription = status.ParseString(row[6].Trim(trimChar));
                int? nMinimumStayNights = status.ParseInt(row[7], false);
                int? nMinimumPax = status.ParseInt(row[8], false);
                int nCalculationUnits = status.ParseInt(row[9]);
                int nCriteria = status.ParseInt(row[10]);
                int nOffer = status.ParseInt(row[11]);
                decimal fAmount = status.ParseDecimal(row[12]);
                int nMaximumLimit = status.ParseInt(row[13]);
                string sOfferNight = status.ParseString(row[14], false);
                string sBookingNight = status.ParseString(row[15], false);
                string sArrivalDays = status.ParseString(row[16]);
                string sStayDays = status.ParseString(row[17]);

                if (status.Current)
                {
                    string strId = String.Concat(nServiceID,
                         sServiceName,
                         dtRuleValidFrom.ToShortDateString(),
                         dtRuleValidTo.ToShortDateString(),
                         lRoomTypes.ToString(),
                         sSpecials,
                         sSpecialsDescription,
                         nMinimumStayNights,
                         nMinimumPax,
                         nCalculationUnits,
                         nCriteria,
                         nOffer,
                         fAmount,
                         nMaximumLimit,
                         sOfferNight,
                         sBookingNight,
                         sArrivalDays,
                         sStayDays);

                    SpecialOffer specialOffer = new SpecialOffer()
                    {
                        ServiceID = nServiceID,
                        ServiceName = sServiceName,
                        RuleValidFrom = dtRuleValidFrom,
                        RuleValidTo = dtRuleValidTo,
                        RoomTypes = lRoomTypes,
                        Specials = sSpecials,
                        SpecialsDescription = sSpecialsDescription,
                        MinimumStayNights = nMinimumStayNights,
                        MinimumPax = nMinimumPax,
                        CalculationUnits = nCalculationUnits,
                        Criteria = nCriteria,
                        Offer = nOffer,
                        Amount = fAmount,
                        MaximumLimit = nMaximumLimit,
                        OfferNight = sOfferNight,
                        BookingNight = sBookingNight,
                        ArrivalDays = sArrivalDays,
                        StayDays = sStayDays
                    };

                    if (!specialOfferIds.Contains(strId))
                    {
                        specialOfferIds.Add(strId);
                        specialOffers.Add(specialOffer);
                    }

                }
                else
                {
                    status.Clear();
                    log.RecordMessage("ERROR PARSING SPECIALOFFER: " + s, Log.MessageType.Error);
                }

            } while (!sr.EndOfStream);


            
            log.RecordMessage("Done Processing Special Offers Cache.", Log.MessageType.Informational);

            return specialOffers;
        }



    }
}
