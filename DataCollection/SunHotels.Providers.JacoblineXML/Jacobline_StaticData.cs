﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using System.IO;

using SunHotels.Providers;
using SunHotels.XML;
using SunHotels.XML.Data;
using SunHotels.XML.HelpClasses;
using SunHotels.Providers.ProviderCommon;

namespace SunHotels.Providers
{
	public partial class Jacobline
	{
		// Read XML document for board mapping.
		Dictionary<string, string> languageMapping = new Dictionary<string, string>();
		SunHotels.Providers.ProviderCommon.DataLoader.StaticData dataLoader;
		public Jacobline(SunHotels.Providers.ProviderCommon.DataLoader.StaticData so, Dictionary<string, string> languageMapping)
		{
			dataLoader = so;
			this.languageMapping = languageMapping;

		}
		#region inner class StaticData (StaticHotel & StaticRoom & StaticRoomType)

		public class StaticData
		{
			public SortedDictionary<string, Jacobline.StaticHotel> hotels;
			public Dictionary<string, Jacobline.StaticRoomType> roomTypes;
			public Dictionary<int, string> hotelCurrencies;


			public StaticData()
			{
				hotels = new SortedDictionary<string, StaticHotel>();
				roomTypes = new Dictionary<string, StaticRoomType>();
				hotelCurrencies = new Dictionary<int, string>();
			}

		}

		public class StaticHotel
		{
			public int ServiceId { get; set; }
			public string LongName { get; set; }
			public string Description { get; set; }
			public int? PriceStartRange { get; set; }
			public int? PriceEndRange { get; set; }
			public string CurrencyCode { get; set; }
			public string Facilities { get; set; }
			public string StarRating { get; set; }
			public string Location { get; set; }
			public string LocationResort { get; set; }
			public string AddressLine1 { get; set; }
			public string AddressLine2 { get; set; }
			public string AddressLine3 { get; set; }
			public string AddressLine4 { get; set; }
			public string AddressPostCode { get; set; }
			public string AddressCity { get; set; }
			public string AddressCountry { get; set; }
			public string AddressTelephoneNumber { get; set; }
			public string AddressFaxnumber { get; set; }
			public string AddressEmailAddress { get; set; }
			public string AddressWebsiteURL { get; set; }
			public string MealPlan { get; set; }
			public Dictionary<int, StaticRoom> Rooms { get; set; }
			public Translations Translations { get; set; }
			public List<Hotel.Image> Images { get; set; }

			public string Latitude, Longitude, Distance, NearestAirport;
		}

		public class StaticRoom
		{
			public int OptionId { get; set; }
			public string OptionName { get; set; }
			public string OccupancyTypeName { get; set; }
			public List<int> OccupancyTypeIds { get; set; }
			public int? OptionGroupId { get; set; }
		}

		public class StaticRoomType
		{
			public string Id { get; set; }
			public string Description { get; set; }
			public int Beds { get; set; }
			public int ExtraBeds { get; set; }
			public bool  NonRefundable { get; set; }

			public StaticRoomType() {
				NonRefundable = false;
			}

			public StaticRoomType Clone() {
				return new StaticRoomType()
				{
					Id = this.Id,
					Description = this.Description,
					Beds = this.Beds,
					ExtraBeds = this.ExtraBeds,
					NonRefundable = this.NonRefundable
				};
			}
		}

		#endregion


		/// <summary>
		/// 
		/// </summary>
		/// <param name="node"></param>
		/// <returns></returns>
		private string GetInnerXml(XmlNode node)
		{
			return node != null ? node.InnerXml : String.Empty;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="log"></param>
		/// <param name="path"></param>
		/// <returns></returns>
		public Jacobline.StaticData GetStaticData(Logger log, string path, JacoblineSettings settings, bool fullStaticData)
		{
			List<string> multiplrRoomIds = new List<string>();

			Jacobline.StaticData staticData = new StaticData();

			log.RecordNewLine(true);
			log.RecordMessage("Start Processing Hotel Cache.", Log.MessageType.Informational);
			Console.WriteLine("Start Processing Hotel Cache.");
			

			DirectoryInfo di = new DirectoryInfo(path);

			if (!di.Exists)
			{
				string msg = string.Format(path + "The directory [{0}] was not found!", path);
				log.RecordMessage(msg, Log.MessageType.Error);
				throw new DirectoryNotFoundException(msg);
			}

			FileInfo[] fs = di.GetFiles("*.xml", SearchOption.TopDirectoryOnly);
			if (fs.Length == 0)
			{
				string msg = string.Format(path + "The file [{0}] was not found!", path);
				log.RecordMessage(msg, Log.MessageType.Error);
				throw new FileLoadException(msg);
			}

			ParseStatus hotelStatus = new ParseStatus();
			StringBuilder nonrefHotels = new StringBuilder();
			foreach (FileInfo f in fs)
			{
				int i = 0;
				if (!f.Name.StartsWith(ServersServerFolderFileType.HotelInfo.ToString()) ||
					!int.TryParse(f.Name.Replace(ServersServerFolderFileType.HotelInfo.ToString(), "").Replace(f.Extension, String.Empty), out i))
				{
					// Don't parse files with a name that isn't an int
					continue;
				}

				// Only use "DebugData.HotelId" when importing availability for a specific hotel/hotels
				if (settings.DebugData.HotelItems != null && !String.IsNullOrEmpty(settings.DebugData.HotelItems.First<HotelItem>().HotelId))
				{
					var ho = settings.DebugData.HotelItems.AsEnumerable().Where(hid => f.Name.StartsWith(hid.HotelId));
					if (ho.Count<HotelItem>() <= 0)
					{
						continue;
					}
				}

				XmlDocument xml = new XmlDocument();
				xml.Load(f.FullName);

				int nServiceID = hotelStatus.ParseInt(GetInnerXml(xml.SelectSingleNode("//SERVICEID")));


				string sLongName = hotelStatus.ParseString(GetInnerXml(xml.SelectSingleNode("//LONG_NAME")));
				string sCurrencyCode = hotelStatus.ParseString(GetInnerXml(xml.SelectSingleNode("//CURRENCY_CODE")), false);
				// create sorted dictionary 
				string sortableKey = nServiceID.ToString();

				string sDescription = hotelStatus.ParseString(GetInnerXml(xml.SelectSingleNode("//DESCRIPTION")));
				string sTermsInclutions = hotelStatus.ParseString(GetInnerXml(xml.SelectSingleNode("//TERMS_INCLUSIONS")), false);
				string sTermsExclutions = hotelStatus.ParseString(GetInnerXml(xml.SelectSingleNode("//TERMS_EXCLUSIONS")), false);

				int? nPriceStartRange = hotelStatus.ParseInt(GetInnerXml(xml.SelectSingleNode("//PRICE_START_RANGE")), false);
				int? nPriceEndRange = hotelStatus.ParseInt(GetInnerXml(xml.SelectSingleNode("//PRICE_END_RANGE")), false);


				string sFacilities = hotelStatus.ParseString(GetInnerXml(xml.SelectSingleNode("//FACILITIES")), false);
				string sStarRating = hotelStatus.ParseString(GetInnerXml(xml.SelectSingleNode("//STAR_RATING")), false);

				string sLocation = hotelStatus.ParseString(GetInnerXml(xml.SelectSingleNode("//LOCATION")), true);
				string sLocationResort = sLocation.Split(',')[1];

				string sAddress_Line1 = hotelStatus.ParseString(GetInnerXml(xml.SelectSingleNode("//LINE1")), false);
				string sAddress_Line2 = hotelStatus.ParseString(GetInnerXml(xml.SelectSingleNode("//LINE2")), false);
				string sAddress_Line3 = hotelStatus.ParseString(GetInnerXml(xml.SelectSingleNode("//LINE3")), false);
				string sAddress_Line4 = hotelStatus.ParseString(GetInnerXml(xml.SelectSingleNode("//LINE4")), false);

				string sAddress_Postcode = hotelStatus.ParseString(GetInnerXml(xml.SelectSingleNode("//POSTCODE")), false);
				string sAddress_City = hotelStatus.ParseString(GetInnerXml(xml.SelectSingleNode("//CITY")));
				string sAddress_Country = hotelStatus.ParseString(GetInnerXml(xml.SelectSingleNode("//COUNTRY")));
				string sAddress_thelephoneNumber = hotelStatus.ParseString(GetInnerXml(xml.SelectSingleNode("//TELEPHONENUMBER")), false);
				string sAddress_faxNumber = hotelStatus.ParseString(GetInnerXml(xml.SelectSingleNode("//FAXNUMBER")), false);
				string sAddress_email = hotelStatus.ParseString(GetInnerXml(xml.SelectSingleNode("//EMAILADDRESS")), false);
				string sAddress_websiteurl = hotelStatus.ParseString(GetInnerXml(xml.SelectSingleNode("//WEBSITEURL")), false);
				string sAddress_state = hotelStatus.ParseString(GetInnerXml(xml.SelectSingleNode("//STATE")), false);

				string sServiceLongitute = hotelStatus.ParseString(GetInnerXml(xml.SelectSingleNode("//SERVICELONGITUTE")), false);
				string sServiceLatitute = hotelStatus.ParseString(GetInnerXml(xml.SelectSingleNode("//SERVICELATITUTE")), false);
				string sNEARESTAIRPORT = hotelStatus.ParseString(GetInnerXml(xml.SelectSingleNode("//NEARESTAIRPORT")), false);
				string sSERVICEDISTANCE = hotelStatus.ParseString(GetInnerXml(xml.SelectSingleNode("//SERVICEDISTANCE")), false);

				string sMealPlan = hotelStatus.ParseString(GetInnerXml(xml.SelectSingleNode("//MEAL_PLAN")), false);

				if (hotelStatus.Current)
				{
					Dictionary<int, StaticRoom> rooms = new Dictionary<int, StaticRoom>();
					ParseStatus roomStatus = new ParseStatus();

					foreach (XmlNode node in xml.SelectNodes("//OPTION"))
					{
						int nOptionId = roomStatus.ParseInt(GetInnerXml(node.SelectSingleNode("OPTION_ID")));
						string sOptionName = roomStatus.ParseString(GetInnerXml(node.SelectSingleNode("OPTION_NAME")));
						int nOccupancyTypeId = roomStatus.ParseInt(GetInnerXml(node.SelectSingleNode("OCCUPANCY_TYPE_ID")));
						string sOccupancyTypeName = roomStatus.ParseString(GetInnerXml(node.SelectSingleNode("OCCUPANCY_TYPE_NAME")));

						if (roomStatus.Current)
						{
							if (!rooms.ContainsKey(nOptionId))
							{
								rooms.Add(nOptionId,
									new StaticRoom
									{
										OptionName = sOptionName,
										OptionId = nOptionId,
										OccupancyTypeIds = new List<int> { nOccupancyTypeId },
										OccupancyTypeName = sOccupancyTypeName,
										OptionGroupId = nOptionId
									});
							}
							else
							{
								StaticRoom room = new StaticRoom();
								rooms.TryGetValue(nOptionId, out room);

								// test
								if (!multiplrRoomIds.Contains(nServiceID.ToString() + "." + nOptionId.ToString()))
								{
									multiplrRoomIds.Add(nServiceID.ToString() + "." + nOptionId.ToString());
									log.RecordMessage(String.Format("PARSING ROOMS: Duplicate rooms for hotel:  ServiceID [{0}] OptionID [{1}] Occurrence [{2}]", nServiceID, nOptionId, (xml.SelectNodes(String.Format("//OPTION[OPTION_ID = {0}]", nOptionId))).Count), Log.MessageType.Error);
								}
								if (!room.OccupancyTypeIds.Contains(nOccupancyTypeId))
								{
									room.OccupancyTypeIds.Add(nOccupancyTypeId);
								}
							}
						}
						else
						{
							roomStatus.Clear();
							log.RecordMessage("PARSING ROOMS: " + node.InnerXml, Log.MessageType.Error);
						}
					}

					// Translations
					var trans = xml.SelectNodes("//SERVICE_ITINERARY[@ITINTYPEID=\"2\"]").AsEnumerable()
							.Where(n => {
								var lang = n.Attributes["LANGCODE"].Value;
								var ok = languageMapping.ContainsKey(lang);
								if (!ok)
									log.RecordMessage(string.Format("Removing translation from hotel {0} language {1} not supported", nServiceID, lang), Log.MessageType.Error);
								return ok;
							})
							.Select(n => new
							{
								lang = languageMapping[n.Attributes["LANGCODE"].Value].Split('-')[0],
								country = languageMapping[n.Attributes["LANGCODE"].Value].Split('-')[1],
								text = n.InnerText
							});
					if (!staticData.hotels.ContainsKey(sortableKey))
					{
						staticData.hotels.Add(sortableKey,
							new StaticHotel
							{
								ServiceId = nServiceID,
								LongName = sLongName,
								Description = sDescription,
								PriceStartRange = nPriceStartRange,
								PriceEndRange = nPriceEndRange,
								CurrencyCode = sCurrencyCode,
								Facilities = sFacilities,
								StarRating = sStarRating,
								Location = sLocation,
								LocationResort = sLocationResort,
								AddressLine1 = sAddress_Line1,
								AddressLine2 = sAddress_Line2,
								AddressLine3 = sAddress_Line3,
								AddressLine4 = sAddress_Line4,
								AddressPostCode = sAddress_Postcode,
								AddressCity = sAddress_City,
								AddressCountry = sAddress_Country,
								AddressTelephoneNumber = sAddress_thelephoneNumber,
								AddressFaxnumber = sAddress_faxNumber,
								AddressEmailAddress = sAddress_email,
								AddressWebsiteURL = sAddress_websiteurl,
								MealPlan = sMealPlan,
								Rooms = rooms,
								Translations = fullStaticData ? new Translations()
								{
									description = trans.Select(t => new TranslationsDescription() { lang = t.lang, country = t.country, Value = t.text }).ToList()
								} : null,
								Latitude = sServiceLatitute,
								Longitude = sServiceLongitute,
								Distance = sSERVICEDISTANCE,
								NearestAirport = sNEARESTAIRPORT
							});

						staticData.hotelCurrencies.Add(nServiceID, sCurrencyCode);
					}
					else
					{
						log.RecordMessage("DROPPING HOTEL: " + "Duplicate hotelid: " + f.Name, Log.MessageType.Warning);
					}
				}
				else
				{
					hotelStatus.Clear();
					log.RecordMessage("PARSING HOTELS: " + f.Name, Log.MessageType.Error);
				}
			}
			
			log.RecordMessage("Done Processing Hotel Cache.", Log.MessageType.Informational);

			// Load images
			GetImages(log, path, settings, staticData);

			return staticData;
		}
		public void GetImages(Logger log, string path, JacoblineSettings settings, Jacobline.StaticData data)
		{
			System.IO.Directory.GetFiles(path)
				.Where(f => System.IO.Path.GetFileName(f).StartsWith(ServersServerFolderFileType.HotelImages.ToString()))
				.Select(f => dataLoader.CreateTableFromCSV(f))
				.SelectMany(t => t.Rows.AsEnumerable())
				.GroupBy(r => r["Hotel ID"].ToString())
				.All(g =>
				{
					if (data.hotels.ContainsKey(g.Key))
						data.hotels[g.Key].Images = new Hotel.Image[]{
							new Hotel.Image()
						{
							Id = g.Key,
							ImageVariants = g.Select(r => new Hotel.ImageVariant() { URL = r["Main Picture"].ToString(), Height = 0, Width = 0 }).ToList()
						}}.ToList();
					return true;
				});

		}
	}
}
