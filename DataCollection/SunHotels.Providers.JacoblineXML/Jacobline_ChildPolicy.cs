﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SunHotels.XML.HelpClasses;
using System.IO;
using System.Text.RegularExpressions;

namespace SunHotels.Providers
{
	partial class Jacobline
	{

		#region inner class ChildPolicy

		public class ChildPolicy
		{
			internal const string DO_NOT_USE = "do not use";
			internal const string ROOM_ONLY = "room only";
			internal const string SHARING = "sharing";
			internal const string MAX = "max";
			internal const int MAX_CHILD_AGE = 11;

			public int ChildPolicyID { get; set; }
			public string ChildPolicyDescription { get; set; }
			public int FromAge { get; set; }
			public int ToAge { get; set; }
			public int? MaxUsage { get; set; }
			internal int PercentageDiscount { get; set; }

			public decimal PriceRate { get { return (PercentageDiscount * new Decimal(0.01)); } }
		}

		#endregion

		public List<Jacobline.ChildPolicy> GetChildPolicies(Logger log, string path, string fileName)
		{

			ParseStatus status = new ParseStatus();
			log.RecordNewLine(true);
			log.RecordMessage("Start Processing Child Policy Cache.", Log.MessageType.Informational);
			Console.WriteLine("Start Processing Child Policy Cache.");
			

			DirectoryInfo di = new DirectoryInfo(path);
			var fiList = di.GetFiles().Where(f => f.Name.IndexOf(fileName) != -1 && f.Extension == ".txt").FirstOrDefault();

			if (fiList == null)
			{
				log.RecordMessage("The file ChildPolicies was not found!", Log.MessageType.Error);
				throw new FileNotFoundException();
			}

			FileStream fs = new FileStream(fiList.FullName, FileMode.Open, FileAccess.Read);
			StreamReader sr = new StreamReader(fs);

			List<ChildPolicy> childpolicies = new List<ChildPolicy>();

			Regex max = new Regex(@"max\s\d", RegexOptions.Singleline);
			Regex digit = new Regex(@"\d", RegexOptions.Singleline);

			List<int> childPolicyBlockList = new List<int>();

			do
			{
				string s = sr.ReadLine();
				if (string.IsNullOrEmpty(s)) s = sr.ReadLine();

				string[] row = s.Split(',');

				int nChildPolicyID = status.ParseInt(row[0]);
				string sChildPolicyDescription = status.ParseString(row[1]);
				int nFromAge = status.ParseInt(row[2]);
				int nToAge = status.ParseInt(row[3]);
				int nPercentageDiscount = status.ParseInt(row[4].Replace("%", string.Empty));

				if (status.Current)
				{
					// block unwated child policies
					if (!(sChildPolicyDescription.ToLower().Contains(ChildPolicy.DO_NOT_USE)
						|| sChildPolicyDescription.ToLower().Contains(ChildPolicy.ROOM_ONLY)
						|| sChildPolicyDescription.ToLower().Contains(ChildPolicy.SHARING)))
					{

						if (nFromAge <= ChildPolicy.MAX_CHILD_AGE)
						{

							string match = digit.Match(max.Match(sChildPolicyDescription).Value).Value;
							int? maxUsage = null;
							if (!String.IsNullOrEmpty(match))
							{
								maxUsage = Convert.ToInt32(match);
							}

							ChildPolicy policy = childpolicies.Find(delegate(ChildPolicy c) { return c.ChildPolicyID.Equals(nChildPolicyID); });

							if (policy != null)
							{
								if ((policy.FromAge < nFromAge))
								{
									policy.ChildPolicyDescription = sChildPolicyDescription;
									policy.FromAge = nFromAge;
									policy.ToAge = nToAge;
									policy.MaxUsage = maxUsage;
									policy.PercentageDiscount = nPercentageDiscount;
								}
							}
							else
							{
								childpolicies.Add(new ChildPolicy()
								{
									ChildPolicyID = nChildPolicyID,
									ChildPolicyDescription = sChildPolicyDescription,
									FromAge = nFromAge,
									ToAge = nToAge,
									PercentageDiscount = nPercentageDiscount
								});
							}
						}
					}
					else
					{
						if (!childPolicyBlockList.Contains(nChildPolicyID))
						{
							childPolicyBlockList.Add(nChildPolicyID);
						}
					}
				}
				else
				{
					status.Clear();
					log.RecordMessage("ERROR PARSING CHILDPOLICY: " + s, Log.MessageType.Warning);
				}

			} while (!sr.EndOfStream);


			// log blocked policies 
			if (childPolicyBlockList.Count > 0)
			{
				StringBuilder sbBlockedPolicies = new StringBuilder();
				foreach (int blockedPolicy in childPolicyBlockList)
				{
					sbBlockedPolicies.AppendFormat("{0}{1}", sbBlockedPolicies.Length > 0 ? "." : String.Empty, blockedPolicy);
				}
				log.RecordMessage("DROPPING CHILDPOLICY: [" + sbBlockedPolicies.ToString() + "] blocked", Log.MessageType.Warning);
			}


			

			log.RecordMessage("Done Processing Child Policy Cache.", Log.MessageType.Informational);
			return childpolicies;
		}



	}
}
