﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SunHotels.XML.HelpClasses;
using System.IO;

namespace SunHotels.Providers
{
	partial class Jacobline
	{

		#region inner class MealPlan
		
		public class MealPlan
		{
			public int MealPlanID { get; set; }
			public string MealPlanName { get; set; }
			public string Breakfast { get; set; }
			public string Lunch { get; set; }
			public string Dinner { get; set; }
		}

		#endregion



		public List<Jacobline.MealPlan> GetMealPlans(Logger log, string path, string fileName)
		{

			ParseStatus status = new ParseStatus();
			log.RecordNewLine(true);
			log.RecordMessage("Start Processing Meal Plan Cache.", Log.MessageType.Informational);
			Console.WriteLine("Start Processing Meal Plan Cache.");
			

			DirectoryInfo di = new DirectoryInfo(path);
			var fiList = di.GetFiles().Where(f => f.Name.IndexOf(fileName) != -1 && f.Extension == ".txt").FirstOrDefault();


			if (fiList == null)
			{
				log.RecordMessage("The file MealPlans was not found!", Log.MessageType.Error);
				throw new FileNotFoundException();
			}

			FileStream fs = new FileStream(fiList.FullName, FileMode.Open, FileAccess.Read);
			StreamReader sr = new StreamReader(fs);

			//TODO: add functionallity to get newest file from ftp;
			List<MealPlan> mealplans = new List<MealPlan>();

			do
			{
				string s = sr.ReadLine();
				if (string.IsNullOrEmpty(s)) s = sr.ReadLine();

				string[] row = s.Split(',');

				int nMealPlanID = status.ParseInt(row[0]);
				string sMealPlanName = status.ParseString(row[1]);
				string sBreakfast = status.ParseString(row[2], false);
				string sLunch = status.ParseString(row[3], false);
				string sDinner = status.ParseString(row[4], false);

				if (status.Current)
				{
					mealplans.Add(new MealPlan()
					{
						MealPlanID = nMealPlanID,
						MealPlanName = sMealPlanName,
						Breakfast = sBreakfast,
						Lunch = sLunch,
						Dinner = sDinner
					});
				}
				else
				{
					status.Clear();
					log.RecordMessage("ERROR PARSING CANCELLATIONPOLICY: " + s, Log.MessageType.Warning);
				}

			} while (!sr.EndOfStream);

			
			log.RecordMessage("Done Processing Meal Plan Cache.", Log.MessageType.Informational);

			return mealplans;
		}





	}
}
