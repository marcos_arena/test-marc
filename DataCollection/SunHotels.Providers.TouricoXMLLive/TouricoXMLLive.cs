using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using SunHotels.XML;
using SunHotels.XML.Data;
using SunHotels.Providers.ProviderCommon.DataLoader;
using Sunhotels.Export;
using System.Threading;

namespace SunHotels.Providers
{
    /// <summary>
    /// Fetch and parse information from Tourico FTP CSV files and SOAP WebService. Add this information to Sunhotels XML structure.
    /// </summary>
    public class TouricoXMLLive : XMLProvider, IStaticDataLogger
    {
        // Create dictionaries for fast search
        Dictionary<string, Place> allPlaces = new Dictionary<string, Place>(1000);
        Dictionary<string, Hotel> allHotels = new Dictionary<string, Hotel>(10000);
        Dictionary<string, string> allBoards = new Dictionary<string, string>(100);
        Dictionary<string, DataRow> allResortFees = new Dictionary<string, DataRow>(10000);

        private static readonly string MAGIC_NUMBER_FOR_APARTMENTS = "3.7";

        /// <summary>
        /// 
        /// </summary>
        /// <param name="root"></param>
        protected override void getProviderDefinition(Root root)
        {
            root.ProviderDefinition.Name = configuration.ProviderName;
            root.ProviderDefinition.Currency = "EUR";
        }

        protected override void UpdateConfiguration(Configuration configuration)
        {
            configuration.WriteHotelsWithoutRooms = true;
            configuration.WriteRoomsWithoutAvailability = true;
        }

        /// <summary>
        /// Entry point for class.
        /// </summary>
        /// <param name="root">Root for Sunhotels XML structure.</param>
        protected override void getData(Root root)
        {
            try
            {
                this.root = root;

                // ------------------------------------------------------------------------------------------------
                // Create log stream
                // ------------------------------------------------------------------------------------------------
                try
                {
                    Directory.CreateDirectory(configuration.LogOutput);
                }
                catch (Exception ex)
                {
                    throw new ApplicationException("Unable to create TouricoXMLLive log file", ex);
                }
                Log(ILoggerStaticDataSeverity.Info, "Stating process");
                // Get all static data from ftp
                GetStaticData();

                Log(ILoggerStaticDataSeverity.Info, "Processing all hotels");
                foreach (var item in allHotels)
                {
                    allPlaces[item.Value.PlaceId].Hotels.Add(item.Value.Id, item.Value);
                }
                Log(ILoggerStaticDataSeverity.Info, "Process finished");
            }
            catch (Exception ex)
            {
                Log(ILoggerStaticDataSeverity.Info, "Unhandled exception [{0}] ", ex.ToString());
                throw;
            }
            finally
            {
            }
        }

        protected override void getFeatureTypes(Root root)
        {
            // Get SH features mapped with the provider
            DataMapper.SunHotelsFeatures.Join(DataMapper["Feature"], o => o.Key, i => i.Key, (o, i) => o)
                .All(f =>
                {
                    root.Features.Add(new Feature()
                    {
                        Id = f.Key,
                        Value = f.Value,
                        Name = f.Value
                    });
                    return true;
                });
        }

        /// <summary>
        /// Save all static CSV data in dictionaries to use later when mapping dynamic data.
        /// </summary>
        private void GetStaticData()
        {
            // Read XML document for hotel feature mapping.
            var features = DataMapper.SunHotelsFeatures.Join(DataMapper["Feature"], o => o.Key, i => i.Key, (o, i) => new { SHFeature = o, ProviderFeature = i })
                            .SelectMany(o => o.ProviderFeature.Value.Select(
                                of => new
                                {
                                    ProviderId = of,
                                    SHFeature = o.SHFeature
                                }
                                ))
                            .ToDictionary(o => o.ProviderId, o => o.SHFeature);

            // Read XML document for board mapping.
            var boards = DataMapper["Board"].SelectMany(o => o.Value.Select(
                                of => new
                                {
                                    ProviderId = of,
                                    SHFeature = o
                                }
                                ))
                            .ToDictionary(o => o.ProviderId, o => o.SHFeature);

            // Get all static data 
            StaticData csvData = new StaticData(configuration.LocalTemp, configuration.ServersConfigFileFullPath, this);
            csvData.MaxDegreeOfParallelism = configuration.MaxDegreeOfParallelism;
            csvData.MaxWebRequestConnections = configuration.MaxDegreeOfParallelism;
            csvData.MaxWebRequestTimeout = Timeout.Infinite;

            csvData.RemoteDirectoryListingFilter = (remotePath, fileInfo, direcotyListing) =>
            {
                // Filter chiness
                if (fileInfo.type == ProviderCommon.ServersServerFolderFileType.HotelDescriptions)
                {
                    direcotyListing = direcotyListing.Where(f => f.Name.IndexOf("zh_") == -1)
                        .Concat(direcotyListing.Where(f => f.Name.IndexOf("zh_Hans") != -1 || f.Name.IndexOf("zh_CN") != -1)
                        .OrderBy(f => f.Name.IndexOf("Hans") != -1 ? 0 : 1) // Preferable Hans, Acceptable CN
                        .Take(1)).ToArray();
                }
                return direcotyListing;
            };

            Log(ILoggerStaticDataSeverity.Info, "GetStaticData started!");
            IEnumerable<DataTable> csvDataTables = csvData.GetStaticData();
            Log(ILoggerStaticDataSeverity.Info, "GetStaticData finished!");

            int processedFiles = 0;
            Log(ILoggerStaticDataSeverity.Info, "Processing tables");


            // Loop through all static data
            foreach (DataTable dt in csvDataTables)
            {
                string csvFileName = dt.TableName;
                Log(ILoggerStaticDataSeverity.Info, "{0} {1} Rows", csvFileName, dt.Rows.Count);
                processedFiles++;
                if (csvFileName.StartsWith("HotelInfo"))
                {
                    // Create XML structure Place and Hotel from imported tables
                    foreach (DataRow dr in dt.Rows)
                    {
                        try
                        {                            
                            string sCountryCode = (string)dr["CountryCode"];
                            string sCountryName = (string)dr["CountryName"];
                            string sStateCode = (string)dr["StateCode"];
                            string sState = (string)dr["State"];
                            string sCity = (string)dr["AddressCity"]; 

                            Place hotelPlace = GetPlace(sCountryName.Trim(), sCountryCode.Trim(), sStateCode.Trim(), sState.Trim(), sCity.Trim());
                            Hotel newHotel = CreateHotel(dr, hotelPlace);

                            if (!hotelPlace.Hotels.ContainsKey(newHotel.Id))
                            {
                                // Save hotel information in hash table for fast access.
                                allHotels.Add(newHotel.Id, newHotel);
                            }
                            else
                            {
                                Log(ILoggerStaticDataSeverity.Info, "Hotel Id: {0} already exist in the hotel collection", newHotel.Id);
                            }
                        }
                        catch (Exception ex)
                        {
                            Log(ILoggerStaticDataSeverity.Info, string.Format("Invalid data detected in {0}: {1}", csvFileName, ex.Message));
                        }
                    }
                }
                else if (csvFileName.StartsWith("HotelBoards"))
                {
                    // Save static board information in hash table for fast access
                    foreach (DataRow dr in dt.Rows)
                    {
                        try
                        {
                            var b = dr["BoardId"].ToString();
                            if (boards.ContainsKey(b))
                            {
                                allBoards.Add(b, boards[b].Key);
                            }
                        }
                        catch (Exception ex)
                        {
                            Log(ILoggerStaticDataSeverity.Info, string.Format("Invalid data detected in {0}: {1}", csvFileName, ex.Message));
                        }
                    }
                }
                else if (csvFileName.StartsWith("HotelAmenities"))
                {
                    // Save static hotel feature information in hash table for fast access
                    foreach (DataRow dr in dt.Rows)
                    {
                        try
                        {
                            var hotelId = dr["HotelID"].ToString();
                            if (!allHotels.ContainsKey(hotelId))
                                continue;
                            var hotel = allHotels[hotelId];
                            var amenity = dr["AmenityId"].ToString();
                            if (features.ContainsKey(amenity))
                            {
                                var shfeature = features[amenity];
                                if (!hotel.Features.Any(f => f.Id == shfeature.Key))
                                {
                                    hotel.Features.Add(new Feature()
                                    {
                                        Id = shfeature.Key,
                                        Name = shfeature.Value,
                                        Value = shfeature.Value
                                    });
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            Log(ILoggerStaticDataSeverity.Info, string.Format("Invalid data detected in {0}: {1}", csvFileName, ex.Message));
                        }
                    }
                }
                else if (csvFileName.StartsWith("ResortFee"))
                {
                    // Save overriding static room type cancellation policy exceptions in hash table for fast access
                    foreach (DataRow dr in dt.Rows)
                    {
                        try
                        {
                            string feeKey = String.Format("{0}", dr["HotelId"].ToString());
                            // Add new fee if it does�t already exists
                            if (!allResortFees.ContainsKey(feeKey))
                            {
                                allResortFees.Add(feeKey, dr);
                            }
                        }
                        catch (Exception ex)
                        {
                            Log(ILoggerStaticDataSeverity.Info, string.Format("Invalid data detected in {0}: {1}", csvFileName, ex.Message));
                        }
                    }
                }
                else if (csvFileName.StartsWith("HotelImages"))
                {
                    // Save images in hash table for fast access
                    foreach (DataRow dr in dt.Rows)
                    {
                        try
                        {
                            var hotelId = dr["HotelID"].ToString();
                            if (!allHotels.ContainsKey(hotelId))
                                continue;
                            var hotel = allHotels[hotelId];
                            var url = dr["ImageURL"].ToString();
                            if (!hotel.Images.Any(i => i.ImageVariants.Any(iv => iv.URL == url)))
                                hotel.Images.Add(new Hotel.Image()
                                {
                                    ImageVariants = new List<Hotel.ImageVariant>()
                                        {
                                            new Hotel.ImageVariant()
                                            {
                                                URL = url,
                                                Height = 200,
                                                Width = 200
                                            }
                                        }
                                });
                        }
                        catch (Exception ex)
                        {
                            Log(ILoggerStaticDataSeverity.Info, string.Format("Invalid data detected in {0}: {1}", csvFileName, ex.Message));
                        }
                    }
                }
                else if (csvFileName.StartsWith("HotelDescriptions"))
                {
                    var m = System.Text.RegularExpressions.Regex.Match(csvFileName, "^.*_([a-z]{2,2})_([A-Z]{2,2})\\.csv$");
                    var langCode = m.Groups[1].Value;
                    var countryCode = m.Groups[2].Value;
                    if (dt.Rows.Count != 0)
                        LoadTranslation(langCode, countryCode, dt);
                }
                else
                    throw new ApplicationException(String.Format("Unknown tablename for static data. Tablename[{0}]", csvFileName));
            }

            if (processedFiles < csvData.TotalFiles)
            {
                throw new ApplicationException(String.Format("At least one static data file missing. Processed tables[{0}]", processedFiles));
            }
        }

        private void LoadTranslation(String langCode, String countryCode, DataTable dt)
        {
            var shortDesc = String.Format("{0}_{1}_{2}", "ShortDescription", langCode, countryCode);
            var longDesc = String.Format("{0}_{1}_{2}", "LongDescription", langCode, countryCode);
            // Find hotel
            foreach (DataRow r in dt.Rows)
            {
                var id = r["HotelId"].ToString();
                if (!allHotels.ContainsKey(id))
                    continue;
                var hotel = allHotels[id];
                var desc = r[longDesc].ToString();
                if (!String.IsNullOrEmpty(desc) && !hotel.Translations.description.Any(d => d.lang == langCode && d.country == countryCode))
                    hotel.Translations.description.Add(new TranslationsDescription()
                    {
                        lang = langCode,
                        country = countryCode,
                        Value = desc
                    });
                var hdesc = r[shortDesc].ToString().Trim();
                if (!String.IsNullOrEmpty(hdesc) && !hotel.Translations.headline.Any(d => d.lang == langCode && d.country == countryCode))
                    hotel.Translations.headline.Add(new TranslationsHeadline()
                    {
                        lang = langCode,
                        country = countryCode,
                        Value = hdesc
                    });
            }
        }


        /// <summary>
        /// Get previously created place or create new.
        /// </summary>
        /// <param name="hotelRow">The data row containing hotel information.</param>
        /// <param name="hotelPlace">The place under witch the hotel will be created.</param>
        /// <returns>The requested hotel.</returns>
        private Hotel CreateHotel(DataRow hotelRow, Place hotelPlace)
        {
            Hotel hNew = new Hotel();
            hNew.PlaceId = hotelPlace.Id;
            hNew.Id = hotelRow["HotelId"].ToString();
            hNew.Name = hotelRow["HotelName"].ToString();
            hNew.Adress_City = hotelRow["AddressCity"].ToString();
            hNew.Adress_Country = hotelRow["CountryName"].ToString();
            hNew.Adress_State = hotelRow["State"].ToString();

            if (hNew.Id == "1032069")
            {
                hNew.Adress_Street1 = "";  // temp fix since the address contains contral characters that f..ks up the xml
            }
            else
            {
                hNew.Adress_Street1 = hotelRow["Address"].ToString();
            }

            hNew.Adress_Zipcode = hotelRow["AddressZip"].ToString();
            var stars = hotelRow["Stars"].ToString();

            // Sanitize
            if (stars.Equals(MAGIC_NUMBER_FOR_APARTMENTS))
            {
                hNew.AccomodationType = AccomodationType.Apartment;
                // No classification for apartments
                hNew.Classification = String.Empty;
            }
            else
            {
                hNew.AccomodationType = AccomodationType.Hotel;
                hNew.Classification = stars.Replace(".5", "+"); // Half star
            }

            hNew.Description = hotelRow["ShortDescription"].ToString();

            double dblDistance = 0;

            if (double.TryParse(hotelRow["RefPointDist"].ToString(), NumberStyles.Float, CultureInfo.InvariantCulture, out dblDistance))
            {
                if (hotelRow["DistUnit"].ToString() == "Mile")
                {
                    // Convert miles to km if distance to airport is specified in miles.
                    dblDistance = dblDistance * 1.609;
                }
                Hotel.Distance newDistance = new Hotel.Distance("airport", Decimal.Round((decimal)dblDistance, 1), hotelRow["NearestAirportIATACode"].ToString());
                hNew.Distances.Add(newDistance);
            }

            hNew.Fax = hotelRow["Fax"].ToString();
            hNew.Phone = hotelRow["Phone"].ToString();
            hNew.Position_Latitude = hotelRow["Latitude"].ToString();
            hNew.Position_Longitude = hotelRow["Longitude"].ToString();
            hNew.BestBuy = hotelRow["ExclusiveDeal"].ToString() == "1";

            // Provider
            var realprovider = hotelRow["Provider"].ToString();
            hNew.Email = realprovider;
            // Provider
            hNew.Identifiers.Add(new Identifier()
            {
                Type = "provider",
                Value = configuration.ProviderName
            });
            return hNew;
        }


        /// <summary>
        /// Get previously created place or create new.
        /// </summary>
        /// <param name="sCountryCode">The country code.</param>
        /// <param name="sStateCode">The state/zip code.</param>
        /// <param name="sState">The state name.</param>
        /// <param name="sCity">The city name.</param>
        /// <returns>The requested palce.</returns>
        private Place GetPlace(string sCountryName, string sCountryCode, string sStateCode, string sState, string sCity)
        {

            // Check or create country level place
            string sCountryID = sCountryCode.ToLower();
            if (allPlaces.ContainsKey(sCountryID) != true)
            {
                Place newPlace = new Place();
                newPlace.Id = sCountryID;
                newPlace.Description = sCountryName;
                allPlaces.Add(sCountryID, newPlace);

                root.Places.Add(newPlace);
            }

            // Check or create state level place if state is specified
            string sStateID = null;
            if (string.IsNullOrEmpty(sStateCode) != true)
            {
                sStateID = string.Format("{0}.{1}", sCountryCode.ToLower(), sStateCode.ToLower());
                if (allPlaces.ContainsKey(sStateID) != true)
                {
                    Place newPlace = new Place();
                    newPlace.Id = sStateID;
                    newPlace.Description = sState;
                    allPlaces.Add(sStateID, newPlace);

                    allPlaces[sCountryID].Places.Add(newPlace);
                }
            }

            // Check or create city level place skipping state level if not present
            string sCityID = null;

            if (string.IsNullOrEmpty(sStateCode) != true)
            {
                sCityID = CreateCityId(sCountryCode, sStateCode, sCity);

                if (allPlaces.ContainsKey(sCityID) != true)
                {
                    Place newPlace = new Place();
                    newPlace.Id = sCityID;
                    newPlace.Description = sCity;
                    allPlaces.Add(sCityID, newPlace);
                    allPlaces[sStateID].Places.Add(newPlace);
                }
            }
            else
            {
                sCityID = string.Format("{0}.{1}", sCountryCode.ToLower(), sCity.ToLower());
                if (allPlaces.ContainsKey(sCityID) != true)
                {
                    Place newPlace = new Place();
                    newPlace.Id = sCityID;
                    newPlace.Description = sCity;
                    allPlaces.Add(sCityID, newPlace);
                    allPlaces[sCountryID].Places.Add(newPlace);
                }
            }

            return allPlaces[sCityID];

        }

        private static string CreateCityId(string sCountryCode, string sStateCode, string sCity)
        {
            string cityName = sCity.ToLower();
            return string.Format("{0}.{1}.{2}", sCountryCode.ToLower(), sStateCode.ToLower(), cityName);
        }
         
        public void Log(ILoggerStaticDataSeverity severity, string message, params object[] args)
        {
            XML.HelpClasses.Log.MessageType s = XML.HelpClasses.Log.MessageType.Informational;
            if (severity == ILoggerStaticDataSeverity.Error)
                s = XML.HelpClasses.Log.MessageType.Error;
            if (severity == ILoggerStaticDataSeverity.Warning)
                s = XML.HelpClasses.Log.MessageType.Warning;
            base.LogEntry(s, message, args);
        }
    }
}
