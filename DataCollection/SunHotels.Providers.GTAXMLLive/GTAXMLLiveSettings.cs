﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace SunHotels.Providers
{
    [Serializable]
    [XmlRoot(ElementName = "Language")]
    public class Language
    {
        [XmlElement(ElementName = "ShCode")]
        public string ShCode { get; set; }
        [XmlElement(ElementName = "GtaCode")]
        public string GtaCode { get; set; }
    }
    [Serializable]
    [XmlRoot(ElementName = "LanguageList")]
    public class LanguageList
    {
        [XmlElement(ElementName = "Language")]
        public List<Language> Language { get; set; }
    }

    [Serializable]
    [XmlRoot(ElementName = "GTAXMLLiveSettings")]
    public class GTAXMLLiveSettings
    {
        public string ProtocolVersion { get; set; }
        public string ContentType { get; set; }        
        public string ResponseDownloadContentType { get; set; }
        public string ResponseErrorContentType { get; set; }
        public bool KeepAlive { get; set; }
        public bool DeleteZipFile { get; set; }
        public bool DeleteFileAfterProcessing { get; set; }
        public string Client { get; set; }
        public string MethodType { get; set; }
        public string Country { get; set; }
        public string Language { get; set; }
        public string DownloadType { get; set; }
        public int FromdateDifferenceInDays { get; set; }
        public int TodateDifferenceInDays { get; set; }
        public int TimeDifferenceMinutes { get; set; }
        public int RequestTimeout { get; set; }
        public int MaxHotelsTranslationProcessing { get; set; }
        public int MaxDownloadRequest { get; set; }
        public int MaxRetryDownloadRequest { get; set; }
        public int MaxUnzip { get; set; }
        public int MaxTranslationProcessing { get; set; }
        public int MaxHotelsProcessing { get; set; }
        public int MaxCityRequestSize { get; set; }
        public int MaxFailedRequestRetryAttempts { get; set; }
        public bool CreateImages { get; set; }
        public bool CreateRooms { get; set; }
        public int ImageHeight { get; set; }
        public int ImageWidth { get; set; }
        [XmlElement(ElementName = "LanguageList")]
        public LanguageList LanguageList { get; set; }
    }
}
