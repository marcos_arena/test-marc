﻿using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;

namespace SunHotels.Providers
{
    public class RestApiUtils
    {
        public string RequestUrl { get; set; }
        public string RequestContentType { private get; set; }
        public string ProtocolVersion { private get; set; }
        public string PostParameter { get; set; }
        public int Timeout { private get; set; }
        public System.Text.Encoding Encoding { private get; set; }

        /// <summary>
        /// HTTP Post
        /// </summary>
        /// <param name="cancellationToken">CancellationToken</param>
        /// <returns>string</returns>
        public async Task<string> Post(System.Threading.CancellationToken cancellationToken)
        {
            using (var wc = GetWebClient(cancellationToken))
                return await wc.UploadStringTaskAsync(RequestUrl, PostParameter).ConfigureAwait(false);
        }

        /// <summary>
        /// GetWebClient
        /// </summary>
        /// <param name="cancellationToken">CancellationToken</param>
        /// <returns>WebClientEx</returns>
        private WebClientEx GetWebClient(System.Threading.CancellationToken cancellationToken)
        {
            var wc = new WebClientEx
            {
                CancellationToken = cancellationToken,
                ProtocolVersion = ProtocolVersion,
                TimeOut = Timeout,
                ContentType = RequestContentType,
                KeepAlive = false,
                Accept = "application/xml",
                Encoding = Encoding
            };
            IgnoreBadCertificates();
            SetTlsConfiguration();
            return wc;
        }

        /// <summary>
        /// Ignore Bad Certificates
        /// </summary>
        private static void IgnoreBadCertificates()
        {
            ServicePointManager.ServerCertificateValidationCallback = AcceptAllCertifications;
        }

        /// <summary>
        /// Accept All Certifications event
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="certification">X509Certificate</param>
        /// <param name="chain">X509Chain</param>
        /// <param name="sslPolicyErrors">SslPolicyErrors</param>
        /// <returns>Boolean</returns>
        private static bool AcceptAllCertifications(object sender, X509Certificate certification, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            return true;
        }

        /// <summary>
        /// Set Tls Configuration
        /// </summary>
        private static void SetTlsConfiguration()
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
        }
    }
}
