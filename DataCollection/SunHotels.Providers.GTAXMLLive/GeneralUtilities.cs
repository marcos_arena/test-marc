﻿using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace SunHotels.Providers
{
    public static class GeneralUtilities
    {
        #region Private Method

        /// <summary>
        /// Convert string to UTF8 array.
        /// </summary>
        /// <param name="pXmlString">XML string</param>
        /// <returns>UTF8 array</returns>
        private static byte[] StringToUtf8ByteArray(string pXmlString)
        {
            var encoding = new UTF8Encoding();
            var byteArray = encoding.GetBytes(pXmlString);
            return byteArray;
        }

        /// <summary>
        /// Taking string from right based on the length
        /// </summary>
        /// <param name="str"></param>
        /// <param name="length"></param>
        /// <returns></returns>
        private static string Right(this string str, int length)
        {
            str = str ?? string.Empty;
            return str.Length >= length ? str.Substring(str.Length - length, length) : str;
        }

        #endregion Private Method

        #region Public Method
        
        /// <summary>
        /// Adding Dot and Space in required places.
        /// </summary>
        /// <param name="str">StringBuilder</param>
        public static void AddDot(this StringBuilder str)
        {
            if (!string.IsNullOrEmpty(str.ToString()))
                str.Append(str.ToString().Trim().Right(1) == "." ? " " : ". ");
        }

        /// <summary>
        /// Adding Dot and Space in required places.
        /// </summary>
        /// <param name="str">string</param>
        public static string AddDot(this string str)
        {
            string result = str;
            if (!string.IsNullOrEmpty(result))
                result = str.Trim().Right(1) == "." ? str.Trim() : str.Trim() + ".";
            return result;
        }

        /// <summary>
        /// Replace String
        /// </summary>
        /// <param name="xmldata">XML data</param>
        /// <param name="patterns">matching patterns array</param>
        /// <param name="replacestring">replacement string</param>
        /// <returns>string</returns>
        public static string ReplaceString(this string xmldata, string[] patterns, string replacestring = null)
        {
            return patterns.Aggregate(xmldata, (current, pattern) => Regex.Replace(current, pattern, replacestring ?? string.Empty));
        }

        /// <summary>
        /// Null To Empty
        /// </summary>
        /// <param name="inputstring">input string</param>
        /// <returns>string</returns>
        public static string NullToEmpty(this string inputstring)
        {
            return string.IsNullOrEmpty(inputstring) ? string.Empty : inputstring;
        }

        /// <summary>
        /// Get XML string From Object
        /// </summary>
        /// <param name="response">Object</param>
        /// <returns>XML string</returns>
        public static string GetXmlFromObject(object response)
        {
            var serializer = new XmlSerializer(response.GetType());
            using (var writer = new StringWriter())
            {
                serializer.Serialize(writer, response);
                return writer.ToString();
            }
        }

        /// <summary>
        /// De-Serialize
        /// </summary>
        /// <typeparam name="T">Expected type</typeparam>
        /// <param name="doc">XDocument</param>
        /// <returns>type of object</returns>
        public static T Deserialize<T>(XDocument doc)
        {
            var xmlSerializer = new XmlSerializer(typeof(T));
            if (doc.Root == null) return default(T);
            using (var reader = doc.Root.CreateReader())
                return (T)xmlSerializer.Deserialize(reader);
        }

        /// <summary>
        /// XML to object conversion
        /// </summary>
        /// <typeparam name="T">expected output type</typeparam>
        /// <param name="pXmlizedString">XML string</param>
        /// <returns>Output in expected type</returns>
        public static T DeserializeXml<T>(string pXmlizedString) where T : class
        {
            if (string.IsNullOrEmpty(pXmlizedString))
                return null;
            var serializer = new XmlSerializer(typeof(T));
            using (var memory = new MemoryStream(StringToUtf8ByteArray(pXmlizedString)))
                return (T)serializer.Deserialize(memory);
        }

        /// <summary>
        /// De-serialize XML File
        /// </summary>
        /// <typeparam name="T">Type</typeparam>
        /// <param name="xmlFilePath">xmlFilePath</param>
        /// <returns>Type</returns>
        public static T DeserializeXmlFile<T>(string xmlFilePath)
        {
            T returnObject;
            if (string.IsNullOrEmpty(xmlFilePath)) return default(T);
            using (var xmlStream = new StreamReader(xmlFilePath))
            {
                var serializer = new XmlSerializer(typeof(T));
                returnObject = (T)serializer.Deserialize(xmlStream);
            }
            return returnObject;
        }

        #endregion Public Method
    }
}
