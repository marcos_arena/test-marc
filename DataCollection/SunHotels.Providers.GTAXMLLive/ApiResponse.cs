﻿namespace SunHotels.Providers
{
    public class ApiResponse
    {
        public string Request { get; set; }
        public string Response { get; set; }
    }
}
