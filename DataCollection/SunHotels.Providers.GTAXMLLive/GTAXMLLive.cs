using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Linq;
using System.Linq;
using SunHotels.XML;
using SunHotels.XML.Data;
using SunHotels.Providers.ProviderCommon.DataLoader;
using System.Threading.Tasks;
using System.Threading;
using System.Globalization;
using System.Text;
using System.Text.RegularExpressions;
using SunHotels.Framework.IO.Compression;
using System.Diagnostics;

namespace SunHotels.Providers
{
    public class GTAXMLLive : XMLProvider, IStaticDataLogger
    {
        #region Global Variable Declaration

        // Create dictionaries for fast search
        private readonly Dictionary<string, Place> _allPlaces = new Dictionary<string, Place>();
        private readonly Dictionary<string, Hotel> _allHotels = new Dictionary<string, Hotel>();
        private GTAXMLLiveSettings _gtaXmlLiveSettings = new GTAXMLLiveSettings();
        private readonly Dictionary<string, string> _citycountry = new Dictionary<string, string>();
        private readonly List<t_Country> _countryList = new List<t_Country>();
        private enum DownloadRequestType { Full, DateRange, Yesterday }
        public class FileInformation
        {
            public string ApiResponse { get; set; }
            public string ZipFilePath { get; set; }
            public string UnZippedDirectoryPath { get; set; }
            public List<string> FilesList { get; set; }

        }
        private class RoomsRoomGroups
        {
            public Dictionary<string, Room> Rooms { get; set; }
            public Dictionary<string, RoomGroup> RoomGroups { get; set; }
        }

        #endregion

        #region XMLProvider Implementation Methods

        /// <summary>
        /// getProviderDefinition
        /// </summary>
        /// <param name="root">Root</param>
        protected override void getProviderDefinition(Root root)
        {
            root.ProviderDefinition.Name = configuration.ProviderName;
            root.ProviderDefinition.Currency = configuration.Currency;
        }

        /// <summary>
        /// Update Configuration
        /// </summary>
        /// <param name="configuration">Configuration</param>
        protected override void UpdateConfiguration(Sunhotels.Export.Configuration configuration)
        {
            configuration.WriteHotelsWithoutRooms = true;
            configuration.WriteRoomsWithoutAvailability = true;
        }

        /// <summary>
        /// Entry point for class.
        /// </summary>
        /// <param name="root">Root for Sunhotels XML structure.</param>
        protected override void getData(Root root)
        {
            try
            {
                this.root = root;

                // ------------------------------------------------------------------------------------------------
                // Create log stream
                // ------------------------------------------------------------------------------------------------
                try
                {
                    Directory.CreateDirectory(configuration.LogOutput);
                }
                catch (Exception ex)
                {
                    throw new ApplicationException("Unable to create GTAXMLLive log file", ex);
                }
                Log(ILoggerStaticDataSeverity.Info, "Starting process");
                // Get all static data 
                GetStaticData();
                Log(ILoggerStaticDataSeverity.Info, "Processing all hotels");
                foreach (var item in _allHotels)
                {
                    _allPlaces[item.Value.PlaceId].Hotels.Add(item.Value.Id, item.Value);
                }
                Log(ILoggerStaticDataSeverity.Info, "Process finished");
            }
            catch (Exception ex)
            {
                Log(ILoggerStaticDataSeverity.Info, "Unhanded exception [{0}] ", ex.ToString());
                throw;
            }

        }

        /// <summary>
        /// Get Global Feature mapping for the provider
        /// </summary>
        /// <param name="root">Root</param>
        protected override void getFeatureTypes(Root root)
        {
            try
            {
                // Get SH features mapped with the provider
                DataMapper?.SunHotelsFeatures?.Join(DataMapper["Feature"], o => o.Key, i => i.Key, (o, i) => o)
                    .All(f =>
                    {
                        root.Features.Add(new Feature
                        {
                            Id = f.Key,
                            Value = f.Value,
                            Name = f.Value
                        });
                        return true;
                    });
            }
            catch (Exception ex)
            {
                Log(ILoggerStaticDataSeverity.Error, $"Error while getting feature types:{ex.Message}");
            }
        }

        /// <summary>
        /// Log function
        /// </summary>
        /// <param name="severity">ILoggerStaticDataSeverity</param>
        /// <param name="message">message</param>
        /// <param name="args">arguments</param>
        public void Log(ILoggerStaticDataSeverity severity, string message, params object[] args)
        {
            var s = XML.HelpClasses.Log.MessageType.Informational;
            if (severity == ILoggerStaticDataSeverity.Error)
                s = XML.HelpClasses.Log.MessageType.Error;
            if (severity == ILoggerStaticDataSeverity.Warning)
                s = XML.HelpClasses.Log.MessageType.Warning;
            LogEntry(s, message, args);
        }

        #endregion

        #region Boolean

        /// <summary>
        /// Check Failed Download
        /// </summary>
        /// <param name="files">Dictionary of Language, FileInformation</param>
        /// <returns>boolean</returns>
        private bool CheckFailedDownload(Dictionary<Language, FileInformation> files)
        {
            try
            {
                if (files == null || files.All(c => string.IsNullOrEmpty(c.Value.ApiResponse))) return true;
                var filelist = files.Where(c => !string.IsNullOrEmpty(c.Value.ApiResponse)).ToArray();
                var total = filelist.Length;
                Log(ILoggerStaticDataSeverity.Info, $"No of failed request found: {total}");
                var pageSize = _gtaXmlLiveSettings.MaxRetryDownloadRequest; // set your page size, which is number of records per page
                var page = 1; // set current page number, must be >= 1
                bool validloop = true;
                while (validloop)
                {
                    var skip = pageSize * (page - 1);
                    var isvalid = skip < total;
                    if (isvalid)
                    {
                        var tasks = new List<Task>();
                        filelist
                            .Select(p => p)
                            .Skip(skip)
                            .Take(pageSize)
                            .Select(file =>
                            {
                                tasks.Add(Task.Factory.StartNew(() =>
                                DownloadRetryTask(file)
                                ));
                                return true;
                            }).ToArray();
                        Task.WaitAll(tasks.ToArray());
                        page++;
                    }
                    else
                        validloop = false;
                }
                return !filelist.Any(c => c.Key.ShCode == _gtaXmlLiveSettings.Language
                       && !string.IsNullOrEmpty(c.Value.ApiResponse));
            }
            catch (Exception ex)
            {
                Log(ILoggerStaticDataSeverity.Error, $"Error while Checking failed download. Error : {ex.Message}");
                return false;
            }
        }

        /// <summary>
        /// Download Request
        /// </summary>
        /// <param name="language">Language</param>
        /// <param name="fileInfoList">Dictionary of Language, FileInformation</param>
        /// <returns>boolean</returns>
        private bool DownloadRequest(Language language, Dictionary<Language, FileInformation> fileInfoList)
        {
            try
            {
                var filePath = configuration.LocalTemp + DateTime.UtcNow.ToString("yyyy-MM-dd-HH-mm-ss-FFF") + "-" +
                         language.GtaCode + ".zip";
                var response = DownloadProcess(language, filePath);
                lock (fileInfoList)
                    fileInfoList.Add(language, new FileInformation { ZipFilePath = filePath, ApiResponse = response });
                return true;
            }
            catch (Exception ex)
            {
                Log(ILoggerStaticDataSeverity.Error, $"Error in Download Request. Language : {language}. Error : {ex.Message}.");
                return false;
            }
        }

        #endregion 

        #region String

        /// <summary>
        /// Download Retry
        /// </summary>
        /// <param name="language">Language</param>
        /// <param name="filePath">filePath</param>
        /// <returns>string</returns>
        private string DownloadRetry(Language language, string filePath)
        {
            string responseMsg = string.Empty;
            try
            {

                int attemptCount = 1;
                while (attemptCount <= _gtaXmlLiveSettings.MaxFailedRequestRetryAttempts)
                {
                    FileDelete(filePath);
                    Log(ILoggerStaticDataSeverity.Info, $"Retry Download started. Attempt no :{attemptCount}. File path: {filePath}. Language : {language}.");
                    responseMsg = DownloadProcess(language, filePath);
                    if (string.IsNullOrEmpty(responseMsg))
                        break;
                    attemptCount++;
                }
                return responseMsg;
            }
            catch (Exception ex)
            {
                responseMsg = $"Error while retrying download. File path: {filePath}. Language : {language}. Error : {ex.Message}.";
                Log(ILoggerStaticDataSeverity.Error, responseMsg);
                return responseMsg;
            }
        }

        /// <summary>
        /// Download Process
        /// </summary>
        /// <param name="language">language</param>
        /// <param name="filePath">filePath</param>
        /// <returns>string</returns>
        private string DownloadProcess(Language language, string filePath)
        {
            string responseMsg;
            try
            {
                var downloadRequest = (DownloadRequestType)Enum.Parse(typeof(DownloadRequestType), _gtaXmlLiveSettings.DownloadType);
                DateTime? fromDate = null;
                DateTime? toDate = null;
                if (downloadRequest == DownloadRequestType.DateRange)
                {
                    //London timezone is used GTA server. UTC and London time difference 60 Minutes. TimeDifferenceMinutes=60 in configuration.
                    fromDate = DateTime.UtcNow.AddMinutes(_gtaXmlLiveSettings.TimeDifferenceMinutes)
                        .AddDays(_gtaXmlLiveSettings.FromdateDifferenceInDays);
                    toDate = DateTime.UtcNow.AddMinutes(_gtaXmlLiveSettings.TimeDifferenceMinutes)
                        .AddDays(_gtaXmlLiveSettings.TodateDifferenceInDays);
                }
                responseMsg = DownloadFile(downloadRequest, language.GtaCode, filePath, fromDate, toDate);
                return responseMsg;
            }
            catch (Exception ex)
            {
                responseMsg = $"Error while download processing. File path: {filePath}. Language : {language}. Error : {ex.Message}.";
                Log(ILoggerStaticDataSeverity.Error, responseMsg);
                return responseMsg;
            }
        }

        /// <summary>
        /// Download File
        /// </summary>
        /// <param name="downloadRequestType">DownloadRequestType</param>
        /// <param name="language">language</param>
        /// <param name="downloadFilePath">downloadFilePath</param>
        /// <param name="fromDate">fromDate</param>
        /// <param name="toTime">toTime</param>
        /// <returns>string</returns>
        private string DownloadFile(DownloadRequestType downloadRequestType, string language,
            string downloadFilePath, DateTime? fromDate, DateTime? toTime)
        {
            try
            {
                var downloadRequest = PrepareRequest(downloadRequestType, fromDate, toTime, language);
                return GetApiResponse(downloadRequest, new CancellationToken(), downloadRequestType, downloadFilePath).Result;
            }
            catch (Exception ex)
            {
                var errorMsg = $"Error in Download File. Download Request Type {downloadRequestType}. " +
                                  $"Language {language}. Download FilePath {downloadFilePath}. " +
                                  $"FromDate(yyyy-MM-dd) {fromDate?.ToString("yyyy-MM-dd")}. " +
                                  $"To Date(yyyy-MM-dd) {toTime?.ToString("yyyy-MM-dd")}. Error : {ex.Message}";
                Log(ILoggerStaticDataSeverity.Error, errorMsg);
                return errorMsg;
            }
        }

        #endregion String

        #region Void

        /// <summary>
        /// Get all static data from API and bind with Dictionary objects.
        /// </summary>              
        private void GetStaticData()
        {
            try
            {
                _gtaXmlLiveSettings = GetGtaxmlLiveSettings();
                Log(ILoggerStaticDataSeverity.Info, "Getting Static information started");
                Directory.CreateDirectory(configuration.LocalTemp);
                LoadCountry(_gtaXmlLiveSettings.Language);
                var files = Download();
                var isValid = CheckFailedDownload(files);
                if (isValid)
                {
                    UnZipFiles(files);
                    ProcessFiles(files);
                    if (_gtaXmlLiveSettings.DeleteFileAfterProcessing)
                        DeleteDirectoryAfterProcessing(files);
                }
                else
                {
                    Log(ILoggerStaticDataSeverity.Info, "Required files are not available to create the hotel and places.");
                }
                Log(ILoggerStaticDataSeverity.Info, $"Total no of hotels created.{_allHotels.Count}.");
                Log(ILoggerStaticDataSeverity.Info, "Getting Static information finished");
            }
            catch (Exception ex)
            {
                Log(ILoggerStaticDataSeverity.Error, $"Error while getting static data. Error : {ex.Message}");
            }
        }

        /// <summary>
        /// Delete Directory After Processing
        /// </summary>
        /// <param name="files">Dictionary of Language, FileInformation</param>
        private void DeleteDirectoryAfterProcessing(Dictionary<Language, FileInformation> files)
        {
            try
            {
                var sw = Stopwatch.StartNew();
                Log(ILoggerStaticDataSeverity.Info, "Delete directory after processing started");
                if (files != null && files.Any(c => !string.IsNullOrEmpty(c.Value?.UnZippedDirectoryPath)))
                {
                    files.Where(c => !string.IsNullOrEmpty(c.Value?.UnZippedDirectoryPath))
                        .Select(file =>
                        {
                            DeleteDirectory(file.Value.UnZippedDirectoryPath);
                            return true;
                        }).ToArray();
                }
                var ems = sw.ElapsedMilliseconds;
                Log(ILoggerStaticDataSeverity.Info, $"Delete directory after processing completed ElapsedMilliseconds:{ems}");
            }
            catch (Exception ex)
            {
                Log(ILoggerStaticDataSeverity.Error, $"Error while delete directory after processing. Error : {ex.Message}");
            }
        }

        /// <summary>
        /// Delete Directory
        /// </summary>
        /// <param name="path">Directory path</param>
        private void DeleteDirectory(string path)
        {
            try
            {
                if (Directory.Exists(path))
                    Directory.Delete(path, true);
            }
            catch (Exception ex)
            {
                Log(ILoggerStaticDataSeverity.Error, $"Error while deleting directory. File path :{path}. Error : {ex.Message}");
            }
        }

        /// <summary>
        /// Download Retry Task
        /// </summary>
        /// <param name="file">KeyValuePair of Language, FileInformation</param>
        /// <returns>boolean</returns>
        private void DownloadRetryTask(KeyValuePair<Language, FileInformation> file)
        {
            var res = DownloadRetry(file.Key, file.Value.ZipFilePath);
            file.Value.ApiResponse = res;
        }

        /// <summary>
        /// File Delete
        /// </summary>
        /// <param name="filePath">filePath</param>
        private void FileDelete(string filePath)
        {
            try
            {
                if (File.Exists(filePath))
                {
                    File.Delete(filePath);
                    Log(ILoggerStaticDataSeverity.Info, $"Successfully file Deleted. File path: {filePath}.");
                }
            }
            catch (Exception ex)
            {
                Log(ILoggerStaticDataSeverity.Error, $"Error in File Delete. File path: {filePath}. Error : {ex.Message}.");
            }
        }

        /// <summary>
        /// UnZip Files
        /// </summary>
        /// <param name="files">Dictionary of Language, FileInformation</param>
        private void UnZipFiles(Dictionary<Language, FileInformation> files)
        {
            try
            {
                var sw = Stopwatch.StartNew();
                Log(ILoggerStaticDataSeverity.Info, "UnZip Files Started");
                var total = files.Count;
                var pageSize = _gtaXmlLiveSettings.MaxUnzip; // set your page size, which is number of records per page
                var page = 1; // set current page number, must be >= 1
                bool validloop = true;
                while (validloop)
                {
                    var skip = pageSize * (page - 1);
                    var isvalid = skip < total;
                    if (isvalid)
                    {
                        var tasks = new List<Task>();
                        files
                            .Select(p => p)
                            .Skip(skip)
                            .Take(pageSize)
                            .Select(f =>
                            {
                                tasks.Add(Task.Factory.StartNew(() =>
                                {
                                    string unzippath;
                                    var fileList = UnZip(f.Value.ZipFilePath, out unzippath);
                                    f.Value.FilesList = fileList;
                                    f.Value.UnZippedDirectoryPath = unzippath;
                                    return true;
                                }));
                                return true;
                            }).ToArray();
                        Task.WaitAll(tasks.ToArray());
                        page++;
                    }
                    else
                        validloop = false;
                }
                var ems = sw.ElapsedMilliseconds;
                Log(ILoggerStaticDataSeverity.Info, $"Unzip Files Completed ElapsedMilliseconds:{ems}.");
            }
            catch (Exception ex)
            {
                Log(ILoggerStaticDataSeverity.Error, $"Error in Unzip Files. Error : {ex.Message}");

            }
        }

        /// <summary>
        /// Process Hotel File
        /// </summary>
        /// <param name="language">Language</param>
        /// <param name="filePath">filePath</param>
        /// <param name="features">features</param>
        private void ProcessHotelFile(Language language, string filePath,
            IReadOnlyDictionary<string, KeyValuePair<string, string>> features)
        {
            try
            {
                var itemDetail = GetItemDetail(filePath);
                var hotelinfo = itemDetail?.Item1 as t_HotelInformation;
                if (hotelinfo == null) return;
                string countryCode = string.Empty;
                string countryName = string.Empty;
                if (_citycountry.ContainsKey(itemDetail.City.Code))
                {
                    _citycountry.TryGetValue(itemDetail.City.Code, out countryCode);
                    lock (_countryList)
                    {
                        if (_countryList.Any(c => c.Code == countryCode))
                        {
                            var countryInfo = _countryList.First(c => c.Code == countryCode);
                            countryName = countryInfo.Value;
                        }
                    }
                }
                var place = GetPlace(countryName, countryCode, itemDetail.City.Value, itemDetail.City.Code);
                //Creating the Images based on the CreateImages property
                List<Hotel.Image> images = null;
                if (_gtaXmlLiveSettings.CreateImages)
                    if (hotelinfo.Links?.ImageLinks != null)
                        images = PrepareHotelImages(itemDetail.Item.Code, hotelinfo.Links.ImageLinks);
                //Creating the Rooms and RoomGroups based on the CreateRooms property
                var roomsRoomGroups = new RoomsRoomGroups { Rooms = new Dictionary<string, Room>(), RoomGroups = new Dictionary<string, RoomGroup>() };
                if (_gtaXmlLiveSettings.CreateRooms)
                    roomsRoomGroups = PrepareRoomsRoomGroups(hotelinfo);
                var hotel = CreateHotel(countryName, itemDetail, place, hotelinfo, images, features, t_ItemType.hotel, roomsRoomGroups);
                if (hotel == null) return;
                if (_allHotels.ContainsKey(hotel.Id)) return;
                lock (_allHotels)
                {
                    _allHotels.Add(hotel.Id, hotel);
                }
                GetTranslations(hotel.Id, countryCode, language.ShCode, hotelinfo);
            }
            catch (Exception ex)
            {
                Log(ILoggerStaticDataSeverity.Error, $"Error in Process Hotel File. File path {filePath}. Error : {ex.Message}");
            }
        }

        /// <summary>
        /// Prepare Hotel Translations
        /// </summary>
        /// <param name="language">Language</param>
        /// <param name="filePath">filePath</param>
        public void PrepareHotelTranslations(Language language, string filePath)
        {
            try
            {
                var itemDetail = GetItemDetail(filePath);
                var hotelinfo = itemDetail?.Item1 as t_HotelInformation;
                if (hotelinfo == null) return;
                string countryCode = string.Empty;
                if (_citycountry.ContainsKey(itemDetail.City.Code))
                    _citycountry.TryGetValue(itemDetail.City.Code, out countryCode);
                GetTranslations(itemDetail.Item.Code + "|" + itemDetail.City.Code, countryCode, language.ShCode, hotelinfo);
            }
            catch (Exception ex)
            {
                Log(ILoggerStaticDataSeverity.Error, $"Error in Prepare Hotel Translations. Language {language.ShCode}. File path {filePath}. Error : {ex.Message}");
            }
        }

        /// <summary>
        /// Process Hotel List
        /// </summary>
        /// <param name="language">Language</param>
        /// <param name="fileInformation">FileInformation</param>
        /// <param name="features">IReadOnlyDictionary of string, KeyValuePair of string, string</param>
        private void ProcessHotelList(Language language, FileInformation fileInformation,
            IReadOnlyDictionary<string, KeyValuePair<string, string>> features)
        {
            try
            {
                var sw = Stopwatch.StartNew();
                if (fileInformation?.FilesList == null || !fileInformation.FilesList.Any())
                    return;
                var filesList = fileInformation.FilesList;
                var total = filesList.Count;
                var pageSize = _gtaXmlLiveSettings.MaxHotelsProcessing;
                // set your page size, which is number of records per page
                var page = 1; // set current page number, must be >= 1
                bool validloop = true;
                while (validloop)
                {
                    var skip = pageSize * (page - 1);
                    var isvalid = skip < total;
                    if (isvalid)
                    {
                        var tasks = new List<Task>();
                        filesList
                            .Select(p => p)
                            .Skip(skip)
                            .Take(pageSize)
                            .Select(filepath =>
                            {
                                tasks.Add(Task.Factory.StartNew(() =>
                                {
                                    ProcessHotelFile(language, filepath, features);
                                    return true;
                                }));
                                return true;
                            }).ToArray();
                        Task.WaitAll(tasks.ToArray());
                        page++;
                    }
                    else
                        validloop = false;
                }
                var ems = sw.ElapsedMilliseconds;
                Log(ILoggerStaticDataSeverity.Info, $"ProcessHotelList ElapsedMilliseconds:{ems}");
            }
            catch (Exception ex)
            {
                Log(ILoggerStaticDataSeverity.Error, $"Error in Process Hotel List. Language {language.ShCode}. Error : {ex.Message}");
            }
        }

        /// <summary>
        /// ProcessTransulation
        /// </summary>
        /// <param name="language">Language</param>
        /// <param name="fileInformation">FileInformation</param>
        private void ProcessTranslations(Language language, FileInformation fileInformation)
        {
            try
            {
                var sw = Stopwatch.StartNew();
                if (fileInformation?.FilesList == null || !fileInformation.FilesList.Any())
                    return;
                var filesList = fileInformation.FilesList;
                var total = filesList.Count;
                var pageSize = _gtaXmlLiveSettings.MaxHotelsTranslationProcessing; // set your page size, which is number of records per page
                var page = 1; // set current page number, must be >= 1
                bool validloop = true;
                while (validloop)
                {
                    var skip = pageSize * (page - 1);
                    var isvalid = skip < total;
                    if (isvalid)
                    {
                        var tasks = new List<Task>();
                        filesList
                            .Select(p => p)
                            .Skip(skip)
                            .Take(pageSize)
                            .Select(filepath =>
                            {
                                tasks.Add(Task.Factory.StartNew(() =>
                                {
                                    PrepareHotelTranslations(language, filepath);
                                    return true;
                                }));
                                return true;
                            }).ToArray();
                        Task.WaitAll(tasks.ToArray());
                        page++;
                    }
                    else
                        validloop = false;
                }
                var ems = sw.ElapsedMilliseconds;
                Log(ILoggerStaticDataSeverity.Info, $"ProcessTransulation ElapsedMilliseconds:{ems}. Language {language.ShCode}");
            }
            catch (Exception ex)
            {
                Log(ILoggerStaticDataSeverity.Error, $"Error in Process Translation. Language {language.ShCode}. Error : {ex.Message}");
            }
        }

        /// <summary>
        /// Load City
        /// </summary>
        /// <param name="countries">countries</param>
        /// <param name="language">language</param>
        private void LoadCity(IEnumerable<string> countries, string language)
        {
            var countryList = countries as string[] ?? countries.ToArray();
            if (countryList.Length <= 0 || string.IsNullOrEmpty(language))
            {
                Log(ILoggerStaticDataSeverity.Error, $"Empty country list in LoadCity. Language {language}");
                return;
            }

            try
            {
                var sw = Stopwatch.StartNew();
                Log(ILoggerStaticDataSeverity.Info, "LoadCity Started");
                var cityrequest = PrepareRequest(ItemsChoiceType.SearchCityRequest, language, countryList.ToList());
                var cityApiResponse = GetApiResponse(cityrequest, _gtaXmlLiveSettings.RequestTimeout, new CancellationToken(), ItemsChoiceType.SearchCityRequest).Result;
                if (string.IsNullOrEmpty(cityApiResponse?.Response)) return;
                var cityresponse = GeneralUtilities.DeserializeXml<t_Response>(cityApiResponse.Response);
                var cityresponseDetails = cityresponse.Item as t_ResponseDetails;
                if (cityresponseDetails?.Items == null || !cityresponseDetails.Items.Any()) return;
                var scityres = cityresponseDetails.Items.Select(c => c as t_SearchCityResponse).ToArray();
                scityres.Select(c =>
                {
                    var cityDetails = c.Item as t_CityDetails;
                    cityDetails?.City?.Select(d =>
                    {
                        lock (_citycountry)
                            if (_citycountry.All(f => f.Key != d.Code))
                                _citycountry.Add(d.Code, c.CountryCode);
                        return true;
                    }).ToArray();
                    return true;
                }).ToArray();
                var ems = sw.ElapsedMilliseconds;
                Log(ILoggerStaticDataSeverity.Info, $"LoadCity Completed ElapsedMilliseconds:{ems}");
            }
            catch (Exception ex)
            {
                Log(ILoggerStaticDataSeverity.Error, $"Error in Load City. Country Count {countryList.Length}. Error : {ex.Message}");
            }
        }

        /// <summary>
        /// Load Country
        /// </summary>
        /// <param name="language">language</param>
        private void LoadCountry(string language)
        {
            try
            {
                var sw = Stopwatch.StartNew();
                Log(ILoggerStaticDataSeverity.Info, "LoadCountry Started");
                var countryrequest = PrepareRequest(ItemsChoiceType.SearchCountryRequest, string.Empty);
                var apiResponse = GetApiResponse(countryrequest, _gtaXmlLiveSettings.RequestTimeout, new CancellationToken(), ItemsChoiceType.SearchCountryRequest).Result;
                if (string.IsNullOrEmpty(apiResponse?.Response)) return;
                var response = GeneralUtilities.DeserializeXml<t_Response>(apiResponse.Response);
                var responseDetails = response.Item as t_ResponseDetails;
                if (responseDetails?.Items == null || !responseDetails.Items.Any()) return;
                var scr = responseDetails.Items.First() as t_SearchCountryResponse;
                var countrylist = scr?.Item as t_CountryDetails;
                if (countrylist?.Country == null) return;
                var countries = countrylist.Country.Select(d => d.Code).ToList();
                lock (_countryList)
                    _countryList.AddRange(countrylist.Country);
                var total = countries.Count;
                var pageSize = _gtaXmlLiveSettings.MaxCityRequestSize; // set your page size, which is number of records per page
                var page = 1; // set current page number, must be >= 1
                bool validloop = true;
                while (validloop)
                {
                    var skip = pageSize * (page - 1);
                    var isvalid = skip < total;
                    if (isvalid)
                    {
                        var tasks = new List<Task>();
                        var selectedlist = countries
                             .Select(p => p)
                             .Skip(skip)
                             .Take(pageSize)
                             .ToList();
                        tasks.Add(Task.Factory.StartNew(() =>
                        {
                            LoadCity(selectedlist, language);
                        }));
                        Task.WaitAll(tasks.ToArray());
                        page++;
                    }
                    else
                        validloop = false;
                }
                var ems = sw.ElapsedMilliseconds;
                Log(ILoggerStaticDataSeverity.Info, $"LoadCountry ElapsedMilliseconds:{ems}");
            }
            catch (Exception ex)
            {
                Log(ILoggerStaticDataSeverity.Error, $"Error in Load Country. Language {language}. Error : {ex.Message}");
            }
        }

        /// <summary>
        /// Process Files
        /// </summary>
        /// <param name="files">Dictionary of Language, FileInformation</param>
        private void ProcessFiles(Dictionary<Language, FileInformation> files)
        {
            try
            {
                var sw = Stopwatch.StartNew();
                Log(ILoggerStaticDataSeverity.Info, "ProcessFiles Started");
                var features = DataMapper?.SunHotelsFeatures?.Join(DataMapper["Feature"], o => o.Key, i => i.Key,
                            (o, i) => new { SHFeature = o, ProviderFeature = i })
                        .SelectMany(o => o.ProviderFeature.Value.Select(of => new { ProviderId = of, o.SHFeature }))
                        .ToDictionary(o => o.ProviderId, o => o.SHFeature);
                var englishfilelist = files.First(c => c.Key.ShCode == _gtaXmlLiveSettings.Language);
                ProcessHotelList(englishfilelist.Key, englishfilelist.Value, features);
                var filelist = files.Where(c => c.Key.ShCode != _gtaXmlLiveSettings.Language);
                var keyValuePairs = filelist as KeyValuePair<Language, FileInformation>[] ?? filelist.ToArray();
                var total = keyValuePairs.Length;
                var pageSize = _gtaXmlLiveSettings.MaxTranslationProcessing; // set your page size, which is number of records per page
                var page = 1; // set current page number, must be >= 1
                bool validloop = true;
                while (validloop)
                {
                    var skip = pageSize * (page - 1);
                    var isvalid = skip < total;
                    if (isvalid)
                    {
                        var tasks = new List<Task>();
                        keyValuePairs
                            .Select(p => p)
                            .Skip(skip)
                            .Take(pageSize)
                            .Select(f =>
                            {
                                tasks.Add(Task.Factory.StartNew(() =>
                                {
                                    ProcessTranslations(f.Key, f.Value);
                                    return true;
                                }));
                                return true;
                            }).ToArray();
                        Task.WaitAll(tasks.ToArray());
                        page++;
                    }
                    else
                        validloop = false;
                }
                var ems = sw.ElapsedMilliseconds;
                Log(ILoggerStaticDataSeverity.Info, $"ProcessFiles Completed ElapsedMilliseconds:{ems}");
            }
            catch (Exception ex)
            {
                Log(ILoggerStaticDataSeverity.Error, $"Error in Process Files. File count {files.Count}. Error : {ex.Message}");
            }
        }

        /// <summary>
        /// Get Translations
        /// </summary>
        /// <param name="hotelcode">hotel code</param>
        /// <param name="countrycode">country code</param>
        /// <param name="language">language</param>
        /// <param name="hotelInformation">hotel Information</param>
        private void GetTranslations(string hotelcode, string countrycode, string language, t_HotelInformation hotelInformation)
        {
            try
            {
                if (!_allHotels.ContainsKey(hotelcode)) return;
                var hotelHeadline = string.Empty;
                var hotelDescription = new StringBuilder(string.Empty);
                var hotelinfo = hotelInformation;
                if (!string.IsNullOrEmpty(hotelinfo?.Category))
                    hotelHeadline = hotelinfo.Category + ". ";
                if (hotelinfo?.Reports != null && hotelinfo.Reports.Any())
                    hotelDescription.Append(string.Join(" ", hotelinfo.Reports.Select(f => f.Value.AddDot())));
                if (hotelinfo?.AreaDetails?.Items != null && hotelinfo.AreaDetails.Items.Any(c => c is string))
                {
                    hotelDescription.AddDot();
                    hotelDescription.Append(string.Join(". ", hotelinfo.AreaDetails.Items.Select(f => f)));
                }
                if (hotelinfo?.Facilities != null && hotelinfo.Facilities.Any(c => !string.IsNullOrEmpty(c.Value)))
                {
                    hotelDescription.AddDot();
                    hotelDescription.Append(string.Join(". ", hotelinfo.Facilities.Select(f => f.Value)));
                    hotelDescription.AddDot();
                }

                lock (_allHotels)
                {
                    Hotel hotel;
                    _allHotels.TryGetValue(hotelcode, out hotel);
                    if (hotel == null) return;
                    string hotelDescriptionstr = Regex.Replace(hotelDescription.ToString().Trim(), @"\t|\n|\r", "");
                    hotelDescriptionstr = Regex.Replace(hotelDescriptionstr.Trim(), "  ", " ");
                    if (language == _gtaXmlLiveSettings.Language)
                        hotel.Description = hotelDescriptionstr;

                    hotel.Translations.description.Add(new TranslationsDescription
                    {
                        country = countrycode,
                        lang = language,
                        Value = hotelDescriptionstr
                    });
                    if (!string.IsNullOrEmpty(hotelHeadline))
                        hotel.Translations.headline.Add(new TranslationsHeadline
                        {
                            country = countrycode,
                            lang = language,
                            Value = hotelHeadline
                        });
                }
            }
            catch (Exception ex)
            {
                Log(ILoggerStaticDataSeverity.Error, "Error while preparing hotel translations. " +
                $"Hotel:{ hotelcode}. Country code {countrycode}. Language {language}. Error : {ex.Message}");
            }
        }

        #endregion

        #region Get Type

        /// <summary>
        /// Get API Response
        /// </summary>
        /// <param name="request">request object</param>
        /// <param name="cancellationToken">CancellationToken</param>
        /// <param name="requesType"></param>
        /// <param name="downloadFilePath"></param>
        /// <returns>Task of ApiResponse</returns>
        private async Task<string> GetApiResponse(object request,
            CancellationToken cancellationToken, DownloadRequestType requesType, string downloadFilePath)
        {
            var sw = Stopwatch.StartNew();
            string apiResponse = string.Empty;
            Log(ILoggerStaticDataSeverity.Info, $"GetApiResponse {requesType} started.");

            try
            {
                var api = new ConnectHttp
                {
                    BaseUrl = configuration.WebServiceUrl,
                    RequestContentType = _gtaXmlLiveSettings.ContentType,
                    DownloadContentType = _gtaXmlLiveSettings.ResponseDownloadContentType,
                    ErrorContentType = _gtaXmlLiveSettings.ResponseErrorContentType,
                    KeepAlive = _gtaXmlLiveSettings.KeepAlive,
                    Method = _gtaXmlLiveSettings.MethodType,
                    TimeOut = _gtaXmlLiveSettings.RequestTimeout,
                    PostParameter = GeneralUtilities.GetXmlFromObject(request).ReplaceString(new[] { "utf-16" }, "utf-8"),
                    ProtocolVersion = _gtaXmlLiveSettings.ProtocolVersion,
                    DownloadFilePath = downloadFilePath,
                    RequestCancellationToken = cancellationToken
                };
                apiResponse = await api.DownloadFile();
                return apiResponse;
            }
            catch (Exception ex)
            {
                string errorMsg = $"Error while getting API response. Request type : {requesType}. Error : {ex.Message}";
                Log(ILoggerStaticDataSeverity.Error, errorMsg);
                return errorMsg;
            }
            finally
            {
                var ems = sw.ElapsedMilliseconds;
                Log(ILoggerStaticDataSeverity.Info, $"GetApiResponse {requesType} completed ElapsedMilliseconds:{ems }. File Path: { downloadFilePath}. API response: {apiResponse}.");
            }
        }

        /// <summary>
        /// Get API Response
        /// </summary>
        /// <param name="request">request object</param>
        /// <param name="timeout">timeout</param>
        /// <param name="cancellationToken">CancellationToken</param>
        /// <param name="requesType"></param>
        /// <returns>Task of ApiResponse</returns>
        private async Task<ApiResponse> GetApiResponse(object request, int? timeout,
            CancellationToken cancellationToken, ItemsChoiceType requesType)
        {
            var sw = Stopwatch.StartNew();
            try
            {
                var apiResponse = new ApiResponse();
                var api = new RestApiUtils
                {
                    ProtocolVersion = _gtaXmlLiveSettings.ProtocolVersion,
                    RequestContentType = _gtaXmlLiveSettings.ContentType,
                    Timeout = timeout ?? _gtaXmlLiveSettings.RequestTimeout,
                    PostParameter = GeneralUtilities.GetXmlFromObject(request).ReplaceString(new[] { "utf-16" }, "utf-8"),
                    RequestUrl = configuration.WebServiceUrl,
                    Encoding = Encoding.UTF8
                };
                apiResponse.Request = api.RequestUrl + api.PostParameter;
                apiResponse.Response = await api.Post(cancellationToken).ConfigureAwait(false);
                return apiResponse;
            }
            catch (Exception ex)
            {
                Log(ILoggerStaticDataSeverity.Error, $"Error while getting API response. Request type : {requesType}. Error : {ex.Message}");
                return null;
            }
            finally
            {
                var ems = sw.ElapsedMilliseconds;
                Log(ILoggerStaticDataSeverity.Info, $"GetApiResponse {requesType} completed ElapsedMilliseconds:{ems}");
            }
        }

        /// <summary>
        /// Construct Request Source
        /// </summary>
        /// <param name="language">language</param>
        /// <returns>t_Source</returns>
        private t_Source ConstructRequestSource(string language)
        {
            try
            {
                return new t_Source
                {
                    RequestorID = new t_RequestorID
                    {
                        Client = _gtaXmlLiveSettings.Client,
                        EMailAddress = configuration.UserId,
                        Password = configuration.Password
                    },
                    RequestorPreferences = new t_RequestorPreferences
                    {
                        Country = _gtaXmlLiveSettings.Country,
                        Currency = configuration.Currency,
                        Language = string.IsNullOrEmpty(language) ? _gtaXmlLiveSettings.Language : language,
                        RequestMode = t_RequestMode.SYNCHRONOUS
                    }
                };
            }
            catch (Exception ex)
            {
                Log(ILoggerStaticDataSeverity.Error, $"Error while construct request source. Language : {language}. Error : {ex.Message}");
                return null;
            }
        }

        /// <summary>
        /// Prepare Request
        /// </summary>
        /// <param name="downloadType">DownloadRequestType</param>
        /// <param name="fromDate">DateTime</param>
        /// <param name="toDate">DateTime</param>
        /// <param name="language">language</param>
        /// <returns>t_Request</returns>
        private t_Request PrepareRequest(DownloadRequestType downloadType, DateTime? fromDate,
            DateTime? toDate, string language)
        {
            try
            {
                var requestDetails = new t_RequestDetails { ItemsElementName = new[] { ItemsChoiceType.ItemInformationDownloadRequest }.ToArray() };
                var downloadRequest = new t_ItemInformationDownloadRequest { ItemType = t_ItemType.hotel };
                switch (downloadType)
                {
                    case DownloadRequestType.Full:
                        break;
                    case DownloadRequestType.DateRange:
                        if (fromDate != null && toDate != null)
                            downloadRequest.IncrementalDownloads = new t_DateRange
                            {
                                FromDateSpecified = true,
                                FromDate = fromDate.Value,
                                ToDateSpecified = true,
                                ToDate = toDate.Value
                            };
                        break;
                    default:
                        downloadRequest.IncrementalDownloads = new t_DateRange();
                        break;
                }
                requestDetails.Items = new object[] { downloadRequest };
                return new t_Request
                {
                    Source = ConstructRequestSource(language),
                    RequestDetails = requestDetails
                };
            }
            catch (Exception ex)
            {
                var errorMsg = $"Error while Preparing Request. Download Request Type {downloadType}. " +
                                 $"Language {language}. FromDate(yyyy-MM-dd) {fromDate?.ToString("yyyy-MM-dd")}. " +
                                 $"To Date(yyyy-MM-dd) {toDate?.ToString("yyyy-MM-dd")}. Error : {ex.Message}";
                Log(ILoggerStaticDataSeverity.Error, errorMsg);
                return null;
            }
        }

        /// <summary>
        /// Prepare Request
        /// </summary>
        /// <param name="itemtype">ItemsChoiceType</param>
        /// <param name="language">language</param>
        /// <param name="countrys">countries</param>
        /// <returns>t_Request</returns>
        private t_Request PrepareRequest(ItemsChoiceType itemtype, string language, IReadOnlyCollection<string> countrys = null)
        {
            try
            {
                var requestDetails = new t_RequestDetails { ItemsElementName = new[] { itemtype }.ToArray() };

                switch (itemtype)
                {
                    case ItemsChoiceType.ItemInformationDownloadRequest:
                        requestDetails.Items = new object[] { new t_ItemInformationDownloadRequest() };
                        break;
                    case ItemsChoiceType.SearchCountryRequest:
                        requestDetails.Items = new object[] { new t_SearchCountryRequest() };
                        break;
                    case ItemsChoiceType.SearchCityRequest:
                        var items = new List<t_SearchCityRequest>();
                        if (countrys != null)
                        {
                            countrys.Select(c =>
                            {
                                items.Add(new t_SearchCityRequest { CountryCode = c });
                                return true;
                            }).ToArray();
                            requestDetails.ItemsElementName = Enumerable.Repeat(itemtype, countrys.Count).ToArray();
                            requestDetails.Items = items.ToArray();
                        }
                        break;
                }
                return new t_Request
                {
                    Source = ConstructRequestSource(language),
                    RequestDetails = requestDetails
                };
            }
            catch (Exception ex)
            {
                Log(ILoggerStaticDataSeverity.Error, $"Error while preparing the request. Request type:{itemtype}. Language {language}. Countries Count : {countrys?.Count}. Error : {ex.Message}");
                return null;
            }
        }

        /// <summary>
        /// Get Hotel Features
        /// </summary>
        /// <param name="hotelCode">hotelCode</param>
        /// <param name="hotelInformation">t_HotelInformation</param>
        /// <param name="features">features Dictionary key pair value</param>
        /// <returns></returns>
        private List<Feature> GetHotelFeatures(string hotelCode, t_HotelInformation hotelInformation, IReadOnlyDictionary<string, KeyValuePair<string, string>> features)
        {
            try
            {
                var featureCollection = new List<Feature>();
                if (features == null || hotelInformation?.Facilities == null)
                    return featureCollection;
                hotelInformation.Facilities.AsParallel().Select(c =>
                {
                    if (!features.ContainsKey(c.Code)) return true;
                    var shfeature = features[c.Code];
                    lock (featureCollection)
                    {
                        if (featureCollection.All(f => f.Id != shfeature.Key))
                        {
                            featureCollection.Add(new Feature
                            {
                                Id = shfeature.Key,
                                Name = shfeature.Value,
                                Value = shfeature.Value
                            });
                        }
                    }
                    return true;
                }).ToList();
                return featureCollection;
            }
            catch (Exception ex)
            {
                Log(ILoggerStaticDataSeverity.Error, $"Error while getting hotel Features. Hotel: {hotelCode}. Error : {ex.Message}");
                return null;
            }
        }

        /// <summary>
        /// Get GTAXMLLiveSettings
        /// </summary>
        /// <param name="fileName">fileName</param>
        /// <returns>GTAXMLLiveSettings</returns>
        private GTAXMLLiveSettings GetGtaxmlLiveSettings(string fileName = "GTAXMLLiveSettings")
        {
            try
            {
                return GeneralUtilities.Deserialize<GTAXMLLiveSettings>(XDocument.Load(configuration.ProviderConfigRoot + "\\" + fileName + ".xml"));
            }
            catch (Exception ex)
            {
                Log(ILoggerStaticDataSeverity.Error, $"Error while getting GTAXMLLiveSettings. File name: {fileName}. Error : {ex.Message}");
                return null;
            }
        }

        /// <summary>
        /// Get previously created place or create new.
        /// </summary>
        /// <param name="countryName">The country code.</param>
        /// <param name="countryCode">The country code.</param>
        /// <param name="cityName">The city name.</param>
        /// <param name="cityCode">The city code.</param>
        /// <returns>The requested place.</returns>
        private Place GetPlace(string countryName, string countryCode, string cityName, string cityCode)
        {
            try
            {
                if (string.IsNullOrEmpty(cityCode))
                    return null;
                lock (_allPlaces)
                {
                    // Check or create country level place            
                    if (!_allPlaces.ContainsKey(countryCode.ToUpper()))
                    {
                        var newPlace = new Place { Id = countryCode, Description = countryName };
                        _allPlaces.Add(countryCode, newPlace);
                        lock (root.Places)
                            root.Places.Add(newPlace);
                    }
                    // Check or create state level place if state is specified            
                    if (string.IsNullOrEmpty(cityCode)) return _allPlaces[cityCode];
                    if (_allPlaces.ContainsKey(cityCode.Trim())) return _allPlaces[cityCode];
                    var place = new Place { Id = cityCode, Description = cityName };
                    if (!_allPlaces.ContainsKey(cityCode))
                        _allPlaces.Add(cityCode, place);

                    if (!_allPlaces[countryCode].Places.Contains(place))
                        _allPlaces[countryCode].Places.Add(place);
                }
                return _allPlaces[cityCode];
            }
            catch (Exception ex)
            {
                string errorMsg = $"Error while creating place. CountryName : {countryName}. CountryCode : {countryCode}. " +
                                  $"CityName : {cityName}. CityCode : {cityCode}. Error : {ex.Message}";
                Log(ILoggerStaticDataSeverity.Error, errorMsg);
                return null;
            }
        }

        /// <summary>
        /// Prepare Hotel Images
        /// </summary>
        /// <param name="hotelCode">hotelCode</param>
        /// <param name="imageLinks">t_ImageLink Array</param>
        /// <returns>List of Hotel.Image</returns>
        private List<Hotel.Image> PrepareHotelImages(string hotelCode, t_ImageLink[] imageLinks)
        {
            var hotelImages = new List<Hotel.Image>();
            try
            {
                if (imageLinks != null && imageLinks.Any(c => !string.IsNullOrEmpty(c?.Image)))
                    imageLinks.Where(c => !string.IsNullOrEmpty(c?.Image)).AsParallel().All(c =>
                    {
                        lock (hotelImages)
                        {
                            hotelImages.Add(new Hotel.Image
                            {
                                Id = c.Text,
                                ImageVariants = new List<Hotel.ImageVariant> {
                                    new Hotel.ImageVariant
                                    {
                                        URL = c.Image,
                                        Height =!string.IsNullOrEmpty(c.Height)?int.Parse(c.Height):_gtaXmlLiveSettings.ImageHeight,
                                        Width =!string.IsNullOrEmpty(c.Width)?int.Parse(c.Width):_gtaXmlLiveSettings.ImageWidth
                                    }
                                }
                            });
                        }
                        return true;
                    });
            }
            catch (Exception ex)
            {
                Log(ILoggerStaticDataSeverity.Error, $"Error while prepare hotel images. Hotel code: {hotelCode}. Error : {ex.Message}");
            }
            return hotelImages;
        }

        /// <summary>
        /// Creating Hotel 
        /// </summary>
        /// <param name="country">t_Country</param>
        /// <param name="hotelitem">t_ItemDetail</param>
        /// <param name="place">Place</param>
        /// <param name="hotelinfo">t_HotelInformation</param>
        /// <param name="images">List of Hotel images</param>
        /// <param name="features">features</param>
        /// <param name="itemType">t_ItemType</param>
        /// <param name="roomsRoomGroups">RoomsRoomGroups</param>
        /// <returns>Hotel</returns>
        private Hotel CreateHotel(string country, t_ItemDetail hotelitem, Place place,
            t_HotelInformation hotelinfo, List<Hotel.Image> images,
            IReadOnlyDictionary<string, KeyValuePair<string, string>> features,
            t_ItemType itemType, RoomsRoomGroups roomsRoomGroups)
        {
            if (hotelitem?.Item == null || hotelitem.City == null || string.IsNullOrEmpty(place?.Id)
                || hotelinfo?.AddressLines == null)
                return null;

            var hotelcode = hotelitem.Item.Code + "|" + place.Id;
            try
            {
                return new Hotel
                {
                    Id = hotelcode,
                    PlaceId = place.Id,
                    Name = hotelitem.Item.Value,
                    AccomodationType = GetAccomodation(itemType),
                    Headline = hotelinfo.Category,
                    Description = string.Empty,
                    Adress_Street1 = hotelinfo.AddressLines.AddressLine1.NullToEmpty() + " " + hotelinfo.AddressLines.AddressLine2.NullToEmpty(),
                    Adress_Street2 = hotelinfo.AddressLines.AddressLine3.NullToEmpty() + " " + hotelinfo.AddressLines.AddressLine4.NullToEmpty(),
                    Adress_Zipcode = string.Empty,
                    Adress_City = hotelitem.City.Value,
                    Adress_Country = country,
                    Position_Latitude = hotelinfo.GeoCodes?.Latitude.ToString(CultureInfo.InvariantCulture) ?? string.Empty,
                    Position_Longitude = hotelinfo.GeoCodes?.Longitude.ToString(CultureInfo.InvariantCulture) ?? string.Empty,
                    Phone = hotelinfo.AddressLines.Telephone,
                    Fax = hotelinfo.AddressLines.Fax,
                    Email = hotelinfo.AddressLines.EmailAddress,
                    Classification = hotelinfo.StarRating.Value,
                    BestBuy = false,
                    Features = GetHotelFeatures(hotelcode, hotelinfo, features),
                    Rooms = roomsRoomGroups?.Rooms,
                    RoomGroups = roomsRoomGroups?.RoomGroups,
                    AdultOnly = false,
                    BlockInfant = false,
                    Images = images,
                    Translations = new Translations
                    {
                        headline = new List<TranslationsHeadline>(),
                        description = new List<TranslationsDescription>()
                    }
                };
            }
            catch (Exception ex)
            {
                Log(ILoggerStaticDataSeverity.Error, $"Error while Creating Hotel. Hotel : { hotelcode}. City : {place.Id}. Error : {ex.Message}");
                return null;
            }
        }

        /// <summary>
        /// Preparing Rooms and RoomGroups
        /// </summary>
        /// <param name="hotelInformation">t_HotelInformation</param>
        /// <returns>RoomsRoomGroups</returns>
        private RoomsRoomGroups PrepareRoomsRoomGroups(t_HotelInformation hotelInformation)
        {
            try
            {
                var roomsRoomGroups = new RoomsRoomGroups
                {
                    RoomGroups = new Dictionary<string, RoomGroup>(),
                    Rooms = new Dictionary<string, Room>()
                };
                if (hotelInformation?.RoomCategories == null || hotelInformation.RoomCategories.Length == 0)
                    return roomsRoomGroups;
                foreach (var roomCategory in hotelInformation.RoomCategories?
                    .Where(c => !string.IsNullOrEmpty(c.Id) && !string.IsNullOrEmpty(c.Description)).AsParallel())
                {
                    lock (roomsRoomGroups.Rooms)
                        if (roomsRoomGroups.Rooms.ContainsKey(roomCategory.Id))
                            continue;

                    // CC-3693 max 50 chars
                    if (roomCategory.Description.Length > 50)
                        roomCategory.Description = roomCategory.Description.Substring(0, 50);

                    var room = new Room
                    {
                        RoomId = roomCategory.Id,
                        Description = roomCategory.RoomDescription,
                        TypeId = roomCategory.Description
                    };

                    RoomGroup roomgroup;
                    lock (roomsRoomGroups.RoomGroups)
                        if (roomsRoomGroups.RoomGroups.ContainsKey(roomCategory.Description))
                            roomsRoomGroups.RoomGroups.TryGetValue(roomCategory.Description, out roomgroup);
                        else
                        {
                            roomgroup = new RoomGroup
                            {
                                RoomGroupId = roomCategory.Description,
                                Rooms = new Dictionary<string, Room>()
                            };
                            lock (roomsRoomGroups.RoomGroups)
                                roomsRoomGroups.RoomGroups.Add(roomCategory.Description, roomgroup);
                            lock (root)
                                if (!root.RoomTypes.ContainsKey(roomCategory.Description))
                                {
                                    root.RoomTypes.Add(roomCategory.Description, new RoomType
                                    {
                                        Type_Id = roomCategory.Description
                                    });
                                }
                        }
                    if (roomgroup != null)
                        lock (roomgroup.Rooms)
                            roomgroup.Rooms.Add(roomCategory.Id, room);
                    lock (roomsRoomGroups.Rooms)
                        roomsRoomGroups.Rooms.Add(roomCategory.Id, room);
                }
                return roomsRoomGroups;
            }
            catch (Exception ex)
            {
                Log(ILoggerStaticDataSeverity.Error, $"Error while preparing Rooms and RoomGroups. Error : {ex.Message}");
                return null;
            }
        }

        /// <summary>
        /// Get Accommodation type 
        /// </summary>
        /// <param name="itemType">t_ItemType</param>
        /// <returns>AccomodationType</returns>
        private AccomodationType GetAccomodation(t_ItemType itemType)
        {
            try
            {
                return itemType == t_ItemType.apartment ? AccomodationType.Apartment : AccomodationType.Hotel;
            }
            catch (Exception ex)
            {
                Log(ILoggerStaticDataSeverity.Error, $"Error while getting accommodation type. Error : {ex.Message}");
                return AccomodationType.Hotel;
            }
        }

        /// <summary>
        /// Unzip specified ZIP file content to specified target directory.
        /// </summary>
        /// <param name="sFile">The path to the zip file.</param>
        /// <param name="unzippath">unzip directory path</param>
        /// <returns>The list of unzipped files.</returns>
        private List<string> UnZip(string sFile, out string unzippath)
        {
            var sw = Stopwatch.StartNew();
            Log(ILoggerStaticDataSeverity.Info, $"UnZip Started. File Path:{sFile}");
            try
            {
                if (!File.Exists(sFile))
                {
                    Log(ILoggerStaticDataSeverity.Error, $"Zip file is not available in specified location. File Path : {sFile}.");
                    unzippath = null;
                    return null;
                }
                unzippath = configuration.LocalTemp + "uncompress\\" + Path.GetFileNameWithoutExtension(sFile);
                if (Directory.Exists(unzippath))
                    Directory.Delete(unzippath, true);

                Directory.CreateDirectory(unzippath);
                lock (this)
                {
                    new Compression().UnCompressFile(sFile, unzippath);
                }
                var files = Directory.GetFiles(unzippath);
                if (_gtaXmlLiveSettings.DeleteZipFile)
                    FileDelete(sFile);
                return files.ToList();
            }
            catch (Exception ex)
            {
                Log(ILoggerStaticDataSeverity.Error, $"Error in UnZip. File Path : {sFile}. Error : {ex.Message}");
                unzippath = null;
                return null;
            }
            finally
            {
                var ems = sw.ElapsedMilliseconds;
                Log(ILoggerStaticDataSeverity.Info, $"UnZip Completed ElapsedMilliseconds:{ems }. File Path:{sFile}");
            }
        }

        /// <summary>
        /// Download
        /// </summary>
        /// <returns>Dictionary of Language, FileInformation</returns>
        private Dictionary<Language, FileInformation> Download()
        {
            try
            {
                var fileInfoList = new Dictionary<Language, FileInformation>();
                var total = _gtaXmlLiveSettings.LanguageList.Language.Count;
                var pageSize = _gtaXmlLiveSettings.MaxDownloadRequest; // set your page size, which is number of records per page
                var page = 1; // set current page number, must be >= 1
                bool validloop = true;
                while (validloop)
                {
                    var skip = pageSize * (page - 1);
                    var isvalid = skip < total;
                    if (isvalid)
                    {
                        var tasks = new List<Task>();
                        _gtaXmlLiveSettings.LanguageList.Language
                            .Select(p => p)
                            .Skip(skip)
                            .Take(pageSize)
                            .Select(language =>
                            {
                                tasks.Add(Task.Factory.StartNew(() => DownloadRequest(language, fileInfoList)));
                                return true;
                            }).ToArray();
                        Task.WaitAll(tasks.ToArray());
                        page++;
                    }
                    else
                        validloop = false;
                }
                return fileInfoList;
            }
            catch (Exception ex)
            {
                Log(ILoggerStaticDataSeverity.Error, $"Error in Download. Error : {ex.Message}");
                return null;
            }
        }

        /// <summary>
        /// GetItemDetail
        /// </summary>
        /// <param name="filePath">filePath</param>
        /// <returns>t_ItemDetail</returns>
        private t_ItemDetail GetItemDetail(string filePath)
        {
            try
            {
                var response = GeneralUtilities.DeserializeXmlFile<t_Response>(filePath);
                var responseDetails = response.Item as t_ResponseDetails;
                if (responseDetails?.Items == null || !responseDetails.Items.Any()) return null;
                var searchItemInformation = responseDetails.Items.First() as t_SearchItemInformationResponse;
                var itemDetails = searchItemInformation?.Item as t_ItemDetails;
                var itemDetail = itemDetails?.ItemDetail;
                return itemDetail?.First();
            }
            catch (Exception ex)
            {
                Log(ILoggerStaticDataSeverity.Error, $"Error in Get Item Detail. File path {filePath}. Error : {ex.Message}");
                return null;
            }
        }

        #endregion
    }
}
