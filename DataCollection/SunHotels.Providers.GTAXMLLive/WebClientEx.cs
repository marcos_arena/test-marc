﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SunHotels.Providers
{
    public class WebClientEx : WebClient
    {
        /// <summary>
        /// Gets or sets the time out in milliseconds.
        /// </summary>
        public int TimeOut { private get; set; }
        public string ProtocolVersion { private get; set; }
        public string ContentType { private get; set; }
        public string Accept { private get; set; }
        public bool KeepAlive { private get; set; }

        public CancellationToken CancellationToken;

        protected override WebRequest GetWebRequest(Uri uri)
        {
            var request = (HttpWebRequest)base.GetWebRequest(uri);
            if (request == null) return null;
            request.Timeout = TimeOut;
            // automatically add support for deflate..
            request.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
            request.KeepAlive = KeepAlive;
            if (!string.IsNullOrEmpty(Accept))
                request.Accept = Accept;
            if (!string.IsNullOrEmpty(ContentType))
                request.ContentType = ContentType;
            request.ProtocolVersion = ProtocolVersion.Equals("1.1") ? HttpVersion.Version11 : HttpVersion.Version10;
            CancellationToken.Register(() => { request.Abort(); });
            return request;
        }
    }

    public static class AsyncHelpers
    {
        /// <summary>
        /// Execute's an async Task of T method which has a void return value synchronously
        /// </summary>
        /// <param name="task">Task of T method to execute</param>
        public static void RunSync(Func<Task> task)
        {
            var oldContext = SynchronizationContext.Current;
            var synch = new ExclusiveSynchronizationContext();
            SynchronizationContext.SetSynchronizationContext(synch);
            synch.Post(async _ =>
            {
                try
                {
                    await task();
                }
                catch (Exception e)
                {
                    synch.InnerException = e;
                    throw;
                }
                finally
                {
                    synch.EndMessageLoop();
                }
            }, null);
            synch.BeginMessageLoop();

            SynchronizationContext.SetSynchronizationContext(oldContext);
        }

        /// <summary>
        /// Execute's an async Task of T method which has a T return type synchronously
        /// </summary>
        /// <typeparam name="T">Return Type</typeparam>
        /// <param name="task">Task of T method to execute</param>
        /// <returns></returns>
        public static T RunSync<T>(Func<Task<T>> task)
        {
            var oldContext = SynchronizationContext.Current;
            var synch = new ExclusiveSynchronizationContext();
            SynchronizationContext.SetSynchronizationContext(synch);
            T ret = default(T);
            synch.Post(async _ =>
            {
                try
                {
                    ret = await task();
                }
                catch (Exception e)
                {
                    synch.InnerException = e;
                    throw;
                }
                finally
                {
                    synch.EndMessageLoop();
                }
            }, null);
            synch.BeginMessageLoop();
            SynchronizationContext.SetSynchronizationContext(oldContext);
            return ret;
        }

        private class ExclusiveSynchronizationContext : SynchronizationContext
        {
            private bool _done;
            public Exception InnerException { private get; set; }
            private readonly AutoResetEvent _workItemsWaiting = new AutoResetEvent(false);
            private readonly Queue<Tuple<SendOrPostCallback, object>> _items =
                new Queue<Tuple<SendOrPostCallback, object>>();

            public override void Send(SendOrPostCallback d, object state)
            {
                throw new NotSupportedException("We cannot send to our same thread");
            }

            public override void Post(SendOrPostCallback d, object state)
            {
                lock (_items)
                {
                    _items.Enqueue(Tuple.Create(d, state));
                }
                _workItemsWaiting.Set();
            }

            public void EndMessageLoop()
            {
                Post(_ => _done = true, null);
            }

            public void BeginMessageLoop()
            {
                while (!_done)
                {
                    Tuple<SendOrPostCallback, object> task = null;
                    lock (_items)
                    {
                        if (_items.Count > 0)
                        {
                            task = _items.Dequeue();
                        }
                    }
                    if (task != null)
                    {
                        task.Item1(task.Item2);
                        if (InnerException != null) // the method threw an exception
                        {
                            throw new AggregateException("AsyncHelpers.Run method threw an exception.", InnerException);
                        }
                    }
                    else
                    {
                        _workItemsWaiting.WaitOne();
                    }
                }
            }

            public override SynchronizationContext CreateCopy()
            {
                return this;
            }
        }
    }

    public class ConnectHttp
    {
        public string BaseUrl { get; set; }
        public bool KeepAlive { get; set; }
        public string RequestContentType { get; set; }
        public string Method { get; set; }
        public string DownloadContentType { get; set; }
        public string ErrorContentType { get; set; }
        public int TimeOut { get; set; }
        public string PostParameter { get; set; }
        public string ProtocolVersion { get; set; }
        public string DownloadFilePath { get; set; }
        public CancellationToken RequestCancellationToken { get; set; }
        /// <summary>
        /// Download File
        /// </summary>
        /// <returns>string</returns>
        public async Task<string> DownloadFile()
        {
            var returnmessage = string.Empty;
            try
            {
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
                ServicePointManager.ServerCertificateValidationCallback = delegate (object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) { return true; };

                var webRequest = (HttpWebRequest)WebRequest.Create(BaseUrl);
                webRequest.Method = Method; //"POST";
                webRequest.ContentType = RequestContentType;
                webRequest.KeepAlive = KeepAlive;
                webRequest.Timeout = TimeOut;
                webRequest.ProtocolVersion = ProtocolVersion.Equals("1.1") ? HttpVersion.Version11 : HttpVersion.Version10;
                webRequest.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
                RequestCancellationToken.Register(() => { webRequest.Abort(); });
                byte[] byteArray = Encoding.UTF8.GetBytes(PostParameter);
                // Get the request stream.
                using (var dataStream = await webRequest.GetRequestStreamAsync().ConfigureAwait(false))
                {
                    // Write the data to the request stream.
                    dataStream.Write(byteArray, 0, byteArray.Length);
                    // Close the Stream object.
                    dataStream.Close();
                    dataStream.Dispose();
                }
                using (var webResponse = await webRequest.GetResponseAsync().ConfigureAwait(false))
                {
                    var httpWebResponse = webResponse as HttpWebResponse;
                    if (httpWebResponse != null)
                    {
                        if (httpWebResponse.StatusCode == HttpStatusCode.OK)
                        {
                            Stream receiveStream = null;
                            try
                            {
                                receiveStream = httpWebResponse.GetResponseStream();
                                if (httpWebResponse.ContentType == DownloadContentType) //"application/download"
                                {
                                    if (receiveStream == null) return returnmessage;
                                    if (httpWebResponse.ContentEncoding.Contains("gzip"))
                                    {
                                        receiveStream = new GZipStream(receiveStream, CompressionMode.Decompress);
                                    }
                                    else if (httpWebResponse.ContentEncoding.Contains("deflate"))
                                    {
                                        receiveStream = new DeflateStream(receiveStream, CompressionMode.Decompress);
                                    }
                                    using (var oWriter = new FileStream(DownloadFilePath, FileMode.Create))
                                    {
                                        receiveStream.CopyTo(oWriter);                                        
                                        oWriter.Dispose();
                                    }
                                }
                                else if (httpWebResponse.ContentType == ErrorContentType) //text/xml;charset=UTF-8
                                {
                                    if (receiveStream == null) return returnmessage;
                                    using (var streamReader = new StreamReader(receiveStream, Encoding.GetEncoding(httpWebResponse.CharacterSet)))
                                        returnmessage = streamReader.ReadToEnd();                                    
                                }
                                else
                                {
                                    returnmessage = $"Invalid response from API. Response Content type: {httpWebResponse.ContentType}";
                                }
                            }
                            finally
                            {
                                if (receiveStream != null)
                                {
                                    receiveStream.Close();
                                    receiveStream.Dispose();
                                }
                            }

                        }
                        else
                        {
                            if (httpWebResponse.StatusCode != HttpStatusCode.NoContent)
                            {
                                returnmessage = httpWebResponse.StatusDescription;
                            }
                        }
                    }
                    else
                    {
                        returnmessage = "Null response from HttpWebResponse";
                    }
                    return returnmessage;
                }
            }
            catch (Exception ex)
            {
                returnmessage = $"Error while downloading zip file. File Path : {DownloadFilePath}. Request : {BaseUrl}-{PostParameter}. Error : {ex.Message}";
                return returnmessage;
            }
        }
    }
}
