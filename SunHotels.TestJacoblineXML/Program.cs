﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Reflection;
using System.Xml.Serialization;
using SunHotels.XML;
using SunHotels.XML.Data;
using SunHotels.XML.HelpClasses;
using SunHotels.Providers;



namespace SunHotels.TestJacoblineXML
{
	class Program
	{
		private static SunHotels.XML.Configuration configuration;

		static void Main(string[] args)
		{

			string provider = "Jacobline";

			XmlSerializer serializer = new XmlSerializer(typeof(Configuration));
			FileStream file = new FileStream(provider + ".xml", FileMode.Open, FileAccess.Read);
			configuration = (Configuration)serializer.Deserialize(file);
			file.Close();

			JacoblineXML jlxml = new JacoblineXML();
			jlxml.StartGathering(configuration);
			

		}
	}
}
