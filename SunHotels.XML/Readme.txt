﻿

- [ S U N H O T E L S C O N F I G ] -


SunHotelsConfig ska användas för att hantera inställningar för Sunhotels webbplatser/webservices och applikationer som bygger
på .Net. Det är även tänkt att skrivas en proxy för classic-asp applikationer.


- [Konfigurationsfilerna] -

: Definition :

<Root>
	<[Key1] value="[Value1]" />
	<[Key2] value="[Value2]" />
	<[Key3]>
		<[Key4] value="[Value4]" />
		<[Key5] value="[Value5]" />
	</[Key3]>
	
	<[Key6] inherit="[Key1]">
	<[Key7] inherit="[Key3].[Key4]">
</Root>

Alla konfigurationsfiler är i xml-format.
Rootnoden kan heta vad som helst.
Strukturen definierar ni som ni vill ha den.
Attributet "value" definierar det värde man vill sätta på inställningen.
Attributet "inherit" pekar ut en befintlig värde via dess Sökväg.




- [Sökvägar och filer] -

: GlobalConfigPath :
I SunHotelsFramework ligger ett projekt som heter SunHotelsConfigSetup.
Detta program har till uppgift att sätta inställningen för GlobalConfigPath i din dators register.
Som standard väljer den katalogen GlobalConfig som ligger under projektet GlobalConfig.
Du kan ange ett argument för att peka ut katalogen någon annanstans. 
Om inställningen redan finns i registret uppdateras den.


: GlobalConfigPath \ ['Dev'|'Test'|'Live'] :
De här katalogerna specificerar de standardlägen man kan köra applikationen i och huserar alla dess konfigurationsfiler.
Man kan även skapa undermapp med valfritt namn i stället för Dev/Test/Live. 
Namnet behöver matchas av motsvarande värde på ConfigMode i applikationens .config-fil (se nedan).


: Namngivning :
Filändelsen är alltid ".xml".
Filerna kan namnges och laddas hierakiskt.

Exempel: 
Sunhotels.xml <- innehåller grundinställningar
Sunhotels.internalwebservice.xml  <- innehåller projektspecifika inställningar för internalwebservice och eventuellt overrides på inställningar från Sunhotels.xml
Sunhotels.xmlapi.xml <- innehåller projektspecifika inställningar för xmlapi och eventuellt overrides på inställningar från Sunhotels.xml


: Konfigurationsnamn :
Konfigurationsnamnet är detsamma som filnamnet exklusive ".xml"
Dvs för Sunhotels.xmlapi.xml är ConfigName = "Sunhotels.xmlapi"


: Web.Config alt. App.Config :
Någon av dessa filer beroende på om det är webb-project eller windows-applikation måste innehålla:
	
	<appSettings>
		<add key="ConfigName" value="[ConfigName]"/> 
		<add key="ConfigMode" value="['Dev'|'Test'|'Live']"/>
	</appSettings>
	
De här läses in vid initieringen av Config-objektet.
	
	

- [Att använda libbet ifrån applikationer] -

Skapa en referens till SunHotelsConfig
Instansiera Config objektet med:
 
	Config cfg = Config.Instance;
	
Ange sökvägen för att komma åt inställningarna
Exempel:
	
	cfg["Key1"]  = "Value1"
	cfg["Key3.Key5"]  = "Value5"




	





