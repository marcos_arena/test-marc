﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SunHotels.Export;

namespace SunHotels.XML.Data
{
    public class Note
    {
        public int Id { get; private set; }
        public DateTime FromDate { get; private set; }
        public DateTime ToDate { get; private set; }

        private Note() {}

        public static List<Note> Create(int id, DateTime fromDate, DateTime toDate, int daySpan, DateTime today)
        {
            // Normal date span
            if (fromDate.Year >= 2000) {
                return new List<Note> { new Note {
                        Id = id,
                        FromDate = fromDate,
                        ToDate = toDate
                    }
                };
            }

            // Dates at year 1900 is repeated every year
            // NOTE: See unit tests

            // Showed on all days
            if ((toDate - fromDate).Days >= 364) {
                return new List<Note> {
                    new Note {
                        Id = id,
                        FromDate = today,
                        ToDate = today.AddDays(daySpan)
                    }
                };
            }

            var notes = new List<Note>();

            var from = fromDate.AddYears(today.Year - fromDate.Year);
            var to = toDate.AddYears(today.Year - fromDate.Year);
            var lastDate = today.AddDays(daySpan);

            // Showed on specific days
            // The specific dates can overlap several years, so create a note for each of them
            for (int i = 0; from.AddYears(i) <= lastDate; i++) {
                if (today.AddYears(i) <= to.AddYears(i) && from.AddYears(i) <= lastDate) {
                    notes.Add(new Note() {
                        Id = id,
                        FromDate = from.AddYears(i) <= today ? today : from.AddYears(i),
                        ToDate = to.AddYears(i) >= lastDate ? lastDate : to.AddYears(i)
                    });
                }
            }

            return notes;
        }
    }
}
