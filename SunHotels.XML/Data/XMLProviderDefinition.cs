﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SunHotels.XML.Data
{
    public class XMLProviderDefinition
    {
        public byte MinRelease { get; set; }
        public byte MinUnits { get; set; }
    }
}
