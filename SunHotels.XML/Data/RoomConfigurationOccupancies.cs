﻿namespace SunHotels.XML.Data
{
    /// <summary>
    /// Min / Max values are inclusive
    /// </summary>
    public class RoomConfigurationOccupancies 
    {
        public int Id { get; set; }
        public int MinOccupancy { get; set; }
        public int MaxOccupancy { get; set; }
        public int MinAdults { get; set; }
        public int MaxAdults { get; set; }
        public int MinChildren { get; set; }
        public int MaxChildren { get; set; }
    }
}