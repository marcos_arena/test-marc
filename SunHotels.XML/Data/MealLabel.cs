﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SunHotels.XML.Data
{
    public class MealLabel
    {
        public int MealId { get; set; }

        public int MealLabelId { get; set; }

        public string MealType
        {
            get
            {
                switch (MealId) {
                    case 3:
                        return "breakfast";
                    case 4:
                        return "half_board";
                    case 5:
                        return "full_board";
                    case 6:
                        return "all_inclusive";
                    default:
                        return "";
                }
            }
        }

        public MealLabel(int mealId, int? mealLabelId)
        {
            this.MealId = mealId;
            this.MealLabelId = (mealLabelId.HasValue) ? mealLabelId.Value : 0;
        }
    }
}
