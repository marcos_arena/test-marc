using System;
using System.Collections.Generic;

namespace SunHotels.XML.Data
{
    public class Room : ICloneable
    {
        public override string ToString()
        {
            return "TypeId: " + typeId + ", BookableId: " + roomId;
        }

        private string typeId;

        public string TypeId
        {
            get { return typeId; }
            set { typeId = value; }
        }

        private string roomId;

        public string RoomId
        {
            get { return roomId; }
            set { roomId = value; }
        }

        private bool adultOnlyHotel;

        public bool AdultOnlyHotel
        {
            get { return adultOnlyHotel; }
            set { adultOnlyHotel = value; }
        }

        private string description;

        public string Description
        {
            get { return description; }
            set { description = value; }
        }

        public bool BestBuy { get; set; }

        public List<Feature> Features { get; set; }
        public List<Theme> Themes { get; set; }

        private SortedDictionary<DateTime, AvailabilityPeriod> availabilityPeriods;
        public SortedDictionary<DateTime, AvailabilityPeriod> AvailabilityPeriods
        {
            get { return availabilityPeriods; }
            set { availabilityPeriods = value; }
        }

        public string Currency { get; set; }
        public List<Note> Notes { get; set; }

        /**
         * Alias for "RequireBookingOfAllBeds" on XMLProvider.
         **/
        public bool MealSupplementRestriction { get; set; }
        public List<string[]> GlobalTypes { get; set; }

        public int RoomConfigurationInfantsAndChildrenId { get; set; }
        public int RoomConfigurationOccupancyId { get; set; }
        public List<int> RoomConfigurationExtrabedFactors { get; set; }
        public List<int> RoomConfigurationOccupancyBlocks { get; set; }

        public Room()
        {
            this.availabilityPeriods = new SortedDictionary<DateTime, AvailabilityPeriod>();
            this.Features = new List<Feature>();
            this.Themes = new List<Theme>(); 
            //this.currentAvailabilityPeriod = null;
        }

        /*public void createAvailabilityPeriods()
        {
            AvailabilityPeriod currentPeriod = null;
            //RoomType currentRoomType = null;
            SortedDictionary<DateTime, AvailabilityPeriod> tempList = new SortedDictionary<DateTime, AvailabilityPeriod>();

            foreach (AvailabilityPeriod ap in availabilityPeriods.Values)
            {
                if (currentPeriod != null
                    && ap.AvailabilityAvailableUnits == currentPeriod.AvailabilityAvailableUnits
                    && ap.Pricing_BasePrice == currentPeriod.Pricing_BasePrice
                    && ap.IsWeeklyStay == currentPeriod.IsWeeklyStay
                    && ap.MinimumStay == currentPeriod.MinimumStay
                    && ap.DateFrom > currentPeriod.DateFrom && ap.DateFrom <= currentPeriod.DateTo.AddDays(1))       //2007-11-01 && ap.DateFrom.Equals(currentPeriod.DateTo))
                {
                    currentPeriod.DateTo = ap.DateTo;
                }
                else
                {
                    currentPeriod = ap;
                    if (currentPeriod.AvailabilityAvailableUnits > 0)
                    {
                        tempList.Add(currentPeriod.DateFrom, currentPeriod);
                    }
                }
            }

            AvailabilityPeriods = tempList;
        }*/

        #region ICloneable Members

        public Room Clone()
        {
            Room result = (Room)((ICloneable)this).Clone();
            SortedDictionary<DateTime, AvailabilityPeriod> apCopy = new SortedDictionary<DateTime, AvailabilityPeriod>();
            foreach (DateTime dt in this.availabilityPeriods.Keys)
            {
                apCopy.Add(dt, this.availabilityPeriods[dt].Clone());
            }
            result.AvailabilityPeriods = apCopy;

            return result;
        }

        object ICloneable.Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion

        public KeyValuePair<int, string> AllotmentPattern { get; set; }
        public bool HasSafePassage { get; set; }
        public bool NoRefundable { get; set; }
         
    }
}
