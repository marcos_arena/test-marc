﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SunHotels.XML.Data
{
    public class PlaceCodes
    {
        public string Type { get; set; }
        public string Value { get; set; }
    }
}
