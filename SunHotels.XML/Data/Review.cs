﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SunHotels.XML.Data
{
    public class Review
    {
        public String Provider { get; set; }
        public String Type { get; set; }
        public String Value { get; set; }
    }
}
