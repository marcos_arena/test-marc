﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SunHotels.XML.Data
{
	public class DiscountSet : IEnumerable<Discount>
	{
		List<Discount> discounts = new List<Discount>();

		#region IEnumerable<Discount> Members

		public IEnumerator<Discount> GetEnumerator()
		{
			return discounts.GetEnumerator();
		}

		#endregion

		#region IEnumerable Members

		System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
		{
			return discounts.GetEnumerator();
		}

		#endregion

		public bool TryAdd(Discount newDiscount)
		{
            if (newDiscount == null) return false;

			IEnumerable<Discount> redundantList = discounts.Where(d => d.Type == newDiscount.Type && d.MinDays == newDiscount.MinDays);
			if (redundantList.Count() == 0)
			{
				// Everything is allright
				discounts.Add(newDiscount);
				return true;
			}
			else
			{
				//Ok, we have a duplicate, should we remove this discount or the old one?
				Discount oldDiscount = redundantList.Single();
				if (newDiscount.Amount > oldDiscount.Amount || newDiscount.Percent > oldDiscount.Percent)
				{
					discounts.Remove(oldDiscount);
					discounts.Add(newDiscount);
				}
			}

			// No need to add the old discount
			return false;
		}

		public int Count { get { return discounts.Count; } }
	}
}
