﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SunHotels.XML.Data
{
    public class Campaign
    {
        public double Value { get; set; }
        public int UnitId { get; set; }
        public int? RoomProviderId { get; set; }
        public DateTime CheckinStartDate { get; set; }
        public DateTime CheckinEndDate { get; set; }
        public int? HotelCountryId { get; set; }
        public int? HotelDestinationId { get; set; }
        public int? HotelResortId { get; set; }
        public int? HotelId { get; set; }
        public int? AgentId { get; set; }
        public int? AgentCountryId { get; set; }
        public int? BrandId { get; set; }
    }
}
