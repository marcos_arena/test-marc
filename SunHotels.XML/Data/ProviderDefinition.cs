using System;
using System.Collections.Generic;
using System.Text;

namespace SunHotels.XML.Data
{
    public class ProviderDefinition
    {
        private string name;

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        private string currency;

        public string Currency
        {
            get { return currency; }
            set { currency = value; }
        }
	}
}
