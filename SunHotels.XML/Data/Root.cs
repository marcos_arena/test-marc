using System.Collections.Generic;
using System.Linq;
using SunHotels.Export;

namespace SunHotels.XML.Data
{
    public class Root : ExportData
    {
        public int Version { get; set; }
        public ProviderDefinition ProviderDefinition { get; set; }
        public List<Place> Places { get; set; }
        public List<Campaign> Campaigns { get; set; }
        public Dictionary<int, List<Note>> CountryNotes { get; set; }
        public Dictionary<int, List<Note>> DestinationNotes { get; set; }
        public Dictionary<int, List<Note>> ResortNotes { get; set; }
        public Dictionary<string, RoomType> RoomTypes { get; set; }
        public Dictionary<string, CancellationPolicy> CancellationsPolicies { get; set; }

        /// <summary>
        /// Only used for pulling out statistics about amount of availability / roomtype
        /// </summary>
        public Dictionary<string, int> RoomTypeAvailability { get; set; }
        public int? AffectedStartDateAdjustment { get; set; }
        public int? AffectedEndDateAdjustment { get; set; }
        public List<string> PlaceCodeTypes { get; set; }
        public List<Feature> Features { get; set; }
        public List<Theme> Themes { get; set; }
        public List<Hotel.Distance> Distance { get; set; }

        public Dictionary<int, RoomConfigurationInfantsAndChildren> RoomConfigInfantsAndChildren { get; set; }
        public Dictionary<int, RoomConfigurationOccupancies> RoomConfigOccupancies { get; set; }
        public Dictionary<int, RoomConfigurationExtrabedFactors> RoomConfigExtrabedFactors { get; set; }
        public Dictionary<int, RoomConfigurationOccupancyBlocks> RoomConfigOccupancyBlocks { get; set; }

        public Root()
        {
            ProviderDefinition = new ProviderDefinition();
            RoomTypes = new Dictionary<string, RoomType>();
            CancellationsPolicies = new Dictionary<string, CancellationPolicy>();
            Places = new List<Place>();
            Campaigns = new List<Campaign>();
            RoomTypeAvailability = new Dictionary<string, int>();
            PlaceCodeTypes = new List<string>();
            Features = new List<Feature>();
            Themes = new List<Theme>();
            Distance = new List<Hotel.Distance>();
            RoomConfigInfantsAndChildren = new Dictionary<int, RoomConfigurationInfantsAndChildren>();
            RoomConfigOccupancies = new Dictionary<int, RoomConfigurationOccupancies>();
            RoomConfigExtrabedFactors = new Dictionary<int, RoomConfigurationExtrabedFactors>();
            RoomConfigOccupancyBlocks = new Dictionary<int, RoomConfigurationOccupancyBlocks>();
        }

		#region Data navigation
		public IEnumerable<Place> AllPlaces(IEnumerable<Place> places = null)
		{
			places = places ?? Places;
			foreach (var p in places)
			{
				yield return p;
				foreach (var pc in AllPlaces(p.Places))
					yield return pc;
			}
		}

		public IEnumerable<Hotel> AllHotels()
		{
			return AllPlaces().SelectMany(p => p.Hotels.Select(kv => kv.Value));
		}
		#endregion Data navigation
	}
}
