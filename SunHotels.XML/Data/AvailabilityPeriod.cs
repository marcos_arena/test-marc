﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SunHotels.XML.Data
{
	public class AvailabilityPeriod : ICloneable
	{
		private object extra;

		public object Extra
		{
			get { return extra; }
			set { extra = value; }
		}

		private bool isWeeklyStay;
		public bool IsWeeklyStay
		{
			get { return isWeeklyStay; }
			set { isWeeklyStay = value; }
		}

		public bool NoRefund { get; set; }

		private bool isArrivalPossible;

		public bool IsArrivalPossible
		{
			get { return isArrivalPossible; }
			set { isArrivalPossible = value; }
		}

		private DateTime dateFrom;

		public DateTime DateFrom
		{
			get { return dateFrom; }
			set { dateFrom = value; }
		}

		private DateTime dateTo;

		public DateTime DateTo
		{
			get { return dateTo; }
			set { dateTo = value; }
		}

		private DateTime dateTemp;

		public DateTime DateTemp
		{
			get { return dateTemp; }
			set { dateTemp = value; }
		}



		private string pricing_baseIncludedBoard;

		public string Pricing_BaseIncludedBoard
		{
			get { return pricing_baseIncludedBoard; }
			set { pricing_baseIncludedBoard = value; }
		}


		private string pricing_baseBookableBoardId;

		public string Pricing_BookableBoardId
		{
			get { return pricing_baseBookableBoardId; }
			set { pricing_baseBookableBoardId = value; }
		}


		private string pricing_basePrice;

		public string Pricing_BasePrice
		{
			get { return pricing_basePrice; }
			set { pricing_basePrice = value; }
		}


		private string pricing_extrabedAdult;

		public string Pricing_ExtrabedAdult
		{
			get { return pricing_extrabedAdult; }
			set { pricing_extrabedAdult = value; }
		}

		private string pricing_extrabedChild;

		public string Pricing_ExtrabedChild
		{
			get { return pricing_extrabedChild; }
			set { pricing_extrabedChild = value; }
		}



		private List<AvailabilityPeriod.AdditionalBoard> additionalBoards;

		public List<AvailabilityPeriod.AdditionalBoard> AdditionalBoards
		{
			get { return additionalBoards; }
			set { additionalBoards = value; }
		}

		private DiscountSet discounts;

		public DiscountSet Discounts
		{
			get { return discounts; }
			set { discounts = value; }
		}

		private int availabilityTotalAllotment;

		public int AvailabilityTotalAllotment
		{
			get { return availabilityTotalAllotment; }
			set { availabilityTotalAllotment = value; }
		}

		private int availabilityAvailableUnits;

		public int AvailabilityAvailableUnits
		{
			get { return availabilityAvailableUnits; }
			set { availabilityAvailableUnits = value; }
		}


		private int releaseDays;

		public int ReleaseDays
		{
			get { return releaseDays; }
			set { releaseDays = value; }
		}

		private int minimumStay;

		public int MinimumStay
		{
			get { return minimumStay; }
			set { minimumStay = value; }
		}


		private List<string> cancellationPolicies;
		/// <summary>
		/// List of Cancellation policy id's
		/// </summary>
		public List<string> CancellationPolicies
		{
			get { return cancellationPolicies; }
			set { cancellationPolicies = value; }
		}

		public int BoardTypeId
		{
			get
			{
				switch (pricing_baseIncludedBoard)
				{
					case "breakfast":
						return 3;
					case "half_board":
						return 4;
					case "full_board":
						return 5;
					case "all_inclusive":
						return 6;
					default:
						return 1;
				}
			}
		}

		public AvailabilityPeriod()
		{
			this.additionalBoards = new List<AdditionalBoard>();
			this.discounts = new DiscountSet();
			this.cancellationPolicies = new List<string>();
		}

		public class AdditionalBoard : ICloneable, IEquatable<AdditionalBoard>
		{
			public string Type { get; set; }

			public string Adult { get; set; }

			public string Child { get; set; }

			public string BookableBoardId { get; set; }

			public int TypeId
			{
				get
				{
					switch (Type)
					{
						case "breakfast":
							return 3;
						case "half_board":
							return 4;
						case "full_board":
							return 5;
						case "all_inclusive":
							return 6;
						default:
							return 1;
					}
				}
			}

			public AdditionalBoard(string type, string adult, string child, string bookableBoardId)
			{
				Type = type;
				Adult = adult;
				Child = child;
				BookableBoardId = bookableBoardId;
			}

			#region ICloneable Members

			public AdditionalBoard Clone()
			{
				return (AdditionalBoard)((ICloneable)this).Clone();
			}

			object ICloneable.Clone()
			{
				return this.MemberwiseClone();
			}

			#endregion

			#region IEquatable<AdditionalBoard> Members

			public bool Equals(AdditionalBoard other)
			{
				return (this.Adult == other.Adult &&
					this.BookableBoardId == other.BookableBoardId &&
					this.Child == other.Child &&
					this.Type == other.Type);
			}

			#endregion

			public override bool Equals(object obj)
			{
				if (obj.GetType() != typeof(AdditionalBoard))
				{
					return false;
				}

				AdditionalBoard compare = (AdditionalBoard)obj;

				return (this.Adult == compare.Adult &&
					this.BookableBoardId == compare.BookableBoardId &&
					this.Child == compare.Child &&
					this.Type == compare.Type);
			}

			public override int GetHashCode()
			{
				return (this.Adult.GetHashCode() ^
					this.BookableBoardId.GetHashCode() ^
					this.Child.GetHashCode() ^
					this.Type.GetHashCode());
			}

			public static Dictionary<int, string> GetBoardTypes()
			{
				return new Dictionary<int, string>() {
                    {1, "No Meals"},
                    {3, "Breakfast"},
                    {4, "Half Board"},
                    {5, "Full Board"},
                    {6, "All Inclusive"}
                };
			}
		}

		#region ICloneable Members

		public AvailabilityPeriod Clone()
		{
			AvailabilityPeriod result = (AvailabilityPeriod)((ICloneable)this).Clone();

			List<AdditionalBoard> additionalBoardCopy = new List<AdditionalBoard>();
			foreach (AdditionalBoard ab in this.AdditionalBoards)
			{
				additionalBoardCopy.Add(ab.Clone());
			}
			result.AdditionalBoards = additionalBoardCopy;

			DiscountSet discountCopy = new DiscountSet();
			foreach (Discount d in this.Discounts)
			{
				discountCopy.TryAdd(d.Clone());
			}
			result.Discounts = discountCopy;

			return result;
		}

		object ICloneable.Clone()
		{
			return this.MemberwiseClone();
		}

		#endregion

		public bool Equals(AvailabilityPeriod ap)
		{
			if (!(
			   this.AvailabilityAvailableUnits == ap.AvailabilityAvailableUnits
				&& this.Pricing_BasePrice == ap.Pricing_BasePrice
				&& this.IsWeeklyStay == ap.IsWeeklyStay
				&& this.MinimumStay == ap.MinimumStay
				&& this.DateFrom.Equals(ap.DateTo.AddDays(1))
				&& this.pricing_baseBookableBoardId == ap.pricing_baseBookableBoardId
				&& this.pricing_baseIncludedBoard == ap.pricing_baseIncludedBoard
				&& this.IsArrivalPossible == ap.IsArrivalPossible
				&& this.Pricing_ExtrabedAdult == ap.Pricing_ExtrabedAdult
				&& this.Pricing_ExtrabedChild == ap.Pricing_ExtrabedChild
				&& this.ReleaseDays == ap.releaseDays
				&& this.CancellationPolicies.Except(ap.CancellationPolicies).Count() == 0
				&& this.Discounts.IsEqual(ap.Discounts)
				&& this.additionalBoards.IsEqual(ap.additionalBoards)
				&& this.NoRefund == ap.NoRefund
				)
				)
			{
				return false;
			}

			return true;
		}
	}
}
