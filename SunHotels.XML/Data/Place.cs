using System.Collections.Generic;

namespace SunHotels.XML.Data
{
    public class Place
    {  
        public List<Place> Places { get; set; }
        public Dictionary<string, Hotel> Hotels { get; set; }
        public string Id { get; set; }
        public string Description { get; set; }
        public List<PlaceCodes> Codes { get; set; }

        public Place()
        {
            Places = new List<Place>();
            Hotels = new Dictionary<string, Hotel>();
            Codes = new List<PlaceCodes>();
        }

        public override string ToString()
        {
            return this.Id;
        }

        public override int GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        public override bool  Equals(object obj)
        {
            if(obj.GetType() == typeof(Place))
            {
 	            return Id.Equals(((Place)obj).Id, System.StringComparison.InvariantCultureIgnoreCase);
            }
            else
            {
                return false;
            }
        }
    }
}
