﻿namespace SunHotels.XML.Data
{
    public class RoomConfigurationOccupancyBlocks
    {
        /// <summary>
        /// Adults / Children values are inclusive
        /// </summary>
        public int Id { get; set; }
        public int NumberOfAdults { get; set; }
        public int NumberOfChildren { get; set; }
    }
}