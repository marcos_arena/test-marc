using System;
using System.Collections.Generic;
using System.Text;

namespace SunHotels.XML.Data
{
    public class CancellationPolicy : ICloneable
    {
        private string id;

        /// <summary>
        ///  The ID of the policy
        /// </summary>
        public string Id
        {
            get { return id; }
            set { id = value; }
        }


        private string deadline_basis;
        /// <summary>
        /// Deadline basis code: "arrival"
        /// </summary>
        public string Deadline_Basis
        {
            get { return deadline_basis; }
            set { deadline_basis = value; }
        }

        private string deadline_unit;
        /// <summary>
        /// Unit code: "hours"
        /// </summary>
        public string Deadline_Unit
        {
            get { return deadline_unit; }
            set { deadline_unit = value; }
        }

        private int? deadline_value;
        /// <summary>
        /// Number of units
        /// </summary>
        public int? Deadline_Value
        {
            get { return deadline_value; }
            set {
                if (value.HasValue && value.Value < 0) throw new ApplicationException(String.Format("Cancelation policy deadline value must not be a negative value ({0}).", value.Value));
                deadline_value = value; 
            }
        }
		

        private string penalty_basis;
        /// <summary>
        /// Penalty basis property: "full_stay", "nights", "fixed_amount"
        /// </summary>
        public string Penalty_Basis
        {
            get { return penalty_basis; }
            set { penalty_basis = value; }
        }


        private int penalty_value;
        /// <summary>
        ///  Penalty fee described as follows: 
        ///
        ///     1.  If basis is �full_stay�, the fee is a percentage of the total booking price. The 
        ///         value of this attribute then specifies the number of percent. 
        /// 
        ///     2.  If basis is �nights�, the fee is a number of nightly fees. This value then 
        ///         specifies the number of nights. 
        /// 
        ///     3.  If basis is �fixed_amount�, the fee is a fixed amount. This value then 
        ///         specifies the amount.                        
        /// </summary>
        public int Penalty_Value
        {
            get { return penalty_value; }
            set { penalty_value = value; }
        }

        public override bool Equals(object obj)
        {
            if (obj.GetType() != typeof(CancellationPolicy))
            {
                return false;
            }

            CancellationPolicy compare = (CancellationPolicy)obj;

            return (this.Deadline_Basis == compare.Deadline_Basis &&
                this.Deadline_Unit == compare.Deadline_Unit &&
                this.Deadline_Value == compare.Deadline_Value &&
                this.Id == compare.Id &&
                this.Penalty_Basis == compare.Penalty_Basis &&
                this.Penalty_Value == compare.Penalty_Value);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        #region ICloneable Members

        public CancellationPolicy Clone()
        {
            return (CancellationPolicy)((ICloneable)this).Clone();
        }

        object ICloneable.Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion
    }
}
