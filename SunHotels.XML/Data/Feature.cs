﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SunHotels.XML.Data
{
    public class Feature
    {
        public String Id { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }

		public override bool Equals(object obj)
		{
			return obj != null && ((Feature)obj).Id == Id;
		}

		public override int GetHashCode()
		{
			return int.Parse(Id);
		}
    }
}
