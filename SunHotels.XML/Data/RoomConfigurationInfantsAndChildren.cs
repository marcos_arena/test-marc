﻿namespace SunHotels.XML.Data
{
    public class RoomConfigurationInfantsAndChildren
    {
        public int Id { get; set; }
        public bool InfantCountAsChild { get; set; }
        public int MaxChildAgeInclusive { get; set; }
    }
}