using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;

namespace SunHotels.XML.Data
{
    [Serializable]
	public class RoomType
    {

		public RoomType()
		{
			this.isWeeklyStay = false;	
			this.starting_days_monday = false;
			this.starting_days_tuesday = false;
			this.starting_days_wednesday = false;
			this.starting_days_thursday = false;
			this.starting_days_friday = false;
			this.starting_days_saturday = false;
			this.starting_days_sunday = false;
			this.NonRefundable = false;
		}
		public RoomType Clone()
		{
			var ms = new MemoryStream();
			var bf = new BinaryFormatter();
			bf.Serialize(ms, this);
			ms.Seek(0, SeekOrigin.Begin);
			return (RoomType)bf.Deserialize(ms);
		}

		public bool NonRefundable { get; set; }

        public bool SharedRoom { get; set; }
        public bool SharedFacilities { get; set; }


        private string description;

        public string Description
        {
            get { return description; }
            set { description = value; }
        }

        private string bookable_id;
        public string Bookable_Id
        {
            get { return bookable_id; }
            set { bookable_id = value; }
        }

		private string typeId;

		public string Type_Id
		{
			get { return typeId; }
			set { typeId = value; }
		}

        private int minStay;

        public int MinimumStay
        {
            get { return minStay; }
            set { minStay = value; }
        }
	


        private int beds;

        public int Beds
        {
            get { return beds; }
            set { beds = value; }
        }

        private int extraBeds;

        public int ExtraBeds
        {
            get { return extraBeds; }
            set { extraBeds = value; }
        }

        private bool isWeeklyStay;

        public bool IsWeeklyStay
        {
            get { return isWeeklyStay; }
            set { isWeeklyStay = value; }
        }

        private bool starting_days_monday;

        public bool Starting_Days_Monday
        {
            get { return starting_days_monday; }
            set { starting_days_monday = value; }
        }

        private bool starting_days_tuesday;

        public bool Starting_Days_Tuesday
        {
            get { return starting_days_tuesday; }
            set { starting_days_tuesday = value; }
        }

        private bool starting_days_wednesday;

        public bool Starting_Days_Wednesday
        {
            get { return starting_days_wednesday; }
            set { starting_days_wednesday = value; }
        }

        private bool starting_days_thursday;

        public bool Starting_Days_Thursday
        {
            get { return starting_days_thursday; }
            set { starting_days_thursday = value; }
        }

        private bool starting_days_friday;

        public bool Starting_Days_Friday
        {
            get { return starting_days_friday; }
            set { starting_days_friday = value; }
        }

        private bool starting_days_saturday;

        public bool Starting_Days_Saturday
        {
            get { return starting_days_saturday; }
            set { starting_days_saturday = value; }
        }

        private bool starting_days_sunday;

        public bool Starting_Days_Sunday
        {
            get { return starting_days_sunday; }
            set { starting_days_sunday = value; }
        }

        public override string ToString()
        {
            return typeId;
        }
    }
}
