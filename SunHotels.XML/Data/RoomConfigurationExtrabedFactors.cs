﻿namespace SunHotels.XML.Data
{
    public class RoomConfigurationExtrabedFactors
    {
        public int Id { get; set; }
        public int FromAgeInclusive { get; set; }
        public int ToAgeExclusive { get; set; }
        public int FromQuantityInclusive { get; set; }
        public int ToQuantityInclusive { get; set; }
        public decimal Value { get; set; }
        public string IncludedBoard { get; internal set; }
    }
}