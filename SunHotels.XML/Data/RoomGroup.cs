using System;
using System.Collections.Generic;
using System.Text;

namespace SunHotels.XML.Data
{
    public class RoomGroup
    {
        private string roomGroupId;

        public string RoomGroupId
        {
            get { return roomGroupId; }
            set { roomGroupId = value; }
        }

        protected Dictionary<string, Room> rooms;

        public Dictionary<string, Room> Rooms
        {
            get { return rooms; }
            set { rooms = value; }
        }

        public RoomGroup()
        {
            rooms = new Dictionary<string, Room>();
        }
	
    }
}
