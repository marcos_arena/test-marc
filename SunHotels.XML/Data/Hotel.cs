using System;
using System.Collections.Generic;


namespace SunHotels.XML.Data
{
    public class Hotel
    {
        public Object Extra { get; set; }
        public string Tag { get; set; }
        public string Id { get; set; }
        public string Name { get; set; }
        public AccomodationType? AccomodationType { get; set; }
        private string _headline;
        public string Headline
        {
            get { return HelpClasses.XmlHelper.StripTagsFromString(_headline); }
            set { _headline = value; }
        }

        private string _description;
        public string Description
        {
            get { return HelpClasses.XmlHelper.StripTagsFromString(_description); }
            set { _description = value; }
        }
        public string PlaceId { get; set; }
        public string Adress_Street1 { get; set; }
        public string Adress_Street2 { get; set; }
        public string Adress_Zipcode { get; set; }
        public string Adress_City { get; set; }
        public string Adress_State { get; set; }
        public string Adress_Country { get; set; }
        public string Position_Latitude { get; set; }
        public string Position_Longitude { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string Classification { get; set; }
        public bool BestBuy { get; set; }
        public List<Hotel.Image> Images { get; set; }
        public List<Feature> Features { get; set; }
        public List<Theme> Themes { get; set; }
        public List<Review> Reviews { get; set; }
        public List<Hotel.Distance> Distances { get; set; }
        public Dictionary<string, Room> Rooms { get; set; }
        public Dictionary<string, RoomGroup> RoomGroups { get; set; }
        public List<Note> Notes { get; set; }
        public bool BlockInfant { get; set; }
        public bool AdultOnly { get; set; }
        public List<MealLabel> MealLabels { get; set; }
        public List<Identifier> Identifiers { get; set; }
		public Translations Translations { get; set; }
        public List<string[]> GlobalTypes { get; set; }
        public string GiataCode { get; set; }
        public string Currency { get; set; }

        public Hotel()
        {
            this.Features = new List<Feature>();
            this.Distances = new List<Distance>();
            this.Rooms = new Dictionary<string, Room>();
            this.RoomGroups = new Dictionary<string, RoomGroup>();
            this.Images = new List<Hotel.Image>();
            this.Themes = new List<Theme>();
            this.Reviews = new List<Review>();
			this.Translations = new Translations();
            this.Identifiers = new List<Identifier>();
        }

        public class Image
        {
            public string Id { get; set; }
            public List<ImageVariant> ImageVariants { get; set; }

            public Image()
            {
                ImageVariants = new List<ImageVariant>();
            }
        }

        public class ImageVariant
        {
            public int? Width { get; set; }
            public int? Height { get; set; }
            public string URL { get; set; }
        }

        public class Distance
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public string Value { get; set; }
            public string ReferencePoint { get; set; }
            public string IataCode { get; set; }

            public Distance() { }

            public Distance(string Name, string Value)
            {
                this.Name = Name;
                this.Value = Value;
            }

			public Distance(string name, string value, string refPoint)
			:this(name, value)
			{
				this.ReferencePoint = refPoint;
			}

            public Distance(string Name, decimal Value)
            {
                this.Name = Name;
                this.Value = Value.ToString(System.Globalization.CultureInfo.InvariantCulture);
            }
			
            public Distance(string name, decimal value, string refPoint)
				: this(name, value)
			{
				this.ReferencePoint = refPoint;
			}

            public Distance(string name, decimal value, string refPoint, string iataCode)
                : this(name, value, refPoint)
            {
                this.IataCode = iataCode;
            }
        }
    }

    public enum AccomodationType { Hotel, Apartment, Villa };
}
