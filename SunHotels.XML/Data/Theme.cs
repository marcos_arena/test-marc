﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SunHotels.XML.Data
{
    public class Theme
    {
        public int Id { get; set; }
        public string Value { get; set; }
    }
}
