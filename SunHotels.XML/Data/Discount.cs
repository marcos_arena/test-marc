﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SunHotels.XML.Data
{
    public enum DiscountType
    {
        EarlyBooking,
        PayStay,
        Value,
        MinMax
    }

    public class Discount : ICloneable, IEquatable<Discount>
    {
        private DiscountType type;

        public DiscountType Type
        {
            get { return type; }
        }

        private int minDays;

        public int MinDays
        {
            get { return minDays; }
        }

        private int? maxDays;

        public int? MaxDays
        {
            get { return maxDays; }
        }

        public int? MaxPeriods
        {
            get { return maxDays == null ? null : (maxDays / minDays); }
        }

        private decimal amount;

        public decimal Amount
        {
            get { return amount; }
        }

        private decimal percent;

        public decimal Percent
        {
            get { return percent; }
        }

        private int? period_id;

        public int? Period_ID
        {
            get { return period_id; }
        }

        public DateTime? LastDate { get; set; }

        public int AccumulativeGroup { get; set; }

        public int TypeId {
            get {
                switch (Type) {
                    case DiscountType.EarlyBooking:
                        return Amount > 0 ? 4 : 3;
                    case DiscountType.PayStay:
                        return 5;
                    case DiscountType.Value:
                        return Amount > 0 ? 2 : 1;
                    case DiscountType.MinMax:
                        return 6;
                    default:
                        throw new ApplicationException("Unknown discount");
                }
            }
        }

        public bool CheckinBased { get; private set; }

        public bool IncludeMeal { get; private set; }

        public bool IncludeExtraBed { get; private set; }

        static public Discount EarlyBookingAmount(int MinDays, decimal Amount, int? Period_ID, int accumulativeGroup = 0, bool checkinBased = false, bool includeMeal = false, bool includeExtraBed = false)
        {
            if (Amount <= 0) {
                throw new ApplicationException("Amount has to be greater than zero");
            }
            return new Discount(DiscountType.EarlyBooking, MinDays, null, Amount, 0, Period_ID, accumulativeGroup, checkinBased, includeMeal, includeExtraBed);
        }

        static public Discount EarlyBookingProcent(int MinDays, decimal Percent, int? Period_ID, int accumulativeGroup = 0, bool checkinBased = false, bool includeMeal = false, bool includeExtraBed = false)
        {
            if (Percent <= 0) {
                throw new ApplicationException("Percent has to be greater than zero");
            }
            return new Discount(DiscountType.EarlyBooking, MinDays, null, 0, Percent, Period_ID, accumulativeGroup, checkinBased, includeMeal, includeExtraBed);
        }

        //static public Discount PayStay(int MinDays, decimal Amount, int accumulativeGroup = 0, bool checkinBased = false, bool includeMeal = false, bool includeExtraBed = false)
        //{
        //    return Discount.PayStay(MinDays, null, Amount, null, accumulativeGroup, checkinBased, includeMeal, includeExtraBed);
        //}

        static public Discount PayStay(int MinDays, int? MaxPeriods, decimal Amount, int? Period_ID = null, int accumulativeGroup = 0, bool checkinBased = false, bool includeMeal = false, bool includeExtraBed = false)
        {
            if (Amount <= 0) {
                throw new ApplicationException("Amount has to be greater than zero");
            }

            return new Discount(DiscountType.PayStay, MinDays, MaxPeriods * MinDays, Convert.ToDecimal(Amount), 0, Period_ID, accumulativeGroup, checkinBased, includeMeal, includeExtraBed);
        }

        //static public Discount ValueAmount(int MinDays, decimal Amount, int accumulativeGroup = 0, bool checkinBased = false, bool includeMeal = false, bool includeExtraBed = false)
        //{
        //    return Discount.ValueAmount(MinDays, Amount, null, accumulativeGroup, checkinBased, includeMeal, includeExtraBed);
        //}

        static public Discount ValueAmount(int MinDays, decimal Amount, int? Period_ID = null, int accumulativeGroup = 0, bool checkinBased = false, bool includeMeal = false, bool includeExtraBed = false)
        {
            if (Amount <= 0) {
                throw new ApplicationException("Amount has to be greater than zero");
            }
            return new Discount(DiscountType.Value, MinDays, null, Amount, 0, Period_ID, accumulativeGroup, checkinBased, includeMeal, includeExtraBed);
        }

        //static public Discount ValuePercent(int MinDays, decimal Percent, int accumulativeGroup = 0, bool checkinBased = false, bool includeMeal = false, bool includeExtraBed = false)
        //{
        //    return Discount.ValuePercent(MinDays, Percent, null, accumulativeGroup, checkinBased, includeMeal, includeExtraBed);
        //}

        static public Discount ValuePercent(int MinDays, decimal Percent, int? Period_ID = null, int accumulativeGroup = 0, bool checkinBased = false, bool includeMeal = false, bool includeExtraBed = false)
        {
            if (Percent <= 0) {
                throw new ApplicationException("Percent has to be greater than zero");
            }
            return new Discount(DiscountType.Value, MinDays, null, 0, Percent, Period_ID, accumulativeGroup, checkinBased, includeMeal, includeExtraBed);
        }

        static public Discount MinMaxPercent(int MinDays, int MaxDays, decimal Percent, int? Period_ID = null, int accumulativeGroup = 0, bool checkinBased = false, bool includeMeal = false, bool includeExtraBed = false)
        {
            if (Percent <= 0) {
                throw new ApplicationException("Percent has to be greater than zero");
            }
            return new Discount(DiscountType.MinMax, MinDays, MaxDays, 0, Percent, Period_ID, accumulativeGroup, checkinBased, includeMeal, includeExtraBed);
        }

        private Discount(DiscountType Type, int MinDays, int? MaxDays, decimal Amount, decimal Percent, int? Period_ID, int accumulativeGroup, bool checkinBased, bool includeMeal, bool includeExtraBed)
        {
            if (!(Amount > 0 || Percent > 0)) {
                throw new ApplicationException("Either Amount or Percent has to be greater than zero");
            }

            this.type = Type;
            this.minDays = MinDays;
            this.maxDays = MaxDays;
            this.amount = Amount;
            this.percent = Percent;
            this.period_id = Period_ID;
            if (this.period_id == 0)
            {
                // Since the xml schema only allows positive integers change 0 to something else.
                this.period_id = int.MaxValue;
            }
            this.AccumulativeGroup = accumulativeGroup;
            this.CheckinBased = checkinBased;
            this.IncludeMeal = includeMeal;
            this.IncludeExtraBed = includeExtraBed;
        }

        #region ICloneable Members

        public Discount Clone()
        {
            Discount result = (Discount)((ICloneable)this).Clone();
            return result;
        }

        object ICloneable.Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion

        #region IEquatable<Discount> Members

        public bool Equals(Discount other)
        {
            return (this.Amount == other.Amount &&
                this.MaxDays == other.MaxDays &&
                this.MaxPeriods == other.MaxPeriods &&
                (this.Type == DiscountType.EarlyBooking && this.LastDate.HasValue
                    ? this.LastDate == other.LastDate
                    : this.MinDays == other.MinDays) &&
                this.Percent == other.Percent &&
                this.Period_ID == other.Period_ID &&
                this.Type == other.Type &&
                this.AccumulativeGroup == other.AccumulativeGroup &&
                this.CheckinBased == other.CheckinBased &&
                this.IncludeMeal == other.IncludeMeal &&
                this.IncludeExtraBed == other.IncludeExtraBed);
        }

        #endregion

        public override bool Equals(object obj)
        {
            if (obj.GetType() != typeof(Discount)) {
                return false;
            }

            Discount compare = (Discount)obj;

            return (this.Amount == compare.Amount &&
                this.MaxDays == compare.MaxDays &&
                this.MaxPeriods == compare.MaxPeriods &&
                (this.Type == DiscountType.EarlyBooking && LastDate.HasValue
                    ? this.LastDate == compare.LastDate
                    : this.MinDays == compare.MinDays) &&
                this.Percent == compare.Percent &&
                this.Period_ID == compare.Period_ID &&
                this.Type == compare.Type &&
                this.AccumulativeGroup == compare.AccumulativeGroup &&
                this.CheckinBased == compare.CheckinBased &&
                this.IncludeMeal == compare.IncludeMeal &&
                this.IncludeExtraBed == compare.IncludeExtraBed);
        }

        public override int GetHashCode()
        {
            return (this.Amount.GetHashCode() ^
                this.MaxDays.GetHashCode() ^
                this.MaxPeriods.GetHashCode() ^
               (this.Type == DiscountType.EarlyBooking && LastDate.HasValue
                    ? this.LastDate.GetHashCode()
                    : this.MinDays.GetHashCode()) ^
                this.MinDays.GetHashCode() ^
                this.Percent.GetHashCode() ^
                this.Period_ID.GetHashCode() ^
                this.Type.GetHashCode() ^
                this.AccumulativeGroup.GetHashCode() ^
                this.CheckinBased.GetHashCode() ^
                this.IncludeMeal.GetHashCode() ^
                this.IncludeExtraBed.GetHashCode());
        }

        public static Dictionary<int, string> GetDiscountTypes() {
            return new Dictionary<int, string> {
                {1, "Value percent"},
                {2, "Value amount"},
                {3, "Early booking percent"},
                {4, "Early booking amount"},
                {5, "Paystay"},
                {6, "Minmax"}
            };
        }
    }
}
