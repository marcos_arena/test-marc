using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.IO;
using System.Xml;
using System.Diagnostics;
using System.IO.Compression;

namespace SunHotels.XML
{
    public class XMLCommunication
    {
        private string requestUrl;
        private string requestMethod = "POST";
        private string requestContentType = "application/x-www-form-urlencoded";
        private Encoding requestEncoding = null;
        private Version httpVersion = System.Net.HttpVersion.Version11;
        private string userId;
        private string password;
        private int timeOut = 30000;



	
        public string RequestUrl    
        {
            get { return requestUrl; }
            set { requestUrl = value; }
        }

        public string RequestMethod
        {
            get { return requestMethod; }
            set { requestMethod = value; }
        }

        public string RequestContentType
        {
            get { return requestContentType; }
            set { requestContentType = value; }
        }

        public Encoding RequestEncoding
        {
            get { return requestEncoding; }
            set { requestEncoding = value; }
        }
	

        public Version HttpVersion
        {
            get { return httpVersion; }
            set { httpVersion = value; }
        }




        public string UserId
        {
            get { return userId; }
            set { userId = value; }
        }



        public string Password
        {
            get { return password; }
            set { password = value; }
        }


        public int Timeout
        {
            get { return timeOut; }
            set { timeOut = value; }
        }
	


        // Constructor

        public XMLCommunication()
        {
        }

        public XMLCommunication(string requestUrl)
        {
            this.requestUrl = requestUrl;
        }


        public XmlDocument Run(string requestData)
        {
            HttpWebRequest request;
            HttpWebResponse response;
            Stream responseStream;


            requestData = requestData.Replace("\r\n","");
                

            try
            {
                // Web request
                request = (HttpWebRequest)WebRequest.Create(this.requestUrl);
                request.Method = this.requestMethod;
                request.ContentType = this.requestContentType;
                request.ProtocolVersion = this.httpVersion;
                request.Timeout = this.timeOut;
                
                
                // Web response
                int retryCount = 0;
                WebException innerException = null;


                while (!SendRequest(request, requestData, out innerException) && retryCount < 5)
                {
                    // 3*1=3, 3*4=12, 3*9=27, 3*16=48, 3*25=75, total 3+12+27+48+75=165 sek.
                    int sleepTime = 3000 * (retryCount+1) * (retryCount+1);
                    System.Threading.Thread.Sleep(sleepTime);
                    retryCount++;
                }

                if (retryCount >= 4)
                {
                    throw new Exception("The request failed 5 times, aborting", innerException);
                }
                
                retryCount = 0;
                
                while (!GetResponse(request, out response, out innerException) && retryCount < 5)
                {
                    // 3*1=3, 3*4=12, 3*9=27, 3*16=48, 3*25=75, total 3+12+27+48+75=165 sek.
                    int sleepTime = 3000 * (retryCount + 1) * (retryCount + 1);
                    System.Threading.Thread.Sleep(sleepTime);
                    retryCount++;
                }

                if(retryCount >= 4)
                {
                    throw new Exception("The request failed 5 times, aborting", innerException);
                }
                
                
                responseStream = response.GetResponseStream();

                return XmlResponse(responseStream, response);
                
                
            }
            catch (Exception ex)
            {
                throw new Exception("Error: ", ex);                
            }
        }

        private bool SendRequest( HttpWebRequest request, string requestData, out WebException exception)
        {
            try
            {
                if (RequestMethod != "GET")
                {
                    Stream requestStream = null;
                    requestStream = request.GetRequestStream();
                    Encoding enc = Encoding.GetEncoding("ISO-8859-1");
                    requestStream.Write(enc.GetBytes(requestData), 0, enc.GetBytes(requestData).Length);
                    requestStream.Close();
                }

                exception = null;
                return true;
            }
            catch (WebException ex)
            {

                exception = ex;
                return false;
            }
        }


        private bool GetResponse(HttpWebRequest request, out HttpWebResponse response, out WebException exception)
        {
            try
            {
                response = (HttpWebResponse)request.GetResponse();
                exception = null;
                return true;
            }
            catch (WebException we)
            {
                exception = we;
                response = null;
                return false;
            }
        }




        //protected abstract void XmlRequest(XmlWriter writer);

        protected virtual XmlDocument XmlResponse(Stream stream, HttpWebResponse response )
        {
            StreamReader streamReader;
            string streamResult;
            XmlDocument xmlDoc = null;

            if (response.ContentEncoding.Contains("gzip"))
            {
                stream = new GZipStream(stream, CompressionMode.Decompress);
            }
            else if (response.ContentEncoding.Contains("deflate"))
            {
                stream = new DeflateStream(stream, CompressionMode.Decompress);
            }

            streamReader = new StreamReader(stream, Encoding.GetEncoding(response.CharacterSet));
            streamResult = streamReader.ReadToEnd();

            Trace.WriteLine(streamResult);
            streamReader.Close();

            if (streamResult.Trim() == "")
            {
                streamResult = "<emptyresponse />";
            }

            xmlDoc = new XmlDocument();

            xmlDoc.LoadXml(streamResult);

            return xmlDoc;
        }
    }
}
