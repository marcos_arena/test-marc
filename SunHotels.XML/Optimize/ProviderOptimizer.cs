﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SunHotels.XML.Data;
using SunHotels.Export;
using Sunhotels.Export;

namespace SunHotels.XML.Optimize
{
    public class ProviderOptimizer : BaseOptimizer
    {
        public ProviderOptimizer(Configuration configuration) : base(configuration)
        {
            this.configuration = configuration;
        }

        public override void OptimizeHotel(Root root, Hotel hotel)
        {
            hotel.Rooms = OptimizeRooms(root, hotel.Rooms);

            foreach (RoomGroup roomGroup in hotel.RoomGroups.Values) {
                roomGroup.Rooms = OptimizeRooms(root, roomGroup.Rooms);
            }

            Data = root;
        }

        public override void OptimizeRoom(Root root, Room room, int hotelId)
        {
            throw new NotImplementedException();
        }

        private Dictionary<string, Room> OptimizeRooms(Root root, Dictionary<string, Room> rooms)
        {
            Dictionary<string, Room> tempRoomList = new Dictionary<string, Room>();

            foreach (Room room in rooms.Values) {
                AvailabilityPeriod currentPeriod = null;
                RoomType currentRoomType = null;
                Room currentRoom = null;

                foreach (AvailabilityPeriod ap in room.AvailabilityPeriods.Values) {

                    // Check for roomType, Has it changed?
                    string roomTypeId = "";

                    if (currentRoomType != null &&
                        ap.IsWeeklyStay == currentRoomType.IsWeeklyStay
                        //&& ap.MinimumStay == currentRoomType.MinimumStay
                        ) {


                    }
                    else {
                        // If roomtype is to be changed, then null out currentPeriod
                        currentPeriod = null;

                        // If weeklystay, create new roomtype
                        if (ap.IsWeeklyStay && ap.IsArrivalPossible) {
                            roomTypeId = room.TypeId + "_" + ap.MinimumStay.ToString() + "_" + ap.DateFrom.DayOfWeek.ToString().Substring(0, 3);
                        }
                        else if (ap.IsWeeklyStay && !ap.IsArrivalPossible) {
                            continue;
                        }
                        else {
                            roomTypeId = room.TypeId;
                        }

                        // Check if room exists in global roomtype-list
                        if (!root.RoomTypes.TryGetValue(roomTypeId, out currentRoomType)) {
                            currentRoomType = new RoomType();
                            currentRoomType.MinimumStay = ap.MinimumStay;
                            currentRoomType.Beds = root.RoomTypes[room.TypeId].Beds;
                            currentRoomType.Bookable_Id = root.RoomTypes[room.TypeId].Bookable_Id;
                            currentRoomType.Description = root.RoomTypes[room.TypeId].Description;
                            currentRoomType.ExtraBeds = root.RoomTypes[room.TypeId].ExtraBeds;
                            currentRoomType.IsWeeklyStay = ap.IsWeeklyStay;
                            currentRoomType.Type_Id = roomTypeId;

                            if (ap.IsWeeklyStay && ap.IsArrivalPossible) {
                                currentRoomType.Description += "(W " + ap.DateFrom.DayOfWeek.ToString().Substring(0, 3) + ")";

                                // Set starting day
                                switch (ap.DateFrom.DayOfWeek.ToString().ToLower()) {
                                    case "monday":
                                        currentRoomType.Starting_Days_Monday = true;
                                        break;

                                    case "tuesday":
                                        currentRoomType.Starting_Days_Tuesday = true;
                                        break;

                                    case "wednesday":
                                        currentRoomType.Starting_Days_Wednesday = true;
                                        break;

                                    case "thursday":
                                        currentRoomType.Starting_Days_Thursday = true;
                                        break;

                                    case "friday":
                                        currentRoomType.Starting_Days_Friday = true;
                                        break;

                                    case "saturday":
                                        currentRoomType.Starting_Days_Saturday = true;
                                        break;

                                    case "sunday":
                                        currentRoomType.Starting_Days_Sunday = true;
                                        break;
                                }
                            }

                            root.RoomTypes.Add(roomTypeId, currentRoomType);
                        }
                        else if (currentRoomType != null)
                        {
                            currentRoomType.MinimumStay = ap.MinimumStay; // ...
                        }
                    }

                    var tempRoomId = String.Format("{0}.{1}", currentRoomType.Type_Id, room.RoomId);
                    if (!tempRoomList.TryGetValue(tempRoomId, out currentRoom)) {
                        currentRoom = new Room();
                        currentRoom.RoomId = room.RoomId;
                        currentRoom.TypeId = currentRoomType.Type_Id;
                        currentRoom.Description = room.Description;
                        currentRoom.MealSupplementRestriction = room.MealSupplementRestriction;
                        currentRoom.RoomConfigurationInfantsAndChildrenId = room.RoomConfigurationInfantsAndChildrenId;
                        currentRoom.RoomConfigurationOccupancyId = room.RoomConfigurationOccupancyId;
                        currentRoom.RoomConfigurationExtrabedFactors = room.RoomConfigurationExtrabedFactors;
                        currentRoom.RoomConfigurationOccupancyBlocks = room.RoomConfigurationOccupancyBlocks;
                        tempRoomList.Add(tempRoomId, currentRoom);
                    }
                    else
                    {
                        tempRoomId = String.Format("{0}.{1}", currentRoom.TypeId, currentRoom.RoomId);
                        currentRoom = tempRoomList[tempRoomId];
                    }

                    if (currentPeriod != null && ap.Equals(currentPeriod)) {
                        currentPeriod.DateTo = ap.DateTo;
                    }
                    else {
                        currentPeriod = ap;
                        if (currentPeriod.AvailabilityAvailableUnits > 0) {
                            currentRoom.AvailabilityPeriods.Add(currentPeriod.DateFrom, currentPeriod);
                        }
                    }
                }
            }

            foreach (Room r in tempRoomList.Values) {
                if (!root.RoomTypeAvailability.ContainsKey(r.RoomId)) {
                    root.RoomTypeAvailability.Add(r.RoomId, 0);
                }

                foreach (AvailabilityPeriod ap in r.AvailabilityPeriods.Values) {
                    TimeSpan ts = new TimeSpan(ap.DateTo.Ticks - ap.DateFrom.Ticks);
                    int availability = ts.Days * ap.AvailabilityAvailableUnits;
                    root.RoomTypeAvailability[r.RoomId] += availability;
                }
            }

            return tempRoomList;
        }
    }
}
