﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SunHotels.XML.Optimize.Data
{
    internal abstract class BasePeriod
    {
        public int HotelId { get; set; }
        public int RoomId { get; set; }

        public DateTime From { get; private set; }
        public DateTime To { get; private set; }

        private BasePeriod() { }

        public BasePeriod(DateTime from, DateTime to)
        {
            From = from;
            To = to;
        }
    }
}
