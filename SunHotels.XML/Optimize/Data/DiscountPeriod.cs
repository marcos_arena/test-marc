﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SunHotels.XML.Data;

namespace SunHotels.XML.Optimize.Data
{
    internal class DiscountPeriod : BasePeriod
    {
        public DiscountSet Discounts { get; private set; }

        public DiscountPeriod(DateTime from, DateTime to, DiscountSet discounts) : base(from, to)
        {
            this.Discounts = discounts;
        }

        public static bool IsEqual(DiscountSet first, DiscountSet second)
        {
            if (first.Count != second.Count)
            {
                return false;
            }

            foreach (var item in first)
            {
                if (!second.Contains(item))
                    return false;
            }

            foreach (var item in second)
            {
                if (!first.Contains(item))
                    return false;
            }

            return true;
        }
    }
}