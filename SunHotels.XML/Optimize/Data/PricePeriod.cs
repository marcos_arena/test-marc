﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SunHotels.XML.Data;

namespace SunHotels.XML.Optimize.Data
{
    internal class PricePeriod : BasePeriod
    {
        public int BoardTypeId { get; private set; }
        public string BasePrice { get; private set; }

        public string ExtrabedAdult { get; private set; }
        public string ExtrabedChild { get; private set; }

        public List<AvailabilityPeriod.AdditionalBoard> AdditionalBoards { get; private set; }

        public PricePeriod(DateTime from, DateTime to, AvailabilityPeriod period, int extraBeds) : base(from, to)
        {
            this.BoardTypeId = period.BoardTypeId;
            this.BasePrice = period.Pricing_BasePrice;
            this.ExtrabedAdult = extraBeds > 0 && !string.IsNullOrEmpty(period.Pricing_ExtrabedAdult) ? period.Pricing_ExtrabedAdult : null;
            this.ExtrabedChild = extraBeds > 0 && !string.IsNullOrEmpty(period.Pricing_ExtrabedChild) ? period.Pricing_ExtrabedChild : null;
            this.AdditionalBoards = period.AdditionalBoards;
        }

        public static bool IsEqual(AvailabilityPeriod first, AvailabilityPeriod second)
        {
            return
                first.Pricing_BookableBoardId == second.Pricing_BookableBoardId &&
                first.Pricing_BaseIncludedBoard == second.Pricing_BaseIncludedBoard &&
                first.Pricing_BasePrice == second.Pricing_BasePrice &&
                first.Pricing_ExtrabedAdult == second.Pricing_ExtrabedAdult &&
                first.Pricing_ExtrabedChild == second.Pricing_ExtrabedChild &&
                first.AdditionalBoards.IsEqual(second.AdditionalBoards);
        }
    }
}
