﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SunHotels.XML.Data;

namespace SunHotels.XML.Optimize.Data
{
    internal class BasicAvailabilityPeriod : BasePeriod
    {
        public int AvailableUnits { get; private set; }

        public int ReleaseDays { get; private set; }

        public int MinimumStay { get; private set; }

        public BasicAvailabilityPeriod(DateTime from, DateTime to, AvailabilityPeriod period) : base(from, to)
        {
            this.AvailableUnits = period.AvailabilityAvailableUnits;
            this.ReleaseDays = period.ReleaseDays;
            this.MinimumStay = period.MinimumStay;
        }

        public static bool IsEqual(AvailabilityPeriod first, AvailabilityPeriod second) {
            return
                first.AvailabilityAvailableUnits == second.AvailabilityAvailableUnits &&
                first.ReleaseDays == second.ReleaseDays &&
                first.MinimumStay == second.MinimumStay;
        }
    }
}
