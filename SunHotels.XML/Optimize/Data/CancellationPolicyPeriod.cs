﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SunHotels.XML.Optimize.Data
{
    internal class CancellationPolicyPeriod : BasePeriod
    {
        public List<string> Policies { get; set; }

        public CancellationPolicyPeriod(DateTime from, DateTime to, List<string> policies) : base(from, to)
        {
            this.Policies = policies;
        }
    }
}