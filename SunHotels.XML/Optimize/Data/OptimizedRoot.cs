﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SunHotels.XML.Data;
using SunHotels.Export;

namespace SunHotels.XML.Optimize.Data
{
    internal class OptimizedRoot : ExportData
    {
        public SortedDictionary<int, SortedDictionary<int, List<DiscountPeriod>>> Discounts { get; private set; }
        public SortedDictionary<int, SortedDictionary<int, List<CancellationPolicyPeriod>>> Policies { get; private set; }
        public SortedDictionary<int, SortedDictionary<int, List<BasicAvailabilityPeriod>>> Availability { get; private set; }
        public SortedDictionary<int, SortedDictionary<int, List<PricePeriod>>> Prices { get; private set; }
        public SortedDictionary<int, SortedDictionary<int, RoomPriority>> RoomPriorities { get; private set; }
        public SortedList<int, SortedList<string, List<int>>> RoomGroups { get; private set; }
        public SortedDictionary<int, SortedDictionary<int, string>> RoomCurrencies { get; private set; }
        public SortedDictionary<int, SortedDictionary<int, List<Note>>> RoomNotes { get; private set; }
        public SortedDictionary<int, List<Note>> HotelNotes { get; private set; }
        public SortedDictionary<int, List<Note>> CountryNotes { get; set; }
        public SortedDictionary<int, List<Note>> DestinationNotes { get; set; }
        public SortedDictionary<int, List<Note>> ResortNotes { get; set; }

        public Dictionary<string, CancellationPolicy> CancellationsPolicies { get; set; }

        public OptimizedRoot()
        {
            Discounts = new SortedDictionary<int, SortedDictionary<int, List<DiscountPeriod>>>();
            Policies = new SortedDictionary<int, SortedDictionary<int, List<CancellationPolicyPeriod>>>();
            Availability = new SortedDictionary<int, SortedDictionary<int, List<BasicAvailabilityPeriod>>>();
            Prices = new SortedDictionary<int, SortedDictionary<int, List<PricePeriod>>>();
            RoomPriorities = new SortedDictionary<int, SortedDictionary<int, RoomPriority>>();
            RoomGroups = new SortedList<int, SortedList<string, List<int>>>();
            RoomCurrencies = new SortedDictionary<int, SortedDictionary<int, string>>();
            RoomNotes = new SortedDictionary<int, SortedDictionary<int, List<Note>>>();
            HotelNotes = new SortedDictionary<int, List<Note>>();
        }
    }
}
