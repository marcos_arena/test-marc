﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SunHotels.XML.Optimize.Data
{
    internal class RoomPriority
    {
        public string RoomTypeId { get; private set; }
        public int Priority { get; private set; }

        internal RoomPriority(string roomTypeId, int priority)
        {
            this.RoomTypeId = roomTypeId;
            this.Priority = priority;
        }
    }
}
