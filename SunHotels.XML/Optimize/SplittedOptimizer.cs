﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SunHotels.XML.Data;
using SunHotels.XML.Optimize.Data;
using SunHotels.Export;
using Sunhotels.Export;

namespace SunHotels.XML.Optimize
{
    public class SplittedOptimizer : BaseOptimizer
    {
        private Dictionary<string, int> maxRoomTypePriority;
        private HashSet<string> policiesToBeRemoved = new HashSet<string>();

        internal new OptimizedRoot Data
        {
            get { return base.Data as OptimizedRoot; }
            set { base.Data = value; }
        }

        public SplittedOptimizer(Configuration configuration) : base(configuration)
        {
            Data = new OptimizedRoot();
        }

        public override void OptimizeHotel(Root root, Hotel hotel)
        {
            maxRoomTypePriority = new Dictionary<string, int>();

            var hotelId = int.Parse(hotel.Id);

            foreach (var key in hotel.Rooms.Keys.ToList()) {
                var room = hotel.Rooms[key];

                OptimizeRoom(root, room, hotelId);

                hotel.Rooms.Remove(key);
            }

            if (hotel.Notes != null)
                Data.HotelNotes.Add(hotelId, hotel.Notes);

            // Sort and add roomgroups if avaliable
            if (hotel.RoomGroups.Any()) {
                var roomGroups =
                    new SortedList<string, List<int>>(hotel.RoomGroups.ToDictionary(
                        rg => rg.Key,
                        rg => rg.Value.Rooms.Select(r => int.Parse(r.Value.RoomId)).OrderBy(r => r).ToList()
                    ));

                Data.RoomGroups.Add(hotelId, roomGroups);
            }
        }

        public override void OnComplete(Root root) {
            Data.CancellationsPolicies = root.CancellationsPolicies;

            foreach (var policy in policiesToBeRemoved) {
                Data.CancellationsPolicies.Remove(policy);
            }

            if (root.CountryNotes != null)
            {
                Data.CountryNotes = new SortedDictionary<int, List<Note>>(root.CountryNotes);
            }

            if (root.DestinationNotes != null)
            {
                Data.DestinationNotes = new SortedDictionary<int, List<Note>>(root.DestinationNotes);
            }

            if (root.ResortNotes != null)
            {
                Data.ResortNotes = new SortedDictionary<int, List<Note>>(root.ResortNotes);
            }
        }
        
        public override void OptimizeRoom(Root root, Room room, int hotelId)
        {
            if (room.AvailabilityPeriods == null || !room.AvailabilityPeriods.Any())
                return;

            int roomId = int.Parse(room.RoomId);
            var currency = room.Currency ?? configuration.Currency ?? "EUR";

            AddToDict(hotelId, roomId, Data.Discounts, GetOptimizedDiscountPeriods(room));
            AddToDict(hotelId, roomId, Data.Policies, GetOptimizedCancellationPolicyPeriods(room, root, currency));
            AddToDict(hotelId, roomId, Data.Availability, GetOptimizedAvailabilityPeriods(room));
            AddToDict(hotelId, roomId, Data.Prices, GetOptimizedPricePeriods(room, root.RoomTypes[room.TypeId]));

            int priority = maxRoomTypePriority.ContainsKey(room.TypeId)
                ? maxRoomTypePriority[room.TypeId] += 1
                : maxRoomTypePriority[room.TypeId]  = 1;

            AddToDict(hotelId, roomId, Data.RoomPriorities, new RoomPriority(room.TypeId, priority));
            AddToDict(hotelId, roomId, Data.RoomCurrencies, currency);
            AddToDict(hotelId, roomId, Data.RoomNotes, room.Notes);
        }

        private void AddToDict<T>(int hotelId, int roomId, SortedDictionary<int, SortedDictionary<int, List<T>>> dict, List<T> periods)
        {
            if (periods == null || !periods.Any())
                return;

            if (!dict.ContainsKey(hotelId))
                dict.Add(hotelId, new SortedDictionary<int, List<T>>() { { roomId, periods } });
            else
                dict[hotelId].Add(roomId, periods);
        }

        private void AddToDict<T>(int hotelId, int roomId, SortedDictionary<int, SortedDictionary<int, T>> dict, T data)
        {
            if (data == null)
                return;

            if (!dict.ContainsKey(hotelId))
                dict.Add(hotelId, new SortedDictionary<int, T>() { { roomId, data } });
            else
                dict[hotelId].Add(roomId, data);
        }

        private List<DiscountPeriod> GetOptimizedDiscountPeriods(Room room)
        {
            var result = new List<DiscountPeriod>();

            var discounts = room.AvailabilityPeriods
                .Where(ap => ap.Value.Discounts != null && ap.Value.Discounts.Any())
                .Select(ap => new { Date = ap.Key, Discounts = ap.Value.Discounts }).ToList();

            if (!discounts.Any())
                return result;

            var previous = discounts.First();
            var from = previous.Date;

            foreach (var d in discounts) {
                foreach (var item in d.Discounts) {
                    if (item.Type == DiscountType.EarlyBooking)
                        item.LastDate = d.Date.AddDays(-item.MinDays);
                }

                if ((d.Date - previous.Date).Days > 1 || !DiscountPeriod.IsEqual(previous.Discounts, d.Discounts)) {
                    result.Add(new DiscountPeriod(from, previous.Date, previous.Discounts));
                    from = d.Date;
                }

                previous = d;
            }

            result.Add(new DiscountPeriod(from, previous.Date, previous.Discounts));

            return result;
        }

        private List<CancellationPolicyPeriod> GetOptimizedCancellationPolicyPeriods(Room room, Root root, string currency)
        {
            var result = new List<CancellationPolicyPeriod>();

            var policies = room.AvailabilityPeriods
                .Select(ap => new { Date = ap.Key, Policies = ap.Value.CancellationPolicies }).ToList();

            if (!policies.Any())
                return result;

            var previous = policies.First();
            var from = previous.Date;

            foreach (var cp in policies.Skip(1))
	        {
                if (!previous.Policies.IsEqual(cp.Policies)) {
                    AddPolicyCurrencies(previous.Policies, root, currency);

                    result.Add(new CancellationPolicyPeriod(from, previous.Date, previous.Policies));
                    from = cp.Date;
                }

                previous = cp;
	        }

            AddPolicyCurrencies(previous.Policies, root, currency);
            result.Add(new CancellationPolicyPeriod(from, previous.Date, previous.Policies));

            return result;
        }

        // Adds currency to cancallation policyId:s for cancellations with fixed amount 
        private void AddPolicyCurrencies(List<string> policies, Root root, string currency)
        {
            for (int i = policies.Count-1; i >= 0; i--) {
                var policyId = policies[i];
                var policy = root.CancellationsPolicies[policyId];

                if (policy.Penalty_Basis == "fixed_amount") {
                    var newPolicyId = policyId + "." + currency;

                    // Replace policy Id
                    policies[i] = newPolicyId;

                    // Add a new cancellation policy if it do not exist yet
                    if (!root.CancellationsPolicies.ContainsKey(newPolicyId)) {
                        var newPolicy = policy.Clone();
                        newPolicy.Id = newPolicyId;
                        root.CancellationsPolicies[newPolicyId] = newPolicy;

                        // Remove unused policy (later)
                        policiesToBeRemoved.Add(policyId);
                    }
                }
            }
        }

        private List<BasicAvailabilityPeriod> GetOptimizedAvailabilityPeriods(Room room)
        {
            var result = new List<BasicAvailabilityPeriod>();

            var avaliability = room.AvailabilityPeriods.Select(ap => new { Date = ap.Key, Availability = ap.Value }).ToList();
            var previous = avaliability.First();
            var from = previous.Date;

            foreach (var ap in avaliability.Skip(1)) {
                if ((ap.Date - previous.Date).Days > 1 || !BasicAvailabilityPeriod.IsEqual(previous.Availability, ap.Availability)) {
                    result.Add(new BasicAvailabilityPeriod(from, previous.Date, previous.Availability));
                    from = ap.Date;
                }

                previous = ap;
            }

            result.Add(new BasicAvailabilityPeriod(from, previous.Date, previous.Availability));

            return result;
        }

        private List<PricePeriod> GetOptimizedPricePeriods(Room room, RoomType type)
        {
            var result = new List<PricePeriod>();

            var avaliability = room.AvailabilityPeriods.Select(ap => new { Date = ap.Key, Availability = ap.Value }).ToList();
            var previous = avaliability.First();
            var from = previous.Date;

            foreach (var ap in avaliability.Skip(1)) {
                if ((ap.Date - previous.Date).Days > 1 || !PricePeriod.IsEqual(previous.Availability, ap.Availability)) {
                    result.Add(new PricePeriod(from, previous.Date, previous.Availability, type.ExtraBeds));
                    from = ap.Date;
                }

                previous = ap;
            }

            result.Add(new PricePeriod(from, previous.Date, previous.Availability, type.ExtraBeds));

            return result;
        }
    }
}