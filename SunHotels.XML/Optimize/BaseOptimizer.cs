﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SunHotels.XML.Data;
using SunHotels.Export;
using Sunhotels.Export;

namespace SunHotels.XML.Optimize
{
    public abstract class BaseOptimizer
    {
        protected Configuration configuration;

        internal ExportData Data { get; set; }

        protected BaseOptimizer(Configuration configuration)
        {
            this.configuration = configuration;
        }

        public abstract void OptimizeHotel(Root root, Hotel room);

        public abstract void OptimizeRoom(Root root, Room room, int hotelId);

        public void OptimizeAvailabilityPeriods(Root root)
        {
            OptimizeAvailabilityPeriods(root, root.Places);
        }

        public virtual void OnComplete(Root root) { }

        private void OptimizeAvailabilityPeriods(Root root, List<Place> places)
        {
            foreach (Place place in places) {
                OptimizeAvailabilityPeriods(root, place.Places);

                foreach (var hotel in place.Hotels) {
                    OptimizeHotel(root, hotel.Value);
                }
            }
        }
    }
}
