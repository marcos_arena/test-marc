﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using System.Globalization;

namespace SunHotels.XML.HelpClasses
{
    public static class JsonExtensions
    {
        public static void WriteProperty(this JsonTextWriter writer, string name, string value)
        {
            writer.WritePropertyName(name);
            writer.WriteValue(value);
        }

        public static void WriteProperty(this JsonTextWriter writer, string name, int? value)
        {
            if (value.HasValue) {
                writer.WritePropertyName(name);
                writer.WriteValue(value.Value.ToString(CultureInfo.InvariantCulture));
            }
        }

        public static void WritePropertyDecimal(this JsonTextWriter writer, string name, string value, int decimals)
        {
            if (!string.IsNullOrEmpty(value)) {
                writer.WritePropertyName(name);
                writer.WriteValue(Math.Round(decimal.Parse(value), decimals).ToString(CultureInfo.InvariantCulture));
            }
        }

        public static void WriteProperty(this JsonTextWriter writer, string name, int value)
        {
            writer.WritePropertyName(name);
            writer.WriteValue(value.ToString(CultureInfo.InvariantCulture));
        }

        public static void WriteProperty(this JsonTextWriter writer, string name, decimal value)
        {
            writer.WritePropertyName(name);
            writer.WriteValue(value.ToString(CultureInfo.InvariantCulture));
        }
		
        public static void WriteProperty(this JsonTextWriter writer, string name, bool value)
        {
            writer.WritePropertyName(name);
            writer.WriteValue(value ? "1" : "0");
        }

        public static void WriteStartArray(this JsonTextWriter writer, string name)
        {
            writer.WritePropertyName(name);
            writer.WriteStartArray();
        }

        public static void WriteStartObject(this JsonTextWriter writer, string name)
        {
            writer.WritePropertyName(name);
            writer.WriteStartObject();
        }
    }
}
