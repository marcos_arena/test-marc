using System.Text.RegularExpressions;
using System.Xml;

namespace SunHotels.XML.HelpClasses
{
    public class XmlHelper
    {
		public static string GetString(XmlNode node, string xPath)
		{
		    if (node.SelectSingleNode(xPath) != null)
            {
                return node.SelectSingleNode(xPath).InnerText;
            }
		    return "";
		}


        public static decimal ConvertCurrency(decimal value, string valueCurrency, string returnCurrency)
		{
            return PriceConverter.PriceConverter.ConvertCurrency(value, valueCurrency, returnCurrency);
		}

        private static readonly Regex HtmlRegex = new Regex("<.*?>", RegexOptions.Compiled);

        /// <summary>
        /// Remove HTML from string.
        /// </summary>
        public static string StripTagsFromString(string source)
        {
            return source != null ? HtmlRegex.Replace(source.Replace("<br/>", " ").Replace("<br />", " "), string.Empty) : null;
        }
    }
}
