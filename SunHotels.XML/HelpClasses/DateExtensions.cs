﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SunHotels.XML.HelpClasses
{
    public static class DateExtensions
    {
        public static string Format(this DateTime date)
        {
            return string.Format("{0:yyyy-MM-dd}", date);
        }

        public static string Format(this DateTime? date)
        {
            return date != null ? date.Value.Format() : null;
        }
    }
}
