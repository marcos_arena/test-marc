using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using SunHotels.Export;
using Sunhotels.Export;
using Newtonsoft.Json;
using System.Linq;

namespace SunHotels.XML.HelpClasses
{

	#region public abstract class Log

	/// <remarks>
	/// Abstract class to dictate the format for the logs that our
	/// logger will use.
	/// </remarks>
	public abstract class Log
	{
		/// <value>Available message severities</value>
		public enum MessageType
		{
			/// <value>Informational message</value>
			Informational = 1,
			/// <value>Failure audit message</value>
			Failure = 2,
			/// <value>Warning message</value>
			Warning = 3,
			/// <value>Error message</value>
			Error = 4
		}

		public abstract void RecordNewLine(bool dashed);
		public abstract void RecordMessage(Exception message, MessageType severity);
		public abstract void RecordMessage(string message, MessageType severity);
	}

	#endregion

	/// <remarks>
	/// Managing class to provide the interface for and control
	/// application logging.  It utilizes the logging objects in
	/// ErrorLog.Logs to perform the actual logging as configured.
	/// </remarks>
	public class Logger
	{
		/// <value>Available log types.</value>
		public enum LogTypes
		{
			/// <value>Log to a file location.</value>
			File = 1

			// Add aditional log types here.
		}

		// Internal logging object
		private Log _logger;

		// Internal log type
		private LogTypes _logType;

		/// <value></value>
		public LogTypes LogType
		{
			get { return this._logType; }
			set
			{
				// Set the Logger to the appropriate log when
				// the type changes.
				switch (value)
				{
					// Add more cases if new types added.

					case LogTypes.File:
						this._logger = new FileLog(_configuration);
						_logType = LogTypes.File;
						break;
					default:
						this._logger = new FileLog(_configuration);
						_logType = LogTypes.File;
						break;
				}
			}
		}

		// Internal configuration
		internal Configuration _configuration;

		/// <summary>
		/// Constructor
		/// </summary>
		public Logger(Configuration configuration)
		{
			this._configuration = configuration;
			this.LogType = LogTypes.File;
		}

		/// <summary>
		/// Log an empty line.
		/// </summary>
		public void RecordNewLine(bool dashed)
		{
			this._logger.RecordNewLine(dashed);
		}

		/// <summary>
		/// Log an exception.
		/// </summary>
		/// <param name="message">Exception to log.</param>
		/// <param name="severity">Error severity level.</param>
		public void RecordMessage(Exception message, Log.MessageType severity)
		{
			this._logger.RecordMessage(message, severity);
		}

		/// Log a message.
		/// </summary>
		/// <param name="message">Message to log.</param>
		/// <param name="severity">Error severity level.</param>
		public void RecordMessage(string message, Log.MessageType severity)
		{
			this._logger.RecordMessage(message, severity);
		}

		#region private class FileLog

		/// <remarks>
		/// Log messages to a file location.
		/// </remarks>
		private class FileLog : Log
		{
			// Internal log file name value
			private string _fileName = "";

			/// <value>Get or set the log file name</value>
			public string FileName
			{
				get { return this._fileName; }
				set { this._fileName = value; }
			}

			// Internal log file location value
			private string _fileLocation = "";

			/// <value>Get or set the log file directory location</value>
			public string FileLocation
			{
				get { return this._fileLocation; }
				set
				{
					this._fileLocation = value;
					// Verify a '\' exists on the end of the location
					if (this._fileLocation.LastIndexOf("\\") != (this._fileLocation.Length - 1))
					{
						this._fileLocation += "\\";
					}
				}
			}

			/// <summary>
			/// Constructor
			/// </summary>
			public FileLog(Configuration configuration)
			{
				this.FileLocation = configuration.LogOutput;
				if (!Directory.Exists(this.FileLocation))
				{
					Directory.CreateDirectory(this.FileLocation);
				}
				this.FileName = string.Format("{0} {1}.log", configuration.ProviderName, DateTime.Now.ToString("yyyyMMdd HHmmss")); ;
			}

			/// <summary>
			/// Log an exception.
			/// </summary>
			/// <param name="Message">Exception to log. </param>
			/// <param name="Severity">Error severity level. </param>
			public override void RecordNewLine(bool dashed)
			{
				FileStream fileStream = null;
				StreamWriter writer = null;

				try
				{
					fileStream = new FileStream(this._fileLocation + this._fileName, FileMode.OpenOrCreate, FileAccess.Write, FileShare.ReadWrite);
					writer = new StreamWriter(fileStream);

					// Set the file pointer to the end of the file
					writer.BaseStream.Seek(0, SeekOrigin.End);

					// Force the write to the underlying file
					writer.WriteLine(String.Empty);

					if (dashed)
					{
						writer.WriteLine("----------------------------------------------------------------------------------------------------");
					}

					writer.Flush();
				}
				catch (Exception ex)
				{
					throw new ApplicationException(String.Format("Unable to open/create log file: {0}", this._fileLocation + this._fileName), ex);
				}
				finally
				{
					if (writer != null) writer.Close();
				}
			}

			/// <summary>
			/// Log an exception.
			/// </summary>
			/// <param name="Message">Exception to log. </param>
			/// <param name="Severity">Error severity level. </param>
			public override void RecordMessage(Exception message, Log.MessageType severity)
			{
				string errorMessage = String.Format("{0}, {1}, {2}", message.Message, message.Source, message.StackTrace);
				this.RecordMessage(errorMessage, severity);
			}

			/// <summary>
			/// Log a message.
			/// </summary>
			/// <param name="Message">Message to log. </param>
			/// <param name="Severity">Error severity level. </param>
			public override void RecordMessage(string message, Log.MessageType severity)
			{
				lock (this)
				{
					FileStream fileStream = null;
					StreamWriter writer = null;
					StringBuilder sb = new StringBuilder();

					try
					{
						fileStream = new FileStream(this._fileLocation + this._fileName, FileMode.OpenOrCreate, FileAccess.Write);
						writer = new StreamWriter(fileStream);

						// Set the file pointer to the end of the file
						writer.BaseStream.Seek(0, SeekOrigin.End);

						// Create the message
						sb.Append(System.DateTime.Now.ToString()).Append(",").Append(severity.ToString()).Append(",").Append(message);

						// Force the write to the underlying file
						writer.WriteLine(sb.ToString());
						writer.Flush();
					}
					catch (Exception ex)
					{
						throw new ApplicationException(String.Format("Unable to open/create log file: {0}", this._fileLocation + this._fileName), ex);
					}
					finally
					{
						if (writer != null) writer.Close();
					}
				}
			}
		}

		#endregion
	}

    #region New Log

    [Serializable]
    public class LogHierarchy
    {
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Description { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<string> Statistics { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string LogPath { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string AgentId { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public DateTime StartTime { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string TotalTime { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore, PropertyName = "p")]
        public Dictionary<string, LogHierarchyChild> Children { get; set; }
        public void AddStatistics(string msg)
        {
            if (Statistics == null) Statistics = new List<string>();
            if (!Statistics.Contains(msg)) Statistics.Add(msg);
        }
        public void Add(params string[] items)
        {
            if (items?.Length == 0) return;
            if (string.IsNullOrEmpty(items[0])) return;
            if (Children == null) Children = new Dictionary<string, LogHierarchyChild>();
            if (!Children.ContainsKey(items[0]))
            {
                Children.Add(items[0], new LogHierarchyChild());
            }
            Children[items[0]].Add(true, items.Skip(1).ToArray());
        }
        public LogHierarchy(string description, string logPath, string agentId)
        {
            this.Description = description;
            this.LogPath = logPath;
            this.AgentId = agentId;
            this.StartTime = DateTime.Now;
        }
        public void SerializeToFile()
        {
            try
            {
                if (string.IsNullOrEmpty(LogPath)) return;

                if (!Directory.Exists(LogPath)) Directory.CreateDirectory(LogPath);

                TotalTime = DateTime.Now.Subtract(this.StartTime).ToString();
                var json = JsonConvert.SerializeObject(this);
                var logPath = Path.Combine(LogPath, Description + "_" + AgentId + "_" + StartTime.ToString("yyyyMMddHHmm") + ".txt");
                System.IO.File.WriteAllText(logPath, json);

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }
    }
    [Serializable]
    public class LogHierarchyChild
    {
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore, PropertyName = "p")]
        public Dictionary<string, LogHierarchyChild> Children { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore, PropertyName = "c")]
        public string SingleName { get; set; }
        public void Add(bool forceAddToChildren = false, params string[] items)
        {
            if (items?.Length == 0) return;
            if (string.IsNullOrEmpty(items[0])) return;
            if (!forceAddToChildren && items.Length == 1)
            {
                SingleName = items[0];
                return;
            }
            if (Children == null) Children = new Dictionary<string, LogHierarchyChild>();
            if (!Children.ContainsKey(items[0])) Children.Add(items[0], new LogHierarchyChild());

            Children[items[0]].Add(false, items.Skip(1).ToArray());
        }
    }

    #endregion
}
