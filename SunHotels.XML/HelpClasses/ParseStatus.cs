﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;

namespace SunHotels.XML.HelpClasses
{
	/// <summary>
	/// 
	/// </summary>
	public class ParseStatus
	{
		private bool _current { get; set; }

		public bool Current { get { return _current; } set { _current = value; } }

		public ParseStatus()
		{
			_current = true;
		}

		public void Clear()
		{
			_current = true;
		}

		public string ParseString(string s)
		{
			this._current = String.IsNullOrEmpty(s) ? false : _current;
			return s;
		}

		public string ParseString(string s, bool isMadatory)
		{
			if (isMadatory)
			{
				return ParseString(s);
			}
			else
			{
				string sNull = null;

				if (!String.IsNullOrEmpty(s))
				{
					sNull = s;
				}

				return sNull;
			}

		}

		public int ParseInt(string s)
		{
			int i = 0;
			this._current = int.TryParse(s, out i).Equals(false) ? false : _current;
			return i;
		}

		public int? ParseInt(string s, bool isMadatory)
		{
			if (isMadatory)
			{
				return ParseInt(s);
			}
			else
			{
				int? iNull = null;

				if (!String.IsNullOrEmpty(s))
				{
					int i = 0;
					if (int.TryParse(s, out i))
					{
						iNull = i;
					}
					else
					{
						this._current = false;
					}
				}

				return iNull;
			}
		}

		public decimal ParseDecimal(string s)
		{
			decimal d = 0;
			this._current = decimal.TryParse(s, NumberStyles.Any, CultureInfo.InvariantCulture, out d).Equals(false) ? false : _current;
			return d;
		}

		public decimal? ParseDecimal(string s, bool isMadatory)
		{
			if (isMadatory)
			{
				return ParseDecimal(s);
			}
			else
			{
				decimal? dNull = null;

				if (!String.IsNullOrEmpty(s))
				{
					decimal d = 0;
					if (decimal.TryParse(s, NumberStyles.Any, CultureInfo.InvariantCulture, out d))
					{
						dNull = d;
					}
					else
					{
						this._current = false;
					}
				}

				return dNull;
			}
		}

		public double ParseDouble(string s)
		{
			double d = 0;
			this._current = double.TryParse(s, NumberStyles.Any, CultureInfo.InvariantCulture, out d).Equals(false) ? false : _current;
			return d;
		}

		public double? ParseDouble(string s, bool isMadatory)
		{
			if (isMadatory)
			{
				return ParseDouble(s);
			}
			else
			{
				double? dNull = null;

				if (!String.IsNullOrEmpty(s))
				{
					double d = 0;
					if (double.TryParse(s, NumberStyles.Any, CultureInfo.InvariantCulture, out d))
					{
						dNull = d;
					}
					else
					{
						this._current = false;
					}
				}

				return dNull;
			}
		}

		public DateTime ParseDateTime(string s)
		{
			DateTime dt = new DateTime();
			this._current = DateTime.TryParse(s, out dt).Equals(false) ? false : _current;
			return dt;
		}

		public DateTime? ParseDateTime(string s, bool isMadatory)
		{
			if (isMadatory)
			{
				return ParseDateTime(s);
			}
			else
			{
				DateTime? dtNull = null;

				if (!String.IsNullOrEmpty(s))
				{
					DateTime dt = new DateTime();
					if (DateTime.TryParse(s, CultureInfo.InvariantCulture, DateTimeStyles.AdjustToUniversal, out dt))
					{
						dtNull = dt;
					}
					else
					{
						this._current = false;
					}
				}

				return dtNull;
			}
		}
	}
}
