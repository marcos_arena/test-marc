﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Globalization;

namespace SunHotels.XML.HelpClasses
{
    public static class XmlExtensions
    {
        public static void WriteElement(this XmlTextWriter writer, string name, int? value)
        {
            if (value.HasValue)
                writer.WriteElementString(name, value.Value.ToString(CultureInfo.InvariantCulture));
        }

        public static void WriteElement(this XmlTextWriter writer, string name, int value)
        {
            writer.WriteElementString(name, value.ToString(CultureInfo.InvariantCulture));
        }

        public static void WriteElement(this XmlTextWriter writer, string name, Decimal value)
        {
            writer.WriteElementString(name, value.ToString(CultureInfo.InvariantCulture));
        }

        public static void WriteElement(this XmlTextWriter writer, string name, bool value)
        {
            writer.WriteElementString(name, value ? "1" : "0");
        }

        public static void WriteAttribute(this XmlTextWriter writer, string name, int? value)
        {
            if (value.HasValue)
                writer.WriteAttributeString(name, value.Value.ToString(CultureInfo.InvariantCulture));
        }

        public static void WriteAttribute(this XmlTextWriter writer, string name, DateTime value)
        {
            writer.WriteAttributeString(name, value.ToString(CultureInfo.InvariantCulture));
        }

        public static void WriteAttributeDecimal(this XmlTextWriter writer, string name, string value, int decimals)
        {
            if (!string.IsNullOrEmpty(value))
                writer.WriteAttributeString(name, Math.Round(decimal.Parse(value), decimals).ToString(CultureInfo.InvariantCulture));
        }

        public static void WriteAttribute(this XmlTextWriter writer, string name, string value)
        {
            if (!string.IsNullOrEmpty(value))
                writer.WriteAttributeString(name, value);
        }
    }
}
