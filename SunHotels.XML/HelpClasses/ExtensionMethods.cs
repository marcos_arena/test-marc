﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SunHotels.XML.Data;

namespace SunHotels.XML
{
    public static class ExtensionMethods
    {
        public static bool IsEqual<T>(this IEnumerable<T> lhs, IEnumerable<T> rhs) where T : IEquatable<T>
        {
            if (lhs.Count() != rhs.Count())
            {
                return false;
            }

            return lhs.Except(rhs).Count() == 0;
        }

        private static String[] MealOrder = {"none", "breakfast", "half_board", "full_board", "all_inclusive"};

        public static void RemoveRedundantMeals(this List<AvailabilityPeriod.AdditionalBoard> additionalBoards, string includedMeal)
        {
            additionalBoards.RemoveAll(m => MealOrder.TakeWhile(mo => mo != includedMeal).Contains(m.Type));
        }
        public static string ToValidDecimal(this string value)
        {
            if (string.IsNullOrEmpty(value))
                return "";
            var tmp = value.Replace(" ", "").Replace("-,", "-0,");
            if (tmp[0] == ',')
                tmp = "0" + tmp;
            return tmp;
        }
    }
}
