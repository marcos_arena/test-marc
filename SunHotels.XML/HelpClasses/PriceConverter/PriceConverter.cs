﻿using SunHotels.Framework.Config;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SunHotels.XML.HelpClasses.PriceConverter
{
	public class PriceConverter
	{
		public static Dictionary<string, decimal> rateTable = new Dictionary<string, decimal>();
		public static decimal ConvertCurrency(decimal value, string valueCurrency, string returnCurrency)
		{
			if (value == 0 || valueCurrency == returnCurrency)
			{
				return value;
			}

			string rateKey = String.Format("{0}_{1}", valueCurrency, returnCurrency);

			if (!rateTable.ContainsKey(rateKey))
			{
				var res = new CurrencyDataContext(Config.Instance["ConnectionStrings.Currency"]).CurrencyConverter(1, valueCurrency, returnCurrency);
				rateTable.Add(rateKey, res.First().converted_value.Value);
			}

			return rateTable[rateKey] * value;
		}
	}
}
