﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Schema;

namespace SunHotels.XML.Exporters
{
	public class XmlWritterManager
	{
		public class XmlWritterValidator : XmlTextWriter
		{
			public XmlWritterManager Manager;
			public XmlWriter WriterBase { get; set; }
			Stack<string> stack = new Stack<string>();
			bool disposed = false;
            bool backwardHotelValidationCompatibility = false;
            string xmlnamespace = string.Empty;
            string xsdfilename = string.Empty;

			public XmlWritterValidator(XmlWritterManager manager, XmlWriter writerBase)
				:base(new MemoryStream(), UTF8Encoding.UTF8)
			{
				Manager = manager;
				WriterBase = writerBase;
			}
			public override void WriteStartElement(string prefix, string localName, string ns)
			{
				base.WriteStartElement(prefix, localName, ns);
				stack.Push(localName);
			}

            public void SetHotelValidationBackwardCompatibility(bool value, string xmlNamespace, string xsdFilename)
            {
                backwardHotelValidationCompatibility = value;
                xmlnamespace = xmlNamespace;
                xsdfilename = xsdFilename;
            }

            public void SetHotelValidationBackwardCompatibility(bool value)
            {
                backwardHotelValidationCompatibility = value;
                xmlnamespace = string.Empty;
                xsdfilename = string.Empty;
            }
            string GetXsdFile()
            {
                return xsdfilename.Replace(".xsd", "_Hotel.xsd");
            }

			public override void WriteEndElement()
			{
				base.WriteEndElement();

				stack.Pop();
				if (stack.Count() == 0)
				{
					disposed = true;
					this.Flush();
					this.BaseStream.Seek(0, SeekOrigin.Begin);
					var xml = new StreamReader(this.BaseStream).ReadToEnd();
					var errs = Validate(xml);
					if(errs.Count() == 0){
						this.BaseStream.Seek(0, SeekOrigin.Begin);
                        WriterBase.WriteRaw(ClearNamespaceFromValidation(xml));
						// Reset stream
						this.BaseStream.SetLength(0);
					}else if(this.Manager.OnInvalidXML != null)
						this.Manager.OnInvalidXML(xml, errs);
				}
			}

            string ClearNamespaceFromValidation(string xml)
            {
                if (backwardHotelValidationCompatibility)
                {
                    return xml.Replace(" xmlns=\"" + xmlnamespace + "\"", string.Empty);    
                }

                return xml;
			}

			public IEnumerable<ValidationEventArgs> Validate(string xml)
			{
				
				List<ValidationEventArgs> errors = new List<ValidationEventArgs>();
				XmlReaderSettings settings = new XmlReaderSettings();
				settings.CloseInput = true;
				settings.ValidationEventHandler += (sender, e) =>
				{
					errors.Add(e);
				};

				settings.ValidationType = ValidationType.Schema;
				settings.ConformanceLevel = ConformanceLevel.Fragment;
				settings.Schemas = this.Manager.XmlSchemaSet;

                if (backwardHotelValidationCompatibility)
                {
                    settings.Schemas = new XmlSchemaSet();
                    settings.Schemas.Add(null, GetXsdFile()); //settings.Schemas.Add(null, "http://xml.dev.sunhotels.net/schemas/HotelValidation.xsd");
                }
				settings.ValidationFlags =
					XmlSchemaValidationFlags.ReportValidationWarnings |
					XmlSchemaValidationFlags.ProcessIdentityConstraints |
					XmlSchemaValidationFlags.ProcessInlineSchema |
					XmlSchemaValidationFlags.ProcessSchemaLocation;

				using (XmlReader validatingReader = XmlReader.Create(new XmlTextReader(xml, XmlNodeType.Element, null), settings))
				{
					// read and validate the rest of the file
					while (validatingReader.Read()) ;
				}
				return errors;
			}

		}

		public XmlSchemaSet XmlSchemaSet { get; set; }
		public delegate void InvalidXMLHandler(string xml, IEnumerable<ValidationEventArgs> errors);
		public InvalidXMLHandler OnInvalidXML { get; set;}

		public XmlWritterManager(string schema)
		{
			XmlSchemaSet = new XmlSchemaSet();
			XmlSchemaSet.Add(null, schema);

		}

		public XmlTextWriter Validate(XmlTextWriter writter)
		{
			return new XmlWritterValidator(this, writter);
		}
	}
}
