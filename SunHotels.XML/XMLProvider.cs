using System;
using System.Collections.Generic;
using SunHotels.XML.Data;
using System.Linq;
using SunHotels.Export;
using SunHotels.XML.Optimize;
using SunHotels.XML.Exporters;
using Sunhotels.Export;
using SunHotels.Providers.ProviderCommon.Mapping;
using SunHotels.XML.HelpClasses;
using System.Data.SqlClient;
using System.Data;

namespace SunHotels.XML
{
    public abstract class XMLProvider
	{
		protected Root root = new Root();
		private Logger log = null;
		public Logger Logger { get { return log; } }
		protected Configuration configuration;
		public static string providerName = "";

		ProviderDataMapping _DataMapping = null;
		/// <summary>
		/// Manages data mapping
		/// <remarks>Override to supply your own DataMapper</remarks>
		/// </summary>
		public virtual IProviderDataMapping DataMapper
		{
			get
			{
				return _DataMapping;
			}
		}

		private BaseOptimizer optimizer;
		protected virtual BaseOptimizer Optimizer
		{
			get
			{
				if (optimizer != null)
					return optimizer;

				switch (configuration.Optimizer)
				{
					case ConfigurationOptimizer.splitted:
						return optimizer = new SplittedOptimizer(configuration);
					default:
						return optimizer = new ProviderOptimizer(configuration);
				}
			}
		}

		private BaseFileExporter exporter;
		protected virtual BaseFileExporter Exporter
		{
			get
			{
				if (exporter != null)
					return exporter;

				switch (configuration.Exporter)
				{
					case ConfigurationExporter.splittedxml:
						return exporter = new XmlSplittedFileExporter(configuration);
					case ConfigurationExporter.json:
						return exporter = new JsonExporter(configuration);
					default:
						return exporter = new XmlExporter(configuration);
				}
			}
		}

		public XMLProvider() { }

		public XMLProvider(Configuration config)
		{
			configuration = config;
			log = new Logger(config);

		}

		/// <summary>
		/// Called when the configuration object is set and before start parsing any data
		/// </summary>
		public virtual void InitializeProvider(Configuration config)
		{
			this.configuration = config;
			providerName = config.Provider;
			this.root.Version = config.Version;
			if (log == null && config.LogOutput != null)
				log = new Logger(config);
			// Create DataMapper if not override
			if (_DataMapping == null && this.GetType().GetProperty("DataMapper").DeclaringType == typeof(XMLProvider))
				_DataMapping = new ProviderDataMapping(config.ProviderName);

			UpdateConfiguration(config);
		}

		#region Mapped Hotels and Rooms

		Dictionary<string, int> mappedHotels = null;
		/// <summary>
		/// Based on the build type defines if the hote should be mapped
		/// </summary>
		/// <param name="hotelId"></param>
		/// <returns></returns>
		public virtual bool ImportHotel(string hotelId) {
			if (!configuration.Content.type.Equals(Sunhotels.Export.ConfigurationContentType.all))
			{
				if (mappedHotels == null)
					mappedHotels = MappedHotels();
				if (configuration.Content.type.Equals(Sunhotels.Export.ConfigurationContentType.mapped))
					return mappedHotels.ContainsKey(hotelId);
				if (configuration.Content.type.Equals(Sunhotels.Export.ConfigurationContentType.unmapped))
					return !mappedHotels.ContainsKey(hotelId);
			}
			return true;
		}

		public virtual Dictionary<string, int> MappedHotels()
		{
			var mappedHotels = new Dictionary<string, int>();

			var query_hotel = @"SELECT hot.id, hpr.XMLhotelid
                                FROM hotel as hot
                                    INNER JOIN vHotelStatus as hst ON hst.Id = hot.id
                                    INNER JOIN HotelProviders as hpr ON hpr.hotelid = hot.id
                                    INNER JOIN xmlProvider as xpr ON xpr.providerid = hpr.XMLProviderid
                                WHERE
                                    (hot.showonsunhotels = 1 OR hot.showonsunhotels IS NULL)
                                    AND (hot.XML = 1)
                                    AND (hst.HotelDeleted = 0)
                                    AND (xpr.providername = '" + configuration.Provider + @"')
                                ORDER BY hot.id";

			using (var connection = new SqlConnection(SunHotels.Framework.Config.Config.Instance["ConnectionStrings.Main"]))
			{
				using (var command = new SqlCommand())
				{
					command.Connection = connection;
					command.CommandText = query_hotel;
					using (var dataTable = new DataTable())
					{
						using (var sqlDataAdapter = new SqlDataAdapter())
						{
							sqlDataAdapter.SelectCommand = command;
							sqlDataAdapter.Fill(dataTable);
						}

						if (dataTable.Rows.Count > 0)
						{
							for (var i = 0; i < dataTable.Rows.Count; i++)
							{
								var row = dataTable.Rows[i];

                                var xmlHotelId = (string)row["XMLhotelid"];
                                var hotelId = (int)row["id"];

                                if (mappedHotels.ContainsKey(xmlHotelId))
                                {
                                    var errMsg = string.Format("Error, duplicated mapping in Hotelbeds: {0} is mapped with {1} and {2}", xmlHotelId, mappedHotels[xmlHotelId], hotelId);
                                    Console.Write(errMsg);
                                    throw new ApplicationException(errMsg);
                                }                                    

                                mappedHotels.Add(xmlHotelId, hotelId);
							}
						}
					}
				}
			}
			return mappedHotels;
		}

		public virtual Dictionary<int, Dictionary<string, int>> MappedRooms(IEnumerable<int> hotelsId)
		{
			var result = new Dictionary<int, Dictionary<string, int>>();

			if (hotelsId.Any())
			{
				var query_rooms = @"SELECT r.id, r.hotelid, r.xmlroomid
                                    FROM rooms as r
                                        INNER JOIN xmlProvider as xpr ON xpr.providerid = r.xmlprovider
                                    WHERE
                                        r.xmlroom = 1
                                        AND r.hotelid IN (" + String.Join<int>(",", hotelsId.ToArray()) + @")
                                        AND (xpr.providername = '" + configuration.Provider + @"')
                                    ORDER BY r.hotelid, r.id";

				using (var connection = new SqlConnection(SunHotels.Framework.Config.Config.Instance["ConnectionStrings.Main"]))
				{
					using (var command = new SqlCommand())
					{
						command.Connection = connection;
						command.CommandText = query_rooms;
						using (var dataTable = new DataTable())
						{
							using (var sqlDataAdapter = new SqlDataAdapter())
							{
								sqlDataAdapter.SelectCommand = command;
								sqlDataAdapter.Fill(dataTable);
							}

							if (dataTable.Rows.Count > 0)
							{
								for (var i = 0; i < dataTable.Rows.Count; i++)
								{
									var row = dataTable.Rows[i];
									var hotelId = (int)row["hotelid"];
									var roomId = (int)row["id"];
									var xmlRoomId = (string)row["xmlroomid"];
									if (!result.ContainsKey(hotelId))
									{
										result.Add(hotelId, new Dictionary<string, int>());
									}
									if (!result[hotelId].ContainsKey(xmlRoomId))
									{
										result[hotelId].Add(xmlRoomId, roomId);
									}
								}
							}
						}
					}
				}
			}

			return result;
		}

		#endregion

		public void StartGathering(Configuration config)
		{
			
			root.AffectedStartDateAdjustment = config.AffectedStartDateAdjustment;
			root.AffectedEndDateAdjustment = config.AffectedEndDateAdjustment;

			getProviderDefinition(root);
			getPlaceCodeTypes(root);
			getFeatureTypes(root);
			getThemeTypes(root);
			getDistanceTypes(root);
			getData(root);

			ProcessContentType(root);

			Optimizer.OnComplete(root);

			Exporter.Write(Optimizer.Data ?? root);

			if (config.AutoValidateOutput)
				ValidateFiles();
		}

		/// <summary>
		/// Validates data against selected content type
		/// </summary>
		/// <remarks>
		/// For mapped product all static content will be rmeoved.
		/// For unmapped product only one rate will be present per roomtype
		/// For all product no actio is performed.</remarks>
		/// <param name="root"></param>
		private void ProcessContentType(Root root)
		{
			switch (configuration.Content.type)
			{
				case ConfigurationContentType.mapped:
					ProcessMappedContent(root);
					break;
				case ConfigurationContentType.unmapped:
					ProcessUnMappedContent(root);
					break;
			}
		}

		/// <summary>
		/// Keep only one rate per room
		/// </summary>
		/// <param name="root"></param>
		private void ProcessUnMappedContent(Root root) {
            // remove all the product which is mapped
		    foreach (var place in root.AllPlaces()) {
		        foreach (var hotelKey in place.Hotels.Keys.ToArray()) {
		            if (!ImportHotel(hotelKey)) { place.Hotels.Remove(hotelKey); }
		        }
		    }
            root.AllHotels()
                .SelectMany(h => h.Rooms)
                .All(room =>
                {
                    if (room.Value.AvailabilityPeriods.Any())
                    {
                        var ap = room.Value.AvailabilityPeriods.First().Value;
                        ap.DateTo = ap.DateFrom;
                        ap.AvailabilityAvailableUnits = 1;
                        ap.MinimumStay = 10;

                        foreach (var key in room.Value.AvailabilityPeriods.Skip(1).Select(kv => kv.Key).ToArray())
                            room.Value.AvailabilityPeriods.Remove(key);
                    }
                    return true;
                });
		}

		/// <summary>
		/// Remove static content
		/// </summary>
		/// <param name="root"></param>
		private void ProcessMappedContent(Root root) {
            // remove all the product which is not mapped
            foreach (var place in root.AllPlaces()) {
                foreach (var hotelKey in place.Hotels.Keys.ToArray()) {
                    if (!ImportHotel(hotelKey)) { place.Hotels.Remove(hotelKey); }
                }
            }

			foreach (var h in root.AllHotels())
			{
				// Simple values
				// Collections
				if (h.Distances != null)
					h.Distances.Clear();
				if (h.Features != null)
					h.Features.Clear();
				if (h.Identifiers != null)
					h.Identifiers.Clear();
				if (h.Images != null)
					h.Images.Clear();
				if (h.MealLabels != null)
					h.MealLabels.Clear();
				// h.Notes <- Keep! Important
				if (h.Reviews != null)
					h.Reviews.Clear();
				// h.RoomGroups <- Keep, Important
				if (h.Themes != null)
					h.Themes.Clear();
				if (h.Translations != null)
					h.Translations = new Translations();
			}
		}

		/// <summary>
		/// Validates genetared xml files
		/// </summary>
		public void ValidateFiles()
		{
			Exporter.Validate();
		}

		protected virtual void UpdateConfiguration(Configuration configuration) { }

		protected abstract void getProviderDefinition(Root root);
		protected abstract void getData(Root root);
		protected virtual void getPlaceCodeTypes(Root root) { }
		protected virtual void getThemeTypes(Root root)
		{
			root.Themes.Add(new Theme() { Id = 1, Value = "luxury" });
			root.Themes.Add(new Theme() { Id = 2, Value = "budget" });
			root.Themes.Add(new Theme() { Id = 4, Value = "spa" });
			root.Themes.Add(new Theme() { Id = 8, Value = "family" });
			root.Themes.Add(new Theme() { Id = 16, Value = "skiing" });
			root.Themes.Add(new Theme() { Id = 32, Value = "environment" });
		}
		protected virtual void getFeatureTypes(Root root)
		{
			root.Features.Add(new Feature() { Id = "1", Name = "airconditioning" });
			root.Features.Add(new Feature() { Id = "4", Name = "elevator" });
			root.Features.Add(new Feature() { Id = "3", Name = "bar" });
			root.Features.Add(new Feature() { Id = "10", Name = "telephone" });
			root.Features.Add(new Feature() { Id = "6", Name = "childrens_pool" });
			root.Features.Add(new Feature() { Id = "5", Name = "pool" });
			root.Features.Add(new Feature() { Id = "8", Name = "safe" });
			root.Features.Add(new Feature() { Id = "9", Name = "sea_view" });
			root.Features.Add(new Feature() { Id = "2", Name = "balcony" });
			root.Features.Add(new Feature() { Id = "7", Name = "restaurant" });
			root.Features.Add(new Feature() { Id = "11", Name = "tv" });
			root.Features.Add(new Feature() { Id = "12", Name = "wireless internet" });
		}
		protected virtual void getDistanceTypes(Root root)
		{
			root.Distance.Add(new Hotel.Distance() { Id = 2, Name = "center" });
			root.Distance.Add(new Hotel.Distance() { Id = 3, Name = "airport" });
			root.Distance.Add(new Hotel.Distance() { Id = 1, Name = "beach" });
		}

		/// <summary>
		/// Logs a new entry on the log file
		/// </summary>
		/// <param name="severity"></param>
		/// <param name="message"></param>
		/// <param name="args"></param>
		protected void LogEntry(Log.MessageType severity, string message, params object[] args)
		{
			log.RecordMessage(string.Format(message, args), severity);
		}

        protected async void LogEntryAndConsoleOutput(Log.MessageType severity, string message, params object[] args)
        {
            var msg = string.Format(message, args);
            log.RecordMessage(msg, severity);
            await Console.Out.WriteLineAsync(msg);
        }
    }
}
