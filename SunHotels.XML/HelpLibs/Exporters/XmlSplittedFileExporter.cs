﻿using System;
using System.Collections.Generic;
using System.Xml;
using SunHotels.Export;
using SunHotels.XML.Data;
using SunHotels.XML.HelpClasses;
using SunHotels.XML.Optimize.Data;
using Sunhotels.Export;

namespace SunHotels.XML.Exporters
{
    public class XmlSplittedFileExporter : BaseFileExporter
    {
        private OptimizedRoot data;

        public XmlSplittedFileExporter(Configuration configuration) : base(configuration) { }

        public override void Write(ExportData data)
        {
            this.data = data as OptimizedRoot;

            CreateXmlFile(
                WriteHotelTemplate<List<DiscountPeriod>>("discounts_cache", configuration.DiscountCacheXSD.Value, this.data.Discounts, CreateDiscountXml, true, WriteDiscountTypes),
                configuration.DiscountCacheOutput, true);

            CreateXmlFile(
				WriteHotelTemplate<List<CancellationPolicyPeriod>>("policies_cache", configuration.CancellationPolicyCacheXSD.Value, this.data.Policies, CreateCancellationPolicyXml, false, WriteCancellationPolicyTypes),
                configuration.CancellationPolicyCacheOutput, true);

            CreateXmlFile(
				WriteHotelTemplate<List<BasicAvailabilityPeriod>>("availability_cache", configuration.AvailabilityCacheXSD.Value, this.data.Availability, CreateAvailabilityXml),
                configuration.AvailabilityCacheOutput, true);

            CreateXmlFile(
				WriteHotelTemplate<List<PricePeriod>>("prices_cache", configuration.PriceCacheXSD.Value, this.data.Prices, CreatePriceXml, true, WriteBoardTypes),
                configuration.PriceCacheOutput, true);

            CreateXmlFile(
				WriteHotelTemplate<RoomPriority>("room_priorities_cache", configuration.RoomPrioritiesXSD.Value, this.data.RoomPriorities, CreateRoomPrioritiesXml),
                configuration.RoomPrioritiesOutput, true);

            CreateXmlFile(
				CreateRoomGroupsXml("room_groups_cache", configuration.RoomGroupsXSD.Value, this.data.RoomGroups),
                configuration.RoomGroupsOutput, true);

            CreateXmlFile(
				WriteHotelTemplate<List<Note>>("room_notes_cache", configuration.RoomNotesXSD.Value, this.data.RoomNotes, CreateRoomNotesXml),
                configuration.RoomNotesOutput, true);

            CreateXmlFile(
                CreateHotelNotesXml("hotel_notes_cache", configuration.HotelNotesXSD.Value, this.data.HotelNotes),
                configuration.HotelNotesOutput, true);
        }

        public override void Validate()
        {
			ValidateXmlFile(configuration.DiscountCacheOutput, configuration.DiscountCacheXSD.Value);
			ValidateXmlFile(configuration.CancellationPolicyCacheOutput, configuration.CancellationPolicyCacheXSD.Value);
			ValidateXmlFile(configuration.AvailabilityCacheOutput, configuration.AvailabilityCacheXSD.Value);
			ValidateXmlFile(configuration.PriceCacheOutput, configuration.PriceCacheXSD.Value);
			ValidateXmlFile(configuration.RoomPrioritiesOutput, configuration.RoomPrioritiesXSD.Value);
			ValidateXmlFile(configuration.RoomGroupsOutput, configuration.RoomGroupsXSD.Value);
			ValidateXmlFile(configuration.HotelNotesOutput, configuration.HotelNotesXSD.Value);
			ValidateXmlFile(configuration.RoomNotesOutput, configuration.RoomNotesXSD.Value);
        }

        private Action<XmlTextWriter> WriteHotelTemplate<T>(string rootName, string ns, SortedDictionary<int, SortedDictionary<int, T>> hotels, Action<XmlTextWriter, T> writeFunc, bool showCurrency = false, params Action<XmlTextWriter>[] additionalWriteFuncs)
        {
            return writer => {
                writer.WriteStartElement(rootName, ns);

                foreach (var func in additionalWriteFuncs)
                    func(writer);

                writer.WriteStartElement("hotels");
                foreach (var hotel in hotels) {
                    writer.WriteStartElement("hotel");
                    writer.WriteAttributeString("id", hotel.Key.ToString());

                    foreach (var room in hotel.Value) {
                        writer.WriteStartElement("room");
                        writer.WriteAttributeString("id", room.Key.ToString());

                        if (showCurrency) {
                            writer.WriteAttributeString("currency", data.RoomCurrencies[hotel.Key][room.Key]);
                        }

                        writeFunc(writer, room.Value);

                        writer.WriteEndElement();
                    }

                    writer.WriteEndElement();
                }
                writer.WriteEndElement();

                writer.WriteEndElement();
            };
        }

        private void CreateDiscountXml(XmlTextWriter writer, List<DiscountPeriod> discounts)
        {
            writer.WriteStartElement("discount_periods");

            foreach (var period in discounts)
	        {
                writer.WriteStartElement("period");
                writer.WriteAttributeString("from", period.From.Format());
                writer.WriteAttributeString("to", period.To.Format());

                foreach (var discount in period.Discounts) {
                    writer.WriteStartElement("discount");
                    writer.WriteAttribute("type_id", discount.TypeId);
                    writer.WriteAttribute("group", discount.AccumulativeGroup);
                    writer.WriteAttribute("period_id", discount.Period_ID);

                    switch (discount.Type) {
                        case DiscountType.EarlyBooking:
                            writer.WriteElementString("last_date", discount.LastDate.Value.Format());

                            if (discount.Amount > 0)
                                writer.WriteElement("amount", discount.Amount);
                            else
                                writer.WriteElement("percent", discount.Percent);

                            break;
                        case DiscountType.Value:
                            writer.WriteElement("days_requried", discount.MinDays);
                            writer.WriteElement("days_maximum", discount.MaxPeriods);

                            if (discount.Amount > 0)
                                writer.WriteElement("amount", discount.Amount);
                            else
                                writer.WriteElement("percent", discount.Percent);

                            break;
                        case DiscountType.MinMax:
                            writer.WriteElement("days_requried", discount.MinDays);
                            writer.WriteElement("days_maximum", discount.MaxDays);
                            writer.WriteElement("percent", discount.Percent);

                            break;

                        case DiscountType.PayStay:
                            writer.WriteElement("days_requried", discount.MinDays);
                            writer.WriteElement("days", discount.Amount);
                            writer.WriteElement("max_periods", discount.MaxPeriods);

                            break;
                    }

                    writer.WriteElement("checkinbased", discount.CheckinBased);
                    writer.WriteElement("include_meal", discount.IncludeMeal);
                    writer.WriteElement("include_extrabed", discount.IncludeExtraBed);

                    writer.WriteEndElement();
                }
                
                writer.WriteEndElement();
	        }

            writer.WriteEndElement();
        }

        private void CreateCancellationPolicyXml(XmlTextWriter writer, List<CancellationPolicyPeriod> cancellationPolicyPeriods) {
            writer.WriteStartElement("cancellation_policy_periods");

            foreach (var cancellationPolicy in cancellationPolicyPeriods) {
                writer.WriteStartElement("period");
                writer.WriteAttributeString("from", cancellationPolicy.From.Format());
                writer.WriteAttributeString("to", cancellationPolicy.To.Format());

                foreach (var id in cancellationPolicy.Policies) {
                    writer.WriteStartElement("policy");
                    writer.WriteAttributeString("id", id);
                    writer.WriteEndElement();
                }
                
                writer.WriteEndElement();
            }

            writer.WriteEndElement();
        }

        private void CreateAvailabilityXml(XmlTextWriter writer, List<BasicAvailabilityPeriod> availabilityPeriods)
        {
            writer.WriteStartElement("availability_periods");

            foreach (var period in availabilityPeriods) {
                writer.WriteStartElement("period");
                writer.WriteAttributeString("from", period.From.Format());
                writer.WriteAttributeString("to", period.To.Format());

                writer.WriteElement("available_units", period.AvailableUnits);
                writer.WriteElement("release_days", period.ReleaseDays);
                writer.WriteElement("minimum_stay", period.MinimumStay);

                writer.WriteEndElement();
            }

            writer.WriteEndElement();
        }

        private void CreatePriceXml(XmlTextWriter writer, List<PricePeriod> pricePeriods)
        {
            writer.WriteStartElement("price_periods");

            foreach (var period in pricePeriods) {
                writer.WriteStartElement("period");
                writer.WriteAttributeString("from", period.From.Format());
                writer.WriteAttributeString("to", period.To.Format());

                writer.WriteStartElement("base");
                writer.WriteAttribute("meal_type_id", period.BoardTypeId);
                writer.WriteAttributeString("price", period.BasePrice);
                writer.WriteEndElement();

                if (period.ExtrabedAdult != null || period.ExtrabedChild != null) {
                    writer.WriteStartElement("extrabed");
                    writer.WriteAttribute("adult", period.ExtrabedAdult);
                    writer.WriteAttribute("child", period.ExtrabedChild);
                    writer.WriteEndElement();
                }

                if (period.AdditionalBoards.Count > 0) {
                    writer.WriteStartElement("additional_meals");

                    foreach (AvailabilityPeriod.AdditionalBoard ab in period.AdditionalBoards) {
                        writer.WriteStartElement("meal");

                        writer.WriteAttribute("type_id", ab.TypeId);
                        writer.WriteAttributeDecimal("adult", ab.Adult, 2);
                        writer.WriteAttributeDecimal("child", ab.Child, 2);

                        writer.WriteEndElement();
                    }

                    writer.WriteEndElement();
                }

                writer.WriteEndElement();
            }

            writer.WriteEndElement();
        }

        private void CreateRoomPrioritiesXml(XmlTextWriter writer, RoomPriority roomPriority) {
            writer.WriteAttribute("priority", roomPriority.Priority);
            writer.WriteAttribute("room_type_id", roomPriority.RoomTypeId);
        }

        public Action<XmlTextWriter> CreateRoomGroupsXml(string rootName, string ns, SortedList<int, SortedList<string, List<int>>> hotels)
        {
            return writer => {
                writer.WriteStartElement(rootName, ns);

                writer.WriteStartElement("hotels");
                foreach (var hotel in hotels) {
                    writer.WriteStartElement("hotel");
                    writer.WriteAttributeString("id", hotel.Key.ToString());

                    foreach (var roomGroups in hotel.Value) {
                        writer.WriteStartElement("room_group");
                        
                        foreach (var roomId in roomGroups.Value) {
                            writer.WriteStartElement("room");
                            writer.WriteAttribute("id", roomId);
                            writer.WriteEndElement();
                        }

                        writer.WriteEndElement();
                    }

                    writer.WriteEndElement();
                }
                writer.WriteEndElement();

                writer.WriteEndElement();
            };
        }

        private void CreateRoomNotesXml(XmlTextWriter writer, List<Note> notes)
        {
            foreach (var note in notes) {
                writer.WriteStartElement("note");
                writer.WriteAttribute("id", note.Id);
                writer.WriteAttribute("from", note.FromDate.Format());
                writer.WriteAttribute("to", note.ToDate.Format());
                writer.WriteEndElement();
            }
        }

        public Action<XmlTextWriter> CreateHotelNotesXml(string rootName, string ns, SortedDictionary<int, List<Note>> hotels)
        {
            return writer => {
                writer.WriteStartElement(rootName, ns);

                writer.WriteStartElement("hotels");
                foreach (var hotel in hotels) {
                    writer.WriteStartElement("hotel");
                    writer.WriteAttributeString("id", hotel.Key.ToString());

                    foreach (var note in hotel.Value) {
                        writer.WriteStartElement("note");

                        writer.WriteAttribute("id", note.Id);
                        writer.WriteAttribute("from", note.FromDate.Format());
                        writer.WriteAttribute("to", note.ToDate.Format());

                        writer.WriteEndElement();
                    }

                    writer.WriteEndElement();
                }
                writer.WriteEndElement();

                writer.WriteEndElement();
            };
        }

        #region Additional data

        private void WriteCancellationPolicyTypes(XmlTextWriter writer) {
            writer.WriteStartElement("cancellation_policies");

            foreach (CancellationPolicy cp in data.CancellationsPolicies.Values) {
                writer.WriteStartElement("cancellation_policy");
                writer.WriteAttribute("id", cp.Id);

                // Skip deadline element if Deadline_Value is null. This policy is always active.
                if (cp.Deadline_Value != null) {
                    writer.WriteStartElement("deadline");
                    writer.WriteAttribute("basis", cp.Deadline_Basis);
                    writer.WriteAttribute("unit", cp.Deadline_Unit);
                    writer.WriteAttribute("value", cp.Deadline_Value);
                    writer.WriteEndElement();
                }

                writer.WriteStartElement("penalty");
                writer.WriteAttribute("basis", cp.Penalty_Basis);
                writer.WriteAttribute("value", cp.Penalty_Value);
                writer.WriteEndElement();

                writer.WriteEndElement();
            }

            writer.WriteEndElement();
        }

        private void WriteBoardTypes(XmlTextWriter writer)
        {
            writer.WriteStartElement("meal_types");

            foreach (var boardType in AvailabilityPeriod.AdditionalBoard.GetBoardTypes()) {
                writer.WriteStartElement("meal_type");
                writer.WriteAttribute("id", boardType.Key);
                writer.WriteAttribute("name", boardType.Value);
                writer.WriteEndElement();
            }

            writer.WriteEndElement();
        }

        private void WriteDiscountTypes(XmlTextWriter writer)
        {
            writer.WriteStartElement("discount_types");

            foreach (var discountType in Discount.GetDiscountTypes()) {
                writer.WriteStartElement("discount_type");
                writer.WriteAttribute("id", discountType.Key);
                writer.WriteAttribute("name", discountType.Value);
                writer.WriteEndElement();
            }

            writer.WriteEndElement();
        }

        #endregion
    }
}