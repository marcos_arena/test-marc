﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Xml;
using SunHotels.Export;
using SunHotels.XML.Data;
using SunHotels.XML.HelpClasses;
using Sunhotels.Export;
using SunHotels.XML.BusinessRules;

namespace SunHotels.XML.Exporters
{
    public class XmlExporter : BaseFileExporter
    {
        private Root root;

        public XmlExporter(Configuration configuration) : base(configuration) { }
        XmlWritterManager cacheXmlWritterManager;

        public override void Write(ExportData data)
        {
            root = data as Root;
            var log = new Logger(configuration);
            // Apply business rule before writting
            if (configuration.Version >= 160)
            {
                new BusinessRulesValidator(log, root).MakeItCompliant();
            }

            cacheXmlWritterManager = new XmlWritterManager(configuration.CacheXSD.Value);
            cacheXmlWritterManager.OnInvalidXML = (xml, errs) =>
            {
                System.Diagnostics.Debug.WriteLine(xml);
                errs.All(e =>
                {
                    log.RecordMessage(string.Format("Hotel dropped due XSD validation: {0}", e.Message), Log.MessageType.Error);
                    return true;
                });
                log.RecordMessage(xml, Log.MessageType.Error);
            };
            if (configuration.AutoWritePlacesXML)
                CreateXmlFile(WritePlacesXML, configuration.PlacesOutput, false);
            if (configuration.AutoWriteCacheXML)
                CreateXmlFile(WriteAvailabilityCacheXML, configuration.CacheOutput, true);

            if (!String.IsNullOrEmpty(configuration.CountryNotesOutput))
            {
                CreateXmlFile(
                    CreateDestinationCommonNotesXml("country_notes_cache", "countries", "country", configuration.CountryNotesXSD.Value, root.CountryNotes)
                    , configuration.CountryNotesOutput, true
                );
            }
            if (!String.IsNullOrEmpty(configuration.DestinationNotesOutput))
            {
                CreateXmlFile(
                    CreateDestinationCommonNotesXml("destination_notes_cache", "destinations", "destination", configuration.DestinationNotesXSD.Value, root.DestinationNotes)
                    , configuration.DestinationNotesOutput, true
                );
            }
            if (!String.IsNullOrEmpty(configuration.ResortNotesOutput))
            {
                CreateXmlFile(
                    CreateDestinationCommonNotesXml("resort_notes_cache", "resorts", "resort", configuration.ResortNotesXSD.Value, root.ResortNotes)
                    , configuration.ResortNotesOutput, true
                );
            }
        }

        public override void Validate()
        {
            if (configuration.AutoWritePlacesXML)
                ValidateXmlFile(configuration.PlacesOutput, configuration.PlacesXSD.Value);
            if (configuration.AutoWriteCacheXML)
                ValidateXmlFile(configuration.CacheOutput, configuration.CacheXSD.Value);

            if (!String.IsNullOrEmpty(configuration.CountryNotesOutput))
            {
                ValidateXmlFile(configuration.CountryNotesOutput, configuration.CountryNotesXSD.Value);
            }
            if (!String.IsNullOrEmpty(configuration.DestinationNotesOutput))
            {
                ValidateXmlFile(configuration.DestinationNotesOutput, configuration.DestinationNotesXSD.Value);
            }
            if (!String.IsNullOrEmpty(configuration.ResortNotesOutput))
            {
                ValidateXmlFile(configuration.ResortNotesOutput, configuration.ResortNotesXSD.Value);
            }
        }

        #region Places

        private void WritePlacesXML(XmlTextWriter writer)
        {
            writer.WriteStartElement("sunhotels_cache_file", configuration.PlacesXSD.@namespace);
            // writer.WriteAttributeString("SchemaVersion", "1.0");
            if (configuration.Version >= 160)
            {
                writer.WriteAttributeString("xmlns", "xsi", "http://www.w3.org/2000/xmlns/", System.Xml.Schema.XmlSchema.InstanceNamespace);
                writer.WriteAttributeString("xsi", "schemaLocation", System.Xml.Schema.XmlSchema.InstanceNamespace, String.Format("{0} {1}", configuration.PlacesXSD.@namespace, configuration.PlacesXSD.Value));
            }

            WriteProviderElement(writer, configuration.PlacesXSD.@namespace);

            if (configuration.Version >= 130)
            {
                WritePlaceCodeTypesElement(writer, root.PlaceCodeTypes);
            }

            WritePlacesElement(writer, root.Places);
            writer.WriteEndElement();
        }

        private void WriteProviderElement(XmlTextWriter writer, string ns)
        {
            writer.WriteStartElement("provider_definition", ns);
            writer.WriteElementString("name", ns, root.ProviderDefinition.Name);
            writer.WriteElementString("currency", ns, root.ProviderDefinition.Currency);
            writer.WriteEndElement();
        }

        private void WritePlaceCodeTypesElement(XmlTextWriter writer, List<string> codeTypes)
        {
            if (codeTypes.Count > 0)
            {
                writer.WriteStartElement("code_types", configuration.PlacesXSD.@namespace);

                foreach (var codeType in codeTypes)
                {
                    writer.WriteElementString("code_type", configuration.PlacesXSD.@namespace, codeType);
                }

                writer.WriteEndElement();
            }
        }

        private void WritePlacesElement(XmlWriter writer, List<Place> places)
        {
            if (places.Count == 0)
                return;

            writer.WriteStartElement("places", configuration.PlacesXSD.@namespace);

            foreach (var place in places)
            {
                writer.WriteStartElement("place", configuration.PlacesXSD.@namespace);
                writer.WriteElementString("id", configuration.PlacesXSD.@namespace, place.Id);
                writer.WriteElementString("description", configuration.PlacesXSD.@namespace, place.Description);
                if (configuration.Version >= 130)
                {
                    WritePlaceCodesElement(writer, place.Codes);
                }

                WritePlacesElement(writer, place.Places);
                writer.WriteEndElement();
            }

            writer.WriteEndElement();
        }

        private void WritePlaceCodesElement(XmlWriter writer, List<PlaceCodes> codes)
        {
            var nonEmptyCodes = codes.Where(x => !String.IsNullOrEmpty(x.Value)).ToArray();
            if (!nonEmptyCodes.Any())
                return;

            writer.WriteStartElement("codes", configuration.PlacesXSD.@namespace);

            foreach (var code in nonEmptyCodes)
            {
                writer.WriteStartElement("code", configuration.PlacesXSD.@namespace);
                writer.WriteAttributeString("type", code.Type);
                writer.WriteAttributeString("value", code.Value);
                writer.WriteEndElement();
            }

            writer.WriteEndElement();
        }

        #endregion

        #region Availability

        private void WriteAvailabilityCacheXML(XmlTextWriter writer)
        {
            writer.WriteStartElement("sunhotels_cache_file", configuration.CacheXSD.@namespace);
            // writer.WriteAttributeString("SchemaVersion", "1.0");
            if (configuration.Version >= 160)
            {
                writer.WriteAttributeString("xmlns", "xsi", "http://www.w3.org/2000/xmlns/", System.Xml.Schema.XmlSchema.InstanceNamespace);
                writer.WriteAttributeString("xsi", "schemaLocation", System.Xml.Schema.XmlSchema.InstanceNamespace, String.Format("{0} {1}", configuration.CacheXSD.@namespace, configuration.CacheXSD.Value));
            }

            if (root.AffectedStartDateAdjustment.HasValue)
            {
                writer.WriteAttributeString("affected_date_start", DateTime.Now.Date.AddDays(root.AffectedStartDateAdjustment.Value).ToString("yyyy-MM-dd"));
            }

            if (root.AffectedEndDateAdjustment.HasValue)
            {
                writer.WriteAttributeString("affected_date_end", DateTime.Now.Date.AddDays(root.AffectedEndDateAdjustment.Value).ToString("yyyy-MM-dd"));
            }

            WriteProviderElement(writer, configuration.CacheXSD.@namespace);

            #region Passenger definitions
            writer.WriteStartElement("passenger_definitions", configuration.CacheXSD.@namespace);

            writer.WriteStartElement("child", configuration.CacheXSD.@namespace);
            writer.WriteAttributeString("max_age", "11");
            writer.WriteEndElement();

            writer.WriteEndElement();
            #endregion

            if (configuration.Version >= 130)
            {
                WriteFeatureTypesElement(writer, root.Features);
                WriteThemeTypesElement(writer, root.Themes);
                WriteDistanceTypesElement(writer, root.Distance);
            }

            WriteRoomTypeElement(writer);
            WriteCancellationPoliciesElement(writer);

            writer.WriteStartElement("hotels", configuration.CacheXSD.@namespace);
            WriteHotelPlaces(writer, root.Places);
            writer.WriteEndElement();

            writer.WriteEndElement();
        }

        private void WriteFeatureTypesElement(XmlWriter writer, IEnumerable<Feature> features)
        {
            var validFeatures = features;
            if (configuration.Version < 130)
            {
                validFeatures = features.Where(f => f.Name != "wireless internet");
            }

            if (!validFeatures.Any())
                return;

            writer.WriteStartElement("features", configuration.CacheXSD.@namespace);

            foreach (var feature in validFeatures)
            {
                writer.WriteStartElement("feature", configuration.CacheXSD.@namespace);
                if (configuration.Version < 130)
                {
                    writer.WriteAttributeString("name", feature.Name);
                    writer.WriteAttributeString("value", feature.Value);
                }
                else
                {
                    if (configuration.Version >= 140 && !String.IsNullOrEmpty(feature.Id))
                    {
                        writer.WriteAttributeString("id", feature.Id);
                    }

                    writer.WriteString(feature.Name);
                }
                writer.WriteEndElement();
            }

            writer.WriteEndElement();
        }

        private void WriteThemeTypesElement(XmlWriter writer, List<Theme> codes)
        {
            if (codes.Count <= 0)
                return;

            if (configuration.Version <= 120)
                return;

            writer.WriteStartElement("themes", configuration.CacheXSD.@namespace);

            foreach (var code in codes)
            {
                writer.WriteStartElement("theme", configuration.CacheXSD.@namespace);
                if (configuration.Version >= 140)
                {
                    writer.WriteAttributeString("id", code.Id.ToString(CultureInfo.InvariantCulture));
                }
                writer.WriteString(code.Value);
                writer.WriteEndElement();
            }

            writer.WriteEndElement();
        }

        private void WriteDistanceTypesElement(XmlWriter writer, List<Hotel.Distance> codes)
        {
            if (codes.Count == 0)
                return;

            writer.WriteStartElement("distance_types", configuration.CacheXSD.@namespace);

            foreach (var code in codes)
            {
                writer.WriteStartElement("distance_type", configuration.CacheXSD.@namespace);
                if (configuration.Version >= 140)
                {
                    writer.WriteAttributeString("id", code.Id.ToString(CultureInfo.InvariantCulture));
                }

                writer.WriteString(code.Name);
                writer.WriteEndElement();
            }

            writer.WriteEndElement();
        }

        private void WriteRoomTypeElement(XmlTextWriter writer)
        {
            writer.WriteStartElement("room_types", configuration.CacheXSD.@namespace);

            foreach (RoomType rt in root.RoomTypes.Values)
            {
                writer.WriteStartElement("type", configuration.CacheXSD.@namespace);

                writer.WriteElementString("description", configuration.CacheXSD.@namespace, rt.Description);
                writer.WriteElementString("id", configuration.CacheXSD.@namespace, rt.Type_Id);
                writer.WriteElementString("beds", configuration.CacheXSD.@namespace, rt.Beds.ToString());
                writer.WriteElementString("extrabeds", configuration.CacheXSD.@namespace, rt.ExtraBeds.ToString());

                if (rt.IsWeeklyStay)
                {
                    writer.WriteStartElement("weekly_stay", configuration.CacheXSD.@namespace);

                    writer.WriteStartElement("starting_days", configuration.CacheXSD.@namespace);
                    writer.WriteAttributeString("monday", rt.Starting_Days_Monday.ToString().ToLower());
                    writer.WriteAttributeString("tuesday", rt.Starting_Days_Tuesday.ToString().ToLower());
                    writer.WriteAttributeString("wednesday", rt.Starting_Days_Wednesday.ToString().ToLower());
                    writer.WriteAttributeString("thursday", rt.Starting_Days_Thursday.ToString().ToLower());
                    writer.WriteAttributeString("friday", rt.Starting_Days_Friday.ToString().ToLower());
                    writer.WriteAttributeString("saturday", rt.Starting_Days_Saturday.ToString().ToLower());
                    writer.WriteAttributeString("sunday", rt.Starting_Days_Sunday.ToString().ToLower());
                    writer.WriteEndElement();

                    writer.WriteEndElement();
                }

                if (configuration.Version >= 140)
                {
                    writer.WriteElementString("shared_room", configuration.CacheXSD.@namespace, rt.SharedRoom.ToString().ToLower());
                    writer.WriteElementString("shared_facilities", configuration.CacheXSD.@namespace, rt.SharedFacilities.ToString().ToLower());
                }
                if (configuration.Version >= 161)
                {
                    writer.WriteElementString("super_deal", configuration.CacheXSD.@namespace, rt.NonRefundable ? "1" : "0");
                }

                writer.WriteEndElement();
            }

            writer.WriteEndElement();
        }

        private void WriteCancellationPoliciesElement(XmlTextWriter writer)
        {
            writer.WriteStartElement("cancellation_policies", configuration.CacheXSD.@namespace);

            foreach (CancellationPolicy cp in root.CancellationsPolicies.Values)
            {
                writer.WriteStartElement("cancellation_policy", configuration.CacheXSD.@namespace);

                writer.WriteElementString("id", configuration.CacheXSD.@namespace, cp.Id.ToString());

                // Skip deadline element if Deadline_Value is null. This policy is always active.
                if (cp.Deadline_Value != null)
                {
                    writer.WriteStartElement("deadline", configuration.CacheXSD.@namespace);
                    writer.WriteAttributeString("basis", cp.Deadline_Basis.ToString());
                    writer.WriteAttributeString("unit", cp.Deadline_Unit.ToString());
                    writer.WriteAttributeString("value", cp.Deadline_Value.ToString());
                    writer.WriteEndElement();
                }

                writer.WriteStartElement("penalty", configuration.CacheXSD.@namespace);
                writer.WriteAttributeString("basis", cp.Penalty_Basis.ToString());
                writer.WriteAttributeString("value", cp.Penalty_Value.ToString());
                writer.WriteEndElement();

                writer.WriteEndElement();
            }

            writer.WriteEndElement();
        }

        private void WriteHotelPlaces(XmlTextWriter writer, List<Place> places)
        {
            var handledHotels = new List<string>();

            foreach (Place p in places)
            {
                foreach (Hotel h in p.Hotels.Values)
                {
                    if (!handledHotels.Contains(h.Id))
                    {
                        handledHotels.Add(h.Id);

                        if (configuration.WriteHotelsWithoutRooms || h.Rooms.Count > 0 || h.RoomGroups.Count > 0)
                        {
                            WriteHotelNode(writer, h, p);
                        }
                    }
                }

                if (p.Places.Count > 0)
                {
                    WriteHotelPlaces(writer, p.Places);
                }
            }
        }

        #endregion

        #region Hotel

        private void WriteHotelNode(XmlTextWriter writer, Hotel h, Place p)
        {
            System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.InvariantCulture;
            System.Threading.Thread.CurrentThread.CurrentUICulture = System.Globalization.CultureInfo.InvariantCulture;
            // Validate each node before writting
            var w = configuration.AutoValidateOutput ? cacheXmlWritterManager.Validate(writer) : writer;
            WriteHotelNodeInternal(w, h, p);
        }
        private void WriteHotelNodeInternal(XmlTextWriter writer, Hotel h, Place p)
        {
            writer.WriteStartElement("hotel", configuration.CacheXSD.@namespace);

            writer.WriteElementString("id", configuration.CacheXSD.@namespace, h.Id);

            if (configuration.ProviderName == "Thomas Cook")
            {
                writer.WriteElementString("giataCode", configuration.CacheXSD.@namespace, h.GiataCode);
            }

            #region identifiers
            if (this.root.Version >= 160)
            {
                if (h.Identifiers.Any())
                {
                    writer.WriteStartElement("identifiers", configuration.CacheXSD.@namespace);

                    h.Identifiers.ForEach(t =>
                    {
                        writer.WriteStartElement("identifier", configuration.CacheXSD.@namespace);
                        writer.WriteAttributeString("type", t.Type);
                        writer.WriteAttributeString("value", t.Value);
                        writer.WriteEndElement();
                    });

                    writer.WriteEndElement();
                }

            }
            #endregion identifiers

            writer.WriteElementString("name", configuration.CacheXSD.@namespace, h.Name);

            #region accommodation type, descending version specifics
            if (this.root.Version >= 160)
            {
                if (h.AccomodationType != null)
                    writer.WriteElementString("accommodation_type", configuration.CacheXSD.@namespace, h.AccomodationType.ToString());
            }
            else if (this.root.Version >= 104)
            {
                if (h.AccomodationType != null)
                {
                    switch (h.AccomodationType)
                    {
                        case AccomodationType.Hotel:
                            writer.WriteElementString("accomodation_type", configuration.CacheXSD.@namespace, "hotel");
                            break;
                        case AccomodationType.Apartment:
                            writer.WriteElementString("accomodation_type", configuration.CacheXSD.@namespace, "apartment");
                            break;
                        case AccomodationType.Villa:
                            writer.WriteElementString("accomodation_type", configuration.CacheXSD.@namespace, "villa");
                            break;
                    }
                }

            }
            #endregion accommodation type

            writer.WriteElementString("headline", configuration.CacheXSD.@namespace, h.Headline);
            writer.WriteElementString("description", configuration.CacheXSD.@namespace, h.Description);
            writer.WriteElementString("place", configuration.CacheXSD.@namespace, p.Id);

            #region adress
            writer.WriteStartElement("address", configuration.CacheXSD.@namespace);
            writer.WriteElementString("street1", configuration.CacheXSD.@namespace, h.Adress_Street1);
            writer.WriteElementString("street2", configuration.CacheXSD.@namespace, h.Adress_Street2);
            writer.WriteElementString("zipcode", configuration.CacheXSD.@namespace, h.Adress_Zipcode);
            writer.WriteElementString("city", configuration.CacheXSD.@namespace, h.Adress_City);
            writer.WriteElementString("state", configuration.CacheXSD.@namespace, h.Adress_State);
            writer.WriteElementString("country", configuration.CacheXSD.@namespace, h.Adress_Country);
            writer.WriteEndElement();
            #endregion

            #region position
            if (h.Position_Latitude != null && h.Position_Longitude != null && !"".Equals(h.Position_Latitude) && !"".Equals(h.Position_Longitude))
            {
                writer.WriteStartElement("position", configuration.CacheXSD.@namespace);
                writer.WriteElementString("latitude", configuration.CacheXSD.@namespace, h.Position_Latitude);
                writer.WriteElementString("longitude", configuration.CacheXSD.@namespace, h.Position_Longitude);
                writer.WriteEndElement();
            }
            #endregion

            writer.WriteElementString("phone", configuration.CacheXSD.@namespace, h.Phone);
            writer.WriteElementString("fax", configuration.CacheXSD.@namespace, h.Fax);
            writer.WriteElementString("email", configuration.CacheXSD.@namespace, h.Email);

            #region classification
            writer.WriteStartElement("classification", configuration.CacheXSD.@namespace);
            if (configuration.Version < 130)
            {
                if (!String.IsNullOrEmpty(h.Classification))
                {
                    writer.WriteString(h.Classification.TrimEnd('+'));
                }
            }
            else
            {
                writer.WriteString(h.Classification);
            }
            writer.WriteEndElement();
            #endregion

            if (configuration.Version >= 140)
            {
                writer.WriteElementString("best_buy", configuration.CacheXSD.@namespace, h.BestBuy.ToString().ToLower());
            }

            if (configuration.Version >= 150)
            {
                if (h.BlockInfant)
                    writer.WriteElementString("infant_restrictions", configuration.CacheXSD.@namespace, "1");
                if (h.AdultOnly)
                    writer.WriteElementString("adult_only", configuration.CacheXSD.@namespace, "1");
            }


            #region meal labels

            if (configuration.Version >= 150 && h.MealLabels != null && h.MealLabels.Any())
            {
                writer.WriteStartElement("meal_labels");

                foreach (var mealLabel in h.MealLabels)
                {
                    var Label = mealLabel.MealLabelId;
                    var mealtype = string.IsNullOrEmpty(mealLabel.MealType) ? "no meal" : mealLabel.MealType;
                    writer.WriteStartElement("label");
                    writer.WriteAttributeString("meal_type", mealtype);
                    writer.WriteAttributeString("meal_id", mealLabel.MealId.ToString());
                    writer.WriteAttributeString("label_id", Label == 0 ? "" : Label.ToString());
                    writer.WriteEndElement();
                }

                writer.WriteEndElement();

            }

            #endregion

            WriteFeatureTypesElement(writer, h.Features);
            WriteThemeTypesElement(writer, h.Themes);

            WriteGlobalTypesElement(writer, h.GlobalTypes);

            #region distances
            if (h.Distances.Count > 0)
            {
                writer.WriteStartElement("distances", configuration.CacheXSD.@namespace);

                foreach (Hotel.Distance d in h.Distances)
                {
                    writer.WriteStartElement("distance", configuration.CacheXSD.@namespace);
                    writer.WriteAttributeString("name", d.Name);
                    writer.WriteAttributeString("value", d.Value);
                    if (configuration.Version >= 160)
                        writer.WriteAttributeString("reference_point", d.ReferencePoint);
                    if (!string.IsNullOrEmpty(d.IataCode))
                        writer.WriteAttributeString("iata_code", d.IataCode);
                    writer.WriteEndElement();
                }

                writer.WriteEndElement();
            }
            #endregion

            #region reviews
            if (h.Reviews.Count > 0)
            {
                if (configuration.Version >= 140)
                {
                    writer.WriteStartElement("reviews", configuration.CacheXSD.@namespace);

                    foreach (Review r in h.Reviews)
                    {
                        writer.WriteStartElement("review", configuration.CacheXSD.@namespace);
                        writer.WriteAttributeString("provider", r.Provider);
                        writer.WriteAttributeString("type", r.Type);
                        writer.WriteAttributeString("value", r.Value);
                        writer.WriteEndElement();
                    }

                    writer.WriteEndElement();
                }
            }
            #endregion

            WriteImagesElement(writer, h);

            #region rooms
            writer.WriteStartElement("rooms", configuration.CacheXSD.@namespace);

            if (h.RoomGroups.Count > 0)
            {
                foreach (RoomGroup rg in h.RoomGroups.Values)
                {
                    // Check that a room is avaliable, as roomgroups can't exist without a room
                    if (!configuration.WriteRoomsWithoutAvailability && !rg.Rooms.Values.Any(r => r.AvailabilityPeriods.Count > 0))
                        continue;

                    // earlier versions don't support room groups so it just add the room to the rooms node
                    if (this.root.Version >= 104)
                    {
                        writer.WriteStartElement("room_group", configuration.CacheXSD.@namespace);

                        writer.WriteElementString("id", configuration.CacheXSD.@namespace, rg.RoomGroupId);
                    }

                    foreach (Room group_room in rg.Rooms.Values)
                    {
                        WriteRoomsNode(writer, group_room);
                    }

                    // earlier versions don't support room groups so it just add the room to the rooms node
                    if (this.root.Version >= 104)
                    {
                        writer.WriteEndElement();
                    }
                }
            }

            foreach (Room hotel_room in h.Rooms.Values)
            {
                WriteRoomsNode(writer, hotel_room);
            }

            writer.WriteEndElement();
            #endregion

            #region translations
            if (this.root.Version >= 160)
            {
                if (h.Translations != null &&
                    (h.Translations.description.Any() || h.Translations.headline.Any()))
                {
                    writer.WriteStartElement("translations", configuration.CacheXSD.@namespace);

                    h.Translations.description.ForEach(t =>
                    {
                        writer.WriteStartElement("description", configuration.CacheXSD.@namespace);
                        writer.WriteAttributeString("lang", t.lang);
                        writer.WriteAttributeString("country", t.country);
                        writer.WriteValue(t.Value);
                        writer.WriteEndElement();
                    });
                    h.Translations.headline.ForEach(t =>
                    {
                        writer.WriteStartElement("headline", configuration.CacheXSD.@namespace);
                        writer.WriteAttributeString("lang", t.lang);
                        writer.WriteAttributeString("country", t.country);
                        writer.WriteValue(t.Value);
                        writer.WriteEndElement();
                    });

                    writer.WriteEndElement();
                }
            }
            #endregion translations
            if (writer is SunHotels.XML.Exporters.XmlWritterManager.XmlWritterValidator && configuration.Version < 160)
            {
                (writer as SunHotels.XML.Exporters.XmlWritterManager.XmlWritterValidator).SetHotelValidationBackwardCompatibility(true, configuration.CacheXSD.@namespace, configuration.CacheXSD.Value);
            }

            writer.WriteEndElement();
        }

        private void WriteGlobalTypesElement(XmlTextWriter writer, List<string[]> globalTypes)
        {
            if (configuration.ProviderName == "Thomas Cook" && globalTypes != null && globalTypes.Any())
            {
                writer.WriteStartElement("DRVGlobalTypes", configuration.CacheXSD.@namespace);
                writer.WriteStartElement("DRVGlobalType", configuration.CacheXSD.@namespace);
                foreach (var gt in globalTypes)
                {
                    writer.WriteStartElement("GTAttribute", configuration.CacheXSD.@namespace);
                    if (!string.IsNullOrEmpty(gt[0])) writer.WriteAttributeString("GT", gt[0]);
                    if (!string.IsNullOrEmpty(gt[1])) writer.WriteAttributeString("ST", gt[1]);
                    if (!string.IsNullOrEmpty(gt[2])) writer.WriteAttributeString("AT", gt[2]);
                    writer.WriteEndElement();
                }
                writer.WriteEndElement();
                writer.WriteEndElement();
            }
        }

        private void WriteImagesElement(XmlTextWriter writer, Hotel h)
        {
            if (root.Version <= 104)
                return;

            if (h.Images == null || h.Images.Count == 0)
                return;

            writer.WriteStartElement("images", configuration.CacheXSD.@namespace);

            foreach (var image in h.Images)
            {
                writer.WriteStartElement("image", configuration.CacheXSD.@namespace);
                if (configuration.Version < 140)
                {
                    var imageVariant = image.ImageVariants.First();
                    if (configuration.Version == 130)
                    {
                        writer.WriteAttributeString("id", image.Id);
                        if (imageVariant.Width.HasValue)
                            writer.WriteAttributeString("width", imageVariant.Width.ToString());
                        if (imageVariant.Height.HasValue)
                            writer.WriteAttributeString("height", imageVariant.Height.ToString());
                    }

                    writer.WriteString(imageVariant.URL);
                }
                else
                {
                    writer.WriteAttributeString("id", image.Id);

                    foreach (var variant in image.ImageVariants)
                    {
                        writer.WriteStartElement("image_variant", configuration.CacheXSD.@namespace);

                        if (variant.Width.HasValue)
                            writer.WriteAttributeString("width", variant.Width.ToString());
                        if (variant.Height.HasValue)
                            writer.WriteAttributeString("height", variant.Height.ToString());

                        writer.WriteString(variant.URL);

                        writer.WriteEndElement();
                    }
                }
                writer.WriteEndElement();
            }

            writer.WriteEndElement();
        }

        #endregion

        #region Rooms


        private void WriteRoomsNode(XmlTextWriter writer, Room r)
        {
            if (r.AvailabilityPeriods.Count == 0 && !configuration.WriteRoomsWithoutAvailability)
            {
                return;
            }

            writer.WriteStartElement("room", configuration.CacheXSD.@namespace);

            writer.WriteAttributeString("type_id", r.TypeId);

            RoomType roomType = root.RoomTypes[r.TypeId];

            writer.WriteStartElement("room_id", configuration.CacheXSD.@namespace);
            if (r.RoomId == null)
            {
                writer.WriteString("");
            }
            else
            {
                writer.WriteString(r.RoomId);
            }
            writer.WriteEndElement();

            if (configuration.Version >= 140)
            {
                writer.WriteElementString("best_buy", configuration.CacheXSD.@namespace, r.BestBuy.ToString().ToLower());
            }

            if (configuration.Version >= 150 && r.MealSupplementRestriction)
            {
                writer.WriteElementString("meal_supplement_restriction", configuration.CacheXSD.@namespace, "1");
            }

            if (configuration.ProviderName == "Thomas Cook")
            {
                writer.WriteElementString("safe_passage", configuration.CacheXSD.@namespace, r.HasSafePassage.ToString().ToLower());
            }

            if (configuration.Version >= 130)
            {
                WriteFeatureTypesElement(writer, r.Features);
            }

            WriteThemeTypesElement(writer, r.Themes);

            WriteGlobalTypesElement(writer, r.GlobalTypes);

            foreach (AvailabilityPeriod ap in r.AvailabilityPeriods.Values)
            {
                writer.WriteStartElement("availability_period", configuration.CacheXSD.@namespace);

                writer.WriteAttributeString("datefrom", ap.DateFrom.ToString("yyyy-MM-dd"));
                writer.WriteAttributeString("dateto", ap.DateTo.ToString("yyyy-MM-dd"));

                #region pricing
                writer.WriteStartElement("pricing", configuration.CacheXSD.@namespace);

                #region base
                writer.WriteStartElement("base", configuration.CacheXSD.@namespace);

                if (ap.Pricing_BookableBoardId != null && ap.Pricing_BookableBoardId != "")
                {
                    writer.WriteAttributeString("bookable_board_id", ap.Pricing_BookableBoardId);
                }

                writer.WriteAttributeString("included_board", ap.Pricing_BaseIncludedBoard);

                writer.WriteAttributeString("price", ap.Pricing_BasePrice.ToString());

                writer.WriteEndElement();
                #endregion

                ///TODO: FIX extrabed
                #region extra beds
                if (roomType.ExtraBeds > 0 && (ap.Pricing_ExtrabedAdult != null || ap.Pricing_ExtrabedChild != null))
                {
                    writer.WriteStartElement("extrabed", configuration.CacheXSD.@namespace);

                    if (ap.Pricing_ExtrabedAdult != null)
                    {
                        writer.WriteAttributeString("adult", ap.Pricing_ExtrabedAdult.ToString());
                    }

                    if (ap.Pricing_ExtrabedChild != null)
                    {
                        writer.WriteAttributeString("child", ap.Pricing_ExtrabedChild.ToString());
                    }

                    writer.WriteEndElement();
                }
                #endregion

                #region additional boards
                if (ap.AdditionalBoards.Count > 0)
                {
                    writer.WriteStartElement("additional_boards", configuration.CacheXSD.@namespace);
                    foreach (AvailabilityPeriod.AdditionalBoard ab in ap.AdditionalBoards)
                    {
                        writer.WriteStartElement("board", configuration.CacheXSD.@namespace);

                        writer.WriteAttributeString("board_type", ab.Type);
                        if (ab.Adult != null)
                            writer.WriteAttributeString("adult", ab.Adult);
                        if (ab.Child != null)
                            writer.WriteAttributeString("child", ab.Child);
                        if (ab.BookableBoardId != null)
                            writer.WriteAttributeString("bookable_board_id", ab.BookableBoardId);

                        writer.WriteEndElement();
                    }

                    writer.WriteEndElement();
                }
                #endregion

                ///TODO: FIX discount
                #region discount
                if (ap.Discounts.Count > 0)
                {
                    writer.WriteStartElement("discounts", configuration.CacheXSD.@namespace);

                    foreach (Discount d in ap.Discounts)
                    {
                        switch (d.Type)
                        {
                            case DiscountType.EarlyBooking:
                                writer.WriteStartElement("early_booking", configuration.CacheXSD.@namespace);

                                if (d.Period_ID != null)
                                {
                                    writer.WriteAttributeString("period_id", d.Period_ID.ToString());
                                }

                                if (this.root.Version >= 150)
                                {
                                    writer.WriteAttribute("group", d.AccumulativeGroup);
                                }

                                writer.WriteElementString("days_requried", configuration.CacheXSD.@namespace, d.MinDays.ToString());

                                if (d.Amount > 0)
                                {
                                    writer.WriteElementString("amount", configuration.CacheXSD.@namespace, d.Amount.ToString(CultureInfo.InvariantCulture));
                                }
                                else if (d.Percent > 0)
                                {
                                    writer.WriteStartElement("percent", configuration.CacheXSD.@namespace);
                                    if (this.root.Version > 120)
                                    {
                                        writer.WriteString(d.Percent.ToString(CultureInfo.InvariantCulture));
                                    }
                                    else
                                    {
                                        writer.WriteString(Math.Floor(d.Percent).ToString(CultureInfo.InvariantCulture));
                                    }
                                    writer.WriteEndElement();
                                }

                                if (this.root.Version >= 150)
                                {
                                    writer.WriteElement("checkinbased", d.CheckinBased);
                                    writer.WriteElement("include_meal", d.IncludeMeal);
                                    writer.WriteElement("include_extrabed", d.IncludeExtraBed);
                                }

                                writer.WriteEndElement();
                                break;
                            case DiscountType.Value:
                                writer.WriteStartElement("value", configuration.CacheXSD.@namespace);

                                if (d.Period_ID != null)
                                {
                                    writer.WriteAttributeString("period_id", d.Period_ID.ToString());
                                }

                                if (this.root.Version >= 150)
                                {
                                    writer.WriteAttribute("group", d.AccumulativeGroup);
                                }

                                writer.WriteElementString("days_requried", configuration.CacheXSD.@namespace, d.MinDays.ToString());

                                if (d.MaxPeriods != null)
                                {
                                    writer.WriteElementString("days_maximum", configuration.CacheXSD.@namespace, d.MaxPeriods.ToString());
                                }

                                if (d.Amount > 0)
                                {
                                    writer.WriteElementString("amount", configuration.CacheXSD.@namespace, d.Amount.ToString(CultureInfo.InvariantCulture));
                                }
                                else if (d.Percent > 0)
                                {
                                    writer.WriteStartElement("percent", configuration.CacheXSD.@namespace);
                                    if (this.root.Version > 120)
                                    {
                                        writer.WriteString(d.Percent.ToString(CultureInfo.InvariantCulture));
                                    }
                                    else
                                    {
                                        writer.WriteString(Math.Floor(d.Percent).ToString(CultureInfo.InvariantCulture));
                                    }
                                    writer.WriteEndElement();
                                }

                                if (this.root.Version >= 150)
                                {
                                    writer.WriteElement("checkinbased", d.CheckinBased);
                                    writer.WriteElement("include_meal", d.IncludeMeal);
                                    writer.WriteElement("include_extrabed", d.IncludeExtraBed);
                                }

                                writer.WriteEndElement();
                                break;
                            case DiscountType.MinMax:
                                writer.WriteStartElement("min_max", configuration.CacheXSD.@namespace);

                                if (d.Period_ID != null)
                                {
                                    writer.WriteAttributeString("period_id", d.Period_ID.ToString());
                                }

                                if (this.root.Version >= 150)
                                {
                                    writer.WriteAttribute("group", d.AccumulativeGroup);
                                }

                                writer.WriteElementString("days_requried", configuration.CacheXSD.@namespace, d.MinDays.ToString());

                                if (d.MaxDays != null)
                                {
                                    writer.WriteElementString("days_maximum", configuration.CacheXSD.@namespace, d.MaxDays.ToString());
                                }

                                if (d.Percent > 0)
                                {
                                    writer.WriteStartElement("percent", configuration.CacheXSD.@namespace);
                                    if (this.root.Version > 120)
                                    {
                                        writer.WriteString(d.Percent.ToString(CultureInfo.InvariantCulture));
                                    }
                                    else
                                    {
                                        writer.WriteString(Math.Floor(d.Percent).ToString(CultureInfo.InvariantCulture));
                                    }
                                    writer.WriteEndElement();
                                }

                                if (this.root.Version >= 150)
                                {
                                    writer.WriteElement("checkinbased", d.CheckinBased);
                                    writer.WriteElement("include_meal", d.IncludeMeal);
                                    writer.WriteElement("include_extrabed", d.IncludeExtraBed);
                                }

                                writer.WriteEndElement();
                                break;

                            case DiscountType.PayStay:
                                string rID = r.RoomId;
                                writer.WriteStartElement("pay_stay", configuration.CacheXSD.@namespace);

                                if (d.Period_ID != null)
                                {
                                    writer.WriteAttributeString("period_id", d.Period_ID.ToString());
                                }

                                if (this.root.Version >= 150)
                                {
                                    writer.WriteAttribute("group", d.AccumulativeGroup);
                                }

                                writer.WriteElementString("days_requried", configuration.CacheXSD.@namespace, d.MinDays.ToString());

                                if (d.Amount > 0)
                                {
                                    writer.WriteElementString("amount", configuration.CacheXSD.@namespace, d.Amount.ToString(CultureInfo.InvariantCulture));
                                }
                                if (d.MaxPeriods != null)
                                {
                                    writer.WriteElementString("max_periods", configuration.CacheXSD.@namespace, d.MaxPeriods.ToString());
                                }

                                if (this.root.Version >= 150)
                                {
                                    writer.WriteElement("checkinbased", d.CheckinBased);
                                    writer.WriteElement("include_meal", d.IncludeMeal);
                                    writer.WriteElement("include_extrabed", d.IncludeExtraBed);
                                }

                                writer.WriteEndElement();
                                break;
                        }
                    }

                    writer.WriteEndElement();
                }
                #endregion

                writer.WriteEndElement();
                #endregion

                writer.WriteStartElement("availability", configuration.CacheXSD.@namespace);
                writer.WriteElementString("available_units", configuration.CacheXSD.@namespace, ap.AvailabilityAvailableUnits.ToString());
                writer.WriteEndElement();

                writer.WriteElementString("release_days", configuration.CacheXSD.@namespace, ap.ReleaseDays.ToString());
                writer.WriteElementString("minimum_stay", configuration.CacheXSD.@namespace, ap.MinimumStay.ToString());

                #region cancellation policies
                if (ap.CancellationPolicies.Count > 0)
                {
                    writer.WriteStartElement("cancellation_policies", configuration.CacheXSD.@namespace);
                    foreach (string cp in ap.CancellationPolicies)
                    {
                        writer.WriteStartElement("cancellation_policy", configuration.CacheXSD.@namespace);
                        writer.WriteAttributeString("policy_id", cp);
                        writer.WriteEndElement();
                    }
                    writer.WriteEndElement();
                }
                #endregion

                writer.WriteEndElement();
            }

            writer.WriteEndElement();


        }

        #endregion

        #region Notes (country, destination and resort)
        public Action<XmlTextWriter> CreateDestinationCommonNotesXml(string rootName, String container, String node, string ns, Dictionary<int, List<Note>> notes)
        {
            return writer =>
            {
                writer.WriteStartElement(rootName, ns);
                writer.WriteStartElement(container);
                foreach (var pair in notes)
                {
                    writer.WriteStartElement(node);
                    writer.WriteAttributeString("id", pair.Key.ToString());

                    foreach (var note in pair.Value)
                    {
                        writer.WriteStartElement("note");

                        writer.WriteAttribute("id", note.Id);
                        writer.WriteAttribute("from", note.FromDate.Format());
                        writer.WriteAttribute("to", note.ToDate.Format());

                        writer.WriteEndElement();
                    }

                    writer.WriteEndElement();
                }
                writer.WriteEndElement();
                writer.WriteEndElement();
            };
        }
        #endregion
    }
}
