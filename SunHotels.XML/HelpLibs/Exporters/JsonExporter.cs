﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Newtonsoft.Json;
using SunHotels.Export;
using SunHotels.XML.Data;
using SunHotels.XML.HelpClasses;
using SunHotels.XML.Optimize.Data;
using Sunhotels.Export;

namespace SunHotels.XML.Exporters
{
    public class JsonExporter : BaseFileExporter
    {
        private OptimizedRoot data;

        public JsonExporter(Configuration configuration) : base(configuration) { }

        public override void Write(ExportData data)
        {
            this.data = data as OptimizedRoot;

            CreateJsonFile(
                WriteHotelTemplate<List<DiscountPeriod>>("discounts_cache", this.data.Discounts, CreateDiscountXml, true, WriteDiscountTypes),
                "Cache\\AvailabilityExport_json\\discounts.json", false);

            CreateJsonFile(
                WriteHotelTemplate<List<CancellationPolicyPeriod>>("policies_cache", this.data.Policies, CreateCancellationPolicyXml, false, WriteCancellationPolicyTypes),
                "Cache\\AvailabilityExport_json\\cancellation_policies.json", false);

            CreateJsonFile(
                WriteHotelTemplate<List<BasicAvailabilityPeriod>>("availability_cache", this.data.Availability, CreateAvailabilityXml),
                "Cache\\AvailabilityExport_json\\availability.json", false);

            CreateJsonFile(
                WriteHotelTemplate<List<PricePeriod>>("prices_cache", this.data.Prices, CreatePriceXml, true, WriteBoardTypes),
                "Cache\\AvailabilityExport_json\\prices.json", false);

            CreateJsonFile(
                WriteHotelTemplate<RoomPriority>("room_priorities_cache", this.data.RoomPriorities, CreateRoomPrioritiesXml),
                "Cache\\AvailabilityExport_json\\room_priorities.json", false);

            CreateJsonFile(
                CreateRoomGroupsXml("room_groups_cache", this.data.RoomGroups),
                "Cache\\AvailabilityExport_json\\room_groups.json", true);

            CreateJsonFile(
                WriteHotelTemplate<List<Note>>("room_notes_cache", this.data.RoomNotes, CreateRoomNotesXml),
                "Cache\\AvailabilityExport_json\\room_notes.json", false);
        }

        public override void Validate()
        {
            
        }

        private Action<JsonTextWriter> WriteHotelTemplate<T>(string rootName, SortedDictionary<int, SortedDictionary<int, T>> hotels, Action<JsonTextWriter, T> writeFunc, bool showCurrency = false, params Action<JsonTextWriter>[] additionalWriteFuncs)
        {
            return writer => {
                foreach (var func in additionalWriteFuncs)
                    func(writer);

                writer.WriteStartArray("hotels");
                foreach (var hotel in hotels) {
                    writer.WriteStartObject();
                    writer.WriteProperty("id", hotel.Key.ToString());

                    writer.WriteStartArray("rooms");
                    foreach (var room in hotel.Value) {
                        writer.WriteStartObject();
                        writer.WriteProperty("id", room.Key.ToString());

                        if (showCurrency) {
                            writer.WriteProperty("currency", data.RoomCurrencies[hotel.Key][room.Key]);
                        }

                        writeFunc(writer, room.Value);

                        writer.WriteEndObject();
                    }
                    writer.WriteEndArray();

                    writer.WriteEndObject();
                }
                writer.WriteEndArray();
            };
        }

        private void CreateDiscountXml(JsonTextWriter writer, List<DiscountPeriod> discounts)
        {
            writer.WriteStartArray("discount_periods");
            foreach (var period in discounts)
	        {
                writer.WriteStartObject();
                writer.WriteProperty("from", period.From.Format());
                writer.WriteProperty("to", period.To.Format());

                writer.WriteStartArray("periods");
                foreach (var discount in period.Discounts) {
                    writer.WriteStartObject();
                    writer.WriteProperty("type_id", discount.TypeId);
                    writer.WriteProperty("group", discount.AccumulativeGroup);
                    writer.WriteProperty("period_id", discount.Period_ID);

                    switch (discount.Type) {
                        case DiscountType.EarlyBooking:
                            writer.WriteProperty("last_date", discount.LastDate.Value.Format());

                            if (discount.Amount > 0)
                                writer.WriteProperty("amount", discount.Amount);
                            else
                                writer.WriteProperty("percent", discount.Percent);

                            break;
                        case DiscountType.Value:
                            writer.WriteProperty("days_requried", discount.MinDays);
                            writer.WriteProperty("days_maximum", discount.MaxPeriods);

                            if (discount.Amount > 0)
                                writer.WriteProperty("amount", discount.Amount);
                            else
                                writer.WriteProperty("percent", discount.Percent);

                            break;
                        case DiscountType.MinMax:
                            writer.WriteProperty("days_requried", discount.MinDays);
                            writer.WriteProperty("days_maximum", discount.MaxDays);
                            writer.WriteProperty("percent", discount.Percent);

                            break;

                        case DiscountType.PayStay:
                            writer.WriteProperty("days_requried", discount.MinDays);
                            writer.WriteProperty("days", discount.Amount);
                            writer.WriteProperty("max_periods", discount.MaxPeriods);

                            break;
                    }

                    writer.WriteProperty("checkinbased", discount.CheckinBased);
                    writer.WriteProperty("include_meal", discount.IncludeMeal);
                    writer.WriteProperty("include_extrabed", discount.IncludeExtraBed);

                    writer.WriteEndObject();
                }
                writer.WriteEndArray();
                
                writer.WriteEndObject();
            }
            writer.WriteEndArray();
        }

        private void CreateCancellationPolicyXml(JsonTextWriter writer, List<CancellationPolicyPeriod> cancellationPolicyPeriods) {
            writer.WriteStartArray("cancellation_policy_periods");

            foreach (var cancellationPolicy in cancellationPolicyPeriods) {
                writer.WriteStartObject();
                writer.WriteProperty("from", cancellationPolicy.From.Format());
                writer.WriteProperty("to", cancellationPolicy.To.Format());

                writer.WriteStartArray("policies");
                foreach (var id in cancellationPolicy.Policies) {
                    writer.WriteValue(id);
                }
                writer.WriteEndArray();

                writer.WriteEndObject();
            }

            writer.WriteEndArray();
        }

        private void CreateAvailabilityXml(JsonTextWriter writer, List<BasicAvailabilityPeriod> availabilityPeriods)
        {
            writer.WriteStartArray("availability_periods");

            foreach (var period in availabilityPeriods) {
                writer.WriteStartObject();
                writer.WriteProperty("from", period.From.Format());
                writer.WriteProperty("to", period.To.Format());

                writer.WriteProperty("available_units", period.AvailableUnits);
                writer.WriteProperty("release_days", period.ReleaseDays);
                writer.WriteProperty("minimum_stay", period.MinimumStay);

                writer.WriteEndObject();
            }

            writer.WriteEndArray();
        }

        private void CreatePriceXml(JsonTextWriter writer, List<PricePeriod> pricePeriods)
        {
            writer.WriteStartArray("price_periods");

            foreach (var period in pricePeriods) {
                writer.WriteStartObject();
                writer.WriteProperty("from", period.From.Format());
                writer.WriteProperty("to", period.To.Format());

                writer.WriteStartObject("base");
                writer.WriteProperty("meal_type_id", period.BoardTypeId);
                writer.WriteProperty("price", period.BasePrice);
                writer.WriteEndObject();

                if (period.ExtrabedAdult != null || period.ExtrabedChild != null) {
                    writer.WriteStartObject("extrabed");
                    writer.WriteProperty("adult", period.ExtrabedAdult);
                    writer.WriteProperty("child", period.ExtrabedChild);
                    writer.WriteEndObject();
                }

                if (period.AdditionalBoards.Count > 0) {
                    writer.WriteStartArray("additional_meals");

                    foreach (AvailabilityPeriod.AdditionalBoard ab in period.AdditionalBoards) {
                        writer.WriteStartObject();

                        writer.WriteProperty("type_id", ab.TypeId);
                        writer.WritePropertyDecimal("adult", ab.Adult, 2);
                        writer.WritePropertyDecimal("child", ab.Child, 2);

                        writer.WriteEndObject();
                    }

                    writer.WriteEndArray();
                }

                writer.WriteEndObject();
            }

            writer.WriteEndArray();
        }

        private void CreateRoomPrioritiesXml(JsonTextWriter writer, RoomPriority roomPriority)
        {
            writer.WriteProperty("priority", roomPriority.Priority);
            writer.WriteProperty("room_type_id", roomPriority.RoomTypeId);
        }

        public Action<JsonTextWriter> CreateRoomGroupsXml(string rootName, SortedList<int, SortedList<string, List<int>>> hotels)
        {
            return writer => {
                writer.WriteStartArray("hotels");
                foreach (var hotel in hotels) {
                    writer.WriteStartObject();
                    writer.WriteProperty("id", hotel.Key.ToString());

                    writer.WriteStartArray("room_groups");
                    foreach (var roomGroup in hotel.Value) {
                        writer.WriteStartObject();

                        writer.WriteStartArray("rooms");
                        foreach (var roomId in roomGroup.Value) {
                            writer.WriteValue(roomId);
                        }
                        writer.WriteEndArray();

                        writer.WriteEndObject();
                    }
                    writer.WriteEndArray();

                    writer.WriteEndObject();
                }
                writer.WriteEndArray();
            };
        }

        private void CreateRoomNotesXml(JsonTextWriter writer, List<Note> notes)
        {
            writer.WriteStartArray("notes");
			
            foreach (var note in notes) {
                writer.WriteStartObject();
                writer.WriteProperty("id", note.Id);
                writer.WriteProperty("from", note.FromDate.Format());
                writer.WriteProperty("to", note.ToDate.Format());
                writer.WriteEndObject();
            }
			
            writer.WriteEndArray();
        }

        #region Additional data

        private void WriteCancellationPolicyTypes(JsonTextWriter writer) {
            writer.WriteStartArray("cancellation_policies");

            foreach (CancellationPolicy cp in data.CancellationsPolicies.Values) {
                writer.WriteStartObject();
                writer.WriteProperty("id", cp.Id);

                // Skip deadline element if Deadline_Value is null. This policy is always active.
                if (cp.Deadline_Value != null) {
                    writer.WriteStartObject("deadline");
                    writer.WriteProperty("basis", cp.Deadline_Basis);
                    writer.WriteProperty("unit", cp.Deadline_Unit);
                    writer.WriteProperty("value", cp.Deadline_Value);
                    writer.WriteEndObject();
                }

                writer.WriteStartObject("penalty");
                writer.WriteProperty("basis", cp.Penalty_Basis);
                writer.WriteProperty("value", cp.Penalty_Value);
                writer.WriteEndObject();

                writer.WriteEndObject();
            }

            writer.WriteEndArray();
        }

        private void WriteBoardTypes(JsonTextWriter writer)
        {
            writer.WriteStartArray("meal_types");

            foreach (var boardType in AvailabilityPeriod.AdditionalBoard.GetBoardTypes()) {
                writer.WriteStartObject();
                writer.WriteProperty("id", boardType.Key);
                writer.WriteProperty("name", boardType.Value);
                writer.WriteEndObject();
            }

            writer.WriteEndArray();
        }

        private void WriteDiscountTypes(JsonTextWriter writer)
        {
            writer.WriteStartArray("discount_types");

            foreach (var discountType in Discount.GetDiscountTypes()) {
                writer.WriteStartObject();
                writer.WriteProperty("id", discountType.Key);
                writer.WriteProperty("name", discountType.Value);
                writer.WriteEndObject();
            }

            writer.WriteEndArray();
        }

        #endregion

        protected void CreateJsonFile(Action<JsonTextWriter> writeFunc, string location, bool useTempFile)
        {
            FileInfo finalFile = new FileInfo(location);
            if (!finalFile.Directory.Exists)
                finalFile.Directory.Create();

            FileInfo tempFile = null;
            if (useTempFile) {
                tempFile = new FileInfo(finalFile.FullName.Remove(finalFile.FullName.Length - finalFile.Extension.Length) + ".tmp");

                if (tempFile.Exists)
                    tempFile.Delete();
            }

            var file = useTempFile ? tempFile : finalFile;

            JsonTextWriter writer = new JsonTextWriter(new StreamWriter(new FileStream(file.FullName, FileMode.Create), Encoding.UTF8));
            //writer.Formatting = Formatting.Indented;

            try {
                writer.WriteStartObject();

                writeFunc(writer);

                writer.WriteEndObject();
                writer.Flush();
            }
            catch (Exception ex) {
                writer.Flush();
                throw ex;
            }
            finally {
                writer.Close();

                if (useTempFile) {
                    if (finalFile.Exists)
                        finalFile.Delete();
                    tempFile.MoveTo(finalFile.FullName);
                }
            }
        }
    }
}
