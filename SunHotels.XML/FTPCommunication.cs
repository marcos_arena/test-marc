using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Net;
using System.Text.RegularExpressions;

namespace SunHotels.XML
{
    public class FTPCommunication
    {
        private string ftpUser = string.Empty;
        private string ftpPassword = string.Empty;
        private bool ftpUsePassive = false;

        public FTPCommunication(string strUser, string strPassword)
        {
            ftpUser = strUser;
            ftpPassword = strPassword;
        }
        public FTPCommunication(string strUser, string strPassword, bool ftpUsePassive)
        {
            ftpUser = strUser;
            ftpPassword = strPassword;
            this.ftpUsePassive = ftpUsePassive;
        }

        public void GetDirectoryList(string sourceURL, Dictionary<string, string> directoryList)
        {

            string directoryResponse = string.Empty;

            StreamReader reader = null;
            try
            {
                FtpWebRequest listRequest = (FtpWebRequest)WebRequest.Create(sourceURL);

                NetworkCredential listCredential = new NetworkCredential(ftpUser, ftpPassword);

                listRequest.Credentials = listCredential;
				listRequest.UsePassive = ftpUsePassive;
                listRequest.Method = WebRequestMethods.Ftp.ListDirectoryDetails;
				
                FtpWebResponse listResponse = (FtpWebResponse)listRequest.GetResponse();

                reader = new StreamReader(listResponse.GetResponseStream());

                directoryResponse = reader.ReadToEnd();

                Regex splitString = new Regex("\n");

                string[] directoryArray = splitString.Split(directoryResponse);

                foreach (string s in directoryArray)
                {
                    if (!string.IsNullOrEmpty(s))
                    {
                        int length = s.Split(" ".ToCharArray(), 9, StringSplitOptions.RemoveEmptyEntries).Length;
                        string sFilename = s.Split(" ".ToCharArray(), 9, StringSplitOptions.RemoveEmptyEntries)[length - 1].TrimEnd();
                        directoryList.Add(sourceURL + sFilename, sFilename);
                    }
                }

            }
            catch (UriFormatException ex)
            {
                throw new ApplicationException("Invalid FTP URL.", ex);
            }
            catch (WebException ex)
            {
                throw new ApplicationException("FTP URL could not be reached.", ex);
            }
            finally
            {
                if (reader != null)
                    reader.Close();
            }
        }

        public void DownloadFile(string strSource, string strDestination)
        {

            {
                try
                {
                    FtpWebRequest downloadRequest = (FtpWebRequest)WebRequest.Create(strSource);
                    downloadRequest.UseBinary = true;
                    downloadRequest.UsePassive = ftpUsePassive;
                    NetworkCredential listCredential = new NetworkCredential(ftpUser, ftpPassword);
                    downloadRequest.Credentials = listCredential;

                    FtpWebResponse downloadResponse = (FtpWebResponse)downloadRequest.GetResponse();
                    using (Stream responseStream = downloadResponse.GetResponseStream())
                    {
                        using (FileStream fileStream = File.Create(strDestination))
                        {
                            byte[] buffer = new byte[1024];
                            int bytesRead;
                            while (true)
                            {
                                bytesRead = responseStream.Read(buffer, 0, buffer.Length);
                                if (bytesRead == 0)
                                    break;
                                fileStream.Write(buffer, 0, bytesRead);
                            }
                        }
                    }

                    //Console.WriteLine("Debug: FTP Download complete.");
                }
                catch (UriFormatException ex)
                {
                    throw new ApplicationException("Invalid path to file.", ex);
                }
                catch (WebException ex)
                {
                    throw new ApplicationException("FTP server could not be reached.", ex);
                }
                catch (IOException ex)
                {
                    throw new ApplicationException("File could not be written to disk.", ex);
                }
            }

        }

    }

}
