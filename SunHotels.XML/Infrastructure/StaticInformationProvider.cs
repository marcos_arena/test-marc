﻿using SunHotels.XML.Infrastructure.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using System;
using System.Linq;

namespace SunHotels.XML.Infrastructure
{

    public class StaticInformationProvider : IStaticInformationProvider
    {
        private IStaticInformationDal staticInformationDal { get; set; }
        private IHotelDal hotelDal { get; set; }

        public StaticInformationProvider(IStaticInformationDal staticInformationDal,
            IHotelDal hotelDal)
        {
            this.staticInformationDal = staticInformationDal;
            this.hotelDal = hotelDal;
        }

        public async Task<IEnumerable<DestinationDataRecord>> GetCountryDestinationAsync(int countryID, string language)
        {
            var destinations = await staticInformationDal.GetDestinationsByCountryData(countryID, language);
            return destinations;
        }

        public async Task<IEnumerable<DestinationDataRecord>> GetCountryDestinationAsync(int countryID, int[] destinationIds, string language)
        {
            var destinations = await staticInformationDal.GetDestinationsByCountryData(countryID, destinationIds, language);
            return destinations;
        }

        public async Task<IEnumerable<RoomStaticDataRecord>> GetResortRoomsAsync(int resortId, string language)
        {
            var rooms = await staticInformationDal.GetResortRoomStaticAsync(resortId, language);
            return rooms;
        }

        public async Task<IEnumerable<RoomStaticDataRecord>> GetCountryRoomStaticAsync(int countryId)
        {
            var rooms = await staticInformationDal.GetCountryRoomStaticAsync(countryId);
            return rooms;
        }

        public async Task<IEnumerable<HotelDataRecord>> GetCountryHotelsAsync(int countryId)
        {
            var hotels = await hotelDal.GetHotelDataByCountryWithoutLanguageAsync(countryId);
            return hotels;
        }


        public async Task<IEnumerable<HotelDataRecord>> GetCountryHotelsAsync(int resortId, string language)
        {
            var hotels = await hotelDal.GetHotelDataByResortIdAsync(resortId, language);
            return hotels;
        }

        public async Task<IEnumerable<HotelImageDataRecord>> GetHotelImageByResortAsync(int resortId)
        {
            var images = await staticInformationDal.GetHotelImageByResortAsync(resortId);
            return images;
        }

        public async Task<IEnumerable<HotelImageDataRecord>> GetHotelImageByCountryAsync(int countryId)
        {
            var images = await staticInformationDal.GetHotelImageByCountryAsync(countryId);
            return images;
        }

        public async Task<IEnumerable<ResortDataRecord>> GetResortsAsync(int destinationId, string language, bool useOldResort, bool includeAllResorts)
        {
            var resorts = await staticInformationDal.GetResorts(destinationId, language, useOldResort, includeAllResorts);
            return resorts;
        }

        public async Task<IEnumerable<MealDataRecord>> GetMealLabelsAsync()
        {
            var mealLabels = await staticInformationDal.GetMealLabelByCountryAsync(0);
            return mealLabels;
        }

        public async Task<IEnumerable<MealDataRecord>> GetMealLabelsByCountryAsync(int countryId)
        {
            var mealLabels = await staticInformationDal.GetMealLabelByCountryAsync(countryId);
            return mealLabels;
        }

        public async Task<IEnumerable<MealDataRecord>> GetMealLabelForThomasCookAsync(int resortId)
        {
            var mealLabels = await staticInformationDal.GetMealLabelForThomasCookAsync(resortId);
            return mealLabels;
        }

        public async Task<IEnumerable<CountryDataRecord>> GetCountriesAsync(string lang)
        {
            var countries = await staticInformationDal.GetCountriesAsync(lang);
            return countries;
        }

        public async Task<IEnumerable<CountryDataRecord>> GetCountriesByDestinationAsync(string lang, int[] destinationIds)
        {
            var countries = await staticInformationDal.GetCountriesByDestinationAsync(lang, destinationIds);
            return countries;
        }

        public async Task<Dictionary<int, string>> GetHotelGiataCodesAsync()
        {
            var iataCodes = await staticInformationDal.GetHotelGiataCodesAsync();
            return iataCodes;
        }
    }

}