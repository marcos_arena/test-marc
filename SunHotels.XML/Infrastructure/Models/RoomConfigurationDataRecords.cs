﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SunHotels.XML.Infrastructure.Models
{
    public class RoomConfigurationDataRecord
    {
        public int RoomId { get; set; }
        public int RoomConfigurationInfantsAndChildrenId { get; set; }
        public int RoomConfigurationOccupancyId { get; set; }
        public List<int> RoomConfigurationExtrabedFactorIds { get; set; }
        public List<int> RoomConfigurationOccupancyBlockIds { get; set; }
    }

   
    public class RoomStaticDataRecord
    {
        [JsonProperty(PropertyName = "ri")]
        public int RoomId { get; set; }

        [JsonProperty(PropertyName = "hi")]
        public int HotelId { get; set; }

        [JsonProperty(PropertyName = "rt")]
        public string RoomTypeName { get; set; }

        [JsonProperty(PropertyName = "eb")]
        public int ExtraBeds { get; set; }

        [JsonProperty(PropertyName = "b")]
        public int Beds { get; set; }

        [JsonProperty(PropertyName = "rti")]
        public int RoomTypeId { get; set; }

        [JsonProperty(PropertyName = "nf")]
        public bool NoRefundable { get; set; }

        [JsonProperty(PropertyName = "hb")]
        public bool HideExtraBeds { get; set; }

        [JsonProperty(PropertyName = "bb")]
        public bool BestBuy { get; set; }

        [JsonProperty(PropertyName = "sr")]
        public bool SharedRoom { get; set; }

        [JsonProperty(PropertyName = "sf")]
        public bool SharedFacilities { get; set; }

        [JsonProperty(PropertyName = "mr")]
        public bool MealSupplementRestricted { get; set; }

        [JsonProperty(PropertyName = "w")]
        public bool Weekly { get; set; }
    }

    public class RoomOccupancyDataRecord 
    {
        public int Id { get; set; }
        public int MinOccupancy { get; set; }
        public int MaxOccupancy { get; set; }
        public int MinAdults { get; set; }
        public int MaxAdults { get; set; }
        public int MinChildren { get; set; }
        public int MaxChildren { get; set; }
    }

    public class RoomExtraBedFactorsDataRecord
    {
        public int Id { get; set; }
        public int FromAgeInclusive { get; set; }
        public int ToAgeExclusive { get; set; }
        public int FromQuantityInclusive { get; set; }
        public int ToQuantityInclusive { get; set; }
        public decimal Value { get; set; }
        public bool IncludeBoard { get; set; }
    }

    public class RoomOccupancyBlocksDataRecord
    {
        public int Id { get; set; }
        public int NumberOfAdults { get; set; }
        public int NumberOfChildren { get; set; }
    }

    public class RoomInfantsAndChildrenDataRecord
    {
        public int Id { get; set; }
        public bool InfantCountAsChild { get; set; }
        public int MaxChildAge { get; set; }
    }


  }
