﻿namespace SunHotels.XML.Infrastructure.Models
{ 
    public class DestinationDataRecord
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
    }
}
