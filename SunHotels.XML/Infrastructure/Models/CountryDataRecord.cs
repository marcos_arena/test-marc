﻿namespace SunHotels.XML.Infrastructure.Models
{
    public class CountryDataRecord
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public int Id { get; set; }
    }
}