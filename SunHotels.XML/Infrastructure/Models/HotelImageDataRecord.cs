﻿using Newtonsoft.Json;

namespace SunHotels.XML.Infrastructure.Models
{
    public class HotelImageDataRecord
    {
        [JsonProperty(PropertyName = "id")]
        public int ImageId { get; set; }

        [JsonProperty(PropertyName = "sw")]
        public int SmallWidth { get; set; }

        [JsonProperty(PropertyName = "sh")]
        public int SmallHeight { get; set; }

        [JsonProperty(PropertyName = "li")]
        public int LargeID { get; set; }

        [JsonProperty(PropertyName = "lw")]
        public int LargeWidth { get; set; }

        [JsonProperty(PropertyName = "lh")]
        public int LargeHeight { get; set; }

        [JsonProperty(PropertyName = "hid")]
        public int HotelId { get; set; }
    }
}
