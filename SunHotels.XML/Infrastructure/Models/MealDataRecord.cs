﻿namespace SunHotels.XML.Infrastructure
{
    public class MealDataRecord
    {
        public int HotelId { get; set; }
        public int MealId { get; set; }
        public int MealLabelId { get; set; }
        public override int GetHashCode()
        {
            var hashCode = HotelId;
            hashCode = (hashCode * 397) ^ MealId;
            hashCode = (hashCode * 397) ^ MealLabelId;
            return hashCode;
        }
    }
}