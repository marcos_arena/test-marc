﻿namespace SunHotels.XML.Infrastructure.Models
{
    public class HotelDataRecord
    {
        public int id { get; set; }
        public string hotelName { get; set; }
        public string reference { get; set; }
        public string address { get; set; }
        public string telephone { get; set; }
        public string fax { get; set; }
        public string classPlus { get; set; }
        public string sunClass { get; set; }
        public string accType { get; set; }
        public string noRooms { get; set; }
        public string noFloors { get; set; }
        public string distBeach { get; set; }
        public string distCentre { get; set; }
        public string distAirport { get; set; }
        public string taxifareCentre { get; set; }
        public string taxifareAirport { get; set; }
        public string bussfare { get; set; }
        public string roomnote { get; set; }
        public string addr_1 { get; set; }
        public string addr_2 { get; set; }
        public string addr_zip { get; set; }
        public string addr_city { get; set; }
        public string addr_state { get; set; }
        public string addr_country { get; set; }
        public string addr_status { get; set; }
        public double GLat { get; set; }
        public double GLng { get; set; }
        public string notes { get; set; }
        public string features { get; set; }
        public int ShowinSVxml { get; set; }
        public int ShowinNOxml { get; set; }
        public int ShowinENxml { get; set; }
        public int ShowinESxml { get; set; }
        public int ShowinGExml { get; set; }
        public int ShowinFRxml { get; set; }
        public int ShowinPLxml { get; set; }
        public int ShowinRUxml { get; set; }
        public int ShowinFIxml { get; set; }
        public int ShowinDKxml { get; set; }
        public int flag { get; set; }
        public bool bestbuy { get; set; }
        public bool ShowReview { get; set; }
        public decimal ReviewAvgRating { get; set; }
        public decimal ReviewAvgPriceValue { get; set; }
        public int ReviewCount { get; set; }
        public int ReviewRecommendCount { get; set; }
        public int ShowinDK { get; set; }
        public int ShowinEN { get; set; }
        public int ShowinES { get; set; }
        public int ShowinFI { get; set; }
        public int ShowinFR { get; set; }
        public int ShowinGE { get; set; }
        public int ShowinNO { get; set; }
        public int ShowinPL { get; set; }
        public int ShowinRU { get; set; }
        public int ShowinSV { get; set; }
        public string HotelHeadline { get; set; }
        public string HotelDescription { get; set; }
        public int ShowinITxml { get; set; }
        public int ShowinZHxml { get; set; }
        public int ShowinPTxml { get; set; }
        public int ShowinIT { get; set; }
        public int ShowinZH { get; set; }
        public int ShowinPT { get; set; }
        public bool AdultOnly { get; set; }
        public bool BlockInfant { get; set; }

    }
}
