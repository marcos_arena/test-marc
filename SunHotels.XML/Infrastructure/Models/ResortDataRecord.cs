﻿namespace SunHotels.XML.Infrastructure.Models
{
    public class ResortDataRecord
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
