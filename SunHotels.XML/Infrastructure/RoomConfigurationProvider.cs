﻿using SunHotels.XML.Data;
using SunHotels.XML.Infrastructure.Models;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SunHotels.XML.Infrastructure
{

    public class RoomConfigurationProvider : IRoomConfigurationProvider
    {

        private IRoomConfigurationDal roomConfigurationDal;

        private Dictionary<int, RoomConfigurationDataRecord> _roomConfigurationCache;

        public static RoomInfantsAndChildrenDataRecord DefaultInfantChildrenDataRecord
            = new RoomInfantsAndChildrenDataRecord()
            {
                Id = 0,
                InfantCountAsChild = false,
                MaxChildAge = 11
            };


        private RoomConfigurationTables roomConfigurationTables;
        public RoomConfigurationTables CurrentRoomConfiguration
        {
            get
            {
                return roomConfigurationTables;
            }
        }

        public RoomConfigurationProvider(IRoomConfigurationDal roomConfigurationDal)
        {
            this.roomConfigurationDal = roomConfigurationDal;

            roomConfigurationTables = new RoomConfigurationTables();
            roomConfigurationTables.RoomExtraBedFactors = GetExtraBedFactorConfigurations();
            roomConfigurationTables.RoomInfantsAndChildren = GetInfantsAndChildrenConfigurations();
            roomConfigurationTables.RoomOccupancy = GetOccupancyConfigurations();
            roomConfigurationTables.RoomOccupancyBlocks = GetOccupancyBlockConfigurations();

            _roomConfigurationCache = GetAllRoomConfigurations();
        }

        public void FillRootRoomConfiguration(Root root)
        {
            var extrabeds = roomConfigurationTables.GetRoomExtraBedFactorsInUse();
            root.RoomConfigExtrabedFactors = extrabeds
            .Where(t => t.FromAgeInclusive < t.ToAgeExclusive) //CC-3714
            .Select(t => new RoomConfigurationExtrabedFactors()
            {
                Id = t.Id,
                FromAgeInclusive = t.FromAgeInclusive,
                FromQuantityInclusive = t.FromQuantityInclusive,
                IncludedBoard = t.IncludeBoard ? "true" : "false",
                ToAgeExclusive = t.ToAgeExclusive,
                ToQuantityInclusive = t.ToQuantityInclusive,
                Value = t.Value
            }).ToDictionary(t => t.Id, t => t);

            var childrenAges = roomConfigurationTables.GetRoomInfantsAndChildrenInUse();
            root.RoomConfigInfantsAndChildren = childrenAges.Select(t => new RoomConfigurationInfantsAndChildren()
            {
                Id = t.Id,
                InfantCountAsChild = t.InfantCountAsChild,
                MaxChildAgeInclusive = t.MaxChildAge
            }).ToDictionary(t => t.Id, t => t);

            var occupancy = roomConfigurationTables.GetRoomOccupancyInUse();
            root.RoomConfigOccupancies = occupancy.Select(t => new RoomConfigurationOccupancies()
            {
                Id = t.Id,
                MaxAdults = t.MaxAdults,
                MaxChildren = t.MaxChildren,
                MaxOccupancy = t.MaxOccupancy,
                MinAdults = t.MinAdults,
                MinChildren = t.MinChildren,
                MinOccupancy = t.MinOccupancy,

            }).ToDictionary(t => t.Id, t => t);

            var blocks = roomConfigurationTables.GetRoomOccupancyBlocksInUse();
            root.RoomConfigOccupancyBlocks = blocks.Select(t => new RoomConfigurationOccupancyBlocks()
            {
                Id = t.Id,
                NumberOfAdults = t.NumberOfAdults,
                NumberOfChildren = t.NumberOfChildren
            }).ToDictionary(t => t.Id, t => t);

        }

        public Task<bool> FillRoomConfigurationAsync(IEnumerable<Room> rooms, Root root, string lang)
        {
            foreach (var room in rooms.ToList())
            {
                var roomId = int.Parse(room.RoomId);

                RoomConfigurationDataRecord configurationData;
                if (_roomConfigurationCache.TryGetValue(roomId, out configurationData))
                {
                    var extraBedsFactors = configurationData.RoomConfigurationExtrabedFactorIds.Where(t => t != 0).ToList();
                    var OccupancyBlocks = configurationData.RoomConfigurationOccupancyBlockIds.Where(t => t != 0).ToList();
                    var InfantsAndChildrenId = configurationData.RoomConfigurationInfantsAndChildrenId;
                    var OccupancyId = configurationData.RoomConfigurationOccupancyId;

                    room.RoomConfigurationExtrabedFactors = extraBedsFactors;
                    room.RoomConfigurationInfantsAndChildrenId = room.AdultOnlyHotel ? GetMaxConfig(InfantsAndChildrenId) : InfantsAndChildrenId;
                    room.RoomConfigurationOccupancyBlocks = OccupancyBlocks;
                    room.RoomConfigurationOccupancyId = OccupancyId;

                    roomConfigurationTables.RoomInfantsAndChildrenInUse.TryAdd(room.RoomConfigurationInfantsAndChildrenId, 0);
                    roomConfigurationTables.RoomOccupancyInUse.TryAdd(room.RoomConfigurationOccupancyId, 0);

                    configurationData.RoomConfigurationOccupancyBlockIds.ForEach(t =>
                    {
                        roomConfigurationTables.RoomOccupancyBlocksInUse.TryAdd(t, 0);
                    });

                    configurationData.RoomConfigurationExtrabedFactorIds.ForEach(t =>
                    {
                        roomConfigurationTables.RoomExtraBedFactorsInUse.TryAdd(t, 0);
                    });
                }
                else
                {
                    RoomType roomType;
                    if (root.RoomTypes.TryGetValue(room.TypeId, out roomType))
                    {
                        var occupancy = GetRoomOccupancy(roomType);
                        var infantAndChildren = GetInfantAndChildrenByDefault();
                        room.RoomConfigurationInfantsAndChildrenId = room.AdultOnlyHotel ? GetMaxConfig(infantAndChildren.Id) : infantAndChildren.Id;
                        room.RoomConfigurationOccupancyId = occupancy.Id;
                        roomConfigurationTables.RoomInfantsAndChildrenInUse.TryAdd(room.RoomConfigurationInfantsAndChildrenId, 0);
                        roomConfigurationTables.RoomOccupancyInUse.TryAdd(room.RoomConfigurationOccupancyId, 0);
                    }
                }
            }

            return Task.FromResult(true);
        }

        private Dictionary<int, RoomConfigurationDataRecord> GetAllRoomConfigurations()
        {
            Dictionary<int, RoomConfigurationDataRecord> roomConfigurations = null;

            Task.Run(async () =>
            {
                var result = await roomConfigurationDal.GetAllRoomConfigurationAsync();
                roomConfigurations = result.ToDictionary(t => t.RoomId, t => t);
            }).Wait();

            return roomConfigurations;
        }

        private ConcurrentDictionary<int, RoomInfantsAndChildrenDataRecord> GetInfantsAndChildrenConfigurations()
        {
            IEnumerable<RoomInfantsAndChildrenDataRecord> records = null;
            Task.Run(async () =>
            {
                records = await roomConfigurationDal.GetRoomInfantsAndChildrenAsync();
            }).Wait();

            return new ConcurrentDictionary<int, RoomInfantsAndChildrenDataRecord>(records.ToDictionary(t => t.Id, t => t));
        }

        private ConcurrentDictionary<int, RoomOccupancyBlocksDataRecord> GetOccupancyBlockConfigurations()
        {
            IEnumerable<RoomOccupancyBlocksDataRecord> records = null;
            Task.Run(async () =>
            {
                records = await roomConfigurationDal.GetRoomOccupancyBlocksAsync();
            }).Wait();

            return new ConcurrentDictionary<int, RoomOccupancyBlocksDataRecord>(records.ToDictionary(t => t.Id, t => t));
        }

        private ConcurrentDictionary<int, RoomExtraBedFactorsDataRecord> GetExtraBedFactorConfigurations()
        {
            IEnumerable<RoomExtraBedFactorsDataRecord> records = null;
            Task.Run(async () =>
            {
                records = await roomConfigurationDal.GetRoomConfigurationExtraBedFactorsAsync();
            }).Wait();

            return new ConcurrentDictionary<int, RoomExtraBedFactorsDataRecord>(records.ToDictionary(t => t.Id, t => t));
        }

        private ConcurrentDictionary<int, RoomOccupancyDataRecord> GetOccupancyConfigurations()
        {
            IEnumerable<RoomOccupancyDataRecord> records = null;
            Task.Run(async () =>
            {
                records = await roomConfigurationDal.GetRoomConfigurationOccupanciesAsync();
            }).Wait();

            return new ConcurrentDictionary<int, RoomOccupancyDataRecord>(records.ToDictionary(t => t.Id, t => t));
        }

        private RoomInfantsAndChildrenDataRecord GetInfantAndChildrenByDefault()
        {
            if (DefaultInfantChildrenDataRecord.Id == 0)
            {
                var infantAndChildren = roomConfigurationTables.RoomInfantsAndChildren.Where(t => AreEqual(t, DefaultInfantChildrenDataRecord)).FirstOrDefault();
                if (infantAndChildren.Value != null)
                {
                    DefaultInfantChildrenDataRecord.Id = infantAndChildren.Value.Id;
                }
                else
                {
                    DefaultInfantChildrenDataRecord.Id = roomConfigurationTables.RoomInfantsAndChildren.Keys.Max() + 1;
                    roomConfigurationTables.RoomInfantsAndChildren.TryAdd(DefaultInfantChildrenDataRecord.Id, DefaultInfantChildrenDataRecord);
                }
            }

            return DefaultInfantChildrenDataRecord;
        }

        private int GetMaxConfig(int correspondingConfigId)
        {
            if (roomConfigurationTables.RoomInfantsAndChildren.TryGetValue(correspondingConfigId, out var correspondingConfig) && correspondingConfig.MaxChildAge > 17)
                return GetConfigId(true, correspondingConfig.MaxChildAge);

            else
                return GetConfigId(true, 17);
        }

        private object _obj = new object();

        private int GetConfigId(bool infantCountAsChild, int maxChildAge)
        {
            var searchConfig = new RoomInfantsAndChildrenDataRecord()
            {
                InfantCountAsChild = infantCountAsChild,
                MaxChildAge = maxChildAge
            };

            var match = roomConfigurationTables.RoomInfantsAndChildren.Where(t => AreEqual(t, searchConfig)).FirstOrDefault();

            if (match.Value != null)
                searchConfig.Id = match.Value.Id;

            else
                lock (_obj)
                {
                    match = roomConfigurationTables.RoomInfantsAndChildren.Where(t => AreEqual(t, searchConfig)).FirstOrDefault();

                    if (match.Value != null)
                        searchConfig.Id = match.Value.Id;

                    else
                    {
                        searchConfig.Id = roomConfigurationTables.RoomInfantsAndChildren.Keys.Max() + 1;
                        roomConfigurationTables.RoomInfantsAndChildren.TryAdd(searchConfig.Id, searchConfig);
                    }
                }

            return searchConfig.Id;
        }

        private bool AreEqual(KeyValuePair<int, RoomInfantsAndChildrenDataRecord> t, RoomInfantsAndChildrenDataRecord compair)
        {
            return t.Value.InfantCountAsChild == compair.InfantCountAsChild
                && t.Value.MaxChildAge == compair.MaxChildAge;
        }

        public RoomOccupancyDataRecord GetRoomOccupancy(RoomType roomType)
        {
            var beds = roomType.Beds;
            var extraBeds = roomType.ExtraBeds;

            var occupancyDummy = new RoomOccupancyDataRecord()
            {
                MaxOccupancy = beds + extraBeds,
                MinOccupancy = 1,
                MinAdults = 1,
                MaxAdults = beds + extraBeds,
                MinChildren = 0,
                MaxChildren = beds + extraBeds - 1
            };


            var occupancy = roomConfigurationTables.RoomOccupancy.Where(t => AreEqual(t, occupancyDummy)).FirstOrDefault();
            if (occupancy.Value != null)
                return occupancy.Value;

            occupancyDummy.Id = roomConfigurationTables.RoomOccupancy.Keys.Max() + 1;
            roomConfigurationTables.RoomOccupancy.TryAdd(occupancyDummy.Id, occupancyDummy);
            return occupancyDummy;
        }

        private bool AreEqual(KeyValuePair<int, RoomOccupancyDataRecord> t, RoomOccupancyDataRecord temp)
        {
            return t.Value.MaxAdults == temp.MaxAdults &&
                t.Value.MaxChildren == temp.MaxChildren &&
                t.Value.MaxOccupancy == temp.MaxOccupancy &&
                t.Value.MinAdults == temp.MinAdults &&
                t.Value.MinChildren == temp.MinChildren &&
                t.Value.MinOccupancy == temp.MinOccupancy;
        }

    }
}
