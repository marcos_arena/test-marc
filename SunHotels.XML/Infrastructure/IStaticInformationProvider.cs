﻿using SunHotels.XML.Infrastructure.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SunHotels.XML.Infrastructure
{
    public interface IStaticInformationProvider
    {
        Task<IEnumerable<DestinationDataRecord>> GetCountryDestinationAsync(int countryID, string language);
        Task<IEnumerable<DestinationDataRecord>> GetCountryDestinationAsync(int countryID, int[] destinationIds, string language);
        Task<IEnumerable<RoomStaticDataRecord>> GetResortRoomsAsync(int resortId, string language);
        Task<IEnumerable<RoomStaticDataRecord>> GetCountryRoomStaticAsync(int countryId);
        Task<IEnumerable<HotelDataRecord>> GetCountryHotelsAsync(int countryId);
        Task<IEnumerable<HotelDataRecord>> GetCountryHotelsAsync(int resortId, string language);
        Task<IEnumerable<HotelImageDataRecord>> GetHotelImageByResortAsync(int resortId);
        Task<IEnumerable<HotelImageDataRecord>> GetHotelImageByCountryAsync(int countryId);
        Task<IEnumerable<ResortDataRecord>> GetResortsAsync(int destinationId, string language, bool useOldResort, bool includeAllResorts);
        Task<IEnumerable<MealDataRecord>> GetMealLabelsAsync();
        Task<IEnumerable<MealDataRecord>> GetMealLabelsByCountryAsync(int countryId);
        Task<IEnumerable<MealDataRecord>> GetMealLabelForThomasCookAsync(int resortId);
        Task<IEnumerable<CountryDataRecord>> GetCountriesAsync(string lang);
        Task<IEnumerable<CountryDataRecord>> GetCountriesByDestinationAsync(string lang, int[] destinationIds);
        Task<Dictionary<int, string>> GetHotelGiataCodesAsync();
    }
}