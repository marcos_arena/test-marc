﻿using SunHotels.XML.Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SunHotels.XML.Infrastructure
{
    public class RoomConfigurationDal : IRoomConfigurationDal
    {
        public string ConnectionString { get; private set; }
        public int CommandTimeout { get; set; }

        public RoomConfigurationDal(string connectionString)
        {
            ConnectionString = connectionString;
            CommandTimeout = 3600;
        }

        public async Task<IEnumerable<RoomConfigurationDataRecord>> GetAllRoomConfigurationAsync()
        {
            var query = @"SELECT
                    ic.RoomId,
                    ic.RoomConfigurationInfantsAndChildrenId,
                    oc.RoomConfigurationOccupancyId,
                    ISNULL(ef.RoomConfigurationExtrabedFactorId, 0) as RoomConfigurationExtrabedFactorId,
                    ISNULL(ob.RoomConfigurationOccupancyBlockId, 0) as RoomConfigurationOccupancyBlockId
                FROM rooms tp 
                    INNER JOIN dbo.Rooms_RoomConfigurationInfantsAndChildren ic WITH (NOLOCK) ON tp.id = ic.RoomId
	                INNER JOIN Rooms_RoomConfigurationOccupancies oc WITH (NOLOCK) ON oc.RoomId = ic.RoomId
	                LEFT JOIN Rooms_RoomConfigurationExtrabedFactors ef WITH (NOLOCK) ON ef.RoomId = ic.RoomId
	                LEFT JOIN Rooms_RoomConfigurationOccupancyBlocks ob WITH (NOLOCK) ON ob.RoomId = ic.RoomId";

            var roomConfigurations = new Dictionary<int, RoomConfigurationDataRecord>();

            using (var con = new SqlConnection(ConnectionString))
            {
                try
                {
                    using (var command = new SqlCommand(query.ToString(), con))
                    {
                        command.CommandTimeout = CommandTimeout;

                        await con.OpenAsync();
                        using (var reader = await command.ExecuteReaderAsync())
                        {
                            while (await reader.ReadAsync())
                            {
                                ProcessRoomConfigurationData(roomConfigurations, reader);
                            }

                        }
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    if (con.State == System.Data.ConnectionState.Open)
                        con.Close();
                }

            }

            return roomConfigurations.Select(t => t.Value);

        }

        public async Task<IEnumerable<RoomConfigurationDataRecord>> GetRoomConfigurationAsync(int[] roomIds)
        {
            var query = new StringBuilder();
            var temporalQuery = DalExtensions.CreateTemporalTableQuery("temporalRooms", "RoomId", "INT", "({0})", roomIds);
            query.AppendLine(temporalQuery);
            query.Append($@"
                SELECT
                    ic.RoomId,
                    ic.RoomConfigurationInfantsAndChildrenId,
                    oc.RoomConfigurationOccupancyId,
                    ISNULL(ef.RoomConfigurationExtrabedFactorId, 0) as RoomConfigurationExtrabedFactorId,
                    ISNULL(ob.RoomConfigurationOccupancyBlockId, 0) as RoomConfigurationOccupancyBlockId
                FROM @temporalRooms tp 
                    INNER JOIN dbo.Rooms_RoomConfigurationInfantsAndChildren ic WITH (NOLOCK) ON tp.RoomId = ic.RoomId
	                INNER JOIN Rooms_RoomConfigurationOccupancies oc WITH (NOLOCK) ON oc.RoomId = ic.RoomId
	                LEFT JOIN Rooms_RoomConfigurationExtrabedFactors ef WITH (NOLOCK) ON ef.RoomId = ic.RoomId
	                LEFT JOIN Rooms_RoomConfigurationOccupancyBlocks ob WITH (NOLOCK) ON ob.RoomId = ic.RoomId ");

            var roomConfigurations = new Dictionary<int, RoomConfigurationDataRecord>();

            using (var con = new SqlConnection(ConnectionString))
            {
                try
                {
                    using (var command = new SqlCommand(query.ToString(), con))
                    {
                        command.CommandTimeout = CommandTimeout;

                        await con.OpenAsync();
                        using (var reader = await command.ExecuteReaderAsync())
                        {
                            while (await reader.ReadAsync())
                            {
                                ProcessRoomConfigurationData(roomConfigurations, reader);
                            }

                        }
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    if (con.State == System.Data.ConnectionState.Open)
                        con.Close();
                }

            }

            return roomConfigurations.Select(t => t.Value);
        }

        private static void ProcessRoomConfigurationData(Dictionary<int, RoomConfigurationDataRecord> roomConfigurations, SqlDataReader reader)
        {
            var roomId = (int)reader["RoomId"];
            RoomConfigurationDataRecord conf = null;
            if (roomConfigurations.TryGetValue(roomId, out conf))
            {
                var extraBedFactor = (int)reader["RoomConfigurationExtrabedFactorId"];
                var occupancyBlock = (int)reader["RoomConfigurationOccupancyBlockId"];
                if (!conf.RoomConfigurationExtrabedFactorIds.Any(t => t == extraBedFactor))
                    conf.RoomConfigurationExtrabedFactorIds.Add(extraBedFactor);
                if (!conf.RoomConfigurationOccupancyBlockIds.Any(t => t == occupancyBlock))
                    conf.RoomConfigurationOccupancyBlockIds.Add(occupancyBlock);
            }
            else
            {
                roomConfigurations.Add(roomId, new RoomConfigurationDataRecord()
                {
                    RoomConfigurationExtrabedFactorIds = new List<int>() { (int)reader["RoomConfigurationExtrabedFactorId"] },
                    RoomConfigurationOccupancyBlockIds = new List<int>() { (int)reader["RoomConfigurationOccupancyBlockId"] },
                    RoomConfigurationInfantsAndChildrenId = (int)reader["RoomConfigurationInfantsAndChildrenId"],
                    RoomConfigurationOccupancyId = (int)reader["RoomConfigurationOccupancyId"],
                    RoomId = roomId,
                });
            }
        }



        public async Task<IEnumerable<RoomOccupancyDataRecord>> GetRoomConfigurationOccupanciesAsync()
        {
            var query = @"SELECT 
                Id,
                MinOccupancy,
                MaxOccupancy,
                MinAdults,
                MaxAdults,
                MinChildren,
                MaxChildren
            FROM RoomConfigurationOccupancies WITH(NOLOCK)";

            var result = new List<RoomOccupancyDataRecord>();
            using (var con = new SqlConnection(ConnectionString))
            {
                try
                {
                    using (var command = new SqlCommand(query, con))
                    {
                        command.CommandTimeout = CommandTimeout;

                        await con.OpenAsync();
                        using (var reader = await command.ExecuteReaderAsync())
                        {
                            while (await reader.ReadAsync())
                            {
                                result.Add(new RoomOccupancyDataRecord()
                                {
                                    Id = (int)reader["Id"],
                                    MinOccupancy = (int)reader["MinOccupancy"],
                                    MaxOccupancy = (int)reader["MaxOccupancy"],
                                    MinAdults = (int)reader["MinAdults"],
                                    MaxAdults = (int)reader["MaxAdults"],
                                    MinChildren = (int)reader["MinChildren"],
                                    MaxChildren = (int)reader["MaxChildren"]
                                });

                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    if (con.State == System.Data.ConnectionState.Open)
                        con.Close();
                }

            }

            return result;
        }

        public async Task<IEnumerable<RoomExtraBedFactorsDataRecord>> GetRoomConfigurationExtraBedFactorsAsync()
        {
            var query = @"SELECT 
                Id,
                FromAgeInclusive,
                ToAgeExclusive,
                FromQuantityInclusive,
                ToQuantityInclusive,
                Value,
                IncludeBoard
            FROM RoomConfigurationExtrabedFactors WITH(NOLOCK)";

            var result = new List<RoomExtraBedFactorsDataRecord>();
            using (var con = new SqlConnection(ConnectionString))
            {
                try
                {
                    using (var command = new SqlCommand(query, con))
                    {
                        command.CommandTimeout = CommandTimeout;

                        await con.OpenAsync();
                        using (var reader = await command.ExecuteReaderAsync())
                        {
                            while (await reader.ReadAsync())
                            {
                                result.Add(new RoomExtraBedFactorsDataRecord()
                                {
                                    Id = (int)reader["Id"],
                                    FromAgeInclusive = (int)reader["FromAgeInclusive"],
                                    ToAgeExclusive = (int)reader["ToAgeExclusive"],
                                    FromQuantityInclusive = (int)reader["FromQuantityInclusive"],
                                    ToQuantityInclusive = (int)reader["ToQuantityInclusive"],
                                    Value = (decimal)reader["Value"],
                                    IncludeBoard = Convert.ToBoolean(reader["IncludeBoard"])
                                });
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    if (con.State == System.Data.ConnectionState.Open)
                        con.Close();
                }
            }

            return result;
        }

        public async Task<IEnumerable<RoomOccupancyBlocksDataRecord>> GetRoomOccupancyBlocksAsync()
        {
            var query = @"SELECT 
                Id,
                NumberOfAdults,
                NumberOfChildren
            FROM RoomConfigurationOccupancyBlocks WITH(NOLOCK)";

            var result = new List<RoomOccupancyBlocksDataRecord>();
            using (var con = new SqlConnection(ConnectionString))
            {
                try
                {
                    using (var command = new SqlCommand(query, con))
                    {
                        command.CommandTimeout = CommandTimeout;

                        await con.OpenAsync();
                        using (var reader = await command.ExecuteReaderAsync())
                        {
                            while (await reader.ReadAsync())
                            {
                                result.Add(new RoomOccupancyBlocksDataRecord()
                                {
                                    Id = (int)reader["Id"],
                                    NumberOfAdults = (int)reader["NumberOfAdults"],
                                    NumberOfChildren = (int)reader["NumberOfChildren"],
                                });
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    if (con.State == System.Data.ConnectionState.Open)
                        con.Close();
                }
            }

            return result;
        }

        public async Task<IEnumerable<RoomInfantsAndChildrenDataRecord>> GetRoomInfantsAndChildrenAsync()
        {
            var query = @"SELECT 
                Id,
                InfantCountAsChild,
                MaxChildAge
            FROM RoomConfigurationInfantsAndChildren WITH(NOLOCK)";

            var result = new List<RoomInfantsAndChildrenDataRecord>();

            using (var con = new SqlConnection(ConnectionString))
            {
                try
                {
                    using (var command = new SqlCommand(query, con))
                    {
                        command.CommandTimeout = CommandTimeout;

                        await con.OpenAsync();
                        using (var reader = await command.ExecuteReaderAsync())
                        {
                            while (await reader.ReadAsync())
                            {
                                result.Add(new RoomInfantsAndChildrenDataRecord()
                                {
                                    Id = (int)reader["Id"],
                                    InfantCountAsChild = (bool)reader["InfantCountAsChild"],
                                    MaxChildAge = (int)reader["MaxChildAge"],
                                });
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    if (con.State == System.Data.ConnectionState.Open)
                        con.Close();
                }
            }

            return result;
        }
    }
}
