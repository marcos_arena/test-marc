﻿using SunHotels.XML.Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace SunHotels.XML.Infrastructure
{
    public class HotelDal : IHotelDal
    {
        public string ConnectionString { get; private set; }
        public int CommandTimeout { get; set; }

        public HotelDal(string connectionString)
        {
            ConnectionString = connectionString;
            CommandTimeout = 3600;
        }

        private const string HotelDataByCountryWithoutLanguageSQL = @"SELECT h.id, 
                               h.hotelname, 
                               ''                                AS reference, 
                               ''                                AS HotelHeadline, 
                               ''                                AS HotelDescription, 
                               Isnull(h.features, '')            AS features, 
                               ''                                AS address, 
                               h.telephone, 
                               h.fax, 
                               h.classplus, 
                               h.sunclass, 
                               h.acctype, 
                               ''                                AS noRooms, 
                               ''                                AS noFloors, 
                               h.distBeach, 
                               h.distCentre, 
                               h.distAirport, 
                               ''                                AS taxifareCentre, 
                               ''                                AS taxifareAirport, 


               

                               ''                                AS bussfare, 
                               ''                                AS roomnote, 
                               h.addr_1, 
                               h.addr_2, 
                               h.addr_zip, 
                               h.addr_city, 
                               h.addr_state, 
                               h.addr_country, 
                               ''                              AS addr_status, 
                               ''                                AS notes, 
                               IsNull(hc.glat, 0)                AS glat,  
                               IsNull(hc.glng, 0)                AS glng, 
                               Isnull(h.showinenxml, 0)          AS 'ShowinENxml', 
                               Isnull(h.showinen, 0)             AS 'ShowinEN', 

                               0                                    AS                        'ShowinDKxml',  
                               0                                    AS                        'ShowinESxml', 
                               0                                    AS                        'ShowinFIxml', 
                               0                                    AS                        'ShowinFRxml', 
                               0                                    AS                        'ShowinGExml', 
                               0                                    AS                        'ShowinNOxml', 
                               0                                    AS                        'ShowinPLxml', 
                               0                                    AS                        'ShowinRUxml', 
                               0                                    AS                        'ShowinSVxml', 
                               0                                    AS                        'ShowinITxml', 
                               0                                    AS                        'ShowinZHxml', 
                               0                                    AS                        'ShowinPTxml', 
                               0                                    AS                        'ShowinDK',  
                               0                                    AS                        'ShowinES', 
                               0                                    AS                        'ShowinFI', 
                               0                                    AS                        'ShowinFR', 
                               0                                    AS                        'ShowinGE', 
                               0                                    AS                        'ShowinNO', 
                               0                                    AS                        'ShowinPL', 
                               0                                    AS                        'ShowinRU', 
                               0                                    AS                        'ShowinSV', 
                               0                                    AS                        'ShowinIT', 
                               0                                    AS                        'ShowinZH', 
                               0                                    AS                        'ShowinPT',


                               Isnull(h.flag, 0)                 AS flag, 
                               CAST(Isnull(h.hotelbestbuy, 0) AS BIT)         AS bestbuy, 
                               h.showreview, 
                               Isnull(h.reviewavgrating, -1)     AS ReviewAvgRating, 
                               Isnull(h.reviewavgpricevalue, -1) AS ReviewAvgPriceValue, 
                               h.reviewcount, 
                               h.reviewrecommendcount, 
                               h.adultonly, 
                               h.blockinfant 
                        FROM   hotel AS h 
                               INNER JOIN destinations AS d 
                                       ON d.id = h.destination 
                                          AND d.country = @countryID 
                               LEFT OUTER JOIN hotelcoordinates AS hc 
                                            ON h.id = hc.hotelid 
                        WHERE ( h.suspendhotel IS NULL ) 
                                OR ( h.suspendhotel = 0 ) ";

        public async Task<IEnumerable<HotelDataRecord>> GetHotelDataByCountryWithoutLanguageAsync(int countryId)
        {
            var result = new List<HotelDataRecord>();

            using (var con = new SqlConnection(ConnectionString))
            {
                try
                {
                    using (SqlCommand command = new SqlCommand(HotelDataByCountryWithoutLanguageSQL, con))
                    {
                        command.Parameters.Add(new SqlParameter() { Value = countryId, DbType = System.Data.DbType.Int32, SqlDbType = System.Data.SqlDbType.Int, ParameterName = "countryID" });
                        command.CommandTimeout = CommandTimeout;

                        await con.OpenAsync();
                        using (SqlDataReader reader = await command.ExecuteReaderAsync())
                        {
                            while (await reader.ReadAsync())
                                result.Add(ConvertToHotelDataRecord(reader));
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    if (con.State == System.Data.ConnectionState.Open)
                        con.Close();
                }
            }

            return result;
        }

        private static HotelDataRecord ConvertToHotelDataRecord(SqlDataReader reader)
        {
            return new HotelDataRecord()
            {
                id = reader.ValueOrDefault<int>("id"),
                hotelName = reader.ValueOrDefault<string>("hotelName"),
                reference = reader.ValueOrDefault<string>("reference"),
                HotelHeadline = reader.ValueOrDefault<string>("HotelHeadline"),
                HotelDescription = reader.ValueOrDefault<string>("HotelDescription"),
                features = reader.ValueOrDefault<string>("features"),
                address = reader.ValueOrDefault<string>("address"),
                telephone = reader.ValueOrDefault<string>("telephone"),
                fax = reader.ValueOrDefault<string>("fax"),
                classPlus = reader.ValueOrDefault<string>("classPlus"),
                sunClass = reader.ValueOrDefault<string>("sunClass"),
                accType = reader.ValueOrDefault<string>("accType"),
                noRooms = reader.ValueOrDefault<string>("noRooms"),
                noFloors = reader.ValueOrDefault<string>("noFloors"),
                distBeach = reader.ValueOrDefault<string>("distBeach"),
                distCentre = reader.ValueOrDefault<string>("distCentre"),
                distAirport = reader.ValueOrDefault<string>("distAirport"),
                taxifareCentre = reader.ValueOrDefault<string>("taxifareCentre"),
                taxifareAirport = reader.ValueOrDefault<string>("taxifareAirport"),
                bussfare = reader.ValueOrDefault<string>("bussfare"),
                roomnote = reader.ValueOrDefault<string>("roomnote"),
                addr_1 = reader.ValueOrDefault<string>("addr_1"),
                addr_2 = reader.ValueOrDefault<string>("addr_2"),
                addr_zip = reader.ValueOrDefault<string>("addr_zip"),
                addr_city = reader.ValueOrDefault<string>("addr_city"),
                addr_state = reader.ValueOrDefault<string>("addr_state"),
                addr_country = reader.ValueOrDefault<string>("addr_country"),
                addr_status = reader.ValueOrDefault<string>("addr_status"),
                notes = reader.ValueOrDefault<string>("notes"),
                GLat = reader.ValueOrDefault<double>("GLat"),
                GLng = reader.ValueOrDefault<double>("GLng"),
                ShowinENxml = reader.ValueOrDefault<int>("ShowinENxml"),
                ShowinEN = reader.ValueOrDefault<int>("ShowinEN"),
                flag = reader.ValueOrDefault<int>("flag"),
                bestbuy = reader.ValueOrDefault<bool>("bestbuy"),
                ShowReview = reader.ValueOrDefault<bool>("ShowReview"),
                ReviewAvgRating = reader.ValueOrDefault<decimal>("ReviewAvgRating"),
                ReviewAvgPriceValue = reader.ValueOrDefault<decimal>("ReviewAvgPriceValue"),
                ReviewCount = reader.ValueOrDefault<int>("ReviewCount"),
                ReviewRecommendCount = reader.ValueOrDefault<int>("ReviewRecommendCount"),
                AdultOnly = reader.ValueOrDefault<bool>("AdultOnly"),
                BlockInfant = reader.ValueOrDefault<bool>("BlockInfant"),
                ShowinDK = reader.ValueOrDefault<int>("ShowinDK"),
                ShowinDKxml = reader.ValueOrDefault<int>("ShowinDKxml"),
                ShowinES = reader.ValueOrDefault<int>("ShowinES"),
                ShowinESxml = reader.ValueOrDefault<int>("ShowinESxml"),
                ShowinFI = reader.ValueOrDefault<int>("ShowinFI"),
                ShowinFIxml = reader.ValueOrDefault<int>("ShowinFIxml"),
                ShowinFR = reader.ValueOrDefault<int>("ShowinFR"),
                ShowinFRxml = reader.ValueOrDefault<int>("ShowinFRxml"),
                ShowinGE = reader.ValueOrDefault<int>("ShowinGE"),
                ShowinGExml = reader.ValueOrDefault<int>("ShowinGExml"),
                ShowinIT = reader.ValueOrDefault<int>("ShowinIT"),
                ShowinITxml = reader.ValueOrDefault<int>("ShowinITxml"),
                ShowinNO = reader.ValueOrDefault<int>("ShowinNO"),
                ShowinNOxml = reader.ValueOrDefault<int>("ShowinNOxml"),
                ShowinPL = reader.ValueOrDefault<int>("ShowinPL"),
                ShowinPLxml = reader.ValueOrDefault<int>("ShowinPLxml"),
                ShowinPT = reader.ValueOrDefault<int>("ShowinPT"),
                ShowinPTxml = reader.ValueOrDefault<int>("ShowinPTxml"),
                ShowinRU = reader.ValueOrDefault<int>("ShowinRU"),
                ShowinRUxml = reader.ValueOrDefault<int>("ShowinRUxml"),
                ShowinSV = reader.ValueOrDefault<int>("ShowinSV"),
                ShowinSVxml = reader.ValueOrDefault<int>("ShowinSVxml"),
                ShowinZH = reader.ValueOrDefault<int>("ShowinZH"),
                ShowinZHxml = reader.ValueOrDefault<int>("ShowinZHxml")
            };
        }



        const string HotelDataSQL = @"
                SELECT h.id, 
                       h.hotelname, 
                       h.reference, 
                       Isnull(Isnull(NULLIF (dbo.Getlanghotelheadline(h.id, @lang), ''), dbo.Getlanghotelheadline(h.id, N'en')), '') AS HotelHeadline, 
                       Isnull(Isnull(NULLIF (dbo.Getlanghoteldescription(h.id, @lang), ''), dbo.Getlanghoteldescription(h.id, N'en')), '') AS  HotelDescription, 
                       Isnull(h.features, '') AS features, 
                       h.address, 
                       h.telephone, 
                       h.fax, 
                       h.classplus, 
                       h.sunclass, 
                       h.acctype, 
                       h.norooms, 
                       h.nofloors, 
 
                       h.distbeach, 
                       h.distcentre, 
                       h.distairport, 
                       h.taxifarecentre, 
                       h.taxifareairport, 
                       h.bussfare, 
                       h.roomnote, 
                       h.addr_1, 
                       h.addr_2, 
                       h.addr_zip, 
                       h.addr_city, 
                       h.addr_state, 
                       h.addr_country, 
                       h.addr_status  AS addr_status,
                       h.notes, 
                       IsNull(hc.glat, 0) AS glat, 
                       IsNull(hc.glng, 0) AS glng, 
                       CAST(Isnull(h.showindkxml, 0)  AS BIT)                                     AS                        'ShowinDKxml', 
                       CAST(Isnull(h.showinenxml, 0)  AS BIT)                                    AS                        'ShowinENxml', 
                       CAST(Isnull(h.showinesxml, 0)  AS BIT)                                    AS                        'ShowinESxml', 
                       CAST(Isnull(h.showinfixml, 0)  AS BIT)                                    AS                        'ShowinFIxml', 
                       CAST(Isnull(h.showinfrxml, 0)  AS BIT)                                    AS                        'ShowinFRxml', 
                       CAST(Isnull(h.showingexml, 0)  AS BIT)                                    AS                        'ShowinGExml', 
                       CAST(Isnull(h.showinnoxml, 0)  AS BIT)                                    AS                        'ShowinNOxml', 
                       CAST(Isnull(h.showinplxml, 0)  AS BIT)                                    AS                        'ShowinPLxml', 
                       CAST(Isnull(h.showinruxml, 0)  AS BIT)                                    AS                        'ShowinRUxml', 
                       CAST(Isnull(h.showinsvxml, 0)  AS BIT)                                    AS                        'ShowinSVxml', 
                       CAST(Isnull(h.showinitxml, 0)  AS BIT)                                    AS                        'ShowinITxml', 
                       CAST(Isnull(h.showinzhxml, 0)  AS BIT)                                    AS                        'ShowinZHxml', 
                       CAST(Isnull(h.showinptxml, 0)  AS BIT)                                    AS                        'ShowinPTxml', 
                       CAST(Isnull(h.showindk, 0)     AS BIT)                                    AS                        'ShowinDK', 
                       CAST(Isnull(h.showinen, 0)     AS BIT)                                    AS                        'ShowinEN', 
                       CAST(Isnull(h.showines, 0)     AS BIT)                                    AS                        'ShowinES', 
                       CAST(Isnull(h.showinfi, 0)     AS BIT)                                    AS                        'ShowinFI', 
                       CAST(Isnull(h.showinfr, 0)     AS BIT)                                    AS                        'ShowinFR', 
                       CAST(Isnull(h.showinge, 0)     AS BIT)                                    AS                        'ShowinGE', 
                       CAST(Isnull(h.showinno, 0)     AS BIT)                                    AS                        'ShowinNO', 
                       CAST(Isnull(h.showinpl, 0)     AS BIT)                                    AS                        'ShowinPL', 
                       CAST(Isnull(h.showinru, 0)     AS BIT)                                    AS                        'ShowinRU', 
                       CAST(Isnull(h.showinsv, 0)     AS BIT)                                    AS                        'ShowinSV', 
                       CAST(Isnull(h.showinit, 0)     AS BIT)                                    AS                        'ShowinIT', 
                       CAST(Isnull(h.showinzh, 0)     AS BIT)                                    AS                        'ShowinZH', 
                       CAST(Isnull(h.showinpt, 0)     AS BIT)                                    AS                        'ShowinPT', 
                       Isnull(h.flag, 0)                                            AS flag, 
                       CAST(Isnull(h.hotelbestbuy, 0) AS BIT)                                    AS bestbuy,                        
                       CAST(Isnull(h.showreview, 0) AS BIT) AS showreview, 
                       Isnull(h.reviewavgrating, -1)                                AS                        ReviewAvgRating, 
                       Isnull(h.reviewavgpricevalue, -1)                            AS                        ReviewAvgPriceValue, 
                       h.reviewcount, 
                       h.reviewrecommendcount, 
                       h.adultonly, 
                       h.blockinfant 
                FROM   hotel AS h 
                       INNER JOIN resorts 
                               ON resorts.id = h.resort 
                                  AND ( resorts.id = @resortID 
                                         OR resorts.parent = @resortID ) 
                       LEFT OUTER JOIN hotelcoordinates AS hc 
                                    ON h.id = hc.hotelid 
                WHERE  ( h.suspendhotel IS NULL ) 
                        OR ( h.suspendhotel = 0 ) ";


        public async Task<IEnumerable<HotelDataRecord>> GetHotelDataByResortIdAsync(int resortID, string language)
        {
            var result = new List<HotelDataRecord>();
            using (var con = new SqlConnection(ConnectionString))
            {
                try
                {
                    using (SqlCommand command = new SqlCommand(HotelDataSQL, con))
                    {
                        command.Parameters.Add(new SqlParameter() { Value = resortID, DbType = System.Data.DbType.Int32, SqlDbType = System.Data.SqlDbType.Int, ParameterName = "resortID" });
                        command.Parameters.Add(new SqlParameter() { Value = language, DbType = System.Data.DbType.String, SqlDbType = System.Data.SqlDbType.VarChar, ParameterName = "lang" });
                        command.CommandTimeout = CommandTimeout;

                        await con.OpenAsync();
                        using (SqlDataReader reader = await command.ExecuteReaderAsync())
                        {
                            while (await reader.ReadAsync())
                                result.Add(ConvertToHotelDataRecord(reader));
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    if (con.State == System.Data.ConnectionState.Open)
                        con.Close();
                }

            }

            return result;
        }


    }

}
