﻿using SunHotels.XML.Infrastructure.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SunHotels.XML.Infrastructure
{
    public interface IRoomConfigurationDal
    {
        Task<IEnumerable<RoomConfigurationDataRecord>> GetAllRoomConfigurationAsync();
        Task<IEnumerable<RoomConfigurationDataRecord>> GetRoomConfigurationAsync(int[] roomIds);
        Task<IEnumerable<RoomExtraBedFactorsDataRecord>> GetRoomConfigurationExtraBedFactorsAsync();
        Task<IEnumerable<RoomOccupancyDataRecord>> GetRoomConfigurationOccupanciesAsync();
        Task<IEnumerable<RoomInfantsAndChildrenDataRecord>> GetRoomInfantsAndChildrenAsync();
        Task<IEnumerable<RoomOccupancyBlocksDataRecord>> GetRoomOccupancyBlocksAsync();

    }
}