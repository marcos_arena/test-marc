﻿using SunHotels.XML.Infrastructure.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SunHotels.XML.Infrastructure
{
    public interface IHotelDal
    {
        Task<IEnumerable<HotelDataRecord>> GetHotelDataByCountryWithoutLanguageAsync(int countryId);
        Task<IEnumerable<HotelDataRecord>> GetHotelDataByResortIdAsync(int resortID, string language);
    }
}