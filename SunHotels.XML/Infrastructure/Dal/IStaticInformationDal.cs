﻿using SunHotels.XML.Infrastructure.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SunHotels.XML.Infrastructure
{
    public interface IStaticInformationDal
    {
        Task<IEnumerable<DestinationDataRecord>> GetAllDestinations(string language);
        Task<IEnumerable<DestinationDataRecord>> GetDestinationsByCountryData(int countryId, string language);
        Task<IEnumerable<DestinationDataRecord>> GetDestinationsByCountryData(int countryID, int[] destinationIds,string language);
        Task<IEnumerable<ResortDataRecord>> GetResorts(int destinationId, string language, bool useOldResort, bool includeAllResorts);

        Task<IEnumerable<RoomStaticDataRecord>> GetRoomStaticAsync(int[] roomIds, string lang);
        Task<IEnumerable<RoomStaticDataRecord>> GetResortRoomStaticAsync(int resortId, string language);
        Task<IEnumerable<RoomStaticDataRecord>> GetCountryRoomStaticAsync(int countryId);
        Task<IEnumerable<HotelImageDataRecord>> GetHotelImageByResortAsync(int resortId);
        Task<IEnumerable<HotelImageDataRecord>> GetHotelImageByCountryAsync(int countryId);

        Task<IEnumerable<MealDataRecord>> GetMealLabelByCountryAsync(int countryId);
        Task<IEnumerable<MealDataRecord>> GetMealLabelForThomasCookAsync(int resortId);

        Task<IEnumerable<CountryDataRecord>> GetCountriesAsync(string lang);
        Task<IEnumerable<CountryDataRecord>> GetCountriesByDestinationAsync(string lang, int[] destinationIds);
        Task<Dictionary<int, string>> GetHotelGiataCodesAsync();
    }
}