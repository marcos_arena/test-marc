﻿using SunHotels.XML.Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace SunHotels.XML.Infrastructure
{
    public class StaticInformationDal : IStaticInformationDal
    {
        public string ConnectionString { get; private set; }
        public int CommandTimeout { get; set; }

        public StaticInformationDal(string connectionString)
        {
            ConnectionString = connectionString;
            CommandTimeout = 6 * 60;//timeout in seconds
        }

        private const string DestinationSQL = @"
        SELECT 
            destinations.id as Id, 
            Isnull(NULLIF (dbo.Getlangdestinationbydestinationid(destinations.id, @lang), ''), dbo.Getlangdestinationbydestinationid(destinations.id, N'en'))  AS Name, 
            destinations.destination_code As Code
        FROM destinations 
               INNER JOIN countries 
                       ON destinations.country = countries.id 
        WHERE  ( countries.id = @countryID OR @countryID = 0 ) 
               AND ( Isnull(destinations.suspend, 0) = 0 ) 
               AND ( Isnull(countries.suspend, 0) = 0 ) ";


        public Task<IEnumerable<DestinationDataRecord>> GetAllDestinations(string language)
        {
            var countryId = 0;
            return GetDestinationsByCountryData(countryId, language);
        }


        public async Task<IEnumerable<DestinationDataRecord>> GetDestinationsByCountryData(int countryId, int[] destinationIds, string language)
        {
            var query = $"{DestinationSQL} AND destinations.Id IN({string.Join(",", destinationIds)}) ";
            var result = new List<DestinationDataRecord>();

            using (var con = new SqlConnection(ConnectionString))
            {
                try
                {
                    using (var command = new SqlCommand(query, con))
                    {
                        command.Parameters.Add(new SqlParameter() { Value = countryId, DbType = System.Data.DbType.Int32, SqlDbType = System.Data.SqlDbType.Int, ParameterName = "countryID" });
                        command.Parameters.Add(new SqlParameter() { Value = language, DbType = System.Data.DbType.String, SqlDbType = System.Data.SqlDbType.VarChar, ParameterName = "lang" });
                        command.CommandTimeout = CommandTimeout;

                        await con.OpenAsync();
                        using (var reader = await command.ExecuteReaderAsync())
                        {
                            while (await reader.ReadAsync())
                                result.Add(ConvertToDestinationDataRecord(reader));
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    if (con.State == System.Data.ConnectionState.Open)
                        con.Close();
                }

            }

            return result;
        }

        private static DestinationDataRecord ConvertToDestinationDataRecord(SqlDataReader reader)
        {
            return new DestinationDataRecord()
            {
                Id = reader.ValueOrDefault<int>("Id"),
                Code = reader.ValueOrDefault<string>("Code"),
                Name = reader.ValueOrDefault<string>("Name")
            };
        }

        public async Task<IEnumerable<DestinationDataRecord>> GetDestinationsByCountryData(int countryId, string language)
        {
            var result = new List<DestinationDataRecord>();

            using (var con = new SqlConnection(ConnectionString))
            {
                try
                {
                    using (var command = new SqlCommand(DestinationSQL, con))
                    {
                        command.Parameters.Add(new SqlParameter() { Value = countryId, DbType = System.Data.DbType.Int32, SqlDbType = System.Data.SqlDbType.Int, ParameterName = "countryID" });
                        command.Parameters.Add(new SqlParameter() { Value = language, DbType = System.Data.DbType.String, SqlDbType = System.Data.SqlDbType.VarChar, ParameterName = "lang" });
                        command.CommandTimeout = CommandTimeout;

                        await con.OpenAsync();
                        using (var reader = await command.ExecuteReaderAsync())
                        {
                            while (await reader.ReadAsync())
                                result.Add(ConvertToDestinationDataRecord(reader));
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    if (con.State == System.Data.ConnectionState.Open)
                        con.Close();
                }

            }

            return result;
        }


        private const string ResortsSQL = @"
        SELECT resorts.id as Id, 
               Isnull(NULLIF (dbo.Getlangresortbyresortid(resorts.id, @lang), ''), 
               dbo.Getlangresortbyresortid(resorts.id, N'en')) AS Name 
        FROM   resorts WITH(NOLOCK)
               INNER JOIN destinations  WITH(NOLOCK)
                       ON destinations.id = resorts.destid 
        WHERE  ( resorts.destid = @DEST_ID ) 
               AND ( Isnull(resorts.suspend, 0) = 0 ) 
               AND ( @useOldResortId = 0 
                      OR @includeAllResorts = 1 ) 
               AND ( resorts.id NOT IN (SELECT parent 
                                        FROM   resorts AS Resorts_1 WITH(NOLOCK) 
                                        WHERE  ( NOT ( parent IS NULL ) ) 
                                               AND ( destid = @DEST_ID )) ) 
                OR ( resorts.destid = @DEST_ID ) 
                   AND ( Isnull(resorts.suspend, 0) = 0 ) 
                   AND ( @useOldResortId = 1 
                          OR @includeAllResorts = 1 ) 
                   AND ( resorts.parent IS NULL ) ";
        public async Task<IEnumerable<ResortDataRecord>> GetResorts(int destinationId, string language, bool useOldResort, bool includeAllResorts)
        {
            var result = new List<ResortDataRecord>();

            using (var con = new SqlConnection(ConnectionString))
            {
                try
                {
                    using (var command = new SqlCommand(ResortsSQL, con))
                    {
                        command.Parameters.Add(new SqlParameter() { Value = destinationId, DbType = System.Data.DbType.Int32, ParameterName = "DEST_ID" });
                        command.Parameters.Add(new SqlParameter() { Value = language, DbType = System.Data.DbType.String, ParameterName = "lang" });
                        command.Parameters.Add(new SqlParameter() { Value = useOldResort, DbType = System.Data.DbType.Boolean, ParameterName = "useOldResortId" });
                        command.Parameters.Add(new SqlParameter() { Value = includeAllResorts, DbType = System.Data.DbType.Boolean, ParameterName = "includeAllResorts" });
                        command.CommandTimeout = CommandTimeout;

                        await con.OpenAsync();
                        using (var reader = await command.ExecuteReaderAsync())
                        {
                            while (await reader.ReadAsync())
                                result.Add(new ResortDataRecord()
                                {
                                    Id = reader.ValueOrDefault<int>("Id"),
                                    Name = reader.ValueOrDefault<string>("Name")
                                });
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    if (con.State == System.Data.ConnectionState.Open)
                        con.Close();
                }

            }

            return result;
        }

        public async Task<IEnumerable<RoomStaticDataRecord>> GetRoomStaticAsync(int[] roomIds, string lang)
        {
            var query = new StringBuilder();
            var temporalQuery = DalExtensions.CreateTemporalTableQuery("temporalRooms", "RoomId", "INT", "({0})", roomIds);
            query.AppendLine(temporalQuery);
            query.Append($@"
                SELECT
	                r.ID as RoomId, 
	                r.hotelId as HotelId, 
	                ISNULL(NULLIF (dbo.GetLangRoomTypeOnRoomTypeID(rt.id, '{lang}'), ''), dbo.GetLangRoomTypeOnRoomTypeID(rt.id, N'en')) AS RoomTypeName, 
	                r.extraBeds as ExtraBeds, 
	                r.beds as Beds, 
	                r.roomtypes_id as RoomTypeId,  
	                CAST(ISNULL(r.norefund, 0) AS BIT) AS  NoRefundable, 
	                CAST(ISNULL(r.hideextrabeds, 0) AS BIT) HideExtraBeds, 
	                CAST(ISNULL(r.RoomBestBuy, 0) AS BIT) AS  BestBuy,
	                CAST(ISNULL(rt.sharedRoom, 0) AS BIT) AS SharedRoom, 
	                CAST(ISNULL(rt.SharedFacilities, 0) AS BIT) AS SharedFacilities, 
	                CAST(ISNULL(IIF(r.XMLRoom = 1, xp.RequireBookingOfAllBeds, msr.IsActive), 0) AS BIT) AS MealSupplementRestricted,
	                CAST( ISNULL(r.weekly, 0) AS BIT) AS Weekly
                FROM
	                @temporalRooms AS tp 
                    INNER JOIN rooms r ON r.Id = tp.RoomId AND (NOT (r.roomtypes_id IS NULL)) AND (r.SuspendRoom = 0) AND NOT r.hotelId IS NULL
	                INNER JOIN roomtypes AS rt WITH(NOLOCK)  ON rt.id = r.roomtypes_id
	                LEFT OUTER JOIN xmlProvider AS xp ON xp.providerid = r.XMLProvider
                    LEFT OUTER JOIN vRoomProviderSettings msr ON msr.roomprovider_id = r.roomprovider_id AND msr.RoomProviderSettingID = 13 ");

            var result = new List<RoomStaticDataRecord>();
            using (var con = new SqlConnection(ConnectionString))
            {
                try
                {
                    using (var command = new SqlCommand(query.ToString(), con))
                    {
                        command.CommandTimeout = CommandTimeout;

                        await con.OpenAsync();
                        using (var reader = await command.ExecuteReaderAsync())
                        {
                            while (await reader.ReadAsync())
                            {
                                var record = ConvertToRoomStaticDataRecord(reader);
                                if (record != null)
                                    result.Add(record);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    if (con.State == System.Data.ConnectionState.Open)
                        con.Close();
                }
            }
            return result;
        }

        private const string DestinationRoomsSQL = @"
        SELECT Isnull(r.id, liverooms.liveroomid)                    AS RoomId, 
               hotelRT.hotelid as HotelId, 
               Isnull(NULLIF (dbo.Getlangroomtypeonroomtypeid(rt.id, @lang), ''), dbo.Getlangroomtypeonroomtypeid(rt.id, N'en')) AS RoomTypeName, 
               Isnull(NULLIF (Isnull(r.extrabeds, 0), 99), 0)        AS ExtraBeds, 
               Isnull(NULLIF (Isnull(r.beds, rt.beds), 99), rt.beds) AS Beds, 
               rt.id                                                 AS RoomTypeId, 
               Cast(ISNULL(r.roombestbuy, 0)	AS BIT)							 AS BestBuy,  
               Cast(ISNULL(r.norefund, 0)	AS BIT)							 AS NoRefundable, 
               Cast(ISNULL(r.hideextrabeds, 0)	AS BIT)					     AS HideExtraBeds,  
               Cast(Isnull(rt.sharedroom, 0) AS BIT)                 AS SharedRoom, 
               Cast(Isnull(rt.sharedfacilities, 0) AS BIT)           AS SharedFacilities, 
               Cast(Isnull(xp.requirebookingofallbeds, 0) AS BIT)    AS MealSupplementRestriction,
               Cast(ISNULL(r.weekly, 0) AS BIT) AS Weekly
        FROM   hotel AS h WITH(NOLOCK)
               INNER JOIN (
                    SELECT h.id  AS hotelId, rt.id AS roomType 
                    FROM   hotel AS h WITH(NOLOCK)
                        LEFT OUTER JOIN rooms AS r WITH(NOLOCK) ON r.hotelid = h.id AND Isnull(r.suspendroom, 0) = 0 
                        LEFT OUTER JOIN roomtypes AS rt WITH(NOLOCK) ON rt.id = r.roomtypes_id 
                    GROUP  BY h.id, rt.id 
                    UNION 
                    SELECT h.id,  rt.id AS Expr1 
                    FROM   hotel AS h WITH(NOLOCK)
                        LEFT OUTER JOIN liverooms AS lr ON lr.hotelid = h.id 
                        LEFT OUTER JOIN roomtypes AS rt ON lr.shroomtypeid = rt.id  
                    GROUP  BY h.id,  rt.id) AS hotelRT 
                       ON hotelRT.hotelid = h.id 
               INNER JOIN roomtypes AS rt WITH(NOLOCK) ON rt.id = hotelRT.roomtype 
               LEFT OUTER JOIN (
                    SELECT hotelid   AS hotelId, 
                        shroomtypeid AS liveroomtype,
                        Min(id)      AS liveroomid 
                    FROM   liverooms AS lr WITH(NOLOCK)
                    GROUP  BY hotelid, shroomtypeid) AS liverooms 
                            ON liverooms.hotelid = h.id 
                               AND liverooms.liveroomtype = rt.id 
               LEFT OUTER JOIN rooms AS r WITH(NOLOCK)
                            ON r.roomtypes_id = rt.id 
                               AND r.hotelid = hotelRT.hotelid 
                               AND Isnull(r.isliveroom, 0) = 0 
                               AND ( r.suspendroom IS NULL 
                                      OR r.suspendroom = 0 ) 
               LEFT OUTER JOIN xmlprovider AS xp WITH(NOLOCK)
                            ON xp.providerid = r.xmlprovider 
        WHERE  ( h.resort = @resortID ) 
               AND ( r.suspendroom = 0 ) 
               AND ( rt.id IS NOT NULL ) 
               AND ( NOT ( r.id IS NULL ) ) 
                OR ( h.resort = @resortID ) 
                   AND ( r.suspendroom = 0 ) 
                   AND ( rt.id IS NOT NULL ) 
                   AND ( NOT ( liverooms.liveroomid IS NULL ) ) 
                OR ( h.resort = @resortID ) 
                   AND ( rt.id IS NOT NULL ) 
                   AND ( liverooms.liveroomtype IS NOT NULL ) 
                   AND ( NOT ( r.id IS NULL ) ) 
                OR ( h.resort = @resortID ) 
                   AND ( rt.id IS NOT NULL ) 
                   AND ( liverooms.liveroomtype IS NOT NULL ) 
                   AND ( NOT ( liverooms.liveroomid IS NULL ) ) ";

        public async Task<IEnumerable<RoomStaticDataRecord>> GetResortRoomStaticAsync(int resortId, string language)
        {
            var result = new List<RoomStaticDataRecord>();
            using (var con = new SqlConnection(ConnectionString))
            {
                try
                {
                    using (var command = new SqlCommand(DestinationRoomsSQL, con))
                    {
                        command.Parameters.Add(new SqlParameter() { Value = resortId, ParameterName = "resortID", DbType = System.Data.DbType.Int32 });
                        command.Parameters.Add(new SqlParameter() { Value = language, ParameterName = "lang", DbType = System.Data.DbType.String });
                        command.CommandTimeout = CommandTimeout;

                        await con.OpenAsync();
                        using (var reader = await command.ExecuteReaderAsync())
                        {
                            while (await reader.ReadAsync())
                            {
                                var record = ConvertToRoomStaticDataRecord(reader);
                                if (record != null)
                                    result.Add(record);
                            }

                        }
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    if (con.State == System.Data.ConnectionState.Open)
                        con.Close();
                }
            }
            return result;

        }
        private static bool ValidateRoomStaticRecord(SqlDataReader reader)
        {

            if (reader.ValueOrDefault<int>("Beds") <= 0)
                return false;

            return true;
        }
        private static RoomStaticDataRecord ConvertToRoomStaticDataRecord(SqlDataReader reader)
        {
            if (!ValidateRoomStaticRecord(reader))
                return default(RoomStaticDataRecord);

            return new RoomStaticDataRecord()
            {
                RoomId = reader.ValueOrDefault<int>("RoomId"),
                HotelId = reader.ValueOrDefault<int>("HotelId"),
                RoomTypeName = reader.ValueOrDefault<string>("RoomTypeName"),
                RoomTypeId = reader.ValueOrDefault<int>("RoomTypeId"),
                ExtraBeds = reader.ValueOrDefault<int>("ExtraBeds"),
                Beds = reader.ValueOrDefault<int>("Beds"),
                BestBuy = reader.ValueOrDefault<bool>("BestBuy"),
                NoRefundable = reader.ValueOrDefault<bool>("NoRefundable"),
                HideExtraBeds = reader.ValueOrDefault<bool>("HideExtraBeds"),
                SharedFacilities = reader.ValueOrDefault<bool>("SharedFacilities"),
                MealSupplementRestricted = reader.ValueOrDefault<bool>("MealSupplementRestriction"),
                SharedRoom = reader.ValueOrDefault<bool>("SharedRoom"),
                Weekly = reader.ValueOrDefault<bool>("Weekly")
            };
        }

        private const string CountryRoomsSQL = @"
        SELECT Isnull(r.id, liverooms.liveroomid)                    AS RoomId, 
               hotelRT.hotelid                                       AS HotelId, 
               ''                                                    AS RoomTypeName, 
               Isnull(NULLIF (Isnull(r.extrabeds, 0), 99), 0)        AS ExtraBeds, 
               Isnull(NULLIF (Isnull(r.beds, rt.beds), 99), rt.beds) AS Beds, 
               rt.id                                                 AS RoomTypeId, 
               Cast(Isnull(r.roombestbuy, 0) AS BIT)                              AS BestBuy, 
               Cast(Isnull(r.norefund, 0) AS BIT)                                   AS NoRefundable, 
               Cast(Isnull(r.hideextrabeds, 0) AS BIT)                             AS HideExtraBeds, 
               Cast(Isnull(rt.sharedroom, 0) AS BIT)                 AS SharedRoom, 
               Cast(Isnull(rt.sharedfacilities, 0) AS BIT)           AS SharedFacilities , 
               Cast(Isnull(IIF(r.XMLRoom = 1, xp.requirebookingofallbeds, vrphs.IsActive), 0) AS BIT)    AS MealSupplementRestriction,
               Cast(Isnull(r.weekly, 0) AS BIT)                                    AS Weekly 
        FROM   hotel AS h WITH(nolock) 
               INNER JOIN dbo.destinations d WITH(nolock) 
                       ON h.destination = d.id 
               INNER JOIN (SELECT h.id  AS hotelId, 
                                  rt.id AS roomType 
                           FROM   hotel AS h WITH(nolock) 
                                  LEFT OUTER JOIN rooms AS r WITH(nolock) 
                                               ON r.hotelid = h.id 
                                                  AND Isnull(r.suspendroom, 0) = 0 
                                  LEFT OUTER JOIN roomtypes AS rt WITH(nolock) 
                                               ON rt.id = r.roomtypes_id 
                           GROUP  BY h.id, 
                                     rt.id 
                           UNION 
                           SELECT h.id, 
                                  rt.id AS Expr1 
                           FROM   hotel AS h WITH(nolock) 
                                  LEFT OUTER JOIN liverooms AS lr WITH(nolock) 
                                               ON lr.hotelid = h.id 
                                  LEFT OUTER JOIN roomtypes AS rt WITH(nolock) 
                                               ON lr.shroomtypeid = rt.id 
                           GROUP  BY h.id, 
                                     rt.id) AS hotelRT 
                       ON hotelRT.hotelid = h.id 
               INNER JOIN roomtypes AS rt WITH(nolock) 
                       ON rt.id = hotelRT.roomtype 
               LEFT OUTER JOIN (SELECT hotelid      AS hotelId, 
                                       shroomtypeid AS liveroomtype, 
                                       Min(id)      AS liveroomid 
                                FROM   liverooms AS lr WITH(nolock) 
                                GROUP  BY hotelid, 
                                          shroomtypeid) AS liverooms 
                            ON liverooms.hotelid = h.id 
                               AND liverooms.liveroomtype = rt.id 
               LEFT OUTER JOIN rooms AS r WITH(nolock) 
                            ON r.roomtypes_id = rt.id 
                               AND r.hotelid = hotelRT.hotelid 
                               AND Isnull(r.isliveroom, 0) = 0 
                               AND ( r.suspendroom IS NULL 
                                      OR r.suspendroom = 0 ) 
               LEFT OUTER JOIN xmlprovider AS xp WITH(nolock) 
                            ON xp.providerid = r.xmlprovider 
               LEFT OUTER JOIN vRoomProviderHotelSettings AS vrphs
							ON vrphs.roomprovider_id = r.roomprovider_id
								AND vrphs.hotelId = r.hotelId
								AND vrphs.RoomProviderSettingID = 13
        WHERE  ( d.country = @countryID ) 
               AND ( r.suspendroom = 0 ) 
               AND ( rt.id IS NOT NULL ) 
               AND ( NOT ( r.id IS NULL ) ) 
                OR ( d.country = @countryID ) 
                   AND ( r.suspendroom = 0 ) 
                   AND ( rt.id IS NOT NULL ) 
                   AND ( NOT ( liverooms.liveroomid IS NULL ) ) 
                OR ( d.country = @countryID ) 
                   AND ( rt.id IS NOT NULL ) 
                   AND ( liverooms.liveroomtype IS NOT NULL ) 
                   AND ( NOT ( r.id IS NULL ) ) 
                OR ( d.country = @countryID ) 
                   AND ( rt.id IS NOT NULL ) 
                   AND ( liverooms.liveroomtype IS NOT NULL ) 
                   AND ( NOT ( liverooms.liveroomid IS NULL ) ) 
        ORDER  BY hotelRT.hotelid, 
                  roomtypename ";

        public async Task<IEnumerable<RoomStaticDataRecord>> GetCountryRoomStaticAsync(int countryId)
        {
            var result = new List<RoomStaticDataRecord>();
            using (var con = new SqlConnection(ConnectionString))
            {
                try
                {
                    using (var command = new SqlCommand(CountryRoomsSQL, con))
                    {
                        command.Parameters.Add(new SqlParameter() { Value = countryId, ParameterName = "countryID", DbType = System.Data.DbType.Int32 });
                        command.CommandTimeout = CommandTimeout;

                        await con.OpenAsync();
                        using (var reader = await command.ExecuteReaderAsync())
                        {
                            while (await reader.ReadAsync())
                            {
                                var record = ConvertToRoomStaticDataRecord(reader);
                                if (record != null)
                                    result.Add(record);
                            }

                        }
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    if (con.State == System.Data.ConnectionState.Open)
                        con.Close();
                }
            }
            return result;

        }

        private const string HotelPictureByResortSQL = @"
        SELECT hp.id as ImageId, 
                Isnull(hp.width, 0)                     AS SmallWidth, 
                Isnull(hp.height, 0)                    AS SmallHeight, 
                Isnull(hpfs.hotelpicturefullsize_id, 0) AS LargeID, 
                Isnull(hpfs.width, 0)                   AS LargeWidth, 
                Isnull(hpfs.height, 0)                  AS LargeHeight, 
                h.id                                    AS HotelId 
        FROM   hotelpictures AS hp 
                LEFT OUTER JOIN tblhotelpicturefullsize AS hpfs 
                            ON hp.id = hpfs.hotelpictures_id 
                                AND hpfs.deletedate IS NULL 
                INNER JOIN hotel AS h 
                        ON hp.hotelid = h.id 
        WHERE  ( h.resort = @RESORT_ID ) 
                AND ( hp.deletedate IS NULL ) 
        ORDER  BY hotelid, 
                    isnull(hp.SortOrder,hp.id) ";

        public async Task<IEnumerable<HotelImageDataRecord>> GetHotelImageByResortAsync(int resortId)
        {
            var result = new List<HotelImageDataRecord>();
            using (var con = new SqlConnection(ConnectionString))
            {
                try
                {
                    using (var command = new SqlCommand(HotelPictureByResortSQL, con))
                    {
                        command.Parameters.Add(new SqlParameter() { Value = resortId, ParameterName = "RESORT_ID", DbType = System.Data.DbType.Int32 });
                        command.CommandTimeout = CommandTimeout;

                        await con.OpenAsync();
                        using (var reader = await command.ExecuteReaderAsync())
                        {
                            while (await reader.ReadAsync())
                                result.Add(ConvertToHotelImageDataRecord(reader));
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    if (con.State == System.Data.ConnectionState.Open)
                        con.Close();
                }
            }
            return result;
        }

        private static HotelImageDataRecord ConvertToHotelImageDataRecord(SqlDataReader reader)
        {
            return new HotelImageDataRecord()
            {
                HotelId = reader.ValueOrDefault<int>("HotelId"),
                ImageId = reader.ValueOrDefault<int>("ImageId"),
                LargeHeight = reader.ValueOrDefault<int>("LargeHeight"),
                LargeID = reader.ValueOrDefault<int>("LargeID"),
                LargeWidth = reader.ValueOrDefault<int>("LargeWidth"),
                SmallHeight = reader.ValueOrDefault<int>("SmallHeight"),
                SmallWidth = reader.ValueOrDefault<int>("SmallWidth"),
            };
        }

        private const string HotelPictureByCountrySQL = @"
        SELECT  hp.ID as ImageId,
                ISNULL(hp.Width,0) AS SmallWidth ,
                ISNULL(hp.Height,0) AS SmallHeight ,
                ISNULL(hpfs.HOTELPICTUREFULLSIZE_ID,0) AS LargeID ,
                ISNULL(hpfs.Width,0) AS LargeWidth ,
                ISNULL(hpfs.Height,0) AS LargeHeight,
                h.id AS HotelId
        FROM    hotelPictures hp
                LEFT JOIN tblHotelPictureFullSize hpfs ON hp.ID = hpfs.HOTELPICTURES_ID AND hpfs.DeleteDate IS NULL
		        JOIN hotel h ON hp.hotelId = h.id
		        JOIN destinations d ON h.destination = d.id
        WHERE   ( d.country = @countryId ) AND ( hp.DeleteDate IS NULL )
        ORDER BY hotelid, isnull(hp.SortOrder,hp.id)";

        public async Task<IEnumerable<HotelImageDataRecord>> GetHotelImageByCountryAsync(int countryId)
        {
            var result = new List<HotelImageDataRecord>();
            using (var con = new SqlConnection(ConnectionString))
            {
                try
                {
                    using (var command = new SqlCommand(HotelPictureByCountrySQL, con))
                    {
                        command.Parameters.Add(new SqlParameter() { Value = countryId, ParameterName = "countryId", DbType = System.Data.DbType.Int32 });
                        command.CommandTimeout = CommandTimeout;

                        await con.OpenAsync();
                        using (var reader = await command.ExecuteReaderAsync())
                        {
                            while (await reader.ReadAsync())
                                result.Add(ConvertToHotelImageDataRecord(reader));
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    if (con.State == System.Data.ConnectionState.Open)
                        con.Close();
                }
            }
            return result;
        }

        private const string GetMealLabelByCountrySQL = @"  
        SELECT HotelId, ISNULL(MealId, 0) AS MealId, IsNull(MealLabelId, 0) As MealLabelId
        FROM HotelMealLabels  WITH(NOLOCK) 
        WHERE HotelID IN (SELECT h.ID FROM  hotel AS h  WITH(NOLOCK)  
		INNER JOIN dbo.Destinations d WITH(NOLOCK) ON d.ID = h.destination AND (d.country = @countryID OR @countryID = 0) )";
        public async Task<IEnumerable<MealDataRecord>> GetMealLabelByCountryAsync(int countryId)
        {
            var result = new List<MealDataRecord>();
            using (var con = new SqlConnection(ConnectionString))
            {
                try
                {
                    using (var command = new SqlCommand(GetMealLabelByCountrySQL, con))
                    {
                        command.Parameters.Add(new SqlParameter() { Value = countryId, ParameterName = "countryID", DbType = System.Data.DbType.Int32 });
                        command.CommandTimeout = CommandTimeout;

                        await con.OpenAsync();
                        using (var reader = await command.ExecuteReaderAsync())
                        {
                            while (await reader.ReadAsync())
                                result.Add(ConvertToMealDataRecord(reader));
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    if (con.State == System.Data.ConnectionState.Open)
                        con.Close();
                }
            }
            return result;
        }

        /// <summary>
        /// Thomas Cook
        /// </summary>
        private const string GetMealLabelForThomasCookSQL_part1 = @"
SELECT  innerHotel.HotelId ,
        ISNULL(m.id, 0) AS MealId ,
        ISNULL(hml.MealLabelID, 0) AS MealLabelId
FROM    ( SELECT    ISNULL(h.id, 0) AS HotelId ,
                    c.meals AS mealname
          FROM      hotel h WITH ( NOLOCK )
                    INNER JOIN rooms r WITH ( NOLOCK ) ON r.hotelId = h.id
                    INNER JOIN dbo.calendar c WITH ( NOLOCK ) ON c.roomId = r.ID
                    INNER JOIN dbo.roomprovider rp WITH ( NOLOCK ) ON rp.id = r.roomprovider_id
          WHERE     ISNULL(r.SuspendRoom, 0) = 0
                    AND ISNULL(rp.suspend, 0) = 0
                    AND c.units > 0
                    AND c.datum > GETDATE()
                    AND h.resort = @resortID
        ) innerHotel
        INNER JOIN dbo.meals m WITH ( NOLOCK ) ON m.mealname = innerHotel.mealname
        LEFT JOIN dbo.HotelMealLabels hml WITH ( NOLOCK ) ON hml.HotelID = innerHotel.HotelId
                                            AND hml.MealID = ( SELECT
                                            m1.id
                                            FROM dbo.meals m1 WITH ( NOLOCK )
                                            WHERE m1.mealname = CASE innerHotel.mealname
                                                WHEN 'all inclusive'
                                                THEN 'all'
                                                ELSE innerHotel.mealname
                                                END
                                            )
GROUP BY innerHotel.HotelId ,
        m.id ,
        hml.MealLabelID";

        private const string GetMealLabelForThomasCookSQL_part2 = @"
            SELECT boardsAsSupplements.hotelId, boardsAsSupplements.MealId, ISNULL(hml.MealLabelID, 0) AS MealLabelId
            FROM
            (   SELECT hotelId,
                        MealId
                FROM
                        (      SELECT hotelId,
                                        MAX(hasNo) hasno,
                                        MAX(hasBRf) hasbrf,
                                        MAX(hasHB) hashb,
                                        MAX(hasFB) hasfb,
                                        MAX(hasAllBoards) hasallboards
                                    FROM
						            (     SELECT DISTINCT r.hotelId,
									            CASE WHEN c.meals = 'None' THEN 1 ELSE 0 END hasNo,
									            CASE WHEN c.Brf > 0 OR c.ChildBrf > 0 THEN 3 ELSE 0 END hasBRf,
									            CASE WHEN c.HB > 0 OR c.ChildHB > 0 THEN 4 ELSE 0 END hasHB,
									            CASE WHEN c.FB > 0 OR c.ChildFB > 0 THEN 5 ELSE 0 END hasFB,
									            CASE WHEN c.AllBoards > 0 OR c.ChildAllBoards > 0 THEN 6 ELSE 0 END hasAllBoards
							            FROM rooms r
								            INNER JOIN dbo.calendar c
								            ON c.roomId = r.ID
									            AND ISNULL(r.SuspendRoom, 0) = 0
									            AND c.units > 0
									            AND c.datum > GETDATE()
						            WHERE EXISTS
						            (      SELECT TOP (1) 1
								            FROM dbo.hotel h
										            INNER JOIN dbo.roomprovider rp
											            ON rp.id = r.roomprovider_id
													            AND ISNULL(rp.suspend, 0) = 0
								            WHERE h.id = r.hotelId
											            AND h.resort = @resortId)
						            AND ISNULL(r.SuspendRoom, 0) = 0) boardsAsSupplements
                        WHERE (hasNo + hasBRf + hasHB + hasFB + hasAllBoards) <> 0
                        GROUP BY boardsAsSupplements.hotelId) P
                UNPIVOT (MealId
            FOR XXX_Name IN (hasno, hasbrf, hashb, hasfb, hasallboards)) unpvt
                WHERE MealId <> 0) boardsAsSupplements
            LEFT JOIN dbo.HotelMealLabels hml
            ON hml.HotelID = boardsAsSupplements.hotelId
                AND hml.MealID = boardsAsSupplements.MealId;";

        public async Task<IEnumerable<MealDataRecord>> GetMealLabelForThomasCookAsync(int resortId)
        {
            var result = new List<MealDataRecord>();
            using (var con = new SqlConnection(ConnectionString))
            {
                using (var command = new SqlCommand(GetMealLabelForThomasCookSQL_part1, con))
                {
                    command.Parameters.Add(new SqlParameter() { Value = resortId, ParameterName = "resortID", DbType = System.Data.DbType.Int32 });
                    command.CommandTimeout = CommandTimeout;

                    await con.OpenAsync();
                    using (var reader = await command.ExecuteReaderAsync())
                    {
                        while (await reader.ReadAsync())
                            result.Add(ConvertToMealDataRecord(reader));
                    }
                }
            //}
            //using (var con = new SqlConnection(ConnectionString))
            //{
                using (var command = new SqlCommand(GetMealLabelForThomasCookSQL_part2, con))
                {
                    command.Parameters.Add(new SqlParameter() { Value = resortId, ParameterName = "resortID", DbType = System.Data.DbType.Int32 });
                    command.CommandTimeout = CommandTimeout;

                    // await con.OpenAsync();
                    using (var reader = await command.ExecuteReaderAsync())
                    {
                        while (await reader.ReadAsync())
                        {
                            var meal = ConvertToMealDataRecord(reader);
                            if (!result.Contains(meal))
                                result.Add(meal);
                        }
                    }
                }
            }
            return result;
        }

        private static MealDataRecord ConvertToMealDataRecord(SqlDataReader reader)
        {
            return new MealDataRecord()
            {
                HotelId = reader.ValueOrDefault<int>("HotelId"),
                MealId = reader.ValueOrDefault<int>("MealId"),
                MealLabelId = reader.ValueOrDefault<int>("MealLabelId")
            };
        }

        private static string GetCountryByLanguageSQL = @"
        SELECT 
            id as Id, 
            Isnull(NULLIF (dbo.Getlangcountry(id, @lang), ''), dbo.Getlangcountry(id, N'en')  ) AS Name, 
            countrycode as Code
        FROM   countries 
        WHERE  ( suspend IS NULL ) 
                OR ( suspend = 0 ) ";

        public async Task<IEnumerable<CountryDataRecord>> GetCountriesAsync(string lang)
        {
            var result = new List<CountryDataRecord>();
            using (var con = new SqlConnection(ConnectionString))
            {
                try
                {
                    using (var command = new SqlCommand(GetCountryByLanguageSQL, con))
                    {
                        command.Parameters.Add(new SqlParameter() { Value = lang, ParameterName = "lang", DbType = System.Data.DbType.String });
                        command.CommandTimeout = CommandTimeout;

                        await con.OpenAsync();
                        using (var reader = await command.ExecuteReaderAsync())
                        {
                            while (await reader.ReadAsync())
                                result.Add(ConvertToCountryDataRecord(reader));
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    if (con.State == System.Data.ConnectionState.Open)
                        con.Close();
                }
            }
            return result;
        }

        private static CountryDataRecord ConvertToCountryDataRecord(SqlDataReader reader)
        {
            return new CountryDataRecord()
            {
                Code = reader.ValueOrDefault<string>("Code"),
                Id = reader.ValueOrDefault<int>("Id"),
                Name = reader.ValueOrDefault<string>("Name")
            };
        }

        private static string GetCountriesByDestinationSQL = @"
		SELECT 
            c.id Id, 
            Isnull(NULLIF (dbo.Getlangcountry(c.id, @lang), ''), dbo.Getlangcountry(c.id, N'en')  ) AS Name , 
            c.countrycode as Code
        FROM   countries  c
		INNER JOIN Destinations d on d.country =  c.id AND d.ID IN({0})
        WHERE   c.suspend IS NULL OR c.suspend = 0
		GROUP BY C.ID, C.countrycode ";

        public async Task<IEnumerable<CountryDataRecord>> GetCountriesByDestinationAsync(string lang, int[] destinationIds)
        {
            var result = new List<CountryDataRecord>();
            using (var con = new SqlConnection(ConnectionString))
            {
                try
                {
                    var combinedQuery = string.Format(GetCountriesByDestinationSQL, string.Join(",", destinationIds));
                    using (var command = new SqlCommand(combinedQuery, con))
                    {
                        command.Parameters.Add(new SqlParameter() { Value = lang, ParameterName = "lang", DbType = System.Data.DbType.String });
                        command.CommandTimeout = CommandTimeout;

                        await con.OpenAsync();
                        using (var reader = await command.ExecuteReaderAsync())
                        {
                            while (await reader.ReadAsync())
                                result.Add(ConvertToCountryDataRecord(reader));
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    if (con.State == System.Data.ConnectionState.Open)
                        con.Close();
                }
            }
            return result;
        }

        private const string GetHotelGiataSQL = @"
        SELECT 
            hotelId as HotelId, 
            value as Code   
        FROM dbo.hotelCode with(nolock) 
        WHERE hotelCodeTypeId = 2";
        public async Task<Dictionary<int, string>> GetHotelGiataCodesAsync()
        {
            var result = new Dictionary<int, string>();
            using (var con = new SqlConnection(ConnectionString))
            {
                try
                {
                    using (var command = new SqlCommand(GetHotelGiataSQL, con))
                    {
                        command.CommandTimeout = CommandTimeout;

                        await con.OpenAsync();
                        using (var reader = await command.ExecuteReaderAsync())
                        {
                            while (await reader.ReadAsync())
                            {
                                var hotelId = reader.ValueOrDefault<int>("HotelId");
                                if (!result.ContainsKey(hotelId))
                                {
                                    var code = reader.ValueOrDefault<string>("Code");
                                    result.Add(hotelId, code);
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    if (con.State == System.Data.ConnectionState.Open)
                        con.Close();
                }
            }
            return result;
        }

    }
}
