﻿using System.Collections.Generic;
using System.Threading.Tasks;
using SunHotels.XML.Data;

namespace SunHotels.XML.Infrastructure
{
    public interface IRoomConfigurationProvider
    {
        Task<bool> FillRoomConfigurationAsync(IEnumerable<Room> rooms, Root root, string lang); 
        void FillRootRoomConfiguration(Root root);
    }
}