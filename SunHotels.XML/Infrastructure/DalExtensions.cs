﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace SunHotels.XML.Infrastructure
{
    public static class DalExtensions
    {
        public static T ValueOrDefault<T>(this SqlDataReader reader, string key)  
        { 
            return (T)Convert.ChangeType(reader[key], typeof(T));
        }
         

        public static string CreateTemporalTableQuery<T>(string tableName, string columnName, string columnType, string valuePattern, T[] items)
        {
            var query = new StringBuilder();

            query.AppendLine($"DECLARE @{tableName} AS TABLE ({columnName} {columnType}) ");

            foreach (var insertValuesQuery in MultipleQueryLists(items, valuePattern))
            {
                query.AppendLine($"INSERT INTO @{tableName}({columnName}) VALUES {insertValuesQuery}");
            }


            return query.ToString();
        }

        private static List<string> MultipleQueryLists<T>(IEnumerable<T> sourceList, string valuePattern)
        {
            var tmpLists = new Dictionary<int, List<T>>();
            if (sourceList != null)
            {
                foreach (var element in sourceList)
                {
                    if (tmpLists.Count == 0)
                    {
                        tmpLists.Add(0, new List<T>());
                    }
                    //Limit queries by a certain number of elements
                    if (tmpLists[tmpLists.Count - 1].Count > 999)
                    {
                        tmpLists.Add(tmpLists.Count, new List<T>());
                    }
                    tmpLists[tmpLists.Count - 1].Add(element);
                }
            }
            var retLists = new List<string>();
            foreach (var tmp in tmpLists.Values)
            {
                retLists.Add(string.Join(",", tmp.Select(t => string.Format(valuePattern, t))));
            }
            return retLists;
        }

    }

}
