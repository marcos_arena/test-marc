﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Concurrent;
using SunHotels.XML.Infrastructure.Models;

namespace SunHotels.XML.Infrastructure
{
    public class RoomConfigurationTables
    {
        private ConcurrentDictionary<int, RoomInfantsAndChildrenDataRecord> _roomInfantsAndChildren;
        private ConcurrentDictionary<int, RoomOccupancyBlocksDataRecord> _roomOccupancyBlocks;
        private ConcurrentDictionary<int, RoomOccupancyDataRecord> _roomOccupancy;
        private ConcurrentDictionary<int, RoomExtraBedFactorsDataRecord> _roomExtraBedFactors;

        private ConcurrentDictionary<int, int> _roomInfantsAndChildrenInUse;
        private ConcurrentDictionary<int, int> _roomOccupancyBlocksInUse;
        private ConcurrentDictionary<int, int> _roomOccupancyInUse;
        private ConcurrentDictionary<int, int> _roomExtraBedFactorsInUse;


        public ConcurrentDictionary<int, RoomInfantsAndChildrenDataRecord> RoomInfantsAndChildren
        {
            get
            {
                return _roomInfantsAndChildren;
            }

            set
            {
                _roomInfantsAndChildren = value;
            }
        }
        public ConcurrentDictionary<int, RoomOccupancyBlocksDataRecord> RoomOccupancyBlocks
        {
            get
            {
                return _roomOccupancyBlocks;
            }

            set
            {
                _roomOccupancyBlocks = value;
            }
        }
        public ConcurrentDictionary<int, RoomOccupancyDataRecord> RoomOccupancy
        {
            get
            {
                return _roomOccupancy;
            }

            set
            {
                _roomOccupancy = value;
            }
        }
        public ConcurrentDictionary<int, RoomExtraBedFactorsDataRecord> RoomExtraBedFactors
        {
            get
            {
                return _roomExtraBedFactors;
            }

            set
            {
                _roomExtraBedFactors = value;
            }
        }

        public ConcurrentDictionary<int, int> RoomInfantsAndChildrenInUse
        {
            get
            {
                return _roomInfantsAndChildrenInUse;
            }

            set
            {
                _roomInfantsAndChildrenInUse = value;
            }
        }
        public ConcurrentDictionary<int, int> RoomOccupancyBlocksInUse
        {
            get
            {
                return _roomOccupancyBlocksInUse;
            }

            set
            {
                _roomOccupancyBlocksInUse = value;
            }
        }
        public ConcurrentDictionary<int, int> RoomOccupancyInUse
        {
            get
            {
                return _roomOccupancyInUse;
            }

            set
            {
                _roomOccupancyInUse = value;
            }
        }
        public ConcurrentDictionary<int, int> RoomExtraBedFactorsInUse
        {
            get
            {
                return _roomExtraBedFactorsInUse;
            }

            set
            {
                _roomExtraBedFactorsInUse = value;
            }
        }

        public RoomConfigurationTables()
        {
            RoomInfantsAndChildrenInUse = new ConcurrentDictionary<int, int>();
            RoomOccupancyBlocksInUse = new ConcurrentDictionary<int, int>();
            RoomOccupancyInUse = new ConcurrentDictionary<int, int>();
            RoomExtraBedFactorsInUse = new ConcurrentDictionary<int, int>();
        }

        public List<RoomInfantsAndChildrenDataRecord> GetRoomInfantsAndChildrenInUse()
        {   
            var infantsAndChildren = _roomInfantsAndChildren.Where(t => _roomInfantsAndChildrenInUse.ContainsKey(t.Key)).Select(t => t.Value).ToList();
            return infantsAndChildren;
        }
        public List<RoomOccupancyBlocksDataRecord> GetRoomOccupancyBlocksInUse()
        {
            var occupancyBlocks = _roomOccupancyBlocks.Where(t => _roomOccupancyBlocksInUse.ContainsKey(t.Key)).Select(t => t.Value).ToList();
            return occupancyBlocks;
        }
        public List<RoomOccupancyDataRecord> GetRoomOccupancyInUse()
        {
            var roomOccupancy = _roomOccupancy.Where(t => _roomOccupancyInUse.ContainsKey(t.Key)).Select(t => t.Value).ToList();
            return roomOccupancy;
        }
        public List<RoomExtraBedFactorsDataRecord> GetRoomExtraBedFactorsInUse()
        {
            var extraBedsFactors = _roomExtraBedFactors.Where(t => _roomExtraBedFactorsInUse.ContainsKey(t.Key)).Select(t => t.Value).ToList();
            return extraBedsFactors;
        }
    }

}
