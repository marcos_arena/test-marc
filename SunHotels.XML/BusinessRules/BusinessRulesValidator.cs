﻿using SunHotels.Export;
using SunHotels.XML.Data;
using SunHotels.XML.HelpClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace SunHotels.XML.BusinessRules
{
	/// <summary>
	/// This class will be responsible to validate provider data againts current business rules to clear/modify those invalid fileds
	/// </summary>
	public class BusinessRulesValidator
	{
		Logger _log;

		class BusinessRule: Attribute{
			/// <summary>
			/// Optional, allows to indicate validation order or dependencies
			/// </summary>
			public int Order = 0;
		}

		static IEnumerable<MethodInfo> _businessRules;
		private Root _data;

		static BusinessRulesValidator() {
			_businessRules = typeof(BusinessRulesValidator).GetMethods(BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance)
						.Select(m => new { m, Validator = (BusinessRule)m.GetCustomAttributes(true).FirstOrDefault(att => att is BusinessRule) })
						.Where(m => m.Validator != null)
						.OrderBy(m => m.Validator.Order)
						.Select(m => m.m)
						.ToArray();
		}

		public BusinessRulesValidator(Logger log, Root data)
		{
			_log = log;
			_data = data;
		}

		void LogMessage(string message, params object[] values) {
			_log.RecordMessage(string.Format(message, values), SunHotels.XML.HelpClasses.Log.MessageType.Warning);
		}

		/// <summary>
		/// Run all validators
		/// </summary>
		/// <param name="data"></param>
		public void MakeItCompliant()
		{
			try
			{
				_businessRules.All(v =>
				{
					v.Invoke(this, new object[0]);
					return true;
				});
			}
			catch (Exception ex) {
				LogMessage("Error applying business rules: {0}", ex.ToString());
			}
		}

		void ForAllHotels(Action<Hotel> doit) { 
			_data.AllHotels()
				.All(h =>
				{
					doit(h);
					return true;
				});
		}

		/// <summary>
		/// IATA Reference_point validator
		/// </summary>
		/// <param name="data"></param>
		[BusinessRule]
		void IATARefrencePointValidator()
		{
			ForAllHotels(h =>
			{
				h.Distances.All(d =>
				{
					if (!string.IsNullOrEmpty(d.ReferencePoint)
						&& d.ReferencePoint.Length > 3)
					{
						LogMessage("reference_point {0} for hotel {1}:{2}", d.ReferencePoint, h.Id, h.Name);
						// Right now we expect a IATA code in here....
						d.ReferencePoint = null;
					}
					return true;
				});
			});
				
		}

		/// <summary>
		/// latitude and Longitude must be a valid 14 characters long double value
		/// </summary>
		/// <param name="data"></param>
		[BusinessRule]
		void LatLngValildator()
		{
			double d;
			ForAllHotels(h => {
				// Clean lat,lng formats e.e 40.889798,23.890980
				if (!string.IsNullOrEmpty(h.Position_Latitude)){
					var parts = h.Position_Latitude.Split(',');
					if (parts.Length == 2)
					{
						h.Position_Latitude = parts[0].Trim();
						h.Position_Latitude = parts[1].Trim();
					}
				}
				if (!string.IsNullOrEmpty(h.Position_Latitude)) {
					if (h.Position_Latitude.Length > 14)
						h.Position_Latitude = h.Position_Latitude.Substring(0, 14);
					h.Position_Latitude = h.Position_Latitude.Replace(",", string.Empty).Trim();
					if (!double.TryParse(h.Position_Latitude,out d))
						h.Position_Latitude = null;
				}
				if (!string.IsNullOrEmpty(h.Position_Longitude))
				{
					if (h.Position_Longitude.Length > 14)
						h.Position_Longitude = h.Position_Longitude.Substring(0, 14);
					h.Position_Longitude = h.Position_Longitude.Replace(",", string.Empty).Trim();
					if (!double.TryParse(h.Position_Longitude, out d))
						h.Position_Longitude = null;
				}
			});
		}
	}
}
