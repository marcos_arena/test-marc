﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace Sunhotels.Export
{
    public partial class ConfigurationManager
    {
        public static Configuration FromFile(string path)
        {
            var props = typeof(Configuration)
                .GetProperties()
                .ToDictionary(p => p.Name, p => new
                {
                    p,
                    setter = p.GetSetMethod(),
                    serializer = p.PropertyType.IsPrimitive || p.PropertyType.Equals(typeof(string))
                        || p.PropertyType.Name.IndexOf("Nullable") != -1 // Avoid nullable
                        ? null : new XmlSerializer(p.PropertyType),
                    type = p.PropertyType.Name.IndexOf("Nullable") != -1 ? Nullable.GetUnderlyingType(p.PropertyType) : p.PropertyType
                });
            var ms = new MemoryStream();
            new XmlSerializer(typeof(ConfigurationCacheXSD))
            .Serialize(ms, new ConfigurationCacheXSD()
            {
                @namespace = @"http:\\yada.yadda"
                ,
                Value = @"http:\\yada.yadda"
            });
            ms.Seek(0, SeekOrigin.Begin);
            var mxltr = XmlTextReader.Create(ms);

            // Defaults
            var conf = new Configuration(path)
            {
                AutoWriteCacheXML = true,
                AutoWritePlacesXML = true,
                AutoValidateOutput = true
            };
            var converType = new Func<string, Type, object>((s, t) => Convert.ChangeType(s, t));
            using (var xmlr = XmlReader.Create(path))
            {
                while (xmlr.Read())
                {
                    var name = xmlr.Name;
                    if (props.ContainsKey(name))
                    {
                        var prop = props[name];
                        object value = null;
                        if (prop.serializer != null)
                        {
                            var xml = "<?xml version=\"1.0\" encoding=\"utf-8\" ?>" + xmlr.ReadOuterXml();
                            xml = xml.Replace(name, prop.p.PropertyType.Name);
                            // Update root
                            value = prop.serializer.Deserialize(XmlReader.Create(new StringReader(xml), new XmlReaderSettings()
                            {
                                ConformanceLevel = ConformanceLevel.Fragment
                                ,
                                IgnoreProcessingInstructions = true
                                ,
                                CheckCharacters = false
                                ,
                                ValidationType = ValidationType.None
                            }));
                        }
                        else
                            value = xmlr.ReadElementContentAs(prop.type, null);
                        prop.setter.Invoke(conf, new object[] { value });
                    }
                }
            }
            // Post build
            if (string.IsNullOrEmpty(conf.ProviderName))
                conf.ProviderName = conf.Provider; 
       
            if (string.IsNullOrEmpty(conf.CacheXSD.@namespace))
                conf.CacheXSD.@namespace = conf.CacheXSD.Value;
            if (string.IsNullOrEmpty(conf.PlacesXSD.@namespace))
                conf.PlacesXSD.@namespace = conf.PlacesXSD.Value;
            if (string.IsNullOrEmpty(conf.AvailabilityCacheXSD.@namespace))
                conf.AvailabilityCacheXSD.@namespace = conf.AvailabilityCacheXSD.Value;
            if (string.IsNullOrEmpty(conf.CancellationPolicyCacheXSD.@namespace))
                conf.CancellationPolicyCacheXSD.@namespace = conf.CancellationPolicyCacheXSD.Value;
            if (string.IsNullOrEmpty(conf.CountryNotesXSD.@namespace))
                conf.CountryNotesXSD.@namespace = conf.CountryNotesXSD.Value;
            if (string.IsNullOrEmpty(conf.DestinationNotesXSD.@namespace))
                conf.DestinationNotesXSD.@namespace = conf.DestinationNotesXSD.Value;
            if (string.IsNullOrEmpty(conf.DiscountCacheXSD.@namespace))
                conf.DiscountCacheXSD.@namespace = conf.DiscountCacheXSD.Value;
            if (string.IsNullOrEmpty(conf.HotelNotesXSD.@namespace))
                conf.HotelNotesXSD.@namespace = conf.HotelNotesXSD.Value;
            if (string.IsNullOrEmpty(conf.PriceCacheXSD.@namespace))
                conf.PriceCacheXSD.@namespace = conf.PriceCacheXSD.Value;
            if (string.IsNullOrEmpty(conf.ResortNotesXSD.@namespace))
                conf.ResortNotesXSD.@namespace = conf.ResortNotesXSD.Value;
            if (string.IsNullOrEmpty(conf.RoomGroupsXSD.@namespace))
                conf.RoomGroupsXSD.@namespace = conf.RoomGroupsXSD.Value;
            if (string.IsNullOrEmpty(conf.RoomNotesXSD.@namespace))
                conf.RoomNotesXSD.@namespace = conf.RoomNotesXSD.Value;
            if (string.IsNullOrEmpty(conf.RoomPrioritiesXSD.@namespace))
                conf.RoomPrioritiesXSD.@namespace = conf.RoomPrioritiesXSD.Value;
             
            return conf;
        }         

        public static Stream Serialize(Configuration conf)
		{
			var ms = new MemoryStream();
			new XmlSerializer(conf.GetType())
			.Serialize(ms, conf);
			ms.Seek(0, SeekOrigin.Begin);
			return ms;
		}

    }
}
