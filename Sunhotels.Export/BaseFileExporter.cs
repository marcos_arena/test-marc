﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.IO;
using SunHotels.Export;
using System.Xml.Schema;
using Sunhotels.Export;
using System.Threading.Tasks;

namespace SunHotels.Export
{
    public abstract class BaseFileExporter
    {
        protected Configuration configuration;

        public BaseFileExporter(Configuration configuration)
        {
            this.configuration = configuration;
        }

        public abstract void Write(ExportData data);

        public abstract void Validate();

        protected void CreateXmlFile(Action<XmlTextWriter> writeFunc, string location, bool useTempFile)
        {
            if (string.IsNullOrEmpty(location)) return;

            var finalFile = new FileInfo(location);
            if (!finalFile.Directory.Exists)
                finalFile.Directory.Create();

            FileInfo tempFile = null;
            if (useTempFile)
            {
                tempFile = new FileInfo(finalFile.FullName.Remove(finalFile.FullName.Length - finalFile.Extension.Length) + ".tmp");

                if (tempFile.Exists)
                    tempFile.Delete();
            }

            var file = useTempFile ? tempFile : finalFile;

            using (var fs = new FileStream(file.FullName, FileMode.Create))
            {
                using (var writer = new XmlTextWriter(fs, new UTF8Encoding(false)) { Formatting = Formatting.Indented, IndentChar = '\t', Indentation = 1 })
                {
                    try
                    {
                        writer.WriteStartDocument();
                        writeFunc(writer);
                        writer.Flush();
                    }
                    catch (Exception)
                    {
                        writer.Flush();
                        throw;
                    }
                    finally
                    {
                        writer.Close();
                        if (useTempFile)
                        {
                            if (finalFile.Exists)
                            {
                                finalFile.Delete();
                            }
                            tempFile.MoveTo(finalFile.FullName);
                        }
                    }
                }
            }
        }

        private static System.Net.Http.HttpClient httpClient = new System.Net.Http.HttpClient();

        protected XmlSchema GetXsdFileSync(string xsdFilePath)
        {
            return Task.Run(() => GetXsdFile(xsdFilePath)).Result;
        }
        protected async Task<XmlSchema> GetXsdFile(string xsdFilePath)
        {
            var returnvalue = new XmlSchema();
            Uri url = null;
            try
            {
                if (!xsdFilePath.Contains("/"))
                    xsdFilePath = "file:///" + (xsdFilePath.Replace("\\", "//"));

                url = new Uri(xsdFilePath);
            }
            catch (Exception e) { throw new Exception("XSD Path invalid!", e); }

            string fileName = url.Segments.Last();

            // we can assume the uri is "valid" if we get here
            if (url.IsFile && url.IsUnc)
            {
                File.Copy(url.LocalPath, ".\\" + fileName);
            }
            else if (url.IsAbsoluteUri)
            {
                //http://xml.sunhotels.net/schemas/AvailabilityCacheCombined_1.5.0.xsd

                // check local folder for downloaded version of this xsd, if it doesnt exist, download it.
                if (!File.Exists(fileName))
                {
                    var content = await httpClient.GetStringAsync(xsdFilePath).ConfigureAwait(false);
                    File.WriteAllText(fileName, content);
                }
            }

            var f = XmlSchema.Read(File.OpenText(fileName), new ValidationEventHandler((obj, args) =>
            {
                string extraData = $"XSD File {xsdFilePath} is invalid!";
                Console.WriteLine("{0}:{1} {2}", args.Severity, args.Message, extraData);
            }));

            return returnvalue;
        }


        protected void ValidateXElementFile(string xmlFile, string xsdFile)
        {
            System.Xml.Linq.XDocument doc = System.Xml.Linq.XDocument.Load(xmlFile);
            var schemas = new XmlSchemaSet();
            //schemas.Add(null, xsdFile);
            schemas.Add(GetXsdFileSync(xsdFile));

            var xpath = new Func<System.Xml.Linq.XElement, string>((el) =>
            {
                string path = el.Name.LocalName;
                el = el.Parent;
                while (el != null)
                {
                    path = el.Name.LocalName + "/" + path;
                    el = el.Parent;
                }
                return path;
            });
            doc.Validate(schemas, (s, e) =>
            {
                var xobj = s as System.Xml.Linq.XObject;
                var el = s as System.Xml.Linq.XElement;
                var att = s as System.Xml.Linq.XAttribute;
                var extraData = string.Empty;
                if (xobj != null)
                {
                    extraData = string.Format("<{0}> {1}", xobj.NodeType,
                        el == null ?
                            att == null ? string.Empty : xpath(att.Parent) + "/@" + att.Name.LocalName :
                    xpath(el));
                }
                Console.WriteLine("{0}:{1} {2}", e.Severity, e.Message, extraData);
            });
        }

        protected void ValidateXmlFile(string xmlFile, string xsdFile)
        {
            if (string.IsNullOrEmpty(xmlFile) || string.IsNullOrEmpty(xsdFile)) return;

            ValidateXElementFile(xmlFile, xsdFile);
            return;
        }

        /// <summary>
        /// Strict validator
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public static void ValidationEventHandler(object sender, ValidationEventArgs e)
        {
            switch (e.Severity)
            {
                case XmlSeverityType.Error:
                    Console.WriteLine("Error: {0}, on line {1} column {2}.", e.Message, e.Exception.LineNumber, e.Exception.LinePosition);
                    break;
                case XmlSeverityType.Warning:
                    Console.WriteLine("Warning {0}, on line {1} column {2}.", e.Message, e.Exception.LineNumber, e.Exception.LinePosition);
                    break;
            }

            throw new ApplicationException(e.Message);
        }
    }
}
