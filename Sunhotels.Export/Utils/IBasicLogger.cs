﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sunhotels.Export.Utils
{
	public interface IBasicLogger
	{
		void WriteLine(string p, params object[] values);

		void Close();
	}
}
