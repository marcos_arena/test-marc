﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sunhotels.Export.Utils
{
	public class BasicLogger: IBasicLogger
	{
		StreamWriter _w = null;
		Object lck = null;
		public static IBasicLogger Create(string p)
		{
			return new BasicLogger(p);
		}

		public BasicLogger(string file) {
			_w = new StreamWriter(file);
			_w.AutoFlush = true;
			lck = new Object();
		}

		public void WriteLine(string p, params object[] values)
		{
			lock(lck)
				_w.WriteLine(p, values);
		}

		public void Close()
		{
			lock (lck)
				_w.Close();
		}
	}
}
