Add-Type -AssemblyName System.IO.Compression
Add-Type -AssemblyName System.IO.Compression.FileSystem

function ZipFiles( $zipfilename, $sourcedir )
{
   Add-Type -Assembly System.IO.Compression.FileSystem
   $compressionLevel = [System.IO.Compression.CompressionLevel]::Optimal
   [System.IO.Compression.ZipFile]::CreateFromDirectory($sourcedir,
        $zipfilename, $compressionLevel, $false)
}

$rootPath = "C:\TeamLoki\Import.ProviderToFileImport\"
$scriptConfigPath = "$($rootPath)Config\PeakworkExportThomasCook\"
$scriptPath = "$($rootPath)SunHotels.Loader\bin\Debug\"
$cachePath = "$($scriptPath)Cache\PeakworkEDF\"

$marketsXml = "MarketAccountMapping.xml"
[xml]$xml = Get-Content $marketsXml

Copy-Item $marketsXml $cachePath
#foreach( $market in $xml.Markets.Market) 
#{  
#    foreach($account in $market.Accounts.Account){
#        $peakworkXmlConfig = "$($scriptConfigPath)PeakworkEDF_$($market.Label)_$($account.Code).xml" 
#        $process = Start-Process  -FilePath $scriptPath -ArgumentList $peakworkXmlConfig        
#        $process.ExitCode
#    }
#} 


$destinationFolder = "$($cachePath)\SunHotels"; 
Get-ChildItem  -Path "$($cachePath)" -exclude "SunHotels" | Where-Object {$_.Attributes -match 'Directory'} | Move-Item -destination $destinationFolder 

$destinationZip = "$($destinationFolder).zip"
Remove-Item $destinationZip 
ZipFiles "$($destinationZip)" "$($destinationFolder)"

Remove-Item "$($destinationFolder)"  -recurse  

 