--------------------------------------------------------------------------------
--Name: Get Available hotels for Thomas Cook Continental
--Author: David Malmstrom
--Date: 2017-10-24
--Description: 
--Comments: Checking for availability and removing if blocked on agents.
--Ticket ID: 
-----------------------------------------------
--Modified By: Francisco Capllonch
--Modify Date: 2018-04-09
--Description: added WITH (NOLOCK) 
--Ticket ID: CC-3817 Thomas Cook static export timeout or deadlock        
--------------------------------------------------------------------------------

DECLARE @TCCoEMultiAgent INT = 21649; --Thomas Cook Continental
DECLARE @roomproviders TABLE
    (
        ID INT
    );

INSERT INTO @roomproviders ( ID )
SELECT DISTINCT RP.id
FROM   agent A
       INNER JOIN dbo.multiagent M ON M.sub_agent = A.id
                                      AND M.main_agent = @TCCoEMultiAgent
       CROSS APPLY dbo.roomprovider RP
       LEFT JOIN dbo.agentroomprovider_block AB ON AB.agentid = A.id
                                                   AND RP.id = AB.roomproviderid
WHERE  AB.roomproviderid IS NULL
       AND ISNULL(RP.suspend, 0) = 0;


SELECT   HotelID = H.id
FROM     hotel H WITH ( NOLOCK )
         INNER JOIN dbo.hotelCode HC WITH ( NOLOCK ) ON HC.hotelId = H.id
                                        AND HC.hotelCodeTypeId = 2
         INNER JOIN rooms R WITH ( NOLOCK ) ON R.hotelId = H.id
                               AND ISNULL(R.isLiveRoom,0)= 0
                               AND EXISTS (   SELECT TOP 1 1
                                              FROM   calendar C WITH ( NOLOCK )
                                              WHERE  C.roomId = R.ID
                                                     AND C.datum >= Convert(date, getdate())
											)
         INNER JOIN @roomproviders RP ON RP.ID = R.roomprovider_id
WHERE    ISNULL(H.showonsunhotels, 1) = 1
         AND ISNULL(H.SuspendHotel, 0) = 0
         AND ShowinEN > 0
GROUP BY H.id;