﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SunHotels.XML.Data;

namespace UnitTests
{
    [TestClass]
    public class NoteTests
    {

        [TestMethod]
        public void Boundries_UriParsesLocalPath()
        {
            var xsdFilePath =  "myFile.xsd";
            if (!xsdFilePath.Contains("/"))
                xsdFilePath = "file:///" + (xsdFilePath.Replace("\\", "//"));

            Uri kk = new Uri(xsdFilePath);
            Assert.IsTrue(kk.IsFile); //  kk.LocalPath
            Assert.IsNotNull(kk.LocalPath);

            xsdFilePath = "http://xml.sunhotels.net/schemas/AvailabilityCacheCombined_1.5.0.xsd";
            kk = new Uri(xsdFilePath);
            Assert.IsTrue(kk.IsFile); //  kk.LocalPath
            Assert.IsNotNull(kk.LocalPath);

        }

        [TestMethod]
        public void Note_Repeated_Dates_Date_Before_Start()
        {
            var from = new DateTime(1900, 01, 01);
            var to = new DateTime(1900, 04, 05);
            var today = new DateTime(2012, 05, 01);
            var daySpan = 40;

            var notes = Note.Create(1, from, to, daySpan, today);

            Assert.AreEqual(0, notes.Count);
        }

        [TestMethod]
        public void Note_Repeated_Dates_Date_After_End()
        {
            var from = new DateTime(1900, 02, 01);
            var to = new DateTime(1900, 04, 05);
            var today = new DateTime(2012, 01, 05);
            var daySpan = 5;

            var notes = Note.Create(1, from, to, daySpan, today);

            Assert.AreEqual(0, notes.Count);
        }

        [TestMethod]
        public void Note_Repeated_Dates_Full_Year_Short_Period()
        {
            var from = new DateTime(1900, 01, 01);
            var to = new DateTime(1900, 12, 31);
            var daySpan = 25;
            var today = new DateTime(2012, 08, 08);

            var notes = Note.Create(1, from, to, daySpan, today);

            Assert.AreEqual(1, notes.Count);
            Assert.AreEqual(new DateTime(2012, 08, 08), notes[0].FromDate);
            Assert.AreEqual(new DateTime(2012, 09, 02), notes[0].ToDate);
        }

        [TestMethod]
        public void Note_Repeated_Dates_Full_Year_Long_Period()
        {
            var from = new DateTime(1900, 01, 01);
            var to = new DateTime(1900, 12, 31);
            var daySpan = 606;
            var today = new DateTime(2012, 08, 08);

            var notes = Note.Create(1, from, to, daySpan, today);
            
            Assert.AreEqual(1, notes.Count);
            Assert.AreEqual(new DateTime(2012, 08, 08), notes[0].FromDate);
            Assert.AreEqual(new DateTime(2014, 4, 6), notes[0].ToDate);
        }

        [TestMethod]
        public void Note_Repeated_Dates_Two_Years()
        {
            var from = new DateTime(1900, 02, 06);
            var to = new DateTime(1900, 03, 06);
            var daySpan = 762;
            var today = new DateTime(2012, 01, 01);

            var notes = Note.Create(1, from, to, daySpan, today);

            Assert.AreEqual(2, notes.Count);
            Assert.AreEqual(new DateTime(2012, 02, 06), notes[0].FromDate);
            Assert.AreEqual(new DateTime(2012, 03, 06), notes[0].ToDate);
            Assert.AreEqual(new DateTime(2013, 02, 06), notes[1].FromDate);
            Assert.AreEqual(new DateTime(2013, 03, 06), notes[1].ToDate);
        }

        [TestMethod]
        public void Note_Repeated_Dates_Three_Years_Start_Half()
        {
            var from = new DateTime(1900, 02, 06);
            var to = new DateTime(1900, 03, 06);
            var daySpan = 754;
            var today = new DateTime(2012, 02, 09);

            var notes = Note.Create(1, from, to, daySpan, today);

            Assert.AreEqual(3, notes.Count);
            Assert.AreEqual(new DateTime(2012, 02, 09), notes[0].FromDate);
            Assert.AreEqual(new DateTime(2012, 03, 06), notes[0].ToDate);
            Assert.AreEqual(new DateTime(2013, 02, 06), notes[1].FromDate);
            Assert.AreEqual(new DateTime(2013, 03, 06), notes[1].ToDate);
            Assert.AreEqual(new DateTime(2014, 02, 06), notes[2].FromDate);
            Assert.AreEqual(new DateTime(2014, 03, 04), notes[2].ToDate);
        }

        [TestMethod]
        public void Note_Repeated_Dates_Over_Year()
        {
            var from = new DateTime(1900, 12, 05);
            var to = new DateTime(1901, 01, 04);
            var daySpan = 200;
            var today = new DateTime(2012, 10, 01);

            var notes = Note.Create(1, from, to, daySpan, today);

            Assert.AreEqual(1, notes.Count);
            Assert.AreEqual(new DateTime(2012, 12, 05), notes[0].FromDate);
            Assert.AreEqual(new DateTime(2013, 01, 04), notes[0].ToDate);
        }

        [TestMethod]
        public void Note_Repeated_Dates_Over_Year_Half()
        {
            var from = new DateTime(1900, 12, 05);
            var to = new DateTime(1901, 01, 09);
            var daySpan = 15;
            var today = new DateTime(2012, 12, 20);

            var notes = Note.Create(1, from, to, daySpan, today);

            Assert.AreEqual(1, notes.Count);
            Assert.AreEqual(new DateTime(2012, 12, 20), notes[0].FromDate);
            Assert.AreEqual(new DateTime(2013, 01, 04), notes[0].ToDate);
        }
    }
}
