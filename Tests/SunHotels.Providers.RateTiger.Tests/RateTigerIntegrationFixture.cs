﻿using Moq;
using NUnit.Framework;
using Ploeh.AutoFixture;
using Ploeh.AutoFixture.AutoMoq;
using Sunhotels.Export;
using SunHotels.Infrastructure.Utils;
using SunHotels.Providers.Infrastructure;
using SunHotels.Providers.Parser;
using SunHotels.XML.Data;
using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Xml;
using System.Xml.Schema;

namespace SunHotels.Providers.RateTigerPushTest
{
    [Explicit]
    public class RateTigerIntegrationFixture
    {
        public static string AssemblyDirectory
        {
            get
            {
                var path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
                return path.Replace("\\bin\\Debug", "");
            }
        }

        public RateTigerPush Sut { get; set; }
        public Configuration XmlConfig { get; set; }
        public IFixture Fixture { get; set; }
        public Mock<IConfig> Config { get; set; }
        public IDownloader Downloader { get; set; }
        public IContentParser Parser { get; set; }
        public IRateTigerRoomProvider Provider { get; set; }
        public IRateTigerDal Dal { get; set; }
        [SetUp]
        public void Setup()
        {

            Fixture = new Fixture()
              .Customize(new AutoMoqCustomization());

            Config = Fixture.Freeze<Mock<IConfig>>();
            Downloader = new Downloader(Config.Object);
            Parser = new ContentParser(null);
            Dal = new RateTigerDal(() => { return new System.Data.SqlClient.SqlConnection(""); }, (ex, msg)=> { return true; });
            Provider = new RateTigerRoomProvider(Dal, Parser);

            var asm = Assembly.GetExecutingAssembly();
            var path = $@"{asm.Location.Replace($"{asm.GetName().Name}.dll", string.Empty)}";

            XmlConfig = ConfigurationManager.FromFile($@"{path}TestCases\FileExampleAccorConfig.xml");
            XmlConfig.ExportConfigFile = $@"{path}Providers\FileExampleAccorSettings.xml";

            Sut = new RateTigerPush();
            
            Sut.InitializeProvider(XmlConfig);

        }

        [Test]        
        public void It_get_ratetiger_data_from_sources_and_map_correctly()
        {
            var root = new Root();
            Sut.GetData(root);            
            Assert.IsNotNull(root.Places);
            Assert.IsNotNull(root.RoomTypes);
        }


        [Test]
        public void It_generate_expected_room_xml_id()
        {
            var root = new Root();
            Sut.GetData(root);
            var hotel = root.Places.First().Places.First().Hotels.First();
            var room = hotel.Value.Rooms.First().Value;
            Assert.AreEqual(2, room.RoomId.Split('|').Length);
        }

        [Test]
        public void Call_syncronize()
        {
            Assert.IsTrue(Sut.Syncronize());
        }

        [Test]
        public void It_contains_at_least_one_room_per_room_type()
        {
            var root = new Root();

            Sut.GetData(root);

            foreach (var roomType in root.RoomTypes)
            {
                var actual =
                    root
                    .Places.Where(
                        country => country.Places.Where(
                            city => city.Hotels.Where(
                                hotel => hotel.Value.Rooms.Where(
                                    room => room.Value.TypeId == roomType.Key)
                                    .Any())
                                .Any())
                        .Any());

                Assert.IsTrue(actual.Any());
            }
        }

        [Test]
        public void Call_data_process()
        {
            Sut.StartGathering(XmlConfig);
        }

        [Test]
        [Explicit]
        public void Test_version_170()
        {
            XmlReaderSettings availabilityCombinedSettings = new XmlReaderSettings();
            availabilityCombinedSettings.Schemas.Add("http://xml.sunhotels.net/schemas/AvailabilityCacheCombined_1.7.0", "http://xml.sunhotels.net/schemas/AvailabilityCacheCombined_1.7.0.xsd");
            availabilityCombinedSettings.ValidationType = ValidationType.Schema;
            availabilityCombinedSettings.ValidationEventHandler += new ValidationEventHandler(booksSettingsValidationEventHandler);

            XmlReader data = XmlReader.Create(@"C:\TeamLoki\Import.ProviderToFileImport\SunHotels.Loader\bin\Debug\Cache\Cache.xml", availabilityCombinedSettings);
            while (data.Read()) { }
        }

        static void booksSettingsValidationEventHandler(object sender, ValidationEventArgs e)
        {
            if (e.Severity == XmlSeverityType.Warning)
            {
                Console.Write("WARNING: ");
                Console.WriteLine(e.Message);
            }
            else if (e.Severity == XmlSeverityType.Error)
            {
                Console.Write("ERROR: ");
                Console.WriteLine(e.Message);
            }
        }
    }
}


