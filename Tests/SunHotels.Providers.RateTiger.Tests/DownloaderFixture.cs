﻿using Moq;
using NUnit.Framework;
using Ploeh.AutoFixture;
using Ploeh.AutoFixture.AutoMoq;
using SunHotels.Infrastructure.Utils;
using System;
using System.Net;
using System.Net.Http;
using System.Net.Sockets;
using System.Threading.Tasks;

namespace SunHotels.Providers.Test
{
    [Explicit]
    public class DownloaderFixture
    {
        private int Port { get; set; }

        private HttpListener Server { get; set; }

        private string ServiceUri { get; set; }

        private IFixture Fixture { get; set; }

        private Mock<IConfig> Config { get; set; }

        private Downloader Sut { get; set; }

        [OneTimeSetUp]
        public void OneTimeSetup()
        {
            Port = GetFreeAvailablePort();


            Server = new HttpListener();
            Server.Prefixes.Add($"http://*:{Port}/");
            Server.Start();

            ServiceUri = $"http://localhost:{Port}";
        }

        private static int GetFreeAvailablePort()
        {
            using (var socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp))
            {
                socket.Bind(new IPEndPoint(IPAddress.Parse("127.0.0.1"), 0));
                return ((IPEndPoint)socket.LocalEndPoint).Port;
            }
        }

        [OneTimeTearDown]
        public void OneTimeTearDown()
        {
            if (Server.IsListening)
                Server.Stop();
        }

        [SetUp]
        public void Setup()
        {
            Fixture = new Fixture()
               .Customize(new AutoMoqCustomization());

            Config = Fixture.Freeze<Mock<IConfig>>();
            Config.Setup(s => s.Get<string>("Accor.Username", null))
                .Returns("username");
            Config.Setup(s => s.Get<string>("Accor.Password", null))
                .Returns("password");

            Sut = Fixture.Create<Downloader>();
        }

        [Test]
        public void It_can_initialize()
        {
            Assert.IsNotNull(Sut);
            Assert.IsInstanceOf<IDownloader>(Sut);
        }

        [Test]
        public async Task Download_returns_a_Stream()
        {
            string requestUrl = "";
            var expected = Fixture.Create<byte[]>();
            OnRequest(async ctx =>
            {
                requestUrl = ctx.Request.Url.ToString();
                var response = expected;

                var output = ctx.Response.OutputStream;
                await output.WriteAsync(response, 0, response.Length);
                output.Close();
            });

            var actual = await Sut.DownloadAsync($"{ServiceUri}/0");

            Assert.AreEqual($"{ServiceUri}/0", requestUrl);
            Assert.AreEqual(expected, await actual.ReadAllAsync());
        }

        private void OnRequest(Action<HttpListenerContext> actionOnRequestReceived)
        {
            Server.GetContextAsync()
                .ContinueWith(async task =>
                {
                    var context = await task;
                    actionOnRequestReceived(context);
                });
        }

        [Test]
        public void Download_throws_if_failed()
        {
            var expectedStatusCode = 503;
            var expectedStatusDescription = Fixture.Create("StatusDescription");
            OnRequest(ctx =>
            {
                var response = ctx.Response;

                response.StatusCode = expectedStatusCode;
                response.StatusDescription = expectedStatusDescription;

                response.Close();
            });

            var ex = Assert.ThrowsAsync<HttpRequestException>(() => Sut.DownloadAsync($"{ServiceUri}/0"));
        }
        
    }
}
