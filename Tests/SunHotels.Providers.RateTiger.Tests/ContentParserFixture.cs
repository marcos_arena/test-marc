﻿using NUnit.Framework;
using Ploeh.AutoFixture;
using Ploeh.AutoFixture.AutoMoq;
using SunHotels.Providers.Model;
using SunHotels.Providers.Parser;
using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace SunHotels.Providers.Test
{
    public class ContentParserFixture
    {
        private IFixture Fixture;

        private ContentParser Sut;
        private string WorkingDirectory { get; set; }

        [SetUp]
        public void Setup()
        {
            var asm = Assembly.GetExecutingAssembly();
            WorkingDirectory = $@"{asm.Location.Replace($"{asm.GetName().Name}.dll", string.Empty)}";

            Fixture = new Fixture()
            .Customize(new AutoMoqCustomization());
            Sut = Fixture.Create<ContentParser>();
        }

        private Stream GetFileStream(string fileName)
        {
            return new FileStream(GetFileRoot(fileName), FileMode.Open, FileAccess.Read);
        }

        private string GetFileRoot(string fileName)
        {
            return $"{WorkingDirectory}TestCases\\{fileName}";
        }

        [Test]
        public async Task It_can_parse_ota_xml_content()
        {
            using (var content = GetFileStream("FileExampleOtaContentExample.xml"))
            {
                var urlList = await Sut.GetHotelXmlUrlFromOtaContentAsync(content);
                Assert.IsNotNull(urlList);
            }
        }

        [Test]
        public async Task It_can_parse_ota_xml_hotel_content()
        {
            using (var content = GetFileStream("FileExampleOtaHotelContent.xml"))
            {
                var contentRepository = await Sut.GetHotelContentFromXMLAsync(content);
                Assert.IsNotNull(contentRepository);
            }
        }

        [Test]
        [Explicit]
        public async Task It_can_parse_ota_excel_hotel_content()
        {
            using (var content = GetFileStream("FileExampleAccorRates.xlsx"))
            {
                var hotelsFromExcel = await Sut.GetHotelContentFromExcelAsync(content, new string[] { });
                Assert.IsNotNull(hotelsFromExcel);
            }
        }

        [Test]

        public async Task It_can_read_single_room_ratetiger_rates_and_parse_correctly()
        {
            using (var content = GetFileStream(("FileExampleForSingleRoomAccorRates.xlsx")))
            {
                var hotelsFromExcel = await Sut.GetHotelContentFromExcelAsync(content, new string[] { });

                Assert.IsNotNull(hotelsFromExcel);

                var hotels = hotelsFromExcel.ToArray();
                Assert.IsFalse(hotels[0].TaxeIncluded);
                Assert.IsTrue(hotels[1].TaxeIncluded);
                Assert.IsTrue(hotels[2].TaxeIncluded);
                Assert.AreEqual(hotels[0].TaxeUnit, TaxeUnit.Amount);
                Assert.AreEqual(hotels[1].TaxeUnit, TaxeUnit.Percentage);
                Assert.AreEqual(hotels[2].TaxeUnit, TaxeUnit.Amount);
            }
        }

        [Test]
        public async Task It_can_read_missing_hotels()
        {
            using (var content = GetFileStream("MissingHotelIssue.xlsx"))
            {
                var hotelsFromExcel = await Sut.GetHotelContentFromExcelAsync(content, new string[] { });
                Assert.IsNotNull(hotelsFromExcel);
            }
        }

        [Test]
        public async Task It_can_correctly_read_decimal_value()
        {
            using (var content = GetFileStream("FileExampleWrongDecimalConverter.xlsx"))
            {
                var contentRepository = await Sut.GetHotelContentFromExcelAsync(content, new string[] { });
                var actual = Convert.ToDecimal(contentRepository.Last().TaxeAmount);
                Assert.AreEqual(19.6m, actual);
            }
        }

        [Test]
        public async Task It_can_parse_correctly()
        {
            using (var content = GetFileStream("FileExampleAccorRates.xlsx"))
            {
                var actual = await Sut.GetHotelContentFromExcelAsync(content, new string[] { });
                Assert.IsNotNull(actual);
            }
        }

        [Test]
        public async Task It_can_parse_correctly_new_format_file_CC_3030()
        {
            using (var content = GetFileStream("CC-3030.xlsx"))
            {
                var actual = await Sut.GetHotelContentFromExcelAsync(content, new string[] { });
                Assert.IsNotNull(actual);
            }
        }

        [Test]
        public async Task It_will_ignore_invalid_taxes_CC_3694()
        {
            using (var content = GetFileStream("CC-3694.xlsx"))
            {
                var invalidTaxCodes = new string[] {
                    "GSTTRF",
                    "TBEVER",
                    "TCNTRF",
                    "TFBTAX",
                    "TFOOD ",
                    "TNFAB1",
                    "TNFAB2",
                    "TSERVF",
                    "TTAFEF",
                    "TDFFOU",
                    "TDFONE",
                    "TDFTHR",
                    "TDFTWO"
                };
                var actual = await Sut.GetHotelContentFromExcelAsync(content, invalidTaxCodes);
                Assert.IsNotNull(actual);
                CollectionAssert.IsNotEmpty(actual);
                Assert.IsTrue(actual.All(t => !invalidTaxCodes.Contains(t.TaxeCode)));
            }
        }
    }
}
