﻿using NUnit.Framework;
using Ploeh.AutoFixture;
using Ploeh.AutoFixture.AutoMoq;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Xml;

namespace SunHotels.Providers.RateTiger.Tests
{


    class CacheFileFixture
    {
        private string WorkingDirectory;
        private IFixture Fixture;

        [SetUp]
        public void Setup()
        {
            var asm = Assembly.GetExecutingAssembly();
            WorkingDirectory = $@"{asm.Location.Replace($"{asm.GetName().Name}.dll", string.Empty)}";

            Fixture = new Fixture()
            .Customize(new AutoMoqCustomization());

        }

        [Test]
        public void Read_xml_cache()
        {

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(GetFileRoot("CacheExample1.xml"));

            var hotels = xmlDoc.GetElementsByTagName("hotels");

            var invalidXmlRoomId = new List<string>();
            foreach (XmlNode hotel in hotels[0].ChildNodes)
            {

                foreach (XmlNode room in hotel.LastChild.ChildNodes)
                {
                    var xmlRoomId = room["room_id"].InnerText;
                    if (xmlRoomId.Length >= 50)
                        invalidXmlRoomId.Add(xmlRoomId);
                }

            }

            CollectionAssert.IsNotEmpty(invalidXmlRoomId);


        } 

        private Stream GetFileStream(string fileName)
        {
            return new FileStream(GetFileRoot(fileName), FileMode.Open, FileAccess.Read);
        }
        private string GetFileRoot(string fileName)
        {
            var path = $"{WorkingDirectory}Cache\\{fileName}";
            return path;
        }
    }
}
