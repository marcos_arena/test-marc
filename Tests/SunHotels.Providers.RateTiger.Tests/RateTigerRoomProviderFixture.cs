﻿using Moq;
using NUnit.Framework;
using Ploeh.AutoFixture;
using Ploeh.AutoFixture.AutoMoq;
using SunHotels.Providers.Infrastructure;
using SunHotels.Providers.Parser;
using System.Collections.Generic;
using System.Linq;

namespace SunHotels.Providers.RateTiger.Tests
{
    public class RateTigerRoomProviderFixture
    {

        public Mock<IRateTigerDal> Dal { get; set; }
        public Mock<IContentParser> Parser { get; set; }
        public IFixture Fixture { get; set; }
        public RateTigerRoomProvider Sut { get; set; }

        private const int MaxRoomXmlIdLength = 50;

        [SetUp]

        public void SetUp()
        {

            Fixture = new Fixture()
                .Customize(new AutoMoqCustomization());

            Dal = Fixture.Freeze<Mock<IRateTigerDal>>();

            Parser = Fixture.Freeze<Mock<IContentParser>>();

            Sut = new RateTigerRoomProvider(Dal.Object, Parser.Object);
        }

        [Test]
        public void It_can_instance()
        {
            Assert.IsNotNull(Sut);
        }

        [Test]
        public void It_can_mapp_room_correctly()
        {

            var xmlRoomIds = new List<string>()
            {
                "1281.TWC.2.0.2.1.RB1SH|2b#10g",
                "1281.TWC200.2.0.2.1.RB1SH|2b#10g",
                "1281.TWC100.2.0.2.1.RB1SH|2b#10g",
            };

            var roomMappedResult = new List<RateTigerRoomDataRecord>();
            roomMappedResult.Add(new RateTigerRoomDataRecord(6374689, "1281|TWC|2|1|RB1SH|2|0"));
            roomMappedResult.Add(new RateTigerRoomDataRecord(7015895, "1281|DBA|2|0|1|0"));
            roomMappedResult.Add(new RateTigerRoomDataRecord(7015896, "1281|DBA|2|0|2|0"));
            Dal.Setup(t => t.GetRoomMappedRooms(It.IsAny<int[]>()))
                .Returns(roomMappedResult);

            var actual = Sut.GetRoomMap(Fixture.Create<int[]>(), xmlRoomIds, Fixture.Create<int>(), MaxRoomXmlIdLength);

            Assert.IsNotEmpty(actual);
            Assert.IsTrue(actual.Where(t => t.RoomXmlIdWithRate == "1281.TWC.2.0.2.1.RB1SH|2b#10g").Count() == 1);
        }
         
        [Test]
        [TestCase("1281.TWC.2.0.2.1.RB1SH|2b#10;10;10;10;10;10;10;10g", false)] 
        public void It_should_mark_to_be_deleted_rooms_with_xmlroomid_length_greather_than_maximun_xmlroomid_lenght(string xmlRoomId, bool toBeDeleted)
        {

            var xmlRoomIds = new List<string>() { xmlRoomId };
            var roomMappedResult = new List<RateTigerRoomDataRecord>();

            roomMappedResult.Add(new RateTigerRoomDataRecord(6374689, "1281|TWC|2|1|RB1SH|2|0"));
            Dal.Setup(t => t.GetRoomMappedRooms(It.IsAny<int[]>()))
                .Returns(roomMappedResult);

            var actual = Sut.GetRoomMap(Fixture.Create<int[]>(), xmlRoomIds, Fixture.Create<int>(), MaxRoomXmlIdLength);
            CollectionAssert.IsNotEmpty(actual);

            var roomWithWrongLength = actual.First(t => t.RoomId == 6374689);
            Assert.IsNotNull(roomWithWrongLength);
            Assert.IsTrue(roomWithWrongLength.ToBeDeleted == toBeDeleted);

        }




    }
}
