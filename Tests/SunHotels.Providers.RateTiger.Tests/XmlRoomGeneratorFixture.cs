﻿using NUnit.Framework;
using Ploeh.AutoFixture;
using Ploeh.AutoFixture.AutoMoq;
using SunHotels.Providers.Model;
using SunHotels.Providers.Parser;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SunHotels.Providers.RateTiger.Tests
{
    public class XmlRoomGeneratorFixture
    {
        private IContentParser Parser { get; set; }
        private IXmlRoomGenerator Sut { get; set; }
        private IFixture Fixture { get; set; }

        [SetUp]
        public void Setup()
        {
            Parser = new ContentParser(null);
            Sut = new XmlRoomGenerator(Parser);
            Fixture = new Fixture().Customize(new AutoMoqCustomization());
        }

        [Test]
        public void It_generate_the_xml_room_id_with_the_sum_of_amounts()
        {

            var hotelRates = new List<RateTigerHotelExcelRowDto>()
            {
                CreateRateTigerHotelItem("RSHAA", TaxeUnit.Amount, TaxeCalculationRule.PerNightPerPerson, true, 10.0m),
                CreateRateTigerHotelItem("RSHAA", TaxeUnit.Amount, TaxeCalculationRule.PerNightPerPerson, true, 5.0m),
                CreateRateTigerHotelItem("RSHAA", TaxeUnit.Amount, TaxeCalculationRule.PerNightPerPerson, true, 6.0m),
                CreateRateTigerHotelItem("RSHAA", TaxeUnit.Amount, TaxeCalculationRule.PerRoomPerNight, true, 6.0m)
            };
             
            var groupByRates = hotelRates.GroupBy(g => new
            {
                g.HotelCode,
                g.RateCode,
                g.RoomType,
                g.MaxOccupancy,
                g.MaxAdultsOccupancy,
                g.MaxChildsOccupancy
            });

            foreach (var groupedRate in groupByRates)
            {
                var xmlRoomId = Sut.GetXmlRoomItem(groupedRate, Fixture.Create<string>()); 
                Assert.IsTrue(xmlRoomId.XmlRoomId.EndsWith("|21a#6c"));
            }
        }

        private RateTigerHotelExcelRowDto CreateRateTigerHotelItem(string rateCode, TaxeUnit taxUnit, TaxeCalculationRule calculationRule, bool taxeIncluded, decimal amount)
        {

            return Fixture.Build<RateTigerHotelExcelRowDto>()
                .With(t => t.HotelCode, "H")
                .With(t => t.RateCode, rateCode)
                .With(t => t.RoomType, "R")
                .With(t => t.MaxOccupancy, 3)
                .With(t => t.MaxAdultsOccupancy, 2)
                .With(t => t.MaxChildsOccupancy, 1)

                .With(t => t.TaxeUnit, taxUnit)
                .With(t => t.TaxeCalculationRule, calculationRule)
                .With(t => t.TaxeIncluded, taxeIncluded)
                .With(t => t.TaxeAmount, amount.ToString())
                .Create();
        }
    }
}
