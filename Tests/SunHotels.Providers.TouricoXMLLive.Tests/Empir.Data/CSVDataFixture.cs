﻿using Empir.Data.CSV.FIX;
using NUnit.Framework;
using System.Reflection;

namespace SunHotels.Providers.TouricoXMLLive.Tests
{
    public class CSVDataFixture
    {

        public CSVData Sut { get; set; }

        [SetUp]
        public void SeTup()
        {
            Sut = new CSVData();
        }

        [Test]
        public void It_can_process_data_with_empty_lines()
        {
            var asm = Assembly.GetExecutingAssembly();
            var path = $@"{asm.Location.Replace($"{asm.GetName().Name}.dll", string.Empty)}";
            var fileName = $@"{path}Empir.Data\file_with_empty_lines.csv";

            Sut.SEPARATOR_CHAR = '|';
            
            var actual  = Sut.Read(fileName);
            
            Assert.IsNotNull(actual);
            
            
        }
    }
}
