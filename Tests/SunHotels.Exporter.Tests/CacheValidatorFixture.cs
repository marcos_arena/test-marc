﻿using NUnit.Framework;
using System;
using System.Xml;
using System.Xml.Schema;

namespace SunHotels.Exporter.Tests
{
    [Explicit]
    public class CacheValidatorFixture
    {


        [SetUp]
        public void Setup()
        {

        }


        [Test]
        public void Test_version_170()
        {
            XmlReaderSettings availabilityCombinedSettings = new XmlReaderSettings();
            availabilityCombinedSettings.Schemas.Add("http://xml.sunhotels.net/schemas/AvailabilityCacheCombined_1.7.0", "http://xml.sunhotels.net/schemas/AvailabilityCacheCombined_1.7.0.xsd");
            availabilityCombinedSettings.ValidationType = ValidationType.Schema;
            availabilityCombinedSettings.ValidationEventHandler += new ValidationEventHandler(booksSettingsValidationEventHandler);

            XmlReader data = XmlReader.Create(@"C:\Test\Cache.xml", availabilityCombinedSettings);
            while (data.Read()) { }
        }

        [Test]
        public void Test_version_CacheRoomOutput()
        {
            XmlReaderSettings availabilityCombinedSettings = new XmlReaderSettings();
            availabilityCombinedSettings.Schemas.Add("HotelRoomsCache_1.7.0", "http://xml.dev.sunhotels.net:8088/schemas/HotelRoomsCache_1.7.0.xsd");
            availabilityCombinedSettings.ValidationType = ValidationType.Schema;
            availabilityCombinedSettings.ValidationEventHandler += new ValidationEventHandler(booksSettingsValidationEventHandler);

            XmlReader data = XmlReader.Create(@"C:\Test\HotelRoomsCache.xml", availabilityCombinedSettings);
            while (data.Read()) { }
        }

        [Test]
        public void Test_version_CacheHotelOutput()
        {
            XmlReaderSettings availabilityCombinedSettings = new XmlReaderSettings();
            availabilityCombinedSettings.Schemas.Add("HotelCache_1.7.0", "http://xml.dev.sunhotels.net:8088/schemas/HotelCache_1.7.0.xsd");
            availabilityCombinedSettings.ValidationType = ValidationType.Schema;
            availabilityCombinedSettings.ValidationEventHandler += new ValidationEventHandler(booksSettingsValidationEventHandler);

            XmlReader data = XmlReader.Create(@"C:\Test\HotelCache.xml", availabilityCombinedSettings);
            while (data.Read()) { }
        }

        static void booksSettingsValidationEventHandler(object sender, ValidationEventArgs e)
        {
            if (e.Severity == XmlSeverityType.Warning)
            {
                Console.Write("WARNING: ");
                Console.WriteLine(e.Message);
            }
            else if (e.Severity == XmlSeverityType.Error)
            {
                Console.Write("ERROR: ");
                Console.WriteLine(e.Message);
            }
        }

    }
}
