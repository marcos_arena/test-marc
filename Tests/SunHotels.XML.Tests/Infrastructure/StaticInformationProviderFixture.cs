﻿using System.Threading.Tasks;
using Moq;
using NUnit.Framework;
using Ploeh.AutoFixture;
using Ploeh.AutoFixture.AutoMoq;
using SunHotels.XML.Infrastructure;
using System.Collections.Generic;
using SunHotels.XML.Infrastructure.Models;

namespace SunHotels.XML.Tests.Infrastructure
{
    public class StaticInformationProviderFixture
    {
        public IFixture Fixture;
        public Mock<IStaticInformationDal> StaticDal;
        public Mock<IHotelDal> HotelDal;
        public IStaticInformationProvider Sut;

        private int CountryId;
        private int ResortId;
        private int DestinationId;
        private string Language;

        [SetUp]
        public void Setup()
        {
            Fixture = new Fixture().Customize(new AutoMoqCustomization());
            HotelDal = Fixture.Freeze<Mock<IHotelDal>>();
            HotelDal.Setup(t => t.GetHotelDataByCountryWithoutLanguageAsync(It.IsAny<int>())).Returns(Task.FromResult(Fixture.Create<IEnumerable<HotelDataRecord>>()));
            HotelDal.Setup(t => t.GetHotelDataByResortIdAsync(It.IsAny<int>(), It.IsAny<string>())).Returns(Task.FromResult(Fixture.Create<IEnumerable<HotelDataRecord>>()));

            StaticDal = Fixture.Freeze<Mock<IStaticInformationDal>>();
            StaticDal.Setup(t => t.GetAllDestinations(It.IsAny<string>())).Returns(Task.FromResult(Fixture.Create<IEnumerable<DestinationDataRecord>>()));
            StaticDal.Setup(t => t.GetCountriesAsync(It.IsAny<string>())).Returns(Task.FromResult(Fixture.Create<IEnumerable<CountryDataRecord>>()));
            StaticDal.Setup(t => t.GetCountriesByDestinationAsync(It.IsAny<string>(), It.IsAny<int[]>())).Returns(Task.FromResult(Fixture.Create<IEnumerable<CountryDataRecord>>()));
            StaticDal.Setup(t => t.GetCountryRoomStaticAsync(It.IsAny<int>())).Returns(Task.FromResult(Fixture.Create<IEnumerable<RoomStaticDataRecord>>()));
            StaticDal.Setup(t => t.GetDestinationsByCountryData(It.IsAny<int>(), It.IsAny<string>())).Returns(Task.FromResult(Fixture.Create<IEnumerable<DestinationDataRecord>>()));
            StaticDal.Setup(t => t.GetHotelGiataCodesAsync()).Returns(Task.FromResult(Fixture.Create<Dictionary<int, string>>()));
            StaticDal.Setup(t => t.GetHotelImageByCountryAsync(It.IsAny<int>())).Returns(Task.FromResult(Fixture.Create<IEnumerable<HotelImageDataRecord>>()));
            StaticDal.Setup(t => t.GetHotelImageByResortAsync(It.IsAny<int>())).Returns(Task.FromResult(Fixture.Create<IEnumerable<HotelImageDataRecord>>()));
            StaticDal.Setup(t => t.GetMealLabelByCountryAsync(It.IsAny<int>())).Returns(Task.FromResult(Fixture.Create<IEnumerable<MealDataRecord>>()));
            StaticDal.Setup(t => t.GetMealLabelForThomasCookAsync(It.IsAny<int>())).Returns(Task.FromResult(Fixture.Create<IEnumerable<MealDataRecord>>()));
            StaticDal.Setup(t => t.GetResortRoomStaticAsync(It.IsAny<int>(), It.IsAny<string>())).Returns(Task.FromResult(Fixture.Create<IEnumerable<RoomStaticDataRecord>>()));
            StaticDal.Setup(t => t.GetResorts(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<bool>(), It.IsAny<bool>())).Returns(Task.FromResult(Fixture.Create<IEnumerable<ResortDataRecord>>()));
            StaticDal.Setup(t => t.GetRoomStaticAsync(It.IsAny<int[]>(), It.IsAny<string>())).Returns(Task.FromResult(Fixture.Create<IEnumerable<RoomStaticDataRecord>>()));

            Sut = new StaticInformationProvider(StaticDal.Object, HotelDal.Object);

            CountryId = Fixture.Create<int>();
            ResortId = Fixture.Create<int>();
            DestinationId = Fixture.Create<int>();
            Language = Fixture.Create<string>();
        }

        [Test]
        public void It_can_initialize()
        {
            Assert.IsNotNull(Sut);
        }

        [Test]
        public async Task It_can_call_GetCountryDestinationAsync()
        { 
            var data = await Sut.GetCountryDestinationAsync(CountryId, Language);
            Assert.IsNotNull(data);

        }
        [Test]
        public async Task It_can_call_GetResortRoomsAsync()
        { 
            var data = await Sut.GetResortRoomsAsync(ResortId, Language);
        }
        [Test]
        public async Task It_can_call_GetCountryRoomStaticAsync()
        {
           
            var data = await Sut.GetCountryRoomStaticAsync(CountryId);

            Assert.IsNotNull(data);

        }
        [Test]
        public async Task It_can_call_GetCountryHotelsAsync()
        {
           
            var data = await Sut.GetCountryHotelsAsync(CountryId);
            Assert.IsNotNull(data);
        }
        [Test]
        public async Task It_can_call_GetCountryHotelByResortsAsync()
        {
           
            var data = await Sut.GetCountryHotelsAsync(ResortId, Language);
            Assert.IsNotNull(data);
        }
        [Test]
        public async Task It_can_call_GetHotelImageByResortAsync()
        {
           
            var data = await Sut.GetHotelImageByResortAsync(ResortId);
            Assert.IsNotNull(data);
        }
        [Test]
        public async Task It_can_call_GetHotelImageByCountryAsync()
        {
           
            var data = await Sut.GetHotelImageByCountryAsync(CountryId);
            Assert.IsNotNull(data);
        }
        [Test]
        public async Task It_can_call_GetResortsAsync()
        {
   
            var useOldResort = true;
            var includeAllResorts = false;

            var data = await Sut.GetResortsAsync(DestinationId, Language, useOldResort, includeAllResorts);
            Assert.IsNotNull(data);
        }
        [Test]
        public async Task It_can_call_GetMealLabelsAsync()
        {
            var data = await Sut.GetMealLabelsAsync();
            Assert.IsNotNull(data);
        }
        [Test]
        public async Task It_can_call_GetMealLabelsByCountryAsync()
        {            
            var data = await Sut.GetMealLabelsByCountryAsync(CountryId);
            Assert.IsNotNull(data);
        }
        [Test]
        public async Task It_can_call_GetMealLabelForThomasCookAsync()
        {
            
            var data = await Sut.GetMealLabelForThomasCookAsync(ResortId);
            Assert.IsNotNull(data);
        }
        [Test]
        public async Task It_can_call_GetCountriesAsync()
        { 
            var data = await Sut.GetCountriesAsync(Language);
            Assert.IsNotNull(data);
        }
        [Test]
        public async Task It_can_call_GetCountriesByDestinationAsync()
        { 
            int[] destinationIds = new int[] { DestinationId };

            var data = await Sut.GetCountriesByDestinationAsync(Language, destinationIds);
        }
        [Test]
        public async Task It_can_call_GetHotelGiataCodesAsync()
        {
            var data = await Sut.GetHotelGiataCodesAsync();
        }
    }
}
