﻿using Moq;
using NUnit.Framework;
using Ploeh.AutoFixture;
using Ploeh.AutoFixture.AutoMoq;
using SunHotels.XML.Data;
using SunHotels.XML.Infrastructure;
using SunHotels.XML.Infrastructure.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SunHotels.XML.Tests.Infrastructure
{
    
    public class RooomConfigurationProviderFixture
    {

        public Mock<IRoomConfigurationDal> Dal; 

        public RoomConfigurationProvider Sut;

        public IFixture Fixture;

        [SetUp]
        public void Setup()
        {
            Fixture = new Fixture().Customize(new AutoMoqCustomization());
            Dal = Fixture.Freeze<Mock<IRoomConfigurationDal>>(); 
            IEnumerable<RoomConfigurationDataRecord> roomConfiguration = Fixture.Create<List<RoomConfigurationDataRecord>>();
            Dal.Setup(t => t.GetRoomConfigurationAsync(It.IsAny<int[]>())).Returns(Task.FromResult(roomConfiguration));            

            IEnumerable<RoomExtraBedFactorsDataRecord> extraBeds = Fixture.Create<List<RoomExtraBedFactorsDataRecord>>();
            Dal.Setup(t => t.GetRoomConfigurationExtraBedFactorsAsync()).Returns(Task.FromResult(extraBeds));

            IEnumerable<RoomOccupancyDataRecord> occupancies = Fixture.Create<List<RoomOccupancyDataRecord>>();
            Dal.Setup(t => t.GetRoomConfigurationOccupanciesAsync()).Returns(Task.FromResult(occupancies));

            IEnumerable<RoomInfantsAndChildrenDataRecord> childs = Fixture.Create<List<RoomInfantsAndChildrenDataRecord>>();
            Dal.Setup(t => t.GetRoomInfantsAndChildrenAsync()).Returns(Task.FromResult(childs));

            IEnumerable<RoomOccupancyBlocksDataRecord> blocks = Fixture.Create<List<RoomOccupancyBlocksDataRecord>>();
            Dal.Setup(t => t.GetRoomOccupancyBlocksAsync()).Returns(Task.FromResult(blocks));

            IEnumerable<RoomConfigurationDataRecord> roomConfigurations = Fixture.Create<List<RoomConfigurationDataRecord>>();
            Dal.Setup(t => t.GetAllRoomConfigurationAsync()).Returns(Task.FromResult(roomConfigurations));

            Sut = new RoomConfigurationProvider(Dal.Object ); 
        }


        [Test]
        public void It_can_initialize()
        {
            Assert.IsNotNull(Sut); 
        }

        [Test]
        public async Task It_call_dal_when_fill_room_configuration()
        {
            var lang = Fixture.Create<string>();
            
            Dictionary<int, List<Room>> hotelRooms = new Dictionary<int, List<Room>>();
            await Sut.FillRoomConfigurationAsync(hotelRooms.SelectMany(o=> o.Value), null,  lang);
            Dal.Verify(t => t.GetRoomConfigurationExtraBedFactorsAsync(), Times.Once());
            Dal.Verify(t => t.GetRoomConfigurationOccupanciesAsync(), Times.Once());
            Dal.Verify(t => t.GetRoomInfantsAndChildrenAsync(), Times.Once());
            Dal.Verify(t => t.GetRoomOccupancyBlocksAsync(), Times.Once());
        }

    }
}
