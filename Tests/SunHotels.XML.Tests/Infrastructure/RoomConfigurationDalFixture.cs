﻿using NUnit.Framework;
using Ploeh.AutoFixture;
using Ploeh.AutoFixture.AutoMoq;
using SunHotels.XML.Infrastructure;
using System.Threading.Tasks;

namespace SunHotels.XML.Tests
{
    [Explicit]
    public class RoomConfigurationDalFixture
    {
        public IRoomConfigurationDal Sut { get; set; }
        public IFixture Fixture { get; set; }

        public const string ConnectionString = @"Data Source=devsql2k14.shdev.local\Develop_D;Initial Catalog=sunhotels;Persist Security Info=True;User ID=app_import_103;Password=35Lf6(#";

        [SetUp]
        public void Setup()
        {
            Fixture = new Fixture().Customize(new AutoMoqCustomization());

            Sut = new RoomConfigurationDal(ConnectionString);
        }

      
        [Test]
        public async Task It_can_get_data_from_GetRoomConfigurationExtraBedFactors()
        {
            var data = await Sut.GetRoomConfigurationExtraBedFactorsAsync();
            Assert.IsNotNull(data);
        }

        [Test]
        public async Task It_can_get_data_from_GetRoomConfigurationOccupancies()
        {
            var data = await Sut.GetRoomConfigurationOccupanciesAsync();
            Assert.IsNotNull(data);
        }

        [Test]
        public async Task It_can_get_data_from_GetRoomInfantsAndChildren()
        {
            var data = await Sut.GetRoomInfantsAndChildrenAsync();
            Assert.IsNotNull(data);
        }

        [Test]
        public async Task It_can_get_data_from_GetRoomOccupancyBlocks()
        {
            var data = await Sut.GetRoomOccupancyBlocksAsync();
            Assert.IsNotNull(data);
        }


        [Test]
        public async Task It_can_get_data_from_GetRoomConfiguration()
        {
            var data = await Sut.GetRoomConfigurationAsync(new[] { 6372275,
                6372276,
                6372277,
                6372278,
                6372279,
                6372280,
                6372281,
                6372282,
                6372283,
                6372284,
                6372285,
                6372286,
                6372287});

            Assert.IsNotNull(data);
        }

    }
}


