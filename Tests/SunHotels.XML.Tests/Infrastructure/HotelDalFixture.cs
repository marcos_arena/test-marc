﻿using NUnit.Framework;
using Ploeh.AutoFixture;
using Ploeh.AutoFixture.AutoMoq;
using SunHotels.XML.Infrastructure;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace SunHotels.XML.Tests.Infrastructure
{
    [TestFixture, Description("SunHotels.XML HotelDalFixture")]
    public class HotelDalFixture
    {
        public IHotelDal Sut { get; set; }
        public IFixture Fixture { get; set; }

        public const string ConnectionString = @"Data Source=devsql2k14.shdev.local\Develop_D;Initial Catalog=sunhotels;Persist Security Info=True;User ID=app_import_103;Password=35Lf6(#";

        [SetUp]
        public void Setup()
        {
            Fixture = new Fixture().Customize(new AutoMoqCustomization());

            Sut = new HotelDal(ConnectionString);
        }

        [Test]

        public async Task It_can_get_hotel_data_by_country_id_without_language()
        {
            var countryId = 2;
            var data = await Sut.GetHotelDataByCountryWithoutLanguageAsync(countryId);
            Assert.IsNotNull(data);
            CollectionAssert.IsNotEmpty(data);
        }

        
        [Repeat(2)]
        [TestCase(2)]
        public Task It_can_get_hotel_data_by_country_id_without_language_multipleTasks(int taskNumber)
        {
            var countryId = 2;
            var tasks = Enumerable.Range(0, taskNumber).Select(task => GetCountryTask(countryId));
            return Task.WhenAll(tasks.ToArray());
        }

        private async Task GetCountryTask(int countryId)
        {
            var data = await Sut.GetHotelDataByCountryWithoutLanguageAsync(countryId);
            Assert.IsNotNull(data);
            CollectionAssert.IsNotEmpty(data);

            var dictionary = data.ToDictionary(t => t.id, t => t);
            CollectionAssert.IsNotEmpty(dictionary);
        }
         

        [Test]

        public async Task It_can_get_hotel_data_by_resort_id()
        {
            var resortId = 8;
            var language = "es";
            var data = await Sut.GetHotelDataByResortIdAsync(resortId, language);
            Assert.IsNotNull(data);
            CollectionAssert.IsNotEmpty(data);
        }

    }
}
