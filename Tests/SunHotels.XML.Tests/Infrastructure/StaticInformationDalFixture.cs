﻿using NUnit.Framework;
using Ploeh.AutoFixture;
using Ploeh.AutoFixture.AutoMoq;
using SunHotels.XML.Infrastructure;
using System.Threading.Tasks;
using SunHotels.XML.Infrastructure.Models;
using System;
using System.Collections.Generic;

namespace SunHotels.XML.Tests
{
    [Explicit]
    public class StaticInformationDalFixture
    {
        public IStaticInformationDal Sut { get; set; }
        public IFixture Fixture { get; set; }

        public const string ConnectionString = @"Data Source=devsql2k14.shdev.local\Develop_D;Initial Catalog=sunhotels;Persist Security Info=True;User ID=app_import_103;Password=35Lf6(#";

        [SetUp]
        public void Setup()
        {
            Fixture = new Fixture().Customize(new AutoMoqCustomization());

            Sut = new StaticInformationDal(ConnectionString);
        }

        [Test]
        public async Task It_can_get_data_From_Get_resort_rooms()
        {
            var resortId = 22;
            var language = "ES";

            var data = await Sut.GetResortRoomStaticAsync(resortId, language);
            CollectionAssert.IsNotEmpty(data);
        }

        [Test]
        public async Task It_can_get_data_From_Get_country_rooms()
        {
            var countryId = 2;
            var data = await Sut.GetCountryRoomStaticAsync(countryId);
            CollectionAssert.IsNotEmpty(data);
        }

        [Test]
        public void It_can_get_data_from_GetRoomStatic()
        {
            var data = Sut.GetRoomStaticAsync(new[] { 1 }, "en");
            Assert.IsNotNull(data);
        }

        [Test]
        public async Task It_can_get_data_GetAllDestinations()
        {
            var language = "ES";
            var data = await Sut.GetAllDestinations(language);
            CollectionAssert.IsNotEmpty(data);
        }
        [Test]
        public async Task It_can_get_data_GetDestinationsByCountryData()
        {
            var countryId = 1;
            var language = "ES";

            var data = await Sut.GetDestinationsByCountryData(countryId, language);
            CollectionAssert.IsNotEmpty(data);
        }

        [Test]
        public async Task It_can_get_data_GetResorts()
        {
            var destinationId = 4;
            var language = "ES";
            var useOldResort = false;
            var includeAllResorts = false;
            var data = await Sut.GetResorts(destinationId, language, useOldResort, includeAllResorts);
            CollectionAssert.IsNotEmpty(data);
        }

        [Test]
        public async Task It_can_get_data_GetHotelImageByResortAsync()
        {
            var resortId = 22;
            var data = await Sut.GetHotelImageByResortAsync(resortId);
            CollectionAssert.IsNotEmpty(data);
        }

        [Test]
        public async Task It_can_get_data_GetHotelImageByCountryAsync()
        {
            var countryId = 1;
            var data = await Sut.GetHotelImageByCountryAsync(countryId);
            CollectionAssert.IsNotEmpty(data);
        }

        [Test]
        public async Task It_can_get_data_GetMealLabelByCountryAsync()
        {
            var countryId = 1;
            var data = await Sut.GetMealLabelByCountryAsync(countryId);
            CollectionAssert.IsNotEmpty(data);
        }
        [Test]
        public async Task It_can_get_data_GetMealLabelForThomasCookAsync()
        {
            var resortId = 22;
            var data = await Sut.GetMealLabelForThomasCookAsync(resortId);
            CollectionAssert.IsNotEmpty(data);
        }
        [Test]
        public async Task It_can_get_data_GetCountriesAsync()
        {
            var language = "ES";
            var data = await Sut.GetCountriesAsync(language);
            CollectionAssert.IsNotEmpty(data);
        }
        [Test]
        public async Task It_can_get_data_GetCountriesByDestinationAsync()
        {
            var language = "ES";
            var destinations = new[] { 4 };

            var data = await Sut.GetCountriesByDestinationAsync(language, destinations);
            CollectionAssert.IsNotEmpty(data);
        }
        [Test]
        public async Task It_can_get_data_GetHotelGiataCodesAsync()
        {
            var data = await Sut.GetHotelGiataCodesAsync();
            CollectionAssert.IsNotEmpty(data);
        }
    }
}
