﻿using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using Combinatorics.Collections;

namespace SunHotels.Providers.Tests
{
    public class HotelBedsCommonFunctionsTests
    {
        private HotelBedsCommonFunctions Sut { get; set; }

        private class PriceInfo
        {
            public bool IsAdultPrice { get; set; }
            public bool IsChildPrice { get; set; }
        }

        private PriceInfo PaxApplyPrice(string minimumAge, string maximumAge)
        {
            bool adultPrice;
            bool childPrice;
            Sut.PaxApplyPrice(minimumAge, maximumAge, out adultPrice, out childPrice);
            return new PriceInfo { IsAdultPrice = adultPrice, IsChildPrice = childPrice };
        }

        private static IEnumerable<IList<int>> AllCombinations()
        {
            var listSunHotelsAdultAges = Enumerable.Range(0, HotelBedsSettings.AdultMaxAge + 1).ToList();
            var combinations = new Combinations<int>(listSunHotelsAdultAges, 2, GenerateOption.WithRepetition);
            var combFiltered = combinations.Where(c =>
                c[0] < HotelBedsSettings.SeniorAge //Senior range filter
            );
            //var debugCombination = combFiltered.ToList().Select(c => c[0] + " - " + c[1]);
            return combFiltered;
        }

        [SetUp]
        public void Setup()
        {
            Sut = new HotelBedsCommonFunctions();
        }

        [Test]
        public void Can_create_instances()
        {
            Assert.IsNotNull(Sut);
        }

        [Test]
        [TestCase(null, null)]
        [TestCase(null, "")]
        [TestCase("", null)]
        [TestCase("", "")]
        public void PaxApplyPrice_returns_all_true_if_any_age_is_null_or_empty(string minimumAge, string maximumAge)
        {
            var result = PaxApplyPrice(minimumAge, maximumAge);
            Assert.IsTrue(result.IsAdultPrice);
            Assert.IsTrue(result.IsChildPrice);
        }

        [Test]
        [TestCase(null, HotelBedsSettings.SunHotelsAdultAgeBooking)]
        [TestCase("", HotelBedsSettings.SunHotelsAdultAgeBooking)]
        [TestCase(0, HotelBedsSettings.SunHotelsAdultAgeBooking)]
        [TestCase(HotelBedsSettings.SunHotelsChildAgeBooking, HotelBedsSettings.SunHotelsAdultAgeBooking)]
        [TestCase(HotelBedsSettings.SunHotelsChildAgeBooking, HotelBedsSettings.SeniorAge)]
        [TestCase(HotelBedsSettings.SunHotelsChildAgeBooking, HotelBedsSettings.AdultMaxAge)]
        public void PaxApplyPrice_returns_all_true_if_min_age_is_null_empty_lower_or_equal_to_SunHotelsChildAgeBooking_and_max_age_is_greater_or_equal_SunHotelsAdultAgeBooking(object minimumAge, int maximumAge)
        {
            var result = PaxApplyPrice(minimumAge == null ? null : minimumAge.ToString(), maximumAge.ToString());
            Assert.IsTrue(result.IsAdultPrice);
            Assert.IsTrue(result.IsChildPrice);
        }

        [Test]
        [TestCase(null, "-1")]
        [TestCase("-1", null)]
        [TestCase("", "-1")]
        [TestCase("-1", "")]
        [TestCase("1", "-1")]
        [TestCase("-1", "1")]
        [TestCase("-1", "-1")]
        public void PaxApplyPrice_returns_all_false_if_any_age_is_less_than_zero(string minimumAge, string maximumAge)
        {
            var result = PaxApplyPrice(minimumAge, maximumAge);
            Assert.IsFalse(result.IsAdultPrice);
            Assert.IsFalse(result.IsChildPrice);
        }

        [Test]
        [TestCase("12", "10")]
        public void PaxApplyPrice_returns_all_false_if_min_age_is_greater_than_max_age(string minimumAge, string maximumAge)
        {
            var result = PaxApplyPrice(minimumAge, maximumAge);
            Assert.IsFalse(result.IsAdultPrice);
            Assert.IsFalse(result.IsChildPrice);
        }

        [Test]
        [TestCase(HotelBedsSettings.SeniorAge, HotelBedsSettings.SeniorAge)]
        [TestCase(HotelBedsSettings.SeniorAge, HotelBedsSettings.AdultMaxAge)]
        [TestCase(HotelBedsSettings.AdultMaxAge, HotelBedsSettings.AdultMaxAge)]
        public void PaxApplyPrice_returns_all_false_if_ages_are_like_hotelbeds_senior_ages(int minimumAge, int maximumAge)
        {
            var result = PaxApplyPrice(minimumAge.ToString(), maximumAge.ToString());
            Assert.IsFalse(result.IsAdultPrice);
            Assert.IsFalse(result.IsChildPrice);
        }

        [Test]
        [TestCaseSource("AllCombinations")]
        public void PaxApplyPrice_validate_all_possible_combinations_from_0_to_AdultMaxAge_except_senior_ages(IList<int> ages)
        {
            var result = PaxApplyPrice(ages[0].ToString(), ages[1].ToString());
            if (ages[0] > HotelBedsSettings.SunHotelsChildAgeBooking)
            {
                Assert.IsTrue(result.IsAdultPrice);
                Assert.IsFalse(result.IsChildPrice);
            }
            else
            {
                if (ages[1] < HotelBedsSettings.SunHotelsAdultAgeBooking)
                {
                    Assert.IsFalse(result.IsAdultPrice);
                    Assert.IsTrue(result.IsChildPrice);
                }
                else
                {
                    Assert.IsTrue(result.IsAdultPrice);
                    Assert.IsTrue(result.IsChildPrice);
                }
            }
        }
    }
}
