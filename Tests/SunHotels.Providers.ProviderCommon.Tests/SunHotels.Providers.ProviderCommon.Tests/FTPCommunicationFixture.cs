﻿using NUnit.Framework;
using SunHotels.Providers.ProviderCommon.DataLoader;
using System;
using System.Linq;
using System.Net;

namespace SunHotels.Providers.ProviderCommon.Tests
{
    public class FTPCommunicationFixture
    {
        [SetUp]
        public void Setup()
        {

        }

        [TestCase("ftp://ftp.touricoholidays.com", "SHUser_2", "r9HFr5cj")]
        public void It_can_connect_to_tourico_ftp(string ftp, string username, string password)
        {
            var Sut = new FTPCommunication(username, password);
            Sut.ConnectionLimit = 100;
            ServicePointManager.DefaultConnectionLimit = 65000;
            Enumerable.Range(0, 10)
                .AsParallel()
                .WithDegreeOfParallelism(10)
                .All(d =>
            {
                var source = $"{ftp}/HomeInfo/PDS2_HomeVoucherRemark_THF_20170507_T0712.csv.zip";
                var target = $"C:\\temp\\file_{Guid.NewGuid().ToString()}";
                 
                Sut.DownloadFile(source, target);
                Console.WriteLine($"Download {source} finished");
                return true;
            });


        }
    }
}
