﻿using NUnit.Framework;
using SunHotels.Providers.ProviderCommon.DataLoader;
using System.Collections.Generic;
using System.Reflection;
using CsvHelper;
using System.IO;

namespace SunHotels.Providers.ProviderCommon.Tests.DataLoader
{
    public class StaticDataFixture
    {

        public StaticData Sut { set; get; }
        public string WorkingCopyPath { get; set; }

        [SetUp]
        public void SetUp()
        {
            var assembly = Assembly.GetExecutingAssembly();
            WorkingCopyPath = $@"{assembly.Location.Replace($"{assembly.GetName().Name}.dll", string.Empty)}";
            Sut = new StaticData(WorkingCopyPath);
        }

        [Test]
        public void It_can_sanitize_file_correctly()
        {
            var sourceFile = $@"{WorkingCopyPath}\DataLoader\file_with_empty_lines.csv";
            StaticData.SanitizeFile(sourceFile);
        }

        [Test]
        public void CsvReader_can_read_corrupted_csv_file()
        {
            var sourceFile = $@"{WorkingCopyPath}\DataLoader\file_with_empty_lines.csv";

            StaticData.SanitizeFile(sourceFile);

            var records = new List<string[]>();

            using (StreamReader streamReader = File.OpenText(sourceFile))
            {
                using (var csv = new CsvReader(streamReader))
                {
                    csv.Configuration.Delimiter = "|";
                    csv.Configuration.IgnoreBlankLines = true;
                    csv.Configuration.SkipEmptyRecords = true;
                    while (csv.Read())
                        records.Add(csv.CurrentRecord);
                }

            }

            Assert.AreEqual(3, records.Count);
        }

        [TestCase(@"C:\VC\TouricoXmlLiveNew\Tmp\HotelImagesPDS2_HotelImages_THF.csv", "|", "\"")]
        public void CreateTableFromCSVBigFiles(string sourceFile, string separator, string qualifier)
        {
            var fileInfo = new FileInfo(sourceFile);

            ServersServerFolderFileCSV csvOptions = new ServersServerFolderFileCSV()
            {
                qualifier = qualifier,
                separator = separator
            };
            System.Data.DataTable dt = new System.Data.DataTable(fileInfo.Name);
            Sut.FillDataTableFromCSVSource(sourceFile, dt, csvOptions);
            Assert.IsNotNull(dt);
        }
    }

    public class HotelInfoDataRow
    {
        public int HotelId { get; set; }
        public string HotelName { get; set; }
        public int ExclusiveDeal { get; set; }
        public int CheckInHour { get; set; }
        public int CheckOutHour { get; set; }
        public int TotalRoomsInHotel { get; set; }
        public string ThumbnailPath { get; set; }
        public string ShortDescription { get; set; }
        public string HotelCurrency { get; set; }
        public decimal Stars { get; set; }
        public string Address { get; set; }
        public string AddressZip { get; set; }
        public string AddressCity { get; set; }
        public string Location { get; set; }
        public string StateCode { get; set; }
        public string State { get; set; }
        public string CountryCode { get; set; }
        public string CountryName { get; set; }
        public float Longitude { get; set; }
        public float Latitude { get; set; }
        public string sDestination { get; set; }
        public string sHotelCityName { get; set; }
        public int DestinationId { get; set; }
        public string Provider { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string NearestAirportIATACode { get; set; }
        public string RefDirection { get; set; }
        public decimal RefPointDist { get; set; }
        public string DistUnit { get; set; }
        public string TimeStamp { get; set; }
        public string ProductStatus { get; set; }
    }
}
