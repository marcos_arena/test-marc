﻿using NUnit.Framework;
using SunHotels.Providers.Infrastructure;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace SunHotels.Providers.PeakworkTests.Infrastructure
{
    [Explicit]
    public class HotelDalFixture
    {
        const string ConnectionString = @"Data Source=maindb03.sh.local;Initial Catalog=sunhotels;Persist Security Info=True;User ID=app_export;Password=66u2jJK6gQN4ZFb5alcY";
        public HotelDal Sut { get; set; }

        [SetUp]
        public void SetUp()
        {
            Sut = new HotelDal(ConnectionString);
        }

        [Test]
        public async Task It_can_get_export_hotel_info()
        {
            var iataNumbers = string.Empty;
            var agentId = 18927;
            var isAgentPush = false;

            var data = await Sut.GetExportHotelAsync(iataNumbers, agentId, isAgentPush);
            Assert.IsNotNull(data);
            Console.Write(data.Count());
        }

        [Test]
        public async Task It_can_get_room_calendar()
        {
            var hotelId = 4931;
            var agentId = 21886;
            var calendar = await Sut.GetAvailabilityAsync(hotelId, agentId, false, false, false, false);
            Assert.IsNotNull(calendar);
        }

        [Test]
        public async Task It_can_get_calendar()
        {
            var iataNumbers = string.Empty;
            var agentId = 18927;
            var isAgentPush = false;

            var stopwatch = new Stopwatch();

            stopwatch.Start();

            Console.Write("GetExportHotelAsync. {0}ms", stopwatch.ElapsedMilliseconds);
            var data = await Sut.GetExportHotelAsync(iataNumbers, agentId, isAgentPush);
            Console.Write("GetExportHotelAsync completed. {0}ms", stopwatch.ElapsedMilliseconds);

            Assert.IsNotNull(data);

            var bag = new ConcurrentDictionary<int, IEnumerable<CalendarDataRow>>();
            var tasks = new ConcurrentBag<Task>();

            Console.Write("Get Calendar. {0}ms", stopwatch.ElapsedMilliseconds);

            data
                .AsParallel()
                .WithDegreeOfParallelism(10)
                .ForAll(hotel => { tasks.Add(CreateCalendarTask(hotel, agentId, bag)); });

            Console.Write("Get Calendar WhenAll. {0}ms", stopwatch.ElapsedMilliseconds);

            await Task.WhenAll(tasks.ToArray());

            Console.Write("Get Calendar Completed. {0}ms", stopwatch.ElapsedMilliseconds);

            Assert.IsNotEmpty(bag);

        }

        private Task CreateCalendarTask(HotelDataRow hotel, int agentId, ConcurrentDictionary<int, IEnumerable<CalendarDataRow>> bag)
        {
            return Task.Run(async () =>
            {
                var calendar = await Sut.GetAvailabilityAsync(hotel.id, agentId, false, false, false, false);
                bag.TryAdd(hotel.id, calendar);
            });
        }

        [Test]
        public async Task It_can_get_Discounts()
        {
            var roomIds = new int[]
            {
                292,
                446
            };
            
            var calendar = await Sut.GetDiscounts(roomIds);

            Assert.IsNotNull(calendar);

        }
    }
}
