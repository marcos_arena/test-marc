﻿using System.Collections.Generic;
using NUnit.Framework;
using SunHotels.XML.Data;
using System.Linq;
using System.Collections.Concurrent;
using Sunhotels.Export;

namespace SunHotels.Providers.PeakworkTests
{
    [Explicit]
    public class PeakworkEDFFixture
    {
                 
        public Root dataRoot { get; set; }

        public PeakworkEDF Sut { get; set; }

        public ILookup<int, PeakworkEDFDataSet.roomGroupRow> roomGroups { get; set; }

        [SetUp]
        public void Setup()
        {
            roomGroups = CreateRoomGroups();
            dataRoot = CreateRoot(20, 10, 100);

            Configuration config = new Configuration()
            {
                DiscountCacheOutput = "true",
            };

            
            Sut.InitializeProvider(config);

        }

        [Test]
        public void It_can_generate_data_root()
        {
            Assert.IsNotNull(dataRoot);
        }

        [Test]
        public void Process_Place_Hotels_In_Parallel()
        {
            //Sut.ProcessPlaceHotelsInParallel(dataRoot, roomGroups);
        }         

        private ILookup<int, PeakworkEDFDataSet.roomGroupRow> CreateRoomGroups()
        {
            var dataTable = new PeakworkEDFDataSet.roomGroupDataTable();
            var rows = new List<PeakworkEDFDataSet.roomGroupRow>();
            Enumerable.Range(0, 10).All(groupId => {
                var dataRow = dataTable.NewRow();
                dataRow["groupId"] = groupId.ToString();
                rows.Add((PeakworkEDFDataSet.roomGroupRow)dataRow);
                return true;
            });

            return rows.ToLookup(t => t.groupId, t => t);
            
        }

        private Root CreateRoot(int numberOfPlaces, int numberOfHotelsPerPlace, int numberOfRoomsPerHotel)
        {
            var root = new Root()
            {
                Places = CreatePlaces(numberOfPlaces, numberOfHotelsPerPlace, numberOfRoomsPerHotel)
            };

            return root;
        }

        private List<Place> CreatePlaces(int numberOfPlaces, int numberOfHotelsPerPlace, int numberOfRoomsPerHotel)
        {
            var places = new ConcurrentBag<Place>();

            Enumerable.Range(0, numberOfPlaces).AsParallel().All(place =>
            {
                places.Add(new Place()
                {
                    Id = place.ToString(),
                    Hotels = CreateHotels(numberOfHotelsPerPlace, numberOfRoomsPerHotel)
                });
                return true;
            });

            return places.ToList();
        }

        private Dictionary<string, Hotel> CreateHotels(int numberOfHotelsPerPlace, int numberOfRoomsPerHotel)
        {
            var hotels = new ConcurrentDictionary<string, Hotel>();
            Enumerable.Range(0, numberOfHotelsPerPlace).AsParallel().All(hotel =>
            {
                hotels.TryAdd(hotel.ToString(), new Hotel()
                {
                    Rooms = CreateRooms(numberOfRoomsPerHotel)
                });
                return true;
            });
            return hotels.ToDictionary(t => t.Key, t => t.Value);
        }

        private Dictionary<string, Room> CreateRooms(int numberOfRoomsPerHotel)
        {
            var rooms = new ConcurrentDictionary<string, Room>();
            Enumerable.Range(0, numberOfRoomsPerHotel).AsParallel().All(room =>
            {
                rooms.TryAdd(room.ToString(), new Room()
                {
                    RoomId = room.ToString()
                });
                return true;
            });

            return rooms.ToDictionary(t => t.Key, t => t.Value);
        }
    }
}
