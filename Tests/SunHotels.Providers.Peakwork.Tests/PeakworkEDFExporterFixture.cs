﻿using System;
using System.Collections.Generic;
using NUnit.Framework;
using Sunhotels.Export;
using SunHotels.XML.Data;
using System.Linq;
using Ploeh.AutoFixture;
using Ploeh.AutoFixture.AutoMoq;
using System.Xml.Serialization;
using System.IO;
using System.Reflection;

namespace SunHotels.Providers.PeakworkEDFTests
{

    [Explicit]
    public class PeakworkEDFExporterFixture
    {
        public PeakworkEDFExporter Sut;
        public PeakworkEDFOptimizer Optimizer;
        public Configuration PeakWorkConfiguration;
        public PeakworkEDFSettings PeakWorkLocalConfig;
        public IFixture Fixture;
        public string RootDirectory { get; set; }
        const string ConfigurationPath = @"C:\TeamLoki\Import.ProviderToFileImport\Config\PeakworkExportThomasCook\PeakworkEDF_Market1_18927.xml";

        [SetUp]
        public void SetUp()
        {
            var asm = Assembly.GetExecutingAssembly();
            RootDirectory = $@"{asm.Location.Replace($"{asm.GetName().Name}.dll", string.Empty)}";

            var IsDeltaFile = false;
            PeakWorkConfiguration = new Configuration(ConfigurationPath);
            Optimizer = new PeakworkEDFOptimizer(PeakWorkConfiguration);
            PeakWorkLocalConfig = LoadSettings();
            
            Sut = new PeakworkEDFExporter(PeakWorkConfiguration, PeakWorkLocalConfig, Optimizer, IsDeltaFile);
            
            Fixture = new Fixture()
                .Customize(new AutoMoqCustomization());
            
           
        }

        [Test]
        public void It_can_start_instance()
        {
            Assert.IsNotNull(Sut);
        }

        [Test]
        public void It_create_dummy_root_info()
        {
            Root RootData = new Root();

            RootData.Places = new List<Place>()
            {
                new Place()
                {
                    Places = CreateMultiplePlaces(200)
                }
            };

            Assert.IsNotNull(RootData);
        }

        [Test]
        public void It_write_to_xml()
        {
            Root RootData = new Root();

            RootData.Places = new List<Place>()
            {
                new Place()
                {
                    Places = CreateMultiplePlaces(200)
                }
            };

            Sut.Write(RootData);

        }

        private List<Place> CreateMultiplePlaces(int numberOfPlaces)
        {
            var list = new List<Place>();
            Enumerable.Range(0, numberOfPlaces).All(_ =>
            {
                list.Add(CreatePlaceWithHotel(10));
                return true;
            });
            return list;
        }

        private Place CreatePlaceWithHotel(int hotelsPerPlace)
        {
            return new Place()
            {
                Codes = CreatePlaceCodes(),
                Description = "",
                Id = "",
                Hotels = CreateHotels(hotelsPerPlace, 30)
            };
        }

        private List<PlaceCodes> CreatePlaceCodes()
        {
            var placeCodes = new List<PlaceCodes>();
            Enumerable.Range(0, 20).All(_ =>
            {
                placeCodes.Add(new PlaceCodes()
                {
                    Type = "",
                    Value = ""
                });
                return true;
            });
            return placeCodes;
        }

        private Dictionary<string, Hotel> CreateHotels(int totalHotels, int totalRoomsPerHotel)
        {
            var hotels = new Dictionary<string, Hotel>();
            var hotelId = 0;
            Enumerable.Range(0, totalHotels)
                .All(_ =>
                {
                    hotelId++;
                    hotels.Add(hotelId.ToString(), new Hotel()
                    {
                        Id = hotelId.ToString(),
                        Name = "",
                        Rooms = CreateHotelRooms(totalRoomsPerHotel)
                    });
                    return true;
                });
            return hotels;
        }

        private Dictionary<string, Room> CreateHotelRooms(int totalRooms)
        {
            var rooms = new Dictionary<string, Room>();
            var roomId = 0;
            Enumerable.Range(0, totalRooms)
                .All(_ =>
                {
                    roomId++;
                    rooms.Add(roomId.ToString(), new Room()
                    {
                        RoomId = roomId.ToString()
                    });
                    return true;
                });

            return rooms;
        }

        [Test]
        public void It_can_get_correct_PreventAvailabilityPatternFile_from_ConfigSettings()
        {
            var config = LoadSettings();
            Assert.IsNotNull(config);
            Assert.AreEqual(true, config.PreventAvailabilityPatternFile);
        }

        private PeakworkEDFSettings LoadSettings()
        {
            var serializer = new XmlSerializer(typeof(PeakworkEDFSettings));
            var s = new FileStream($@"{RootDirectory}\Files\PeakworkMarketSettings.xml", FileMode.Open, FileAccess.Read);
            return (PeakworkEDFSettings)serializer.Deserialize(s);
        }
    }
}
