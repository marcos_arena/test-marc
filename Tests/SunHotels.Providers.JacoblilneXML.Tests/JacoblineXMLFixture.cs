﻿using NUnit.Framework;
using System.Reflection;
using System.Collections.Generic;
using SunHotels.XML.HelpClasses;
using Sunhotels.Export;

namespace SunHotels.Providers.JacoblilneXML.Tests
{

    [Explicit]
    public class JacoblineXMLFixture
    {
        private ProviderCommon.DataLoader.StaticData staticData;
        private Dictionary<string, string> languageMapping;

        private string workingDirectory;

        private Jacobline Sut { get; set; }
        private Logger logger { get; set; }

        private Configuration configuration { get; set; }

        [SetUp]
        public void Setup()
        {
            var asm = Assembly.GetExecutingAssembly();
            workingDirectory = $@"{asm.Location.Replace($"{asm.GetName().Name}.dll", string.Empty)}";

            staticData = new ProviderCommon.DataLoader.StaticData(workingDirectory);
            languageMapping = new Dictionary<string, string>()
            {
                { "en", "en" }
            };

            configuration = new Configuration();
            logger = new Logger(configuration);
            Sut = new Jacobline(staticData, languageMapping);
        }


        [Test]
        public void It_can_setup()
        {
            Assert.IsNotNull(Sut);
        }

        [Test]
        public void It_can_read_cancellation()
        {
            var path = $@"{workingDirectory}CC-2856\";
            var actual = Sut.GetCancellationPolicy(logger, path, "HotelAvailabilityCancellationPolicy");
            Assert.AreEqual(238, actual.Count);
        }

        [Test]
        public void It_can_read_invalid_line()
        {
            var path = $@"{workingDirectory}CC-2856\";
            var actual = Sut.GetCancellationPolicy(logger, path, "SingleLineInvalidHotelAvailabilityCancellationPolicy");
            Assert.AreEqual(4, actual.Count);
        }

    }
}
