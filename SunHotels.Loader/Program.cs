using System;
using System.Collections.Generic;
using System.IO;
using SunHotels.XML;
using System.Reflection;
using Sunhotels.Export;
using System.Linq;
using System.Threading;
using System.Globalization;

namespace SunHotels.Loader
{
    class Program
    {
        private static Configuration _configuration;
        private static string _customSpecificDestinations;
        private static bool _isDeltaFile;

        public enum ExitCode
        {
            Success = 0,
            NoConfigSpecified = 1,
            OtherError = 255
        }

        static int Main(string[] args)
        {
            try
            {
                // SetUp globalization context for DateTime.Parse, Double.Parse,... etc...
                var enUs = new CultureInfo("en-US");
                Thread.CurrentThread.CurrentCulture = enUs;
                Thread.CurrentThread.CurrentUICulture = enUs;

                if (args.Length == 0)
                {
                    Console.WriteLine("Error: No provider loaded!");
                    Console.WriteLine("Usage: Sunhotels.Loader.exe <configuration file> [<configuration file2> ... <configuration fileN>] [-dest<dest-CSV>] [-delta]");
                    return (int)ExitCode.NoConfigSpecified;
                }

                ParseDestinationFilter(args);

                ParseDeltaFilter(args);

                var now = DateTime.Now;
                Console.WriteLine("Starting process....");

                foreach (var filename in args.Where(a => !a.StartsWith("-")))
                {
                    try
                    {
                        var file = filename;
                        _configuration = ConfigurationManager.FromFile(file);
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Error loading configuration", ex);
                    }

                    if (!String.IsNullOrEmpty(_customSpecificDestinations)) { 
                        _configuration.SpecificDestinations = _customSpecificDestinations;
                    }
                    
                    _configuration.IsDeltaFile = _isDeltaFile;

                    if (!File.Exists(@"Providers\" + _configuration.Provider + ".dll"))
                    {
                        throw new Exception(_configuration.Provider + ".dll not found");
                    }

                    var providerAssembly = Assembly.LoadFrom(@"Providers\" + _configuration.Provider + ".dll");
                    var typeProvider = providerAssembly.GetType("SunHotels.Providers." + _configuration.Provider);

                    var provider = (XMLProvider)Activator.CreateInstance(typeProvider);
                    provider.InitializeProvider(_configuration);
                    if (args.Any(a => a.IndexOf("validateOnly", StringComparison.Ordinal) != -1))
                    {
                        provider.ValidateFiles();
                    }
                    else
                    {
                        provider.StartGathering(_configuration);
                    }
                }

                if (args.Any(a => a.IndexOf("pause", StringComparison.Ordinal) != -1))
                {
                    Console.WriteLine("Total processing time: {0}\nPress any key", DateTime.Now - now);
                    Console.ReadKey();
                } 
                return (int)ExitCode.Success;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString()); 
                return (int)ExitCode.OtherError;
            }
        }

        private static void ParseDestinationFilter(IEnumerable<string> args)
        {
            const string destinationKey = "-dest";
            var arg = args.SingleOrDefault(x => x.StartsWith(destinationKey));
            if (arg == null)
                return;

            _customSpecificDestinations = arg.Substring(destinationKey.Length);
        }

        private static void ParseDeltaFilter(IEnumerable<string> args)
        {
            const string deltaKey = "-delta";
            var arg = args.SingleOrDefault(x => x.StartsWith(deltaKey));
            if (arg != null)
                _isDeltaFile = true;
        }
    }
}
